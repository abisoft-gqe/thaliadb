from rest_framework import pagination

class BrAPIPagination(pagination.PageNumberPagination):
    page_size = 100
    page_size_query_param = 'pageSize'
    max_page_size = 500
    