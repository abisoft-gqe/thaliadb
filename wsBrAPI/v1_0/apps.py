from django.apps import AppConfig


class V10Config(AppConfig):
    name = 'wsBrAPI.v1_0'
