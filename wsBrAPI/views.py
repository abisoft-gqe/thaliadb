
# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Laetitia Courgey

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from rest_framework.decorators import api_view
from rest_framework.response import Response
from datetime import datetime 
from unidecode import unidecode
from django.conf import settings
import os
import csv
import re
from phenotyping.models import Ontology
from collections import OrderedDict


def test_value(tab,column,fieldnames, method, trait , scale, variable):
    """ 
        cast the data and put it in the right dict 
        we check the fieldnames to know in which dict it should be store and to change them so there're similar for all ontologies 
        
        :var dict tab : data of this line
        :var int column : column we are extracting the data from
        :var dict fieldnames : all the columns
        :var function fct : function that will cast in the type expected
        :var dict method : we give the dict as an argument so we can update it here 
        :var dict trait : we give the dict as an argument so we can update it here 
        :var dict scale : we give the dict as an argument so we can update it here 
        :var dict variable : we give the dict as an argument so we can update it here 
        
        :var dict l : name of the dict in which we will store it 
        :var string s : name that should be associated with the value in the dict
        
    """
    
    fct = get_function(fieldnames,column) # to know the type of data we should have
    s = fieldnames[column]
    
    #if the fieldname contains 'method' or 'formula', it should be stored in the method dict
    if re.search('method', fieldnames[column], re.IGNORECASE) or re.search('formula', fieldnames[column], re.IGNORECASE):
        l = method
        
    #if the fieldname contains 'trait', 'entity' or 'attribute', it should be stored in the trait dict
    elif re.search('trait', fieldnames[column], re.IGNORECASE) or re.search('entity', fieldnames[column], re.IGNORECASE) or re.search('attribute', fieldnames[column], re.IGNORECASE): 
        l = trait
        s = get_fieldname(column,fieldnames)
        
    #if the fieldname contains 'scale', 'places', 'limit' or 'category', it should be stored in the scale dict
    elif re.search('scale', fieldnames[column], re.IGNORECASE) or re.search('places', fieldnames[column], re.IGNORECASE) or re.search('limit', fieldnames[column], re.IGNORECASE) or re.search('category', fieldnames[column], re.IGNORECASE): 
        l = scale
        
    # else it should be stored in the variable dict
    else : 
        l = variable
        s = get_fieldname(column,fieldnames)
        
    #once we know the correct fieldname and which dict to use, we can add it 
    if(tab[column]!=""):
#         if fct == str :
#             l[s] = tab[column].encode('utf8')
#         else : 
        l[s] = fct(tab[column])
    
    
    
def get_fieldname(column,fieldnames):
    """ 
        test_value_trait checks which column we are reading in order to use the correct fieldname
        we check the fieldname to know what is store in it and we give it the correct fieldname (the one that is usually used) 
        
        this method is use so the main fieldnames are always the same
        
        :var int column : column we are extracting the data from
        :var string s : name that should be associated with the value in the dict
        :var dict fieldnames : all the columns
        
    """ 
    #we will use test_value_trait or test_value_variable to get the correct name for the column 
    s = fieldnames[column]#but if the name is not one that we check we use the one in the file  
     
    if re.search('id', fieldnames[column], re.IGNORECASE):
        if re.search('trait', fieldnames[column], re.IGNORECASE):    #if it contains 'id' and 'trait' it's the trait id 
            s = 'traitDbId'
        elif re.search('variable', fieldnames[column], re.IGNORECASE):        #if it contains 'id' and 'variable' it's the variable id 
            s = 'observationVariableDbId'
        elif re.search('ontology', fieldnames[column], re.IGNORECASE):        #if it contains 'id' and 'ontology' it's the ontology id 
            s = 'ontologyDbId'
        
    #if it contains 'class' and 'trait' it's the trait class 
    elif re.search('class', fieldnames[column], re.IGNORECASE) and re.search('trait', fieldnames[column], re.IGNORECASE):
        s = 'class'
        
    elif re.search('name', fieldnames[column], re.IGNORECASE):
        if re.search('trait', fieldnames[column], re.IGNORECASE) or re.search('variable', fieldnames[column], re.IGNORECASE):    #if it contains 'name' it's the trait name or the variable name
            s = 'name'
        elif re.search('name', fieldnames[column], re.IGNORECASE) and re.search('ontology', fieldnames[column], re.IGNORECASE):        #if it contains 'name' and 'ontology' it's the ontology name 
            s = 'ontologyName'
        
    return s


@api_view(['GET', 'POST'])
def variable_list(request,page=0,page_size=1000):
    """
        variable_list returns the variable list in JSON 
        the variables are from the ontology files in the database 
        and it returns only the variables that should be displayed on this page
        
        :var int page : which page to display
        :var int page_size : number of variables on each page
        :var int nb_total : number of variables 
        :var list data : list of the variables to display
        :var string repertoire : where the ontology files are stored
        :var Ontology obj : used in the for, it's the ontology we are reading 
        :var string f : ontology file for the ontology obj
        :var int j : used in the for to know how many variable were in each file, so we can add it to nb_total
        :var - dialect : used to detect what was used in the csv file as a delimiter
        :var - reader : used to read the files
        :var list fieldnames : all the columns
        :var string ont_name : name of the ontology obj
        :var string ont_id : id of the ontology obj
        :var dict line : used in the for, to store each line of the file
        :var dict variable : contains variable data (method, trait, scale, ontology)
        :var dict method : contains method data
        :var dict trait : contains trait data
        :var dict scale : contains scale data
        :var function fct : function that will cast in the type expected
        :var dict metadata : dict containing the pagination dict
        :var dict pagination : dict containing page, page_size and nb_total
        :var int debut : used to know the first variable that should be displayed on this page
        :var int fin : used to know the last variable that should be displayed on this page
        :var dict result : dict containing data 
        :var dict yourdata : dict containing result and metadata
        
    """
    print("variable_list")
    
    #if page and pageSize were in the url, we update the value of the variables page and pageSize
    try :
        page=request.GET['page']
        page_size=request.GET['pageSize']
    except Exception:
        pass
  
    #we start to read the data
    nb_total = 0
    data = [] 
    
    repertoire = settings.MEDIA_ROOT+'/'
    os.makedirs(repertoire, exist_ok=True)
    
    for obj in Ontology.objects.all() : 
        f = obj.file.name

        j = 0  
        with open(str(repertoire+f),'r', encoding="utf8", errors='ignore') as csvhandler :
            dialect = csv.Sniffer().sniff(csvhandler.read(4096))
            csvhandler.seek(0)

            #we get the fieldnames
            reader = csv.DictReader(csvhandler)
            fieldnames = reader.fieldnames
            fieldnames = str(fieldnames).replace("\"", "")
            fieldnames = str(fieldnames).replace("'", "")
            fieldnames = str(fieldnames).replace(",", ";")
            fieldnames = str(fieldnames)[1:len(str(fieldnames))-1].split(';')
            reader = csv.reader(csvhandler, dialect)
            
            #we get the ontology info
            ont_name = obj.name
            ont_id = obj.ontology_id
            
            for line in reader : #we read each line 
                #we empty the dicts because they are used for each line
                variable = OrderedDict()
                method = OrderedDict()
                trait = OrderedDict()
                scale = OrderedDict()

                for i in range (0,len(line)) : # i is for the column we are reading on this line 
                    try :
                        test_value(line,i,fieldnames,method,trait,scale,variable)#store the data in the right dict
                    except Exception:
                        pass
                        
                #once we have read all the data on this line, we put it all in the variable dict and then add variable to the data dict 
                #then we start again with another line 
                 
                variable["ontologyName"] = ont_name
                variable["ontologyDbId"] = ont_id
                variable["method"] = method    
                variable["trait"] = trait 
                variable["scale"] = scale
                data.append(variable)  
                j+=1    #used to know how many variables were on this file 
                
        nb_total += j #how many variables in total 
       
    #we create the metadata dict          
    metadata = {}
    metadata['datafiles'] = []
    pagination = {}
    pagination['currentPage'] = int(page)
    pagination['pageSize'] = int(page_size)
    pagination['totalCount'] = nb_total
    r = 0
    if int(nb_total)%int(page_size) != 0:
        r = 1
    pagination['totalPages'] = int(int(nb_total)/int(page_size)) + r 
    metadata['pagination'] = pagination
    metadata['status'] = []      
        
    #we return only the data on the page asked by the user    
    debut = int(page)*int(page_size)
    fin = debut + int(page_size)
    data = data[debut:fin]
    result = {}
    result["data"] = data  
    yourdata = {"metadata":metadata, "result" : result}
    return Response(yourdata)



def get_function(fieldnames,i):
    """ 
        returns the function that will cast the data read in the type expected
        
        :var list fieldnames : list of the columns
        :var int i : used in the for of the variable_list method to know which column we are reading 
    
    """
    
    if re.search('date', fieldnames[i], re.IGNORECASE) : 
        return datetime.utcfromtimestamp
    if re.search('limit', fieldnames[i], re.IGNORECASE) or re.search('decimal', fieldnames[i], re.IGNORECASE) :
        return int 
    else :
        return str
