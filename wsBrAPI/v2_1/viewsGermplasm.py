# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Laetitia Courgey, Elise Peluso

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import datetime 
import pytz

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_401_UNAUTHORIZED
)
from rest_framework import status
from django.conf import settings
from wsBrAPI.paginator import BrAPIPagination

from accession.models import Accession
from lot.models import Seedlot
from genealogy_managers.models import Reproduction, Reproduction_method
from accession.serializers import GermplasmBrAPISerializer, PedigreeBrAPISerializer
from lot.serializers import SeedlotBrAPISerializer 
from genealogy_managers.serializers import CrossesBrAPISerializer, BreedingMethodsBrAPISerializer


class Germplasm(APIView):
    """
    View to get the details of specific Germplasm.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """   
    def get(self, request):  
        
        try:
            project_list = request.user.project_set.all()
            allgermplasm = Accession.objects.filter(projects__in = project_list).distinct()
            
            accessionNumber = request.GET.get('accessionNumber')
            commonCropName = request.GET.get('commonCropName')
            externalReferenceId = request.GET.get('externalReferenceId')
            externalReferenceSource = request.GET.get('externalReferenceSource')
            genus = request.GET.get('genus')
            germplasmDbId = request.GET.get('germplasmDbId')
            germplasmName = request.GET.get('germplasmName')
            parentDbId = request.GET.get('parentDbId')
            page = int(request.GET.get('page')) if request.GET.get('page') else 1
            pageSize = int(request.GET.get('pageSize')) if request.GET.get('pageSize') else allgermplasm.count()
            progenyDbId = request.GET.get('progenyDbId')
            species = request.GET.get('species')     
            
            if accessionNumber : 
                allgermplasm = allgermplasm.filter(name = accessionNumber)
            if commonCropName and not commonCropName==settings.COMMONCROPNAME :
                allgermplasm = Accession.objects.none()   
            if externalReferenceId and externalReferenceSource :
                if externalReferenceSource == 'DOI' :
                    allgermplasm = allgermplasm.filter(doi = externalReferenceId)       
            if genus and not genus==settings.GENUS :
                allgermplasm = Accession.objects.none() 
            if germplasmDbId :
                allgermplasm = allgermplasm.filter(name = germplasmDbId)
            if germplasmName :
                allgermplasm = allgermplasm.filter(name = germplasmName)        
            if parentDbId :
                allgermplasm = allgermplasm.filter(relations_as_child__parent__name = parentDbId)
            if progenyDbId :
                allgermplasm = allgermplasm.filter(relations_as_parent__child__name = progenyDbId)
            if species and not species==settings.SPECIES :
                allgermplasm = Accession.objects.none()
                
            if int(allgermplasm.count() % int(pageSize))!= 0 :
                totalPages = int(allgermplasm.count() / int(pageSize))+1
            else :
                totalPages = int(allgermplasm.count() / int(pageSize))    
            
            paginator = BrAPIPagination()
            result = paginator.paginate_queryset(allgermplasm, request)    
            serializer = GermplasmBrAPISerializer(result, many = True)  
            
            response = {'metadata' :{
                            'datafiles' : [],
                            'pagination' : {
                                'currentPage' : page,
                                'pageSize' : pageSize,
                                'totalCount' : allgermplasm.count(),
                                'totalPages' : totalPages
                                },
                            'status' : [{
                                'message' : 'Request accepted, response successful',
                                'messageType': 'INFO'
                                }]
                            },
                        'result' : {'data' : serializer.data}}                       
                             
        except Accession.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)
        
    @staticmethod
    def getCallInfo():
        return {
            'contentTypes' : ['application/json'],
            'dataTypes': ['application/json'],
            'methods' : ['GET'],
            'service': 'germplasm',
            'versions' : ['2.1'],
        }
        
                
class GermplasmDbId(APIView):
    """
    View to get the details of specific Germplasm.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """ 
    def get(self, request, germplasmDbId=None):
        
        try: 
            project_list = request.user.project_set.all()   
            result = Accession.objects.filter(projects__in = project_list).distinct().get(id=germplasmDbId)
            serializer = GermplasmBrAPISerializer(result) 
            
            response = {'metadata' :{
                            'datafiles' : [],
                            'pagination' : {
                                'currentPage' : 1,
                                'pageSize' : 1,
                                'totalCount' : 1,
                                'totalPages' : 1
                                },
                            'status' : [{
                                'message' : 'Request accepted, response successful',
                                'messageType': 'INFO'
                                }]
                            },
                        'result' : serializer.data} 
                      
        except Accession.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)
     
    @staticmethod
    def getCallInfo():
        return {
            'contentTypes' : ['application/json'],
            'dataTypes': ['application/json'],
            'methods' : ['GET'],
            'service': 'germplasm/{germplasmDbId}',
            'versions' : ['2.1'],
        }   
   
        
class BreedingMethods(APIView):
    """
    View to get the list of germplasm breeding methods available in a system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """ 
    def get(self, request):
        
        try:    
            #project_list = request.user.project_set.all()
            #result = Reproduction_method.objects.filter(projects__in = project_list).distinct()
            result = Reproduction_method.objects.all()
            serializer = BreedingMethodsBrAPISerializer(result, many=True)
            
            response = {'metadata' :{
                                'datafiles' : [],
                                'pagination' : {
                                    'currentPage' : 1, 
                                    'pageSize' : 1, 
                                    'totalCount' : 1, 
                                    'totalPages' : 1 
                                    },
                                'status' : [{
                                    'message' : 'Request accepted, response successful',
                                    'messageType': 'INFO'
                                    }]
                                },
                            'result' : {'data' : serializer.data}
                            }
                 
        except Reproduction_method.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)    
        
    @staticmethod
    def getCallInfo():
        return {
            'contentTypes' : ['application/json'],
            'dataTypes': ['application/json'],
            'methods' : ['GET'],
            'service': 'breedingmethods',
            'versions' : ['2.1'],
        }    


class BreedingMethodDbId(APIView):
    """
    View to get the germplasm breeding methods available in a system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """           
    def get(self, request, breedingMethodDbId=None):
        
        try:    
            #project_list = request.user.project_set.all()
            #result = Reproduction_method.objects.filter(projects__in = project_list).distinct().get(id=breedingMethodDbId)
            result = Reproduction_method.objects.all().get(id=breedingMethodDbId)
            serializer = BreedingMethodsBrAPISerializer(result)
            
            response = {'metadata' :{
                                'datafiles' : [],
                                'pagination' : {
                                    'currentPage' : 1, 
                                    'pageSize' : 1, 
                                    'totalCount' : 1, 
                                    'totalPages' : 1 
                                    },
                                'status' : [{
                                    'message' : 'Request accepted, response successful',
                                    'messageType': 'INFO'
                                    }]
                                },
                            'result' : serializer.data}
                 
        except Reproduction_method.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)    
        
    @staticmethod
    def getCallInfo():
        return {
            'contentTypes' : ['application/json'],
            'dataTypes': ['application/json'],
            'methods' : ['GET'],
            'service': 'breedingmethods/{breedingMethodDbId}',
            'versions' : ['2.1'],
        } 
                 
        
class Pedigree(APIView):
    """
    View to get the details of pedigree.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """ 
    def get(self, request):
        
        try:       
            project_list = request.user.project_set.all()
            allpedigree = Accession.objects.filter(projects__in = project_list).distinct()
            
            accessionNumber = request.GET.get('accessionNumber')
            binomialName = request.GET.get('binomialName')
            collection = request.GET.get('collection')
            commonCropName = request.GET.get('commonCropName')
            externalReferenceId = request.GET.get('externalReferenceId')
            externalReferenceSource = request.GET.get('externalReferenceSource')
            familyCode = request.GET.get('familyCode')
            genus = request.GET.get('genus')
            germplasmDbId = request.GET.get('germplasmDbId')
            germplasmName = request.GET.get('germplasmName')
            germplasmPUI = request.GET.get('germplasmPUI')
            includeFullTree = request.GET.get('includeFullTree')
            includeParents = request.GET.get('includeParents')
            includeProgeny = request.GET.get('includeProgeny')
            includeSiblings = request.GET.get('includeSiblings')
            page = int(request.GET.get('page')) if request.GET.get('page') else 1
            pageSize = int(request.GET.get('pageSize')) if request.GET.get('pageSize') else allpedigree.count()
            pedigreeDepth = request.GET.get('pedigreeDepth')
            progenyDepth = request.GET.get('progenyDepth')
            programDbId = request.GET.get('programDbId')
            species = request.GET.get('species')
            synonym = request.GET.get('synonym')
            studyDbId = request.GET.get('studyDbId')
            trialDbId = request.GET.get('trialDbId')
            
            if accessionNumber:
                allpedigree = allpedigree.filter(name = accessionNumber)
            if binomialName: 
                allpedigree = Accession.objects.none()
            if collection: 
                allpedigree = Accession.objects.none()
            if commonCropName and not commonCropName==settings.COMMONCROPNAME:
                allpedigree = Accession.objects.none()
            if externalReferenceId and externalReferenceSource :
                if externalReferenceSource == 'DOI' :
                    allpedigree = allpedigree.filter(doi = externalReferenceId) 
            if familyCode: 
                allpedigree = Accession.objects.none() 
            if genus and not genus==settings.GENUS :
                allpedigree = Accession.objects.none() 
            if germplasmDbId:
                allpedigree = allpedigree.filter(name = germplasmDbId)
            if germplasmName:
                allpedigree = allpedigree.filter(name = germplasmName)
            if germplasmPUI:
                allpedigree = Accession.objects.none() 
            if includeFullTree:
                allpedigree = Accession.objects.none() 
            if includeParents:
                allpedigree = allpedigree.filter(relations_as_child__parent__name = parentDbId)
            if includeProgeny: 
                allpedigree = allpedigree.filter(relations_as_parent__child__name = progenyDbId)
            if includeSiblings:
                allpedigree = Accession.objects.none() 
            if pedigreeDepth: 
                allpedigree = Accession.objects.none() 
            if progenyDepth: 
                allpedigree = Accession.objects.none() 
            if programDbId: 
                allpedigree = Accession.objects.none() 
            if species: 
                allpedigree = Accession.objects.none() 
            if synonym: 
                allpedigree = Accession.objects.none() 
            if studyDbId: 
                allpedigree = Accession.objects.none() 
            if trialDbId: 
                allpedigree = Accession.objects.none() 
            
            if int(allpedigree.count() % int(pageSize))!= 0 :
                totalPages = int(allpedigree.count() / int(pageSize))+1
            else :
                totalPages = int(allpedigree.count() / int(pageSize)) 
            
            paginator = BrAPIPagination()
            result = paginator.paginate_queryset(allpedigree, request)   
            serializer = PedigreeBrAPISerializer(result, many=True) 
            
            response = {'metadata' :{
                            'datafiles' : [],
                            'pagination' : {
                                'currentPage' : page, 
                                'pageSize' : pageSize,
                                'totalCount' : allpedigree.count(), 
                                'totalPages' : totalPages
                                },
                            'status' : [{
                                'message' : 'Request accepted, response successful',
                                'messageType': 'INFO'
                                }]
                            },
                        'result' : {'data' : serializer.data}
                        }
             
        except Accession.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)
        
             
    @staticmethod
    def getCallInfo():
        return {
            'contentTypes' : ['application/json'],
            'dataTypes': ['application/json'],
            'methods' : ['GET'],
            'service': 'pedigree',
            'versions' : ['2.1'],
        }
 
class SeedLots(APIView):
    """
    View to get the details of seedlots.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    def get(self, request):
       
        try:
            #project_list = request.user.project_set.all()
            #allseedlot = Seedlot.objects.filter(projects__in = project_list).distinct() 
            allseedlot = Seedlot.objects.all()
            
            commonCropName = request.GET.get('commonCropName')     
            crossDbId = request.GET.get('crossDbId') 
            crossName = request.GET.get('crossName')
            externalReferenceId = request.GET.get('externalReferenceId')  
            externalReferenceID = request.GET.get('externalReferenceID') 
            externalReferenceSource = request.GET.get('externalReferenceSource')  
            germplasmDbId = request.GET.get('germplasmDbId')  
            germplasmName = request.GET.get('germplasmName')  
            page = int(request.GET.get('page')) if request.GET.get('page') else 1
            pageSize = int(request.GET.get('pageSize')) if request.GET.get('pageSize') else allseedlot.count()     
            programDbId = request.GET.get('programDbId')  
            seedLotDbId = request.GET.get('seedLotDbId')  
              
            if commonCropName and not commonCropName==settings.COMMONCROPNAME:
                allseedlot = Seedlot.objects.none()  
            if crossDbId :
                allseedlot = Seedlot.objects.none()
            if crossName :
                allseedlot = Seedlot.objects.none()
            if externalReferenceId :
                allseedlot = Seedlot.objects.none()
            if externalReferenceID :
                allseedlot = Seedlot.objects.none()
            if externalReferenceSource :
                allseedlot = Seedlot.objects.none()
            if germplasmDbId :
                allseedlot = Seedlot.objects.filter(accession = germplasmDbId)
            if germplasmName :
                allseedlot = Seedlot.objects.filter(accession = germplasmName)
            if programDbId :
                allseedlot = Seedlot.objects.filter(projects = programDbId)
            if seedLotDbId :
                allseedlot = Seedlot.objects.filter(name = seedLotDbId)
            
            if int(allseedlot.count() % int(pageSize))!= 0 :
                totalPages = int(allseedlot.count() / int(pageSize))+1
            else :
                totalPages = int(allseedlot.count() / int(pageSize))
              
            paginator = BrAPIPagination()
            result = paginator.paginate_queryset(allseedlot, request)
            serializer = SeedlotBrAPISerializer(result, many=True)
                
            response = {'metadata' :{
                    'datafiles' : [],
                    'pagination' : {
                        'currentPage' : page,
                        'pageSize' : pageSize,
                        'totalCount' : allseedlot.count(), 
                        'totalPages' : totalPages
                        },
                    'status' : [{
                        'message' : 'Request accepted, response successful',
                        'messageType': 'INFO'
                        }]
                    },
                'result' : {'data' : serializer.data}
                }
                 
        except Seedlot.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)     
        
    @staticmethod
    def getCallInfo():
        return {
            'contentTypes' : ['application/json'],
            'dataTypes': ['application/json'],
            'methods' : ['GET'],
            'service': 'seedlots',
            'versions' : ['2.1'],
        }     


class SeedLotDbId(APIView):
    """
    View to get the details of a seedlot.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    def get(self, request, seedLotDbId=None):
       
        try:    
            #project_list = request.user.project_set.all()
            #result = Seedlot.objects.filter(projects__in = project_list).distinct().get(id=seedLotDbId)
            result = Seedlot.objects.all().get(id=seedLotDbId)
            serializer = SeedlotBrAPISerializer(result)
            
            response = {'metadata' :{
                                'datafiles' : [],
                                'pagination' : {
                                    'currentPage' : 1, 
                                    'pageSize' : 1, 
                                    'totalCount' : 1, 
                                    'totalPages' : 1 
                                    },
                                'status' : [{
                                    'message' : 'Request accepted, response successful',
                                    'messageType': 'INFO'
                                    }]
                                },
                            'result' : serializer.data}
                 
        except Seedlot.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)    
        
    @staticmethod
    def getCallInfo():
        return {
            'contentTypes' : ['application/json'],
            'dataTypes': ['application/json'],
            'methods' : ['GET'],
            'service': 'seedlots/{seedLotDbId}',
            'versions' : ['2.1'],
        } 
                
        
class Crosses(APIView):      
    """
    View to get the details of Crosses.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    def get(self, request):
        
        try:
            #project_list = request.user.project_set.all()
            #allcross = Reproduction.objects.filter(projects__in = project_list).distinct() 
            allcross = Reproduction.objects.all()
            
            commonCropName = request.GET.get('commonCropName')
            crossDbId = request.GET.get('crossDbId')
            crossName = request.GET.get('crossName')
            crossingProjectDbId = request.GET.get('crossingProjectDbId') 
            crossingProjectName = request.GET.get('crossingProjectName') 
            externalReferenceID = request.GET.get('externalReferenceID')
            externalReferenceId = request.GET.get('externalReferenceId')
            externalReferenceSource = request.GET.get('externalReferenceSource')
            page = int(request.GET.get('page')) if request.GET.get('page') else 1
            pageSize = int(request.GET.get('pageSize')) if request.GET.get('pageSize') else allcross.count()  
            programDbId = request.GET.get('programDbId')
            
            if commonCropName and not commonCropName==settings.COMMONCROPNAME:
                allcross = Reproduction.objects.none() 
            if crossDbId :
                allcross = Reproduction.objects.filter(name = crossDbId)
            if crossName :
                allcross = Reproduction.objects.filter(name = crossName)  
            if crossingProjectDbId :
                allcross = Reproduction.objects.none()    
            if crossingProjectName :
                allcross = Reproduction.objects.none() 
            if externalReferenceID :
                allcross = Reproduction.objects.none()
            if externalReferenceId :
                allcross = Reproduction.objects.none()
            if externalReferenceSource :
                allcross = Reproduction.objects.none()    
            if programDbId :
                allcross = Reproduction.objects.none()    
             
            if int(allcross.count() % int(pageSize))!= 0 :
                totalPages = int(allcross.count() / int(pageSize))+1
            else :
                totalPages = int(allcross.count() / int(pageSize))
                
            paginator = BrAPIPagination()
            result = paginator.paginate_queryset(allcross, request)
            serializer = CrossesBrAPISerializer(result, many=True)
                
            response = {'metadata' :{
                    'datafiles' : [],
                    'pagination' : {
                        'currentPage' : page,
                        'pageSize' : pageSize,
                        'totalCount' : allcross.count(), 
                        'totalPages' : totalPages 
                        },
                    'status' : [{
                        'message' : 'Request accepted, response successful',
                        'messageType': 'INFO'
                        }]
                    },
                'result' : {'data' : serializer.data}
                }
                 
        except Reproduction.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)   

    @staticmethod
    def getCallInfo():
        return {
            'contentTypes' : ['application/json'],
            'dataTypes': ['application/json'],
            'methods' : ['GET'],
            'service': 'crosses',
            'versions' : ['2.1'],
        }                     