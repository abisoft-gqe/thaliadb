# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Laetitia Courgey, Elise Peluso

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import datetime 
import pytz

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication
from rest_framework.status import (
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_401_UNAUTHORIZED
)
from rest_framework import status
from django.conf import settings
from wsBrAPI.paginator import BrAPIPagination

from accession.models import Accession
from phenotyping.models import Environment
from wsBrAPI.v2_1.viewsGermplasm import Germplasm, GermplasmDbId, BreedingMethods, BreedingMethodDbId, Pedigree, Crosses, SeedLots, SeedLotDbId
from wsBrAPI.v2_1.viewsPhenotyping import Methods, Traits, Ontologies
from wsBrAPI.v2_1.viewsGenotyping import Maps, MarkerPositions, Samples
from team.models import Project, Person
from team.serializers import PeopleBrAPISerializer, ProgramsBrAPISerializer
from phenotyping.serializers import StudiesBrAPISerializer
                
                
class ServerInfo(APIView):
    """
    View to get all of the implemented methods.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """   
    authentication_classes = []
    permission_classes = [] 
    
    def get(self, request):
        try: 
            response = {'metadata' :{
                            'datafiles' : [],
                            'pagination' : {
                                'currentPage' : 1,
                                'pageSize' : 12,
                                'totalCount' : 1,
                                'totalPages' : 1
                                },
                            'status' : [{
                                'message' : 'Request accepted, response successful',
                                'messageType': 'INFO'
                                }]
                            },
                        'result' : {
                            'calls' : 
                                [
                                    #BreedingMethodDbId.getCallInfo(),
                                    #BreedingMethods.getCallInfo(),
                                    #Crosses.getCallInfo(),
                                    #Germplasm.getCallInfo(),
                                    #GermplasmDbId.getCallInfo(),
                                    Maps.getCallInfo(),
                                    MarkerPositions.getCallInfo(),
                                    Methods.getCallInfo(),
                                    Ontologies.getCallInfo(),
                                    #Pedigree.getCallInfo(),
                                    #People.getCallInfo(),
                                    #PersonDbId.getCallInfo(),
                                    #ProgramDbId.getCallInfo(),
                                    #Programs.getCallInfo(),
                                    Samples.getCallInfo(),
                                    #SeedLotDbId.getCallInfo(),
                                    #SeedLots.getCallInfo(),
                                    #Studies.getCallInfo(),
                                    Traits.getCallInfo(),
                                ],
                            'contactEmail' : 'contact@institute.org',
                            'documentationURL' : 'institute.org/server',
                            'location' : 'France',
                            'organizationName' : 'The institute',
                            'organizationURL' : 'institute.org/home',
                            'serverDescription' : 'The BrAPI Test Server\nWeb Server - Apache Tomcat 7.0.32\nDatabase - Postgres 10\nSupported BrAPI Version - V2.1',
                            'serverName' : 'The BrAPI Test Server'
                            },
                        }
                                     
        except Accession.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)

          
class People (APIView):
    """
    View to get the details of people.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    def get(self, request): 
        
        try: 
            allpeople = Person.objects.all()
            
            commonCropName = request.GET.get('commonCropName')
            externalReferenceId = request.GET.get('externalReferenceId')
            externalReferenceID = request.GET.get('externalReferenceID') 
            externalReferenceSource = request.GET.get('externalReferenceSource')
            firstName = request.GET.get('firstName')
            lastName = request.GET.get('lastName')
            page = int(request.GET.get('page')) if request.GET.get('page') else 1
            pageSize = int(request.GET.get('pageSize')) if request.GET.get('pageSize') else allpeople.count()
            personDbId = request.GET.get('personDbId')
            programDbId = request.GET.get('programDbId')
            userID = request.GET.get('userID')
            
            if commonCropName and not commonCropName==settings.COMMONCROPNAME :
                allpeople = Person.objects.none() 
            if externalReferenceId :
                allpeople = Person.objects.none() 
            if externalReferenceID :
                allpeople = Person.objects.none()    
            if externalReferenceSource :
                allpeople = Person.objects.none() 
            if firstName :
                allpeople = Person.objects.filter(first_name = firstName)
            if lastName :
                allpeople = Person.objects.filter(last_name = lastName)
            if personDbId :
                allpeople = Person.objects.filter(id = personDbId)
            if programDbId :
                allpeople = Person.objects.none()    
            if userID :
                allpeople = Person.objects.filter(user__id = userID)    
             
            if int(allpeople.count() % int(pageSize))!= 0 :
                totalPages = int(allpeople.count() / int(pageSize))+1
            else :
                totalPages = int(allpeople.count() / int(pageSize))  
                        
            paginator = BrAPIPagination()
            result = paginator.paginate_queryset(allpeople, request)
            serializer = PeopleBrAPISerializer(result, many=True) 
            
            response = {'metadata' :{
                'datafiles' : [],
                'pagination' : {
                    'currentPage' : page,
                    'pageSize' : pageSize,
                    'totalCount' : allpeople.count(), 
                    'totalPages' : totalPages
                    },
                'status' : [{
                    'message' : 'Request accepted, response successful',
                    'messageType': 'INFO'
                    }]
                },
            'result' : {'data' : serializer.data}
            }
            
        except Person.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)  
        
    @staticmethod
    def getCallInfo():
        return {
            'contentTypes' : ['application/json'],
            'dataTypes': ['application/json'],
            'methods' : ['GET'],
            'service': 'people',
            'versions' : ['2.1'],
        }


class PersonDbId (APIView):
    """
    View to get the details of a person.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    def get(self, request, personDbId=None):
        
        try: 
            result = Person.objects.all().get(id=personDbId)
            serializer = PeopleBrAPISerializer(result)
            
            response = {'metadata' :{
                            'datafiles' : [],
                            'pagination' : {
                                'currentPage' : 1,
                                'pageSize' : 1,
                                'totalCount' : 1,
                                'totalPages' : 1
                                },
                            'status' : [{
                                'message' : 'Request accepted, response successful',
                                'messageType': 'INFO'
                                }]
                            },
                        'result' : serializer.data}
            
        except Person.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)  
        
    @staticmethod
    def getCallInfo():
        return {
            'contentTypes' : ['application/json'],
            'dataTypes': ['application/json'],
            'methods' : ['GET'],
            'service': 'people/{personDbId}',
            'versions' : ['2.1'],
        }                  
            
            
class Programs (APIView):
    """
    View to get the details of projects

    * Requires token authentication.
    * Only admin users are able to access this view.
    """   
    def get(self, request):
        
        try: 
            allprogram = Project.objects.all()
            
            abbreviation = request.GET.get('abbreviation')
            commonCropName = request.GET.get('commonCropName') 
            externalReferenceId = request.GET.get('externalReferenceId') 
            externalReferenceID = request.GET.get('externalReferenceID') 
            externalReferenceSource = request.GET.get('externalReferenceSource') 
            programDbId = request.GET.get('programDbId') 
            programName = request.GET.get('programName') 
            programType = request.GET.get('programType') 
            page = int(request.GET.get('page')) if request.GET.get('page') else 1
            pageSize = int(request.GET.get('pageSize')) if request.GET.get('pageSize') else allprogram.count()
            
            if abbreviation :
                allprogram = Project.objects.none()
            if commonCropName and not commonCropName==settings.COMMONCROPNAME :
                allprogram = Project.objects.none() 
            if externalReferenceId :
                allprogram = Project.objects.none() 
            if externalReferenceID :
                allprogram = Project.objects.none()    
            if externalReferenceSource :
                allprogram = Project.objects.none()   
            if programDbId :
                allprogram = Project.objects.filter(id = programDbId) 
            if programName :
                allprogram = Project.objects.filter(name = programName)  
            if programType :
                allprogram = Project.objects.none()
            
            if int(allprogram.count() % int(pageSize))!= 0 :
                totalPages = int(allprogram.count() / int(pageSize))+1
            else :
                totalPages = int(allprogram.count() / int(pageSize)) 
            
            paginator = BrAPIPagination()
            result = paginator.paginate_queryset(allprogram, request)
            serializer = ProgramsBrAPISerializer(result, many=True) 
            
            response = {'metadata' :{
                'datafiles' : [],
                'pagination' : {
                    'currentPage' : page,
                    'pageSize' : pageSize,
                    'totalCount' : allprogram.count(), 
                    'totalPages' : totalPages
                    },
                'status' : [{
                    'message' : 'Request accepted, response successful',
                    'messageType': 'INFO'
                    }]
                },
            'result' : {'data' : serializer.data}
            }
            
        except Project.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)  
  
    @staticmethod
    def getCallInfo():
        return {
            'contentTypes' : ['application/json'],
            'dataTypes': ['application/json'],
            'methods' : ['GET'],
            'service': 'programs',
            'versions' : ['2.1'],
        } 
   
        
class ProgramDbId (APIView):
    """
    View to get the details of a project.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    def get(self, request, programDbId=None):
       
        try: 
            result = Project.objects.all().get(id=programDbId)
            serializer = ProgramsBrAPISerializer(result)
            
            response = {'metadata' :{
                            'datafiles' : [],
                            'pagination' : {
                                'currentPage' : 1,
                                'pageSize' : 1,
                                'totalCount' : 1,
                                'totalPages' : 1
                                },
                            'status' : [{
                                'message' : 'Request accepted, response successful',
                                'messageType': 'INFO'
                                }]
                            },
                        'result' : serializer.data}
            
        except Project.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)  
        
    @staticmethod
    def getCallInfo():
        return {
            'contentTypes' : ['application/json'],
            'dataTypes': ['application/json'],
            'methods' : ['GET'],
            'service': 'programs/{programDbId}',
            'versions' : ['2.1'],
        } 
        
class Studies (APIView):
    """
    View to get the details of studies.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    def get(self, request):
       
        try: 
            project_list = request.user.project_set.all()
            allstudies = Environment.objects.filter(projects__in = project_list).distinct()
            
            active = request.GET.get('active')
            commonCropName = request.GET.get('commonCropName')
            externalReferenceId = request.GET.get('externalReferenceId')
            externalReferenceSource = request.GET.get('externalReferenceSource')
            germplasmDbId = request.GET.get('germplasmDbId')
            locationDbId = request.GET.get('locationDbId')
            observationVariableDbId = request.GET.get('observationVariableDbId')
            page = int(request.GET.get('page')) if request.GET.get('page') else 1
            pageSize = int(request.GET.get('pageSize')) if request.GET.get('pageSize') else allstudies.count()
            programDbId = request.GET.get('programDbId')
            seasonDbId = request.GET.get('seasonDbId')
            sortBy = request.GET.get('sortBy')
            sortOrder = request.GET.get('sortOrder')
            studyCore = request.GET.get('studyCore')
            studyDbId = request.GET.get('studyDbId')
            studyName = request.GET.get('studyName')
            studyPUI = request.GET.get('studyPUI')
            studyType = request.GET.get('studyType')
            trialDbId = request.GET.get('trialDbId')
            
            if active==False:
                allstudies = Environment.objects.none()
            if commonCropName and not commonCropName==settings.COMMONCROPNAME :
                allstudies = Environment.objects.none() 
            if externalReferenceId:
                allstudies = Environment.objects.none()    
            if externalReferenceSource :
                allstudies = Environment.objects.none()   
            if germplasmDbId:
                allstudies = Environment.objects.none()
            if locationDbId:
                allstudies = Environment.objects.none()
            if observationVariableDbId :
                allstudies = Environment.objects.none()
            if programDbId :
                allstudies = Environment.objects.none()
            if seasonDbId :
                allstudies = Environment.objects.none()
            if sortBy :
                allstudies = Environment.objects.none()
            if sortOrder :
                allstudies = Environment.objects.none()
            if studyCore :
                allstudies = Environment.objects.none()
            if studyDbId :
                allstudies = Environment.objects.none()
            if studyName :
                allstudies = Environment.objects.none()
            if studyPUI :
                allstudies = Environment.objects.none()
            if studyType :
                allstudies = Environment.objects.none()
            if trialDbId :
                allstudies = Environment.objects.none()
            
            if int(allstudies.count() % int(pageSize))!= 0 :
                totalPages = int(allstudies.count() / int(pageSize))+1
            else :
                totalPages = int(allstudies.count() / int(pageSize)) 
            
            paginator = BrAPIPagination()
            result = paginator.paginate_queryset(allstudies, request)
            serializer = StudiesBrAPISerializer(result, many=True)
            
            response = {'metadata' :{
                'datafiles' : [],
                'pagination' : {
                    'currentPage' : page,
                    'pageSize' : pageSize,
                    'totalCount' : allstudies.count(), 
                    'totalPages' : totalPages
                    },
                'status' : [{
                    'message' : 'Request accepted, response successful',
                    'messageType': 'INFO'
                    }]
                },
            'result' : {'data' : serializer.data}
            }
            
        except Environment.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)  
        
    @staticmethod
    def getCallInfo():
        return {
            'contentTypes' : ['application/json'],
            'dataTypes': ['application/json'],
            'methods' : ['GET'],
            'service': 'studies',
            'versions' : ['2.1'],
        }  