
# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Laetitia Courgey, Elise Peluso

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.urls import path
from wsBrAPI.v2_1 import viewsGermplasm, viewsCore, viewsPhenotyping, viewsGenotyping

urlpatterns = [
    #views Germplasm
    path('germplasm', viewsGermplasm.Germplasm.as_view(), name='brapiv2.1_germplasm'),
    path('germplasm/<str:germplasmDbId>', viewsGermplasm.GermplasmDbId.as_view(), name='brapiv2.1_germplasmDbId'),
    path('breedingmethods', viewsGermplasm.BreedingMethods.as_view(), name='brapiv2.1_breedingMethods'),
    path('breedingmethods/<str:breedingMethodDbId>', viewsGermplasm.BreedingMethodDbId.as_view(), name='brapiv2.1_breedingMethodDbId'),
    path('pedigree', viewsGermplasm.Pedigree.as_view(), name='brapiv2.1_pedigree'),
    path('seedlots', viewsGermplasm.SeedLots.as_view(), name='brapiv2.1_seedLots'),
    path('seedlots/<str:seedLotDbId>', viewsGermplasm.SeedLotDbId.as_view(), name='brapiv2.1_seedLotDbId'),
    path('crosses', viewsGermplasm.Crosses.as_view(), name='brapiv2.1_crosses'),
    
    #views Core
    path('serverinfo', viewsCore.ServerInfo.as_view(), name='brapiv2.1_serverInfo'),
    path('calls', viewsCore.ServerInfo.as_view(), name='brapiv2.1_serverInfo'),  
    path('people', viewsCore.People.as_view(), name='brapiv2.1_people'),  
    path('people/<str:personDbId>', viewsCore.PersonDbId.as_view(), name='brapiv2.1_personDbId'),   
    path('programs', viewsCore.Programs.as_view(), name='brapiv2.1_programs'), 
    path('programs/<str:programDbId>', viewsCore.ProgramDbId.as_view(), name='brapiv2.1_programDbId'),
    path('studies', viewsCore.Studies.as_view(), name='brapiv2.1_studies'),
    
    #views Phenotyping
    path('methods', viewsPhenotyping.Methods.as_view(), name='brapiv2.1_methods'),
    path('ontologies', viewsPhenotyping.Ontologies.as_view(), name='brapiv2.1_ontologies'),
    path('traits', viewsPhenotyping.Traits.as_view(), name='brapiv2.1_traits'),  

    #views Genotyping
    path('maps', viewsGenotyping.Maps.as_view(), name='brapiv2.1_maps'),
    path('markerpositions', viewsGenotyping.MarkerPositions.as_view(), name='brapiv2.1_markerpositions'),
    path('samples', viewsGenotyping.Samples.as_view(), name='brapiv2.1_samples'),
]