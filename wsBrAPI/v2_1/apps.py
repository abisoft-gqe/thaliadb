from django.apps import AppConfig


class V21Config(AppConfig):
    name = 'wsBrAPI.v2_1'
