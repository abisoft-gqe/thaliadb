# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Laetitia Courgey, Elise Peluso

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import datetime 
import pytz

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_401_UNAUTHORIZED
)
from rest_framework import status
from django.conf import settings
from wsBrAPI.paginator import BrAPIPagination

from genotyping.models import GenomeVersion, LocusPosition, Sample
from genotyping.serializers import MapsBrAPISerializer, MarkerPositionsBrAPISerializer, SamplesBrAPISerializer

class Maps(APIView):
    """
    View to get the details of genome maps.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """   
    def get(self, request):  
        
        try:
            allmaps = GenomeVersion.objects.all()
            
            mapDbId = request.GET.get('mapDbId')
            mapPUI = request.GET.get('mapPUI')
            scientificName = request.GET.get('scientificName')
            type = request.GET.get('type')
            commonCropName = request.GET.get('commonCropName')
            programDbId = request.GET.get('programDbId')
            trialDbId = request.GET.get('trialDbId')
            studyDbId = request.GET.get('studyDbId')
            page = int(request.GET.get('page')) if request.GET.get('page') else 1
            pageSize = int(request.GET.get('pageSize')) if request.GET.get('pageSize') else allmaps.count()
            
            if mapDbId :
                allmaps = allmaps.filter(id = mapDbId) 
            if mapPUI :
                allmaps = GenomeVersion.objects.none()
            if scientificName :
                allmaps = GenomeVersion.objects.none()
            if type :
                allmaps = GenomeVersion.objects.none()
            if commonCropName and not commonCropName==settings.COMMONCROPNAME :
                allmaps = GenomeVersion.objects.none()
            if programDbId :
                allmaps = GenomeVersion.objects.none()
            if trialDbId :
                allmaps = GenomeVersion.objects.none()
            if studyDbId :
                allmaps = GenomeVersion.objects.none()
            
            # if int(allmaps.count() % int(pageSize))!= 0 :
            #     totalPages = int(allmaps.count() / int(pageSize))+1
            # else :
            #     totalPages = int(allmaps.count() / int(pageSize))    
            
            paginator = BrAPIPagination()
            result = paginator.paginate_queryset(allmaps, request)    
            serializer = MapsBrAPISerializer(result, many = True)  
            
            response = {'metadata' :{
                            'datafiles' : [],
                            'pagination' : {
                                'currentPage' : page,
                                'pageSize' : pageSize,
                                'totalCount' : allmaps.count(),
                                'totalPages' : 1 #totalPages
                                },
                            'status' : [{
                                'message' : 'Request accepted, response successful',
                                'messageType': 'INFO'
                                }]
                            },
                        'result' : {'data' : serializer.data}}                       
                             
        except GenomeVersion.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)
        
    @staticmethod
    def getCallInfo():
        return {
            'contentTypes' : ['application/json'],
            'dataTypes': ['application/json'],
            'methods' : ['GET'],
            'service': 'maps',
            'versions' : ['2.1'],
        }
        

class MarkerPositions(APIView):
    """
    View to get the details of marker positions of genome maps.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """   
    def get(self, request):  
        
        try:
            allmarkerposition = LocusPosition.objects.all()
            
            mapDbId = request.GET.get('mapDbId')
            linkageGroupName = request.GET.get('linkageGroupName')
            variantDbId = request.GET.get('variantDbId')
            minPosition = request.GET.get('minPosition')
            maxPosition = request.GET.get('maxPosition')
            page = int(request.GET.get('page')) if request.GET.get('page') else 1
            pageSize = int(request.GET.get('pageSize')) if request.GET.get('pageSize') else allmarkerposition.count()
            
            if mapDbId :
                allmarkerposition = allmarkerposition.filter(genome_version__id = mapDbId) 
            if linkageGroupName :
                allmarkerposition = LocusPosition.objects.none()
            if variantDbId :
                allmarkerposition = allmarkerposition.filter(locus__id = variantDbId) 
            if minPosition :
                allmarkerposition = LocusPosition.objects.none()
            if maxPosition :
                allmarkerposition = LocusPosition.objects.none()
            
            # if int(int(allmarkerposition.count()) % int(pageSize))!= 0 :
            #     totalPages = int(allmarkerposition.count() / int(pageSize))+1
            # else :
            #     totalPages = int(allmarkerposition.count() / int(pageSize))    
            
            paginator = BrAPIPagination()
            result = paginator.paginate_queryset(allmarkerposition, request)    
            serializer = MarkerPositionsBrAPISerializer(result, many = True)  
            
            response = {'metadata' :{
                            'datafiles' : [],
                            'pagination' : {
                                'currentPage' : page,
                                'pageSize' : pageSize,
                                'totalCount' : allmarkerposition.count(),
                                'totalPages' : 1 #totalPages
                                },
                            'status' : [{
                                'message' : 'Request accepted, response successful',
                                'messageType': 'INFO'
                                }]
                            },
                        'result' : {'data' : serializer.data}}                       
                             
        except LocusPosition.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)
        
    @staticmethod
    def getCallInfo():
        return {
            'contentTypes' : ['application/json'],
            'dataTypes': ['application/json'],
            'methods' : ['GET'],
            'service': 'markerpositions',
            'versions' : ['2.1'],
        }
        
class Samples(APIView):
    """
    View to get the details of sample.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """   
    def get(self, request):  
        
        try:
            #project_list = request.user.project_set.all()
            allsample = Sample.objects.all()
            #allsample = Sample.objects.filter(projects__in = project_list).distinct()
            
            sampleDbId = request.GET.get('sampleDbId')
            sampleName = request.GET.get('sampleName')
            sampleGroupDbId = request.GET.get('')
            observationUnitDbId = request.GET.get('')
            plateDbId = request.GET.get('')
            plateName = request.GET.get('')
            commonCropName = request.GET.get('')
            programDbId = request.GET.get('')
            trialDbId = request.GET.get('')
            studyDbId = request.GET.get('')
            germplasmDbId = request.GET.get('')
            externalReferenceID = request.GET.get('')
            externalReferenceId = request.GET.get('')
            externalReferenceSource = request.GET.get('')
            page = int(request.GET.get('page')) if request.GET.get('page') else 1
            pageSize = int(request.GET.get('pageSize')) if request.GET.get('pageSize') else allsample.count()
            
            if sampleDbId :
                allsample = allsample.filter(id = sampleDbId)
            if sampleName :
                allsample = allsample.filter(name = sampleName)
            if sampleGroupDbId :
                allsample = Sample.objects.none()
            if observationUnitDbId :
                allsample = Sample.objects.none()
            if plateDbId :
                allsample = Sample.objects.none()
            if plateName :
                allsample = Sample.objects.none()
            if commonCropName and not commonCropName==settings.COMMONCROPNAME :
                allsample = Sample.objects.none()
            if programDbId :
                allsample = Sample.objects.none()
            if trialDbId :
                allsample = Sample.objects.none()
            if studyDbId :
                allsample = Sample.objects.none()
            if germplasmDbId : 
                allsample = allsample.filter(seedlot__accession__id = germplasmDbId)
            if externalReferenceId :
                allsample = Sample.objects.none()
            if externalReferenceID :
                allsample = Sample.objects.none()
            if externalReferenceSource :
                allsample = Sample.objects.none()
            
            # if int(int(allsample.count()) % int(pageSize))!= 0 :
            #     totalPages = int(allsample.count() / int(pageSize))+1
            # else :
            #     totalPages = int(allsample.count() / int(pageSize))    
            
            paginator = BrAPIPagination()
            result = paginator.paginate_queryset(allsample, request)    
            serializer = SamplesBrAPISerializer(result, many = True)  
            
            response = {'metadata' :{
                            'datafiles' : [],
                            'pagination' : {
                                'currentPage' : page,
                                'pageSize' : pageSize,
                                'totalCount' : allsample.count(),
                                'totalPages' : 1 #totalPages
                                },
                            'status' : [{
                                'message' : 'Request accepted, response successful',
                                'messageType': 'INFO'
                                }]
                            },
                        'result' : {'data' : serializer.data}}                       
                             
        except Sample.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)
        
    @staticmethod
    def getCallInfo():
        return {
            'contentTypes' : ['application/json'],
            'dataTypes': ['application/json'],
            'methods' : ['GET'],
            'service': 'samples',
            'versions' : ['2.1'],
        }