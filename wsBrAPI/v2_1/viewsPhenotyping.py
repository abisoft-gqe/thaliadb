# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Laetitia Courgey, Elise Peluso

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import datetime 
import pytz

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_401_UNAUTHORIZED
)
from rest_framework import status
from django.conf import settings
from wsBrAPI.paginator import BrAPIPagination

from team.models import Method
from team.serializers import MethodsBrAPISerializer
from phenotyping.models import Trait, Ontology
from phenotyping.serializers import TraitsBrAPISerializer, OntologiesBrAPISerializer


class Methods(APIView):
    """
    View to get the details of methods.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """   
    def get(self, request):  
        
        try:
            allmethods = Method.objects.all().distinct()
            
            commonCropName = request.GET.get('commonCropName')
            externalReferenceId = request.GET.get('externalReferenceId')
            externalReferenceID = request.GET.get('externalReferenceID')
            externalReferenceSource = request.GET.get('externalReferenceSource')
            methodDbId = request.GET.get('methodDbId')
            observationVariableDbId = request.GET.get('observationVariableDbId')
            ontologyDbId = request.GET.get('ontologyDbId')
            page = int(request.GET.get('page')) if request.GET.get('page') else 1
            pageSize = int(request.GET.get('pageSize')) if request.GET.get('pageSize') else allmethods.count()
            programDbId = request.GET.get('programDbId') 
            
            if commonCropName and not commonCropName==settings.COMMONCROPNAME :
                allmethods = Method.objects.none() 
            if externalReferenceId :
                allmethods = Method.objects.none() 
            if externalReferenceID :
                allmethods = Method.objects.none() 
            if externalReferenceSource :
                allmethods = Method.objects.none() 
            if methodDbId : 
                allmethods = allmethods.filter(id = methodDbId)
            if observationVariableDbId :
                allmethods = Method.objects.none() 
            if ontologyDbId :
                allmethods = Method.objects.none() 
            if programDbId :
                allmethods = Method.objects.none()   
            
            if int(allmethods.count() % int(pageSize))!= 0 :
                totalPages = int(allmethods.count() / int(pageSize))+1
            else :
                totalPages = int(allmethods.count() / int(pageSize))    
            
            paginator = BrAPIPagination()
            result = paginator.paginate_queryset(allmethods, request)    
            serializer = MethodsBrAPISerializer(result, many = True)  
            
            response = {'metadata' :{
                            'datafiles' : [],
                            'pagination' : {
                                'currentPage' : page,
                                'pageSize' : pageSize,
                                'totalCount' : allmethods.count(),
                                'totalPages' : totalPages
                                },
                            'status' : [{
                                'message' : 'Request accepted, response successful',
                                'messageType': 'INFO'
                                }]
                            },
                        'result' : {'data' : serializer.data}}                       
                             
        except Method.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)
        
    @staticmethod
    def getCallInfo():
        return {
            'contentTypes' : ['application/json'],
            'dataTypes': ['application/json'],
            'methods' : ['GET'],
            'service': 'methods',
            'versions' : ['2.1'],
        }
        
class Traits(APIView):
    """
    View to get the details of traits.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """   
    def get(self, request):  
        
        try:
            project_list = request.user.project_set.all()
            alltrait = Trait.objects.filter(projects__in = project_list).distinct()
            
            commonCropName = request.GET.get('commonCropName')
            externalReferenceId = request.GET.get('externalReferenceId')
            externalReferenceID = request.GET.get('externalReferenceID')
            externalReferenceSource = request.GET.get('externalReferenceSource')
            observationVariableDbId = request.GET.get('observationVariableDbId')
            ontologyDbId = request.GET.get('ontologyDbId')
            page = int(request.GET.get('page')) if request.GET.get('page') else 1
            pageSize = int(request.GET.get('pageSize')) if request.GET.get('pageSize') else alltrait.count()
            programDbId = request.GET.get('programDbId') 
            traitDbId = request.GET.get('traitDbId')
            
            if commonCropName and not commonCropName==settings.COMMONCROPNAME :
                alltrait = Trait.objects.none() 
            if externalReferenceId :
                alltrait = Trait.objects.none() 
            if externalReferenceID :
                alltrait = Trait.objects.none() 
            if externalReferenceSource :
                alltrait = Trait.objects.none() 
            if observationVariableDbId :
                alltrait = Trait.objects.none() 
            if ontologyDbId :
                alltrait = Trait.objects.none() 
            if programDbId :
                alltrait = Trait.objects.none() 
            if traitDbId : 
                alltrait = alltrait.filter(id = traitDbId)  
            
            if int(alltrait.count() % int(pageSize))!= 0 :
                totalPages = int(alltrait.count() / int(pageSize))+1
            else :
                totalPages = int(alltrait.count() / int(pageSize))    
            
            paginator = BrAPIPagination()
            result = paginator.paginate_queryset(alltrait, request)    
            serializer = TraitsBrAPISerializer(result, many = True)  
            
            response = {'metadata' :{
                            'datafiles' : [],
                            'pagination' : {
                                'currentPage' : page,
                                'pageSize' : pageSize,
                                'totalCount' : alltrait.count(),
                                'totalPages' : totalPages
                                },
                            'status' : [{
                                'message' : 'Request accepted, response successful',
                                'messageType': 'INFO'
                                }]
                            },
                        'result' : {'data' : serializer.data}}                       
                             
        except Trait.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)
        
    @staticmethod
    def getCallInfo():
        return {
            'contentTypes' : ['application/json'],
            'dataTypes': ['application/json'],
            'methods' : ['GET'],
            'service': 'traits',
            'versions' : ['2.1'],
        }

class Ontologies(APIView):
    """
    View to get the details of ontologies.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """   
    def get(self, request):  
        
        try:
            allontology = Ontology.objects.all()
            
            ontologyDbId = request.GET.get('ontologyDbId')
            ontologyName = request.GET.get('ontologyName')
            page = int(request.GET.get('page')) if request.GET.get('page') else 1
            pageSize = int(request.GET.get('pageSize')) if request.GET.get('pageSize') else allontology.count()
            
            if ontologyDbId :
                allontology = allontology.filter(ontology_id = ontologyDbId) 
            if ontologyName :
                allontology = allontology.filter(name = ontologyName) 
            
            if int(allontology.count() % int(pageSize))!= 0 :
                totalPages = int(allontology.count() / int(pageSize))+1
            else :
                totalPages = int(allontology.count() / int(pageSize))    
            
            paginator = BrAPIPagination()
            result = paginator.paginate_queryset(allontology, request)    
            serializer = OntologiesBrAPISerializer(result, many = True)  
            
            response = {'metadata' :{
                            'datafiles' : [],
                            'pagination' : {
                                'currentPage' : page,
                                'pageSize' : pageSize,
                                'totalCount' : allontology.count(),
                                'totalPages' : totalPages
                                },
                            'status' : [{
                                'message' : 'Request accepted, response successful',
                                'messageType': 'INFO'
                                }]
                            },
                        'result' : {'data' : serializer.data}}                       
                             
        except Ontology.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(response, status=status.HTTP_200_OK)
        
    @staticmethod
    def getCallInfo():
        return {
            'contentTypes' : ['application/json'],
            'dataTypes': ['application/json'],
            'methods' : ['GET'],
            'service': 'ontologies',
            'versions' : ['2.1'],
        }