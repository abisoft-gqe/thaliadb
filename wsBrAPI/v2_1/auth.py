import datetime 
import pytz

from rest_framework.authentication import TokenAuthentication
from rest_framework import exceptions
from django.conf import settings

class BrAPIAuthentication(TokenAuthentication):
    keyword = 'Bearer'
    
    def authenticate(self, request):
        user, token = super().authenticate(request)
        expiration_date = (token.created + datetime.timedelta(days=settings.AUTH_TOKEN_LIFE)).replace(tzinfo = pytz.UTC)
        has_expired = datetime.datetime.now().replace(tzinfo = pytz.UTC) > expiration_date
        if has_expired :
            message = "Token has expired, please create a new one"
            raise exceptions.AuthenticationFailed(message)
        else :
            return user, token