import datetime
import hashlib

from django.core.files import File
from django.db import transaction
from django.apps import apps





LIST_FILE = '/home/yannick/thalia-images-data/images_list.txt'
IMAGES_FOLDER = '/home/yannick/thalia-images-data/'
fh = open(LIST_FILE,'r')

images_data = {}

for line in fh.readlines() :
    accession, image = line.split('|')
    accession = accession.strip()
    image = image.strip()
    if accession not in images_data.keys() :
        images_data[image] = []
    images_data[image].append(accession)

with transaction.atomic():
    Image = apps.get_model('images', 'image')
    ImageLink = apps.get_model('images', 'imagelink')
    Accession = apps.get_model('accession', 'accession')
    for image in images_data.keys():
        img = None
        try :
            img = open(IMAGES_FOLDER+image,'rb')
        except :
            print("Impossible d'ouvrir {0} l'image n'existe pas".format(image))
        else :
            image_object = Image()
            content = File(img)
            image_object.image_path.save(image, content)
            
            image_object.image_name = image
            image_object.date = datetime.date.today()
            
            img.seek(0)
            stream = img.read()
            crypt = hashlib.sha1()
            crypt.update(stream) 
            image_object.footprint = crypt.hexdigest()
            image_object.save()
            
            for accession in images_data[image] :
                try :
                    accession_object = Accession.objects.get(name=accession)
                except :
                    print("Impossible de trouver l'accession {0} pour l'image {1}".format(accession, image))
                else :
                    l = ImageLink(image=image_object, entity=accession_object, entity_name=accession_object.name)
                    l.save()
                    
