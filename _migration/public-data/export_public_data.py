import os, django
from bokeh._testing.util.project import ls_files
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
django.setup()

import json
import shutil

from django.apps import apps
from django.core import serializers
from django.core.exceptions import SuspiciousFileOperation
from django.db.models import Q
from django.contrib.contenttypes.models import ContentType

DUMP_FOLDER = "/home/yannick/test/thaliadb/"
FILES_FOLDER = DUMP_FOLDER+"files/"

"""
Fichiers :
Image.image_path
Ontology.file
DataFile.datafile
Method.method_file
"""

i = 0

def write_file(model_class, data):
    global i
    i+=1
    f = open("{0}{1:0=2d}-{2}_{3}.json".format(DUMP_FOLDER, i, model_class._meta.app_label, model_class._meta.model_name), "w")
    f.write(data)
    f.close()

def update_m2mids(public_qs, public_data, field_name):
    ids = [p.id for p in public_qs]
    dictdata = json.loads(public_data)
    i = 0
    for record in dictdata :
        i = dictdata.index(record)
        newids = list(set(dictdata[i]['fields'][field_name]).intersection(ids))
        dictdata[i]['fields'][field_name] = newids
    return json.dumps(dictdata)



"""
=========================================================================

        Importing Model classes
        
=========================================================================
"""
Project = apps.get_model('team','project')
Institution = apps.get_model('team','institution')
Person = apps.get_model('team','person')
Documents = apps.get_model('team','documents')
DataFile = apps.get_model('team','datafile')
Method = apps.get_model('team','method')

Seedlot = apps.get_model('lot','seedlot')
SeedlotType = apps.get_model('lot','seedlottype')
SeedlotTypeAttribute = apps.get_model('lot','seedlottypeattribute')
SeedlotAttributePosition = apps.get_model('lot','seedlotattributeposition')

Accession = apps.get_model('accession','accession')
AccessionType = apps.get_model('accession','accessiontype')
AccessionTypeAttribute = apps.get_model('accession','accessiontypeattribute')
AccessionAttributePosition = apps.get_model('accession','accessionattributeposition')

GenotypingID = apps.get_model('genotyping','genotypingid')
Sample = apps.get_model('genotyping','sample')
Experiment = apps.get_model('genotyping','experiment')
Referential = apps.get_model('genotyping','referential')
GenotypingValue = apps.get_model('genotyping','genotypingvalue')
CodeAllele = apps.get_model('genotyping','codeallele')
LocusToValue = apps.get_model('genotyping','locustovalue')
Locus = apps.get_model('genotyping','locus')
LocusType = apps.get_model('genotyping','locustype')
LocusTypeAttribute = apps.get_model('genotyping','locustypeattribute')
LocusAttributePosition = apps.get_model('genotyping','locusattributeposition')
LocusPosition = apps.get_model('genotyping','locusposition')
GenomeVersion = apps.get_model('genotyping','genomeversion')

Environment = apps.get_model('phenotyping','environment')
SpecificTreatment = apps.get_model('phenotyping','specifictreatment')
Trait = apps.get_model('phenotyping','trait')
Ontology = apps.get_model('phenotyping','ontology')
PhenotypicValue = apps.get_model('phenotyping','phenotypicvalue')

Classification = apps.get_model('classification','classification')
Classe = apps.get_model('classification','classe')
ClasseValue = apps.get_model('classification','classevalue')

Reproduction_method = apps.get_model('genealogy_managers','reproduction_method')
Reproduction = apps.get_model('genealogy_managers','reproduction')
AccessionRelation = apps.get_model('genealogy_accession','accessionrelation')
SeedlotRelation = apps.get_model('genealogy_seedlot','seedlotrelation')

Image = apps.get_model('images','image')
ImageLink = apps.get_model('images','imagelink')

"""
=========================================================================

        Extraction of objects linked to projects and related objects

=========================================================================
"""
public_projects = Project.objects.filter(public=True)
print(public_projects)


""" ============
       Accessions and related objects
    ============== """
public_accessions = Accession.objects.filter(Q(projects__in=public_projects)|
                                             Q(seedlot__projects__in=public_projects)|
                                             Q(seedlot__sample__projects__in=public_projects)|
                                             Q(seedlot__sample__genotypingid__projects__in=public_projects)).distinct()

public_accessiontypes = AccessionType.objects.filter(accession__in = public_accessions).distinct()
public_accessionattributespositions = AccessionAttributePosition.objects.filter(type__in=public_accessiontypes).distinct()
public_accessionattributes = AccessionTypeAttribute.objects.filter(accessionattributeposition__in=public_accessionattributespositions).distinct()


public_accessionrelations = AccessionRelation.objects.filter((Q(child__in=public_accessions)|Q(child__isnull=True)), parent__in=public_accessions).distinct()

print("Accessions : {0}".format(public_accessions.count()))

""" ============
       Seedlot and related objects
    ============== """
public_seedlots = Seedlot.objects.filter(Q(projects__in=public_projects)|
                                             Q(sample__projects__in=public_projects)|
                                             Q(sample__genotypingid__projects__in=public_projects)).distinct()
                                             
public_seedlottypes = SeedlotType.objects.filter(seedlot__in = public_seedlots).distinct()
public_seedlotattributespositions = SeedlotAttributePosition.objects.filter(type__in=public_seedlottypes).distinct()
public_seedlotattributes = SeedlotTypeAttribute.objects.filter(seedlotattributeposition__in=public_seedlotattributespositions).distinct()

public_seedlotrelations = SeedlotRelation.objects.filter((Q(child__in=public_seedlots)|Q(child__isnull=True)), parent__in=public_seedlots).distinct()

print("Seedlots : {0}".format(public_seedlots.count()))

""" ============
       Samples
    ============== """                                         
public_sample = Sample.objects.filter(Q(projects__in=public_projects)|
                                             Q(genotypingid__projects__in=public_projects)).distinct()

print("Samples : {0}".format(public_sample.count()))


""" =============
    Referential and Experiment objects
    ============= """
public_experiment = Experiment.objects.filter(Q(projects__in=public_projects)|Q(genotypingid__projects__in=public_projects)).distinct()

public_referential = Referential.objects.filter(projects__in=public_projects).distinct()


""" ============
       GenotypingID
    ============== """
public_genotypingid = GenotypingID.objects.filter(projects__in=public_projects,
                                                  referential__in=public_referential
                                                  ,experiment__in=public_experiment).distinct()

print("GenotypingID : {0}".format(public_genotypingid.count()))

""" =============
    Genotyping data and descriptors objects
    ============= """

public_genotypingvalues = GenotypingValue.objects.filter(genotyping_id__in=public_genotypingid,referential__in=public_referential).distinct()
public_allelecode = CodeAllele.objects.filter(genotypingvalue__in=public_genotypingvalues).distinct()
public_locustovalue = LocusToValue.objects.filter(genotyping_value__in=public_genotypingvalues).distinct()
public_locus = Locus.objects.filter(locustovalue__in=public_locustovalue).distinct()
print("Locus : {0}".format(public_locus.count()))
public_locustype = LocusType.objects.filter(locus__in=public_locus).distinct()
public_locuspositions = LocusPosition.objects.filter(locus__in=public_locus).distinct()
public_locusattributeposition = LocusAttributePosition.objects.filter(type__in=public_locustype).distinct()
public_locustypeattributes = LocusTypeAttribute.objects.filter(locusattributeposition__in=public_locusattributeposition).distinct()
public_genomeversion = GenomeVersion.objects.filter(locusposition__in=public_locuspositions).distinct()

""" =============
    Phenotyping data and descriptors objects
    ============= """
    
public_environment = Environment.objects.filter(projects__in=public_projects).distinct()
public_specifictreatment = SpecificTreatment.objects.filter(environment__in=public_environment).distinct()
public_traits = Trait.objects.filter(projects__in=public_projects).distinct()
public_ontology = Ontology.objects.all()
if not os.path.exists(FILES_FOLDER+'ontology/'):
    os.mkdir(FILES_FOLDER+'ontology/')
for ontology in public_ontology :
    try :
        shutil.copy(ontology.file.file.name, FILES_FOLDER+ontology.file.name)
    except (FileNotFoundError, SuspiciousFileOperation) as e:
        public_ontology = public_ontology.exclude(ontology_id = ontology.ontology_id)
        print(e)

public_phenovalues = PhenotypicValue.objects.filter(seedlot__in=public_seedlots, trait__in=public_traits, environment__in=public_environment).distinct()

""" =============
    Classifications
    ============= """

public_classification = Classification.objects.filter(projects__in=public_projects).distinct()
public_classes = Classe.objects.filter(classification__in=public_classification).distinct()
public_classevalues = ClasseValue.objects.filter(classe__in=public_classes, seedlot__in=public_seedlots).distinct()

""" =============
    Genealogies
    ============= """

public_reproductions = Reproduction.objects.filter(Q(accessionrelation__in=public_accessionrelations)|Q(seedlotrelation__in=public_seedlotrelations)).distinct()
public_reproductionmethods = Reproduction_method.objects.filter(reproduction__in=public_reproductions).distinct()

""" =============
    Images
    ============= """

public_imagelinks = ImageLink.objects.filter(Q(content_type=ContentType.objects.get_for_model(Accession), object_id__in=[a['id'] for a in public_accessions.values('id')])|
                                             Q(content_type=ContentType.objects.get_for_model(Seedlot), object_id__in=[a['id'] for a in public_seedlots.values('id')])|
                                             Q(content_type=ContentType.objects.get_for_model(Environment), object_id__in=[a['id'] for a in public_environment.values('id')])
                                             ).distinct()
public_images = Image.objects.filter(imagelink__in=public_imagelinks).distinct()
if not os.path.exists(FILES_FOLDER+'images/'):
    os.mkdir(FILES_FOLDER+'images/')
for image in public_images :
    try :
        shutil.copy(image.image_path.file.name, FILES_FOLDER+image.image_path.name)
    except (FileNotFoundError, SuspiciousFileOperation) as e:
        print(e)
        public_images = public_images.exclude(image_path=image.image_path)

""" =============
    Team module other objects
    ============= """

public_methods = Method.objects.filter(Q(phenotypicvalue__in=public_phenovalues)|
                                       Q(classification__in=public_classification)).distinct()
if not os.path.exists(FILES_FOLDER+'methods/'):
    os.mkdir(FILES_FOLDER+'methods/')
for method in public_methods :
    if method.method_file :
        try :
            shutil.copy(method.method_file.file.name, FILES_FOLDER+method.method_file.name)
        except (FileNotFoundError, SuspiciousFileOperation) as e:
            method.method_file = None
            method.save()
            print(e)

public_documents = Documents.objects.filter(Q(phenotypicvalue__in=public_phenovalues),
                                            Q(project__in=public_projects)|
                                            Q(referential__in=public_referential)).distinct()
                                            
public_datafiles = DataFile.objects.filter(document__in=public_documents)
if not os.path.exists(FILES_FOLDER+'documents/'):
    os.mkdir(FILES_FOLDER+'documents/')
for datafile in public_datafiles :
    try :
        shutil.copy(datafile.datafile.file.name, FILES_FOLDER+datafile.datafile.name)
    except (FileNotFoundError, SuspiciousFileOperation) as e:
        public_datafiles = public_datafiles.exclude(id=datafile.id)
        print(e)

public_institutions = Institution.objects.filter(Q(experiment__in=public_experiment)|
                                                 Q(project__in=public_projects)).distinct()

public_person = Person.objects.filter(Q(experiment__in=public_experiment)|
                                      Q(referential__in=public_referential)).distinct()


"""
=========================================================================

        Objects serialization

=========================================================================
"""

print("Starting serialization")

#TODO : serialization can be improved with // tasks

data_accessions = serializers.serialize("json", public_accessions)
data_accessions = update_m2mids(public_projects, data_accessions, 'projects')

data_accessiontypes = serializers.serialize("json", public_accessiontypes)
data_accessiontypeattributes = serializers.serialize("json", public_accessionattributes)
data_accessionattributepositions = serializers.serialize("json", public_accessionattributespositions)
data_accessionrelations = serializers.serialize("json", public_accessionrelations)

data_seedlots = serializers.serialize("json", public_seedlots)
data_seedlots = update_m2mids(public_projects, data_seedlots, 'projects')

data_seedlottypes = serializers.serialize("json", public_seedlottypes)
data_seedlotattribtes = serializers.serialize("json", public_seedlotattributes)
data_seedlotattributepositions = serializers.serialize("json", public_seedlotattributespositions)
data_seedlotrelations = serializers.serialize("json", public_seedlotrelations)

data_sample = serializers.serialize("json", public_sample)
data_sample = update_m2mids(public_projects, data_sample, 'projects')

data_genotypingid = serializers.serialize("json", public_genotypingid)
data_genotypingid = update_m2mids(public_projects, data_genotypingid, 'projects')

data_experiment = serializers.serialize("json", public_experiment)
data_experiment = update_m2mids(public_projects, data_experiment, 'projects')

data_referential = serializers.serialize("json", public_referential)
data_referential = update_m2mids(public_documents, data_referential, 'linkedfiles')
data_referential = update_m2mids(public_projects, data_referential, 'projects')

data_genovalues = serializers.serialize("json", public_genotypingvalues)
data_codeallele = serializers.serialize("json", public_allelecode)
data_locustovalue = serializers.serialize("json", public_locustovalue)
data_locus = serializers.serialize("json", public_locus)
data_locustype = serializers.serialize("json", public_locustype)
data_locusposition = serializers.serialize("json", public_locuspositions)
data_locustypeattributes = serializers.serialize("json", public_locustypeattributes)
data_locusattributepositions = serializers.serialize("json", public_locusattributeposition)
data_genomeversion = serializers.serialize("json", public_genomeversion)

data_environment = serializers.serialize("json", public_environment)
data_environment = update_m2mids(public_projects, data_environment, 'projects')

data_specifictreatment = serializers.serialize("json", public_specifictreatment)
data_traits = serializers.serialize("json", public_traits)
data_traits = update_m2mids(public_projects, data_traits, 'projects')

data_ontology = serializers.serialize("json", public_ontology)
data_phenovalues = serializers.serialize("json", public_phenovalues)

data_classifications = serializers.serialize("json", public_classification)
data_classifications = update_m2mids(public_projects, data_classifications, 'projects')

data_classes = serializers.serialize("json", public_classes)
data_classevalues = serializers.serialize("json", public_classevalues)

data_reproductions = serializers.serialize("json", public_reproductions)
data_reproductionmethods = serializers.serialize("json", public_reproductionmethods)

data_imagelinks = serializers.serialize("json", public_imagelinks, use_natural_foreign_keys=True)
data_images = serializers.serialize("json", public_images)

data_institutions = serializers.serialize("json", public_institutions)
data_person = serializers.serialize("json", public_person, fields=["first_name","last_name"])
#data_person = update_m2mids(public_institutions, data_person, 'institutions')

data_methods = serializers.serialize("json", public_methods)
data_documents = serializers.serialize("json", public_documents)
data_files = serializers.serialize("json", public_datafiles)

data_projects = serializers.serialize("json", public_projects, fields=["name","authors","start_date","end_date","description","institutions","linked_files"])
data_projects = update_m2mids(public_institutions, data_projects, 'institutions')
data_projects = update_m2mids(public_documents, data_projects, 'linked_files')

print("End serialization")
print("Starting writing file")

#TODO : this part can be improved with a map() function to write files

write_file(Institution, data_institutions)
write_file(Project, data_projects)
write_file(Person, data_person)
write_file(Method, data_methods)
write_file(Documents, data_documents)
write_file(DataFile, data_files)



write_file(AccessionType, data_accessiontypes)
write_file(AccessionTypeAttribute, data_accessiontypeattributes)
write_file(AccessionAttributePosition, data_accessionattributepositions)
write_file(Accession, data_accessions)

write_file(SeedlotType, data_seedlottypes)
write_file(SeedlotTypeAttribute, data_seedlotattribtes)
write_file(SeedlotAttributePosition, data_seedlotattributepositions)
write_file(Seedlot, data_seedlots)

write_file(LocusType, data_locustype)
write_file(LocusTypeAttribute, data_locustypeattributes)
write_file(LocusAttributePosition, data_locusattributepositions)
write_file(GenomeVersion, data_genomeversion)
write_file(Experiment, data_experiment)
write_file(Referential, data_referential)
write_file(Sample, data_sample)
write_file(GenotypingID, data_genotypingid)
write_file(Locus, data_locus)
write_file(LocusPosition, data_locusposition)
write_file(CodeAllele, data_codeallele)
write_file(GenotypingValue, data_genovalues)
write_file(LocusToValue, data_locustovalue)

write_file(SpecificTreatment, data_specifictreatment)
write_file(Environment, data_environment)
write_file(Trait, data_traits)
write_file(Ontology, data_ontology)
write_file(PhenotypicValue, data_phenovalues)

write_file(Classification, data_classifications)
write_file(Classe, data_classes)
write_file(ClasseValue, data_classevalues)

write_file(ImageLink, data_imagelinks)
write_file(Image, data_images)

write_file(Reproduction_method, data_reproductionmethods)
write_file(Reproduction, data_reproductions)
write_file(AccessionRelation, data_accessionrelations)
write_file(SeedlotRelation, data_seedlotrelations)

print("End writing file")

