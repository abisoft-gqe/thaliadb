from django.apps import AppConfig


class CsvIoConfig(AppConfig):
    name = 'csv_io'
