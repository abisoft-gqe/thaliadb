# -*- coding: utf-8 -*-
"""LICENCE
    ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
LICENCE"""

from __future__ import absolute_import

import time
import datetime
import ast 
import csv
import colorcet as cc
import json
import folium
import operator
import os
import pandas as pd
import six
import statistics
import re
import numpy as np

from _csv import Dialect
from bokeh.embed import components
from bokeh.plotting import figure
from bokeh.palettes import linear_palette
from bokeh.transform import cumsum
from collections import Counter
from dal import autocomplete
from itertools import product
from math import pi
from operator import ior, iand

from django import forms
from django.http import HttpResponse, JsonResponse
from django.http.response import Http404
from django.shortcuts import render, redirect
from django.core.exceptions import ObjectDoesNotExist
from django.utils.dateformat import DateFormat
from django.utils.datastructures import MultiValueDictKeyError
from django.db import IntegrityError
from django.db.models import Q as Query_dj
from django.db.models import Count
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.contenttypes.models import ContentType
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.template.loader import render_to_string
from django.db.models import Prefetch

from commonfct.utils import add_pagination, execute_function_in_thread, get_list_from_list_file
from commonfct.genericviews import return_csv
from dataview.forms import PhenoViewerAccessionForm, PhenoViewerTraitForm, PhenoViewerEnvironmentForm,\
                           DataviewPhenotypingValuesForm, PhenoViewerTraitForm2,GenoViewerAccessionForm,\
                           GenoViewerLocusForm, GenoViewerExperimentForm, PhenoViewerEnvironmentForm2,\
                           DataviewGenotypingValuesForm, GenoViewerReferentialForm, GenoViewerChrForm,\
                           DataviewGenoIDForm, UploadLocusFileForm, DtviewProjectForm, FicheForm,\
                           DtviewAccessionTypeForm,DataviewAccession, GenoViewerAccessionForm2, PhenoViewerAccessionForm2,\
                           ClassificationViewerForm, ClassificationViewerForm2, ClasseViewerForm, SeedlotViewerForm,\
                           DtviewGenealogyTypeForm, DataviewClassificationVisuForm, GenoViewerAccessionForm3, VcfForm,\
                           UploadAccessionFileForm, GenoViewerGenomeVersionForm, GenoViewerGenomeLocationFormSet
from lot.models import Seedlot, SeedlotTypeAttribute 
from phenotyping.models import PhenotypicValue, Trait, Environment, SpecificTreatment
from genotyping.models import Sample, LocusType, Locus, Referential, Experiment, GenotypingID, LocusPosition, GenomeVersion, GenotypingValue
from accession.models import Accession, AccessionType, AccessionTypeAttribute
from team.models import User, Project, Institution
from dataview.decorators import login_required_on_private_instance

from team.views import write_csv_search_result
from genealogy_accession.models import AccessionRelation
from genealogy_seedlot.models import SeedlotRelation
from classification.models import Classification, Classe, ClasseValue
from images.models import Image, ImageLink
from classification.views import write_structure_csv_from_viewer



# Create your views here.
@login_required_on_private_instance
def dtgenohome(request):
    return render(request,'dataview/phenotyping_dtview_base.html',{'dataview':True,})

#=======================================VIEWER ==================================================
@login_required
def return_csv_pheno(request):
    tmpfile=open(settings.TEMP_FOLDER+'/view_pheno.csv','r')  
    response=HttpResponse(tmpfile.read(),content_type="application/vnd.ms-excel")
    response['Content-Disposition']='attachment; filename=view_pheno.csv'
    return response



#################################### CLASSIFICATION DATAVIEW #################################
@login_required_on_private_instance
def classViewer_management(request):
    """
      Structure's dataview in the Dataview menu.
         
      :var string template: template for dataview
      :var int n_page: page / step of the search in the dataview
      :var list select_classification: classification selected in the first step
      :var list classe_values: all the values in the classification
      :var Seedlot sl: seedlot in the classification
      :var int export_or_web: boolean to have the type of visualization
      :var int show_accession: boolean to show accession in the table
    """
    template = "dataview/classification_viewer.html"
    filename = 'view_classif'
    
    if request.method == 'GET':
        formproject = DtviewProjectForm(request.POST)
        username = None
        if request.user and request.user.is_authenticated:
            username = request.user.login
            if User.objects.get(login=username).is_superuser():
                projects = Project.objects.all()
            else:
                username_id=User.objects.get(login=username).id
                projects = Project.objects.filter(users=username_id)
        elif settings.PUBLIC_INSTANCE :
            projects = Project.objects.all()
        else :
            projects = Project.objects.none()
        formproject.fields['project'].queryset=projects.order_by('name')
           
        n_page=0
        
        return render(request,template,{'dataview':True, 'n_page':n_page, 'formproject':formproject})
    
    elif request.method == "POST":
        #Get the actual page number
        n_page = int(request.POST['n_page'])
        
        #Choose Classification
        if int(n_page) == 1:
            
            formproject = DtviewProjectForm(request.POST)
            
            if formproject.is_valid():
                project = request.POST.getlist('project')
                request.session['projects'] = project
                
            else:
                return render(request, template, {'dataview':True, "n_page":0, 'formproject':formproject})
            
            formclassification = ClassificationViewerForm(projects=project)
            
            return render(request,template,{'dataview':True, "n_page":n_page, 'formclassification': formclassification})
    
        
        #Choose Seedlots / Groups
        if int(n_page) == 2:
            
            classification = request.POST.get('classification')
#             print(classification)
#             print(Classification.objects.get(id=classification))
            
            visumodeform = DataviewClassificationVisuForm()
            formclasse = ClasseViewerForm()
            formseedlot = SeedlotViewerForm()
            
            classe_values = ClasseValue.objects.filter(classe__in=Classe.objects.filter(classification=classification).distinct())
            names = classe_values.values_list('seedlot__name', flat=True)
            
            projects = Project.objects.filter(id__in=request.session['projects'])
            
            formseedlot.fields["seedlot"].queryset = Seedlot.objects.filter(name__in=list(set(names)), projects__in=projects).distinct().order_by('name')
            formclasse.fields["group"].queryset = Classe.objects.filter(classification=classification).distinct().order_by('name')
            
            return render(request,template,{'dataview':True, "n_page":n_page, 'formclasse': formclasse, 'formseedlot': formseedlot, "visumodeform":visumodeform, "classification":classification})
        
        #Show the results
        if int(n_page) == 3:
            
            classification_id = request.POST.get('classification') #ID de la classification
            classe = request.POST.getlist('classe')
            seedlot = request.POST.getlist('seedlot')
            export_or_web = request.POST.get('export_or_web')
            show_accession = request.POST.get('show_accession')
            
            classification = Classification.objects.get(id=classification_id)
            
            formclasse = ClasseViewerForm(request.POST)
            formseedlot = SeedlotViewerForm(request.POST)
            
            if formclasse.is_valid() and formseedlot.is_valid():
                select_classe = formclasse.cleaned_data['group']
                select_seedlot = formseedlot.cleaned_data['seedlot']
                
            else:
                visumodeform = DataviewClassificationVisuForm(request.POST)
                return render(request,template,{'dataview':True, "n_page":2, 'formclasse': formclasse, 'formseedlot': formseedlot, "visumodeform":visumodeform, "classification":classification})
            
            fields_label = ["Seedlot"]
            
            if show_accession == "1":
                fields_label.append("Accession")
            for cl in select_classe:
                fields_label.append(cl.name)
            
            #Récupération des données
            values = []
            
            for seedlot in select_seedlot:
                line = [seedlot]
                if show_accession == "1":
                    line.append(seedlot.accession.name)
                for cl in select_classe:
                    cv = ClasseValue.objects.get(seedlot=seedlot, classe=cl)
                    line.append(cv.value)
                
                values.append(line)
                
            write_structure_csv_from_viewer(filename, fields_label, values)
            
            if export_or_web == "1":
                return return_csv(request, filename)
            
            else:
                tag_fields = {'dataview':True,
                              'n_page':n_page,
                              'show_accession': show_accession,
                              'values':values,
                              'filename':filename,
                              'fields_label':fields_label,
                              'export_or_web': export_or_web,
                              'list_seedlot':select_seedlot,
                              'classification':classification}
                
                return render(request,template,tag_fields)


@login_required_on_private_instance
def classPieChart(request, seedlot, classification):
    """
        Get the data for a seedlot in one classification. And create a Piechart.
        
        :var string template: template for classPieChart
        :var Seedlot seedlot: seedlot to show
        :var list classe_values: all the classe_values for the seedlot in the classification
        :var Classification classification: classification's name
    """
    template = "dataview/classification_piechart.html"

    #Get the Seedlot
    seedlot = Seedlot.objects.get(id=seedlot)
    #Get the CV of the Seedlot in this classification
    classe_values = ClasseValue.objects.filter(seedlot=seedlot, classe__in=Classe.objects.filter(classification=Classification.objects.get(id=classification)))
    #Get the name of the classification
    classification = (Classification.objects.get(id=classification)).name
    
    pie_data = {}
    color_dict = {}
    for val in classe_values:
        pie_data[val.classe.name] = float(val.value)
        
        if val.classe.name not in color_dict:
            color_dict[val.classe.name] = ''
    
    i = 0
    for classe in color_dict.keys():
        color_dict[classe] = cc.b_glasbey_hv[i]
        i += 1
    
    classe_list = sorted(pie_data.items(), key=operator.itemgetter(1), reverse=True)
    
    #The palette is made to be identical for all pie charts (to have the same colors for the groups)
    palette = []
    for classe in classe_list:
        palette.append(color_dict[classe[0]])
    
    data = pd.Series(pie_data).reset_index(name='value').rename(columns={'index':'classes'}).sort_values(by='value', ascending=False)
    data['angle'] = data['value']/data['value'].sum() * 2*pi
    data['color'] = palette
    
    title = seedlot.name+" repartition in classification "+classification
    legend = "Groups"
    size = 345
    
    plot = figure(height=size, width=size+250, title=title, tooltips="@classes: @value", x_range=(-0.5, 1.0))
    plot.wedge(x=0, y=1, radius=0.4,
               start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
               line_color="white", fill_color='color', legend_field='classes', source=data)
    
    plot.axis.axis_label=None
    plot.axis.visible=False
    plot.grid.grid_line_color = None
    
    plot.legend.title = legend
    plot.legend.label_text_font_size = '9pt'
    
    script, div = components(plot)
    
    return render(request, template, {'dataview':True,'script':script,'div':div,'size':size})



@login_required_on_private_instance
def class_map_viewer_management(request):
    """
      Classification map dataview menu.
      
      :var string template: template for classMapViewer_management
      :var int n_page: page / step of the search in the dataview
      :var list project: all the projects with access
      :var list classification: classification selected in the first step
      :var list classe_values: all the values in the classification
      :var list list_groups: groups in the selected classification
      :var list list_lat: latitude for each accessions
      :var list list_long: longitude for each accessions
      :var Classe select_group: selected group for the map
    """
    template = "dataview/classification_map.html"
    
    if request.method == 'GET' and 'id_classification' not in request.GET.keys():
        
        formproject = DtviewProjectForm(request.POST)
    
        username = None
        if request.user and request.user.is_authenticated:
            username = request.user.login
            if User.objects.get(login=username).is_superuser():
                projects = Project.objects.all()
            else:
                username_id=User.objects.get(login=username).id
                projects = Project.objects.filter(users=username_id)
        elif settings.PUBLIC_INSTANCE :
            projects = Project.objects.all()
        else :
            projects = Project.objects.none()
        formproject.fields['project'].queryset=projects.order_by('name')
           
        n_page=0
        
        return render(request,template,{'dataview':True, "n_page":n_page, 'formproject':formproject})
    
    elif request.method == 'POST' or 'id_classification' in request.GET.keys():
        #Get the actual page number
        if 'id_classification' in request.GET.keys():
            n_page = 2
        else:
            n_page = int(request.POST['n_page'])

        #We get the user's projects
        if settings.PUBLIC_INSTANCE :
            user_projects = Project.objects.all()
        else :
            username = None
            if request.user.is_authenticated:
                username = request.user.login
            username_id=User.objects.get(login=username).id
            user_projects = Project.objects.filter(users=username_id)

        #Choose Classification
        if int(n_page) == 1:
            
            project = request.POST.getlist('project')
            request.session['projects'] = project
            formclassification = ClassificationViewerForm(projects=project)
            formproject = DtviewProjectForm(request.POST)
            
            #Check le formulaire et récupération des projets dans une liste
            if formproject.is_valid():
                list_select_project = formproject.cleaned_data['project']
                
                #len() permet de checker si on a sélectionné au moins 1 projet
                if len(list_select_project) != 0:
                    return render(request,template,{'dataview':True, "n_page":n_page, 'formclassification': formclassification})

        #Show the map and select a group
        if int(n_page) == 2:
            if 'id_classification' in request.GET.keys():
                classification_id = request.GET['id_classification']
                request.session['projects'] = [proj.id for proj in user_projects]
            else:
                classification_id = request.POST.get('classification')
            classification = Classification.objects.get(id=int(classification_id))
            #print(classification)  
            
            #We get the projects linked to the classification
            classif_projects = classification.projects.all()
            
            #We select the projects that the user can see and that are linked to the classification 
            #(intersection of the 2 querysets)
            project_qs_int = user_projects & classif_projects

            #We get the queryset of the selected projects
            projects = Project.objects.filter(id__in=request.session['projects'])
            
            #We refine the queryset by the projects that were selected at first
            project_qs = project_qs_int & projects.all()
            
            #Get groups
            list_groups = Classe.objects.filter(classification=classification)
            
            #Get seedlots
            classe_values = ClasseValue.objects.filter(classe__in=list_groups)
            list_seedlots = []
            for cv in classe_values:
                if not cv.seedlot in list_seedlots:
                    if cv.seedlot.projects.all() & project_qs:
                        list_seedlots.append(cv.seedlot)
            
            #Seedlots names
            list_seedlots_names=[]
            for seedlot in list_seedlots:
                list_seedlots_names.append(seedlot.name)

            #Nb Seedlots
            nb_seedlots = []
            for i in range(0, len(list_seedlots)):
                nb_seedlots.append(i)
                    
            #Get Latitude & Longitude
            list_lat = []
            list_long = []
            
            for seedlot in list_seedlots:
                list_lat.append(seedlot.accession.latitude)
                list_long.append(seedlot.accession.longitude)

            data_card_url = "/dataview/data_card/?seedlot="
            
            #Selected group
            if request.POST.get('group'):
                select_group = request.POST["group"]
                select_group = Classe.objects.get(id=select_group)
                
                #Get the classe_values for the group
                list_cv = []
                for seedlot in list_seedlots:
                    list_cv.append(ClasseValue.objects.get(classe=select_group, seedlot=seedlot).value)
                
                #carte coloree
                fig = folium.Figure()
                m = folium.Map(
                    width=950,
                    height=500,
                    location=[20, 10],
                    zoom_start=2,
                )
                m.add_to(fig)
                
                shadow_path = os.path.join(settings.PROJECT_PATH, 'media/img/marker-shadow.png')
                
                for i in nb_seedlots:
                    if list_lat[i] and list_long[i]:
                        
                        icon_path = get_marker_image(list_cv[i])
                        
                        marker = folium.Marker([list_lat[i],list_long[i]], 
                                               popup="<a href="+data_card_url+list_seedlots_names[i]+">"+list_seedlots_names[i]+"</a> : "+list_cv[i], 
                                               tooltip=list_seedlots_names[i])
                        icon = folium.CustomIcon(icon_image=icon_path,
                                                 icon_size=[25, 40], 
                                                 icon_anchor=[12.5, 40], # To have the tip of the marker on the coordinates
                                                 shadow_image=shadow_path,
                                                 shadow_size=[37, 40],
                                                 shadow_anchor=[10, 40], # To have the shadow starting at the tip of the marker
                                                 popup_anchor=[0, -45]) # To have the pop up displaying on top of the marker
                        marker.add_child(icon)
                        m.add_child(marker)
                
                
                fig.render()
                
                return render(request,template,{'dataview':True, "n_page":n_page,"color":True, "list_lat":list_lat, "list_long":list_long, 
                                                "classification":classification, "nb_seedlots": nb_seedlots, "list_cv":list_cv, 
                                                "list_seedlots_names": list_seedlots_names, "list_groups": list_groups, "select_group": select_group, 'map':fig})

            else:
                select_group = None
                list_cv=[]
                
                #carte bleue
                fig = folium.Figure()
                m = folium.Map(
                    width=950,
                    height=500,
                    location=[20, 10],
                    zoom_start=2,
                )
                m.add_to(fig)
                
                for i in nb_seedlots:
                    if list_lat[i] and list_long[i]:
                        marker = folium.Marker([list_lat[i],list_long[i]], 
                                               popup="<a href="+data_card_url+list_seedlots_names[i]+">"+list_seedlots_names[i]+"</a>", 
                                               tooltip=list_seedlots_names[i])
                        m.add_child(marker)
                        
                fig.render()
                
                return render(request,template,{'dataview':True, "n_page":n_page, "color":False, "list_lat":list_lat, "list_long":list_long,
                                                "classification":classification, "nb_seedlots": nb_seedlots, "list_cv":list_cv,
                                                "list_seedlots_names": list_seedlots_names, "list_groups": list_groups, "select_group": select_group, 'map':fig})
                
        else:
            return redirect('classMapViewer_management')


def get_marker_image(value):
    """
      Function to return the marker image of 
      the right color according to the value.
    """
    if float(value) > 0.9:
        icon_path = os.path.join(settings.PROJECT_PATH, 'media/img/marker10.png')
        
    elif float(value) <= 0.9 and float(value) > 0.8:
        icon_path = os.path.join(settings.PROJECT_PATH, 'media/img/marker9.png')
        
    elif float(value) <= 0.8 and float(value) > 0.7:
        icon_path = os.path.join(settings.PROJECT_PATH, 'media/img/marker8.png')
        
    elif float(value) <= 0.7 and float(value) > 0.6:
        icon_path = os.path.join(settings.PROJECT_PATH, 'media/img/marker7.png')
        
    elif float(value) <= 0.6 and float(value) > 0.5:
        icon_path = os.path.join(settings.PROJECT_PATH, 'media/img/marker6.png')
        
    elif float(value) <= 0.5 and float(value) > 0.4:
        icon_path = os.path.join(settings.PROJECT_PATH, 'media/img/marker5.png')
        
    elif float(value) <= 0.4 and float(value) > 0.3:
        icon_path = os.path.join(settings.PROJECT_PATH, 'media/img/marker4.png')
        
    elif float(value) <= 0.3 and float(value) > 0.2:
        icon_path = os.path.join(settings.PROJECT_PATH, 'media/img/marker3.png')
        
    elif float(value) <= 0.2 and float(value) > 0.1:
        icon_path = os.path.join(settings.PROJECT_PATH, 'media/img/marker2.png')
        
    elif float(value) <= 0.1:
        icon_path = os.path.join(settings.PROJECT_PATH, 'media/img/marker1.png')
        
    else:
        icon_path = os.path.join(settings.PROJECT_PATH, 'media/img/marker_blue.png')
    
    return icon_path


######################################## PHENOTYPING DATAVIEW #################################
@login_required_on_private_instance
def phenoViewer_management(request):
    project=[]
    form2 = PhenoViewerAccessionForm2(request.POST)
    formtrait = PhenoViewerTraitForm(request.POST)
    formenv = PhenoViewerEnvironmentForm(request.POST)
    formproject = DtviewProjectForm(request.POST)
    error_msg=""
    headername = None
    if request.method=="POST":
        projects_or_not=request.POST['projects_or_not']
        if projects_or_not=="0":
            form2 = PhenoViewerAccessionForm2()
            formtrait = PhenoViewerTraitForm()
            formenv = PhenoViewerEnvironmentForm()
            formproject = DtviewProjectForm(request.POST)
            visumodeform = DataviewPhenotypingValuesForm()
            projects_or_not=1
            project=request.POST.getlist('project')
            request.session['projects'] = project
            form2.fields["accession"].queryset = Accession.objects.filter(projects__in=project).distinct().order_by('name')
            formtrait.fields["trait"].queryset = Trait.objects.filter(projects__in=project).distinct().order_by('name')
            formenv.fields["environment"].queryset = Environment.objects.filter(projects__in=project).distinct().order_by('name')
            return render(request,'dataview/phenotyping_viewer.html',{'dataview':True,"projects":project,'form2':form2,"projects_or_not":projects_or_not,'formtrait':formtrait, 'formenv':formenv, 'formproject':formproject, 'visumodeform':visumodeform})

        else:    
            if form2.is_valid() and formenv.is_valid() and formtrait.is_valid():
                projects = request.session['projects']
                if request.POST.getlist("name"):
                    id_autocomplete = request.POST.getlist("name")
                    listaccessions = Accession.objects.filter(id__in=id_autocomplete)   
                else:   
                    listaccessions = form2.cleaned_data['accession']
                listraits = formtrait.cleaned_data['trait']
                listenvs = formenv.cleaned_data['environment']   
                
                #============================
                mode = request.POST["choose_visualization_mode"]
                if mode == "1":
                    headername = 'Accession'
                else:
                    headername = 'Seedlot'     
                #===========================================================           
                if listaccessions!= None and listraits!= None and listenvs!= None :
                    df = getPhenodata(listaccessions, listraits, listenvs, headername, projects)
                    if df.empty:
                        html=None
                    else:
                        html = df.to_html()    
                        html = html.split('\n')

                    pheno_html="dataview/phenotyping_dtview_base.html"
                    if request.POST["export_or_view_in_web_browser"]=="2":
                        return render(request, 'phenotyping/pheno_values/dtviewphenovalues.html', {'dataview':True,"pheno_html":pheno_html,"html":html})
                    else:
                        return return_csv_pheno(request)
                else:                
                    if listaccessions==None:
                        error_msg="Please choose at least one accession (step 1)."
                    elif listraits==None:
                        error_msg="Please choose at least one trait (step 2)."
                    elif listenvs==None:
                        error_msg="Please choose at least one environment (step 3)."
                    return render(request, 'dataview/phenotyping_viewer.html', {'dataview':True,'accesform':GenoViewerAccessionForm(), 'traitform':PhenoViewerTraitForm, 'enviform':PhenoViewerEnvironmentForm, 'visumodeform':DataviewPhenotypingValuesForm})       
                          
                    #========================================================
                    
            elif request.method=="POST":
                if not request.POST.getlist("name") and not request.POST.getlist('accession'):
                    error_msg="Please choose at least one accession (step 1)."
                elif not request.POST.getlist("trait"):
                    error_msg="Please choose at least one trait (step 2)."
                elif not request.POST.getlist('environment'):   
                    error_msg="Please choose at least one environment (step 3)."
    form2 = PhenoViewerAccessionForm2()
    formtrait = PhenoViewerTraitForm()
    formenv = PhenoViewerEnvironmentForm()
    visumodeform = DataviewPhenotypingValuesForm()
    
    if request.user and request.user.is_authenticated:
        username = request.user.login
        if User.objects.get(login=username).is_superuser():
            projects = Project.objects.all()
        else:
            username_id=User.objects.get(login=username).id
            projects = Project.objects.filter(users=username_id)
    elif settings.PUBLIC_INSTANCE :
        projects = Project.objects.all()
    else :
        projects = Project.objects.none()
    formproject.fields['project'].queryset=projects.order_by('name')
    
    projects_or_not = 0
    
    if error_msg!="":
        projects_or_not=1
        project=request.POST['project']
        projects=project.split("'")
        project=[]
        for i in projects:
            if i.isdigit():
                project.append(i)        
        form2.fields["accession"].queryset = Accession.objects.filter(projects__in=project).distinct().order_by('name')
    else:
        project=""
    return render(request,'dataview/phenotyping_viewer.html',{'dataview':True,"formproject":formproject,"formenv":formenv,"formtrait":formtrait,"form2":form2,'projects':project,'error_msg':error_msg,'formproject':formproject,"projects_or_not":projects_or_not,'accesform':GenoViewerAccessionForm(), 'visumodeform':visumodeform})

def getPhenodata(listaccessions, listraits, listenvs, headername, projects):#headers
    datadict ={}
    traitslist = [exp for exp in listraits if exp != None]        
    envilist = [env for env in listenvs if env != None]
    acclist = [acc for acc in listaccessions if acc != None]
    headers = []
    headers.append(headername)   
    pheno_viewer=False 
    df_pheno=pd.DataFrame()
    
    #New version to accelerate query
    
    seedlots = Seedlot.objects.filter(accession__in=acclist, projects__in=projects)
    
    phenos = PhenotypicValue.objects.select_related('trait','seedlot','environment','seedlot__accession').filter(seedlot__in=seedlots, environment__in= envilist,trait__in=traitslist).order_by('seedlot__name','trait__name','environment__name')
    
    for pheno in phenos:
        tr = str(pheno.trait.name) +'\n'+ str(pheno.environment.name)
        if headername == 'Accession' or headername == "Accession_pheno":
            df_pheno.loc[str(pheno.seedlot.accession.name),tr]=str(pheno.value).replace(',', '.')
        elif headername == "Geno_pheno":
            sample=Sample.objects.filter(seedlot=pheno.seedlot)
            for sple in sample:
                genoid=GenotypingID.objects.filter(sample=sple)
                for geno in genoid:
                    df_pheno.loc[str(geno),tr]=str(pheno.value).replace(',', '.')
        elif headername == "Sample_pheno":
            sample=Sample.objects.filter(seedlot=pheno.seedlot)
            for sple in sample:
                df_pheno.loc[str(sple),tr]=str(pheno.value).replace(',', '.')
        else: 
            df_pheno.loc[str(pheno.seedlot),tr]=str(pheno.value).replace(',', '.')
    
    df_pheno = df_pheno.fillna('NA')                    
    df_pheno.to_csv(settings.TEMP_FOLDER+"/view_pheno.csv", sep=';')
    return df_pheno


#======================================GENOTYPING VALUES===================================================

def get_genotyping_viewer_experiments(request):
    """
      Return a list of experiments, matching the selected 
      referential(s) to populate the experiment form field.
    """
    selected_refs = list(map(int,request.POST.get('selectedRefs').split(',')))
    if not selected_refs:
        experiments = Experiment.objects.all()
    
    else:
        exps_ids = GenotypingID.objects.filter(genotypingvalue__referential_id__in=selected_refs).values_list('experiment_id').distinct()
        experiments = Experiment.objects.filter(id__in=exps_ids)
    exps_list = []
    for exp in experiments:
        exps_list.append((exp.id, exp.name))
    
    return JsonResponse({
        'expsList': exps_list,
    })



def get_genotyping_viewer_nb_locus(request):
    """
      Return the number of locus matching the position 
      information and referentials (optional) to display
      next to the position ranges.
    """
    
    nb_locus = ''
    selected_refs = list(map(int,request.POST.get('selectedRefs').split(',')))
    gv_val = request.POST.get('gvVal')
    chr_val = request.POST.get('chrVal')
    start_val = request.POST.get('startVal')
    end_val = request.POST.get('endVal')
    print(selected_refs)
    if chr_val and end_val and (start_val or start_val == 0):
        if selected_refs == []:
            nb_locus = LocusPosition.objects.filter(genome_version=gv_val, chromosome=chr_val, position__gte=start_val, position__lte=end_val).values('locus').distinct().count()
        
        else:
            loci_list = LocusPosition.objects.filter(genome_version=gv_val, chromosome=chr_val, position__gte=start_val, position__lte=end_val).order_by('locus').values_list('locus', flat=True).distinct()
            nb_locus = Locus.objects.filter(id__in=loci_list, geno_values__referential_id__in=selected_refs).distinct().count()
    
    return JsonResponse({
        'nbLocus': nb_locus,
    })


@login_required_on_private_instance
def genotyping_viewer(request):
    """
      View allowing the user to visualize genotyping data.
    """
    title = "Genotyping viewer"
    template = 'dataview/genotyping_viewer_new.html'
    is_admin = False
    parent_temp = "dataview/phenotyping_dtview_base.html"
    
    if request.method == 'GET':
        formproject = DtviewProjectForm()

        if request.user and request.user.is_authenticated:
            username = request.user.login
            if User.objects.get(login=username).is_superuser():
                projects = Project.objects.all()
            else:
                username_id=User.objects.get(login=username).id
                projects = Project.objects.filter(users=username_id)
        elif settings.PUBLIC_INSTANCE :
            projects = Project.objects.all()
        else :
            projects = Project.objects.none()

        formproject.fields['project'].queryset=projects.order_by('name')

        n_page = 0

        return render(request,template,{'title':title, 'n_page':n_page, 'dataview':True,
                                        'parent_temp':parent_temp, 'formproject':formproject})
    
    elif request.method == 'POST':
        formproject = DtviewProjectForm(request.POST)

        #Get the actual page number
        n_page = int(request.POST['n_page'])
    
        if int(n_page) == 1:
            if formproject.is_valid():
                projects = request.POST.getlist('project')
                request.session['projects'] = projects
            else:
                return render(request,template,{'title':title, 'n_page':0, 'dataview':True, 'parent':parent_temp, 'formproject':formproject})
    
            tag_fields = display_genoviewer_form(request, projects, is_admin)
        
            tag_fields.update({'title': title,
                               'n_page':n_page,
                               'dataview':True,
                               'parent_temp':parent_temp})
            
            return render(request,template,tag_fields)
        
        if int(n_page) == 2:
            start_time = time.time()
            
            tag_fields, locus, genome_version = check_genoviewer_form_data(request, title, is_admin)
            
            if tag_fields == {}:
                return process_genoviewer_form_data(request, template, title, locus, genome_version, start_time, is_admin, parent_temp)
            
            else:
                tag_fields.update({'title': title,
                                   'dataview':True,
                                   'parent_temp':parent_temp})
                
                return render(request,template,tag_fields)
            

def display_genoviewer_form(request, projects, is_admin):
    """
      Instantiate the various forms used for the genotyping viewer,
      in dataview and admin mode.
    """
    #If the viewer is called from admin, even the objects without projects are queried
    if is_admin:
        query_proj = ior(Query_dj(projects__in=projects), Query_dj(projects__isnull=True))
    else:
        query_proj = Query_dj(projects__in=projects)
    
    formacc = GenoViewerAccessionForm2()
    formaccfile = UploadAccessionFileForm()
    formexp = GenoViewerExperimentForm()
    formref = GenoViewerReferentialForm()
    formlocifile = UploadLocusFileForm()
    formgenov = GenoViewerGenomeVersionForm()
    formchrset = GenoViewerGenomeLocationFormSet()
    visumodeform = DataviewGenotypingValuesForm()
    
    formacc.fields["accession"].queryset = Accession.objects.filter(query_proj).distinct().order_by('name')
    formexp.fields["experiment"].queryset = Experiment.objects.filter(query_proj).distinct().order_by('name')
    formref.fields["referential"].queryset = Referential.objects.filter(query_proj).distinct().order_by('name')
    
    genomev_to_chr = LocusPosition.objects.all().values_list('genome_version','chromosome').order_by('genome_version','chromosome').distinct()
    
    chr_choices = []
    gv_chr_dict = {}
    
    for gv_chr_cp in genomev_to_chr:
        if gv_chr_cp[0] in gv_chr_dict.keys():
            gv_chr_dict[gv_chr_cp[0]].append(gv_chr_cp[1])
            chr_choices.append([gv_chr_cp[1],gv_chr_cp[1]])
        else:
            gv_chr_dict[gv_chr_cp[0]] = [gv_chr_cp[1]]
            chr_choices.append([gv_chr_cp[1],gv_chr_cp[1]])
    
    for formchr in formchrset.forms:
        formchr.fields['chromosome'].choices = chr_choices
    
    gv_chr = json.dumps(gv_chr_dict)
    
    request.session['chr_choices'] = chr_choices
    request.session['gv_chr'] = gv_chr
    
    tag_fields = {'refine_search':True,
                  'formacc':formacc,
                  'formaccfile':formaccfile,
                  'visumodeform':visumodeform,
                  'formexp':formexp,
                  'formref':formref,
                  'formgenov':formgenov,
                  'formchrset':formchrset,
                  'formlocifile':formlocifile,
                  'projects':projects,
                  'gv_chr':gv_chr}

    return tag_fields


def check_genoviewer_form_data(request, title, is_admin):
    """
      Verifies all data submitted in forms and returns the necessary 
      information to displaying the genotyping data.
    """
    tag_fields = {}
    locus = ''
    genome_version = ''
    
    formacc = GenoViewerAccessionForm2(request.POST)
    formaccfile = UploadAccessionFileForm(request.POST, request.FILES)
    formexp = GenoViewerExperimentForm(request.POST)
    formref = GenoViewerReferentialForm(request.POST)
    formlocifile = UploadLocusFileForm(request.POST, request.FILES)
    formgenov = GenoViewerGenomeVersionForm(request.POST)
    formchrset = GenoViewerGenomeLocationFormSet(request.POST)
    visumodeform = DataviewGenotypingValuesForm(request.POST)
    
    projects = request.session['projects']
    gv_chr = request.session['gv_chr']
    
    #If the viewer is called from admin, even the objects without projects are queried
    if is_admin:
        query_proj = ior(Query_dj(projects__in=projects), Query_dj(projects__isnull=True))
    else:
        query_proj = Query_dj(projects__in=projects)
    
    if request.FILES.get('acc_file'):
        acc_file = request.FILES['acc_file'].read()
        acc_list = get_list_from_list_file(acc_file)
        
        accessions = Accession.objects.filter(query_proj, name__in=acc_list).distinct()
        
    else:
        acc_list = request.POST.getlist('accession')
        
        accessions = Accession.objects.filter(query_proj, id__in=acc_list).distinct()
        
    if not accessions:
        formacc.fields["accession"].queryset = Accession.objects.filter(query_proj).distinct().order_by('name')
        formexp.fields["experiment"].queryset = Experiment.objects.filter(query_proj).distinct().order_by('name')
        formref.fields["referential"].queryset = Referential.objects.filter(query_proj).distinct().order_by('name')
        
        messages.add_message(request, messages.ERROR, "An error occurred, please select accessions in the form or submit a file with accession names.")
        tag_fields = {'title': title, 'n_page':1, 'refine_search':True,'formacc':formacc,'formaccfile':formaccfile,'visumodeform':visumodeform,
                      'formexp':formexp,'formref':formref, 'formgenov':formgenov,'formchrset':formchrset,'formlocifile':formlocifile,'projects':projects,'gv_chr':gv_chr}
        return tag_fields, locus, genome_version
    
    experiments = Experiment.objects.filter(id__in=request.POST.getlist('experiment'))
    
    if not experiments:
        formacc.fields["accession"].queryset = Accession.objects.filter(query_proj).distinct().order_by('name')
        formexp.fields["experiment"].queryset = Experiment.objects.filter(query_proj).distinct().order_by('name')
        formref.fields["referential"].queryset = Referential.objects.filter(query_proj).distinct().order_by('name')
        
        messages.add_message(request, messages.ERROR, "An error occurred, please select at least one experiment.")
        tag_fields = {'title': title, 'n_page':1, 'refine_search':True,'formacc':formacc,'formaccfile':formaccfile,'visumodeform':visumodeform,
                      'formexp':formexp,'formref':formref, 'formgenov':formgenov,'formchrset':formchrset,'formlocifile':formlocifile,'projects':projects,'gv_chr':gv_chr}
        return tag_fields, locus, genome_version
    
    referentials = Referential.objects.filter(id__in=request.POST.getlist('referential'))
    
    if not referentials:
        formacc.fields["accession"].queryset = Accession.objects.filter(query_proj).distinct().order_by('name')
        formexp.fields["experiment"].queryset = Experiment.objects.filter(query_proj).distinct().order_by('name')
        formref.fields["referential"].queryset = Referential.objects.filter(query_proj).distinct().order_by('name')
        
        messages.add_message(request, messages.ERROR, "An error occurred, please select at least one referential.")
        tag_fields = {'title': title, 'n_page':1, 'refine_search':True,'formacc':formacc,'formaccfile':formaccfile,'visumodeform':visumodeform,
                      'formexp':formexp,'formref':formref, 'formgenov':formgenov,'formchrset':formchrset,'formlocifile':formlocifile,'projects':projects,'gv_chr':gv_chr}
        return tag_fields, locus, genome_version
            
    genotyping_ids = GenotypingID.objects.filter(query_proj, sample__seedlot__accession__in=accessions, experiment__in=experiments).distinct()
    genoids_names = list(genotyping_ids.values_list('name', flat=True))
    
    
    if request.FILES.get('loci_file'):
        genome_version_id = request.POST.get('genome_version_file')
        
        try:
            genome_version = GenomeVersion.objects.get(id=genome_version_id)
        except Exception:
            formacc.fields["accession"].queryset = Accession.objects.filter(query_proj).distinct().order_by('name')
            formexp.fields["experiment"].queryset = Experiment.objects.filter(query_proj).distinct().order_by('name')
            formref.fields["referential"].queryset = Referential.objects.filter(query_proj).distinct().order_by('name')
            
            messages.add_message(request, messages.ERROR, "An error occurred, please select a genome version.")
            tag_fields = {'title': title, 'n_page':1, 'refine_search':True,'formacc':formacc,'formaccfile':formaccfile,'visumodeform':visumodeform,
                          'formexp':formexp,'formref':formref, 'formgenov':formgenov,'formchrset':formchrset,'formlocifile':formlocifile,'projects':projects,'gv_chr':gv_chr}
            return tag_fields, locus, genome_version
        
        loci_file = request.FILES['loci_file'].read()
        loci_list = get_list_from_list_file(loci_file)
        
        locus = Locus.objects.prefetch_related(
                    Prefetch('locusposition_set',queryset=LocusPosition.objects.filter(genome_version=genome_version), to_attr='positions'),
                    Prefetch('geno_values', queryset=GenotypingValue.objects.select_related('genotyping_id__sample',
                                                                                            'genotyping_id__sample__seedlot',
                                                                                            'genotyping_id__sample__seedlot__accession',
                                                                                            'genotyping_id__experiment',
                                                                                            'allele',
                                                                                            'referential').filter(genotyping_id__in=genotyping_ids, referential__in=referentials), to_attr='genovalues')
                ).filter(name__in=loci_list, geno_values__referential__in=referentials).distinct()
        
        return tag_fields, locus, genome_version
    
    else:
        genome_version_id = request.POST.get('genome_version')
        
        try:
            genome_version = GenomeVersion.objects.get(id=genome_version_id)
        except Exception:
            formacc.fields["accession"].queryset = Accession.objects.filter(query_proj).distinct().order_by('name')
            formexp.fields["experiment"].queryset = Experiment.objects.filter(query_proj).distinct().order_by('name')
            formref.fields["referential"].queryset = Referential.objects.filter(query_proj).distinct().order_by('name')
            
            messages.add_message(request, messages.ERROR, "An error occurred, please select a genome version.")
            tag_fields = {'title': title, 'n_page':1, 'refine_search':True,'formacc':formacc,'formaccfile':formaccfile,'visumodeform':visumodeform,
                          'formexp':formexp,'formref':formref, 'formgenov':formgenov,'formchrset':formchrset,'formlocifile':formlocifile,'projects':projects,'gv_chr':gv_chr}
            return tag_fields, locus, genome_version
        
        chr_list = []
        pos_start_list = []
        pos_end_list = []
        
        chr_choices = request.session['chr_choices']
        #Needed for validation because the choices are empty in the form
        for formchr in formchrset.forms:
            formchr.fields['chromosome'].choices = chr_choices
        
        if formchrset.is_valid():
            for formchr in formchrset:
                chr_data = formchr.cleaned_data
                
                chr_list.append(chr_data.get('chromosome')) if chr_data.get('chromosome') else chr_list.append('')
                
                if chr_data.get('start_position') or chr_data.get('start_position') == 0:
                    pos_start_list.append(chr_data.get('start_position'))
                else:
                    pos_start_list.append('')
                    
                pos_end_list.append(chr_data.get('end_position')) if chr_data.get('end_position') else pos_end_list.append('')
            
            if '' in chr_list or '' in pos_start_list or '' in pos_end_list:
                formacc.fields["accession"].queryset = Accession.objects.filter(query_proj).distinct().order_by('name')
                formexp.fields["experiment"].queryset = Experiment.objects.filter(query_proj).distinct().order_by('name')
                formref.fields["referential"].queryset = Referential.objects.filter(query_proj).distinct().order_by('name')
                
                messages.add_message(request, messages.ERROR, "An error occurred, please add valid numbers for chromosome and start and end positions.")
                tag_fields = {'title': title, 'n_page':1, 'refine_search':True,'formacc':formacc,'formaccfile':formaccfile,'visumodeform':visumodeform,
                              'formexp':formexp,'formref':formref, 'formgenov':formgenov,'formchrset':formchrset,'formlocifile':formlocifile,'projects':projects,'gv_chr':gv_chr}
                return tag_fields, locus, genome_version
            
            loci_list = []
            for i in range(len(chr_list)):
                positions = LocusPosition.objects.filter(genome_version=genome_version, chromosome=chr_list[i], position__gte=pos_start_list[i], position__lte=pos_end_list[i])
                loci_list = loci_list + list(positions.values_list('locus', flat=True))
                
            locus = Locus.objects.prefetch_related(
                    Prefetch('locusposition_set',queryset=LocusPosition.objects.filter(genome_version=genome_version), to_attr='positions'),
                    Prefetch('geno_values', queryset=GenotypingValue.objects.select_related('genotyping_id__sample',
                                                                                            'genotyping_id__sample__seedlot',
                                                                                            'genotyping_id__sample__seedlot__accession',
                                                                                            'genotyping_id__experiment',
                                                                                            'allele',
                                                                                            'referential').filter(genotyping_id__in=genotyping_ids, referential__in=referentials), to_attr='genovalues')
                ).filter(id__in=loci_list, geno_values__referential__in=referentials).distinct()
            
            return tag_fields, locus, genome_version
        
        else:
            formacc.fields["accession"].queryset = Accession.objects.filter(query_proj).distinct().order_by('name')
            formexp.fields["experiment"].queryset = Experiment.objects.filter(query_proj).distinct().order_by('name')
            formref.fields["referential"].queryset = Referential.objects.filter(query_proj).distinct().order_by('name')
            
            messages.add_message(request, messages.ERROR, "An error occurred, please add valid informations for chromosome and start and end positions.")
            tag_fields = {'title': title, 'n_page':1, 'refine_search':True,'formacc':formacc,'formaccfile':formaccfile,'visumodeform':visumodeform,
                          'formexp':formexp,'formref':formref, 'formgenov':formgenov,'formchrset':formchrset,'formlocifile':formlocifile,'projects':projects,'gv_chr':gv_chr}
            return tag_fields, locus, genome_version


def process_genoviewer_form_data(request, template, title, locus, genome_version, start_time, is_admin, parent_temp):
    """
      Calls the function that will format the data to display 
      according to the user's choices.
    """
    visualization_mode = request.POST.get('choose_visualization_mode')
    export_or_display = request.POST.get('export_or_display')
    ind_or_freq = request.POST.get('ind_or_freq')
    
    # print(" Fin traitement")
    # print("--- %s seconds ---" % (time.time() - start_time))
    
    # Individual
    if ind_or_freq == "1":
        
        # Genotyping_ID
        if visualization_mode == "4":
            return display_matrix_genotyping_id(request, template, title, locus, genome_version, export_or_display, start_time, is_admin, parent_temp)
        
        # Sample
        elif visualization_mode == "3":
            return display_matrix_sample(request, template, title, locus, genome_version, export_or_display, start_time, is_admin, parent_temp)
        
        # Seedlot
        elif visualization_mode == "2":
            return display_matrix_seedlot(request, template, title, locus, genome_version, export_or_display, start_time, is_admin, parent_temp)

        # Accession
        elif visualization_mode == "1":
            return display_matrix_accession(request, template, title, locus, genome_version, export_or_display, start_time, is_admin, parent_temp)
        
        else:
            messages.add_message(request, messages.ERROR, "This option is not available yet.")
            return render(request,template,{'title': title, 'n_page':2, 'parent_temp':parent_temp})
    
    
    # Frequency
    elif ind_or_freq == "2":
        # Genotyping_ID
        if visualization_mode == "4":
            return display_frequencies_genotyping_id(request, template, title, locus, genome_version, export_or_display, start_time, is_admin, parent_temp)

        # Sample
        elif visualization_mode == "3":
            return display_frequencies_sample(request, template, title, locus, genome_version, export_or_display, start_time, is_admin, parent_temp)
        
        # Seedlot
        elif visualization_mode == "2":
            return display_frequencies_seedlot(request, template, title, locus, genome_version, export_or_display, start_time, is_admin, parent_temp)
        
        # Accession
        elif visualization_mode == "1":
            return display_frequencies_accession(request, template, title, locus, genome_version, export_or_display, start_time, is_admin, parent_temp)
        
        else:
            messages.add_message(request, messages.ERROR, "This option is not available yet.")
            return render(request,template,{'title': title, 'n_page':2, 'parent_temp':parent_temp})
  

def display_matrix_genotyping_id(request, template, title, locus, genome_version, export_or_display, start_time, is_admin, parent_temp):
    """
      To display genotyping matrix for genotyping_ID mode.
    """
    data = {'Genome Version': {},'Chromosome':{},'Position':{}}
    columns = ['Accession','Seedlot','Sample','Experiment','Referential']
    index = {}
    
    for elem in columns:
        data['Genome Version'][elem] = '--'
        data['Chromosome'][elem] = '--'
        data['Position'][elem] = '--'
    
    locus = locus.order_by('locusposition__chromosome', 'locusposition__position')
    
    for loc in locus:
        columns.append(loc.name)
        
        if loc.positions:
            data['Genome Version'][loc.name] = genome_version
            data['Chromosome'][loc.name] = loc.positions[0].chromosome
            data['Position'][loc.name] = '{:,}'.format(loc.positions[0].position).replace(',', ' ')
        else:
            data['Genome Version'][loc.name] = 'Unknown'
            data['Chromosome'][loc.name] = 'Unknown'
            data['Position'][loc.name] = 'Unknown'
        
        gvalues = {}
        
        # On ajoute les valeurs de genotypage
        for value in loc.genovalues:
            if (value.genotyping_id.name,value.referential.name) not in index.keys():
                index[(value.genotyping_id.name,value.referential.name)] = value.genotyping_id.name
                
            if (value.genotyping_id.name,value.referential.name) not in data.keys():
                data[(value.genotyping_id.name,value.referential.name)] = {'Accession':value.genotyping_id.sample.seedlot.accession,
                                                                           'Seedlot':value.genotyping_id.sample.seedlot,
                                                                           'Sample':value.genotyping_id.sample,
                                                                           'Experiment':value.genotyping_id.experiment,
                                                                           'Referential':value.referential}
                
            if (value.genotyping_id.name,value.referential.name) in gvalues.keys():
                gvalues[(value.genotyping_id.name,value.referential.name)].append(value)
            else:
                gvalues[(value.genotyping_id.name,value.referential.name)] = [value]
         
         
        for genoid, genovalues in gvalues.items():
            if len(genovalues) == 1:
                if genovalues[0].allele.code_allele == "NA":
                    data[genoid][loc.name] = genovalues[0].allele.code_allele
                elif genovalues[0].frequency != 1.0:
                    data[genoid][loc.name] = genovalues[0].allele.code_allele + "/"
                else:
                    data[genoid][loc.name] = genovalues[0].allele.code_allele + "/" + genovalues[0].allele.code_allele
            elif len(genovalues) > 1:
                w_val = genovalues[0].allele.code_allele
                del genovalues[0]
                for gval in genovalues:
                    w_val = w_val + "/" + gval.allele.code_allele
                data[genoid][loc.name] = w_val
               
    
#     print("Fin boucle")
#     print("--- %s seconds ---" % (time.time() - start_time))
    
    df = pd.DataFrame.from_dict(data, orient='index',columns=columns)
    
    # We recover the index
    index_list = list(df.index)
    
    index_list.remove("Position")
    index_list.remove("Chromosome")
    index_list.remove("Genome Version")
    
    # We sort it in alphabetical order
    index_list.sort()
    
    # We put back the position information to the beginning
    index_list.insert(0, "Position")
    index_list.insert(0, "Chromosome")
    index_list.insert(0, "Genome Version")
    
    # We replace the index with the one that was formatted
    df = df.reindex(index_list)
    
    # And remove referential info to leave only the genoid name
    final_df = df.rename(index=index)
    
    row, col = final_df.shape
    nb_genoids = row - 3
    nb_locus = col - 5
    #print(nb_genoids, nb_locus)
#                     print(final_df)
#                     print(final_df.iloc[0])
#                     print(final_df.iloc[[0]])
    
    now = datetime.datetime.now()
    filetime = now.strftime("_%m-%d-%Y_%H-%M-%S")
    filename = "genotyping_viewer"+filetime
    thread_name = execute_function_in_thread(final_df.to_csv(path_or_buf=settings.TEMP_FOLDER+filename+".csv", sep=';'), [], {})
    
    if is_admin:
        is_dataview = False
    else:
        is_dataview = True
    
    if export_or_display == "1":
        return render(request,template,{'parent_temp':parent_temp, 'title': title, 'n_page':2, 'nb_genoids':nb_genoids, 'nb_locus':nb_locus, 'mode':'genotyping_ID',
                                        'thread_name':thread_name, 'filename':filename, 'export':True, 'admin':is_admin, 'dataview':is_dataview})   
    
    else:
        html = final_df.to_html(index=True, border=0, justify='center', classes="pandas-result pandas-result-ind alternate tablesorter", table_id="basictable")
        html = html.split('\n')

#         print("Fin dataframe")
#         print("--- %s seconds ---" % (time.time() - start_time))
           
        return render(request,template,{'parent_temp':parent_temp, 'title': title, 'n_page':2, 'html':html, 'nb_genoids':nb_genoids, 'nb_locus':nb_locus, 'mode':'genotyping_ID',
                                        'display':'individual', 'thread_name':thread_name, 'filename':filename, 'admin':is_admin, 'dataview':is_dataview})   


def display_matrix_sample(request, template, title, locus, genome_version, export_or_display, start_time, is_admin, parent_temp):
    """
      To display genotyping matrix for sample mode.
    """
    data = {'Genome Version': {},'Chromosome':{},'Position':{}}
    columns = ['Accession','Seedlot','Experiment','Referential']
    index = {}
    
    for elem in columns:
        data['Genome Version'][elem] = '--'
        data['Chromosome'][elem] = '--'
        data['Position'][elem] = '--'
    
    locus = locus.order_by('locusposition__chromosome', 'locusposition__position')
    
    for loc in locus:
        columns.append(loc.name)
        
        if loc.positions:
            data['Genome Version'][loc.name] = genome_version
            data['Chromosome'][loc.name] = loc.positions[0].chromosome
            data['Position'][loc.name] = '{:,}'.format(loc.positions[0].position).replace(',', ' ')
        else:
            data['Genome Version'][loc.name] = 'Unknown'
            data['Chromosome'][loc.name] = 'Unknown'
            data['Position'][loc.name] = 'Unknown'
        
        gvalues = {}

        # On ajoute les valeurs de genotypage
        for value in loc.genovalues:
            if (value.genotyping_id.sample.name,value.referential.name) not in index.keys():
                index[(value.genotyping_id.sample.name,value.referential.name)] = value.genotyping_id.sample.name
            
            if (value.genotyping_id.sample.name,value.referential.name) not in data.keys():
                data[(value.genotyping_id.sample.name,value.referential.name)] = {'Accession':value.genotyping_id.sample.seedlot.accession,
                                                                                  'Seedlot':value.genotyping_id.sample.seedlot,
                                                                                  'Experiment':value.genotyping_id.experiment,
                                                                                  'Referential':value.referential}
                
            if (value.genotyping_id.sample.name,value.referential.name) in gvalues.keys():
                if (value.genotyping_id.name,value.referential) in gvalues[(value.genotyping_id.sample.name,value.referential.name)].keys():
                    gvalues[(value.genotyping_id.sample.name,value.referential.name)][(value.genotyping_id.name,value.referential)].append(value)
                else:
                    gvalues[(value.genotyping_id.sample.name,value.referential.name)][(value.genotyping_id.name,value.referential)] = [value]
            else:
                gvalues[(value.genotyping_id.sample.name,value.referential.name)] = {}
                gvalues[(value.genotyping_id.sample.name,value.referential.name)][(value.genotyping_id.name,value.referential)] = [value]
                
        
        for sample, genoid_dict in gvalues.items():
            list_pot_values = []
            for genoid, genovalues in genoid_dict.items():
                if len(genovalues) == 1: #Homozygote
                    if genovalues[0].allele.code_allele == "NA":
                        if genovalues[0].allele.code_allele not in list_pot_values:
                            list_pot_values.append(genovalues[0].allele.code_allele)
                    elif genovalues[0].frequency != 1.0:
                        if genovalues[0].allele.code_allele + "/" not in list_pot_values:
                            list_pot_values.append(genovalues[0].allele.code_allele + "/")
                    else:
                        if genovalues[0].allele.code_allele + "/" + genovalues[0].allele.code_allele not in list_pot_values:
                            list_pot_values.append(genovalues[0].allele.code_allele + "/" + genovalues[0].allele.code_allele)
                elif len(genovalues) > 1: #Heterozygote
                    w_val = genovalues[0].allele.code_allele
                    del genovalues[0]
                    for gval in genovalues:
                        w_val = w_val + "/" + gval.allele.code_allele
                    if w_val not in list_pot_values:
                        list_pot_values.append(w_val)
                    
            if len(list_pot_values) == 1:
                data[sample][loc.name] = list_pot_values[0]
            elif len(list_pot_values) == 2 and 'NA' in list_pot_values:
                list_pot_values.remove('NA')
                data[sample][loc.name] = list_pot_values[0]
            else:
                data[sample][loc.name] = 'IC'
                
#     print("Fin boucle")
#     print("--- %s seconds ---" % (time.time() - start_time))
    
    df = pd.DataFrame.from_dict(data, orient='index',columns=columns)
    
    # We recover the index
    index_list = list(df.index)
    
    index_list.remove("Position")
    index_list.remove("Chromosome")
    index_list.remove("Genome Version")
    
    # We sort it in alphabetical order
    index_list.sort()
    
    # We put back the position information to the beginning
    index_list.insert(0, "Position")
    index_list.insert(0, "Chromosome")
    index_list.insert(0, "Genome Version")
    
    # We replace the index with the one that was formatted
    df = df.reindex(index_list)
    
    # And remove referential info to leave only the sample name
    final_df = df.rename(index=index)
    
    row, col = final_df.shape
    nb_genoids = row - 3
    nb_locus = col - 4
    #print(nb_genoids, nb_locus)
    
    now = datetime.datetime.now()
    filetime = now.strftime("_%m-%d-%Y_%H-%M-%S")
    filename = "genotyping_viewer"+filetime
    thread_name = execute_function_in_thread(final_df.to_csv(path_or_buf=settings.TEMP_FOLDER+filename+".csv", sep=';'), [], {})

    if is_admin:
        is_dataview = False
    else:
        is_dataview = True

    if export_or_display == "1":
        return render(request,template,{'parent_temp':parent_temp, 'title': title, 'n_page':2, 'nb_genoids':nb_genoids, 'nb_locus':nb_locus, 'mode':'sample',
                                        'thread_name':thread_name, 'filename':filename, 'export':True, 'admin':is_admin, 'dataview':is_dataview}) 
    
    else:
        html = final_df.to_html(index=True, border=0, justify='center', classes="pandas-result pandas-result-ind alternate tablesorter", table_id="basictable")
        html = html.split('\n')

#         print("Fin dataframe")
#         print("--- %s seconds ---" % (time.time() - start_time))
           
        return render(request,template,{'parent_temp':parent_temp, 'title': title, 'n_page':2, 'html':html, 'nb_genoids':nb_genoids, 'nb_locus':nb_locus, 'mode':'sample',
                                        'display':'individual', 'thread_name':thread_name, 'filename':filename, 'admin':is_admin, 'dataview':is_dataview})   


def display_matrix_seedlot(request, template, title, locus, genome_version, export_or_display, start_time, is_admin, parent_temp):
    """
      To display genotyping matrix for seedlot mode.
    """
    data = {'Genome Version': {},'Chromosome':{},'Position':{}}
    columns = ['Accession','Experiment','Referential']
    index = {}
    
    for elem in columns:
        data['Genome Version'][elem] = '--'
        data['Chromosome'][elem] = '--'
        data['Position'][elem] = '--'
    
    locus = locus.order_by('locusposition__chromosome', 'locusposition__position')
    
    for loc in locus:
        columns.append(loc.name)
        
        if loc.positions:
            data['Genome Version'][loc.name] = genome_version
            data['Chromosome'][loc.name] = loc.positions[0].chromosome
            data['Position'][loc.name] = '{:,}'.format(loc.positions[0].position).replace(',', ' ')
        else:
            data['Genome Version'][loc.name] = 'Unknown'
            data['Chromosome'][loc.name] = 'Unknown'
            data['Position'][loc.name] = 'Unknown'
        
        gvalues = {}

        # On ajoute les valeurs de genotypage
        for value in loc.genovalues:
            if (value.genotyping_id.sample.seedlot.name,value.referential.name) not in index.keys():
                index[(value.genotyping_id.sample.seedlot.name,value.referential.name)] = value.genotyping_id.sample.seedlot.name
            
            if (value.genotyping_id.sample.seedlot.name,value.referential.name) not in data.keys():
                data[(value.genotyping_id.sample.seedlot.name,value.referential.name)] = {'Accession':value.genotyping_id.sample.seedlot.accession,
                                                                                          'Experiment':value.genotyping_id.experiment,
                                                                                          'Referential':value.referential}
                
            if (value.genotyping_id.sample.seedlot.name,value.referential.name) in gvalues.keys():
                if (value.genotyping_id.name,value.referential) in gvalues[(value.genotyping_id.sample.seedlot.name,value.referential.name)].keys():
                    gvalues[(value.genotyping_id.sample.seedlot.name,value.referential.name)][(value.genotyping_id.name,value.referential)].append(value)
                else:
                    gvalues[(value.genotyping_id.sample.seedlot.name,value.referential.name)][(value.genotyping_id.name,value.referential)] = [value]
            else:
                gvalues[(value.genotyping_id.sample.seedlot.name,value.referential.name)] = {}
                gvalues[(value.genotyping_id.sample.seedlot.name,value.referential.name)][(value.genotyping_id.name,value.referential)] = [value]
                
        
        for seedlot, genoid_dict in gvalues.items():
            list_pot_values = []
            for genoid, genovalues in genoid_dict.items():
                if len(genovalues) == 1:
                    if genovalues[0].allele.code_allele == "NA":
                        if genovalues[0].allele.code_allele not in list_pot_values:
                            list_pot_values.append(genovalues[0].allele.code_allele)
                    elif genovalues[0].frequency != 1.0:
                        if genovalues[0].allele.code_allele + "/" not in list_pot_values:
                            list_pot_values.append(genovalues[0].allele.code_allele + "/")
                    else:
                        if genovalues[0].allele.code_allele + "/" + genovalues[0].allele.code_allele not in list_pot_values:
                            list_pot_values.append(genovalues[0].allele.code_allele + "/" + genovalues[0].allele.code_allele)
                elif len(genovalues) > 1:
                    w_val = genovalues[0].allele.code_allele
                    del genovalues[0]
                    for gval in genovalues:
                        w_val = w_val + "/" + gval.allele.code_allele
                    if w_val not in list_pot_values:
                        list_pot_values.append(w_val)
                    
            if len(list_pot_values) == 1:
                data[seedlot][loc.name] = list_pot_values[0]
            elif len(list_pot_values) == 2 and 'NA' in list_pot_values:
                list_pot_values.remove('NA')
                data[seedlot][loc.name] = list_pot_values[0]
            else:
                data[seedlot][loc.name] = 'IC'
                
#     print("Fin boucle")
#     print("--- %s seconds ---" % (time.time() - start_time))
    
    df = pd.DataFrame.from_dict(data, orient='index',columns=columns)
    
    # We recover the index
    index_list = list(df.index)
    
    index_list.remove("Position")
    index_list.remove("Chromosome")
    index_list.remove("Genome Version")
    
    # We sort it in alphabetical order
    index_list.sort()
    
    # We put back the position information to the beginning
    index_list.insert(0, "Position")
    index_list.insert(0, "Chromosome")
    index_list.insert(0, "Genome Version")
    
    # We replace the index with the one that was formatted
    df = df.reindex(index_list)
    
    # And remove referential info to leave only the seedlot name
    final_df = df.rename(index=index)
    
    row, col = final_df.shape
    nb_genoids = row - 3
    nb_locus = col - 3
    #print(nb_genoids, nb_locus)
    
    now = datetime.datetime.now()
    filetime = now.strftime("_%m-%d-%Y_%H-%M-%S")
    filename = "genotyping_viewer"+filetime
    thread_name = execute_function_in_thread(final_df.to_csv(path_or_buf=settings.TEMP_FOLDER+filename+".csv", sep=';'), [], {})
    
    if is_admin:
        is_dataview = False
    else:
        is_dataview = True
    
    if export_or_display == "1":
        return render(request,template,{'parent_temp':parent_temp, 'title': title, 'n_page':2, 'nb_genoids':nb_genoids, 'nb_locus':nb_locus, 'mode':'seedlot',
                                        'thread_name':thread_name, 'filename':filename, 'export':True, 'admin':is_admin, 'dataview':is_dataview})
    
    else:
        html = final_df.to_html(index=True, border=0, justify='center', classes="pandas-result pandas-result-ind alternate tablesorter", table_id="basictable")
        html = html.split('\n')

#         print("Fin dataframe")
#         print("--- %s seconds ---" % (time.time() - start_time))
           
        return render(request,template,{'parent_temp':parent_temp, 'title': title, 'n_page':2, 'html':html, 'nb_genoids':nb_genoids, 'nb_locus':nb_locus, 'mode':'seedlot',
                                        'display':'individual', 'thread_name':thread_name, 'filename':filename, 'admin':is_admin, 'dataview':is_dataview})


def display_matrix_accession(request, template, title, locus, genome_version, export_or_display, start_time, is_admin, parent_temp):
    """
      To display genotyping matrix for accession mode.
    """
    data = {'Genome Version': {},'Chromosome':{},'Position':{}}
    columns = ['Experiment','Referential']
    index = {}
    
    for elem in columns:
        data['Genome Version'][elem] = '--'
        data['Chromosome'][elem] = '--'
        data['Position'][elem] = '--'
    
    locus = locus.order_by('locusposition__chromosome', 'locusposition__position')
    
    for loc in locus:
        columns.append(loc.name)
        
        if loc.positions:
            data['Genome Version'][loc.name] = genome_version
            data['Chromosome'][loc.name] = loc.positions[0].chromosome
            data['Position'][loc.name] = '{:,}'.format(loc.positions[0].position).replace(',', ' ')
        else:
            data['Genome Version'][loc.name] = 'Unknown'
            data['Chromosome'][loc.name] = 'Unknown'
            data['Position'][loc.name] = 'Unknown'
        
        gvalues = {}

        # On ajoute les valeurs de genotypage
        for value in loc.genovalues:
            if (value.genotyping_id.sample.seedlot.accession.name,value.referential.name) not in index.keys():
                index[(value.genotyping_id.sample.seedlot.accession.name,value.referential.name)] = value.genotyping_id.sample.seedlot.accession.name
            
            if (value.genotyping_id.sample.seedlot.accession.name,value.referential.name) not in data.keys():
                data[(value.genotyping_id.sample.seedlot.accession.name,value.referential.name)] = {'Experiment':value.genotyping_id.experiment,
                                                                                                    'Referential':value.referential}
                
            if (value.genotyping_id.sample.seedlot.accession.name,value.referential.name) in gvalues.keys():
                if (value.genotyping_id.name,value.referential) in gvalues[(value.genotyping_id.sample.seedlot.accession.name,value.referential.name)].keys():
                    gvalues[(value.genotyping_id.sample.seedlot.accession.name,value.referential.name)][(value.genotyping_id.name,value.referential)].append(value)
                else:
                    gvalues[(value.genotyping_id.sample.seedlot.accession.name,value.referential.name)][(value.genotyping_id.name,value.referential)] = [value]
            else:
                gvalues[(value.genotyping_id.sample.seedlot.accession.name,value.referential.name)] = {}
                gvalues[(value.genotyping_id.sample.seedlot.accession.name,value.referential.name)][(value.genotyping_id.name,value.referential)] = [value]
                
        
        for accession, genoid_dict in gvalues.items():
            list_pot_values = []
            for genoid, genovalues in genoid_dict.items():
                if len(genovalues) == 1:
                    if genovalues[0].allele.code_allele == "NA":
                        if genovalues[0].allele.code_allele not in list_pot_values:
                            list_pot_values.append(genovalues[0].allele.code_allele)
                    elif genovalues[0].frequency != 1.0:
                        if genovalues[0].allele.code_allele + "/" not in list_pot_values:
                            list_pot_values.append(genovalues[0].allele.code_allele + "/")
                    else:
                        if genovalues[0].allele.code_allele + "/" + genovalues[0].allele.code_allele not in list_pot_values:
                            list_pot_values.append(genovalues[0].allele.code_allele + "/" + genovalues[0].allele.code_allele)
                elif len(genovalues) > 1:
                    w_val = genovalues[0].allele.code_allele
                    del genovalues[0]
                    for gval in genovalues:
                        w_val = w_val + "/" + gval.allele.code_allele
                    if w_val not in list_pot_values:
                        list_pot_values.append(w_val)
                    
            if len(list_pot_values) == 1:
                data[accession][loc.name] = list_pot_values[0]
            elif len(list_pot_values) == 2 and 'NA' in list_pot_values:
                list_pot_values.remove('NA')
                data[accession][loc.name] = list_pot_values[0]
            else:
                data[accession][loc.name] = 'IC'
                
#     print("Fin boucle")
#     print("--- %s seconds ---" % (time.time() - start_time))
    
    df = pd.DataFrame.from_dict(data, orient='index',columns=columns)
    
    # We recover the index
    index_list = list(df.index)
    
    index_list.remove("Position")
    index_list.remove("Chromosome")
    index_list.remove("Genome Version")
    
    # We sort it in alphabetical order
    index_list.sort()
    
    # We put back the position information to the beginning
    index_list.insert(0, "Position")
    index_list.insert(0, "Chromosome")
    index_list.insert(0, "Genome Version")
    
    # We replace the index with the one that was formatted
    df = df.reindex(index_list)
    
    # And remove referential info to leave only the accession name
    final_df = df.rename(index=index)
    
    row, col = final_df.shape
    nb_genoids = row - 3
    nb_locus = col - 2
    #print(nb_genoids, nb_locus)
    
    now = datetime.datetime.now()
    filetime = now.strftime("_%m-%d-%Y_%H-%M-%S")
    filename = "genotyping_viewer"+filetime
    thread_name = execute_function_in_thread(final_df.to_csv(path_or_buf=settings.TEMP_FOLDER+filename+".csv", sep=';'), [], {})
    
    if is_admin:
        is_dataview = False
    else:
        is_dataview = True
    
    if export_or_display == "1":
        return render(request,template,{'parent_temp':parent_temp, 'title': title, 'n_page':2, 'nb_genoids':nb_genoids, 'nb_locus':nb_locus, 'mode':'accession',
                                        'thread_name':thread_name, 'filename':filename, 'export':True, 'admin':is_admin, 'dataview':is_dataview})
    
    else:
        html = final_df.to_html(index=True, border=0, justify='center', classes="pandas-result pandas-result-ind alternate tablesorter", table_id="basictable")
        html = html.split('\n')

#         print("Fin dataframe")
#         print("--- %s seconds ---" % (time.time() - start_time))
           
        return render(request,template,{'parent_temp':parent_temp, 'title': title, 'n_page':2, 'html':html, 'nb_genoids':nb_genoids, 'nb_locus':nb_locus, 'mode':'accession',
                                        'display':'individual', 'thread_name':thread_name, 'filename':filename, 'admin':is_admin, 'dataview':is_dataview})


def display_frequencies_genotyping_id(request, template, title, locus, genome_version, export_or_display, start_time, is_admin, parent_temp):
    """
      To display flat matrix with frequencies for genotyping_id mode.
    """
    print("frequence genoid")
                    
    columns = ['Genotyping ID','Accession','Seedlot','Sample','Experiment','Referential','Locus','Genome Version','Chromosome','Position','Value','Frequency']
    data = []
    
    nb_genoids_list = []
    nb_locus_list = []
    
    for loc in locus:
        
        if loc.positions:
            loc_genome_version = genome_version
            chromosome = str(loc.positions[0].chromosome)
            position = '{:,}'.format(loc.positions[0].position).replace(',', ' ')  
        else:
            loc_genome_version = 'Unknown'
            chromosome = 'Unknown'
            position = 'Unknown'
            
        if loc.genovalues and loc.id not in nb_locus_list:
            nb_locus_list.append(loc.id)
        
        # On ajoute les valeurs de genotypage
        for value in loc.genovalues:
            if value.genotyping_id not in nb_genoids_list: 
                nb_genoids_list.append(value.genotyping_id)
                
            if value.frequency == None or value.allele.code_allele == 'NA':
                frequency = 'NA'
            else:
                frequency = value.frequency
            
            data.append([value.genotyping_id.name,
                         value.genotyping_id.sample.seedlot.accession,
                         value.genotyping_id.sample.seedlot,
                         value.genotyping_id.sample,
                         value.genotyping_id.experiment,
                         value.referential.name,
                         loc.name,
                         loc_genome_version,
                         chromosome,
                         position,
                         value.allele.code_allele,
                         frequency])
            
#     print("Fin boucle")
#     print("--- %s seconds ---" % (time.time() - start_time))
    
    df = pd.DataFrame(data, columns=columns)
    df.sort_values(by=['Genotyping ID', 'Referential', 'Locus'], inplace=True)
    df.set_index('Genotyping ID', inplace=True)
    
    now = datetime.datetime.now()
    filetime = now.strftime("_%m-%d-%Y_%H-%M-%S")
    filename = "genotyping_viewer"+filetime
    thread_name = execute_function_in_thread(df.to_csv(path_or_buf=settings.TEMP_FOLDER+filename+".csv", sep=';'), [], {})
    
    nb_genoids = len(nb_genoids_list)
    nb_locus = len(nb_locus_list)
    
    if is_admin:
        is_dataview = False
    else:
        is_dataview = True
    
    if export_or_display == "1":
        return render(request,template,{'parent_temp':parent_temp, 'title': title, 'n_page':2, 'nb_genoids':nb_genoids, 'nb_locus':nb_locus, 'mode':'genotyping_ID',
                                        'thread_name':thread_name, 'filename':filename, 'export':True, 'admin':is_admin, 'dataview':is_dataview})
    
    else:
        html = df.to_html(index=True, border=0, justify='center', classes="pandas-result pandas-result-freq alternate tablesorter", table_id="basictable")
        html = html.split('\n')
        
#         print("Fin dataframe")
#         print("--- %s seconds ---" % (time.time() - start_time))
        
        return render(request,template,{'parent_temp':parent_temp, 'title': title, 'n_page':2, 'html':html, 'nb_genoids':nb_genoids, 'nb_locus':nb_locus, 'mode':'genotyping_ID',
                                        'display':'frequencies', 'thread_name':thread_name, 'filename':filename, 'admin':is_admin, 'dataview':is_dataview})


def display_frequencies_sample(request, template, title, locus, genome_version, export_or_display, start_time, is_admin, parent_temp):
    """
      To display flat matrix with frequencies for sample mode.
    """
    print("frequence sample")
                    
    allele_codes = get_allele_codes(locus)
    
    columns = ['Sample','Accession','Seedlot','Experiment','Referential','Locus','Genome Version','Chromosome','Position','Value','Frequency','Standard deviation','Count']
    data = []
    
    nb_genoids_list = []
    nb_locus_list = []
    
    for loc in locus:
        
        if loc.positions:
            loc_genome_version = genome_version
            chromosome = str(loc.positions[0].chromosome)
            position = '{:,}'.format(loc.positions[0].position).replace(',', ' ')
        else:
            loc_genome_version = 'Unknown'
            chromosome = 'Unknown'
            position = 'Unknown'
        
        if loc.genovalues and loc.id not in nb_locus_list:
            nb_locus_list.append(loc.id)
            
        infos = {'Locus':loc.name,
                 'Genome Version':loc_genome_version,
                 'Chromosome':chromosome,
                 'Position':position}
        
        gvalues = {}
        # On ajoute les valeurs de genotypage dans un dict en fonction du genoid et regroupe pour le sample
        for value in loc.genovalues:
            if (value.genotyping_id.sample.name,value.referential) not in infos.keys():
                infos[(value.genotyping_id.sample.name,value.referential)] = {'Accession':value.genotyping_id.sample.seedlot.accession,
                                                                              'Seedlot':value.genotyping_id.sample.seedlot,
                                                                              'Experiment':value.genotyping_id.experiment,
                                                                              'Referential':value.referential.name}
            
            if value.genotyping_id not in nb_genoids_list: 
                nb_genoids_list.append(value.genotyping_id)
            
            if (value.genotyping_id.sample.name,value.referential) in gvalues.keys():
                if (value.genotyping_id.name,value.referential) in gvalues[(value.genotyping_id.sample.name,value.referential)].keys():
                    gvalues[(value.genotyping_id.sample.name,value.referential)][(value.genotyping_id.name,value.referential)].append(value)
                else:
                    gvalues[(value.genotyping_id.sample.name,value.referential)][(value.genotyping_id.name,value.referential)] = [value]
            else:
                gvalues[(value.genotyping_id.sample.name,value.referential)] = {}
                gvalues[(value.genotyping_id.sample.name,value.referential)][(value.genotyping_id.name,value.referential)] = [value]
            
        # Pour chaque sample
        for sample, genoid_dict in gvalues.items():
            
            dict_allele, dict_nb, nb_genoids, nb_na = get_genoids_genovalues(genoid_dict)

            #On boucle dans les valeurs d'allele specifiees dans allele_codes  
            
            if dict_allele: #On a des donnees pour ce sample
                        
                for allele in allele_codes:
                    # On reporte les compte d'individu homozygote dans le dictionnaire qui compte les individus au niveau allele rempli cote heterozygote
                    nb_indiv = 0
                    nb_indiv_na = 0
                    if allele in dict_nb.keys():
                        nb_indiv = dict_nb[allele][0] + nb_genoids
                        nb_indiv_na = dict_nb[allele][1] + nb_na
                    else:
                        nb_indiv = nb_genoids
                        nb_indiv_na = nb_na
                    
                    if allele in dict_allele.keys(): # si on a des donnees pour cet allele
                        mean_freq, stdr_dev = get_mean_and_stdev_values(dict_allele, allele, nb_indiv)
                              
                        data.append([sample[0],
                                     infos[sample]['Accession'],
                                     infos[sample]['Seedlot'],
                                     infos[sample]['Experiment'],
                                     infos[sample]['Referential'],
                                     infos['Locus'],
                                     infos['Genome Version'],
                                     infos['Chromosome'],
                                     infos['Position'],
                                     allele,
                                     mean_freq,
                                     stdr_dev,
                                     str(nb_indiv)+" / "+str(nb_indiv_na)])
                          
                    else: # Si on a pas de donnees pour cet allele on inscrit la ligne a 0
                        data.append([sample[0],
                                     infos[sample]['Accession'],
                                     infos[sample]['Seedlot'],
                                     infos[sample]['Experiment'],
                                     infos[sample]['Referential'],
                                     infos['Locus'],
                                     infos['Genome Version'],
                                     infos['Chromosome'],
                                     infos['Position'],
                                     allele,
                                     0.0,
                                     0.0,
                                     str(nb_indiv)+" / "+str(nb_indiv_na)])
            
            else: #Si pas de donnees pour le sample
                if nb_genoids == 0 and nb_na != 0: # On regarde si il y avait des donnees NA, si oui on affiche une ligne avec le compte
                    data.append([sample[0],
                                 infos[sample]['Accession'],
                                 infos[sample]['Seedlot'],
                                 infos[sample]['Experiment'],
                                 infos[sample]['Referential'],
                                 infos['Locus'],
                                 infos['Genome Version'],
                                 infos['Chromosome'],
                                 infos['Position'],
                                 'NA',
                                 'NA',
                                 'NA',
                                 "0 / "+str(nb_na)])
                    
                    

    
#     print("Fin boucle")
#     print("--- %s seconds ---" % (time.time() - start_time))
    
    with pd.option_context("display.precision", 3):
        df = pd.DataFrame(data, columns=columns)
        df.sort_values(by=['Sample', 'Referential', 'Locus'], inplace=True)
        df.set_index('Sample', inplace=True)
        
        now = datetime.datetime.now()
        filetime = now.strftime("_%m-%d-%Y_%H-%M-%S")
        filename = "genotyping_viewer"+filetime
        thread_name = execute_function_in_thread(df.to_csv(path_or_buf=settings.TEMP_FOLDER+filename+".csv", sep=';'), [], {})
    
    nb_genoids = len(nb_genoids_list)
    nb_locus = len(nb_locus_list)
    
    if is_admin:
        is_dataview = False
    else:
        is_dataview = True
    
    if export_or_display == "1":
        return render(request,template,{'parent_temp':parent_temp, 'title': title, 'n_page':2, 'nb_genoids':nb_genoids, 'nb_locus':nb_locus, 'mode':'sample',
                                        'thread_name':thread_name, 'filename':filename, 'export':True, 'admin':is_admin, 'dataview':is_dataview})
        
    else:
        with pd.option_context("display.precision", 3):
            html = df.to_html(index=True, border=0, justify='center', classes="pandas-result pandas-result-freq alternate tablesorter", table_id="basictable")
            html = html.split('\n')
        
#         print("Fin dataframe")
#         print("--- %s seconds ---" % (time.time() - start_time))
        
        return render(request,template,{'parent_temp':parent_temp, 'title': title, 'n_page':2, 'html':html, 'nb_genoids':nb_genoids, 'nb_locus':nb_locus, 'mode':'sample',
                                        'display':'frequencies', 'thread_name':thread_name, 'filename':filename, 'admin':is_admin, 'dataview':is_dataview})


def display_frequencies_seedlot(request, template, title, locus, genome_version, export_or_display, start_time, is_admin, parent_temp):
    """
      To display flat matrix with frequencies for seedlot mode.
    """
    print("frequence seedlot")
                    
    allele_codes = get_allele_codes(locus)
    
    columns = ['Seedlot','Accession','Experiment','Referential','Locus','Genome Version','Chromosome','Position','Value','Frequency','Standard deviation','Count']
    data = []
    
    nb_genoids_list = []
    nb_locus_list = []
    
    for loc in locus:
        
        if loc.positions:
            loc_genome_version = genome_version
            chromosome = str(loc.positions[0].chromosome)
            position = '{:,}'.format(loc.positions[0].position).replace(',', ' ')
        else:
            loc_genome_version = 'Unknown'
            chromosome = 'Unknown'
            position = 'Unknown'
            
        if loc.genovalues and loc.id not in nb_locus_list:
            nb_locus_list.append(loc.id)
            
        infos = {'Locus':loc.name,
                 'Genome Version':loc_genome_version,
                 'Chromosome':chromosome,
                 'Position':position}
        
        gvalues = {}
        # On ajoute les valeurs de genotypage dans un dict en fonction du genoid et regroupe pour le seedlot
        for value in loc.genovalues:
            if (value.genotyping_id.sample.seedlot.name,value.referential) not in infos.keys():
                infos[(value.genotyping_id.sample.seedlot.name,value.referential)] = {'Accession':value.genotyping_id.sample.seedlot.accession,
                                                                                      'Experiment':value.genotyping_id.experiment,
                                                                                      'Referential':value.referential.name}
            
            if value.genotyping_id not in nb_genoids_list: 
                nb_genoids_list.append(value.genotyping_id)
            
            if (value.genotyping_id.sample.seedlot.name,value.referential) in gvalues.keys():
                if (value.genotyping_id.name,value.referential) in gvalues[(value.genotyping_id.sample.seedlot.name,value.referential)].keys():
                    gvalues[(value.genotyping_id.sample.seedlot.name,value.referential)][(value.genotyping_id.name,value.referential)].append(value)
                else:
                    gvalues[(value.genotyping_id.sample.seedlot.name,value.referential)][(value.genotyping_id.name,value.referential)] = [value]
            else:
                gvalues[(value.genotyping_id.sample.seedlot.name,value.referential)] = {}
                gvalues[(value.genotyping_id.sample.seedlot.name,value.referential)][(value.genotyping_id.name,value.referential)] = [value]
            
        
        # Pour chaque seedlot
        for seedlot, genoid_dict in gvalues.items():
            
            dict_allele, dict_nb, nb_genoids, nb_na = get_genoids_genovalues(genoid_dict)
                
            #On boucle dans les valeurs d'allele specifiees dans allele_codes
            
            if dict_allele: #On a des donnees pour ce seedlot
                
                for allele in allele_codes:
                    # On reporte les compte d'individu homozygote dans le dictionnaire qui compte les individus au niveau allele rempli cote heterozygote
                    nb_indiv = 0
                    nb_indiv_na = 0
                    if allele in dict_nb.keys():
                        nb_indiv = dict_nb[allele][0] + nb_genoids
                        nb_indiv_na = dict_nb[allele][1] + nb_na
                    else:
                        nb_indiv = nb_genoids
                        nb_indiv_na = nb_na
                        
                    if allele in dict_allele.keys(): # si on a des donnees pour cet allele
                        mean_freq, stdr_dev = get_mean_and_stdev_values(dict_allele, allele, nb_indiv)
                            
                        data.append([seedlot[0],
                                     infos[seedlot]['Accession'],
                                     infos[seedlot]['Experiment'],
                                     infos[seedlot]['Referential'],
                                     infos['Locus'],
                                     infos['Genome Version'],
                                     infos['Chromosome'],
                                     infos['Position'],
                                     allele,
                                     mean_freq,
                                     stdr_dev,
                                     str(nb_indiv)+" / "+str(nb_indiv_na)])
                        
                    else: # Si on a pas de donnees pour cet allele on inscrit la ligne a 0
                        data.append([seedlot[0],
                                     infos[seedlot]['Accession'],
                                     infos[seedlot]['Experiment'],
                                     infos[seedlot]['Referential'],
                                     infos['Locus'],
                                     infos['Genome Version'],
                                     infos['Chromosome'],
                                     infos['Position'],
                                     allele,
                                     0.0,
                                     0.0,
                                     str(nb_indiv)+" / "+str(nb_indiv_na)])
                    
            else: #Si pas de donnees pour le seedlot
                if nb_genoids == 0 and nb_na != 0: # On regarde si il y avait des donnees NA, si oui on affiche une ligne avec le compte
                    data.append([seedlot[0],
                                 infos[seedlot]['Accession'],
                                 infos[seedlot]['Experiment'],
                                 infos[seedlot]['Referential'],
                                 infos['Locus'],
                                 infos['Genome Version'],
                                 infos['Chromosome'],
                                 infos['Position'],
                                 'NA',
                                 'NA',
                                 'NA',
                                 "0 / "+str(nb_na)])
                
                
#     print("Fin boucle")
#     print("--- %s seconds ---" % (time.time() - start_time))
    
    with pd.option_context("display.precision", 3):
        df = pd.DataFrame(data, columns=columns)
        df.sort_values(by=['Seedlot', 'Referential', 'Locus'], inplace=True)
        df.set_index('Seedlot', inplace=True)
        
        now = datetime.datetime.now()
        filetime = now.strftime("_%m-%d-%Y_%H-%M-%S")
        filename = "genotyping_viewer"+filetime
        thread_name = execute_function_in_thread(df.to_csv(path_or_buf=settings.TEMP_FOLDER+filename+".csv", sep=';'), [], {})
    
    nb_genoids = len(nb_genoids_list)
    nb_locus = len(nb_locus_list)
    
    if is_admin:
        is_dataview = False
    else:
        is_dataview = True
    
    if export_or_display == "1":
        return render(request,template,{'parent_temp':parent_temp, 'title': title, 'n_page':2, 'nb_genoids':nb_genoids, 'nb_locus':nb_locus, 'mode':'seedlot',
                                        'thread_name':thread_name, 'filename':filename, 'export':True, 'admin':is_admin, 'dataview':is_dataview})
        
    else:
        with pd.option_context("display.precision", 3):
            html = df.to_html(index=True, border=0, justify='center', classes="pandas-result pandas-result-freq alternate tablesorter", table_id="basictable")
            html = html.split('\n')
    
#         print("Fin dataframe")
#         print("--- %s seconds ---" % (time.time() - start_time))
        
        return render(request,template,{'parent_temp':parent_temp, 'title': title, 'n_page':2, 'html':html, 'nb_genoids':nb_genoids, 'nb_locus':nb_locus, 'mode':'seedlot',
                                        'display':'frequencies', 'thread_name':thread_name, 'filename':filename, 'admin':is_admin, 'dataview':is_dataview})

def display_frequencies_accession(request, template, title, locus, genome_version, export_or_display, start_time, is_admin, parent_temp):
    """
      To display flat matrix with frequencies for accession mode.
    """
    print("frequence accession")
                    
    allele_codes = get_allele_codes(locus)
    
    columns = ['Accession','Experiment','Referential','Locus','Genome Version','Chromosome','Position','Value','Frequency','Standard deviation','Count']
    data = []
    
    nb_genoids_list = []
    nb_locus_list = []
    
    for loc in locus:
        
        if loc.positions:
            loc_genome_version = genome_version
            chromosome = str(loc.positions[0].chromosome)
            position = '{:,}'.format(loc.positions[0].position).replace(',', ' ')
        else:
            loc_genome_version = 'Unknown'
            chromosome = 'Unknown'
            position = 'Unknown'
        
        if loc.genovalues and loc.id not in nb_locus_list:
            nb_locus_list.append(loc.id)
            
        infos = {'Locus':loc.name,
                 'Genome Version':loc_genome_version,
                 'Chromosome':chromosome,
                 'Position':position}
        
        gvalues = {}
        # On ajoute les valeurs de genotypage dans un dict en fonction du genoid et regroupe pour l'accession
        for value in loc.genovalues:
            if (value.genotyping_id.sample.seedlot.accession.name,value.referential) not in infos.keys():
                infos[(value.genotyping_id.sample.seedlot.accession.name,value.referential)] = {'Accession':value.genotyping_id.sample.seedlot.accession,
                                                                                                'Experiment':value.genotyping_id.experiment,
                                                                                                'Referential':value.referential.name}
            
            if value.genotyping_id not in nb_genoids_list: 
                nb_genoids_list.append(value.genotyping_id)
            
            if (value.genotyping_id.sample.seedlot.accession.name,value.referential) in gvalues.keys():
                if (value.genotyping_id.name,value.referential) in gvalues[(value.genotyping_id.sample.seedlot.accession.name,value.referential)].keys():
                    gvalues[(value.genotyping_id.sample.seedlot.accession.name,value.referential)][(value.genotyping_id.name,value.referential)].append(value)
                else:
                    gvalues[(value.genotyping_id.sample.seedlot.accession.name,value.referential)][(value.genotyping_id.name,value.referential)] = [value]
            else:
                gvalues[(value.genotyping_id.sample.seedlot.accession.name,value.referential)] = {}
                gvalues[(value.genotyping_id.sample.seedlot.accession.name,value.referential)][(value.genotyping_id.name,value.referential)] = [value]
        
        # Pour chaque accession
        for accession, genoid_dict in gvalues.items():
            
            dict_allele, dict_nb, nb_genoids, nb_na = get_genoids_genovalues(genoid_dict)
            
            #On boucle dans les valeurs d'allele specifiees dans allele_codes
            if dict_allele: #On a des donnees pour ce seedlot
                
                for allele in allele_codes:
                    # On reporte les compte d'individu homozygote dans le dictionnaire qui compte les individus au niveau allele rempli cote heterozygote
                    nb_indiv = 0
                    nb_indiv_na = 0
                    if allele in dict_nb.keys():
                        nb_indiv = dict_nb[allele][0] + nb_genoids
                        nb_indiv_na = dict_nb[allele][1] + nb_na
                    else:
                        nb_indiv = nb_genoids
                        nb_indiv_na = nb_na
                    
                    if allele in dict_allele.keys(): # si on a des donnees pour cet allele
                        mean_freq, stdr_dev = get_mean_and_stdev_values(dict_allele, allele, nb_indiv)

                        data.append([accession[0],
                                     infos[accession]['Experiment'],
                                     infos[accession]['Referential'],
                                     infos['Locus'],
                                     infos['Genome Version'],
                                     infos['Chromosome'],
                                     infos['Position'],
                                     allele,
                                     mean_freq,
                                     stdr_dev,
                                     str(nb_indiv)+" / "+str(nb_indiv_na)])
                        
                    else: # Si on a pas de donnees pour cet allele on inscrit la ligne a 0
                        data.append([accession[0],
                                     infos[accession]['Experiment'],
                                     infos[accession]['Referential'],
                                     infos['Locus'],
                                     infos['Genome Version'],
                                     infos['Chromosome'],
                                     infos['Position'],
                                     allele,
                                     0.0,
                                     0.0,
                                     str(nb_indiv)+" / "+str(nb_indiv_na)])
                    
            else: #Si pas de donnees pour l'accession
                if nb_genoids == 0 and nb_na != 0: # On regarde si il y avait des donnees NA, si oui on affiche une ligne avec le compte
                    data.append([accession[0],
                                 infos[accession]['Experiment'],
                                 infos[accession]['Referential'],
                                 infos['Locus'],
                                 infos['Genome Version'],
                                 infos['Chromosome'],
                                 infos['Position'],
                                 'NA',
                                 'NA',
                                 'NA',
                                 "0 / "+str(nb_na)])
                
                
#     print("Fin boucle")
#     print("--- %s seconds ---" % (time.time() - start_time))
    
    with pd.option_context("display.precision", 3):
        df = pd.DataFrame(data, columns=columns)
        df.sort_values(by=['Accession', 'Referential', 'Locus'], inplace=True)
        df.set_index('Accession', inplace=True)
        
        now = datetime.datetime.now()
        filetime = now.strftime("_%m-%d-%Y_%H-%M-%S")
        filename = "genotyping_viewer"+filetime
        thread_name = execute_function_in_thread(df.to_csv(path_or_buf=settings.TEMP_FOLDER+filename+".csv", sep=';'), [], {})
    
    nb_genoids = len(nb_genoids_list)
    nb_locus = len(nb_locus_list)
    
    if is_admin:
        is_dataview = False
    else:
        is_dataview = True
    
    if export_or_display == "1":
        return render(request,template,{'parent_temp':parent_temp, 'title': title, 'n_page':2, 'nb_genoids':nb_genoids, 'nb_locus':nb_locus, 'mode':'accession',
                                        'thread_name':thread_name, 'filename':filename, 'export':True, 'admin':is_admin, 'dataview':is_dataview})
    
    else:
        with pd.option_context("display.precision", 3):
            html = df.to_html(index=True, border=0, justify='center', classes="pandas-result pandas-result-freq alternate tablesorter", table_id="basictable")
            html = html.split('\n')
        
#         print("Fin dataframe")
#         print("--- %s seconds ---" % (time.time() - start_time))
        
        return render(request,template,{'parent_temp':parent_temp, 'title': title, 'n_page':2, 'html':html, 'nb_genoids':nb_genoids, 'nb_locus':nb_locus, 'mode':'accession',
                                        'display':'frequencies', 'thread_name':thread_name, 'filename':filename, 'admin':is_admin, 'dataview':is_dataview})


def get_genoids_genovalues(genoid_dict):
    """
      Retrieval of the frequency of each allele for each genotyping_id, 
      and the number of genotyping_ids for each allele.
    """
    
    dict_allele = {}
    dict_nb = {}
    nb_genoids = 0
    nb_na = 0
    
    # On met les frequences de chaque allele dans un dict
    for genoid, genovalues in genoid_dict.items():
        if len(genovalues) == 1: #Homozygote
            if genovalues[0].allele.code_allele != 'NA' and genovalues[0].frequency:
                nb_genoids += 1 #On prend en compte l'individu dans le compte total
                
                if genovalues[0].allele.code_allele not in dict_allele.keys():
                    dict_allele[genovalues[0].allele.code_allele] = [genovalues[0].frequency]
                else:
                    dict_allele[genovalues[0].allele.code_allele].append(genovalues[0].frequency)
            
            else: #On prend en compte l'individu dans le compte NA
                nb_na += 1
            
        elif len(genovalues) > 1: #Heterozygote
            indiv_na = False
            
            for gval in genovalues: # On regarde si pas d'allele a NA ou de fréquence a NA pour l'individu
                if gval.allele.code_allele == 'NA' or not gval.frequency:
                    indiv_na = True
                    
            if indiv_na: # Si c'est le cas il est compté comme NA
                nb_na += 1
                
            else:
                for gval in genovalues:
                    if gval.allele.code_allele not in dict_allele.keys():
                        dict_allele[gval.allele.code_allele] = [gval.frequency]
                    else:
                        dict_allele[gval.allele.code_allele].append(gval.frequency)
                    
                    if gval.allele.code_allele not in dict_nb.keys():
                        dict_nb[gval.allele.code_allele] = [1,0]
                    else:
                        dict_nb[gval.allele.code_allele][0] += 1
                        
                        
#     print(nb_genoids)
#     print(nb_na)
#     print("allele: ", dict_allele)
#     print("nb:     ", dict_nb)
    
    return dict_allele, dict_nb, nb_genoids, nb_na



def add_zero_values_to_frequency_list(dict_allele, allele, nb_indiv, get_stdev):
    """
      Add zero values to the frequency list for the 
      genotyping_id that have a 0 value for this allele
      in order to calculate the standard deviation.
    """
    stdr_dev = 0.0
    
    if len(dict_allele[allele]) < nb_indiv:
        nb_zero = nb_indiv - len(dict_allele[allele])
        for i in range(0, nb_zero):
            dict_allele[allele].append(0)
        stdr_dev = statistics.stdev(dict_allele[allele])
        
    if get_stdev:
        return dict_allele, stdr_dev
    else:
        return dict_allele


def get_mean_and_stdev_values(dict_allele, allele, nb_indiv):
    """
      Calculate the mean and stdev values of the 
      list of frequencies for the considered allele.
    """
    mean_freq = 0.0
    if len(dict_allele[allele]) > 1:
        for freq in dict_allele[allele]:
            mean_freq += freq
        mean_freq = mean_freq / nb_indiv
        
        # Les valeurs a 0 ne sont pas precisees dans le dict allele, on les ajoute si besoin pour le calcul de l'ecart
        dict_allele = add_zero_values_to_frequency_list(dict_allele, allele, nb_indiv, False)
        
        stdr_dev = statistics.stdev(dict_allele[allele])
    else:
        mean_freq = dict_allele[allele][0] / nb_indiv
        
        # Les valeurs a 0 ne sont pas precisees dans le dict allele, on les ajoute si besoin pour le calcul de l'ecart
        dict_allele, stdr_dev = add_zero_values_to_frequency_list(dict_allele, allele, nb_indiv, True)
        
    return mean_freq, stdr_dev
            

def get_allele_codes(locus):
    """
      To get all the allele in the locus query. 
      Used to display the allelic frequencies for all the allele 
      in concatenation mode of genotyping viewer (in frequency mode).
    """
    allele_codes = []

    for loc in locus:
        for value in loc.genovalues:
            if value.allele.code_allele not in allele_codes:
                allele_codes.append(value.allele.code_allele)
    
    if 'NA' in allele_codes:
        allele_codes.remove('NA')
    
    return allele_codes


#====================================== CROSSVIEW ===================================================

def uploadFile(files):
    if files != None:
        tabline = []
        i=0
        for line in files:
            tabline.append(line.strip())
            i+=1
        return tabline   


def refine_html(request, html, mode):
    v=0
    if mode =="4":
        mode = "Genotyping ID"
    html2=[]
    for i in html:
        if "</tr>" in i or "</th>" in i:
            v=0
        
        if "<th>Referential</th>" in i and mode=="Genotyping ID":
            new_title='<th><center>Referential<br></center></th>'
            html2.append(new_title)
            v=2
        elif "<th>Experiment</th>" in i and mode=="Genotyping ID":
            new_title='<th><center>Experiment<br></center></th>'
            html2.append(new_title)
            v=2
        
#                         if mode == 'Genotyping ID':
        elif "<th>Position</th>" in i or "<th>Chromosome</th>" in i or "<th>Genome Version</th>" in i or "<th>Experiment</th>" in i or "<th>Referential</th>" in i:
            new_th="<th style='background-color:#848484; border:1px solid #848484;'>"+str(i.replace("<th>","").replace("</th>",""))+"</th>"
            html2.append(new_th)
            v=1
        elif v==1: 
            new_td="<td style='background-color:#BDBDBD; border:1px solid #848484;'>"+str(i.replace("<td>","").replace("</td>",""))+"</td>"
            html2.append(new_td)
        elif v==0:
            html2.append(i)
    return html2


def create_list_csv(data_name, list_test_csv, loc, geno, i):
    test_csv=1
    for k in range(len(list_test_csv)):
        if data_name in list_test_csv[k].values():
            num_list = k
            list_test_csv[num_list][loc] = geno[int(i)]['genotyping'][0]['genotype']
            test_csv=0
    if test_csv==1:
        dico_names={'':data_name,loc:geno[int(i)]['genotyping'][0]['genotype']}
        list_test_csv.append(dico_names)


@login_required_on_private_instance
def crossViewer_management(request):
    project=[]
    formproject=DtviewProjectForm()
    formacc = GenoViewerAccessionForm3(request.POST)
    formtrait = PhenoViewerTraitForm(request.POST)
    formenv = PhenoViewerEnvironmentForm2(request.POST)
    formlocus = GenoViewerLocusForm(request.POST)
    formchr = GenoViewerChrForm(request.POST)
    formfile = UploadLocusFileForm(request.POST,request.FILES)
    formclassif = ClassificationViewerForm2(request.POST)
    visumodeform = DataviewGenotypingValuesForm(request.POST)
    print(request.GET)
    if request.method == 'POST' or "or_and" in request.GET:        
        if "or_and" in request.GET:
            projects_or_not = '1'
        else:
            projects_or_not=request.POST['projects_or_not']
            
        if projects_or_not == '0':
            formproject = DtviewProjectForm(request.POST)
            formacc = GenoViewerAccessionForm3()
            formtrait = PhenoViewerTraitForm()
            formenv = PhenoViewerEnvironmentForm2()
            formlocus = GenoViewerLocusForm()
            formchr = GenoViewerChrForm()
            formclassif = ClassificationViewerForm2()
            formfile = UploadLocusFileForm()
            project=request.POST.getlist('project')
            request.session['projects_acc']=project
            formacc.fields["accession"].queryset = Accession.objects.filter(projects__in=project).distinct().order_by('name')
            formtrait.fields["trait"].queryset = Trait.objects.filter(projects__in=project).distinct().order_by('name')
            formenv.fields["environment"].queryset = Environment.objects.filter(projects__in=project).distinct().order_by('name')
            formclassif.fields["classification"].queryset = Classification.objects.filter(projects__in=project).distinct().order_by('name')
            visumodeform = DataviewGenotypingValuesForm()
            projects_or_not=1
            return render(request,'dataview/crossview_viewer.html',{'dataview':True,'formproject':DtviewProjectForm(),"formacc":formacc,"visumodeform":visumodeform,
                                                                    "formtrait":formtrait,"formenv":formenv,"formlocus":formlocus,"formchr":formchr,"formfile":formfile,
                                                                    "formclassif":formclassif,"projects_or_not":projects_or_not,"projects":project})
            
        elif projects_or_not == '1':
            
            if "or_and" in request.GET and "no_search" not in request.GET:
                mode = request.session['mode']
                dico_get={}
                or_and = None
                or_and=request.GET['or_and']
                for i in range(1,11):
                    data="data"+str(i)
                    text_data="text_data"+str(i)
                    if data in request.GET and text_data in request.GET:
                        if request.GET[data] == "experiment" and 'test' not in request.GET:
                            dico_get[data]={"name":request.GET[text_data]}
                        else:
                            dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
                length_dico_get = len(dico_get)
                nb_per_page = 10
                df_test = pd.DataFrame.from_csv(settings.TEMP_FOLDER+'/view_crossview.csv',index_col=None)
                if or_and == 'and':
                    df_empty = 1
                    for dat, dico_type_data in dico_get.items():
                        for data_type, value in dico_type_data.items():
                            if mode != '4':
                                if data_type == 'Accession' or data_type == 'Seed lot' or data_type == 'Sample':
                                    data_type = 'Unnamed: 0'
                            elif data_type == 'Genotyping ID':
                                data_type = 'Unnamed: 0'
                            #data_type = data_type[0].upper()+data_type[1:]

                            #Pour les donnees composee (phenotypage et classif ou les entetes de colonne rassemblent env et trait ou classif et groupe)
                            if '\r' in data_type:
                                data_type = data_type.replace('\r','')

                            if mode == "4":
                                if df_empty == 1:
                                    df_or_and = df_test[data_type].str.contains(value,case=False) | df_test['Unnamed: 0'].str.contains("Genome Version") | df_test['Unnamed: 0'].str.contains("Chromosome") | df_test['Unnamed: 0'].str.contains("Position")
                                    df_empty = 2
                                else:
                                    df_or_and = df_test[data_type].str.contains(value,case=False) & df_or_and
                            else:
                                if df_empty == 1:
                                    df_or_and = df_test[data_type].str.contains(value,case=False) | df_test['Unnamed: 0'].str.contains("Genome Version") | df_test['Unnamed: 0'].str.contains("Chromosome")| df_test['Unnamed: 0'].str.contains("Position") | df_test['Unnamed: 0'].str.contains("Referential") | df_test['Unnamed: 0'].str.contains("Experiment")
                                    df_empty = 2
                                else:
                                    df_or_and = df_test[data_type].str.contains(value,case=False) & df_or_and
                elif or_and == 'or':
                    df_empty = 1
                    for dat, dico_type_data in dico_get.items():
                        for data_type, value in dico_type_data.items():
                            if mode != '4':
                                if data_type == 'Accession' or data_type == 'Seed lot' or data_type == 'Sample':
                                    data_type = 'Unnamed: 0'
                            elif data_type == 'Genotyping ID':
                                data_type = 'Unnamed: 0'
                            #data_type = data_type[0].upper()+data_type[1:]
                            
                            #Pour les donnees composee (phenotypage et classif ou les entetes de colonne rassemblent env et trait ou classif et groupe)
                            if '\r' in data_type:
                                data_type = data_type.replace('\r','')
                            
                            if mode == "4":
                                if df_empty == 1:
                                    df_or_and = df_test[data_type].str.contains(value,case=False) | df_test['Unnamed: 0'].str.contains("Genome Version") | df_test['Unnamed: 0'].str.contains("Chromosome") | df_test['Unnamed: 0'].str.contains("Position")
                                    df_empty = 2
                                else:
                                    df_or_and = df_test[data_type].str.contains(value,case=False) | df_or_and
                            else:
                                if df_empty == 1:
                                    df_or_and = df_test[data_type].str.contains(value,case=False) | df_test['Unnamed: 0'].str.contains("Genome Version") | df_test['Unnamed: 0'].str.contains("Chromosome")| df_test['Unnamed: 0'].str.contains("Position") | df_test['Unnamed: 0'].str.contains("Referential") | df_test['Unnamed: 0'].str.contains("Experiment")
                                    df_empty = 2
                                else:
                                    df_or_and = df_test[data_type].str.contains(value,case=False) | df_or_and
                df = df_test[df_or_and]
                list_attributes = list(df.columns.values)
                df = df.set_index('Unnamed: 0')
                df.index.names = [None] #permet d'avoir la premiere colonne du dataframe en index
                df.to_csv(settings.TEMP_FOLDER+"/view_crossview_refine_search.csv", index=False, encoding='utf-8')  
                html = df.to_html(index=True,justify={'center'})
                html = html.split('\n')
                html2 = refine_html(request, html, mode)
                if mode == '1':
                    nb_rows = len(df)-5
                    mode = 'Accession'
                    nb_marqueurs = len(df.columns)
                elif mode == '2':
                    nb_rows = len(df)-5
                    mode = 'Seed lot'
                    nb_marqueurs = len(df.columns)
                elif mode == '3':
                    nb_rows = len(df)-5
                    mode = 'Sample'
                    nb_marqueurs = len(df.columns)
                elif mode == '4':
                    nb_rows = len(df)-3
                    mode = 'Genotyping ID'
                    nb_marqueurs = len(df.columns)-5
                for i in list_attributes : 
                    if "Unnamed:" in i:
                        list_attributes.remove(i)
                        list_attributes = [mode] + list_attributes
            
                return render(request, 'dataview/dtviewcrossviewvalues.html', {"mode":mode,"nb_marqueurs": nb_marqueurs,"nb_rows":nb_rows,"list_attributes":list_attributes, "html":html2,'projects_or_not':projects_or_not,"or_and":or_and,})  
            
            elif "no_search" in request.GET:
                df = pd.DataFrame.from_csv(settings.TEMP_FOLDER+'/view_crossview.csv',index_col=None)
                list_attributes = list(df.columns.values)
                df = df.set_index('Unnamed: 0')
                df.index.names = [None]
                html = df.to_html(index=True,justify={'center'})
                html = html.split('\n')
                mode = request.session['mode']
                if mode == '1':
                    nb_rows = len(df)-5
                    mode = 'Accession'
                    nb_marqueurs = len(df.columns)
                elif mode == '2':
                    nb_rows = len(df)-5
                    mode = 'Seed lot'
                    nb_marqueurs = len(df.columns)
                elif mode == '3':
                    nb_rows = len(df)-5
                    mode = 'Sample'
                    nb_marqueurs = len(df.columns)
                elif mode == '4':
                    nb_rows = len(df)-3
                    mode = 'Genotyping ID'
                    nb_marqueurs = len(df.columns)-5
                for i in list_attributes : 
                    if "Unnamed:" in i:
                        list_attributes.remove(i)
                        list_attributes = [mode] + list_attributes
                html2 = refine_html(request, html, mode)
                return render(request, 'dataview/dtviewcrossviewvalues.html', {"mode":mode,"nb_marqueurs": nb_marqueurs,"nb_rows":nb_rows,"list_attributes":list_attributes, "html":html2,'projects_or_not':projects_or_not})  
                
            
            
            if "projects_acc" in request.session:
                projects_acc_post=request.session['projects_acc']
            else:
                projects_acc_post = request.POST['project']
            projects_acc=[]
            for g in projects_acc_post:
                if g.isdigit() :
                    projects_acc.append(g)
            
            formacc = GenoViewerAccessionForm3(request.POST)        
   

            d1 = datetime.datetime.now()
            
            if "mode" not in request.POST:
                etape="4"        

                if not formacc.is_valid():
                    message_error="Please enter some accession. You may not have any right on the accessions. Please contact an administrator."
                    return render(request,'dataview/crossview_viewer.html',{"projects_or_not":1,'dataview':True,'message_error':message_error,'formacc':GenoViewerAccessionForm3(), 
                                                                            'formchr':GenoViewerChrForm(),"formtrait":PhenoViewerTraitForm2(), "formenv":PhenoViewerEnvironmentForm2(),'formlocus':GenoViewerLocusForm(), 
                                                                            'formclassif':ClassificationViewerForm2(),'formfile':UploadLocusFileForm(), 'visumodeform':DataviewGenotypingValuesForm()})
                    
                if formacc.is_valid(): #and formtrait.is_valid():
                #cas ou l'on requete sur chromosome, region
                    name = []                
                    if formchr.is_valid() and not request.FILES:
                        chromosome = formchr.cleaned_data['chromosome']
                        genomeversion = formchr.cleaned_data['genome_version']
                        position_minimale = formchr.cleaned_data['position_minimale']
                        position_maximale = formchr.cleaned_data['position_maximale']
                        name = None
                        for i in [",",";","/"]:
                            if i in genomeversion:
                                genomeversion=genomeversion.split(i)
                            if i in chromosome:
                                chromosome=chromosome.split(i)
                        if type(genomeversion)==str and len(genomeversion)==1: 
                            genomeversion=genomeversion.split()
                        elif type(genomeversion)==list:
                            genomeversion=genomeversion
                        else:
                            formacc.fields["accession"].queryset = Accession.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                            formtrait.fields["trait"].queryset = Trait.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                            formenv.fields["environment"].queryset = Environment.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                            formclassif.fields["classification"].queryset = Classification.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                            message_error="Please use the right delimiters for the genome version or select a locus file."
                            return render(request,'dataview/crossview_viewer.html',{"projects_or_not":1,'dataview':True,'message_error':message_error,'formacc':formacc,
                                                                                    "formtrait":formtrait,"formenv":formenv,'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(),
                                                                                    'formclassif':formclassif,'formfile':UploadLocusFileForm(),'visumodeform':DataviewGenotypingValuesForm()})
                        if type(chromosome)==str: 
                            chromosome=chromosome.split()
                        elif type(chromosome)==list:
                            chromosome=chromosome
                        else:
                            formacc.fields["accession"].queryset = Accession.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                            formtrait.fields["trait"].queryset = Trait.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                            formenv.fields["environment"].queryset = Environment.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                            formclassif.fields["classification"].queryset = Classification.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                            message_error="Please use the right delimiters for the chromosome."
                            return render(request,'dataview/crossview_viewer.html',{"projects_or_not":1,'dataview':True,'message_error':message_error,'formacc':formacc,
                                                                                    "formtrait":formtrait,"formenv":formenv,'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(),
                                                                                    'formclassif':formclassif,'formfile':UploadLocusFileForm(),'visumodeform':DataviewGenotypingValuesForm()})
                    #cas ou on choisit une liste de locus
                    elif formlocus.is_valid():
                        chromosome = None
                        genomeversion = None
                        position_minimale = None
                        position_maximale = None
                        name = formlocus.cleaned_data['locus_name']
                        
                    #file upload avec des noms de locus un par ligne
                    elif formfile.is_valid():
                        chromosome = None
                        genomeversion = None
                        position_minimale = None
                        position_maximale = None
                        name = uploadFile(request.FILES['file'])
                        #for line in request.FILES['file']:
                        #    line = line.rstrip('\n')
                        #    name.append(line)
                    else:
                        message_error="Please enter some locus values."
                        formacc.fields["accession"].queryset = Accession.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                        formtrait.fields["trait"].queryset = Trait.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                        formenv.fields["environment"].queryset = Environment.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                        formclassif.fields["classification"].queryset = Classification.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                        return render(request,'dataview/crossview_viewer.html',{"projects_or_not":1,'dataview':True,'message_error':message_error,'formacc':formacc,
                                                                                "formtrait":formtrait,"formenv":formenv,'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(),
                                                                                'formclassif':formclassif,'formfile':UploadLocusFileForm(),'visumodeform':DataviewGenotypingValuesForm()})
                    listaccessions = formacc.cleaned_data['accession']
                    listaccessions_post=[i.name for i in listaccessions]    
#                     listtraits = formtrait.cleaned_data["trait"]
#                     listtraits_post=[i.name for i in listtraits]   
                    mode = request.POST["choose_visualization_mode"]    
                    request.session['mode']=mode
                    export_or_view_in_web_browser = request.POST['export_or_view_in_web_browser']
                    ind_or_freq = request.POST['ind_or_freq']
                
                
#                 if mode != '4':
#                     formacc.fields["accession"].queryset = Accession.objects.filter(projects__in=projects_acc).distinct().order_by('name')
#                     formtrait.fields["trait"].queryset = Trait.objects.filter(projects__in=projects_acc).distinct().order_by('name')
#                     formenv.fields["environment"].queryset = Environment.objects.filter(projects__in=projects_acc).distinct().order_by('name')
#                     formclassif.fields["classification"].queryset = Classification.objects.filter(projects__in=projects_acc).distinct().order_by('name')
#                     message_error='This mode is not available now.'
#                     return render(request,'dataview/crossview_viewer.html',{"projects_or_not":1,'dataview':True,'message_error':message_error,'formacc':formacc,
#                                                                             "formtrait":formtrait,"formenv":formenv,'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(),
#                                                                             'formclassif':formclassif,'formfile':UploadLocusFileForm(),'visumodeform':DataviewGenotypingValuesForm()})
                if formtrait.is_valid():
                    listtraits = formtrait.cleaned_data["trait"]
                    listtraits_post=[i.name for i in listtraits]
                    
                if formenv.is_valid():
                    listenvs = formenv.cleaned_data["environment"]
                    listenvs_post=[i.name for i in listenvs]
                    
                if formclassif.is_valid():
                    listclassif = formclassif.cleaned_data['classification']
                                              
            elif "mode" in request.POST:
                etape=request.POST['etape']
                mode=request.POST['mode']
                if mode=="Accession":
                    mode="1"
                elif mode=="Seed lot":
                    mode="2"
                elif mode=="Sample":
                    mode="3"
                elif mode=="Genotyping ID":
                    mode="4"
                ind_or_freq=request.POST["ind_or_freq"]
                export_or_view_in_web_browser=request.POST["export_or_view_in_web_browser"]
                chromosome_post=request.POST["chromosome"]
                chromosome=[]
                for c in chromosome_post:
                    if c.isdigit() or c==",":
                        chromosome.append(c)
                chromosome=''.join(chromosome)
                chromosome=chromosome.split(',')
                position_minimale=request.POST["position_minimale"]
                position_minimale=int(position_minimale)
                position_maximale=request.POST["position_maximale"]
                position_maximale=int(position_maximale)        
                genomeversion_post=request.POST["genomeversion"]
                genomeversion=[]       
                for g in genomeversion_post:
                    if g.isdigit() or g==',':
                        genomeversion.append(g)
                genomeversion="".join(genomeversion)
                genomeversion=genomeversion.split(',')

                listaccessions_post=request.POST["listaccessions"]
                listaccessions=listaccessions_post.strip('[').strip(']').split("'")
                listtraits_post=request.POST["listtraits"]
                listtraits=listtraits_post.strip('[').strip(']').split("'")
                listenvs_post=request.POST["listenvs"]
                listenvs=listenvs_post.strip('[').strip(']').split("'")

                for i in ['','u',', u']:
                    for j in listaccessions:
                        if i==j:
                            listaccessions.remove(j)
                    for j in listtraits:
                        if i==j:
                            listtraits.remove(j)  
                    for j in listenvs:
                        if i==j:
                            listenvs.remove(j)  
                
                listaccessions=Accession.objects.filter(name__in=listaccessions)
                name=request.POST["name"]
                listtraits=Trait.objects.filter(name__in=listtraits)
                listenvs=Environment.objects.filter(name__in=listenvs)
                
            # niveau individu
            if ind_or_freq == ['1'] or ind_or_freq=="[u'1']" or ind_or_freq=="1":
                # export CSV
                if export_or_view_in_web_browser == '1':
                    void = False
                    no_values=0
                    df_cross, nb_locus, nb_marqueurs = getCrossviewdata(etape, projects_acc,chromosome, position_minimale, position_maximale, genomeversion, listaccessions, name, mode, listtraits, listclassif, listenvs)
                    
                    if df_cross is not None:
                        if len(df_cross)==0:
                            nb_rows = 0
                        else:
                            nb_rows = len(df_cross)-5
                        if mode == '1':
                            headername = "Accession_pheno"
                        elif mode == '2':
                            headername = "Seedlot_pheno"
                        elif mode == '3':
                            headername = "Sample_pheno"
                        elif mode == '4':
                            headername = "Geno_pheno"
                            if len(df_cross)==0:
                                nb_rows = 0
                            else:
                                nb_rows = len(df_cross)-3

                        
                        if df_cross[3:].empty:
                            no_values=1
                        df = df_cross
                    
#                     if df is not None:
                        df.to_csv(settings.TEMP_FOLDER+'/view_crossview.csv')
                        if df.empty or no_values:
                            # envoi du variable au template pour dire qu'il n'y a pas de donnes
                            void = True
                        return render(request, 'dataview/dtviewcrossviewvalues_csv.html', {'dataview':True,'void':void, 'mode':mode})
                    else:
                        formacc.fields["accession"].queryset = Accession.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                        formtrait.fields["trait"].queryset = Trait.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                        formenv.fields["environment"].queryset = Environment.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                        formclassif.fields["classification"].queryset = Classification.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                        message_error='nb loci found: ' + str(nb_locus) + '. Too many loci found: refine the query'
                        return render(request,'dataview/crossview_viewer.html',{"projects_or_not":1,'dataview':True,'message_error':message_error,'formacc':formacc,
                                                                                "formtrait":formtrait,"formenv":formenv,'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(),
                                                                                'formclassif':formclassif,'formfile':UploadLocusFileForm(),'visumodeform':DataviewGenotypingValuesForm()})
                    
                
                else:
                    no_values=0
                    df_cross, nb_locus, nb_marqueurs = getCrossviewdata(etape, projects_acc,chromosome, position_minimale, position_maximale, genomeversion, listaccessions, name, mode, listtraits, listclassif, listenvs)
                    if df_cross is not None:
                        if len(df_cross)==0:
                            nb_rows = 0
                        else:
                            nb_rows = len(df_cross)-5
                        if mode == '1':
                            headername = "Accession_pheno"
                        elif mode == '2':
                            headername = "Seedlot_pheno"
                        elif mode == '3':
                            headername = "Sample_pheno"
                        elif mode == '4':
                            headername = "Geno_pheno"
                            if len(df_cross)==0:
                                nb_rows = 0
                            else:
                                nb_rows = len(df_cross)-3
                        
                        
                        if mode=="1" or mode=="2" or mode=="3":
                            type_table=1
                            if mode=="1":
                                mode="Accession"
                            elif mode=="2":
                                mode="Seedlot"
                            elif mode=="3":
                                mode="Sample"
                        elif mode=="4":
                            mode="Genotyping ID"
                            type_table=0
                        
                        if df_cross[3:].empty:
                            no_values=1
                        df = df_cross
                        
                        df=df.dropna(axis=1, how='all')
                        df=df.fillna(value="--")
                    #if df is not None:
                        list_attributes = list(df.columns.values)
                        list_attributes = [mode] + list_attributes
                    
                        df.to_csv(settings.TEMP_FOLDER+'/view_crossview.csv')
                        html = df.to_html(classes=["tablesorter"])
                        html = html.split('\n')
                        v=0
                        count=0
                        count_geno_pheno=0
                        dico_geno_or_pheno={}
                        html2=[]
                        
                        html2 = refine_html(request, html, mode)
                                 
                        if df.empty:
                            html = None
                            html2 = None
                        d2 = datetime.datetime.now()
                        etape=1
                        
                        if no_values==1:
                            html2=None
                        dico_genovalue={'mode':mode,
                                "ind_or_freq":ind_or_freq,
                                "export_or_view_in_web_browser":export_or_view_in_web_browser,
                                "chromosome":chromosome,
                                "position_minimale":position_minimale,
                                "position_maximale":position_maximale,
                                "genomeversion":genomeversion,
                                "listaccessions":listaccessions_post,
                                "name":name,
                                "html":html2,
                                "type_table":type_table,
                                "projects_or_not":projects_or_not,
                                "project":projects_acc,
                                "listtraits":listtraits_post,
                                "listenvs":listenvs_post,
                                "nb_marqueurs":nb_marqueurs,
                                "nb_rows":nb_rows,
                                "list_attributes":list_attributes,
                                'dataview':True,
                                }
                        return render(request, 'dataview/dtviewcrossviewvalues.html', dico_genovalue)   
                    
                    else:
                        formacc.fields["accession"].queryset = Accession.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                        formtrait.fields["trait"].queryset = Trait.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                        formenv.fields["environment"].queryset = Environment.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                        formclassif.fields["classification"].queryset = Classification.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                        message_error='nb loci found: ' + str(nb_locus) + '. Too many loci found: refine the query'
                        return render(request,'dataview/crossview_viewer.html',{"projects_or_not":1,'dataview':True,'message_error':message_error,'formacc':formacc,
                                                                                "formtrait":formtrait,"formenv":formenv,'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(),
                                                                                'formclassif':formclassif,'formfile':UploadLocusFileForm(),'visumodeform':DataviewGenotypingValuesForm()})   
            # niveau frequency
            elif ind_or_freq == ['2'] or ind_or_freq=="[u'2']" or ind_or_freq=="2":
                formacc.fields["accession"].queryset = Accession.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                formtrait.fields["trait"].queryset = Trait.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                formenv.fields["environment"].queryset = Environment.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                formclassif.fields["classification"].queryset = Classification.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                message_error='Frequency level is not available.'
                return render(request,'dataview/crossview_viewer.html',{"projects_or_not":1,'dataview':True,'message_error':message_error,'formacc':formacc,
                                                                        "formtrait":formtrait,"formenv":formenv,'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(),
                                                                        'formclassif':formclassif,'formfile':UploadLocusFileForm(),'visumodeform':DataviewGenotypingValuesForm()})         
                                    
                                    
    else:
        print("in GET")
        formproject = DtviewProjectForm()
        # formacc = GenoViewerAccessionForm3()
        # formtrait = PhenoViewerTraitForm2()
        # formenv = PhenoViewerEnvironmentForm2()
        # formlocus = GenoViewerLocusForm()   
        # formchr = GenoViewerChrForm()
        # formclassif = ClassificationViewerForm2()
        # formfile = UploadLocusFileForm()
        if request.user and request.user.is_authenticated:
            username = request.user.login
            if User.objects.get(login=username).is_superuser():
                projects = Project.objects.all()
            else:
                username_id=User.objects.get(login=username).id
                projects = Project.objects.filter(users=username_id)
        elif settings.PUBLIC_INSTANCE :
            projects = Project.objects.all()
        else :
            projects = Project.objects.none()
        print(projects)
        formproject.fields['project'].queryset=projects.order_by('name')
        #visumodeform = DataviewGenotypingValuesForm()
    projects_or_not = 0
    return render(request,'dataview/crossview_viewer.html',{'dataview':True,'formproject':formproject,"projects_or_not":projects_or_not})


def getCrossviewdata(etape, projects_acc, chromosome, position_minimale, position_maximale, genomeversion, listaccessions, name, mode, listraits, listclassif, listenvs):

    listSamples = []
    samples = []
    sample_name = ["GENOME VERSION","CHROMOSOME","POSITION","REFERENTIAL","EXPERIMENT",]
    sd_name = ["GENOME VERSION","CHROMOSOME","POSITION","REFERENTIAL","EXPERIMENT",]
    acc_name = ["GENOME VERSION","CHROMOSOME","POSITION","REFERENTIAL","EXPERIMENT",]
    genotypingid_name = ["GENOME VERSION","CHROMOSOME","POSITION",]
    seed_lots = []
    ls_samples = []
    listexperiments = []
    listreferentials = []
    listIdExp = []
    listIdRef = []
    traitslist = [trait for trait in listraits if trait != None] 
    envslist = [env for env in listenvs if env != None]
    # si aucune accession n'est selectionne, ttes le st par defaut
    if not listaccessions:
        listaccessions = Accession.objects.filter(projects__in=projects_acc)


    listexperiments = Experiment.objects.filter(projects__in=projects_acc)
    for exp in listexperiments:
        listIdExp.append(exp.id)
        
    listreferentials = Referential.objects.filter(projects__in=projects_acc)
    for ref in listreferentials:
        listIdRef.append(ref.id)
        
    # recup des id des listes demandees par l'usr pr les request
    for acc in listaccessions:
        acc_name.append(acc.name)
        seed = Seedlot.objects.filter(accession=acc,projects__in=projects_acc)
        if seed != None:
            for sd in seed:
                seed_lots.append(sd)
                sd_name.append(sd.name)
                samples = Sample.objects.filter(seedlot=sd,projects__in=projects_acc)
                if samples != None:
                    for sp in samples:
                        sample_name.append(sp.name)
                        listSamples.append(sp.id)
                        ls_samples.append(sp)
                        
#     if genomeversion==None:
#         genomeversion="3"
    if name!=None:  
        name = [x.decode() for x in name]
        
    if chromosome==None:
        #on recup tous les locus dont le nom se trouve dans le fichier donne
        for i in name:
            listlocus=Locus.objects(name=i)
        listlocus = Locus.objects(name__in=name)
    else:  
        if chromosome != None and position_minimale != None and position_maximale != None:
            listlocus = Locus.objects(Q(positions__chromosome__in=chromosome) \
                & Q(positions__position__gte=position_minimale) \
                & Q(positions__position__lte=position_maximale) \
                & Q(positions__genomeversion__in=genomeversion))
        elif chromosome !=None:
            listlocus = Locus.objects(positions__chromosome__in=chromosome)
    try :        
        listlocus2=listlocus.distinct('name')
    except :
        return None, None, None
    if len(listlocus2) > 300:
        df_pd_test = None
        nb_columns = 0
    
    else:
        locus_names = []
        for locus in listlocus2:    
            locus_names.append(locus)
        listref = []
        i = 0
        dico_tableau={}
        d2 = datetime.datetime.now()

        #TODO MongoClient a remplacer
#         client = MongoClient(settings.MONGODB_DATABASES['default']['host'], settings.MONGODB_DATABASES['default']['port'])
#         db = client[settings.MONGODB_DATABASES['default']['name']]
        list_test_csv=[]
        list2=[]
        num_list=0
        dico2={}
        position_locus_counter=0
        request=[]
        dico_loc={}
        
        try:
            if etape=="1":
                df_pd_test=pd.read_csv(settings.TEMP_FOLDER+'/view_crossview.csv', index_col=0)
                nb_columns=len(locus_names)
                list_column_names=list(df_pd_test.columns.values)
                list_row_names=list(df_pd_test.index.values)
                dico_locus_column_names={}
                df_pd_etape1=pd.DataFrame(index=list_row_names)
                for i in listlocus2:
                    dico_locus_column_names[i]=[]
                    for k in list_column_names:
                        if i == k.replace(' ',''):
                            dico_locus_column_names[i].append(k)
                    if dico_locus_column_names[i]==[]:
                        del dico_locus_column_names[i]
                for key,value in dico_locus_column_names.items():
                    df_pd_etape1[key]=""
                    for v in value:
                        if v==value[-1]:
                            df_pd_etape1[key]=df_pd_etape1[key]+df_pd_test[v]
                        else:
                            df_pd_etape1[key]=df_pd_etape1[key]+df_pd_test[v]+"/"
                        df_pd_etape1.ix["Genome Version",key]=df_pd_test.loc["Genome Version",key]
                        df_pd_etape1.ix["Chromosome",key]=df_pd_test.loc["Chromosome",key]
                        df_pd_etape1.ix["Position",key]=df_pd_test.loc["Position",key]
        
                try:
                    df_pd_etape1=df_pd_etape1.T.sort('Position', ascending=True).T
                    df_pd_etape1=df_pd_etape1.dropna(axis=1, how='all')      
                except:
                    pass  
                return df_pd_etape1, len(listlocus), nb_columns     
        
            elif etape=="0":
                df_pd_etape2=pd.read_csv(settings.TEMP_FOLDER+'/view_crossview.csv', index_col=0)
                nb_columns=len(locus_names)
                try:
                    df_pd_etape2=df_pd_etape2.T.sort('Position', ascending=True).T
                    df_pd_etape2=df_pd_etape2.dropna(axis=1, how='all')  
                except:
                    pass      
                return df_pd_etape2, len(listlocus), nb_columns               
            
        except Exception as e:
            print("\n\n\n===>",e,"\n\n\n")
            pass
        
        #ACCESSION
        if mode != "4":
            dico_acc_geno={}
            list_acc_geno=[]
            name_test_csv=['']   
            geno = ''
            req_position = ''
            requete_geno = ''
            for loc in locus_names:
                req_position=db.locus.find({'$and':[{'positions':{"$exists":True}},{'name':loc}]})
                geno=db.locus.find({"$and": [{'genotyping':{"$ne":[]}}, {'name':loc}]}).distinct('genotyping')
                exp_list=[]
                ref_list=[]
                liste_loc=[loc]
                
                if len(geno)!=0 and req_position.count()!=0:
                    req_position=req_position[0]
                    name_test_csv.append(loc)
                    for i in range(len(geno)):
                        #genotypingID=GenotypingID.objects.get(sample=geno[i]['sample_id'],experiment=geno[i]['genotyping'][0]['experiment_id'],referential=geno[i]['genotyping'][0]['referential_id'])
                        #genotypingID=GenotypingID.objects.filter(sample=geno[i]['sample_id'],experiment=geno[i]['genotyping'][0]['experiment_id'],referential=geno[i]['genotyping'][0]['referential_id'])
                        if position_locus_counter == 0:
                            try:
                                dico_genome_version={'':'Genome Version',loc:req_position['positions'][0]['genomeversion']}
                                dico_chromosome={'':'Chromosome',loc:req_position['positions'][0]['chromosome']}
                                dico_position={'':'Position',loc:req_position['positions'][0]['position']}
                                dico_referential={'':'Referential',loc:Referential.objects.get(id=geno[i]['genotyping'][0]['referential_id']).name}
                                dico_experiment={'':'Experiment',loc:Experiment.objects.get(id=geno[i]['genotyping'][0]['experiment_id']).name}
                                position_locus_counter=1
                                list_test_csv.append(dico_genome_version)
                                list_test_csv.append(dico_chromosome)
                                list_test_csv.append(dico_position)
                                list_test_csv.append(dico_referential)
                                list_test_csv.append(dico_experiment)
                            except Exception as e:
                                pass
                        
                        else:
                            list_test_csv[0][loc]=req_position['positions'][0]['genomeversion']
                            list_test_csv[1][loc]=req_position['positions'][0]['chromosome']
                            list_test_csv[2][loc]=req_position['positions'][0]['position']
                            
                        #if int(geno[i]['sample_id']) in listSamples and int(geno[i]['genotyping'][0]['experiment_id']) in listIdExp and int(geno[i]['genotyping'][0]['referential_id']) in listIdRef :
                        if int(geno[i]['sample_id']) in listSamples and int(geno[i]['genotyping'][0]['experiment_id']) in listIdExp and int(geno[i]['genotyping'][0]['referential_id']) in listIdRef:
                            sample=Sample.objects.get(id=geno[i]['sample_id']).name
                            sample_id=Sample.objects.get(id=geno[i]['sample_id']).id
                            experiment_id=geno[i]['genotyping'][0]['experiment_id']
                            referential_id=geno[i]['genotyping'][0]['referential_id']
                            if experiment_id not in exp_list :
                                exp_list.append(experiment_id) 
                                if len(exp_list)>1:
                                    liste_loc.append(str(liste_loc[-1])+' ')
                            if referential_id not in ref_list :
                                ref_list.append(referential_id) 
                                if len(ref_list)>1:
                                    liste_loc.append(str(liste_loc[-1])+' ')
                            compteur=-1 
                            for j in exp_list:
                                if j == experiment_id : 
                                    numero_locus=compteur
                                compteur = compteur + 1
                            for j in ref_list:
                                if j == referential_id : 
                                    numero_locus=compteur
                                compteur = compteur + 1
                            loc = liste_loc[numero_locus]
                            
                            if loc not in name_test_csv :
                                name_test_csv.append(loc)
                            seedlot=Seedlot.objects.get(sample=sample_id)
                            accession=Accession.objects.get(seedlot=seedlot.id)
                            if accession.name not in dico_acc_geno.keys():
                                dico_acc_geno[accession.name] = ''
                                list_acc_geno.append(accession)
                            test_csv=1
                            list_test_csv[3][loc]=Referential.objects.get(id=referential_id).name
                            list_test_csv[4][loc]=Experiment.objects.get(id=experiment_id).name
                            if mode == "1":
                                create_list_csv(accession, list_test_csv, loc, geno, i)
                            elif mode == "2":
                                create_list_csv(seedlot, list_test_csv, loc, geno, i)
                            elif mode == "3":
                                create_list_csv(sample, list_test_csv, loc, geno, i)
            
            
            if listraits:
                df_pheno=pd.DataFrame()
                
                if not listenvs:
                    env_id = [pheno_val.environment_id for pheno_val in PhenotypicValue.objects.filter(seedlot_id__in=seed_lots,trait__in=listraits).distinct()]
                    listenvs = Environment.objects.filter(id__in=env_id, projects__in=projects_acc).distinct()
                
                for trait in traitslist:
                    #print(trait)
                    for e in listenvs:
                        tr = str(trait.name) +'\n'+ str(e.name)
                        #for acc in listaccessions:
                        for acc in list_acc_geno:
                            values_list_acc = []
                            seedlots = Seedlot.objects.filter(accession=acc,projects__in=projects_acc).distinct()
                            for seed in seedlots:
                                if seed != None:
                                    values_list_sl = []
                                    phenos=PhenotypicValue.objects.filter(seedlot_id=seed.id,trait_id=trait.id, environment_id=e.id)
                                    for pheno in phenos:
                                        values_list_acc.append(str(pheno.value).replace(',', '.'))
                                        values_list_sl.append(str(pheno.value).replace(',', '.'))
                                        sample=Sample.objects.filter(seedlot_id=seed.id)
                                        for sple in sample:     
                                            if mode == "3":  
                                                df_pheno.ix[str(sple.name),tr]=str(pheno.value).replace(',', '.')
                                    if mode == "2":
                                        if values_list_sl != []:
                                            df_pheno.ix[str(seed.name),tr]=' | '.join(values_list_sl)   
                            if mode == "1":
                                if values_list_acc != []:
                                    df_pheno.ix[str(acc.name),tr]=' | '.join(values_list_acc)
                df_pheno = df_pheno.fillna('NA')
                
            if listenvs and not listraits:
                df_pheno=pd.DataFrame()
            
                trait_id = [pheno_val.trait_id for pheno_val in PhenotypicValue.objects.filter(seedlot_id__in=seed_lots,environment__in=listenvs).distinct()]
                traitslist = Trait.objects.filter(id__in=trait_id, projects__in=projects_acc).distinct()
            
                for env in listenvs:
                    #print(env)
                    for t in traitslist:
                        tr = str(t.name) +'\n'+ str(env.name)
                        #for acc in listaccessions:
                        for acc in list_acc_geno:
                            values_list_acc = []
                            seedlots = Seedlot.objects.filter(accession=acc,projects__in=projects_acc).distinct()
                            for seed in seedlots:
                                if seed != None:
                                    values_list_sl = []
                                    phenos=PhenotypicValue.objects.filter(seedlot_id=seed.id,trait_id=t.id, environment_id=env.id)
                                    for pheno in phenos:
                                        values_list_acc.append(str(pheno.value).replace(',', '.'))
                                        values_list_sl.append(str(pheno.value).replace(',', '.'))
                                        sample=Sample.objects.filter(seedlot_id=seed.id)
                                        for sple in sample:
                                            if mode == "3":  
                                                df_pheno.ix[str(sple.name),tr]=str(pheno.value).replace(',', '.')         
                                    if mode == "2":
                                        if values_list_sl != []:
                                            df_pheno.ix[str(seed.name),tr]=' | '.join(values_list_sl)     
                            if mode == "1":
                                if values_list_acc != []:
                                    df_pheno.ix[str(acc.name),tr]=' | '.join(values_list_acc)                        
                df_pheno = df_pheno.fillna('NA')
            
            if listclassif:
                df_classif=pd.DataFrame()
                groups_id = [cv.classe_id for cv in ClasseValue.objects.filter(seedlot_id__in=seed_lots).distinct()]
                #grouplist = Classe.objects.filter(id__in=groups_id, classification_id__in=listclassif).distinct()
            
                for classif in listclassif:
                    grouplist = []
                    grouplist = Classe.objects.filter(id__in=groups_id, classification_id=classif.id).distinct()
                    for g in grouplist:
                        tr = str(classif.name) +'\n'+ str(g.name)
                        #for acc in listaccessions:
                        for acc in list_acc_geno:
                            values_list_acc = []
                            seedlots = Seedlot.objects.filter(accession=acc,projects__in=projects_acc).distinct()
                            for seed in seedlots:
                                if seed != None:
                                    values_list_sl = []
                                    classevals=ClasseValue.objects.filter(seedlot_id=seed.id,classe_id=g.id)
                                    for classeval in classevals:
                                        values_list_acc.append(str(classeval.value).replace(',', '.'))
                                        values_list_sl.append(str(classeval.value).replace(',', '.'))
                                        sample=Sample.objects.filter(seedlot_id=seed.id)
                                        for sple in sample:
                                            if mode == "3":  
                                                    df_classif.ix[str(sple.name),tr]=str(classeval.value).replace(',', '.')
                                    if mode == "2":
                                        if values_list_sl != []:
                                            df_classif.ix[str(seed.name),tr]=' | '.join(values_list_sl)  
                            if mode == "1":
                                if values_list_acc != []:
                                    df_classif.ix[str(acc.name),tr]=' | '.join(values_list_acc)
            
                                
        #GENOTYPINGID
        else: 
            #name_test_csv=['','Accession','Parent M (A)','Parent F (A)','SeedLot','Parent M','Parent F','Parent X','Sample','Experiment','Referential'] 
            name_test_csv=['','Accession','SeedLot','Sample','Experiment','Referential'] 
            dico2={}  
            for loc in locus_names:         
                req_position=db.locus.find({"$and": [{'genotyping':[]},{'positions':{"$exists":True}},{'name':loc}]})
                geno=db.locus.find({"$and": [{'genotyping':{"$ne":[]}}, {'name':loc}]}).distinct('genotyping')
                if len(geno)!=0 and req_position.count()!=0:
                    req_position=req_position[0]
                    name_test_csv.append(loc)
                    for i in range(len(geno)):
                        if position_locus_counter == 0:
                            #dico_genome_version={'':'Genome Version','Accession':'--','Parent M (A)':'--','Parent F (A)':'--','SeedLot':'--','Parent M':'--','Parent F':'--','Parent X':'--','Sample':'--','Experiment':'--','Referential':'--',loc:req_position['positions'][0]['genomeversion']}
                            dico_genome_version={'':'Genome Version','Accession':'--','SeedLot':'--','Sample':'--','Experiment':'--','Referential':'--',loc:req_position['positions'][0]['genomeversion']}
                            list_test_csv.append(dico_genome_version)
                            #dico_chromosome={'':'Chromosome','Accession':'--','Parent M (A)':'--','Parent F (A)':'--','SeedLot':'--','Parent M':'--','Parent F':'--','Parent X':'--','Sample':'--','Experiment':'--','Referential':'--',loc:req_position['positions'][0]['chromosome']}
                            dico_chromosome={'':'Chromosome','Accession':'--','SeedLot':'--','Sample':'--','Experiment':'--','Referential':'--',loc:req_position['positions'][0]['chromosome']}
                            list_test_csv.append(dico_chromosome)
                            #dico_position={'':'Position','Accession':'--','Parent M (A)':'--','Parent F (A)':'--','SeedLot':'--','Parent M':'--','Parent F':'--','Parent X':'--','Sample':'--','Experiment':'--','Referential':'--',loc:req_position['positions'][0]['position']}
                            dico_position={'':'Position','Accession':'--','SeedLot':'--','Sample':'--','Experiment':'--','Referential':'--',loc:req_position['positions'][0]['position']}
                            list_test_csv.append(dico_position)
                            position_locus_counter=1
                        else:
                            list_test_csv[0][loc]=req_position['positions'][0]['genomeversion']
                            list_test_csv[1][loc]=req_position['positions'][0]['chromosome']
                            list_test_csv[2][loc]=req_position['positions'][0]['position']
                        
                        if geno[i]['sample_id'] in listSamples and geno[i]['genotyping'][0]['experiment_id'] in listIdExp and geno[i]['genotyping'][0]['referential_id'] in listIdRef:
                            try:
                                #genotypingID=GenotypingID.objects.get(sample=geno[i]['sample_id'],experiment=geno[i]['genotyping'][0]['experiment_id'],referential=geno[i]['genotyping'][0]['referential_id'])
                                genotypingID=GenotypingID.objects.filter(sample=geno[i]['sample_id'],experiment=geno[i]['genotyping'][0]['experiment_id'],referential=geno[i]['genotyping'][0]['referential_id'])
                                dico={'':genotypingID,'Sample':geno[i]['sample_id'],'Experiment':genotypingID.experiment,'Referential':genotypingID.referential,loc:geno[i]['genotyping'][0]['genotype']}
                                test_csv=1
                                for k in range(len(list_test_csv)):
                                    if genotypingID in list_test_csv[k].values():
                                        num_list = k
                                        list_test_csv[num_list][loc] = geno[i]['genotyping'][0]['genotype']
                                        test_csv=0
                                if test_csv==1:
                                    seedlot=Seedlot.objects.get(sample=geno[i]['sample_id'])
#                                     sl_parent_x = SeedlotRelation.objects.filter(child_id=seedlot.id, parent_gender='X')
#                                     sl_parent_x = [Seedlot.objects.get(id=slr.parent_id).name for slr in sl_parent_x]
#                                     sl_parent_m = SeedlotRelation.objects.filter(child_id=seedlot.id, parent_gender='M')
#                                     sl_parent_m = [Seedlot.objects.get(id=slr.parent_id).name for slr in sl_parent_m]
#                                     sl_parent_f = SeedlotRelation.objects.filter(child_id=seedlot.id, parent_gender='F')
#                                     sl_parent_f = [Seedlot.objects.get(id=slr.parent_id).name for slr in sl_parent_f]
#                                     
                                    accession=Accession.objects.get(seedlot=seedlot.id)
#                                     a_parent_m = AccessionRelation.objects.filter(child_id=accession.id, parent_gender='M')
#                                     a_parent_m = [Accession.objects.get(id=acr.parent_id).name for acr in a_parent_m]
#                                     a_parent_f = AccessionRelation.objects.filter(child_id=accession.id, parent_gender='F')
#                                     a_parent_f = [Accession.objects.get(id=acr.parent_id).name for acr in a_parent_f]
                                    #dico_names={'Accession':accession,'Parent M (A)':' | '.join(a_parent_m),'Parent F (A)':' | '.join(a_parent_f),'SeedLot':seedlot,'Parent M':' | '.join(sl_parent_m),'Parent F':' | '.join(sl_parent_f),'Parent X':' | '.join(sl_parent_x),'':genotypingID,'Sample':Sample.objects.get(id=geno[i]['sample_id']).name,'Experiment':Experiment.objects.get(id=geno[i]['genotyping'][0]['experiment_id']).name, 'Referential':Referential.objects.get(id=geno[i]['genotyping'][0]['referential_id']).name ,loc:geno[i]['genotyping'][0]['genotype']}
                                    dico_names={'Accession':accession,'SeedLot':seedlot,'':genotypingID,'Sample':Sample.objects.get(id=geno[i]['sample_id']).name,'Experiment':Experiment.objects.get(id=geno[i]['genotyping'][0]['experiment_id']).name, 'Referential':Referential.objects.get(id=geno[i]['genotyping'][0]['referential_id']).name ,loc:geno[i]['genotyping'][0]['genotype']}
                                    list_test_csv.append(dico_names)
                            except:
                                genotypingID=GenotypingID.objects.filter(sample=geno[i]['sample_id'],experiment=geno[i]['genotyping'][0]['experiment_id'],referential=geno[i]['genotyping'][0]['referential_id'])
                                test_csv=1
                                for genotyping_num in genotypingID:
                                    dico={'':genotyping_num,'Sample':geno[i]['sample_id'],'Experiment':geno[i]['genotyping'][0]['experiment_id'],'Referential':geno[i]['genotyping'][0]['referential_id'],loc:geno[i]['genotyping'][0]['genotype']}
                                    for k in range(len(list_test_csv)):
                                        if genotyping_num in list_test_csv[k].values():
                                            num_list = k
                                            list_test_csv[num_list][loc] = geno[i]['genotyping'][0]['genotype']
                                            test_csv=0
                                if test_csv==1:
                                    for genotyping_num in genotypingID:
                                        seedlot=Seedlot.objects.get(sample=geno[i]['sample_id'])
#                                         sl_parent_x = SeedlotRelation.objects.filter(child_id=seedlot.id, parent_gender='X')
#                                         sl_parent_x = [Seedlot.objects.get(id=slr.parent_id).name for slr in sl_parent_x]
#                                         sl_parent_m = SeedlotRelation.objects.filter(child_id=seedlot.id, parent_gender='M')
#                                         sl_parent_m = [Seedlot.objects.get(id=slr.parent_id).name for slr in sl_parent_m]
#                                         sl_parent_f = SeedlotRelation.objects.filter(child_id=seedlot.id, parent_gender='F')
#                                         sl_parent_f = [Seedlot.objects.get(id=slr.parent_id).name for slr in sl_parent_f]
                                        
                                        accession=Accession.objects.get(seedlot=seedlot.id)
#                                         a_parent_m = AccessionRelation.objects.filter(child_id=accession.id, parent_gender='M')
#                                         a_parent_m = [Accession.objects.get(id=acr.parent_id).name for acr in a_parent_m]
#                                         a_parent_f = AccessionRelation.objects.filter(child_id=accession.id, parent_gender='F')
#                                         a_parent_f = [Accession.objects.get(id=acr.parent_id).name for acr in a_parent_f]
                                        #dico_names={'Accession':accession,'Parent M (A)':' | '.join(a_parent_m),'Parent F (A)':' | '.join(a_parent_f),'SeedLot':seedlot,'Parent M':' | '.join(sl_parent_m),'Parent F':' | '.join(sl_parent_f),'Parent X':' | '.join(sl_parent_x),'':genotyping_num,'Sample':Sample.objects.get(id=geno[i]['sample_id']).name,'Experiment':genotyping_num.experiment, 'Referential':genotyping_num.referential ,loc:geno[i]['genotyping'][0]['genotype']}
                                        dico_names={'Accession':accession,'SeedLot':seedlot,'':genotyping_num,'Sample':Sample.objects.get(id=geno[i]['sample_id']).name,'Experiment':genotyping_num.experiment, 'Referential':genotyping_num.referential ,loc:geno[i]['genotyping'][0]['genotype']}
                                        list_test_csv.append(dico_names)
            
            if listraits:
                df_pheno=pd.DataFrame()
                
                if not listenvs:
                    env_id = [pheno_val.environment_id for pheno_val in PhenotypicValue.objects.filter(seedlot_id__in=seed_lots,trait__in=listraits).distinct()]
                    listenvs = Environment.objects.filter(id__in=env_id).distinct()
                
                
                sl_id = [pheno_val.seedlot_id for pheno_val in PhenotypicValue.objects.filter(seedlot_id__in=seed_lots,trait__in=listraits).distinct()]
                seedlotlist = Seedlot.objects.filter(id__in=sl_id).distinct()
                for trait in traitslist:
                    #print(trait)
                    for e in listenvs:
                        tr = str(trait.name) +'\n'+ str(e.name)
                        #for seed in seed_lots:
                        for seed in seedlotlist:
                            phenos=PhenotypicValue.objects.filter(seedlot_id=seed.id,trait_id=trait.id, environment_id=e.id)
                            for pheno in phenos:
                                sample=Sample.objects.filter(seedlot_id=seed.id)
                                for sple in sample:
                                    genoid=GenotypingID.objects.filter(sample_id=sple.id)
                                    for geno in genoid:
                                        df_pheno.ix[str(geno),tr]=str(pheno.value).replace(',', '.')

                df_pheno = df_pheno.fillna('NA')
            
            if listenvs and not listraits:
                df_pheno=pd.DataFrame()
            
                trait_id = [pheno_val.trait_id for pheno_val in PhenotypicValue.objects.filter(seedlot_id__in=seed_lots,environment__in=listenvs).distinct()]
                traitslist = Trait.objects.filter(id__in=trait_id).distinct()
                
                sl_id = [pheno_val.seedlot_id for pheno_val in PhenotypicValue.objects.filter(seedlot_id__in=seed_lots,environment__in=listenvs).distinct()]
                seedlotlist = Seedlot.objects.filter(id__in=sl_id).distinct()
            
                for env in listenvs:
                    #print(env)
                    for t in traitslist:
                        tr = str(t.name) +'\n'+ str(env.name)
                        for seed in seedlotlist:
                            phenos=PhenotypicValue.objects.filter(seedlot_id=seed.id,trait_id=t.id, environment_id=env.id)
                            for pheno in phenos:
                                sample=Sample.objects.filter(seedlot_id=seed.id)
                                for sple in sample:
                                    genoid=GenotypingID.objects.filter(sample_id=sple.id)
                                    for geno in genoid:
#                                         print("phenotypage")
                                        df_pheno.ix[str(geno),tr]=str(pheno.value).replace(',', '.')
                df_pheno = df_pheno.fillna('NA')
            
            
            if listclassif:
                df_classif=pd.DataFrame()
                groups_id = [cv.classe_id for cv in ClasseValue.objects.filter(seedlot_id__in=seed_lots).distinct()]
                sl_id = [cv.seedlot_id for cv in ClasseValue.objects.filter(seedlot_id__in=seed_lots).distinct()]
            
                #grouplist = Classe.objects.filter(id__in=groups_id, classification_id__in=listclassif).distinct()
                seedlotlist = Seedlot.objects.filter(id__in=sl_id).distinct()
                for classif in listclassif:
                    grouplist = []
                    grouplist = Classe.objects.filter(id__in=groups_id, classification_id=classif.id).distinct()
                    for g in grouplist:
                        tr = str(classif.name) +'\n'+ str(g.name)
                        for seed in seedlotlist:
                            classevals=ClasseValue.objects.filter(seedlot_id=seed.id,classe_id=g.id)
                            for classeval in classevals:
                                sample=Sample.objects.filter(seedlot_id=seed.id)
                                for sple in sample:
                                    #print(sple)
                                    genoid=GenotypingID.objects.filter(sample_id=sple.id)
                                    for geno in genoid:
#                                         print("classification")
                                        df_classif.ix[str(geno),tr]=str(classeval.value).replace(',', '.')
            
#         print(list_test_csv)    
        
        for i in list_test_csv:        
            for names in name_test_csv:  
                if not names in i:
                    i[names]='--'
        with open(settings.TEMP_FOLDER+'/view_crossview.csv','w') as f:                     
            writer=csv.DictWriter(f, fieldnames=name_test_csv)
            writer.writeheader()
            for i in list_test_csv:
                writer.writerow(i)
        df_pd_test=None       
        try:
            df_pd_test=pd.read_csv(settings.TEMP_FOLDER+'/view_crossview.csv', index_col=0)
            df_pd_test=(df_pd_test.T).sort('Position', ascending=True).T
        except:
            pass

        df_pd_final=df_pd_test  
        
        if listclassif:
#             print(df_classif)
            if not df_classif.empty and not df_pd_final[3:].empty:
                df_pd_final = pd.concat([df_pd_final,df_classif], axis=1, join_axes=[df_pd_final.index])
          
        if listraits or listenvs:
#             print(df_pheno)
            if not df_pheno.empty and not df_pd_final[3:].empty:
                df_pd_final = pd.concat([df_pd_final,df_pheno], axis=1, join_axes=[df_pd_final.index])
            
        d2 = datetime.datetime.now()
        
        
    return df_pd_final, len(listlocus), len(listlocus2)

                                                
@login_required_on_private_instance
def data_card(request):
    username = None
    if request.user and request.user.is_authenticated:
        username = request.user.login
        if User.objects.get(login=username).is_superuser():
            projects = Project.objects.all()
        else:
            username_id=User.objects.get(login=username).id
            projects = Project.objects.filter(users=username_id)
    elif settings.PUBLIC_INSTANCE :
        projects = Project.objects.all()
    else :
        projects = Project.objects.none()
    template="dataview/data_card.html"

    dico_search = {}
    if request.method=="GET":  
        if "search" in request.GET:
            dico_search[request.GET['type_data']]=request.GET['search']
            print(dico_search) 
            
        if 'classification' in request.GET:
            structure = request.GET['classification']
            type_data = 'structure'
              
            try:
                classification = Classification.objects.get(name=structure)
            except:
                return render(request, template, {'error':"ERROR! The {0} {1} doesn't exist.".format("structure",structure)})
            
            if classification.projects.filter(id__in=projects) or User.objects.get(login=username).is_superuser():
                type_data = "Structure"
                seedlot_list=[]
                classes = Classe.objects.filter(classification=classification)
                for classe in classes:
                    classe_values = ClasseValue.objects.filter(classe=classe)
                    for classe_value in classe_values:
                        seedlot_list.append(classe_value.seedlot)
                        
                projects_list=classification.projects.filter()
                classif_projects=[]
                projects_id=[]
                for i in projects_list:
                    if i in projects:
                        projects_id.append(i.id)
                        classif_projects.append(i.name)
                 
                dico={"name":classification.name,
                      "id_classification":classification.id,
                      "classif_name":classification.name,
                      "classif_date":classification.date,
                      "classif_reference":classification.reference,
                      "classif_method":classification.method,
                      "classif_person":classification.person,
                      "classif_seedlots":set(seedlot_list),
                      "classif_projects":classif_projects,
                      'dataview':True,
                      "type_data":type_data,
                      }
            else:
                return render(request, template,{'dataview':True, "error":"ERROR! You don't have any right on the structure {0}. Please contact a data administrator via the contact page.".format(structure)})
                          
        if 'genealogy' in request.GET:
            genealogy_child = request.GET['genealogy']
            parent = request.GET['parent']
            type_data = 'Genealogy'
            
            try:
                child_entity = Accession.objects.get(name=genealogy_child)
                object_type = "Accession"
            except:
                child_entity = Seedlot.objects.get(name=genealogy_child)
                object_type = "Seedlot"
            
            if child_entity.__class__ == Accession:
                relations = AccessionRelation.objects.filter(child_id=child_entity.id)
                reproduction = relations[0].reproduction
                relations = AccessionRelation.objects.filter(child_id=child_entity.id, reproduction=reproduction) #Au cas ou il y aurait plusieurs relations de genealogie
                
                parent_male = []
                parent_female = []
                parent_x = []
                
                for relation in relations:
                    if relation.parent_gender == "M":
                        parent_male.append(relation.parent.name)
                    elif relation.parent_gender == "F":
                        parent_female.append(relation.parent.name)
                    elif relation.parent_gender == "X":
                        parent_x.append(relation.parent.name)
                
                first_production = relations[0].first_production
                method = reproduction.reproduction_method.name
                description = reproduction.description
                
                if reproduction.reproduction_method.category == 2:
                    model = 'Accession Cross'
   
                elif reproduction.reproduction_method.category == 4:
                    model = 'Other Pedigree'
                    
                elif reproduction.reproduction_method.category == 5:
                    model = 'Selfing'
                    
                else:
                    model = ''
                    
                site = ''
                start_date = ''
                end_date = ''
                        
            elif child_entity.__class__ == Seedlot:
                relations = SeedlotRelation.objects.filter(child_id=child_entity.id)
                reproduction = relations[0].reproduction
                relations = SeedlotRelation.objects.filter(child_id=child_entity.id, reproduction=reproduction) #Au cas ou il y aurait plusieurs relations de genealogie
                
                parent_male = []
                parent_female = []
                parent_x = []
                
                for relation in relations:
                    if relation.parent_gender == "M":
                        parent_male.append(relation.parent.name)
                    elif relation.parent_gender == "F":
                        parent_female.append(relation.parent.name)
                    elif relation.parent_gender == "X":
                        parent_x.append(relation.parent.name)
                
                site = relations[0].site
                start_date = reproduction.start_date
                end_date = reproduction.end_date
                method = reproduction.reproduction_method.name
                description = reproduction.description
                
                if reproduction.reproduction_method.category == 3:
                    model = 'Seedlot Cross'
                    
                elif reproduction.reproduction_method.category == 1:
                    model = 'Multiplication'
                
                else:
                    model = ''
                    
                first_production = ''
                
            else:
                return render(request, template, {'error':"ERROR! The {0} {1} doesn't exist.".format("child",genealogy_child)})
            
            
            dico={'name_genealogy':child_entity.name,
                  'object_type':object_type,
                  'name':child_entity.name,
                  'type_data':type_data,
                  'parent_female':parent_female,
                  'parent_male':parent_male,
                  'parent_x':parent_x,
                  'model':model,
                  'first_production':first_production,
                  'site':site,
                  'start_date':start_date,
                  'end_date':end_date,
                  'method':method,
                  'comments':description,
                  'dataview':True,
                  }
            
                
        elif "trait" in request.GET:
            trait_pheno=request.GET["trait"]
            type_data="trait"
            try:
                trait=Trait.objects.get(name=trait_pheno)
            except:
                return render(request, template, {'error':"ERROR! The {0} {1} doesn't exist.".format("trait",trait_pheno)})
            
            if trait.projects.filter(id__in=projects) or User.objects.get(login=username).is_superuser():
                type_data="Trait"
                trait_abbreviation=trait.abbreviation
                trait_ontology_terms=trait.ontology_terms
                trait_comments=trait.comments
                projects_list=trait.projects.filter()
                trait_projects=[]
                projects_id=[]
                for i in projects_list:
                    if i in projects:
                        projects_id.append(i.id)
                        trait_projects.append(i.name)
                
                env_id = [pheno_val.environment_id for pheno_val in PhenotypicValue.objects.filter(trait=trait).distinct()]
                sl_id = [pheno_val.seedlot_id for pheno_val in PhenotypicValue.objects.filter(trait=trait).distinct()]
                envlist = Environment.objects.filter(id__in=env_id).distinct()
                seedlotlist = Seedlot.objects.filter(id__in=sl_id).distinct()
                
                dico={"name":trait_pheno,
                      "name_trait":trait_pheno,
                      "trait_abbreviation":trait_abbreviation,
                      "trait_ontology_terms":trait_ontology_terms,
                      "trait_comments":trait_comments,
                      "trait_projects":trait_projects,
                      "type_data":type_data,
                      "env_list":envlist,
                      "seedlot_list":seedlotlist,
                      'dataview':True,
                      }
            else:
                return render(request, template,{'dataview':True, "error":"ERROR! You don't have any right on the trait {0}. Please contact a data administrator via the contact page.".format(trait_pheno)})
            
        if "environment" in request.GET:
            environment_pheno=request.GET["environment"]
            type_data="environment"
            try:
                environment=Environment.objects.get(name=environment_pheno)
            except:
                return render(request, template, {'error':"ERROR! The {0} {1} doesn't exist.".format("environment",environment_pheno)})
            
            if environment.projects.filter(id__in=projects) or User.objects.get(login=username).is_superuser():
                type_data="Environment"
                environment_date=environment.date
                environment_location=environment.location
                environment_description=environment.description
                environment_specific_treatment=environment.specific_treatment
                projects_list=environment.projects.filter()
                environment_projects=[]
                projects_id=[]
                for i in projects_list:
                    if i in projects:
                        projects_id.append(i.id)
                        environment_projects.append(i.name)
                        
                trait_id = [pheno_val.trait_id for pheno_val in PhenotypicValue.objects.filter(environment=environment).distinct()]
                sl_id = [pheno_val.seedlot_id for pheno_val in PhenotypicValue.objects.filter(environment=environment).distinct()]
                traitlist = Trait.objects.filter(id__in=trait_id).distinct()
                seedlotlist = Seedlot.objects.filter(id__in=sl_id).distinct()
                 
                dico={"name":environment_pheno,
                      "name_environment":environment_pheno,
                      "environment_date":environment_date,
                      "environment_location":environment_location,
                      "environment_description":environment_description,
                      "environment_specific_treatment":environment_specific_treatment,
                      "type_data":type_data,
                      "environment_projects":environment_projects,
                      "trait_list":traitlist,
                      "seedlot_list":seedlotlist,
                      'dataview':True,
                      "type_data":type_data,
                      }
                
                #Ici ajout des images, dans le même principe que la Map
                try:
                    env = Environment.objects.get(name=environment_pheno)
                    
                    #Liste des links et des images
                    images_link = ImageLink.objects.filter(entity_name=env.name)
                    images = []
                    for image_link in images_link:
                        if image_link.content_type.name == "environment":
                            images.append(image_link.image)

                    dico.update({"images":images})
                    dico.update({"MEDIA_ROOT":settings.MEDIA_ROOT})
                except:
                    pass;
                
            else:
                return render(request, template,{'dataview':True, "error":"ERROR! You don't have any right on the environment {0}. Please contact a data administrator via the contact page.".format(environment_pheno)})
            
        if "genoid" in request.GET or "genoid" in dico_search:      
            try:          
                genotypingid=request.GET["genoid"]
            except:
                genotypingid = dico_search['genoid']
            type_data="GenotypingID"
            try:
                genoid=GenotypingID.objects.get(name__iexact=genotypingid)
            except:
                return render(request, template,{"error":"ERROR! The {0} {1} doesn't exist.".format("genotypingid",genotypingid)})
            
            if genoid.projects.filter(id__in=projects) or User.objects.get(login=username).is_superuser():
                sample_genoid=genoid.sample
                seedlot_genoid = sample_genoid.seedlot
                accession_genoid = seedlot_genoid.accession
                experiment_genoid=genoid.experiment
                sentrixbarcode_a_genoid=genoid.sentrixbarcode_a
                sentrixposition_a_genoid=genoid.sentrixposition_a
                funding_genoid=genoid.funding
                projects_list=genoid.projects.filter()
                projects_genoid=[]
                projects_id=[]
                for i in projects_list:
                    if i in projects:
                        projects_id.append(i.id)
                        projects_genoid.append(i.name)
                dico={"type_data":type_data,
                      "name":genoid.name,
                      "name_genoid":genoid.name,
                      "sample_genoid":sample_genoid,
                      "seedlot_genoid":seedlot_genoid,
                      "accession_genoid":accession_genoid,
                      "experiment_genoid":experiment_genoid,
                      "sentrixbarcode_a_genoid":sentrixbarcode_a_genoid,
                      "sentrixposition_a_genoid":sentrixposition_a_genoid,
                      "funding_genoid":funding_genoid,
                      "projects_genoid":projects_genoid,
                      "dataview":True,}
            else:
                return render(request, template,{'dataview':True, "error":"ERROR! You don't have any right on the genotypingID {0}. Please contact a data administrator via the contact page.".format(genotypingid)})
             
        if "accession" in request.GET or "accession" in dico_search:
            try:
                accession=request.GET["accession"]
            except:
                accession=dico_search['accession']
            type_data="Accession"
            try:
                acc=Accession.objects.get(name__iexact=accession)
            except:
                acc=Accession.objects.filter(name__icontains=accession)
                if len(acc) == 1:
                    acc = Accession.objects.get(name__icontains=accession)
                elif len(acc) > 1:
                    return render(request, template,{"error":"The search for the {0} {1} returned more than one result. Please, refine your search".format("accession",accession)})
                else:
                    return render(request, template,{"error":"ERROR! The {0} {1} doesn't exist.".format("accession",accession)})             
            if acc.projects.filter(id__in=projects) or User.objects.get(login=username).is_superuser():
                type_acc = acc.type
                name = acc.name
                doi = acc.doi
                private_doi = acc.private_doi
                pedigree = acc.pedigree
                description = acc.description
                variety = acc.variety if acc.variety != None else ''
                country_of_origin = acc.country_of_origin
                donors = acc.donors
                institute_name = acc.institute_name
                institute_code = acc.institute_code
                attributes = acc.attributes_values
                if type(attributes) != dict:
                    attributes=ast.literal_eval(attributes)
                
                attributes = list(attributes.items())
                attributes2=[]
                for i, j in sorted(attributes):
                    for nb in range(len(attributes)):
                        if i==attributes[nb][0]:
                            attributes2.append((AccessionTypeAttribute.objects.get(id=i).name,j))
    #                 images = acc.images
                projects_list=acc.projects.filter()
                projects_acc=[]
                projects_id=[]
                
                for i in projects_list:
                    if i in projects:
                        projects_id.append(i.id)
                        projects_acc.append(i.name)
                
                ##seedlots à montrer dans la fiche
                seedlots = set(Seedlot.objects.filter(accession=acc, projects__in=projects_id))
                seedlot_list=[]
                classification_list=[]
                for seedlot in seedlots:
                    seedlot_list.append(seedlot.name)
                    #Recherche de classification associee aux seed lots
                    classe_value = ClasseValue.objects.filter(seedlot=seedlot)
                    for value in classe_value:
                        classification_list.append(value.classe.classification)
                
                ##sample dans la fiche
                samples = set(Sample.objects.filter(seedlot__in=seedlots, projects__in=projects_id))
                sample_list=[]
                for sample in samples:
                    sample_list.append(sample.name)
                
                
                ##genoid dans la fiche
                genoids = set(GenotypingID.objects.filter(sample__in=samples, projects__in=projects_id))
                genoid_list=[]
                experiment_list=[]
                for genoid in genoids:
                    genoid_list.append(genoid.name)
                    experiment_list.append(genoid.experiment)
                
                
                parent_male_accession = []
                parent_female_accession = []
                parent_x_accession = []
                relations = AccessionRelation.objects.filter(child=acc)
                
                if relations:
                    parent_male_accession = relations.filter(parent_gender = "M").values_list("parent__name", flat=True)
                    parent_female_accession = relations.filter(parent_gender = "F").values_list("parent__name", flat=True)
                    parent_x_accession = relations.filter(parent_gender = "X").values_list("parent__name", flat=True)
                
                dico={'name':acc.name,
                      'name_accession':acc.name,
                      'doi_acc':doi,
                      'private_doi':private_doi,
                      'pedigree_acc':pedigree,
                      'description_acc':description,
                      'variety_acc':variety,
                      'country_of_origin_acc':country_of_origin,
                      'donors_acc':donors,
                      'institute_name_acc':institute_name,
                      'institute_code_acc':institute_code,
                      'attributes_acc':attributes2,
                      'projects_acc':projects_acc,
                      'seedlots_acc':seedlot_list,
                      'samples_acc':sample_list,
                      'genoid_acc':genoid_list,
                      'experiment_acc':set(experiment_list),
                      'classification_acc':set(classification_list),
                      "type_data":type_data,
                      'dataview':True,
                      "parent_male_accession":parent_male_accession,
                      "parent_female_accession":parent_female_accession,
                      "parent_x_accession":parent_x_accession}
                                
                #Ajout Arthur : Affichage de la map dans la fiche des accessions (si possible)             
                #Get accession
                if acc.longitude and acc.latitude:
                    latitude=acc.latitude
                    longitude=acc.longitude
                    
                    fig = folium.Figure()
                    m = folium.Map(
                        width=500,
                        height=300,
                        location=[acc.latitude, acc.longitude],
                        zoom_start=2,
                    )
                    m.add_to(fig)
                    
                    marker = folium.Marker([acc.latitude,acc.longitude], popup=acc.name, tooltip=acc.name)
                    m.add_child(marker)
                    fig.render()
                
                    dico.update({"latitude":latitude, "longitude":longitude, "acc":acc, 'map':fig})
                    
                
                #Ici ajout des images, dans le même principe que la Map                   
                #Liste des links et des images
                images_link = ImageLink.objects.filter(content_type=ContentType.objects.get_for_model(acc.__class__), object_id = acc.id)
                #print(images_link)
                images = []
                for image_link in images_link:
                    if image_link.content_type.name == "accession":
                        images.append(image_link.image)
                #print(images)
                dico.update({"images":images})
                dico.update({"MEDIA_ROOT":settings.MEDIA_ROOT})
                
                
                
            else:
                return render(request, template,{'dataview':True, "error":"ERROR! You don't have any right on the accession {0}. Please contact a data administrator via the contact page.".format(accession)})

        if "experiment" in request.GET:
            experiment=request.GET["experiment"]
            type_data="Experiment"
            try:
                exp=Experiment.objects.get(name=experiment)
            except:
                return render(request, template,{"error":"ERROR! The {0} {1} doesn't exist.".format("experiment",experiment)})    
                
            if exp.projects.filter(id__in=projects) or User.objects.get(login=username).is_superuser():
                institution_experiment=exp.institution
                person_experiment=exp.person
                date_experiment=exp.date
                comments_experiment=exp.comments
                projects_list=exp.projects.filter()
                projects_experiment=[]
                projects_id=[]
            
                for i in projects_list:
                    if i in projects:
                        projects_id.append(i.id)
                        projects_experiment.append(i.name)
                
                genoids = set(GenotypingID.objects.filter(experiment=exp, projects__in=projects_id))
                genoid_list=[]
                for genoid in genoids:
                    genoid_list.append(genoid.name)
                
                dico={"name":experiment,
                      "name_experiment":experiment,
                      "institution_experiment":institution_experiment,
                      "person_experiment": person_experiment,
                      "date_experiment":date_experiment,
                      "comments_experiment":comments_experiment,
                      "projects_experiment":projects_experiment,
                      "genoid_experiment":genoid_list,
                      "type_data":type_data,
                      'dataview':True,
                      }
            else:
                return render(request, template,{'dataview':True, "error":"ERROR! You don't have any right on the experiment {0}. Please contact a data administrator via the contact page.".format(experiment)})

        if "referential" in request.GET and "genoid" not in request.GET:
            referential=request.GET["referential"]
            type_data="Referential"
            try:
                ref=Referential.objects.get(name=referential)
            except:
                return render(request, template,{"error":"ERROR! The {0} {1} doesn't exist.".format("referential",referential)})    
                
            if ref.projects.filter(id__in=projects) or User.objects.get(login=username).is_superuser():
                person_referential=ref.person
                date_referential=ref.date
                comments_referential=ref.comments
                projects_list=ref.projects.filter()
                projects_referential=[]
                projects_id=[]
                
                for i in projects_list:
                    if i in projects:
                        projects_id.append(i.id)
                        projects_referential.append(i.name)

                
                dico={"name":referential,
                      "name_referential":referential,
                      "person_referential": person_referential,
                      "date_referential":date_referential,
                      "comments_referential":comments_referential,
                      "projects_referential":projects_referential,
                      "type_data":type_data,
                      'dataview':True,
                      }
            else:
                return render(request, template,{'dataview':True, "error":"ERROR! You don't have any right on the referential {0}. Please contact a data administrator via the contact page.".format(referential)})


        if "seedlot" in request.GET or "seedlot" in dico_search:
            try:
                seedlot=request.GET['seedlot']
            except:
                seedlot = dico_search['seedlot']
            type_data="Seedlot"
            try:
                sl=Seedlot.objects.get(name__iexact=seedlot)
            except:
                sl=Seedlot.objects.filter(name__icontains=seedlot)
                if len(sl) == 1:
                    sl = Seedlot.objects.get(name__icontains=seedlot)
                elif len(sl) > 1:
                    return render(request, template,{"error":"The search for the {0} {1} returned more than one result. Please, refine your search".format("seedlot",seedlot)})
                else:
                    return render(request, template,{"error":"ERROR! The {0} {1} doesn't exist.".format("seedlot",seedlot)}) 
            if sl.projects.filter(id__in=projects) or User.objects.get(login=username).is_superuser():
                accession_sl=sl.accession.name
                type_sl=sl.type.name
                is_obsolete_sl=sl.is_obsolete
                description_sl=sl.description
                attributes=sl.attributes_values
                if type(attributes) != dict:
                    attributes=ast.literal_eval(attributes) ##conversion de la chaine de char en dictionnaire
                
                attributes = list(attributes.items())
                attributes2=[]
                for i, j in sorted(attributes):
                    for nb in range(len(attributes)):
                        if i==attributes[nb][0]:
                            attributes2.append((SeedlotTypeAttribute.objects.get(id=i).name,j))

                projects_seedlot=sl.projects.filter()
                projects_sl=[]
                projects_id=[]
                for i in projects:
                    if i in projects_seedlot:
                        projects_id.append(i.id)
                        projects_sl.append(i.name)
                samples = Sample.objects.filter(seedlot=sl, projects__in=projects_id)
#                 genoids = GenotypingID.objects.filter(sample__in=samples, projects__in=projects_id)
                
                genoids = set(GenotypingID.objects.filter(sample__in=samples, projects__in=projects_id))
                genoid_list=[]
                for genoid in genoids:
                    genoid_list.append(genoid.name)
                
                pheno_values = PhenotypicValue.objects.filter(seedlot=sl)
                
                environment_list = []
                trait_list = []
                for value in pheno_values:
                    environment_list.append(value.environment)
                    trait_list.append(value.trait)
                
                #On ajoute les classifications
                classification_list=[]
                classe_value = ClasseValue.objects.filter(seedlot=sl)
                for value in classe_value:
                    classification_list.append(value.classe.classification)
                
                parent_male_sl = []
                parent_female_sl = []
                parent_x_sl = []
                relations = SeedlotRelation.objects.filter(child=sl)
                
                if relations:
                    parent_male_sl = relations.filter(parent_gender = "M").values_list("parent__name", flat=True)
                    parent_female_sl = relations.filter(parent_gender = "F").values_list("parent__name", flat=True)
                    parent_x_sl = relations.filter(parent_gender = "X").values_list("parent__name", flat=True)
                
                dico={"name":sl.name,
                      "name_sl":sl.name,
                      "accession_sl":accession_sl,
                      "type_sl": type_sl,
                      "is_obsolete_sl":is_obsolete_sl,
                      "description_sl":description_sl,
                      "attributes_sl":attributes2,
                      "projects_sl":projects_sl,
                      "environment_sl":set(environment_list),
                      "trait_sl":set(trait_list),
                      "samples":set(samples),
                      #"genoid":set(genoids),
                      "genoid_list":genoid_list,
                      'classification_sl':set(classification_list),
                      "type_data":type_data,
                      'dataview':True,
                      'parent_male_sl':parent_male_sl,
                      'parent_female_sl':parent_female_sl,
                      'parent_x_sl':parent_x_sl
                      }
                                
                #Ici ajout des images, dans le même principe que la Map
                try:
                    seed = Seedlot.objects.get(name=seedlot)

                    #Liste des links et des images
                    images_link = ImageLink.objects.filter(entity_name=seed.name)
                    images = []
                    for image_link in images_link:
                        if image_link.content_type.name == "seedlot":
                            images.append(image_link.image)

                    dico.update({"images":images})
                    dico.update({"MEDIA_ROOT":settings.MEDIA_ROOT})
                except:
                    pass;
                
            else:
                return render(request, template,{'dataview':True,"error":"ERROR! You don't have any right on the seedlot {0}. Please contact a data administrator via the contact page.".format(seedlot)})
            
        if "sample" in request.GET or "sample" in dico_search:
            try:
                sample=request.GET['sample']
            except:
                sample = dico_search['sample']   
            type_data = "Sample"
            try:
                sple=Sample.objects.get(name__iexact=sample)
            except:
                sple=Sample.objects.filter(name__icontains=sample)
                if len(sple) == 1:
                    sple = Sample.objects.get(name__icontains=sample)
                elif len(sple) > 1:
                    return render(request, template,{"error":"The search for the {0} {1} returned more than one result. Please, refine your search".format("sample",sample)})
                else:
                    return render(request, template,{"error":"ERROR! The {0} {1} doesn't exist.".format("sample",sample)}) 
            if sple.projects.filter(id__in=projects) or User.objects.get(login=username).is_superuser():
                seedlot_sample = sple.seedlot.name
                accession_sample = sple.seedlot.accession.name
                description_sample = sple.description
                is_bulk_sample = sple.is_bulk
                is_obsolete_sample = sple.is_obsolete
                codelabo_sample = sple.codelabo
                tubename_sample = sple.tubename
                projects_sple = sple.projects.filter()
                projects_sample = []
                projects_id = []
                for i in projects:
                    if i in projects_sple:
                        projects_id.append(i.id)
                        projects_sample.append(i.name)

                genoids = set(GenotypingID.objects.filter(sample=sple, projects__in=projects_id))
                genoid_list=[]
                experiment_list=[]
                for genoid in genoids:
                    genoid_list.append(genoid.name)
                    experiment_list.append(genoid.experiment)
                
                dico={"name":sple.name,
                      "name_sample":sple.name,
                      "seedlot_sample":seedlot_sample,
                      "accession_sample":accession_sample,
                      "description_sample":description_sample,
                      "is_bulk_sample":is_bulk_sample,
                      "is_obsolete_sample":is_obsolete_sample,
                      "codelabo_sample":codelabo_sample,
                      "tubename_sample":tubename_sample,
                      "projects_sample":projects_sample,
                      "genoid_list":genoid_list,
                      "experiment_list":set(experiment_list), 
                      "type_data":type_data,  
                      'dataview':True,              
                      }
            else:
                return render(request, template,{'dataview':True, "error":"ERROR! You don't have any right on the sample {0}. Please contact a data administrator via the contact page.".format(sample)})
        if "project" in request.GET or "project" in dico_search:
            try:
                project_name=request.GET['project']
            except:
                project_name = dico_search['project']
            project = Project.objects.get(name=project_name)
            if project.users.filter(login=username) or User.objects.get(login=username).is_superuser():
                name = project.name
                authors = project.authors
                start_date = project.start_date
                end_date = project.end_date
                description = project.description
                experiment= Experiment.objects.filter(projects=project)
                project_experiment=[]
                for exp in experiment:
                    project_experiment.append(exp.name)
                fiche="project_card"
                instit = ','.join([p['name'] for p in project.institutions.values('name')])
                     
                if len(project.linked_files.all())!=0:
                    linked_files = project.linked_files.all()
                else:
                    linked_files=""
                return render(request, 'dataview/project_infos.html', locals())
            else :
                return render(request, template,{'dataview':True, "error":"ERROR! You don't have any right on the project {0}. Please contact a data administrator via the contact page.".format(project_name)})
        
        if request.user.is_authenticated and request.user.is_team_admin :
            dico.update({"team_admin":request.user.is_team_admin})

        return render(request, template, dico)

@login_required_on_private_instance
def structure_viewer(request):
    number_all=0
    template="dataview/GenoExp_viewer.html"
    formproject=DtviewProjectForm()
    title="Structure viewer"
    username = None
    number_all=0
    or_and = None
    dico_get={}
    type_data="classification"
    projects_or_not=1
    if request.user and request.user.is_authenticated:
        username = request.user.login
        if User.objects.get(login=username).is_superuser():
            projects = Project.objects.all()
        else:
            username_id=User.objects.get(login=username).id
            projects = Project.objects.filter(users=username_id)
    elif settings.PUBLIC_INSTANCE :
        projects = Project.objects.all()
    else :
        projects = Project.objects.none()
    projects_allowed=', '.join([(v.name) for v in projects])
    projects_allowed = projects_allowed.split(', ')
    list_attributes=[]
    for i in ['structure', 'projects']:
        list_attributes.append(i)
    
    if "page" in request.GET:
        if request.session['projects']!=[]:
            projects = request.session['projects']
            list_proj=[i for i in projects]
            list_projects=[]
            for i in list_proj:
                list_projects.append(Project.objects.get(id=i))
            structures=_get_structures(request, list_projects)
            labels = ['Structure','Description', 'Project']
            dico={}
            for i in structures:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                dico[i.name]=[i.name,i.description,', '.join(projects_to_show)]
            if dico:
                structures_all=Classification.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                number_all=len(structures_all)
                for i in structures_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                            if k not in request.session['projects_to_show']:
                                request.session['projects_to_show'].append(k)
                    dico_all[i.name]=[i.name,i.description,', '.join(projects_to_show)]    
                getPandasDf(settings.TEMP_FOLDER+"/dtview_structure.csv",*labels, **dico_all)    
            dico=dico.items()
            projects_or_not=1
            tag_fields = {'all':dico,
                          'fields_label': labels,
                          "title":title,
                          'creation_mode': True,
                          "pagination":structures,
                          'projects_or_not':projects_or_not,
                          "structure":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          "list_attributes":list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
                          "refine_search":True,
                          }
            return render(request, template, tag_fields)        
                    
    if request.method == 'POST' or "or_and" in request.GET :  
        tag_fields = {}
        if 'or_and' in request.GET and "no_search" not in request.GET:
            projects_to_show = request.session['projects_to_show']
            list_projects = request.session['list_projects']
            struct_name_list = request.session["struct_name_list"]
            dico_get={}
            or_and = None
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    if request.GET[data] == "structure":
                        dico_get[data]={"name":request.GET[text_data]}
                    else:
                        dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
            length_dico_get = len(dico_get)
            nb_per_page = 10                
            if or_and == "or":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query |= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            if data_type == "structure":
                                data_type = "name"
                            var_search = str(data_type + "__icontains" ) 
                            query |= Query_dj(**{var_search : search_value}) 
            
            if or_and == "and":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query &= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            if data_type == "structure":
                                data_type = "name"
                            var_search = str(data_type + "__icontains" )
                            query &= Query_dj(**{var_search : search_value})        
            
            proj_allowed = Project.objects.filter(name__in=projects_to_show)
            query = Query_dj(query, projects__in=proj_allowed, name__in=struct_name_list)   
            structures = Classification.objects.filter(query).distinct() 
            number_all = len(structures)
            list_exceptions = ['date','reference','method','person','classe']
            download_not_empty = False
            if structures:
                download_not_empty = write_csv_search_result(structures, list_exceptions)
            tag_fields.update({"download_not_empty":download_not_empty})  
            
        elif "no_search" in request.GET:
            list_projects = request.session['list_projects']
            projects_or_not=1
            structures=_get_structures(request, list_projects)     
            
        else:
            formproject=DtviewProjectForm(request.POST)
            formfiche = FicheForm(request.POST)
            if formproject.is_valid():
                list_projects_id = formproject.cleaned_data['project']
            list_projects = []
            for i in list_projects_id:
                list_projects.append(i.id)
            request.session['list_projects'] = list_projects
            structures=_get_structures(request, list_projects)
            struct_name_list=[]
            for i in structures : 
                struct_name_list.append(i.name)
            request.session["struct_name_list"] = struct_name_list
            
        if projects_or_not==1:
            labels = ['Structure','Description', 'Project']
            dico={}
            request.session['projects_to_show']=[]
            for i in structures:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                        if k not in request.session['projects_to_show']:
                            request.session['projects_to_show'].append(k)
                dico[i.name]=[i.name,i.description,', '.join(projects_to_show)] 
            if dico:
                structures_all=Classification.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                if number_all == 0:
                    number_all=len(structures_all)
                for i in structures_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                    dico_all[i.name]=[i.name,i.description,', '.join(projects_to_show)]
                getPandasDf(settings.TEMP_FOLDER+"/dtview_structure.csv",*labels, **dico_all)  
            dico=dico.items()
            projects_or_not=1
            list_proj=[(i) for i in list_projects]
            request.session['projects']=list_proj
            tag_fields.update({'all':dico,
                          "title":title,
                          'fields_label': labels,
                          'creation_mode': True,
                          "pagination":structures,
                          'projects_or_not':projects_or_not,
                          "structure":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          "list_attributes":list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
                          })
            return render(request, template, tag_fields)
       
    else:
        projects_or_not = 0    
        formproject.fields['project'].queryset=projects.order_by('name')
    
        return render(request, template, {"type_data":type_data,'dataview':True,"projects":projects_allowed,'formproject':formproject,"projects_or_not":projects_or_not, "title":title})  
            
                    
def _get_structures(request, list_projects):
    structures=Classification.objects.filter(projects__in=list_projects).distinct()
    paginator = Paginator(structures,50)
    page = request.GET.get('page')
    try:
        structures = paginator.page(page)
    except PageNotAnInteger:
        structures = paginator.page(1)
    except EmptyPage:
        structures = paginator.page(paginator.num_pages)
    return structures


@login_required_on_private_instance
def accession_viewer(request):
    """
      This view contains several forms to perform a query on accessions. It is possible 
      to display the associated seedlots and samples. The result of the query is displayed 
      in a table with export and refine search functionalities.
    """
    list_attributes = ['accession', 'type', 'doi', 'pedigree', 'description', 'variety', 'latitude', 'longitude', 'country_of_origin', 'donors', 'institute_name', 'institute_code', 'projects']
    template = 'dataview/dtview_tableau.html'
    
    if request.method == 'GET':
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        
        if "nb_per_page" in request.GET and not "in_search" in request.GET:
            list_projects_ids = request.session['list_projects_ids']
            list_type = request.session['list_type']
            list_more = request.session['list_more']
            
            list_projects = Project.objects.filter(id__in=list_projects_ids)
            
            accessions = Accession.objects.filter(projects__in=list_projects, type_id__in=list_type).distinct()
            string_accessions = str('_'.join(str(acc.id) for acc in accessions))
        
            objects, tag_fields = get_accession_viewer_data(request, accessions, list_more, list_attributes, list_projects, list_type)
            download_not_empty = True
            
            tag_fields.update({'string_accessions':string_accessions,
                               'nb_per_page':nb_per_page,
                               'user_projects':request.session['user_projects'],
                               'download_not_empty':download_not_empty,
                               'filename':"accession_viewer",
                               'dataview':True,
                               'refine_search':True})
            
            return render(request, template, tag_fields)
            
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            list_projects_ids = request.session['list_projects_ids']
            list_type = request.session['list_type']
            list_more = request.session['list_more']
            
            accessiontype = ''
            if len(list_type) == 1:
                accessiontype = list_type[0]
            
            searched_accs = Accession.objects.viewer_refine_search(or_and, accessiontype, dico_get)
            
            list_projects = Project.objects.filter(id__in=list_projects_ids)
            
            filtered_accs = Accession.objects.filter(projects__in=list_projects, type_id__in=list_type).distinct()
            accessions = searched_accs.intersection(filtered_accs)
            string_accessions = str('_'.join(str(acc.id) for acc in accessions))
            
            objects, tag_fields = get_accession_viewer_data(request, accessions, list_more, list_attributes, list_projects, list_type)
            
            download_not_empty = True
            
            tag_fields.update({'string_accessions':string_accessions,
                               'nb_per_page':nb_per_page,
                               'user_projects':request.session['user_projects'],
                               'dataview':True,
                               'search_result':dico_get,
                               'or_and':or_and,
                               'download_not_empty':download_not_empty,
                               'refine_search':True})
            
            return render(request, template, tag_fields) 
            
        #Query in the refine search
        elif "or_and" in request.GET:
            dico_get={}
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
            
            list_projects_ids = request.session['list_projects_ids']
            list_type = request.session['list_type']
            list_more = request.session['list_more']
            
            accessiontype = ''
            if len(list_type) == 1:
                accessiontype = list_type[0]
            
            searched_accs = Accession.objects.viewer_refine_search(or_and, accessiontype, dico_get)
            
            list_projects = Project.objects.filter(id__in=list_projects_ids)
            
            filtered_accs = Accession.objects.filter(projects__in=list_projects, type_id__in=list_type).distinct()
            accessions = searched_accs.intersection(filtered_accs)
            string_accessions = str('_'.join(str(acc.id) for acc in accessions))
            
            objects, tag_fields = get_accession_viewer_data(request, accessions, list_more, list_attributes, list_projects, list_type)
            
            download_not_empty = False
            thread_name = ''
            if objects:
                thread_name = execute_function_in_thread(write_accession_csv_from_viewer, [objects, tag_fields, "Result search"], {})
                download_not_empty = True
            
            tag_fields.update({'string_accessions':string_accessions,
                               'nb_per_page':nb_per_page,
                               'user_projects':request.session['user_projects'],
                               'dataview':True,
                               'search_result':dico_get,
                               'or_and':or_and,
                               'download_not_empty':download_not_empty,
                               'thread_name':thread_name,
                               'refine_search':True})
            
            return render(request, template, tag_fields) 
        
          
        formproject = DtviewProjectForm()
        
        if request.user and request.user.is_authenticated:
            username = request.user.login
            if User.objects.get(login=username).is_superuser():
                projects = Project.objects.all()
            else:
                username_id=User.objects.get(login=username).id
                projects = Project.objects.filter(users=username_id)
        elif settings.PUBLIC_INSTANCE :
            projects = Project.objects.all()
        else :
            projects = Project.objects.none()
        
        request.session['user_projects'] = list(projects.values_list("name",flat=True))
        
        formproject.fields['project'].queryset=projects.order_by('name')
        accessiontypeform = DtviewAccessionTypeForm()
        viewaccession = DataviewAccession()
        
        tag_fields = {'accession_viewer':True,
                      'dataview':True,
                      'user_projects':projects,
                      'formproject':formproject,
                      'accessiontypeform':accessiontypeform,
                      'viewaccession':viewaccession}
        
        return render(request, 'dataview/choose_project.html', tag_fields)

    elif request.method == "POST":
        formproject = DtviewProjectForm(request.POST)
        accessiontypeform = DtviewAccessionTypeForm(request.POST)
        viewaccession = DataviewAccession(request.POST)
        
        nb_per_page = 50
        
        if formproject.is_valid() and accessiontypeform.is_valid():
            list_projects = formproject.cleaned_data['project']
            list_type = request.POST.getlist('accession_type')
            list_more = request.POST.getlist('more_info')

            request.session['list_projects_ids'] = list(list_projects.values_list('id', flat=True))
            request.session['list_type'] = list_type
            request.session['list_more'] = list_more
            
        else:
            return redirect('accession_viewer')
        
        accessions = Accession.objects.filter(projects__in=list_projects, type_id__in=list_type).distinct()
        string_accessions = str('_'.join(str(acc.id) for acc in accessions))
        
        objects, tag_fields = get_accession_viewer_data(request, accessions, list_more, list_attributes, list_projects, list_type)

        #Usage of a thread: the file is computed while the page is displayed
        thread_name = execute_function_in_thread(write_accession_csv_from_viewer, [objects, tag_fields, "accession_viewer"], {})
        download_not_empty = True

        tag_fields.update({'user_projects':request.session['user_projects'],
                           'string_accessions':string_accessions,
                           'nb_per_page':nb_per_page,
                           'download_not_empty':download_not_empty,
                           'thread_name':thread_name,
                           'filename':"accession_viewer",
                           'dataview':True,
                           'refine_search':True})
        
        return render(request, template, tag_fields)


def get_accession_viewer_data(request, accessions, list_more, list_attributes, list_projects, list_type):
    """
      Return the data to display in the accession viewer.
    """
    accessions = accessions.order_by("name")
    
    if list_more == ['1'] or list_more == [u'1']:
        list_attributes.insert(1,'seedlot')
        title = 'Accessions and Seedlots linked to the project(s)'
        labels = ['Accession','Seedlot', 'Type', 'DOI', 'Pedigree', 'Description', 'Variety', 'Latitude', 'Longitude', 'Country of Origin', 'Donors', 'Institute Name',' Institute Code', 'Projects']
        names = ['name', 'seedlot', 'type', 'doi', 'pedigree', 'description', 'variety', 'latitude', 'longitude', 'country_of_origin', 'donors', 'institute_name', 'institute_code', 'projects']
        
        if len(list_type) == 1:
            accessiontype = AccessionType.objects.get(id=list_type[0])
            attributes = accessiontype.accessiontypeattribute_set.all()
            for att in attributes:
                labels.append(str(att).capitalize())
                names.append(str(att))
                list_attributes.append(str(att))
                
            set_accession_attributes(accessions, attributes)
        
        accession_list = []
        seedlot_list = []
        for acc in accessions:
            acc_seedlots = acc.seedlot_set.filter(projects__in=list_projects).distinct()
            if acc_seedlots:
                for seedlot in acc_seedlots:
                    accession_list.append(acc)
                    seedlot_list.append(seedlot)

            else:
                accession_list.append(acc)
                seedlot_list.append('--')

        number_all = len(accession_list)
        
        objects = zip(accession_list, seedlot_list)

        accession_list = add_pagination(request, accession_list)
        seedlot_list = add_pagination(request, seedlot_list)
        
        data = zip(accession_list, seedlot_list)
        
        tag_fields = {'all':accession_list,
                      'data':data,
                      'object_type':'seedlot',
                      'title':title,
                      'fields_name':names,
                      'fields_label':labels,
                      'list_attributes':list_attributes,
                      'number_all':number_all
                      }
        
        
    elif list_more == ['2'] or list_more == [u'2']:
        list_attributes.insert(1,'sample')
        title = 'Accessions and Samples linked to the project(s)'
        labels = ['Accession','Sample', 'Type', 'DOI', 'Pedigree', 'Description', 'Variety', 'Latitude', 'Longitude', 'Country of Origin', 'Donors', 'Institute Name', 'Institute Code', 'Projects']
        names = ['name', 'sample', 'type', 'doi', 'pedigree', 'description', 'variety', 'latitude', 'longitude', 'country_of_origin', 'donors', 'institute_name', 'institute_code', 'projects']
        
        if len(list_type) == 1:
            accessiontype = AccessionType.objects.get(id=list_type[0])
            attributes = accessiontype.accessiontypeattribute_set.all()
            for att in attributes:
                labels.append(str(att).capitalize())
                names.append(str(att))
                list_attributes.append(str(att))
                
            set_accession_attributes(accessions, attributes)
        
        accession_list = []
        sample_list = []
        for acc in accessions:
            acc_seedlots = acc.seedlot_set.filter(projects__in=list_projects).distinct()
            if acc_seedlots:
                for seedlot in acc_seedlots:
                    sl_sample = seedlot.sample_set.filter(projects__in=list_projects).distinct()
                    if sl_sample:
                        for sample in sl_sample:
                            accession_list.append(acc)
                            sample_list.append(sample)
                        
                    else:
                        accession_list.append(acc)
                        sample_list.append('--')
            
            else:
                accession_list.append(acc)
                sample_list.append('--')
                
        number_all = len(accession_list)
        
        objects = zip(accession_list, sample_list)

        accession_list = add_pagination(request, accession_list)
        sample_list = add_pagination(request, sample_list)
        
        data = zip(accession_list, sample_list)
        
        tag_fields = {'all':accession_list,
                      'data':data,
                      'object_type':'sample',
                      'title':title,
                      'fields_name':names,
                      'fields_label':labels,
                      'list_attributes':list_attributes,
                      'number_all':number_all
                      }
        
        
    elif list_more == ['1','2'] or list_more == [u'1',u'2']:
        list_attributes.insert(1,'seedlot')
        list_attributes.insert(2,'sample')
        title = 'Accessions, Seedlots and Samples linked to the project(s)'
        labels = ['Accession','Seedlot', 'Sample','Type', 'DOI', 'Pedigree', 'Description', 'Variety', 'Latitude', 'Longitude', 'Country of Origin', 'Donors', 'Institute Name', 'Institute Code', 'Projects']
        names = ['name', 'seedlot', 'sample', 'type', 'doi', 'pedigree', 'description', 'variety', 'latitude', 'longitude', 'country_of_origin', 'donors', 'institute_name', 'institute_code', 'projects']
        
        if len(list_type) == 1:
            accessiontype = AccessionType.objects.get(id=list_type[0])
            attributes = accessiontype.accessiontypeattribute_set.all()
            for att in attributes:
                labels.append(str(att).capitalize())
                names.append(str(att))
                list_attributes.append(str(att))
                
            set_accession_attributes(accessions, attributes)
        
        accession_list = []
        seedlot_list = []
        sample_list = []
        for acc in accessions:
            acc_seedlots = acc.seedlot_set.filter(projects__in=list_projects).distinct()
            if acc_seedlots:
                for seedlot in acc_seedlots:
                    sl_sample = seedlot.sample_set.filter(projects__in=list_projects).distinct()
                    if sl_sample:
                        for sample in sl_sample:
                            accession_list.append(acc)
                            seedlot_list.append(seedlot)
                            sample_list.append(sample)
                        
                    else:
                        accession_list.append(acc)
                        seedlot_list.append(seedlot)
                        sample_list.append('--')
            
            else:
                accession_list.append(acc)
                seedlot_list.append('--')
                sample_list.append('--')
                
        number_all = len(accession_list)
        
        objects = zip(accession_list, seedlot_list, sample_list)
        
        accession_list = add_pagination(request, accession_list)
        seedlot_list = add_pagination(request, seedlot_list)
        sample_list = add_pagination(request, sample_list)
        
        data = zip(accession_list, seedlot_list, sample_list)
        
        tag_fields = {'all':accession_list,
                      'data':data,
                      'object_type':'seedlot_sample',
                      'title':title,
                      'fields_name':names,
                      'fields_label':labels,
                      'list_attributes':list_attributes,
                      'number_all':number_all
                      }
        
        
    else:
        title = 'Accessions linked to the project(s)'
        labels = ['Accession','Type', 'DOI', 'Pedigree', 'Description', 'Variety', 'Latitude', 'Longitude', 'Country of Origin', 'Donors', 'Institute Name', 'Institute Code', 'Projects']
        names = ['name', 'type', 'doi', 'pedigree', 'description', 'variety', 'latitude', 'longitude', 'country_of_origin', 'donors', 'institute_name', 'institute_code', 'projects']
        
        if len(list_type) == 1:
            accessiontype = AccessionType.objects.get(id=list_type[0])
            attributes = accessiontype.accessiontypeattribute_set.all()
            for att in attributes:
                labels.append(str(att).capitalize())
                names.append(str(att))
                list_attributes.append(str(att))
                
            set_accession_attributes(accessions, attributes)
        
        number_all = len(accessions)
        
        objects = list(accessions)
        
        accessions = add_pagination(request, accessions)
        
        tag_fields = {'all':accessions,
                      'object_type':'accession',
                      'title':title,
                      'fields_name':names,
                      'fields_label':labels,
                      'list_attributes':list_attributes,
                      'number_all':number_all
                      }
        
    return objects, tag_fields


def set_accession_attributes(accessions, attributes):
    """
      Get attribute's values for each accession to write in the table.
    """
    for accession in accessions:
        for attribute in attributes:
            if accession.attributes_values:
                if type(accession.attributes_values) is dict:
                    attributes_values = accession.attributes_values
                else:
                    attributes_values = json.loads(accession.attributes_values)
                    
                if str(attribute.id) in attributes_values.keys():
                    try:
                        setattr(accession,attribute.name.lower().replace(' ','_'),attributes_values[str(attribute.id)])
                    except ValueError as e:
                        print(e)
                        setattr(accession,attribute.name.lower().replace(' ','_'),'')
                else:
                    setattr(accession,attribute.name.lower().replace(' ','_'),'')
            else:
                setattr(accession,attribute.name.lower().replace(' ','_'),'')


def write_accession_csv_from_viewer(objects, tag_fields, title):
    """
      CSV writer specific for accession viewer, to display in separated columns 
      the accession name and attributes's informations (seedlot, sample).
    """
    filename = title.lower().replace(" ","_")
    dialect = Dialect(delimiter = ';')
    
    with open(settings.TEMP_FOLDER+filename+".csv", 'w', encoding='utf-8') as csv_file:
        writer = csv.writer(csv_file, dialect)
        writer.writerow(tag_fields['fields_label'])
        
        if tag_fields['object_type'] == "accession":
            for acc in objects:
                row = []
                for attribute in tag_fields['fields_name']:
                    try:
                        attr_value = acc.__getattribute__(attribute.lower().replace(' ','_'))
                    except:
                        attr_value = ""
                    
                    if attribute == "projects":
                        attr_value = ', '.join([str(att) for att in attr_value.filter(name__in=tag_fields['user_projects'])])
                    
                    row.append(attr_value)
                    
                writer.writerow(row)
                
        elif tag_fields['object_type'] == "seedlot" or tag_fields['object_type'] == "sample":
            for acc, scnd_obj in objects:
                row = []
                for attribute in tag_fields['fields_name']:
                    try:
                        if attribute == 'seedlot' or attribute == 'sample':
                            attr_value = scnd_obj
                        
                        else:
                            attr_value = acc.__getattribute__(attribute.lower().replace(' ','_'))
                            
                    except:
                        attr_value = ""
                    
                    if attribute == "projects":
                        attr_value = ', '.join([str(att) for att in attr_value.filter(name__in=tag_fields['user_projects'])])
                    
                    row.append(attr_value)
                    
                writer.writerow(row)
            
        elif tag_fields['object_type'] == "seedlot_sample":
            for acc, seedlot, sample in objects:
                row = []
                for attribute in tag_fields['fields_name']:
                    try:
                        if attribute == 'seedlot': 
                            attr_value = seedlot
                        elif attribute == 'sample':
                            attr_value = sample
                        else:
                            attr_value = acc.__getattribute__(attribute.lower().replace(' ','_'))
                            
                    except:
                        attr_value = ""
                    
                    if attribute == "projects":
                        attr_value = ', '.join([str(att) for att in attr_value.filter(name__in=tag_fields['user_projects'])])
                    
                    row.append(attr_value)
                    
                writer.writerow(row)
            
        else:
            print("Erreur: on ne devrait pas etre ici")
        
    download_not_empty = True
    return download_not_empty


@login_required
def accession_map(request):
    """
      To display accessions markers on a map
    """
    template = "dataview/accession_map.html"
    
    acc_id_str = request.POST.get('acc_id_str')
    
    list_accessions_id = acc_id_str.split("_")
    #Récupération des accessions
    
    fig = folium.Figure()
    m = folium.Map(
        width=1020,
        height=574,
        location=[20, 10],
        zoom_start=2,
    )
    m.add_to(fig)
    
    for id in list_accessions_id:
        acc = Accession.objects.get(id=id)
        
        if acc.latitude != None and acc.longitude != None:
            marker = folium.Marker([acc.latitude,acc.longitude], popup="<a onclick='return ReturnDataCardInformation(\""+str(acc.name)+"\");' href=#>"+acc.name+"</a>", tooltip=acc.name)
            m.add_child(marker)
          
    fig.render()
    
    tag_fields = {'dataview':True,
                  'map':fig}
                  
    return render(request, template, tag_fields)
    

def _get_accessions(request, list_projects, list_type):
    accessions = list(set(Accession.objects.filter(projects__in=list_projects, type_id__in=list_type).order_by('name')))
    paginator = Paginator(accessions,50)
    
    page = request.GET.get('page')
    try:
        accessions = paginator.page(page)
    except PageNotAnInteger:
        accessions = paginator.page(1)
    except EmptyPage:
        accessions = paginator.page(paginator.num_pages)
    return accessions


@login_required_on_private_instance
def genotypingid_viewer(request):
    """
      This view contains several forms to perform a query on genotyping_ids. It is possible 
      to display the associated seedlots, samples and accessions. The result of the query is displayed 
      in a table with export and refine search functionalities.
    """
    list_attributes = ['genotyping id','projects']
    template = 'dataview/genotypingid_viewer.html'
    title="Genotyping ID viewer"
    
    if request.method == 'GET':
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        
        if "nb_per_page" in request.GET and not "in_search" in request.GET:
            list_projects_ids = request.session['list_projects_ids']
            list_more = request.session['list_more']
            
            list_projects = Project.objects.filter(id__in=list_projects_ids)
            
            genoids = GenotypingID.objects.select_related('sample','sample__seedlot','sample__seedlot__accession').prefetch_related('projects').filter(projects__in=list_projects).distinct()
            genoids = genoids.order_by('name')
            number_all = len(genoids)
            genoids = add_pagination(request, genoids)
            
            tag_fields = get_genotypingid_viewer_infos(list_more, list_attributes)
            download_not_empty = True
            
            tag_fields.update({'all':genoids,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'user_projects':request.session['user_projects'],
                               'download_not_empty':download_not_empty,
                               'filename':"genotypingid_viewer",
                               'dataview':True,
                               'refine_search':True})
            
            return render(request, template, tag_fields)
        
        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            list_projects_ids = request.session['list_projects_ids']
            list_more = request.session['list_more']
            
            searched_genoids = GenotypingID.objects.viewer_refine_search(or_and, dico_get)
            list_projects = Project.objects.filter(id__in=list_projects_ids)
        
            filtered_genoids = GenotypingID.objects.select_related('sample','sample__seedlot','sample__seedlot__accession').prefetch_related('projects').filter(projects__in=list_projects).distinct()
            genoids = searched_genoids.intersection(filtered_genoids)
            genoids = genoids.order_by('name')
            number_all = len(genoids)
            genoids = add_pagination(request, genoids)
            
            tag_fields = get_genotypingid_viewer_infos(list_more, list_attributes)
            download_not_empty = True
            
            tag_fields.update({'all':genoids,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'user_projects':request.session['user_projects'],
                               'search_result':dico_get,
                               'or_and':or_and,
                               'download_not_empty':download_not_empty,
                               'dataview':True,
                               'refine_search':True})
            
            return render(request, template, tag_fields)
        
        #Query in the refine search
        elif "or_and" in request.GET:
            dico_get={}
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
            
            list_projects_ids = request.session['list_projects_ids']
            list_more = request.session['list_more']
            
            searched_genoids = GenotypingID.objects.viewer_refine_search(or_and, dico_get)
            
            list_projects = Project.objects.filter(id__in=list_projects_ids)
            
            filtered_genoids = GenotypingID.objects.select_related('sample','sample__seedlot','sample__seedlot__accession').prefetch_related('projects').filter(projects__in=list_projects).distinct()
            genoids_np = searched_genoids.intersection(filtered_genoids)
            genoids_np = genoids_np.order_by('name')
            number_all = len(genoids_np)
            genoids = add_pagination(request, genoids_np)
            
            tag_fields = get_genotypingid_viewer_infos(list_more, list_attributes)
            download_not_empty = True
            thread_name = ''
            if genoids:
                #Usage of a thread: the file is computed while the page is displayed
                thread_name = execute_function_in_thread(write_genoids_csv_from_viewer, [genoids_np, tag_fields, "Result search"], {})
                download_not_empty = True
                
            tag_fields.update({'all':genoids,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'user_projects':request.session['user_projects'],
                               'search_result':dico_get,
                               'or_and':or_and,
                               'download_not_empty':download_not_empty,
                               'thread_name':thread_name,
                               'dataview':True,
                               'refine_search':True})
            
            return render(request, template, tag_fields)
        
        formproject = DtviewProjectForm()
        
        if request.user and request.user.is_authenticated:
            username = request.user.login
            if User.objects.get(login=username).is_superuser():
                projects = Project.objects.all()
            else:
                username_id=User.objects.get(login=username).id
                projects = Project.objects.filter(users=username_id)
        elif settings.PUBLIC_INSTANCE :
            projects = Project.objects.all()
        else :
            projects = Project.objects.none()
            
        request.session['user_projects'] = list(projects.values_list("name",flat=True))

        formproject.fields['project'].queryset=projects.order_by('name')
        formgenoid = DataviewGenoIDForm()
        
        tag_fields = {'title':title,
                      'dataview':True,
                      'genoid_viewer':True,
                      'user_projects':projects,
                      'formproject':formproject,
                      'formgenoid':formgenoid}
        
        return render(request, 'dataview/choose_project.html', tag_fields)
    
    elif request.method == "POST":
        formproject = DtviewProjectForm(request.POST)
        formgenoid = DataviewGenoIDForm(request.POST)
        
        nb_per_page = 50
        
        if formproject.is_valid():
            list_projects = formproject.cleaned_data['project']
            list_more = request.POST.getlist('more_info')
            
            request.session['list_projects_ids'] = list(list_projects.values_list('id', flat=True))
            request.session['list_more'] = list_more

        else:
            return redirect('genotypingid_viewer')
        
        genoids_np = GenotypingID.objects.select_related('sample','sample__seedlot','sample__seedlot__accession').prefetch_related('projects').filter(projects__in=list_projects).distinct()
        genoids_np = genoids_np.order_by('name')
        number_all = len(genoids_np)
        genoids = add_pagination(request, genoids_np)
        
        tag_fields = get_genotypingid_viewer_infos(list_more, list_attributes)
        
        #Usage of a thread: the file is computed while the page is displayed
        thread_name = execute_function_in_thread(write_genoids_csv_from_viewer, [genoids_np, tag_fields, "genotypingid_viewer"], {})
        download_not_empty = True
        
        tag_fields.update({'all':genoids,
                           'number_all':number_all,
                           'user_projects':request.session['user_projects'],
                           'nb_per_page':nb_per_page,
                           'download_not_empty':download_not_empty,
                           'thread_name':thread_name,
                           'filename':"genotypingid_viewer",
                           'dataview':True,
                           'refine_search':True})
        
        return render(request, template, tag_fields)
        
        
def get_genotypingid_viewer_infos(list_more, list_attributes):
    """
      Return the data to display in the genotyping id viewer.
    """
    if list_more == ['1'] or list_more == [u'1']:
        list_attributes.insert(1,'sample')
        title = 'Genotyping ID and Samples linked to the project(s)'
        labels = ['Genotyping ID', 'Sample', 'Projects']
        names = ['name', 'sample', 'projects']
        
    elif list_more == ['2'] or list_more == [u'2']:
        list_attributes.insert(1,'seedlot')
        title = 'Genotyping ID and Seedlots linked to the project(s)'
        labels = ['Genotyping ID', 'Seedlot', 'Projects']
        names = ['name', 'seedlot', 'projects']
        
    elif list_more == ['3'] or list_more == [u'3']:
        list_attributes.insert(1,'accession')
        title = 'Genotyping ID and Accessions linked to the project(s)'
        labels = ['Genotyping ID', 'Accession', 'Projects']
        names = ['name', 'accession', 'projects']
        
    elif list_more == ['1','2'] or list_more == [u'1',u'2']:
        list_attributes.insert(1,'seedlot')
        list_attributes.insert(1,'sample')
        title = 'Genotyping ID, Samples and Seedlots linked to the project(s)'
        labels = ['Genotyping ID', 'Sample', 'Seedlot', 'Projects']
        names = ['name', 'sample', 'seedlot', 'projects']
        
    elif list_more == ['1','3'] or list_more == [u'1',u'3']:
        list_attributes.insert(1,'accession')
        list_attributes.insert(1,'sample')
        title = 'Genotyping ID, Samples and Accessions linked to the project(s)'
        labels = ['Genotyping ID', 'Sample', 'Accession', 'Projects']
        names = ['name', 'sample', 'accession', 'projects']
        
    elif list_more == ['2','3'] or list_more == [u'2',u'3']:
        list_attributes.insert(1,'accession')
        list_attributes.insert(1,'seedlot')
        title = 'Genotyping ID, Seedlots and Accessions linked to the project(s)'
        labels = ['Genotyping ID', 'Seedlot', 'Accession', 'Projects']
        names = ['name', 'seedlot', 'accession', 'projects']
        
    elif list_more == ['1','2','3'] or list_more == [u'1',u'2',u'3']:
        list_attributes.insert(1,'accession')
        list_attributes.insert(1,'seedlot')
        list_attributes.insert(1,'sample')
        title = 'Genotyping ID, Samples, Seedlots and Accessions linked to the project(s)'
        labels = ['Genotyping ID', 'Sample', 'Seedlot', 'Accession', 'Projects']
        names = ['name', 'sample', 'seedlot', 'accession', 'projects']
        
    else:
        title = 'Genotyping ID linked to the project(s)'
        labels = ['Genotyping ID', 'Projects']
        names = ['name', 'projects']
         
    tag_fields = {'title':title,
                  'fields_name':names,
                  'fields_label':labels,
                  'list_attributes':list_attributes,
                  }
        
    return tag_fields
        
def write_genoids_csv_from_viewer(genoids, tag_fields, title):
    """
      CSV writer specific for genoitypingID viewer, to display in separated columns 
      the genoid name and attributes's informations (accession, seedlot, sample).
    """
    filename = title.lower().replace(" ","_")
    dialect = Dialect(delimiter = ';')
    
    with open(settings.TEMP_FOLDER+filename+".csv", 'w', encoding='utf-8') as csv_file:
        writer = csv.writer(csv_file, dialect)
        writer.writerow(tag_fields['fields_label'])
        
        for genoid in genoids:
            row = []
            for attribute in tag_fields['fields_name']:
                if attribute == "seedlot":
                    attr_value = genoid.sample.seedlot.name
                elif attribute == "accession":
                    attr_value = genoid.sample.seedlot.accession.name
                elif attribute == "projects":
                    attr_value = ', '.join([str(att) for att in genoid.projects.filter(name__in=tag_fields['user_projects'])])
                else:
                    try:
                        attr_value = genoid.__getattribute__(attribute.lower().replace(' ','_'))
                    except:
                        attr_value = ""
                
                row.append(attr_value)
                
            writer.writerow(row)
            
    download_not_empty = True
    return download_not_empty


@login_required_on_private_instance
def experiment_viewer(request):
    number_all=0
    template="dataview/GenoExp_viewer.html"
    formproject=DtviewProjectForm()
    title="Experiment viewer"
    username = None
    number_all=0
    or_and = None
    dico_get={}
    type_data="experiment"
    projects_or_not=1
    if request.user and request.user.is_authenticated:
        username = request.user.login
        if User.objects.get(login=username).is_superuser():
            projects = Project.objects.all()
        else:
            username_id=User.objects.get(login=username).id
            projects = Project.objects.filter(users=username_id)
    elif settings.PUBLIC_INSTANCE :
        projects = Project.objects.all()
    else :
        projects = Project.objects.none()
    projects_allowed=', '.join([(v.name) for v in projects])
    projects_allowed = projects_allowed.split(', ')
    list_attributes=[]
    for i in ['experiment', 'projects']:
        list_attributes.append(i)
    
    if "page" in request.GET:
        if request.session['projects']!=[]:
            projects = request.session['projects']
            list_proj=[i for i in projects]
            list_projects=[]
            for i in list_proj:
                list_projects.append(Project.objects.get(id=i))
            experiments=_get_experiments(request, list_projects)
            labels = ['Experiment', 'Project']
            dico={}
            for i in experiments:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                dico[i.name]=[i.name,', '.join(projects_to_show)]
            if dico:
                experiments_all=Experiment.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                number_all=len(experiments_all)
                for i in experiments_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                            if k not in request.session['projects_to_show']:
                                request.session['projects_to_show'].append(k)
                    dico_all[i.name]=[i.name,', '.join(projects_to_show)]
                getPandasDf(settings.TEMP_FOLDER+"/dtview_experiment.csv",*labels, **dico_all)
            dico=dico.items()
            projects_or_not=1
            tag_fields = {'all':dico,
                          'fields_label': labels,
                          "title":title,
                          'creation_mode': True,
                          "pagination":experiments,
                          'projects_or_not':projects_or_not,
                          "experiment":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          "list_attributes":list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
                          "refine_search":True,
                          }
            return render(request, template, tag_fields)
    
    if request.method == 'POST' or "or_and" in request.GET :  
        tag_fields = {}
        if 'or_and' in request.GET and "no_search" not in request.GET:
            projects_to_show = request.session['projects_to_show']
            list_projects = request.session['list_projects']
            exp_name_list = request.session["exp_name_list"]
            dico_get={}
            or_and = None
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    if request.GET[data] == "experiment":
                        dico_get[data]={"name":request.GET[text_data]}
                    else:
                        dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
            length_dico_get = len(dico_get)
            nb_per_page = 10
            if or_and == "or":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query |= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            if data_type == "experiment":
                                data_type = "name"
                            var_search = str(data_type + "__icontains" ) 
                            query |= Query_dj(**{var_search : search_value}) 
            
            if or_and == "and":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query &= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            if data_type == "experiment":
                                data_type = "name"
                            var_search = str(data_type + "__icontains" )
                            query &= Query_dj(**{var_search : search_value})
            
            proj_allowed = Project.objects.filter(name__in=projects_to_show)
            query = Query_dj(query, projects__in=proj_allowed, name__in=exp_name_list)   
            experiments = Experiment.objects.filter(query).distinct() 
            number_all = len(experiments)
            list_exceptions = ['genotypingid','id','institution','person','date','comments']
            download_not_empty = False
            if experiments:
                download_not_empty = write_csv_search_result(experiments, list_exceptions)
            tag_fields.update({"download_not_empty":download_not_empty})
        
        elif "no_search" in request.GET:
            list_projects = request.session['list_projects']
            projects_or_not=1
            experiments=_get_experiments(request, list_projects)
            
        else:
            formproject=DtviewProjectForm(request.POST)
            formfiche = FicheForm(request.POST)
            if formproject.is_valid():
                list_projects_id = formproject.cleaned_data['project']
            list_projects = []
            for i in list_projects_id:
                list_projects.append(i.id)
            request.session['list_projects'] = list_projects
            experiments=_get_experiments(request, list_projects)
            exp_name_list=[]
            for i in experiments : 
                exp_name_list.append(i.name)
            request.session["exp_name_list"] = exp_name_list
            
        if projects_or_not==1:
            labels = ['Experiment', 'Project']
            dico={}
            request.session['projects_to_show']=[]
            for i in experiments:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                        if k not in request.session['projects_to_show']:
                            request.session['projects_to_show'].append(k)
                dico[i.name]=[i.name,', '.join(projects_to_show)] 
            if dico:
                experiments_all=Experiment.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                if number_all == 0:
                    number_all=len(experiments_all)
                for i in experiments_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                    dico_all[i.name]=[i.name,', '.join(projects_to_show)]
                getPandasDf(settings.TEMP_FOLDER+"/dtview_experiment.csv",*labels, **dico_all)   
            dico=dico.items()
            projects_or_not=1
            list_proj=[(i) for i in list_projects]
            request.session['projects']=list_proj
            tag_fields.update({'all':dico,
                          "title":title,
                          'fields_label': labels,
                          'creation_mode': True,
                          "pagination":experiments,
                          'projects_or_not':projects_or_not,
                          "experiment":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          "list_attributes":list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
                          })
            return render(request, template, tag_fields)
       
    else:
        projects_or_not = 0    
        formproject.fields['project'].queryset=projects.order_by('name')
    
        return render(request, template, {"type_data":type_data,'dataview':True,"projects":projects_allowed,'formproject':formproject,"projects_or_not":projects_or_not, "title":title})  
  
@login_required_on_private_instance
def referential_viewer(request):
    number_all=0
    template="dataview/GenoExp_viewer.html"
    formproject=DtviewProjectForm()
    title="Referential viewer"
    username = None
    number_all=0
    or_and = None
    dico_get={}
    type_data="referential"
    projects_or_not=1
    if request.user and request.user.is_authenticated:
        username = request.user.login
        if User.objects.get(login=username).is_superuser():
            projects = Project.objects.all()
        else:
            username_id=User.objects.get(login=username).id
            projects = Project.objects.filter(users=username_id)
    elif settings.PUBLIC_INSTANCE :
        projects = Project.objects.all()
    else :
        projects = Project.objects.none()
    projects_allowed=', '.join([(v.name) for v in projects])
    projects_allowed = projects_allowed.split(', ')
    list_attributes=[]
    for i in ['referential', 'projects']:
        list_attributes.append(i)
    
    if "page" in request.GET:
        if request.session['projects']!=[]:
            projects = request.session['projects']
            list_proj=[i for i in projects]
            list_projects=[]
            for i in list_proj:
                list_projects.append(Project.objects.get(id=i))
            referentials=_get_referentials(request, list_projects)
            labels = ['Referential', 'Project']
            dico={}
            for i in referentials:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                dico[i.name]=[i.name,', '.join(projects_to_show)]
            if dico:
                referential_all=Referential.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                number_all=len(referential_all)
                print("-----------> NUMBER ALL:   ",number_all)
                for i in referential_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                            if k not in request.session['projects_to_show']:
                                request.session['projects_to_show'].append(k)
                    dico_all[i.name]=[i.name,', '.join(projects_to_show)]
                getPandasDf(settings.TEMP_FOLDER+"/dtview_experiment.csv",*labels, **dico_all)
            dico=dico.items()
            projects_or_not=1
            
            tag_fields = {'all':dico,
                          'fields_label': labels,
                          "title":title,
                          'creation_mode': True,
                          "pagination":referentials,
                          'projects_or_not':projects_or_not,
                          "experiment":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          "list_attributes":list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
                          }
            
            return render(request, template, tag_fields)
    
    if request.method == 'POST' or "or_and" in request.GET :  
        tag_fields = {}
        if 'or_and' in request.GET and "no_search" not in request.GET:
            
            projects_to_show = request.session['projects_to_show']
            print('proj to show: ',projects_to_show)
            list_projects = request.session['list_projects']
            ref_name_list = request.session["ref_name_list"]
            dico_get={}
            or_and = None
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    if request.GET[data] == "referential":
                        dico_get[data]={"name":request.GET[text_data]}
                    else:
                        dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera

            length_dico_get = len(dico_get)
            nb_per_page = 10
            
            if or_and == "or":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query |= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            if data_type == "referential":
                                data_type = "name"
                            var_search = str(data_type + "__icontains" ) 
                            query |= Query_dj(**{var_search : search_value}) 
            
            if or_and == "and":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query &= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            print(data_type)
                            search_value = dico_type_data[data_type]
                            if data_type == "referential":
                                data_type = "name"
                            var_search = str(data_type + "__icontains" )
                            query &= Query_dj(**{var_search : search_value})
            
            proj_allowed = Project.objects.filter(name__in=projects_to_show)
            print('proj to show: ',projects_to_show)
            print('ref_name_list: ',ref_name_list)
            
            query = Query_dj(query, projects__in=proj_allowed, name__in=ref_name_list)   
            print('query: ', query)
            referentials = Referential.objects.filter(query).distinct() 
            number_all = len(referentials)
            list_exceptions = ['id',"genotypingid",'person','date']
            download_not_empty = False
            if referentials:
                download_not_empty = write_csv_search_result(referentials, list_exceptions)
            tag_fields.update({"download_not_empty":download_not_empty})
        
        elif "no_search" in request.GET:
            list_projects = request.session['list_projects']
            print("\n\n======+> TEST", list_projects)
            projects_or_not=1
            referentials=_get_referentials(request, list_projects)
            
        else:
            formproject=DtviewProjectForm(request.POST)
            formfiche = FicheForm(request.POST)
            if formproject.is_valid():
                list_projects_id = formproject.cleaned_data['project']
            list_projects = []
            for i in list_projects_id:
                list_projects.append(i.id)
            request.session['list_projects'] = list_projects
            referentials=_get_referentials(request, list_projects)
            ref_name_list=[]
            for i in referentials : 
                ref_name_list.append(i.name)
            request.session["ref_name_list"] = ref_name_list
            
        if projects_or_not==1:
            labels = ['Referential', 'Project']
            dico={}
            request.session['projects_to_show']=[]
            for i in referentials:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                        if k not in request.session['projects_to_show']:
                            request.session['projects_to_show'].append(k)
                dico[i.name]=[i.name,', '.join(projects_to_show)] 
            if dico:
                referentials_all=Referential.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                print("-----------> NUMBER ALL:   ",number_all)
                if number_all == 0:
                    number_all=len(referentials_all)
                for i in referentials_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                    dico_all[i.name]=[i.name,', '.join(projects_to_show)]
                getPandasDf(settings.TEMP_FOLDER+"/dtview_referential.csv",*labels, **dico_all)   
            dico=dico.items()
            projects_or_not=1
            list_proj=[(i) for i in list_projects]
            request.session['projects']=list_proj
            tag_fields.update({'all':dico,
                          "title":title,
                          'fields_label': labels,
                          'creation_mode': True,
                          "pagination":referentials,
                          'projects_or_not':projects_or_not,
                          "referential":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          "list_attributes":list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
                          })
            return render(request, template, tag_fields)
       
    else:
        projects_or_not = 0    
        formproject.fields['project'].queryset=projects.order_by('name')
    
        return render(request, template, {"type_data":type_data,'dataview':True,"projects":projects_allowed,'formproject':formproject,"projects_or_not":projects_or_not, "title":title})  


def _get_referentials(request, list_projects):
    referentials=Referential.objects.filter(projects__in=list_projects).distinct()
    paginator = Paginator(referentials,50)
    page = request.GET.get('page')
    try:
        referentials = paginator.page(page)
    except PageNotAnInteger:
        referentials = paginator.page(1)
    except EmptyPage:
        referentials = paginator.page(paginator.num_pages)
    return referentials


@login_required_on_private_instance
def environment_viewer(request):
    number_all=0
    template="dataview/GenoExp_viewer.html"
    formproject=DtviewProjectForm()
    title="Environment viewer"
    type_data="environment"
    username = None
    projects_or_not=1
    username = None
    or_and = None
    dico_get = {}
    if request.user and request.user.is_authenticated:
        username = request.user.login
        if User.objects.get(login=username).is_superuser():
            projects = Project.objects.all()
        else:
            username_id=User.objects.get(login=username).id
            projects = Project.objects.filter(users=username_id)
    elif settings.PUBLIC_INSTANCE :
        projects = Project.objects.all()
    else :
        projects = Project.objects.none()
    projects_allowed=', '.join([(v.name) for v in projects])
    projects_allowed = projects_allowed.split(', ')
    list_attributes=[]
    for i in ['environment', 'date', 'location', 'specific treatment', 'projects']:
        list_attributes.append(i)
    
    if "page" in request.GET:
        if request.session['projects']!=[]:
            projects = request.session['projects']
            list_proj=[i for i in projects]
            list_projects=[]
            for i in list_proj:
                list_projects.append(Project.objects.get(id=i))
            environments=_get_environments(request, list_projects)
            labels = ['Environment', 'Date', 'Location', 'Specific treatment', 'Project']
            dico={}
            for i in environments:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                        if k not in request.session['projects_to_show']:
                            request.session['projects_to_show'].append(k)
                dico[i.name]=[i.name,i.date,i.location,i.specific_treatment,', '.join(projects_to_show)]
            if dico:
                environments_all=Environment.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                number_all=len(environments_all)
                for i in environments_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                    dico_all[i.name]=[i.name,i.date,i.location,i.specific_treatment,', '.join(projects_to_show)]
                getPandasDf(settings.TEMP_FOLDER+"/dtview_environment.csv",*labels, **dico_all)
            dico=dico.items()
            projects_or_not=1
            
            tag_fields = {'all':dico,
                          'fields_label': labels,
                          "title":title,
                          'creation_mode': True,
                          "pagination":environments,
                          'projects_or_not':projects_or_not,
                          "environment":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          "list_attributes":list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
                          "refine_search":True,
                          }
            
            return render(request, template, tag_fields)
    
    if request.method == 'POST' or "or_and" in request.GET :  
        tag_fields = {}
        if 'or_and' in request.GET and "no_search" not in request.GET:
            projects_to_show = request.session['projects_to_show']
            list_projects = request.session['list_projects']
            env_name_list = request.session['env_name_list']
            dico_get={}
            or_and = None
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    if request.GET[data] == "environment":
                        dico_get[data]={"name":request.GET[text_data]}
                    else:
                        dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera

            length_dico_get = len(dico_get)
            nb_per_page = 10
            if or_and == "or":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query |= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            type_search = "__icontains"
                            if data_type == "environment":
                                data_type = "name"
                            elif data_type == "specific":
                                search_value = SpecificTreatment.objects.filter(name__icontains = dico_type_data[data_type])
                                data_type="specific_treatment"
                                type_search = "__in"
                            var_search = str(data_type + type_search )
                            query |= Query_dj(**{var_search : search_value}) 
            
            if or_and == "and":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query &= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            type_search = "__icontains"
                            if data_type == "environment":
                                data_type = "name"
                            elif data_type == "specific":
                                search_value = SpecificTreatment.objects.filter(name__icontains = dico_type_data[data_type])
                                data_type="specific_treatment"
                                type_search = "__in"
                            var_search = str(data_type + type_search )
                            query &= Query_dj(**{var_search : search_value})
            proj_allowed = Project.objects.filter(name__in=projects_to_show)
            query = Query_dj(query, projects__in=proj_allowed, name__in=env_name_list)   
            environments = Environment.objects.filter(query).distinct() 
            number_all = len(environments)
            list_exceptions = ['phenotypicvalue','description','id']
            download_not_empty = False
            if environments:
                download_not_empty = write_csv_search_result(environments, list_exceptions)
            tag_fields.update({"download_not_empty":download_not_empty})
        
        elif "no_search" in request.GET:
            list_projects = request.session['list_projects']
            projects_or_not=1
            environments=_get_environments(request, list_projects)
            
        else:
            formproject=DtviewProjectForm(request.POST)
            formfiche = FicheForm(request.POST)
            if formproject.is_valid():
                list_projects_id = formproject.cleaned_data['project']
            list_projects = []
            for i in list_projects_id:
                list_projects.append(i.id)
            request.session['list_projects'] = list_projects
            
            environments=_get_environments(request, list_projects)
            env_name_list=[]
            for i in environments:
                env_name_list.append(i.name)
            request.session['env_name_list']=env_name_list
        
        if projects_or_not==1:
            labels = ['Environment', 'Date', 'Location', 'Specific treatment', 'Project']
            dico={}
            request.session['projects_to_show']=[]
            
            for i in environments:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                        if k not in request.session['projects_to_show']:
                            request.session['projects_to_show'].append(k)
                dico[i.name]=[i.name,i.date,i.location,i.specific_treatment,', '.join(projects_to_show)]
            if dico:
                environments_all=Environment.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                if number_all == 0:
                    number_all=len(environments_all)
                for i in environments_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                    dico_all[i.name]=[i.name,i.date,i.location,i.specific_treatment,', '.join(projects_to_show)]
                getPandasDf(settings.TEMP_FOLDER+"/dtview_environment.csv",*labels, **dico_all)   
            dico=dico.items()
            projects_or_not=1
            list_proj=[(i) for i in list_projects]
            request.session['projects']=list_proj
            tag_fields.update({'all':dico,
                          "title":title,
                          'fields_label': labels,
                          'creation_mode': True,
                          "pagination":environments,
                          'projects_or_not':projects_or_not,
                          "environment":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          "list_attributes":list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
                          })
            
            return render(request, template, tag_fields)
       
    else:
        projects_or_not = 0    
        formproject.fields['project'].queryset=projects.order_by('name')
        return render(request, template, {"type_data":type_data,'dataview':True,"projects":projects_allowed,'formproject':formproject,"projects_or_not":projects_or_not, "title":title})  


def _get_environments(request, list_projects):
    environments=Environment.objects.filter(projects__in=list_projects).distinct()
    paginator = Paginator(environments,50)
    page = request.GET.get('page')
    try:
        environments = paginator.page(page)
    except PageNotAnInteger:
        environments = paginator.page(1)
    except EmptyPage:
        environments = paginator.page(paginator.num_pages)
    return environments


@login_required_on_private_instance
def trait_viewer(request):
    number_all=0
    template="dataview/GenoExp_viewer.html"
    formproject=DtviewProjectForm()
    title="Trait viewer"
    type_data="trait"
    username = None
    or_and = None
    dico_get={}
    number_all = 0
    projects_or_not=1
    if request.user and request.user.is_authenticated:
        username = request.user.login
        if User.objects.get(login=username).is_superuser():
            projects = Project.objects.all()
        else:
            username_id=User.objects.get(login=username).id
            projects = Project.objects.filter(users=username_id)
    elif settings.PUBLIC_INSTANCE :
        projects = Project.objects.all()
    else :
        projects = Project.objects.none()
    projects_allowed=', '.join([(v.name) for v in projects])
    projects_allowed = projects_allowed.split(', ')
    list_attributes=[]
    for i in ['trait', 'projects', 'abbreviation','comments','ontology_terms']:
        list_attributes.append(i)
    
    if "page" in request.GET:
        if request.session['projects']!=[]:
            projects = request.session['projects']
            list_proj=[i for i in projects]
            list_projects=[]
            for i in list_proj:
                list_projects.append(Project.objects.get(id=i))
            traits=_get_traits(request, list_projects)
            labels = ['Trait', 'Project', 'Abbreviation','Comments','Ontology Terms']
            dico={}
            for i in traits:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                        if k not in request.session['projects_to_show']:
                            request.session['projects_to_show'].append(k)
                dico[i.name]=[i.name,', '.join(projects_to_show),i.abbreviation,i.comments,i.ontology_terms]
            if dico:
                traits_all=Trait.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                number_all=len(traits_all)
                for i in traits_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                    dico_all[i.name]=[i.name,', '.join(projects_to_show),i.abbreviation,i.comments,i.ontology_terms]
                getPandasDf(settings.TEMP_FOLDER+"/dtview_trait.csv",*labels, **dico_all)
            dico=dico.items()
            projects_or_not=1
            
            tag_fields = {'all':dico,
                          'fields_label': labels,
                          "title":title,
                          'creation_mode': True,
                          "pagination":traits,
                          'projects_or_not':projects_or_not,
                          "trait":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          'list_attributes':list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
                          "refine_search":True,
                          }
            
            return render(request, template, tag_fields)
    
    
    if request.method == 'POST' or "or_and" in request.GET :  
        tag_fields = {}
        if 'or_and' in request.GET and "no_search" not in request.GET:
            projects_to_show = request.session['projects_to_show']
            list_projects = request.session['list_projects']
            trait_name_list = request.session["trait_name_list"]
            dico_get={}
            or_and = None
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    if request.GET[data] == "trait":
                        dico_get[data]={"name":request.GET[text_data]}
                    else:
                        dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
            length_dico_get = len(dico_get)
            nb_per_page = 10
            if or_and == "or":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query |= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            if data_type == "trait":
                                data_type = "name"
                            var_search = str(data_type + "__icontains" ) 
                            query |= Query_dj(**{var_search : search_value}) 
            
            if or_and == "and":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query &= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            if data_type == "trait":
                                data_type = "name"
                            var_search = str(data_type + "__icontains" )
                            query &= Query_dj(**{var_search : search_value})
            
            proj_allowed = Project.objects.filter(name__in=projects_to_show)
            query = Query_dj(query, projects__in=proj_allowed, name__in=trait_name_list)   
            traits = Trait.objects.filter(query).distinct() 
            number_all = len(traits)
            list_exceptions = ["phenotypicvalue","id","abbreviation","measure_unit","comments"]
            download_not_empty = False
            if traits:
                download_not_empty = write_csv_search_result(traits, list_exceptions)
            tag_fields.update({"download_not_empty":download_not_empty})
        
        elif "no_search" in request.GET:
            list_projects = request.session['list_projects']
            projects_or_not=1
            traits=_get_traits(request, list_projects)
            
        else:
            formproject=DtviewProjectForm(request.POST)
            formfiche = FicheForm(request.POST)
            if formproject.is_valid():
                list_projects_id = formproject.cleaned_data['project']
            list_projects = []
            for i in list_projects_id:
                list_projects.append(i.id)
            request.session['list_projects'] = list_projects
            traits=_get_traits(request, list_projects)
            trait_name_list=[]
            for i in traits : 
                trait_name_list.append(i.name)
            request.session["trait_name_list"] = trait_name_list
            

        if projects_or_not==1:
            labels = ['Trait','Project', 'Abbreviation','Comments', 'Ontology Terms']
            request.session['projects_to_show']=[]
            dico={}
            for i in traits:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                        if k not in request.session['projects_to_show']:
                            request.session['projects_to_show'].append(k)
                dico[i.name]=[i.name,', '.join(projects_to_show),i.abbreviation,i.comments, i.ontology_terms]
            if dico:
                traits_all=Trait.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                if number_all == 0:
                    number_all=len(traits_all)
                for i in traits_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                    dico_all[i.name]=[i.name,', '.join(projects_to_show),i.abbreviation,i.comments, i.ontology_terms]
                getPandasDf(settings.TEMP_FOLDER+"/dtview_trait.csv",*labels, **dico_all)   
            dico=dico.items()
            projects_or_not=1
            list_proj=[(i) for i in list_projects]
            request.session['projects']=list_proj
            tag_fields.update({'all':dico,
                          "title":title,
                          'fields_label': labels,
                          'creation_mode': True,
                          "pagination":traits,
                          'projects_or_not':projects_or_not,
                          "trait":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          'list_attributes':list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
                          })
            return render(request, template, tag_fields)
       
    else:
        projects_or_not = 0    
        formproject.fields['project'].queryset=projects.order_by('name')
        return render(request, template, {"refine_search":True,"type_data":type_data,'dataview':True,"projects":projects_allowed,'formproject':formproject,"projects_or_not":projects_or_not, "title":title})  


def _get_traits(request, list_projects):
    traits=Trait.objects.filter(projects__in=list_projects).distinct()
    paginator = Paginator(traits,50)
    page = request.GET.get('page')
    try:
        traits = paginator.page(page)
    except PageNotAnInteger:
        traits = paginator.page(1)
    except EmptyPage:
        traits = paginator.page(paginator.num_pages)
    return traits


def getPandasDf(name_of_file, *labels, **dico):
    df = pd.DataFrame(data=dico, index=None)
    df=df.transpose()
    df.columns=labels
    df.to_csv(name_of_file, index=False, encoding='utf-8')     
    return df


def _get_experiments(request, list_projects):
    experiments=Experiment.objects.filter(projects__in=list_projects).distinct()
    paginator = Paginator(experiments,50)
    page = request.GET.get('page')
    try:
        experiments = paginator.page(page)
    except PageNotAnInteger:
        experiments = paginator.page(1)
    except EmptyPage:
        experiments = paginator.page(paginator.num_pages)
    return experiments


@login_required
def aseedsam_viewer(request):
    if request.method == 'POST':
        formproject = DtviewProjectForm(request.POST)
        if formproject.is_valid():
            projects_name = []
            list_projects = formproject.cleaned_data['project']
            for project in list_projects:
                projects_name.append(project.name)
            listgenoid = []
            header = ['Genotyping ID','Sample','Seed Lot', 'Accession']
            accessions = Accession.objects.filter(projects__in=list_projects)
            seedlots = Seedlot.objects.filter(accession__in=accessions)
            samples = Sample.objects.filter(seedlot__in=seedlots)
            genotypingid = GenotypingID.objects.filter(sample__in=samples)
            for g in genotypingid:
                listgenoid.append(g.name)
            index = range(len(listgenoid))
            df = pd.DataFrame(columns=header, index=index)

            i = 0
            for g in genotypingid:
                df.set_value(i, 'Genotyping ID', g.name)
                df.set_value(i, 'Sample', g.sample)
                df.set_value(i, 'Seed Lot', g.sample.seedlot)
                df.set_value(i, 'Accession', g.sample.seedlot.accession)

                i += 1
            df = df.dropna(how='all')

            html = df.to_html(index=False, justify={'center'}, col_space='150px',)
            if df.empty:
                html = None
            
            return render(request, 'dataview/dtview_aseedsam.html', {'dataview':True,}, locals())
    else:
        formproject = DtviewProjectForm()
    return render(request, 'dataview/choose_project.html', {'accession_viewer':True,'dataview':True,'formproject':formproject})


@login_required_on_private_instance
def project_viewer(request):
    if request.method == 'POST':
        if "project_fiche" not in request.POST:
            formfiche = FicheForm(request.POST)
        else:
            fiche=request.POST["project_fiche"]
            formfiche = FicheForm()
    else:
        formfiche = FicheForm()
    projects = _get_projects(request)
    title = 'All projects'
    names = ['name', 'authors', 'users', 'institutions']
    labels = ['Project', 'Authors', 'Users', 'institutions']
    
    tag_fields = {'all': projects,
                  'title': title,
                  'fields_name': names,
                  'fields_label': labels,
                  'creation_mode': True,
                  'dataview':True,
                  }    
    if formfiche.is_valid() or "project_fiche" in request.POST:
        if "project_fiche" not in request.POST:
            fiche = request.POST['fiche']
        project = Project.objects.get(name=fiche)
        name = project.name
        authors = project.authors
        start_date = project.start_date
        end_date = project.end_date
        description = project.description
        experiment= Experiment.objects.filter(projects=project)
        project_experiment=[]
        for exp in experiment:
            project_experiment.append(exp.name)
        fiche="project_card"
        instit = ', '.join([p['name'] for p in project.institutions.values('name')])
        linked_files = ', '.join([p['name'] for p in project.linked_files.values('name')])
        
        return render(request, 'dataview/project_infos.html', locals())
    return render(request, 'dataview/dtview_tableau_projects.html', tag_fields)


def _get_projects(request):
    username = None
    if request.user and request.user.is_authenticated:
        username = request.user.login
        if User.objects.get(login=username).is_superuser():
            projects = Project.objects.all()
        else:
            username_id=User.objects.get(login=username).id
            projects = Project.objects.filter(users=username_id)
    elif settings.PUBLIC_INSTANCE :
        projects = Project.objects.all()
    else :
        projects = Project.objects.none()
    
    # Adding Paginator        
    paginator = Paginator(projects, 50)
    page = request.GET.get('page')
    try:
        projects = paginator.page(page)
    except PageNotAnInteger:
        # If page number is not an integer then page one 
        projects = paginator.page(1)
    except EmptyPage:
        # invalid page number show the last
        projects = paginator.page(paginator.num_pages)    
    return projects


@login_required_on_private_instance
def genealogy_viewer(request):
    username = None
    or_and = None
    if request.user.is_authenticated:
        username = request.user.login
    if "return" not in request.session:
        request.session['return']="no"
    formfiche=FicheForm()     
    list_attributes=[]
    for i in ['accession', 'type', 'pedigree', 'description', 'projects']:
        list_attributes.append(i)
    number_all=0
    tag_fields = {}
    request.session['return']="no"
     
    if request.method=='POST':
        
        projects_id = []
        #if request.POST.getlist('project') != []:
        projects_id = request.POST.getlist('project')
        #else:
            #projects = Project.objects.filter(users=User.objects.get(login=username).id)
            #projects_id = [project.id for project in projects]
        accession_all = Accession.objects.filter(projects__in = projects_id)
        seedlot_all = Seedlot.objects.filter(projects__in = projects_id)
        dico_relation = {}
        title = ''
        fields_label = []
        method_cat = { 'Accession Cross':2,
                       'Other Pedigree':4,
                       'Selfing':5,
                       'Seedlot Cross':3,
                       'Multiplication':1}
        
        titles = {'1':'Accession Cross Dataview',
                  '2':'Seedlot Cross Dataview',
                  '3':'Seedlot Multiplication Dataview',
                  '4':'Other Accession Pedigree Dataview',
                  '5':'Selfing Dataview'}
        
        fields_labels = {'1':['Name','Parent Male','Parent Female','First Production','Method','Comments'],
                         '2':['Name','Parent Male','Parent Female','Site','Start Date','End Date','Method','Comments'],
                         '3':['Name','Parent(s)','Site','Start Date','End Date','Method','Comments'],
                         '4':['Name','Parent Male','Parent Female','First Production','Method','Comments'],
                         '5':['Name','Parent(s)','First Production','Method','Comments']}
        
        all_infos = True
        
        if len(request.POST.getlist('genealogy_types')) == 1:
            title = titles[request.POST.getlist('genealogy_types')[0]]
            fields_label = fields_labels[request.POST.getlist('genealogy_types')[0]]
            all_infos = False
        else:
            title = 'Genealogy Dataview'
            fields_label = ['Name','Parent Male','Parent Female','Parent(s)','First Production','Site','Start Date','End Date','Method','Comments','Type of Reproduction']
        
        if '1' in request.POST.getlist('genealogy_types'):
            dico_relation = get_accession_cross_all_infos(accession_all, dico_relation, all_infos)
            
        if '2' in request.POST.getlist('genealogy_types'):
            dico_relation = get_seedlot_cross_all_infos(seedlot_all, dico_relation, all_infos)
            
        if '3' in request.POST.getlist('genealogy_types'):
            dico_relation = get_seedlot_multiplication_all_infos(seedlot_all, dico_relation, all_infos)
            
        if '4' in request.POST.getlist('genealogy_types'):
            dico_relation = get_other_accession_pedigree_all_infos(accession_all, dico_relation, all_infos)
            
        if '5' in request.POST.getlist('genealogy_types'):
            dico_relation = get_selfing_all_infos(accession_all, dico_relation, all_infos)
         
        request.session['genealogy_types'] = request.POST.getlist('genealogy_types')
        request.session['dico_relation'] = dico_relation
        request.session['title'] = title
        request.session['projects_id'] = projects_id
        request.session['fields_label'] = fields_label
        list_attributes = fields_label
        if dico_relation != {}:
            df = pd.DataFrame.from_dict(dico_relation, orient='index').reset_index()
            ## read csv cf locus data genotyping view
            df.columns=fields_label
            df.to_csv(settings.TEMP_FOLDER+'/dtview_genealogy.csv', encoding='utf-8', index=False)     
            return render(request,'dataview/genealogy_viewer.html', {'list_attributes':list_attributes,"refine_search":True,'title':title,'genealogy_viewer':True,'dataview':True,'dico_relation':dico_relation.items(),'fields_label':fields_label})
        else:
            df = pd.DataFrame(data=None, columns=fields_label)
            df.to_csv(settings.TEMP_FOLDER+'/dtview_genealogy.csv', encoding='utf-8', index=False)
            return render(request,'dataview/genealogy_viewer.html', {'title':title,'genealogy_viewer':True,'dataview':True,'dico_relation':dico_relation.items(),"number_all":number_all})

    else:
        if "or_and" in request.GET:
            genealogy_types = request.session['genealogy_types']
            data = request.session['dico_relation']
            title = request.session['title']
            projects_id = request.session['projects_id']
            fields_label = request.session['fields_label']
            list_attributes = fields_label
            form = None
            formf = None
            template = 'dataview/genealogy_viewer.html'
            df_test = pd.DataFrame.from_dict(data, orient='index')
            df_test.columns = fields_label[1:]

            dico_get={}
            or_and = None
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
                    
            length_dico_get = len(dico_get)
            nb_per_page = 10

            if or_and == 'and':
                
                df_empty = 1
                
                for dat, dico_type_data in dico_get.items():
                    for data_type, value in dico_type_data.items():
                        if df_empty == 1:
                            if data_type == "Name":
                                df_or_and = df_test.index.to_series().str.contains(value,case=False,na=False)
                            else:
                                df_or_and = df_test[data_type].str.contains(value,case=False,na=False)
                            df_empty = 2
                        else:
                            if data_type == "Name":
                                df_or_and = df_test.index.to_series().str.contains(value,case=False,na=False) & df_or_and
                            else:
                                df_or_and = df_test[data_type].str.contains(value,case=False,na=False) & df_or_and
            elif or_and == 'or':
                df_empty = 1
                for dat, dico_type_data in dico_get.items():
                    for data_type, value in dico_type_data.items():
                        if df_empty == 1:
                            if data_type == "Name":
                                df_or_and = df_test.index.to_series().str.contains(value,case=False,na=False)
                            else:
                                df_or_and = df_test[data_type].str.contains(value,case=False,na=False)
                            df_empty = 2
                        else:
                            if data_type == "Name":
                                df_or_and = df_test.index.to_series().str.contains(value,case=False,na=False) & df_or_and
                            else:
                                df_or_and = df_test[data_type].str.contains(value,case=False,na=False) & df_or_and
            #print(df_or_and)
            df = df_test[df_or_and]
            dico_locus = {}
    #         print('test1')
            df.index.names = ['name']
            #df_test = df.to_dict('index')
            df.reset_index()
            df_test = df.to_dict()
            dico = {}

            for i in df_test[fields_label[1]].keys():
                #print(i)
                dico[i]=[]
                for j in fields_label[1:]:
                    #print(j)
                    dico[i].append(df_test[j][i])
            
            if 'no_search' in request.GET:
                dico_get = False
                    
            return render(request,'dataview/genealogy_viewer.html', {"search_result":dico_get,"or_and":or_and,"refine_search":True,'list_attributes':list_attributes,'title':title,'genealogy_viewer':True,'dataview':True,'dico_relation':dico.items,'fields_label':fields_label})

        formproject = DtviewProjectForm()
        if request.user and request.user.is_authenticated:
            username = request.user.login
            if User.objects.get(login=username).is_superuser():
                projects = Project.objects.all()
            else:
                username_id=User.objects.get(login=username).id
                projects = Project.objects.filter(users=username_id)
        elif settings.PUBLIC_INSTANCE :
            projects = Project.objects.all()
        else :
            projects = Project.objects.none()
        projects_allowed=', '.join([(v.name) for v in projects])
        projects_allowed = projects_allowed.split(', ')
        formproject.fields['project'].queryset=projects.order_by('name')
        genealogytypeform = DtviewGenealogyTypeForm()
        
    return render(request,'dataview/choose_project.html', {'genealogy_viewer':True,'dataview':True,'projects':projects_allowed,'formproject':formproject,'genealogytypeform':genealogytypeform})


def get_accession_cross_all_infos(accession_all, dico_relation, all_infos):
    accession_ids = AccessionRelation.objects.filter(parent__in=accession_all, child__in=accession_all, reproduction__reproduction_method__category=2).values_list('child', flat=True).distinct() 
    for acc_id in accession_ids:
        acc = Accession.objects.get(id=acc_id)
        parent_male = AccessionRelation.objects.get(parent_gender = "M", child=acc)
        parent_female = AccessionRelation.objects.get(parent_gender = "F", child=acc)
        dico_relation[acc.name] = []
        dico_relation[acc.name].append(parent_male.parent.name)
        dico_relation[acc.name].append(parent_female.parent.name)
        if all_infos : dico_relation[acc.name].append('')
        dico_relation[acc.name].append(DateFormat(parent_male.first_production).format(settings.DATE_FORMAT)) if parent_male.first_production else dico_relation[acc.name].append('')
        if all_infos : dico_relation[acc.name].append('')
        if all_infos : dico_relation[acc.name].append('')
        if all_infos : dico_relation[acc.name].append('')
        dico_relation[acc.name].append(parent_male.reproduction.reproduction_method.name)
        dico_relation[acc.name].append(parent_male.reproduction.description)
        if all_infos : dico_relation[acc.name].append('Accession Cross')
    
    return dico_relation


def get_seedlot_cross_all_infos(seedlot_all, dico_relation, all_infos):
    seedlot_ids = SeedlotRelation.objects.filter(parent__in=seedlot_all, child__in=seedlot_all, reproduction__reproduction_method__category=3).values_list('child', flat=True).distinct() 
    for sl_id in seedlot_ids:
        sl = Seedlot.objects.get(id=sl_id)
        parent_male = SeedlotRelation.objects.get(parent_gender = "M", child=sl)
        parent_female = SeedlotRelation.objects.get(parent_gender = "F", child=sl)
        dico_relation[sl.name] = []
        dico_relation[sl.name].append(parent_male.parent.name)
        dico_relation[sl.name].append(parent_female.parent.name)
        if all_infos : dico_relation[sl.name].append('')
        if all_infos : dico_relation[sl.name].append('')
        dico_relation[sl.name].append(parent_male.site)
        dico_relation[sl.name].append(DateFormat(parent_male.reproduction.start_date).format(settings.DATE_FORMAT)) if parent_male.reproduction.start_date else dico_relation[sl.name].append('')
        dico_relation[sl.name].append(DateFormat(parent_male.reproduction.end_date).format(settings.DATE_FORMAT)) if parent_male.reproduction.end_date else dico_relation[sl.name].append('')
        dico_relation[sl.name].append(parent_male.reproduction.reproduction_method.name)
        dico_relation[sl.name].append(parent_male.reproduction.description)
        if all_infos : dico_relation[sl.name].append('Seedlot Cross')
        
    return dico_relation


def get_other_accession_pedigree_all_infos(accession_all, dico_relation, all_infos):
    accession_ids = AccessionRelation.objects.filter(parent__in=accession_all, child__in=accession_all, reproduction__reproduction_method__category=4).values_list('child', flat=True).distinct() 
    for acc_id in accession_ids:
        acc = Accession.objects.get(id=acc_id)
        parent_male = AccessionRelation.objects.filter(parent_gender = "M", child=acc)
        parent_female = AccessionRelation.objects.filter(parent_gender = "F", child=acc)
        list_p_male = []
        list_p_female = []
        for male in parent_male:
            parent = male
            list_p_male.append(male.parent.name)
        for female in parent_female:
            list_p_female.append(female.parent.name)
            
        dico_relation[acc.name] = []
        dico_relation[acc.name].append(' | '.join(list_p_male))
        dico_relation[acc.name].append(' | '.join(list_p_female))
        if all_infos : dico_relation[acc.name].append('')
        dico_relation[acc.name].append(DateFormat(parent.first_production).format(settings.DATE_FORMAT)) if parent.first_production else dico_relation[acc.name].append('')
        if all_infos : dico_relation[acc.name].append('')
        if all_infos : dico_relation[acc.name].append('')
        if all_infos : dico_relation[acc.name].append('')
        dico_relation[acc.name].append(parent.reproduction.reproduction_method.name)
        dico_relation[acc.name].append(parent.reproduction.description)
        if all_infos : dico_relation[acc.name].append('Other Accession Pedigree')
        
    return dico_relation


def get_seedlot_multiplication_all_infos(seedlot_all, dico_relation, all_infos):
    seedlot_ids = SeedlotRelation.objects.filter(parent__in=seedlot_all, child__in=seedlot_all, reproduction__reproduction_method__category=1).values_list('child', flat=True).distinct()
    for sl_id in seedlot_ids:
        sl = Seedlot.objects.get(id=sl_id)
        parent_x = SeedlotRelation.objects.filter(parent_gender = "X", child=sl)
        list_p_x = []
        for x in parent_x:
            parent = x
            list_p_x.append(x.parent.name)
            
        dico_relation[sl.name] = []
        if all_infos : dico_relation[sl.name].append('')
        if all_infos : dico_relation[sl.name].append('')
        dico_relation[sl.name].append(' | '.join(list_p_x))
        if all_infos : dico_relation[sl.name].append('')
        dico_relation[sl.name].append(parent.site)
        dico_relation[sl.name].append(DateFormat(parent.reproduction.start_date).format(settings.DATE_FORMAT)) if parent.reproduction.start_date else dico_relation[sl.name].append('')
        dico_relation[sl.name].append(DateFormat(parent.reproduction.end_date).format(settings.DATE_FORMAT)) if parent.reproduction.end_date else dico_relation[sl.name].append('')
        dico_relation[sl.name].append(parent.reproduction.reproduction_method.name)
        dico_relation[sl.name].append(parent.reproduction.description)
        if all_infos : dico_relation[sl.name].append('Seedlot Multiplication')
        
    return dico_relation


def get_selfing_all_infos(accession_all, dico_relation, all_infos):
    accession_ids = AccessionRelation.objects.filter(parent__in=accession_all, child__in=accession_all, reproduction__reproduction_method__category=5).values_list('child', flat=True).distinct() 
    for acc_id in accession_ids:
        acc = Accession.objects.get(id=acc_id)
        parent_x = AccessionRelation.objects.filter(parent_gender = "X", child=acc)
        list_p_x = []
        for x in parent_x:
            parent = x
            list_p_x.append(x.parent.name)
            
        dico_relation[acc.name] = []
        if all_infos : dico_relation[acc.name].append('')
        if all_infos : dico_relation[acc.name].append('')
        dico_relation[acc.name].append(' | '.join(list_p_x))
        dico_relation[acc.name].append(DateFormat(parent.first_production).format(settings.DATE_FORMAT)) if parent.first_production else dico_relation[acc.name].append('')
        if all_infos : dico_relation[acc.name].append('')
        if all_infos : dico_relation[acc.name].append('')
        if all_infos : dico_relation[acc.name].append('')
        dico_relation[acc.name].append(parent.reproduction.reproduction_method.name)
        dico_relation[acc.name].append(parent.reproduction.description)
        if all_infos : dico_relation[acc.name].append('Selfing')
    
    return dico_relation
    

class AccessionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated :
            return Accession.objects.none()
        username = None
        if self.request.user.is_authenticated:
            username = self.request.user
            username_id=User.objects.get(login=username).id
            projects=Project.objects.filter(users=username_id)

        if User.objects.get(login=username).is_superuser():
            qs = Accession.objects.all()
        else:       
            qs = Accession.objects.filter(projects__in=projects)
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs


class SeedlotAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Seedlot.objects.none()
        username = None
        if self.request.user.is_authenticated:
            username = self.request.user
            username_id=User.objects.get(login=username).id
            projects=Project.objects.filter(users=username_id)
            qs = Seedlot.objects.filter(projects__in=projects)
        if User.objects.get(login=username).is_superuser():
            qs = Seedlot.objects.all()
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs


class SampleAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Sample.objects.none()
        username = None
        if self.request.user.is_authenticated:
            username = self.request.user
            username_id=User.objects.get(login=username).id
            projects=Project.objects.filter(users=username_id)
            qs = Sample.objects.filter(projects__in=projects)
        if User.objects.get(login=username).is_superuser():
            qs = Sample.objects.all()
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs


class GenotypingIDAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return GenotypingID.objects.none()
        username = None
        if self.request.user.is_authenticated:
            username = self.request.user
            username_id=User.objects.get(login=username).id
            projects=Project.objects.filter(users=username_id)
            qs = GenotypingID.objects.filter(projects__in=projects)
        if User.objects.get(login=username).is_superuser():
            qs = GenotypingID.objects.all()
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs
    
    def get_result_label(self, result):
        """Return the label of a result."""
        label = "{0}".format(result)
        if self.template:
            return render_to_string(self.template, {"result": label})
        else:
            return six.text_type(label)


class ExperimentAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Experiment.objects.none()
        username = None
        if self.request.user.is_authenticated:
            username = self.request.user
            username_id=User.objects.get(login=username).id
            projects=Project.objects.filter(users=username_id)
            qs = Experiment.objects.filter(projects__in=projects)
        if User.objects.get(login=username).is_superuser():
            qs = Experiment.objects.all()
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs


class ReferentialAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Referential.objects.none()
        username = None
        if self.request.user.is_authenticated:
            username = self.request.user
            username_id = User.objects.get(login=username).id
            projects = Project.objects.filter(users=username_id)
            qs = Referential.objects.filter(projects__in=projects)
        if User.objects.get(login=username).is_superuser():
            qs = Referential.objects.all()
        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class ClassificationAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Classification.objects.none()
        username = None
        if self.request.user.is_authenticated:
            username = self.request.user
            username_id = User.objects.get(login=username).id
            projects = Project.objects.filter(users=username_id)
            qs = Classification.objects.filter(projects__in=projects)
        if User.objects.get(login=username).is_superuser():
            qs = Classification.objects.all()
        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class GenericAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        try :
            module_name = self.kwargs['module']
            class_name = self.kwargs['classname']
            mod = __import__(module_name)
            models = getattr(mod,'models')
            BaseClass = getattr(models, class_name)
        except :
            raise Http404
        if not self.request.user.is_authenticated:
            return BaseClass.objects.none()
        username = None
        if self.request.user.is_authenticated:
            username = self.request.user
            username_id=User.objects.get(login=username).id
            projects=Project.objects.filter(users=username_id)
            qs = BaseClass.objects.filter(projects__in=projects)
        if User.objects.get(login=username).is_superuser():
            qs = BaseClass.objects.all()
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs


class UserAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return User.objects.none()
        qs = User.objects.all()
        if self.q:
            qs=qs.filter(login__istartswith=self.q)
        return qs


class LocusAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Locus.objects.none()
        qs = Locus.objects.all()
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs


class EnvironmentAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Environment.objects.none()
        username = None
        if self.request.user.is_authenticated:
            username = self.request.user
            username_id=User.objects.get(login=username).id
            projects=Project.objects.filter(users=username_id)
            qs = Environment.objects.filter(projects__in=projects)
        if User.objects.get(login=username).is_superuser():
            qs = Environment.objects.all()
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs


class TraitAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Trait.objects.none()
        username = None
        if self.request.user.is_authenticated:
            username = self.request.user
            username_id=User.objects.get(login=username).id
            projects=Project.objects.filter(users=username_id)
            qs = Trait.objects.filter(projects__in=projects)
        if User.objects.get(login=username).is_superuser():
            qs = Trait.objects.all()
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs
    
def viewphenodata(request):
    return

def get_projects_to_show(request,i,projects_allowed):
    projects_to_be_seen = i.projects.filter()
    projects_allowed = str(projects_allowed).strip('[]')
    projects_allowed = (projects_allowed.strip("'"))
    projects_allowed = projects_allowed.split("', '")
    projects_to_show = []
    for k in projects_to_be_seen:
        k = (k.name)
        if k in projects_allowed:
            projects_to_show.append(k)
            if k not in request.session['projects_to_show']:
                request.session['projects_to_show'].append(k)
    return projects_to_show

def vcf_management(request):
    projects_or_not = 1
    if request.method == 'POST' or "or_and" in request.GET:
        print('MongoClient a remplacer')
        #TODO MongoClient a remplacer
        #TODO gridfs depend de PyMongo et doit donc etre remplace
#         client = MongoClient(settings.MONGODB_DATABASES['default']['host'],
# #                              settings.MONGODB_DATABASES['default']['port'])
# #         db = client[settings.MONGODB_DATABASES['default']['name']]
#         fs = gridfs.GridFSBucket(db)
#         path = settings.TEMP_FOLDER + '/' + request.POST['vcf_name'] + '.vcf'
#         tmpfile = open(path, 'wb+')
#         fs.download_to_stream_by_name(request.POST['vcf_name'], tmpfile)
#         client.close()
#         tmpfile.close()
#         with open(path, 'rb') as f:
#             response = HttpResponse(f.read(), content_type="text/plain")
#             response['Content-Disposition'] = 'attachment; filename=' + request.POST['vcf_name']
#         return response
#     return render(request, 'dataview/vcf_export.html',
#                   {'dataview': True, 'formvcf': VcfForm(), "projects_or_not":projects_or_not})
