from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test

def login_required_on_private_instance(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url=None):
    """
    Decorator for views that checks that the user is logged in, redirecting
    to the log-in page if necessary.
    """
    
    if settings.PUBLIC_INSTANCE:
        actual_decorator = user_passes_test(
            lambda u: not u.is_authenticated,
            login_url=login_url,
            redirect_field_name=redirect_field_name
        )
    else :
        actual_decorator = user_passes_test(
            lambda u: u.is_authenticated,
            login_url=login_url,
            redirect_field_name=redirect_field_name
        )
    if function:
        return actual_decorator(function)
    return actual_decorator