#-*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.urls import path
from dataview.views import AccessionAutocomplete, LocusAutocomplete,SeedlotAutocomplete,SampleAutocomplete, ExperimentAutocomplete, TraitAutocomplete, ReferentialAutocomplete, EnvironmentAutocomplete, GenotypingIDAutocomplete, UserAutocomplete, GenericAutocomplete, ClassificationAutocomplete
from dataview.views import dtgenohome, viewphenodata, crossViewer_management, referential_viewer, accession_viewer, data_card, genotypingid_viewer, \
                           experiment_viewer, trait_viewer, environment_viewer, phenoViewer_management, classViewer_management, classPieChart, accession_map,\
                           class_map_viewer_management, structure_viewer, vcf_management, genotyping_viewer, get_genotyping_viewer_experiments, get_genotyping_viewer_nb_locus
from dataview.views import aseedsam_viewer, project_viewer, genealogy_viewer
from commonfct.views import global_search, GlobalSearchAutocomplete
from commonfct.genericviews import return_csv

urlpatterns = [
    path('dthome/', dtgenohome, name='dtgenohome'),
    path('phenoViewer/', phenoViewer_management, name='phenoViewer_management'),
    path('viewpheno/', viewphenodata,name='viewphenodata'),
    path('genotypingviewer/', genotyping_viewer, name='genotyping_viewer'),
    path('classViewer/', classViewer_management, name='classViewer_management'),
    path('classMapViewer/', class_map_viewer_management,name='classMapViewer_management'),
    path('classPieChart/<str:classification>/<str:seedlot>/', classPieChart,name='classPieChart'),
    path('crossViewer/', crossViewer_management,name='crossViewer_management'),
    path('accessionViewer/', accession_viewer, name='accession_viewer'),
    path('accessionMap/', accession_map,name='accessionMap'),
    path('data_card/', data_card, name='data_card'),
    path('genotypingidViewer/', genotypingid_viewer, name='genotypingid_viewer'),
    path('experimentViewer/', experiment_viewer, name='experiment_viewer'),
    path('structureViewer/', structure_viewer, name='structure_viewer'),
    path('referentialViewer/', referential_viewer, name='referential_viewer'),
    path('traitViewer/', trait_viewer, name='trait_viewer'),
    path('environmentViewer/', environment_viewer, name='environment_viewer'),
    path('aseedsamViewer/', aseedsam_viewer, name='aseedsam_viewer'),
    path('projectViewer/', project_viewer, name='project_viewer'),
    path('genealogyViewer/', genealogy_viewer, name='genealogy_viewer'),
    #path('vcfExport/', vcf_management, name='vcf_management'),
    path('getexperiments/',get_genotyping_viewer_experiments,name='get_experiments'),
    path('getnblocus/',get_genotyping_viewer_nb_locus,name='get_nb_locus'),

    path('referential-autocomplete/', ReferentialAutocomplete.as_view(), name="referential-autocomplete"),
    path('trait-autocomplete/', TraitAutocomplete.as_view(), name='trait-autocomplete'),
    path('environment-autocomplete/', EnvironmentAutocomplete.as_view(), name='environment-autocomplete'),
    path('user-autocomplete/', UserAutocomplete.as_view(), name='user-autocomplete'),
    path('accession-autocomplete/', AccessionAutocomplete.as_view(), name='accession-autocomplete'),
    path('seedlot-autocomplete/', SeedlotAutocomplete.as_view(), name='seedlot-autocomplete'),
    path('sample-autocomplete/', SampleAutocomplete.as_view(), name='sample-autocomplete'),
    path('genotypingID-autocomplete/', GenotypingIDAutocomplete.as_view(), name='genotypingID-autocomplete'),
    path('experiment-autocomplete/', ExperimentAutocomplete.as_view(), name='experiment-autocomplete'),
    path('locus-autocomplete/', LocusAutocomplete.as_view(), name='locus-autocomplete'),
    path('classification-autocomplete/', ClassificationAutocomplete.as_view(), name='classification-autocomplete'),
    path('globalsearch/', global_search, name='globalsearch'),
    path('search-autocomplete/', GlobalSearchAutocomplete.as_view(), name='search-autocomplete'),
                       
                       
    path('generic-autocomplete/<str:module>/<str:classname>/', GenericAutocomplete.as_view(), name='generic-autocomplete'),
                       
    path('return_csv/<str:filename>/', return_csv,name='return_csv')
]