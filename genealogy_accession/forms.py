# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django import forms

from lot.models import SeedlotType, SeedlotTypeAttribute, Seedlot
from accession.models import Accession
from genealogy_managers.models import Reproduction_method, Reproduction
from genealogy_accession.models import AccessionRelation
from genealogy_seedlot.models import SeedlotRelation
from django.utils.safestring import mark_safe
from commonfct.forms_utils import HorizontalRadioSelect
from django.contrib.admin.widgets import FilteredSelectMultiple
from dal import autocomplete
import datetime

class AccessionHybridCrossForm(forms.Form):
    accession = forms.ModelChoiceField(required=True,
                                  queryset=Accession.objects.all(),
                                  widget=autocomplete.ModelSelect2(url='accession-autocomplete'))
    parent_male = forms.ModelChoiceField(required=True,
                                  queryset=Accession.objects.all(),
                                  widget=autocomplete.ModelSelect2(url='accession-autocomplete'))
    parent_female = forms.ModelChoiceField(required=True,
                                  queryset=Accession.objects.all(),
                                  widget=autocomplete.ModelSelect2(url='accession-autocomplete'))
    method = forms.ModelChoiceField(required=True,
                                  queryset=Reproduction_method.objects.filter(category=2))
    first_production = forms.DateField(widget=forms.TextInput(attrs={'placeholder': 'DD/MM/YYYY', 'class': 'datePicker'}))
    comments = forms.CharField(required=False)

    def create_hybrid_cross(self):
        repro = Reproduction.objects.create(reproduction_method = self.cleaned_data['method'], entity_type = 0, description = self.cleaned_data['comments'])
        male_relation = AccessionRelation.objects.create(reproduction=repro, parent = self.cleaned_data['parent_male'], child = self.cleaned_data['accession'], parent_gender = "M",
                                                     first_production = self.cleaned_data['first_production'])
        female_relation = AccessionRelation.objects.create(reproduction=repro, parent = self.cleaned_data['parent_female'], child = self.cleaned_data['accession'], parent_gender = "F",
                                                     first_production = self.cleaned_data['first_production'])
        return repro.id, male_relation.child, male_relation.parent, female_relation.parent, self.cleaned_data['method'].name, female_relation.first_production, repro.description
    
    
class OtherAccessionPedigreeForm(forms.Form):
    accession = forms.ModelChoiceField(required=True,
                                  queryset=Accession.objects.all(),
                                  widget=autocomplete.ModelSelect2(url='accession-autocomplete'))
    parent_male = forms.ModelMultipleChoiceField(required=True,
                                  queryset=Accession.objects.all(),
                                  widget=autocomplete.ModelSelect2Multiple(url='accession-autocomplete'))
    parent_female = forms.ModelMultipleChoiceField(required=True,
                                  queryset=Accession.objects.all(),
                                  widget=autocomplete.ModelSelect2Multiple(url='accession-autocomplete'))
    method = forms.ModelChoiceField(required=True,
                                  queryset=Reproduction_method.objects.filter(category=4))
    first_production = forms.DateField(widget=forms.TextInput(attrs={'placeholder': 'DD/MM/YYYY', 'class':'datePicker'}))
    comments = forms.CharField(required=False)

    def create_other_accession_pedigree(self):
        repro = Reproduction.objects.create(reproduction_method = self.cleaned_data['method'], entity_type = 0, description = self.cleaned_data['comments'])
        parent_male = []
        parent_female = []
        for parent_male_i in self.cleaned_data['parent_male']:
            male_relation = AccessionRelation.objects.create(reproduction=repro, parent = parent_male_i, child = self.cleaned_data['accession'], parent_gender = "M",
                                                     first_production = self.cleaned_data['first_production'])
            parent_male.append(male_relation.parent.name)
            
        for parent_female_i in self.cleaned_data['parent_female']:
            female_relation = AccessionRelation.objects.create(reproduction=repro, parent = parent_female_i, child = self.cleaned_data['accession'], parent_gender = "F",
                                                     first_production = self.cleaned_data['first_production'])
            parent_female.append(female_relation.parent.name)
            
        return repro.id, male_relation.child, parent_male, parent_female, self.cleaned_data['method'].name, female_relation.first_production, repro.description
    
    
    
class SelfingForm(forms.Form):
    accession = forms.ModelChoiceField(required=True,
                                  queryset=Accession.objects.all(),
                                  widget=autocomplete.ModelSelect2(url='accession-autocomplete'))
    parent_x = forms.ModelMultipleChoiceField(required=True,
                                  queryset=Accession.objects.all(),
                                  widget=autocomplete.ModelSelect2Multiple(url='accession-autocomplete'),
                                  label='Parent(s)')
    method = forms.ModelChoiceField(required=True,
                                  queryset=Reproduction_method.objects.filter(category=5))
    first_production = forms.DateField(widget=forms.TextInput(attrs={'placeholder': 'DD/MM/YYYY', 'class':'datePicker'}))
    comments = forms.CharField(required=False)
    
    def create_selfing(self):
        repro = Reproduction.objects.create(reproduction_method = self.cleaned_data['method'], entity_type = 0, description = self.cleaned_data['comments'])
        parent = []
        for parent_i in self.cleaned_data['parent_x']:
            relation = AccessionRelation.objects.create(reproduction=repro, parent = parent_i, child = self.cleaned_data['accession'], parent_gender = "X",
                                                     first_production = self.cleaned_data['first_production'])
            parent.append(relation.parent.name)
            
        return repro.id, relation.child, parent, self.cleaned_data['method'].name, relation.first_production, repro.description
    
    
    
    
    
    
    
    
    
    