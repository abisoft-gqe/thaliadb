from rest_framework import serializers

from genealogy_accession.models import AccessionRelation

class AccessionRelationParentSerializer(serializers.ModelSerializer):   
    """
        AccessionRelationParentSerializer gives the informations of the parent germplasm
    """
    germplasmDbId = serializers.CharField(source = 'parent.id')
    germplasmName = serializers.CharField(source = 'parent.name')
    parentType = serializers.CharField(source = 'getParentType')
    
    class Meta:
        model = AccessionRelation
        fields = [
            'germplasmDbId',
            'germplasmName',
            'parentType',
            ]
    
class AccessionRelationChildSerializer(serializers.ModelSerializer):   
    """
        AccessionRelationChildSerializer gives the informations of the child germplasm
    """
    germplasmDbId = serializers.CharField(source = 'child.id')
    germplasmName = serializers.CharField(source = 'child.name')
    parentType = serializers.CharField(source = 'getParentType')
    
    class Meta:
        model = AccessionRelation
        fields = [
            'germplasmDbId',
            'germplasmName',
            'parentType',
            ]
        