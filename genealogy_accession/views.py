# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import absolute_import

import datetime
import ast ##convertir des strings en dict
import csv
import json

from itertools import product

from django import forms
from django.http import HttpResponse
from django.http.response import Http404
from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.utils.datastructures import MultiValueDictKeyError
from django.db.models import Q
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.db import models, IntegrityError, transaction

from commonfct.utils import get_fields, add_pagination
from accession.models import Accession
from genealogy_managers.models import Reproduction_method, Reproduction
from genealogy_accession.models import AccessionRelation
from genealogy_managers.forms import MethodForm
from genealogy_accession.forms import AccessionHybridCrossForm, OtherAccessionPedigreeForm, SelfingForm
from genealogy_seedlot.forms import SeedlotHybridCrossForm, MultiplicationForm
from genealogy_seedlot.views import _create_genealogydata_from_file, fill_dico, refine_search
from accession.forms import UploadFileForm, UploadFileFormWithoutDelimiter


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def Accessionhybridcross_management(request):
    accessionhybridform = AccessionHybridCrossForm()
    template = "genealogy/hybridcross.html"
    names, labels = get_fields(accessionhybridform) 
    title = "Accession Hybrid Cross Management"
    list_attributes=['accession', 'parent male', 'parent female', "method", "first production", "comments"]
    list_exceptions = []
    formf = UploadFileFormWithoutDelimiter()
    filetype = "accessionhybridcross"
    typeid = 0
    errormsg = False
    type_method = 2 ##type method 2 => crossing method
    model_relation = "AccessionRelationCross"
    classname = "AccessionRelation"
    modelname = AccessionRelation
    data_or_null = 0
    dico = fill_dico(type_method, model_relation, modelname, data_or_null)
    tag_fields = {'title':title,
              'fields_name':names,
              'fields_label':labels,
              "admin":True,
              "form":accessionhybridform,
              "formfile":formf,
              'excel_empty_file':True,
              "creation_mode":True,
              'nb_per_page':50,
              'all':dico.items,
              "filetype":filetype,
              "typeid":typeid,
              "classname":"AccessionRelation",
              }
    if request.method == "POST":
        accessionhybridform = AccessionHybridCrossForm(request.POST)
        formf = UploadFileFormWithoutDelimiter(request.POST, request.FILES) 
        if accessionhybridform.is_valid():
            try:
                with transaction.atomic():
                    repro_id, accession, parent_male, parent_female, repro_method, first_production, comments = accessionhybridform.create_hybrid_cross()
        
                    dico[repro_id] = [accession, parent_male, parent_female, repro_method, first_production, comments]
                    
                    tag_fields.update({"all":dico.items,
                                       "hybridform":accessionhybridform,
                                       })
                    return render(request, template, tag_fields)
            
            except ValidationError as e:
                errormsg=e.message
                
        elif formf.is_valid():
            return _create_genealogydata_from_file(request, template, tag_fields, model_relation, modelname, type_method)
                
        else:
            errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in accessionhybridform.errors.items() ])
    return refine_search(request, accessionhybridform, formf, dico, template, list_attributes, list_exceptions, title, modelname, filetype, typeid, errormsg, model_relation, type_method, classname)


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def Otheraccessionpedigree_management(request):
    otheraccessionpedigreeform = OtherAccessionPedigreeForm()
    template = "genealogy/hybridcross.html"
    names, labels = get_fields(otheraccessionpedigreeform) 
    title = "Other Accession Pedigree Management"
    list_attributes=['accession', 'parent male', 'parent female', "method", "first production", "comments"]
    list_exceptions = []
    formf = UploadFileFormWithoutDelimiter()
    #filetype = "accessionhybridcross"
    filetype = "otheraccessionpedigree"
    typeid = 0
    errormsg = False
    type_method = 4 ##type method 2 => other accession pedigree
    model_relation = "AccessionRelationCross"
    classname = "AccessionRelation"
    modelname = AccessionRelation
    data_or_null = 0
    dico = fill_dico(type_method, model_relation, modelname, data_or_null)
    tag_fields = {'title':title,
              'fields_name':names,
              'fields_label':labels,
              "admin":True,
              "form":otheraccessionpedigreeform,
              "formfile":formf,
              'excel_empty_file':True,
              "creation_mode":True,
              'nb_per_page':50,
              'all':dico.items,
              "filetype":filetype,
              "typeid":typeid,
              "classname":"AccessionRelation",
              }
    if request.method == "POST":
        otheraccessionpedigreeform = OtherAccessionPedigreeForm(request.POST)
        formf = UploadFileFormWithoutDelimiter(request.POST, request.FILES) 
        if otheraccessionpedigreeform.is_valid():
            try:
                with transaction.atomic():
                    repro_id, accession, parent_male, parent_female, repro_method, first_production, comments = otheraccessionpedigreeform.create_other_accession_pedigree()
                    
                    dico[repro_id] = [accession, ' | '.join(parent_male), ' | '.join(parent_female), repro_method, first_production, comments]
                    
                    tag_fields.update({"all":dico.items,
                                       "hybridform":otheraccessionpedigreeform,
                                       })
                    
                    return render(request, template, tag_fields)
                    
            except ValidationError as e:
                errormsg=e.message
                
        elif formf.is_valid():
            return _create_genealogydata_from_file(request, template, tag_fields, model_relation, modelname, type_method)
                
        else:
            errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in otheraccessionpedigreeform.errors.items() ])
    return refine_search(request, otheraccessionpedigreeform, formf, dico, template, list_attributes, list_exceptions, title, modelname, filetype, typeid, errormsg, model_relation, type_method, classname)


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def selfing_management(request):
    """
      New genealogy category to declare single-parent accessions
    """
    selfingform = SelfingForm()
    template = "genealogy/hybridcross.html"
    names, labels = get_fields(selfingform)
    title = "Selfing Management"
    list_attributes=['accession', 'parent(s)', 'method', 'first production', 'comments']
    list_exceptions = []
    formf = UploadFileFormWithoutDelimiter()
    filetype = "selfing"
    typeid = 0
    errormsg = False
    type_method = 5 ##type method 5 => selfing
    #model_relation = "AccessionRelationCross"
    model_relation = "AccessionSelfing"
    classname = "AccessionRelation"
    modelname = AccessionRelation
    data_or_null = 0
    dico = fill_dico(type_method, model_relation, modelname, data_or_null)
    tag_fields = {'title':title,
                  'fields_name':names,
                  'fields_label':labels,
                  "admin":True,
                  "form":selfingform,
                  "formfile":formf,
                  'excel_empty_file':True,
                  "creation_mode":True,
                  'nb_per_page':50,
                  'all':dico.items,
                  "filetype":filetype,
                  "typeid":typeid,
                  "classname":"AccessionRelation",
                  }
    
    if request.method == "POST":
        selfingform = SelfingForm(request.POST)
        formf = UploadFileFormWithoutDelimiter(request.POST, request.FILES)
        if selfingform.is_valid():
            try:
                with transaction.atomic():
                    repro_id, accession, parent, repro_method, first_production, comments = selfingform.create_selfing()
                    dico[repro_id] = [accession, ' | '.join(parent), repro_method, first_production, comments]
                    
                    tag_fields.update({"all":dico.items,
                                       "hybridform":selfingform,
                                       })
                    
                    return render(request, template, tag_fields)
                
            except ValidationError as e:
                errormsg=e.message
                
        elif formf.is_valid():
            return _create_genealogydata_from_file(request, template, tag_fields, model_relation, modelname, type_method)
    
        else:
            errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in selfingform.errors.items() ])
    return refine_search(request, selfingform, formf, dico, template, list_attributes, list_exceptions, title, modelname, filetype, typeid, errormsg, model_relation, type_method, classname)


