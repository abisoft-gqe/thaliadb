==============================
Classification App Description
==============================

Classification models
=====================
.. automodule:: classification.models
    :members:

Classification views
====================
.. automodule:: classification.views
    :members:

Classification managers
=======================
.. automodule:: classification.managers
    :members:  