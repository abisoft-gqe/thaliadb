.. ThaliaDB documentation master file, created by
   sphinx-quickstart on Fri Jun 15 11:00:44 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ThaliaDB's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   rstfiles/accession
   rstfiles/team
   rstfiles/classification


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
