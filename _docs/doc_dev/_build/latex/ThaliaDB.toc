\select@language {english}
\contentsline {chapter}{\numberline {1}Accession App Description}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Accession models}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Accession forms}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Accession views}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Accession tests}{3}{section.1.4}
\contentsline {chapter}{\numberline {2}Team App Description}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Team models}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Team forms}{7}{section.2.2}
\contentsline {section}{\numberline {2.3}Team managers}{10}{section.2.3}
\contentsline {section}{\numberline {2.4}Team views}{10}{section.2.4}
\contentsline {section}{\numberline {2.5}Team tests}{13}{section.2.5}
\contentsline {chapter}{\numberline {3}Classification App Description}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Classification models}{15}{section.3.1}
\contentsline {section}{\numberline {3.2}Classification views}{15}{section.3.2}
\contentsline {section}{\numberline {3.3}Classification managers}{16}{section.3.3}
\contentsline {chapter}{\numberline {4}Indices and tables}{17}{chapter.4}
\contentsline {chapter}{Python Module Index}{19}{section*.78}
\contentsline {chapter}{Index}{21}{section*.79}
