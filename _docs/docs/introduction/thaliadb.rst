Thaliadb overview
=================


Login page
----------

.. figure:: ../_img/Introduction-login_page.png
   :align: center
   
   *Login page*

To access Thaliadb, a user account is needed. Accounts can be created by data
administrators and admin users. Without user account, it is possible to access information on the
database and user documentation.

Home page
---------

.. figure:: ../_img/Introduction-home_page.png
   :align: center
   
   *Home page*

Once logged in, the home page of the application is displayed. On this page, there is a research
bar that allow to search among objects in the database (more information on the research bar in the
Dataview part). There are also several tabs that allow navigation in the application.

*Home*
******
The first tab is Home, this page displays general information on the database and logos of the
partner units and projects.

*Dataview*
**********
This tab contains Thaliadb’s data visualization interfaces. A chapter of the documentation is
devoted to the description of the submenus and features of this tab.

*Admin*
*******
The admin tab is visible only for the users with administrator status. As indicated by his name,
this tab contains features to administrate data. All the submenus of this tab are detailed in a :ref:`specific chapter<Administration>` of this documentation.

*User profile*
**************

.. figure:: ../_img/Introduction-user_profile.png
   :align: center
   
   *User profile*

The user profile page indicates the projects to which the user has access, to visualize data in the
dataview interfaces of the database.

*Mailbox*
*********
The mailbox stores messages received when using the database, for example when submitting data files.

*Help*
******
The help tab give access to the user’s documentation and allow the users to report bugs in the issue tracker.

*Logout*
********
A click on this tab will allow a user to disconnect of the database.
