Dataview
========

.. container:: divright

   Thaliadb provides interfaces to explore information about genetics resources. To do this you will need a :ref:`user account<Users management>` and an access to one or several :ref:`projects<Give access to projects>`. 
   
   Thaliadb dataview gives an overview of the :ref:`projects<Projects viewer>` and the :ref:`accessions<Accessions viewer>` of the database. 
   It provides also exploring tools for :ref:`structure<Structure viewer>`, :ref:`genotyping<Genotyping viewer>` and :ref:`phenotyping<Phenotyping viewer>` data.

.. container:: divleft
   
   .. figure:: /_img/dataview/Dataview.png
   
      *Dataview menu*

   
.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: User reference guide
   
   search
   datacards
   overview
   projects-viewer
   accession-viewer
   structure-viewer
   genotyping-viewer
   phenotyping-viewer
   crossdata-viewer

