Global search bar
-----------------

The global search bar is located in the top right corner of the web interface, it is available everywhere in the interface. 
This feature works with an autocompletion search on the name of elements and enables a direct access to the :ref:`data card<Data cards>` following elements:

- :ref:`Accession<Accession management>`
- :ref:`Seedlot<Seed lot management>`
- :ref:`Sample<Sample>`
- :ref:`GenotypingID<GenotypingID>`
- :ref:`Classification<Structures>`
- :ref:`Trait<Traits>`
- :ref:`Environment<Environments>`
- :ref:`Experiment<Experiment>`
- :ref:`Referential<Referential>`
- :ref:`Project<Projects>`

.. figure:: /_img/dataview/searchbar.png
   :align: center
   
   *Global search bar in Thaliadb*