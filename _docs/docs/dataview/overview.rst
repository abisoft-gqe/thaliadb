Common features
---------------

Export file
***********

On most of the Thaliadb dataview interfaces an 'Export file' link is available to download data in CSV format.

.. figure:: /_img/dataview/Export-file.png
   :align: center
   
   *Export results table in a CSV file*

Pagination & display option
***************************

Most of Thaliadb dataview interfaces also provides various options : 

- You can select the number of row to display (10, 50 100, 200 or ALL)
- You can navigate in the results table with 'Next', 'Last', 'Previous' or 'First' links
- Or select the page number to reach this page in one click

.. figure:: /_img/dataview/Pagination.png
   :align: center
   
   *Pagination section*


Refine search
*************

The refine search form makes possible to filter visible rows in the table according to a textual search on the object type fields.

.. figure:: /_img/dataview/Refine-search.png
   :align: center
   
   *Filter results table*