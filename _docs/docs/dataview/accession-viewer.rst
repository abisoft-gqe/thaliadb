Accessions viewer
-----------------

Accession table
***************

The Accession table summarize information available for a set of Accessions selected by a 3 setp filtering process. In a first step, the user can select a list of projects, 
then in a second step select a list of accession types and then, in a third step, check if seed lot and/or sample have to be displayed in the table.

.. figure:: /_img/dataview/Viewer-Accessions-Filters.png

   *Accessions table filters*

The result table provides the :ref:`common features<Common features>`. It displays a list of accession with basic fields such as the pedigree, DOI, variety, latitutde, longitude. 
A direct link to the :ref:`accession card<Data cards>` is available.

.. figure:: /_img/dataview/Accessions.png
   
      *Accessions table list*

Genealogy table
***************
