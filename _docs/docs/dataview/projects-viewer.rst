Projects viewer
---------------

This view summarize all the projects a user can access. The table provides a direct link to the :ref:`project card<Data cards>` and displays information on the managers of this project, 
institutions involved and users that have an access to the data.

.. figure:: /_img/dataview/Projects.png
   
      *Projects table list*
