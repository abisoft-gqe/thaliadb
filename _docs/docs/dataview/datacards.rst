Data cards
----------

The data cards summarize information on each element of the database and displays the basic fields of it. The card provides links to the related objects :

- :ref:`Accession<Accession management>`
- :ref:`Seedlot<Seed lot management>`
- :ref:`Sample<Sample>`
- :ref:`GenotypingID<GenotypingID>`
- :ref:`Classification<Structures>`
- :ref:`Trait<Traits>`
- :ref:`Environment<Environments>`
- :ref:`Experiment<Experiment>`
- :ref:`Referential<Referential>`
- :ref:`Project<Projects>`

.. figure:: /_img/dataview/DataCard.png
   :align: center
   
   *Project data card*