..    include:: <isonum.txt>

Projects
--------

.. contents::
   :depth: 2
   :local:
   :backlinks: top

Create/Update projects
**********************

A project is the key element to share data with thaliadb :ref:`users<Users management>`. 

Projects can be managed in the menu *Admin > Team > Project*.

It is defined by the fields described in the table below. Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. Projects can be edited from the :ref:`table<Table>` below the form.

+----------------+------------------------------------------------------------------------------------------+
| Field name     | Description                                                                              |
|                |                                                                                          |
+================+==========================================================================================+
|                | The name of the project                                                                  |
| **Name**       | (for instance : Amaizing )                                                               |
+----------------+------------------------------------------------------------------------------------------+
|                | A string describing authors of this project                                              |
| **Authors**    |                                                                                          |
+----------------+------------------------------------------------------------------------------------------+
|                | Start date of the project                                                                |
| **Start date** |                                                                                          |
+----------------+------------------------------------------------------------------------------------------+
|                | End date of the project                                                                  |
| *End date*     |                                                                                          |
+----------------+------------------------------------------------------------------------------------------+
|                | Description of the project                                                               |
| *Description*  |                                                                                          |
+----------------+------------------------------------------------------------------------------------------+
|                | The list of :ref:`users<Users management>` who can access the data shared in this project|
| *Users*        |                                                                                          |
+----------------+------------------------------------------------------------------------------------------+
|                | List of :ref:`institutions<Institutions management>` involved in the project             |
| *Institutions* |                                                                                          |
+----------------+------------------------------------------------------------------------------------------+
|                | List of :ref:`documents<Manage documents in projects>` available for the project         |
| *Linked files* |                                                                                          |
+----------------+------------------------------------------------------------------------------------------+

Manage documents in projects
****************************


A document is a set of files that can be linked to a :ref:`project<Projects>`. 
It can be, for instance, a set of data files, a scienfic article with source data and/or scripts, a document describing the scientific project etc.
Documents are managed in the menu *Admin > Team > Documents*.

It is defined by the fields described in the table below. 
Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`.

+------------+-----------------------------------------------------------+
| Field name | Description                                               |
|            |                                                           |
+============+===========================================================+
|            | The name of the document                                  |
| **Name**   | (for instance : "Raw phenotyping data from 2019 to 2022") |
+------------+-----------------------------------------------------------+
|            | A predefined category describing the document :           |
| **Type**   | choose from the drop-down menu                            |
|            | (Project, AccessionImage, SeedlotImage, Referential,      |
|            | Phenotying, Article)                                      |
+------------+-----------------------------------------------------------+

Documents can be edited from the :ref:`table<Table>` below the form. This table is different of the classic one, you can add files to this Document by clicking on |addfile|, you will access a :ref:`form<Form>` to add a new file to this document.

.. figure:: /_img/admin/Documents-table.png
   :align: center
   
   *Admin documents table*

A file is defined by the fields described in the table below. 
Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`.

+--------------+--------------------------------------------------------------------+
| Field name   | Description                                                        |
|              |                                                                    |
+==============+====================================================================+
|              | The source file to upload in thaliadb                              |
| **Datafile** |                                                                    |
+--------------+--------------------------------------------------------------------+
|              | The :ref:`person<People management>` that provides this  data file |
| **Source**   |                                                                    |
+--------------+--------------------------------------------------------------------+
|              | A description or a comment for this file                           |
| *Comments*   |                                                                    |
+--------------+--------------------------------------------------------------------+

Give access to projects
***********************

One of the aim of thaliadb is to share :ref:`project<Projects>` data to a specific set of :ref:`users<Users management>`. 
To do so, browse to *Admin > Team > link data to projects*.

.. figure:: /_img/admin/Link-Users.png
   :align: center
   
   *Give access to projects*
   
From this interface you will be able to manage user access to :ref:`projects<Projects>`.
First, choose if you want to link or unlink users (1), select one or several projects (2) and one or several users (3).

If you choose to link users to projects, this action will add the selected users to the selected projects. Thus, these users will access to the data of these projects.

If you choose to unlink users from projects, this action will remove the selected users from the selected projects. Thus, these users won't have access anymore to the data of these projects.

Instead of selecting a list of users in the form, you can upload a list of users (4) in a simple text file and select the projects to link/unlink users.

Another way to give access to project data is to :ref:`edit the project<Create/Update projects>` itself. From the form, in edition mode, you can select/unselect the list of users who can access this project.

Share data in projects
**********************

Sharing data in projects works the same way than :ref:`giving users access to projects<Give access to projects>`. 
Sharing data in projects can be done through multiple objects of thaliadb :
:ref:`GenotypingID<GenotypingID>`,
:ref:`Sample<Sample>`,
:ref:`Seedlot<Seed lot management>`,
:ref:`Accession<Accession management>`,
:ref:`Experiment<Experiment>`,
:ref:`Referential<Referential>`,
:ref:`Environment<Environments>`,
:ref:`Trait<Traits>`,
:ref:`Classification<Structures>`.

.. figure:: /_img/admin/Link-data.png
   :align: center
   
   *Link data to projects*

From this interface you will be able to share data in :ref:`projects<Projects>`.
First, choose if you want to share or unshare data (1), select one or several projects (2) and one or several data objects (3).

If you choose to share data to projects, this action will add the selected objects to the selected projects. Thus, these objects will accessible from these projects.

If you choose to unshare data to projects, this action will remove the selected objects from the selected projects. Thus, these objects won't be accessible anymore from these projects.

Instead of selecting a list of objects in the form, you can upload a list of objects (4) in a simple text file and select the projects to share/unshare data.

**Important** : when you share objects in a project, raw data will be accessible only if all related objects are shared in the project. For instance :

* You want to share phenotyping data in a project. A phenotyping value is related to a :ref:`Seedlot<Seed lot management>`, a :ref:`Trait<Traits>`, an :ref:`Environment<Environments>`.
* You carried out a trial with 50 Seedlots, on one site one year, and measured the trait "Plant height"
* To make your data available in a project you need to share in this project : the 50 Seedlots, the Environment (1 site 1 year) and the Trait 'Plant height'.

.. |addfile| image:: /_img/folder_add.png

Specific sharing : Accession, Seedlot, Sample, GenotypingID
***********************************************************

Linking genetic material (Accession, Seedlot, Sample, GenotypingID) to projects can be achieve the same way than other objects. Anyway, specific rules will be applied for this kind of objects.

Sharing objects in projects will follow an ascending linking according to the genetic material organisation (:ref:`GenotypingID<GenotypingID>` |rarr| :ref:`Sample<Sample>` |rarr| :ref:`Seedlot<Seed lot management>` |rarr| :ref:`Accession<Accession management>`)
For instance in the figure below, *GenotypingID 1.1.1* is shared in the Amaizing project, the consequence is that *Sample 1.1*, *SeedLot1* and *Accession* objects will be automatically shared in the Amaizing project. However, this sharing rules only apply in the ascending way.
In the same figure, sharing *SeedLot2* will result in the automatic sharing of Accession in the project, but all other objects at a lower hierarchical level (*Sample2.1*, *Sample 2.2*, *GenotypingID 2.1.1*, *GenotypingID 2.2.1*, *GenotypingID 2.2.2*) won't be shared automatically in the project. 

.. figure:: /_img/material-link2projects/Diapositive1.PNG
   :align: center
   
   *Link genetic material to projects*

Unlink genetic material from a project will follow the reverse way of linking objects. For instance, if we consider the starting state of the DROPS project (figure below, 1/2) :
*Accession*, *SeedLot1* and all related Sample and GenotypingID are linked to the project.

.. figure:: /_img/material-link2projects/Diapositive2.PNG
   :align: center
   
   *Unlink genetic material from a projects (1/2)*

Unlink *Sample1.1* from the project will result in the automatic unlink event of *GenotypingID1.1.1* and *GenotypingID1.1.2*, but all the objects with a higher hierarchical level remains shared with the project. The new state is described in the figure 2/2.


.. figure:: /_img/material-link2projects/Diapositive3.PNG
   :align: center
   
   *Unlink genetic material from a projects (2/2)*
