Overview
--------

Here is an overview of the admin interfaces of Thaliadb. Most of them are build based on a similar structure:

* A form to create or update an item in the database
* A table of items already recorded in the database
* A section allowing to submit a file in order to create or update a list of items

Form
****
All administration interfaces in Thaliadb provide a form to record single objects in the database regarding its type (Accession, Seedlot, Sample, Trait etc...)

.. figure:: /_img/admin/institution-form.png
   :align: center
   
   *Example of admin form (Institutions form)*
   
The form include all the fields that describe an object type in Thaliadb. For instance, an institution will be described with a name, an acronym, an address, a postal code, a city, a country, a web site and a contact email.
When the form is filled with required information, the User can click on the "Create" button to valid the form. `Messages`_ will be displayed according to the result of the submission.

The administrator can also edit an object of the database if he click on the |edit| button in the `Table`_. In that case the form will be pre-filled with information of the edited object and the "Update" button will submit the modifications.

Table
*****

All administration interfaces in Thaliadb provide a table to display all recorded objects of a given type (Accession, Seedlot, Sample, Trait etc...)

.. figure:: /_img/admin/institution-table.png
   :align: center
   
   *Example of admin table (Institutions table)*

The table displays all the fields that describe an object type in Thaliadb. For instance, the institutions table will display the name, acronym, address, postal code, city, country, web site and contact email of each institution.

In the first column of the table the administrator can click on the |edit| button to access to the pre-filled `Form`_. He can also delete a single record by clicking on the |delete| button or select a list of objects with the check boxes and choose the "|delete| Delete selected" option.

The table provides also some display options such as :

* set the number of visible rows (10, 50, 100, 200 or ALL)
* pagination (previous and next page) and navigation (choose page number)
* filter the table with the refine search form

.. figure:: /_img/admin/refine-search.png
   :align: center
   
   *Refine search form*

The refine search form makes possible to filter visible rows in the table according to a textual search on the object type fields.

Load from file
**************

Most of administration interfaces in Thaliadb provides a feature to load data massively from files. By this way data can created or updated.

To create data, the administrator can download a template file ('Download headers for CSV file' link) while to update data the administrator can download a pre-filled file by clicking on the 'Download CSV file' link. 
As soon as the file is filled, the administrator can submit it in the file form and choose to insert new data or update existing data. You can't mix insertion and update so, regarding the clicked button, relevant `Messages`_ will be displayed.

.. figure:: /_img/admin/load-from-file.png
   :align: center
   
   *Load from file*

Messages
********

general description of messages


.. |edit| image:: /_img/edit.png
.. |delete| image:: /_img/cross.png

