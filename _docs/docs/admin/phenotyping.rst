Phenotyping
-----------

.. contents::
   :depth: 2
   :local:
   :backlinks: top
   
Specific Treatments
*******************

A Specific treatment is an action applying to an environment and characterizing a specific condition in a field trial.

These treatments can be managed in the menu *Admin > Phenotyping > Specific treatment*.

It is defined by the fields described in the table below. Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. Specific treatment can be edited from the :ref:`table<Table>` below the form.

+---------------+-------------------------------------+
| Field name    | Description                         |
|               |                                     |
+===============+=====================================+
|               | The name of this specific treatment |
| **Name**      |                                     |
+---------------+-------------------------------------+
|               | Description of the treatment.       |
| *Description* | (Nitrogen, water stress etc.)       |
+---------------+-------------------------------------+


Environments
************

An Environment is an object characterizing a field trial in a specific location a given year.

Environments can be managed in the menu *Admin > Phenotyping > Environment*.

It is defined by the fields described in the table below. Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. Environments can be edited from the :ref:`table<Table>` below the form.

+----------------------+-------------------------------------------+
| Field name           | Description                               |
|                      |                                           |
+======================+===========================================+
|                      | The name of this environment              |
| **Name**             |                                           |
+----------------------+-------------------------------------------+
|                      | The year of the trial                     |
| **Date**             |                                           |
+----------------------+-------------------------------------------+
|                      | The site for the experimentation          |
| **Location**         |                                           |
+----------------------+-------------------------------------------+
|                      | Description of the environment.           |
| *Description*        |                                           |
+----------------------+-------------------------------------------+
|                      | A specific treatment applied to the trial |
| *Specific treatment* |                                           |
+----------------------+-------------------------------------------+

Traits
******

A Trait is a quantifiable phenotypic characteristic.

Traits can be managed in the menu *Admin > Phenotyping > Trait*.

It is defined by the fields described in the table below. Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. Traits can be edited from the :ref:`table<Table>` below the form.

+------------------+---------------------------------------------+
| Field name       | Description                                 |
|                  |                                             |
+==================+=============================================+
|                  | The name of this trait                      |
| **Name**         |                                             |
+------------------+---------------------------------------------+
|                  | Short name used for this trait              |
| *Abbreviation*   |                                             |
+------------------+---------------------------------------------+
|                  | Description of this trait                   |
| *Comments*       |                                             |
+------------------+---------------------------------------------+
|                  | A list of ontology terms                    |
| *Ontology_terms* | (a term must unique in thaliadb ontologies) |
+------------------+---------------------------------------------+
|                  | Projects in which the trait is shared       |
| *Projects*       |                                             |
+------------------+---------------------------------------------+

Ontologies
**********

Thaliadb makes possible to describe ontologies commonly used in plant science. 

Ontologies can be managed in *Admin > Phenotyping > Ontology*.

It is defined by the fields described in the table below. Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. Ontologies can be edited from the :ref:`table<Table>` below the form.

+-----------------+-------------------------------------------------------------------------------------------------+
| Field name      | Description                                                                                     |
|                 |                                                                                                 |
+=================+=================================================================================================+
|                 | The unique id for this ontology                                                                 |
| **Ontology_id** | (for instance the Maize crop ontology is CO_322)                                                |
+-----------------+-------------------------------------------------------------------------------------------------+
|                 | A name for this ontology                                                                        |
| **Name**        |                                                                                                 |
+-----------------+-------------------------------------------------------------------------------------------------+
|                 | Description of this ontology                                                                    |
| *Description*   |                                                                                                 |
+-----------------+-------------------------------------------------------------------------------------------------+
|                 | A link that points to this ontology                                                             |
| *URL*           |                                                                                                 |
+-----------------+-------------------------------------------------------------------------------------------------+
|                 | The ontology file in `Trait Dictionary format <https://cgspace.cgiar.org/handle/10568/110906>`_ |
| *File*          |                                                                                                 |
+-----------------+-------------------------------------------------------------------------------------------------+

Phenotyping methods
*******************

A Phenotypic method is the statistical method used to compute phenotypic values stored in thaliadb.

These type of methods can be managed in the menu *Admin > Phenotyping > Statistical method*.

It is defined by the fields described in the table below. Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. Methods can be edited from the :ref:`table<Table>` below the form.

+---------------+------------------------------------------------------------------------------+
| Field name    | Description                                                                  |
|               |                                                                              |
+===============+==============================================================================+
|               | The name of this phenotypic method                                           |
| **Name**      |                                                                              |
+---------------+------------------------------------------------------------------------------+
|               | Description of the method                                                    |
| *Description* |                                                                              |
+---------------+------------------------------------------------------------------------------+
|               | A file describing or giving information on how the method have been computed |
| *Method file* |                                                                              |
+---------------+------------------------------------------------------------------------------+


Insert phenotyping data
***********************

Phenotyping data can be uploaded in Thaliadb with files. The form to submit data files can be found in the menu *Admin > Phenotyping > insert data*.

The file to submit must be a text CSV format with :ref:`seedlot<Seed lot management>` in line and :ref:`traits<Traits>` in column.

.. figure:: /_img/admin/Upload-Pheno-data.png
   :align: center
   
   *Form to upload Phenotypic data*
