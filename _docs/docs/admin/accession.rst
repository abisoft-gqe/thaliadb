Accession
---------

.. contents::
   :depth: 2
   :local:
   :backlinks: top

Defining Accession types and attributes
***************************************

.. container:: divright

   An accession type is a category of genetics material with common descriptors. It is one of the descriptors of an :ref:`Accession<Accession management>`. For instance, an accession type can be populations, inbred lines, hybrids etc.

   Accession type can be managed in the menu *Admin > Accession > Accession type*.

   It is defined by the fields described in the table below. Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`.
   

.. container:: divleft
   
   .. figure:: /_img/admin/AccessionMenu.png
   
      *Accession administration menu*

Accession types can be edited from the :ref:`table<Table>` below the form.

+---------------+--------------------------------------------------+
| Field name    | Description                                      |
|               |                                                  |
+===============+==================================================+
|               | The name of this accession type                  |
| **Name**      | (for instance : Population, Inbred line, Hybrid) |
+---------------+--------------------------------------------------+
|               | A text describing this accession type            |
| *Description* |                                                  |
+---------------+--------------------------------------------------+

:ref:`Accessions<Accession management>` are described regarding a set of fixed attributes (see below), common to all accession types. Thaliadb makes possible to add specific attributes to each accession type.   
Attributes can be managed in the menu 'Attributes' in the left navigation toolbar. An attribute is defined by the fields described in the table below. Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. Attributes can be edited from the :ref:`table<Table>` below the form.
   

+---------------+-----------------------------------------------------------------------------+
| Field name    | Description                                                                 |
|               |                                                                             |
+===============+=============================================================================+
|               | The name of this attribute                                                  |
| **Name**      |                                                                             |
+---------------+-----------------------------------------------------------------------------+
|               | A type defining the nature of the information to choose between :           |
| **Type**      | Short text, Long text, Number, URL                                          |
|               | The type will have consequences on the look of the widget in Accession form |
+---------------+-----------------------------------------------------------------------------+
|               | A short description for this attribute                                      |
| *Description* |                                                                             |
+---------------+-----------------------------------------------------------------------------+

As soon as your attributes are defined you can add them as additional descriptors in accession type. Adding attributes to an accession type will enrich the fields available when you create a new accession of this type. To do so, go back to the Accession type table and edit one of them. A new tab 'Attributes' will appear in the edition form.

In this interface, you can add attributes to the current accession type (1). By default, this attribute will be added at the last position. You can change this order by drag'n drop, as soon as you're fine with your attributes order you can click on the 'Save order' button (2).
This order that you defined will be respected in the Accession form and when you will export accession data from thaliadb.

.. figure:: /_img/admin/Attributes-management.png
   :align: center
   
   *Accession type attributes management*

Accession management
********************

An Accession is the upper level of the genetic material defined in thaliadb. It is an entry in national collection.

Accession can be managed in the menu *Admin > Accession > Accession type*. In the left navigation toolbar, a menu is available for each :ref:`accession type<Defining Accession types and attributes>` that have been defined in the tool.

It is defined at least by the fields described in the table below (additional fields can be defined as explained in the previous section). Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. Accessions can be edited from the :ref:`table<Table>` below the form.


+---------------+---------------------------------------------------------------------------------------------+
| Field name    | Description                                                                                 |
|               |                                                                                             |
+===============+=============================================================================================+
|               | The name of this Accession                                                                  |
| **Name**      |                                                                                             |
+---------------+---------------------------------------------------------------------------------------------+
|               | A Digital Object Identifier associated to this accession.                                   |
| *DOI*         | DOI can be considered as a unique identifier across different systems                       |
|               |                                                                                             |
+---------------+---------------------------------------------------------------------------------------------+
|               | A boolean attribute which indicates whether the DOI is private or public                    |
| *private DOI* |                                                                                             |
+---------------+---------------------------------------------------------------------------------------------+
|               | The pedigree of this Accession                                                              |
| *pedigree*    |                                                                                             |
+---------------+---------------------------------------------------------------------------------------------+
|               | Indicates if this accession is still used or not                                            |
| *is_obsolete* |                                                                                             |
+---------------+---------------------------------------------------------------------------------------------+
|               | A description for this Accession                                                            |
| *Description* |                                                                                             |
+---------------+---------------------------------------------------------------------------------------------+
|               | The variety of this accession                                                               |
| *Variety*     |                                                                                             |
+---------------+---------------------------------------------------------------------------------------------+
|               | The latitude of this Accession following MCPD definiton 15.1 :                              |
| *Latitude*    | Latitude expressed in decimal degrees. Positive values are North of the Equator;            |
|               | negative values are South of the Equator (e.g. -44.6975).                                   |
+---------------+---------------------------------------------------------------------------------------------+
|               | The longitude of this Accession following MCPD definiton 15.3 :                             |
| *Longitude*   | Longitude expressed in decimal degrees. Positive values are East of the Greenwich Meridian; |
|               | negative values are West of the Greenwich Meridian (e.g. +120.9123).                        |
+---------------+---------------------------------------------------------------------------------------------+
|               | Projects in which this Accession is accessible                                              |
| *Projects*    |                                                                                             |
+---------------+---------------------------------------------------------------------------------------------+

