Images
------

.. contents::
   :depth: 2
   :local:
   :backlinks: top

Single image management
***********************

Images can be loaded in thaliadb, it can describe accessions, seedlots or environments. the 3 types of objects can be combined 

Images can be managed in the menu *Admin > Team > Image*.

It is defined by the fields described in the table below. Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. Images can be edited from the :ref:`table<Table>` below the form.

+----------------+-----------------------------------------------------------------------------+
| Field name     | Description                                                                 |
|                |                                                                             |
+================+=============================================================================+
|                | The image to upload in thaliadb                                             |
| **Image path** |                                                                             |
+----------------+-----------------------------------------------------------------------------+
|                | The date the image was taken                                                |
| *Date*         |                                                                             |
+----------------+-----------------------------------------------------------------------------+
|                | A descritpion of this image                                                 |
| *Comment*      |                                                                             |
+----------------+-----------------------------------------------------------------------------+
|                | A list of :ref:`Accessions<Accession management>` represented by this image |
| *Accession*    |                                                                             |
+----------------+-----------------------------------------------------------------------------+
|                | A list of :ref:`Seedlots<Seed lot management>` represented by this image    |
| *Seedlot*      |                                                                             |
+----------------+-----------------------------------------------------------------------------+
|                | A list of :ref:`Environments<Environments>` represented by this image       |
| *Environment*  |                                                                             |
+----------------+-----------------------------------------------------------------------------+


Upload a set of images
**********************

Images can be uploaded in batch in the menu *Admin > Team > Upload Images*. 
In this interface you need to provide a file with the list of images you want to upload. The figure below shows the structure of this file. You can download a template file from thaliadb. 

The field 'IMAGE_NAME' list the name of the files, it must be strictly the same name, with extension (png, jpg etc.). 'ACCESSION', 'SEEDLOT' and 'ENVIRONMENT' must contains a list (comma separated) of objects of these types.
With the other field of the form you can choose multiple images files to upload and then click on 'Insert data'.

.. figure:: /_img/admin/Images-CSV.png
   :align: center
   
   *List file to upload images in batch*

In a second step you reach a multiple form prefilled with information of the images list file. It is possible at this step to modify information you will submit in thaliadb. Errors that occurred in the first step are also listed here.
As soon as you are fine with your modifications you can submit the form in thaliadb and upload images definitely.

.. figure:: /_img/admin/Images-upload.png
   :align: center
   
   *Multiple images upload , validation step*

