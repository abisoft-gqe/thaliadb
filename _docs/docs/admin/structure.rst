Structures
----------

.. contents::
   :depth: 2
   :local:
   :backlinks: top

Structure methods management
****************************

A Structure method is the statistical method used to established a group structure population.

These type of methods can be managed in the menu *Admin > Structure > Method*.

It is defined by the fields described in the table below. Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. Methods can be edited from the :ref:`table<Table>` below the form.

+---------------+------------------------------------------------------------------------------+
| Field name    | Description                                                                  |
|               |                                                                              |
+===============+==============================================================================+
|               | The name of this structure method                                            |
| **Name**      |                                                                              |
+---------------+------------------------------------------------------------------------------+
|               | Description of the method                                                    |
| *Description* |                                                                              |
+---------------+------------------------------------------------------------------------------+
|               | A file describing or giving information on how the method have been computed |
| *Method file* |                                                                              |
+---------------+------------------------------------------------------------------------------+


Manage structure datasets
*************************

A Structure dataset is a matrix describing genetic groups for a set of :ref:`seedlots<Seed lot management>`.

Structure can be managed in the menu *Admin > Structure > Structure*.

It is defined by the fields described in the table below. Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. Structure can be edited from the :ref:`table<Table>` below the form.

+---------------+------------------------------------------------------+
| Field name    | Description                                          |
|               |                                                      |
+===============+======================================================+
|               | The name of this structure method                    |
| **Name**      | (for instance : Amaizing )                           |
+---------------+------------------------------------------------------+
|               | Date of creation of this structure dataset           |
| **Date**      |                                                      |
+---------------+------------------------------------------------------+
|               | ???                                                  |
| *Reference*   |                                                      |
+---------------+------------------------------------------------------+
|               | The method used to compute this structure dataset    |
| **Method**    |                                                      |
+---------------+------------------------------------------------------+
|               | The person who build this structure dataset          |
| **Person**    |                                                      |
+---------------+------------------------------------------------------+
|               | A description of the structure dataset               |
| *Description* |                                                      |
+---------------+------------------------------------------------------+
|               | Projects in which the structure dataset is available |
| *Projects*    |                                                      |
+---------------+------------------------------------------------------+

Manage population groups
************************

A population group is a genetic groups explaining genetic structure of :ref:`accessions<Accession management>`.

Groups can be managed in the menu *Admin > Structure > Group*.

It is defined by the fields described in the table below. Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. Groups can be edited from the :ref:`table<Table>` below the form.


+--------------------+-----------------------------------------------+
| Field name         | Description                                   |
|                    |                                               |
+====================+===============================================+
|                    | The name of this population group             |
| **Name**           |                                               |
+--------------------+-----------------------------------------------+
|                    | Description of the group                      |
| *Description*      |                                               |
+--------------------+-----------------------------------------------+
|                    | Structure dataset in which this group is used |
| **Classification** |                                               |
+--------------------+-----------------------------------------------+

Insert structure data
*********************

Structure data can uploaded in Thaliadb with files. The form to submit data files can be found in the menu *Admin > Structure > add data*.

The file to submit must be a text CSV format with :ref:`seedlot<Seed lot management>` in line and :ref:`population groups<Manage population groups>` in column.

.. figure:: /_img/admin/Upload-structure-data.png
   :align: center
   
   *Form to upload structure data*
   

