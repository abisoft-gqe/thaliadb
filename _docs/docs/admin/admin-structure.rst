Administration
**************
..    include:: <isonum.txt>

Thaliadb makes possible to manage data about genetic resources. To do this you will need an administrator role (see below). Biological material is described by several objects with a hierarchical link :

:ref:`GenotypingID<GenotypingID>` |rarr| :ref:`Sample<Sample>` |rarr| :ref:`Seedlot<Seed lot management>` |rarr| :ref:`Accession<Accession management>`

A GenotypingID is described by one Sample, and a Sample can referred to multiple GenotypingID. 
A Sample is described by one Seedlot, and a Seedlot can referred to multiple Sample. 
A Seedlot is described by one Accession, and an Accession can referred to multiple Seedlot. 

Data in Thaliadb will be managed at different levels :

* :ref:`Genotyping<Genotyping>` data are managed at GenotypinID level
* :ref:`Phenotyping<Phenotyping>` and :ref:`Structure<Structures>` data are managed at Seedlot level
* :ref:`Genealogy<Genealogy>` and :ref:`Images<Images>` data are managed at Accession or Seedlot level

In Thaliadb, access to data is controlled by several mechanisms: 

*  :ref:`User<Users management>` account enabling an authentication system.
*  A role is given to each user: Read only user or administrator, administration can be done regarding data types.
*  Project access : To query data, users must be members of projects. Data are :ref:`shared in projects<Share data in projects>`.

.. toctree::
   :hidden:
   :caption: User reference guide
   :maxdepth: 2
   :glob:
   
   admin-interfaces
   team
   project
   accession
   lot
   genealogy
   images
   structure
   phenotyping
   genotyping
   
