Lots
----

.. contents::
   :depth: 2
   :local:
   :backlinks: top

Defining Seed lot types and attributes
**************************************

.. container:: divright

   A seedlot type is a category of seeds material with common descriptors. It is one of the descriptors of a :ref:`Seedlot<Seed lot management>`.
   
   Seedlot type can be managed in the menu *Admin > Lot > Lot type*.
   

.. container:: divleft
   
   .. figure:: /_img/admin/SeedlotMenu.png
   
      *Seedlot administration menu*

It is defined by the fields described in the table below. Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`.
Seedlot types can be edited from the :ref:`table<Table>` below the form.

+---------------+-------------------------------------+
| Field name    | Description                         |
|               |                                     |
+===============+=====================================+
|               | The name of this seedlot type       |
| **Name**      | (for instance : Hybrid)             |
+---------------+-------------------------------------+
|               | A text describing this seedlot type |
| *Description* |                                     |
+---------------+-------------------------------------+

:ref:`Seedlots<Seed lot management>` are described regarding a set of fixed attributes (see below), common to all seedlot types. Thaliadb makes possible to add specific attributes to each seedlot type.   
Attributes can be managed in the menu 'Attributes' in the left navigation toolbar. An attribute is defined by the fields described in the table below. Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. Attributes can be edited from the :ref:`table<Table>` below the form.
   

+---------------+---------------------------------------------------------------------------+
| Field name    | Description                                                               |
|               |                                                                           |
+===============+===========================================================================+
|               | The name of this attribute                                                |
| **Name**      |                                                                           |
+---------------+---------------------------------------------------------------------------+
|               | A type defining the nature of the information to choose between :         |
| **Type**      | Short text, Long text, Number, URL                                        |
|               | The type will have consequences on the look of the widget in seedlot form |
+---------------+---------------------------------------------------------------------------+
|               | A short description for this attribute                                    |
| *Description* |                                                                           |
+---------------+---------------------------------------------------------------------------+

As soon as your attributes are defined you can add them as additional descriptors in seedlot type. Adding attributes to a seedlot type will enrich the fields available when you create a new seedlot of this type. To do so, go back to the Seedlot type table and edit one of them. A new tab 'Attributes' will appear in the edition form.

In this interface, you can add attributes to the current seedlot type (1). By default, this attribute will be added at the last position. You can change this order by drag'n drop, as soon as you're fine with your attributes order you can click on the 'Save order' button (2).
This order that you defined will be respected in the Seedlot form and when you will export seedlot data from thaliadb.

.. figure:: /_img/admin/Attributes-management.png
   :align: center
   
   *Seedlot type attributes management works as Accession type attributes management*

Seed lot management
*******************

A Seedlot is the genetic material that will be used at the field in thaliadb. A seedlot is related to an :ref:`Accession<Accession management>`.

Seedlot can be managed in the menu *Admin > Lot > Lot type*. In the left navigation toolbar, a menu is available for each :ref:`seedlot type<Defining Seed lot types and attributes>` that have been defined in the tool.

It is defined at least by the fields described in the table below (additional fields can be defined as explained in the previous section). Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. Seedlot can be edited from the :ref:`table<Table>` below the form.


+---------------+------------------------------------------------+
| Field name    | Description                                    |
|               |                                                |
+===============+================================================+
|               | The name of this Seedlot                       |
| **Name**      |                                                |
+---------------+------------------------------------------------+
|               | The accession related to this seedlot          |
| **Accession** |                                                |
|               |                                                |
+---------------+------------------------------------------------+
|               | Indicates if this seedlot is still used or not |
| *is_obsolete* |                                                |
+---------------+------------------------------------------------+
|               | A description for this Seedlot                 |
| *Description* |                                                |
+---------------+------------------------------------------------+
|               | Projects in which this Seedlot is accessible   |
| *Projects*    |                                                |
+---------------+------------------------------------------------+

