Team
----

.. contents::
   :depth: 2
   :local:
   :backlinks: top

Institutions management
***********************

An institution is the partner structure that can be involved in a :ref:`project<Projects>`. 
It is necessary to create an institution before loading a new :ref:`person<People management>` in the database. 
Several users can share the same institution.

Institutions can be managed in the menu *Admin > Team > Institution*.

It is defined by the fields described in the table below. Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. Institutions can be edited from the :ref:`table<Table>` below the form.

+-------------+---------------------------------------------------------------------+
| Field name  | Description                                                         |
|             |                                                                     |
+=============+=====================================================================+
|             | The long name of an institution                                     |
| **Name**    | (for instance : Institut National                                   |
|             | de Recherche pour l'Agriculture, l'Alimentation et l'Environnement) |
+-------------+---------------------------------------------------------------------+
|             | The short name of an institution                                    |
| *Acronym*   | (for instance : INRAE)                                              |
+-------------+---------------------------------------------------------------------+
|             | The address of this institution                                     |
| *Address*   |                                                                     |
+-------------+---------------------------------------------------------------------+
|             | Post code (or zip code) of this institution                         |
| *Post code* |                                                                     |
+-------------+---------------------------------------------------------------------+
|             | The city of the institution                                         |
| *City*      |                                                                     |
+-------------+---------------------------------------------------------------------+
|             | The country of the institution                                      |
| *Country*   |                                                                     |
+-------------+---------------------------------------------------------------------+
|             | The web site of the institution                                     |
| *Web site*  |                                                                     |
+-------------+---------------------------------------------------------------------+
|             | The contact email of the institution                                |
| *Email*     |                                                                     |
+-------------+---------------------------------------------------------------------+



People management
*****************

A person is a partner that belongs to an :ref:`institution<Institutions management>`. A person can take part to several institutions.
It is necessary to create a person before loading a new :ref:`user<Users management>` in the database. 

People can be managed in the menu *Admin > Team > Person*.

It is defined by the fields described in the table below. Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. People can be edited from the :ref:`table<Table>` below the form.

+-------------------+---------------------------------+
| Field name        | Description                     |
|                   |                                 |
+===================+=================================+
|                   |                                 |
| **First name**    | The first name of the person    |
|                   |                                 |
+-------------------+---------------------------------+
|                   | The last name of the person     |
| **Last name**     |                                 |
+-------------------+---------------------------------+
|                   | Initial of this person          |
| *Initial*         |                                 |
+-------------------+---------------------------------+
|                   | Title of this person            |
| *Title of person* |                                 |
+-------------------+---------------------------------+
|                   | Languages of this person        |
| *Other language*  |                                 |
+-------------------+---------------------------------+
|                   | Work phone for this person      |
| *Work phone*      |                                 |
+-------------------+---------------------------------+
|                   | ?                               |
| *Work extension*  |                                 |
+-------------------+---------------------------------+
|                   | Fax number of the person        |
| *Fax number*      |                                 |
+-------------------+---------------------------------+
|                   | The contact email of the person |
| *Email address*   |                                 |
+-------------------+---------------------------------+
|                   | Additionnal information         |
| *Note on person*  |                                 |
+-------------------+---------------------------------+
|                   | Institution the person belongs  |
| *Institutions*    |                                 |
+-------------------+---------------------------------+

Users management
****************

A user is a :ref:`person<People management>` with an access account to Thaliadb.

Users can be managed in the menu *Admin > Team > User*.

It is defined by the fields described in the table below. Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. Users can be edited from the :ref:`table<Table>` below the form.

About admin checkboxes, note that if nothing is selected, the user will have a read only access to data.

+---------------------------+----------------------------------+
| Field name                | Description                      |
|                           |                                  |
+===========================+==================================+
|                           |                                  |
| **Login**                 | The authentication login         |
|                           | of this user                     |
+---------------------------+----------------------------------+
|                           | The authentication password      |
| **Password**              | of this user                     |
+---------------------------+----------------------------------+
|                           | :ref:`person<People management>` |
| **Person**                | linked to this user              |
+---------------------------+----------------------------------+
|                           | A boolean value informing if     |
| *Is team admin*           | this user can manage             |
|                           | team data                        |
+---------------------------+----------------------------------+
|                           | A boolean value informing if     |
| *Is accession admin*      | this user can manage             |
|                           | accession data                   |
+---------------------------+----------------------------------+
|                           | A boolean value informing if     |
| *Is seedlot admin*        | this user can manage             |
|                           | seedlot data                     |
+---------------------------+----------------------------------+
|                           | A boolean value informing if     |
| *Is classification admin* | this user can manage             |
|                           | classification data              |
+---------------------------+----------------------------------+
|                           | A boolean value informing if     |
| *Is phenotyping admin*    | this user can manage             |
|                           | phenotyping data                 |
+---------------------------+----------------------------------+
|                           | A boolean value informing if     |
| *Is genotyping admin*     | this user can manage             |
|                           | genotyping data                  |
+---------------------------+----------------------------------+
