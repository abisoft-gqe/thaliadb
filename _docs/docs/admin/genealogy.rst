Genealogy
---------

.. contents::
   :depth: 2
   :local:
   :backlinks: top

Breeding methods management
***************************

A Breeding method is the description of a schema used to conduct plant breeding.

Breeding methods can be managed in the menu *Admin > Genealogy > Method*.

It is defined by the fields described in the table below. 
Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. Breeding methods can be edited from the :ref:`table<Table>` below the form.

+---------------+----------------------------------------------------------------------------------------+
| Field name    | Description                                                                            |
|               |                                                                                        |
+===============+========================================================================================+
|               | The name of this breeding method                                                       |
| **Name**      |                                                                                        |
+---------------+----------------------------------------------------------------------------------------+
|               | A category describing the type of breeeding.                                           |
| **Category**  | The category will have consequences on the form to record a new genealogy relationship |
+---------------+----------------------------------------------------------------------------------------+
|               | A text describing this method                                                          |
| *Description* |                                                                                        |
+---------------+----------------------------------------------------------------------------------------+


Accession genealogy relationship management
*******************************************



Accession Hybrid Cross & Other Accession Pedigree
=================================================

An hybrid cross is the cross event between two parental line, one used as male parent, the other used as female parent. 
Other accession pedigree are described the same way, only the :ref:`breeding method<Breeding methods management>` will be different.

It is defined by the fields described in the table below. 
Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. This kind of genealogy relationship can be edited from the :ref:`table<Table>` below the form.

+--------------------+--------------------------------------------------------------------------+
| Field name         | Description                                                              |
|                    |                                                                          |
+====================+==========================================================================+
|                    | The accession resulting of this hybrid cross event                       |
| **Accession**      |                                                                          |
+--------------------+--------------------------------------------------------------------------+
|                    | The accession used as male parent                                        |
| **Parent male**    |                                                                          |
+--------------------+--------------------------------------------------------------------------+
|                    | The accession used as female parent                                      |
| **Parent female**  |                                                                          |
+--------------------+--------------------------------------------------------------------------+
|                    | The breeding method used for this event                                  |
| **Method**         | The breeding methods are displayed regarding the type of event created : |
|                    | hybrid cross or other pedigree                                           |
+--------------------+--------------------------------------------------------------------------+
|                    | The date of first production of this hybrid                              |
| *First production* |                                                                          |
+--------------------+--------------------------------------------------------------------------+
|                    | A text describing this breeding event                                    |
| *Comments*         |                                                                          |
+--------------------+--------------------------------------------------------------------------+

Selfing
=======

*Need a definition here of selfing breeding*

It is defined by the fields described in the table below. 
Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. This kind of genealogy relationship can be edited from the :ref:`table<Table>` below the form.

+--------------------+--------------------------------------------------------------------------+
| Field name         | Description                                                              |
|                    |                                                                          |
+====================+==========================================================================+
|                    | The accession resulting of this selfing event                            |
| **Accession**      |                                                                          |
+--------------------+--------------------------------------------------------------------------+
|                    | The accession(s) used to create the child accession                      |
| **Parent(s)**      |                                                                          |
+--------------------+--------------------------------------------------------------------------+
|                    | The breeding method used for this event                                  |
| **Method**         | The breeding methods are displayed regarding the type of event created : |
|                    | Selfing                                                                  |
+--------------------+--------------------------------------------------------------------------+
|                    | The date of first production of this accession                           |
| *First production* |                                                                          |
+--------------------+--------------------------------------------------------------------------+
|                    | A text describing this breeding event                                    |
| *Comments*         |                                                                          |
+--------------------+--------------------------------------------------------------------------+

Seedlot genealogy relationship management
*****************************************

Seedlot Hybrid Cross
====================

An hybrid cross is the cross event between two parental seed lot, one used as male parent, the other used as female parent. 

It is defined by the fields described in the table below. 
Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. This kind of genealogy relationship can be edited from the :ref:`table<Table>` below the form.

+-------------------+--------------------------------------------------------------------------+
| Field name        | Description                                                              |
|                   |                                                                          |
+===================+==========================================================================+
|                   | The seed lot resulting of this hybrid cross event                        |
| **Seedlot**       |                                                                          |
+-------------------+--------------------------------------------------------------------------+
|                   | The accession used as male parent                                        |
| **Parent male**   |                                                                          |
+-------------------+--------------------------------------------------------------------------+
|                   | The accession used as female parent                                      |
| **Parent female** |                                                                          |
+-------------------+--------------------------------------------------------------------------+
|                   | The breeding method used for this event                                  |
| **Method**        | The breeding methods are displayed regarding the type of event created : |
|                   | seedlot hybrid cross                                                     |
+-------------------+--------------------------------------------------------------------------+
|                   | The site that produced this seed lot                                     |
| *Site*            |                                                                          |
+-------------------+--------------------------------------------------------------------------+
|                   | Starting date of the cross event                                         |
| *Start date*      |                                                                          |
+-------------------+--------------------------------------------------------------------------+
|                   | End date of the crossing event                                           |
| *End date*        |                                                                          |
+-------------------+--------------------------------------------------------------------------+
|                   | A text describing this crossing event                                    |
| *Comments*        |                                                                          |
+-------------------+--------------------------------------------------------------------------+

Multiplication
==============

*Need a definition here of selfing breeding*

It is defined by the fields described in the table below. 
Fields in bold are required while italic ones are optional when submitting the :ref:`form<Form>`. This kind of genealogy relationship can be edited from the :ref:`table<Table>` below the form.

+---------------+--------------------------------------------------------------------------+
| Field name    | Description                                                              |
|               |                                                                          |
+===============+==========================================================================+
|               | The seed lot resulting of this hybrid cross event                        |
| **Seedlot**   |                                                                          |
+---------------+--------------------------------------------------------------------------+
|               | The seedlot(s) used to create the child seedlot                          |
| **Parent(s)** |                                                                          |
+---------------+--------------------------------------------------------------------------+
|               | The breeding method used for this event                                  |
| **Method**    | The breeding methods are displayed regarding the type of event created : |
|               | seedlot multiplication                                                   |
+---------------+--------------------------------------------------------------------------+
|               | The site that produced this seed lot                                     |
| *Site*        |                                                                          |
+---------------+--------------------------------------------------------------------------+
|               | Starting date of the multiplication event                                |
| *Start date*  |                                                                          |
+---------------+--------------------------------------------------------------------------+
|               | End date of the multiplication event                                     |
| *End date*    |                                                                          |
+---------------+--------------------------------------------------------------------------+
|               | A text describing this multiplication event                              |
| *Comments*    |                                                                          |
+---------------+--------------------------------------------------------------------------+

