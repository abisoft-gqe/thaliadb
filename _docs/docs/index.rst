.. Thaliadb documentation master file, created by
   sphinx-quickstart on Tue Jun 28 15:12:16 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Thaliadb
========

.. include:: introduction/introduction.rst

.. toctree::
   :hidden:
   :caption: Quick start
   
   introduction/install
   introduction/thaliadb
   
   
.. toctree::
   :hidden:
   :caption: User reference guide
   :titlesonly:
   
   admin/admin-structure
   dataview/dataview-structure
   api