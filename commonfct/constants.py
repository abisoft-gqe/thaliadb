# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

""" Router constants """
MONGO_TABLES = ["locus","genotypingvalue","locusproject"]

""" Models constants """
ATTRIBUTE_TYPES = ((1,"Short Text"),
                   (2,"Long Text"),
                   (3,"Number"),
                   (4,"URL"),
                   (5,"File")
                   )

METHOD_TYPE = (
               (1,"phenotyping"),
               (2,"classification"),
               (3,"accession"),
               (4,"lot")
               )

DOCUMENT_TYPE = (
                 (1,"Project"),
                 (2,"AccessionImage"),
                 (3,"SeedlotImage"),
                 (4,"Referential"),
                 (5,"Phenotyping"),
                 (6,"Article")
                 )


ACCEPTED_TRUE_VALUES = ('TRUE', 'True', 'true', 'T', 't', 'Yes', 'yes')

ACCEPTED_FALSE_VALUES = ('FALSE', 'False', 'false', 'F', 'f', 'No', 'no')

THREAD_ORIGIN = ((1,"Insert Genotyping data report"),)

""" View constants """

standard_allele = ['A','T','G','C','-','+']

iupac_convertion = {'R':'AG',
                    'Y':'CT',
                    'S':'GC',
                    'W':'AT',
                    'K':'GT',
                    'M':'AC',
                    'N':'NA',
                    '0':'-+',
                    '.':'-',
                    'B':'CGT',
                    'D':'AGT',
                    'H':'ACT',
                    'V':'ACG',
                    }

missing_data = {'NA':'NA',
                'NaN':'NA',
                '---':'NA'}


""" The filerenderer_headers dict is made to contain all files' headers to modify them easily if needed"""

filerenderer_headers = {'Accession':['Name','DOI','Private DOI','Pedigree','Is Obsolete','Description','Variety','Latitude','Longitude', 'Country of Origin','Donors','Institute Name','Institute Code','Projects'],
                        'Seedlot':['Name','Accession','Is Obsolete','Description','Projects'],
                        'Accession Cross':['Name','Parent Male','Parent Female','First Production','Method','Comments'],
                        'Seedlot Cross':['Name','Parent Male','Parent Female','Site','Start Date','End Date','Method','Comments'],
                        'Seedlot Multiplication':['Name','Parent(s)','Site','Start Date','End Date','Method','Comments'],
                        'Other Accession Pedigree':['Name','Parent Male','Parent Female','First Production','Method','Comments'],
                        'Selfing':['Name','Parent(s)','First Production','Method','Comments'],
                        'Locus':['Name','Comments','Genome version','Chromosome','Position'],
                        'Locus_position':['Name', 'Chromosome', 'Position'],
                        'Flat_matrix':['Genotyping_ID','Locus','Allele_value','Allelic_frequency'],
                        'Sample':['Name','Seedlot','Description','Is obsolete','Codelabo','Tubename','Projects'],
                        'GenotypingID':['Name','Sample','Experiment','Sentrixbarcode a','Sentrixposition a','Funding','Projects']}




