# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Laetitia Courgey

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import ast
import json
import csv
import sys
import collections

from types import *
from django.conf import settings
from django.http import HttpResponse
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
#import StringIO
#import xlsxwriter 

from team.models import Method
from accession.models import AccessionType, Accession, AccessionTypeAttribute
from genotyping.models import LocusType, Locus, Sample, GenotypingID, LocusPosition, Experiment, Referential

from classification.models import Classification, Classe
from lot.models import SeedlotType, Seedlot, SeedlotTypeAttribute
from genealogy_seedlot.models import SeedlotRelation
from genealogy_accession.models import AccessionRelation
from genealogy_managers.models import Reproduction, Reproduction_method
from phenotyping.models import Environment, SpecificTreatment, Trait, Ontology
from team.models import DataFile
from commonfct.constants import filerenderer_headers
from commonfct.utils import recode
from images.models import Image




@login_required
def filerenderer(request, filetype, type_id):
    print('filerenderer')
    #output = StringIO.StringIO()      
    #workbook = xlsxwriter.Workbook(output, {'in_memory': True})
    #test
    #csv.writer(open('eggs.csv', 'w'), delimiter=';',quotechar="", quoting=csv.QUOTE_MINIMAL)
    
#     with open("the_new_csv.csv", "w+") as to_file:
#         writer = csv.writer(to_file, delimiter=";")
#         for new_row in data:
#             writer.writerow(new_row)
    response = HttpResponse(content_type='text/csv') 
    writer = csv.writer(response,delimiter=';', quoting=csv.QUOTE_ALL)
    headers = []
    filename = None
    
    if request.method == "GET":   
        if filetype == 'ontology':
            try :
                id = request.GET['ont_id']
                print("id : "+str(id))
                obj = Ontology.objects.filter(ontology_id=id)[0]
                print("obj : "+str(obj))
                filename = obj.file
                print("filename : "+str(filename))
                #test_file = open(filename, 'rb')
                #print("aaa")
                response = HttpResponse(content=filename)
                response['Content-Disposition'] = 'attachment; filename="%s.csv"' %obj.ontology_id
                print(response)
                return response
                
            except :
                filename = "OntologyFile_headers.csv"
                fieldnames = ['Curation','Variable ID','Variable name','Variable synonyms','Context of use','Growth stage','Variable status','Variable Xref',
                            'Institution','Scientist', 'Date', 'Language','Crop', 'Trait ID', 'Trait name','Trait class','Trait description', 'Trait synonyms',    
                            'Main trait abbreviation','Alternative trait abbreviations','Entity','Attribute','Trait status','Trait Xref','Method ID','Method name',
                            'Method class','Method description','Formula','Method reference','Scale ID','Scale name','Scale class','Decimal places','Lower limit',
                            'Upper limit','Scale Xref','Category 1','Category 2','Category 3','Category 4','Category 5','Category 6','Category 7','Category 8','Category 9',
                            'Category 10']
                for field in fieldnames :
                    headers.append(field)
                writer.writerow(headers)
                response['Content-Disposition'] = 'attachment; filename=%s' %filename  
                return response
        
                 
        if filetype == 'sample':
            filename = filetype+'.csv'
            headers.append('Name')
            headers.append('Seedlot')
            headers.append('Description')
            headers.append('Is obsolete')
            headers.append('Codelabo')
            headers.append('Tubename')
            headers.append('Projects')
            writer.writerow(headers)
            if request.GET["requesttype"] == "all":
                sample_qs = Sample.objects.all().order_by('name')
                
                for sample in sample_qs:
                    line = []
                    line.append(sample.name)
                    line.append(sample.seedlot.name)
                    line.append(sample.description)
                    line.append(sample.is_obsolete)
                    line.append(sample.codelabo)
                    line.append(sample.tubename)
                    line.append(','.join([p.name for p in sample.projects.all()]))
                    writer.writerow(line)

        elif filetype == 'genotypingid':
            filename = filetype+'.csv'
            headers.append('Name')
            headers.append('Sample')
            headers.append('Experiment')
            headers.append('Sentrixbarcode a')
            headers.append('Sentrixposition a')
            headers.append('Funding')
            headers.append('Projects')
            writer.writerow(headers)
            
            if request.GET["requesttype"] == "all":
                genoid_qs = GenotypingID.objects.all().order_by('name')
                
                for genoid in genoid_qs:
                    line = []
                    line.append(genoid.name)
                    line.append(genoid.sample)
                    line.append(genoid.experiment)
                    line.append(genoid.sentrixbarcode_a)
                    line.append(genoid.sentrixposition_a)
                    line.append(genoid.funding)
                    line.append(','.join([p.name for p in genoid.projects.all()]))
                    writer.writerow(line)

    
        elif filetype =='accession':
            accessiontype = AccessionType.objects.get(id=int(type_id))
            filename = accessiontype.name+'.csv'
            attributes = accessiontype.accessiontypeattribute_set.all()
            
            #headers = filerenderer_headers['Accession']
            headers = Accession.ACCESSION_FILE_HEADERS.copy()
            
            for at in attributes:
                headers.append(str(at.name).strip())
            
            if request.GET['requesttype'] == 'all': headers.append('Parent Male')
            if request.GET['requesttype'] == 'all': headers.append('Parent Female')
            if request.GET['requesttype'] == 'all': headers.append('Parent(s)')
            writer.writerow(headers)
            #Une fois les en-tête récupérés, on vérifie si l'utilisateur veut récupérer toutes les données ou non
            if request.GET['requesttype'] == 'all': #Si l'utilisateur a cliqué sur 'Get CSV file, alors on récupère toutes les données
                accession_qs = Accession.objects.filter(type=accessiontype)
                
                for accession in accession_qs:
                    line = []
                    line.append(accession.name)
                    line.append(accession.doi)
                    line.append(accession.private_doi)
                    line.append(accession.pedigree)
                    line.append(accession.is_obsolete)
                    line.append(accession.description)
                    line.append(accession.variety)
                    line.append(accession.latitude)
                    line.append(accession.longitude)
                    line.append(accession.country_of_origin)
                    line.append(accession.donors)
                    line.append(accession.institute_name)
                    line.append(accession.institute_code)
                    line.append(' | '.join([p.name for p in accession.projects.all()]))
                    
                    if type(accession.attributes_values) is dict :
                        values = accession.attributes_values
                    else :
                        values = ast.literal_eval(accession.attributes_values)
                    for at in attributes :
                        line.append(values.get(str(at.id)))
                        
                    line.append(','.join([str(a.parent.name) for a in accession.relations_as_child.filter(parent_gender='M')]))
                    line.append(','.join([str(a.parent.name) for a in accession.relations_as_child.filter(parent_gender='F')]))
                    line.append(','.join([str(a.parent.name) for a in accession.relations_as_child.filter(parent_gender='X')]))
                    writer.writerow(line)
                
        elif filetype =='locus':
            locustype = LocusType.objects.get(id=int(type_id))
            filename = (locustype.name).lower().replace(' ','_')+'.csv'
            arg = locustype.locustypeattribute_set.all()
            headers.append('Name')
            headers.append('Comments') 
            for at in arg:
                headers.append(str(at.name))
            headers.append('Genome version')
            headers.append('Chromosome') 
            headers.append('Position')
            writer.writerow(headers)
        
        elif filetype == "experiment":
            filename = filetype+'.csv'
            headers.append('Name')
            headers.append('Institution') 
            headers.append('Person')          
            headers.append('Date') 
            headers.append('Comments')   
            headers.append('Projects')   
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                database = Experiment.objects.values_list('name')

                for entry_name in database:
                    line=[]
                    entry_value = Experiment.objects.filter(name=entry_name[0])[0] #On récupère l'entrée à l'aide de son nom
                    line.append((entry_value.name))
                    line.append((entry_value.institution.name))
                    line.append((entry_value.person.first_name)+" "+(entry_value.person.last_name))
                    line.append((entry_value.date.strftime("%x")))
                    line.append((entry_value.comments))
                    line.append((', '.join([i.name for i in entry_value.projects.filter()])))
                    writer.writerow(line)
                    
        elif filetype == "referential":
            filename = filetype+".csv"
            headers.append('Name')
            headers.append('Person')          
            headers.append('Date') 
            headers.append('Comments')   
            headers.append('Projects')   
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                database = Referential.objects.values_list('name')

                for entry_name in database:
                    line=[]
                    entry_value = Referential.objects.filter(name=entry_name[0])[0] #On récupère l'entrée à l'aide de son nom
                    line.append((entry_value.name))
                    line.append((entry_value.person.first_name)+" "+(entry_value.person.last_name))
                    line.append((entry_value.date.strftime("%x")))
                    line.append((entry_value.comments))
                    line.append((', '.join([i.name for i in entry_value.projects.filter()])))
                    writer.writerow(line)                    
        elif filetype == "treatment":
            filename = filetype+'.csv'
            headers.append('Name')
            headers.append('Description')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                database = SpecificTreatment.objects.values_list('name')
                for entry_name in database:
                    line = []
                    entry_value = SpecificTreatment.objects.filter(name=entry_name[0])[0]
                    line.append((entry_value.name))
                    line.append((entry_value.description))
                    writer.writerow(line)
                    
        elif filetype == "trait":
            filename = filetype + ".csv"
            headers.append('Name')
            headers.append('Abbreviation')
            headers.append('Comments')
            headers.append('Ontology terms')
            headers.append('Projects')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                database = Trait.objects.values_list('name')
                for entry_name in database:
                    line = []
                    entry_value = Trait.objects.filter(name = entry_name[0])[0]
                    line.append(entry_value.name)
                    line.append(entry_value.abbreviation)
                    line.append(entry_value.comments)
                    line.append(str(entry_value.ontology_terms))
                    line.append(', '.join([i.name for i in entry_value.projects.filter()]))
                    writer.writerow(line)
                    
                    
        elif filetype == "environment":
            filename = filetype + ".csv"
            headers.append('Name')
            headers.append('Date')
            headers.append('Location')
            headers.append('Description')
            headers.append('Specific treatment')
            headers.append('Projects')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                database = Environment.objects.values_list('name')
                for entry_name in database:
                    line = []
                    entry_value = Environment.objects.filter(name=entry_name[0])[0]
                    line.append((entry_value.name))
                    line.append((entry_value.date))
                    line.append((entry_value.location))
                    line.append((entry_value.description))
                    line.append((entry_value.specific_treatment))
                    line.append((', '.join([i.name for i in entry_value.projects.filter()])))
                    writer.writerow(line)
        elif filetype == "method":
            filename = filetype + ".csv"
            headers.append('Name')
            headers.append('Description')
            writer.writerow(headers)
            if type_id != 0 and request.GET['requesttype'] == 'all':
                database = Method.objects.filter(type=type_id).values_list('name')
                for entry_name in database:
                    line = []
                    entry_value = Method.objects.filter(name=entry_name[0])[0]
                    line.append((entry_value.name))
                    line.append((entry_value.description))
                    writer.writerow(line)
                    
        elif filetype == "image":
            filename = filetype + ".csv"
            headers.append('IMAGE_NAME')
            headers.append('DATE')
            headers.append('COMMENT')
            headers.append('ACCESSION')
            headers.append('SEEDLOT')
            headers.append('ENVIRONMENT')
            writer.writerow(headers)


        elif filetype == "seedlothybridcross":
            filename = filetype+".csv"
            headers.append('Name')
            headers.append('Parent Male')
            headers.append('Parent Female')
            headers.append('Method')
            headers.append('Site')
            headers.append('Start Date')
            headers.append('End Date')
            headers.append('Comments')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                dico_interm = {}
                #database = SeedlotRelation.objects.filter(reproduction__in=Reproduction.objects.filter(reproduction_method__in=Reproduction_method.objects.filter(type = 2)))
                database = SeedlotRelation.objects.filter(reproduction__in=Reproduction.objects.filter(reproduction_method__in=Reproduction_method.objects.filter(category = 3)))
                for entry_name in database:
                    line=[]
                    
                    if entry_name.child.name not in dico_interm:
                        dico_interm[entry_name.child.name]={}
                    if entry_name.parent_gender == "M":
                        dico_interm[entry_name.child.name]['parent_male'] = entry_name.parent.name
                    else:
                        dico_interm[entry_name.child.name]['parent_female'] = entry_name.parent.name
                    dico_interm[entry_name.child.name]['method'] = entry_name.reproduction.reproduction_method.name
                    dico_interm[entry_name.child.name]['comments']=entry_name.reproduction.description
                    dico_interm[entry_name.child.name]['site']=entry_name.site
                    dico_interm[entry_name.child.name]['start_date']=entry_name.reproduction.start_date
                    dico_interm[entry_name.child.name]['end_date']=entry_name.reproduction.end_date
                for i, j, in dico_interm.items():
                    line=[]
                    line.append(i)
                    line.append(j['parent_male'])
                    line.append(j['parent_female'])
                    line.append(j['method'])
                    line.append(j['site'])
                    line.append(j['start_date'])
                    line.append(j['end_date'])
                    line.append(j['comments'])
                    writer.writerow(line)
        
        elif filetype == "seedlotmultiplication":
            filename = filetype+".csv"
            headers.append('Name')
            headers.append('Parent(s)')
            headers.append('Method')
            headers.append('Site')
            headers.append('Start Date')
            headers.append('End Date')
            headers.append('Comments')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                dico_interm = {}
                #database = SeedlotRelation.objects.filter(reproduction__in=Reproduction.objects.filter(reproduction_method__in=Reproduction_method.objects.filter(type = 1)))
                database = SeedlotRelation.objects.filter(reproduction__in=Reproduction.objects.filter(reproduction_method__in=Reproduction_method.objects.filter(category = 1)))
                for entry_name in database:
                    line=[]
                    
                    if entry_name.child.name not in dico_interm:
                        dico_interm[entry_name.child.name]={}
                        
                    if 'parent' not in dico_interm[entry_name.child.name].keys():
                        dico_interm[entry_name.child.name]['parent'] = [entry_name.parent.name]
                    else:
                        dico_interm[entry_name.child.name]['parent'].append(entry_name.parent.name)
                        
                    dico_interm[entry_name.child.name]['method'] = entry_name.reproduction.reproduction_method.name
                    dico_interm[entry_name.child.name]['comments']=entry_name.reproduction.description
                    dico_interm[entry_name.child.name]['site']=entry_name.site
                    dico_interm[entry_name.child.name]['start_date']=entry_name.reproduction.start_date
                    dico_interm[entry_name.child.name]['end_date']=entry_name.reproduction.end_date
                for i, j, in dico_interm.items():
                    line=[]
                    line.append(i)
                    line.append(' | '.join(j['parent']))
                    line.append(j['method'])
                    line.append(j['site'])
                    line.append(j['start_date'])
                    line.append(j['end_date'])
                    line.append(j['comments'])
                    writer.writerow(line)
                    
        elif filetype == "accessionhybridcross":
            filename = filetype+".csv"
            headers.append('Name')
            headers.append('Parent Male')
            headers.append('Parent Female')
            headers.append('Method')
            headers.append('First Production')
            headers.append('Comments')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                dico_interm = {}
                #database = AccessionRelation.objects.filter(reproduction__in=Reproduction.objects.filter(reproduction_method__in=Reproduction_method.objects.filter(type = 2)))
                database = AccessionRelation.objects.filter(reproduction__in=Reproduction.objects.filter(reproduction_method__in=Reproduction_method.objects.filter(category = 2)))
                for entry_name in database:
                    line=[]
                    
                    if entry_name.child.name not in dico_interm:
                        dico_interm[entry_name.child.name]={}
                    if entry_name.parent_gender == "M":
                        dico_interm[entry_name.child.name]['parent_male'] = entry_name.parent.name
                    else:
                        dico_interm[entry_name.child.name]['parent_female'] = entry_name.parent.name
                    dico_interm[entry_name.child.name]['method'] = entry_name.reproduction.reproduction_method.name
                    dico_interm[entry_name.child.name]['comments']=entry_name.reproduction.description
                    dico_interm[entry_name.child.name]['first_production']=entry_name.first_production
                for i, j, in dico_interm.items():
                    line=[]
                    line.append(i)
                    line.append(j['parent_male'])
                    line.append(j['parent_female'])
                    line.append(j['method'])
                    line.append(j['first_production'])
                    line.append(j['comments'])
                    writer.writerow(line)    
                    
        elif filetype == "otheraccessionpedigree":
            filename = filetype+".csv"
            headers.append('Name')
            headers.append('Parent Male')
            headers.append('Parent Female')
            headers.append('Method')
            headers.append('First Production')
            headers.append('Comments')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                dico_interm = {}
                database = AccessionRelation.objects.filter(reproduction__in=Reproduction.objects.filter(reproduction_method__in=Reproduction_method.objects.filter(category = 4)))
                for entry_name in database:
                    line=[]
                    
                    if entry_name.child.name not in dico_interm:
                        dico_interm[entry_name.child.name]={}
                    if entry_name.parent_gender == "M":
                        if 'parent_male' not in dico_interm[entry_name.child.name].keys():
                            dico_interm[entry_name.child.name]['parent_male'] = [entry_name.parent.name]
                        else:
                            dico_interm[entry_name.child.name]['parent_male'].append(entry_name.parent.name)
                    else:
                        if 'parent_female' not in dico_interm[entry_name.child.name].keys():
                            dico_interm[entry_name.child.name]['parent_female'] = [entry_name.parent.name]
                        else:
                            dico_interm[entry_name.child.name]['parent_female'].append(entry_name.parent.name)
                    dico_interm[entry_name.child.name]['method'] = entry_name.reproduction.reproduction_method.name
                    dico_interm[entry_name.child.name]['comments']=entry_name.reproduction.description
                    dico_interm[entry_name.child.name]['first_production']=entry_name.first_production
                for i, j, in dico_interm.items():
                    line=[]
                    line.append(i)
                    line.append(' | '.join(j['parent_male']))
                    line.append(' | '.join(j['parent_female']))
                    line.append(j['method'])
                    line.append(j['first_production'])
                    line.append(j['comments'])
                    writer.writerow(line)
                    
        elif filetype == "selfing":
            filename = filetype+".csv"
            headers.append('Name')
            headers.append('Parent(s)')
            headers.append('Method')
            headers.append('First Production')
            headers.append('Comments')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                dico_interm = {}
                database = AccessionRelation.objects.filter(reproduction__in=Reproduction.objects.filter(reproduction_method__in=Reproduction_method.objects.filter(category = 5)))
                for entry_name in database:
                    line=[]
                    
                    if entry_name.child.name not in dico_interm:
                        dico_interm[entry_name.child.name]={}
                    
                    if 'parent' not in dico_interm[entry_name.child.name].keys():
                        dico_interm[entry_name.child.name]['parent'] = [entry_name.parent.name]
                    else:
                        dico_interm[entry_name.child.name]['parent'].append(entry_name.parent.name)
                        
                    dico_interm[entry_name.child.name]['method'] = entry_name.reproduction.reproduction_method.name
                    dico_interm[entry_name.child.name]['comments']=entry_name.reproduction.description
                    dico_interm[entry_name.child.name]['first_production']=entry_name.first_production

                for i, j, in dico_interm.items():
                    line=[]
                    line.append(i)
                    line.append(' | '.join(j['parent']))
                    line.append(j['method'])
                    line.append(j['first_production'])
                    line.append(j['comments'])
                    writer.writerow(line)

        elif filetype == "method":
            filename = filetype + ".csv"
            headers.append('Name')
            headers.append('Description')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                database = Method.objects.values_list('name')
                for entry_name in database:
                    line = []
                    entry_value = Method.objects.filter(name=entry_name[0])[0]
                    line.append((entry_value.name))
                    line.append((entry_value.description))
                    writer.writerow(line)
        elif filetype == "classification":
            filename = filetype + ".csv"
            headers.append('Name')
            headers.append('Date')
            headers.append('Reference')
            headers.append('Method')
            headers.append('Person')
            headers.append('Description')
            headers.append('Projects')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                database = Classification.objects.values_list('name')
                for entry_name in database:
                    line = []
                    entry_value = Classification.objects.filter(name=entry_name[0])[0]
                    line.append((entry_value.name))
                    line.append((entry_value.date))
                    line.append((entry_value.reference))
                    line.append((entry_value.method))
                    line.append((entry_value.person))
                    line.append((entry_value.description))
                    line.append(('|'.join([i.name for i in entry_value.projects.filter()])))
                    writer.writerow(line)
                    
        elif filetype == "classe":
            filename = filetype + ".csv"
            headers.append('Name')
            headers.append('Description')
            headers.append('Classification')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                database = Classe.objects.all()
                for entry_value in database:
                    line = []
                    #entry_value = Classe.objects.filter(name=entry_name[0])[0]
                    line.append((entry_value.name))
                    line.append((entry_value.description))
                    line.append(entry_value.classification)
                    writer.writerow(line)  
        else :
            seedlottype = SeedlotType.objects.get(id=int(type_id))
            filename = seedlottype.name+'.csv'
            attributes = seedlottype.seedlottypeattribute_set.all()
            headers.append('Name')
            headers.append('Accession')
            headers.append('Is Obsolete')
            headers.append('Description')
            headers.append('Projects')
            
            for at in attributes:
                headers.append(str(at.name))
            
            if request.GET['requesttype'] == 'all': headers.append('Parent Male')
            if request.GET['requesttype'] == 'all': headers.append('Parent Female')
            if request.GET['requesttype'] == 'all': headers.append('Parent(s)')
            writer.writerow(headers)

            if request.GET['requesttype'] == 'all': #Si l'utilisateur a cliqué sur 'Get CSV file, alors on récupère toutes les données
                seedlot_qs = Seedlot.objects.filter(type=seedlottype)
                
                for seedlot in seedlot_qs:
                    line = []
                    line.append(seedlot.name)
                    line.append(seedlot.accession.name)
                    line.append(seedlot.is_obsolete)
                    line.append(seedlot.description)
                    line.append(','.join([p.name for p in seedlot.projects.all()]))
                    
                    if type(seedlot.attributes_values) is dict :
                        values = seedlot.attributes_values
                    else :
                        values = ast.literal_eval(seedlot.attributes_values)
                    for at in attributes :
                        line.append(values.get(str(at.id)))
                
                    line.append(','.join([str(s.parent.name) for s in seedlot.relations_as_child.filter(parent_gender='M')]))
                    line.append(','.join([str(s.parent.name) for s in seedlot.relations_as_child.filter(parent_gender='F')]))
                    line.append(','.join([str(s.parent.name) for s in seedlot.relations_as_child.filter(parent_gender='X')]))
                    writer.writerow(line)
                
        # Create the HttpResponse object with the appropriate CSV header.
        response['Content-Disposition'] = 'attachment; filename=%s' %filename  
        writer = csv.writer(response)
    return response

@login_required
def get_file(request,file_id):
    datafile = DataFile.objects.get(id=file_id)
    
    if datafile.document.project_set.filter(id__in=request.user.project_set.values('id')) or request.user.is_superuser():
        #path=os.path.join(settings.MEDIA_ROOT,file_name)
        #f = open(datafile.datafile.path, 'r', encoding="utf-8")
        f = open(datafile.datafile.path, 'rb')
        #fileContent = File(f)
        response = HttpResponse(f.read(), content_type='application/octet-stream')
        response['Content-Disposition'] = 'attachment; filename='+str(datafile.datafile.name)
        return response
    else :
        raise PermissionDenied()


def headersrenderer(request, filetype):
    """
      This view return a file containing the headers of a file type.
    """
    if request.method == 'GET':
        
        response = HttpResponse(content_type='text/csv') 
        writer = csv.writer(response,delimiter=';', quoting=csv.QUOTE_ALL)
        headers = get_filerenderer_headers(filetype)
        writer.writerow(headers)
        
        filename = filetype+'_headers.csv'

        response['Content-Disposition'] = 'attachment; filename=%s' %filename  
        writer = csv.writer(response)
        
        return response
    
    else:
        print("There is a problem with the headersrenderer function")

def get_filerenderer_headers(filetype):
    """
      This function give the headers for the csv file returned by FileRenderer.
      Headers are specific according to the filetype
      
      :param str filetype: Name of the model
      
      :returns: headers
      :rtype list
    """
    headers = []
    if filetype == "Locus_position":
        headers.extend(filerenderer_headers['Locus_position'])   
    elif filetype == "Flat_matrix":
        headers.extend(filerenderer_headers['Flat_matrix'])
    
    return headers

