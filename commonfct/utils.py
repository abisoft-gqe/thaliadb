# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import csv
import sys
import uuid


from threading import Thread

from django.conf import settings
from django.apps import apps
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from csv_io.views import IOFactory

def get_fields(form):
    """
    get_fields est une fonction qui permet à partir d'un formulaire de récupérer le nom
    des champs du Model, ainsi que les labels qui leur sont associés
    """
    fields_name = []
    fields_label = []
    
    for field in form.fields.keys():
        fields_name.append(field)
        fields_label.append(form[field].label)
        
    return fields_name, fields_label

def get_model_from_name(classname):
    """Warning : this function doesn't work if two classes has the same name in two different apps"""
    for model in apps.get_models():
        if model._meta.model_name == classname.lower() :
            return model
    raise Exception("No model found for Class name : {0}".format(classname))

#     all_models = []
#     for module in settings.INSTALLED_APPS :
#         if not module.startswith('django') :
#             packagedir = module+'.models'
#             try :
#                 model = getattr(sys.modules[packagedir], classname)
#                 if model not in all_models :
#                     all_models.append(model)
#             except :
#                 continue
#     
#     if len(all_models) == 1 :
#         return all_models[0]
#     else :
#         raise Exception

def recode(string):
    encoding = ['ascii', 'utf-8','iso-8859-1','utf-16']
    for e in encoding :
        try :
            return string.decode(e)
        except :
            pass
    raise UnicodeDecodeError("%s can't be decoded"%string)


def add_pagination(request, objects):
    """ Add pagination to a queryset of objects
        Base of 50 objects by page. """
    if 'nb_per_page' in request.GET.keys() and request.GET.get('nb_per_page') != '':
        nb_per_page = request.GET.get('nb_per_page')
        
        if nb_per_page == 'all':
            nb_per_page = len(objects) if len(objects) != 0 else 50
    else:
        nb_per_page = 50
    
    paginator = Paginator(objects, nb_per_page)
    page = request.GET.get('page')
    try:
        pag_objects = paginator.page(page)
    except PageNotAnInteger:
        # If page number is not an integer then page one
        pag_objects = paginator.page(1)
    except EmptyPage:
        # invalid page number show the last
        pag_objects = paginator.page(paginator.num_pages)
        
    return pag_objects


def execute_function_in_thread(function, args, kwargs, name = None):
    """
      To execute a funtion in a thread (writing of csv files for example)
      :param Function function: Function to execute
      :param list args: List of arguments of the function
      :param dict kwargs: Dict of function kwargs
    """
    t = Thread(target = function, args=args, kwargs=kwargs)
    t.daemon = True
    
    if name:
        t.name = name
        
    t.start()
    
    return t.name



def get_list_from_list_file(file):
    """
      Recode and find the dialect of the file, then split the file
      according to the delimiter and remove trailing newlines, spaces, etc.
      Return the list of objects in the file.
    """
    myfactory = IOFactory(file)
    file = myfactory.recode(file)
    
    dialect = None
    try:
        file_dialect = csv.Sniffer().sniff(file, delimiters=" |,;\t")
        dialect = file_dialect.delimiter
    except:
        if '\n' in file:
            dialect = '\n'
            
    final_list = []     
    if dialect:
        list = file.split(str(dialect))
        for item in list:
            item = item.strip().strip('"').rstrip("\n")
            if item:
                final_list.append(item)
        
    return final_list

def bytestomegabytes(bytes):
    return (bytes / 1024) / 1024

# def get_encoding(file_name, factory):
#     """
#       This function open the file with different encodings 
#       to return the good one to decode the file.
#     """
#     encodings = ['utf-8', 'cp1252']
#     gd_encoding = ''
#          
#     for encoding in encodings:
#         try:
#             with open(file_name,'r', encoding=encoding) as file:
#                 myfactory = factory(file)
#                 myfactory.find_dialect()   
#                 
#                 gd_encoding = encoding
#                 break
#         
#         except Exception as e:
# #             print("This encoding returns an error."+ str(e))
#             pass
#     
#     return gd_encoding

