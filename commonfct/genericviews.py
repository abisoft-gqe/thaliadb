# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import ast
import csv
import json
from itertools import chain
import copy
import pandas as pd
import datetime
import threading
import os

from itertools import chain
from types import  *

from django.http import HttpResponse,HttpResponseServerError, Http404, JsonResponse
from django.shortcuts import render
from django.conf import settings
from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db import models
from django.forms.models import model_to_dict
from django.db.models.query import QuerySet
from django.utils.dateformat import DateFormat

from commonfct.utils import get_model_from_name, get_fields, add_pagination
from team.models import Project, Institution, User
from accession.models import AccessionTypeAttribute, AccessionType, Accession
from lot.models import SeedlotTypeAttribute
from lot.models import SeedlotTypeAttribute
from genotyping.models import LocusType, Locus, LocusPosition, LocusTypeAttribute
from phenotyping.models import Environment, SpecificTreatment, Trait, Ontology
from lot.models import Seedlot
from genealogy_managers.models import Reproduction


def return_csv(request, filename):
    tmpfile=open(settings.TEMP_FOLDER+'/'+filename+'.csv','rb')
    response=HttpResponse(tmpfile.read(),content_type="application/vnd.ms-excel")
    response['Content-Disposition']='attachment; filename='+filename+'.csv'
    return response
 

def get_generic_management_objects(MyModel, adds):
    """
      Function used to get the objects of the generic management view
    """
    if "query" in adds:
        allobjects = MyModel.objects.filter(adds['query']).distinct().order_by('name') #si on effectue une requete via le refine search
    else:
        allobjects = MyModel.objects.all()
        
    if "method_type" in adds:
        allobjects = MyModel.objects.filter(type=adds["method_type"])
        
    return allobjects



def generic_management(MyModel, MyModelForm, request, template, adds, page_title):
    #print("on est ici dans generic management (commonfct)")
    """ La classe Model passée en argument permet de récupérer tous les objets de ce Model
    On créé également le formulaire correspondant"""
  
    allobjects = get_generic_management_objects(MyModel, adds)
    number_all = len(allobjects)
        
    form = MyModelForm()
    """ Depuis le formulaire on récupère une liste des champs qui décrivent le modèle 
    fields_name : les noms des champs dans le Model
    fields_label : ce qui doit s'afficher
    """
    fields_name, fields_label = get_fields(form)

    allobjects = add_pagination(request, allobjects)
    
    tag_fields = {'all':allobjects,
                  'number_all':number_all,
                  'fields_name':fields_name,
                  'fields_label':fields_label,
                  'title':page_title}
    tag_fields.update(adds)
    
    if request.method == "GET":
        #Quand on arrive en GET on affiche juste le formulaire de création
        tag_fields.update({'form':form,'creation_mode':True})
        return render(request,template,tag_fields)
    elif request.method == "POST":
        #Quand on arrive en POST on distingue l'Update de la creation
        #'hiddenid' permet de récupérer l'id d'un objet déjà existant
        if "hiddenid" in request.POST.keys() :
            try :
                #on récupère l'objet déjà existant puis on utilise les fonctionnalités du formulaire pour l'updater
                instance = MyModel.objects.get(id=request.POST['hiddenid'])
                form = MyModelForm(instance=instance, data=request.POST)
                if form.is_valid():
                    form.save()
                    
                    allobjects = get_generic_management_objects(MyModel, adds)
                    number_all = len(allobjects)
                    allobjects = add_pagination(request, allobjects)
                    
                    tag_fields.update({'all':allobjects,
                                       'number_all':number_all,
                                       'form':form,
                                       'creation_mode':False,
                                       'object':instance,
                                       'hiddenid':instance.id})
                    return render(request,template,tag_fields)
                else :
                    #sinon on génère un message d'erreur qui sera affiché
                    errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form.errors.items() ])
                    
                    tag_fields.update({'form':form,
                                       'creation_mode':False,
                                       'hiddenid':instance.id,
                                       'errormsg':errormsg})
                    return render(request,template,tag_fields)
            except ObjectDoesNotExist :
                tag_fields.update({'form':form,
                                    'creation_mode':True,
                                    'errormsg':"The object you try to update doesn't exist in the database"})
                return render(request,template,tag_fields)
        else :   
            #si on est en mode création le formulaire est validé puis l'objet créé
            form = MyModelForm(request.POST, request.FILES)
            if 'error_field' in adds:
                errormsg=adds['error_field']
                if MyModel.objects.get(name=adds['name']):
                    MyModel.objects.get(name=adds['name']).delete()
                    
                allobjects = get_generic_management_objects(MyModel, adds)
                number_all = len(allobjects)
                allobjects = add_pagination(request, allobjects)
                
                tag_fields.update({'form':form,
                                   'creation_mode':True,
                                   'errormsg':errormsg,
                                   'all':allobjects,
                                   'number_all':number_all})
                
                return render(request,template,tag_fields)
            
            if form.is_valid() :
                newobject = form.save()
                if "method_type" in adds:
                    newobject.type = adds["method_type"]
                    newobject.save()
                #---------------#
                allobjects = get_generic_management_objects(MyModel, adds)
                number_all = len(allobjects)
                allobjects = add_pagination(request, allobjects)
                
                tag_fields.update({'form':form,
                                   'object':newobject,
                                   'creation_mode':True,
                                   'all':allobjects,
                                   'number_all':number_all})
                return render(request,template,tag_fields)
            
            else :
                    #si le formulaire n'est pas valide on va afficher les erreurs
                errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form.errors.items() ])
                tag_fields.update({'form':form,
                                   'creation_mode':True,
                                   'errormsg':errormsg})
                return render(request,template,tag_fields)


def genericdelete(request):
    print("------------------------------ generic delete ----------------------------")
    if request.method == "POST":
        objectid = None
        classname = None
        try :              
            classname = request.POST['classname']
            print("classname: "+str(classname))

            if classname == "LocusPosition":
                objectid = request.POST['objectid']
                locus = LocusPosition.objects.get(id=int(objectid)).locus
                if len(locus.locusposition_set.all()) > 1:
                    MyModel = get_model_from_name("LocusPosition")
                    instance=LocusPosition.objects.get(id=int(objectid))
                else:
                    MyModel = get_model_from_name("Locus")
                    instance=LocusPosition.objects.get(id=int(objectid)).locus
                
            elif classname == "SeedlotRelation" or classname == "AccessionRelation":
                objectid = request.POST['objectid']
                MyModel = get_model_from_name(classname)
                instance = Reproduction.objects.filter(id=objectid)
            else:
                objectid = int(request.POST['objectid']) 
                MyModel = get_model_from_name(classname)
                instance = MyModel.objects.get(id=objectid)
                
            if classname=="LocusType":
                instance=Locus.objects.filter(type=objectid)
                name=LocusType.objects.get(id=objectid)
                name.delete()
            print("objectid: "+str(objectid))
            print("MyModel: "+str(MyModel))
            
            print(instance)

            if classname == 'Ontology' :
                print('fileeeeeeeeeeeeeeeeeeeeeeeee :'+str(instance.file))
                os.remove(settings.MEDIA_ROOT+'/'+str(instance.file))
            elif classname == "SeedlotRelation" or classname == "AccessionRelation":
                for i in instance:
                    i.delete()   
            elif classname == "ThreadReport":
                classname = "Message"
                
            instance.delete()
            print("ici")
        except Exception as e:
            print(e)
            return HttpResponseServerError()
        else :
            if classname == "SeedlotRelation" or classname == "AccessionRelation":
                instance = objectid
            response = {"response":"{0} with name {1} has been deleted from database".format(classname, instance)}
            return HttpResponse(json.dumps(response), content_type='application/javascript')

    else :
        raise Http404
    
def genericedit(request):
    print('genericedit')
    # To detect if it is a jQuery ajax request (replace request.is_ajax())
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
        objectid = None
        classname = None
        instance = None
        try :
            try: 
                objectid = int(request.POST['objectid'])
            except Exception as e :
                objectid = request.POST['objectid']
                 
            classname = request.POST['classname']
            if classname=='LocusPosition':
                MyModel = Locus
            else:
                MyModel = get_model_from_name(classname)
            if classname=='LocusPosition':
                if type(objectid) != "int":
                    objects = LocusPosition.objects.filter(id=objectid)
                    for obj in objects:
                        if obj.locus.type != None:
                            objectid = obj.locus.id
                instance = Locus.objects.get(id=objectid)
                position = objects[0]
            elif classname=="SeedlotRelation":
                if type(objectid) != "int":
                    try:
                        objectid = MyModel.objects.get(child=Seedlot.objects.get(name=objectid)).id
                    except:
                        print(MyModel.objects.filter(child=Seedlot.objects.get(name=objectid)))
                        objectid = [i.id for i in MyModel.objects.filter(child=Seedlot.objects.get(name=objectid))]
            elif classname == "AccessionRelation":
                if type(objectid) != "int":
                    try:
                        objectid = MyModel.objects.get(reproduction_id=objectid).id
                    except:
                        objectid = [i.id for i in MyModel.objects.filter(reproduction_id=objectid)]
            try:
                instance = MyModel.objects.get(id=objectid)
            except:
                instance = MyModel.objects.filter(id__in=objectid)
            try :
                try:
                    attributes = instance.type.attribute_set().all()
                except :
                    locustype = LocusType.objects.get(id=instance.type)
                    attributes = locustype.attribute_set().all()             
                    
                #att_values = json.loads(instance.attributes_values)
                if isinstance(instance.attributes, dict):
                    att_values = instance.attributes
                    
                else:
                    att_values={}
                    values_form={}
                    for i in instance.attributes:
                        att_values[str(i.attribute_id)]=i.value
                for att in attributes:
#                     print('att', att)
                    if att_values :
                        if str(att.id) in att_values.keys() :
                            setattr(instance,att.name.lower().replace(' ','_'),att_values[str(att.id)])
                            values_form[att.name.lower().replace(' ','_')] = att_values[str(att.id)]
                        else :
                            setattr(instance,att.name.lower().replace(' ','_'),None)
                            values_form[att.name.lower().replace(' ','_')] = None
                    else :
                        setattr(instance,att.name.lower().replace(' ','_'),'')  
                        values_form[att.name.lower().replace(' ','_')] =  ''
            except Exception as e:
                print("==> exception1: ",e)
        except Exception as e:
            return HttpResponseServerError(str(e))

        else :
            print('test1:',type(instance))
            
            if isinstance(instance, QuerySet):
                for inst in instance:
                    if classname == "AccessionRelation":
                        object_todump = {'child':inst.child,'parent':inst.parent,'parent_gender':inst.parent_gender,'method':Reproduction.objects.get(id=objectid[0]).reproduction_method_id,'synonymous':inst.synonymous,'first_production':str(inst.first_production)}
                        objectcopy = copy.copy(object_todump)
                        for key in list(object_todump):
                            if not isinstance(object_todump[key], (list, dict, str, int, float, bool, type(None))) :
                                del objectcopy[key]
                    elif classname == "SeedlotRelation":
                        object_todump = {'child':inst.child,'parent':inst.parent,'parent_gender':inst.parent_gender,'site':inst.site,'start_date':inst.reproduction.start_date, 'end_date':inst.reproduction.end_date, 'comments':inst.reproduction.description}
                        objectcopy = copy.copy(object_todump)
                        for key in list(object_todump):
                            if not isinstance(object_todump[key], (list, dict, str, int, float, bool, type(None))) :
                                del objectcopy[key]
                return HttpResponse(json.dumps(objectcopy), content_type='application/javascript')
            else:
                if classname=="LocusPosition":
                    genome_version_id = None
                    if position.genome_version:
                        genome_version_id = position.genome_version.id
                    object_todump={"name":instance.name,"comments":instance.comments,"attributes_values":instance.attributes_values,"position":position.position,'genome_version':genome_version_id,'chromosome':position.chromosome}
                else:
                    object_todump = model_to_dict(instance)
                    if classname == "Institution":
                        object_todump['country'] = object_todump['country'].code

                objectcopy = copy.copy(object_todump)
                for key in list(object_todump):  #previously in python2: 'for key in object_todump.keys()' but it doesn't work in python3 because keys returns an iterator instead of a list
    #                 print("$ ", key)
                    if not isinstance(object_todump[key], (dict, str, int, float, bool, type(None))) :
                        #del objectcopy[key]
                        
                        if key == "projects":
#                             print(list(object_todump[key]))
                            del objectcopy[key]
                            objectcopy[key]=[i.id for i in Project.objects.filter(name__in=list(object_todump[key]))]
                        elif key == "institutions":
    #                         print(list(object_todump[key]))
                            del objectcopy[key]
                            objectcopy[key]=[i.id for i in Institution.objects.filter(name__in=list(object_todump[key]))]
                        elif key == "users":
                            del objectcopy[key]
                            objectcopy[key]=[i.id for i in User.objects.filter(login__in=list(object_todump[key]))]
                        elif key == "linked_files":
                            del objectcopy[key]
                            objectcopy[key]=[i.id for i in object_todump[key]]
                        elif key == "datafile":
                            del objectcopy[key]
                            objectcopy[key]= object_todump[key].url
                        elif key == "country_of_origin":
                            objectcopy[key] = objectcopy[key].code
                        
                        if type(object_todump[key]) in [datetime.date, datetime.datetime] :
                            del objectcopy[key]
                            objectcopy[key] = object_todump[key].strftime("%Y-%m-%d")
                            
                    elif key.endswith('_id'):
                        newkey = key.replace('_id','')
    #                     print("/ ", newkey)
                        value = object_todump[key]
    #                     print('valeur à éditer', value)
    #                     print("newkey  :   ",newkey)
                        if classname!="User" and isinstance(getattr(instance,newkey),models.Model):
    #                         print('valeur à effacer', key)
                            del objectcopy[key]
                            objectcopy[newkey] = value
    #                         print('valeur éditée', objectcopy[newkey])
    
                values_attr={}
                for key, val in objectcopy.items():
                    if key == "attributes_values":
                        #objectcopy[key] = ast.literal_eval(objectcopy[key])
                        #print(type(objectcopy[key]))
                        
                        if type(objectcopy[key]) != dict:
                            objectcopy_items = (ast.literal_eval(objectcopy[key])).items()
                        else:
                            objectcopy_items = objectcopy[key].items()
                        
                        for key2, val2 in objectcopy_items:
                            #print(key2)
                            if classname == "Seedlot":
                                #(ast.literal_eval(objectcopy[key]))[SeedlotTypeAttribute.objects.get(id=key2).name]=(ast.literal_eval(objectcopy[key])).pop(key2) ##remplacer la clé du dico de id par name pour le javascript plus tard
                                attr = SeedlotTypeAttribute.objects.get(id=key2)
                                values_attr[attr.name.lower().replace(' ','_')] = val2
                            elif classname == "Accession":
                                #(ast.literal_eval(objectcopy[key]))[AccessionTypeAttribute.objects.get(id=key2).name]=(ast.literal_eval(objectcopy[key])).pop(key2)
                                attr = AccessionTypeAttribute.objects.get(id=key2)
                                values_attr[attr.name.lower().replace(' ','_')] = val2
                                
                            elif classname == "LocusPosition":
                                attr = LocusTypeAttribute.objects.get(id=key2)
                                values_attr[attr.name.lower().replace(' ','_')] = val2
                        
#                         if type(objectcopy[key])==str or type(objectcopy[key])==JSONString:
#                             for key2, val2 in (ast.literal_eval(objectcopy[key])).items():
#                                 #print(key2)
#                                 if classname == "Seedlot":
#                                     #(ast.literal_eval(objectcopy[key]))[SeedlotTypeAttribute.objects.get(id=key2).name]=(ast.literal_eval(objectcopy[key])).pop(key2) ##remplacer la clé du dico de id par name pour le javascript plus tard
#                                     attr = SeedlotTypeAttribute.objects.get(id=key2)
#                                     values_attr[attr.name.lower().replace(' ','_')] = val2
#                                 elif classname == "Accession":
#                                     #(ast.literal_eval(objectcopy[key]))[AccessionTypeAttribute.objects.get(id=key2).name]=(ast.literal_eval(objectcopy[key])).pop(key2)
#                                     attr = AccessionTypeAttribute.objects.get(id=key2)
#                                     values_attr[attr.name.lower().replace(' ','_')] = val2
#                                     
#                                 elif classname == "LocusPosition":
#                                     attr = LocusTypeAttribute.objects.get(id=key2)
#                                     values_attr[attr.name.lower().replace(' ','_')] = val2
#                         else:
#                             for key2, val2 in objectcopy[key].items():
#                                 if classname == "Seedlot":
#                                     #objectcopy[key][SeedlotTypeAttribute.objects.get(id=key2).name]=objectcopy[key].pop(key2) ##remplacer la clé du dico de id par name pour le javascript plus tard
#                                     attr = SeedlotTypeAttribute.objects.get(id=key2)
#                                     values_attr[attr.name.lower().replace(' ','_')] = val2
#                                 elif classname == "Accession":
#                                     #objectcopy[key][AccessionTypeAttribute.objects.get(id=key2).name]=objectcopy[key].pop(key2)
#                                     attr = AccessionTypeAttribute.objects.get(id=key2)
#                                     values_attr[attr.name.lower().replace(' ','_')] = val2
#                                 
#                                 elif classname == "LocusPosition":
#                                     attr = LocusTypeAttribute.objects.get(id=key2)
#                                     values_attr[attr.name.lower().replace(' ','_')] = val2
                                    
                #print(objectcopy)
                #print(values_attr)
                if classname == "Seedlot" or classname=="Accession" or classname=="LocusPosition":
                    objectcopy = {**objectcopy, **values_attr}
                #print(objectcopy)
                #Lorsqu'on tente d'éditer des données déjà éditées précedemment ou insérées depuis un .csv, objectcopy[attributes_values] n'est pas un dictionnaire
                #Il est traité comme un String contenant le disctionnaire, il faut donc forcer la conversion
                return HttpResponse(json.dumps(objectcopy), content_type='application/javascript')
    else :
        raise Http404

def getattributes(request):
    """à rendre generic pour LocusTypeAttribute et SeedlotTypeAttribute"""
    # To detect if it is a jQuery ajax request (replace request.is_ajax())
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
        attributes_list = []
        for attribute in AccessionTypeAttribute.objects.all().order_by('name'):
            attributes_list.append("{0} ({1})".format((attribute.name).encode('utf-8'),(attribute.get_type_display()).encode('utf-8')))
        return HttpResponse(json.dumps(attributes_list), content_type="application/json")
    else :
        raise Http404


def addattributes(request):
    # To detect if it is a jQuery ajax request (replace request.is_ajax())
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
        id_object = None
        type_instance = None
        classname = request.POST['classname']
        print('classname', classname)
        type_classname = classname+'Type'
        print('type_classname', type_classname)
        attribute_classname = type_classname+'Attribute'
        name = request.POST['attribute']
        print('name', name)
        type_num = request.POST['type']
        print('type_num', type_num)
        
        AttributeModel = get_model_from_name(attribute_classname)
#         print(AttributeModel)
       
        attribute, created = AttributeModel.objects.get_or_create(name=name, type=type_num)
#         print(request.POST)
        print(AttributeModel.objects.filter(name=name))   
#         print(Accession.attributes_values
        if 'objectid' in request.POST:
#             print("oui"
            id_object = request.POST['objectid']
            #print('id_object', id_object)
            TypeModel = get_model_from_name(type_classname)
            #print(('TypeModel', TypeModel)
            type_instance = TypeModel.objects.get(id=id_object)
            #print('type_instance', type_instance)
            print("classname: "+str(classname))
            position_set_attribute = classname.lower()+'attributeposition_set'
            print(position_set_attribute)
            qs_elements = type_instance.__getattribute__(position_set_attribute).all().order_by('-position')
#             print('qs_element', qs_element)
            last_element = None
            if qs_elements:
                last_element = qs_elements[0]
            last_position = 0
            if last_element:
                last_position = last_element.position
            
            PositionModel = get_model_from_name(classname+"AttributePosition")
            PositionModel.objects.create(attribute=attribute,
                                         type=type_instance,
                                         position=last_position+1)
        return HttpResponse()
    else :
        raise Http404
    
@login_required
@user_passes_test(lambda u: u.is_superuser(), login_url='/')
def typeedit(request):
    # To detect if it is a jQuery ajax request (replace request.is_ajax())
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
        objectid = int(request.POST['objectid'])
        classname = request.POST['classname']
        type_classname = classname+'Type'
        TypeModel = get_model_from_name(type_classname)
        
        type_entity = TypeModel.objects.get(id=objectid)
        
        att_set = "{0}attributeposition_set".format(classname.lower())
        positions = type_entity.__getattribute__(att_set).all()
        data = serializers.serialize("json", chain([type_entity,], positions))

        return HttpResponse(data)
    else:
        raise Http404

def get_empty_file(request, classtype, object_id):
#     try :
#         ModelClass = get_model_from_name(classname)
#     except :
#         raise Http404
    print(classtype)
    if classtype == 'Accession':
        return _empty_accession_file(object_id)
    elif classtype =='Seedlot':
        pass
    else :
        raise Http404
    
def _empty_accession_file(id) :
    
    try :
        AccessionType.objects.get(id=id)
    except :
        raise Http404

    stream = "name\tpedigree\t"
    response = HttpResponse(stream, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename=accessions.csv'
    return response 


def check_thread_activity(request):
    """
      Check if a thread is active and 
      return True or False
    """
    thread_name = request.POST.get('thread_name')
    status = ''
#     print(thread_name)
    for thread in threading.enumerate():
#         print(thread)
        if thread_name == str(thread.name):
            status = thread.is_alive()
            return JsonResponse({
                'status': status,
            })
    
    if not status:
        return JsonResponse({
                'status': False,
        })


