# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

    
import settings
from team.forms import GlobalSearchBarForm
from commonfct.forms import GlobalSearchForm

from team.urls import urls_as_dict as teamurls
from accession.urls import urls_as_dict as accessionurls
from classification.urls import urls_as_dict as classificationurls
from genealogy_managers.urls import urls_as_dict as genealogyurls
from genotyping.urls import urls_as_dict as genotypingurls
from images.urls import urls_as_dict as imagesurls
from lot.urls import urls_as_dict as loturls
from phenotyping.urls import urls_as_dict as phenotypingurls

def code_version(request):
    return {'VERSION':settings.VERSION}

def rooturl(request):
    return {'ROOT_URL':settings.ROOT_URL}

def formsearch_bar(request):
    search_bar = GlobalSearchBarForm()
    return {'search_bar':search_bar}

def global_search_form(request):
    return {'global_search':GlobalSearchForm()}

def is_public(request):
    return {'is_public_instance':settings.PUBLIC_INSTANCE}

def user_doc(request):
    return {'user_doc':settings.USER_DOC}

def userdocpattern(request):
    urls_as_dict = teamurls
    urls_as_dict.update(accessionurls)
    urls_as_dict.update(classificationurls)
    urls_as_dict.update(genealogyurls)
    urls_as_dict.update(genotypingurls)
    urls_as_dict.update(imagesurls)
    urls_as_dict.update(loturls)
    urls_as_dict.update(phenotypingurls)
    
    name = request.resolver_match.url_name
    try :
        return {'userdocsection': urls_as_dict[name]['docpath']}
    except Exception as e:
        return {}
    
