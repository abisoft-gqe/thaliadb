# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2017, Mélanie Polart-Donat, Yannick De Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import csv
import ast

from _csv import Dialect

from django.forms.models import model_to_dict
from django.db.models.query import QuerySet
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings

class RefineSearchFactory():
        
    
    def write_accession(self, accessions_init, list_col, AccessionTypeAttribute, AccessionRelation):
        #AccessionTypeAttribute et AccessionRelation pas dans les params
        dialect = Dialect(delimiter = ';')
        
        with open(settings.TEMP_FOLDER+"/result_search.csv", 'w', encoding='utf-8') as csv_file:
            writer = csv.writer(csv_file, dialect, quoting=csv.QUOTE_ALL)
            writer.writerow(list_col)
            for i in accessions_init:
                row=[]
                dico=model_to_dict(i)
                
                try:
                    dico_attr_values = ast.literal_eval(getattr(i,"attributes_values"))
                except:
                    dico_attr_values = getattr(i,"attributes_values")
                for id_attr, val_attr in dico_attr_values.items():
                    dico[AccessionTypeAttribute.objects.get(id=id_attr).name.strip().lower().replace(" ","_")]=val_attr
                
                dico['parent_male'] = [a.parent.name for a in AccessionRelation.objects.filter(parent_gender = "M", child=i)]
                dico['parent_female'] = [a.parent.name for a in AccessionRelation.objects.filter(parent_gender = "F", child=i)]
                dico['parent(s)'] = [a.parent.name for a in AccessionRelation.objects.filter(parent_gender = "X", child=i)]
                       
                for key, value in dico.items():
                    if not dico[key]:
                        dico[key]=""
                    elif isinstance(dico[key], QuerySet):
                        list_qs = []
                        for queryset_i in dico[key]:
                            list_qs.append(queryset_i.name)
                        dico[key]=' | '.join(list_qs)
                    elif isinstance(dico[key], list):
                        dico[key]=' | '.join(str(obj) for obj in dico[key])
                for col in list_col:
                    col_field = col.strip().lower().replace(" ","_")
                    if col_field not in dico:
                        dico[col_field]=""
                    row.append(dico[col_field])
                writer.writerow(row)
    
    
    
    def write_locus(self, locus_positions, list_col, LocusTypeAttribute):
        dialect = Dialect(delimiter = ';')
        
        with open(settings.TEMP_FOLDER+"/result_search.csv", 'w', encoding='utf-8') as csv_file:
            writer = csv.writer(csv_file, dialect)
            writer.writerow(list_col)
            for i in locus_positions:
                row=[]
                dico=model_to_dict(i.locus)
                
                try:
                    dico_attr_values = ast.literal_eval(getattr(i.locus,"attributes_values"))
                except:
                    dico_attr_values = getattr(i.locus,"attributes_values")
                for id_attr, val_attr in dico_attr_values.items():
                    dico[LocusTypeAttribute.objects.get(id=id_attr).name]=val_attr
                    
                dico['genome version'] = i.genome_version
                dico['chromosome'] = i.chromosome
                dico['position'] = i.position
                
                for key, value in dico.items():
                    if not dico[key]:
                        dico[key]=""
                    elif isinstance(dico[key], QuerySet):
                        list_qs = []
                        for queryset_i in dico[key]:
                            list_qs.append(queryset_i.name)
                        dico[key]=', '.join(list_qs)
                    elif isinstance(dico[key], list):
                        dico[key]=', '.join(str(obj) for obj in dico[key])
                for col in list_col:
                    if col not in dico:
                        dico[col]=""
                    row.append(dico[col])
                writer.writerow(row)

