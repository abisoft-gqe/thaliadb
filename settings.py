
# Django settings for Thaliadbv3 project.
import os
import sys
from dotenv import load_dotenv

PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))

dotenv_path = os.path.join(PROJECT_PATH, '.env')
load_dotenv(dotenv_path)

"""
    Thaliadb specific Settings
"""
VERSION = '3.7.0'
USER_DOC = "https://thaliadb.readthedocs.io"
#define if this instance is a public one
PUBLIC_INSTANCE = eval(os.getenv('PUBLIC_INSTANCE'))

#Use it if you host several django app on the same apache server
DJANGO_ROOT_SETTING = os.getenv('DJANGO_ROOT_SETTING')

"""
    Django Main Settings
"""
DEBUG = True

# Make this unique, and don't share it with anybody.
SECRET_KEY = os.getenv('SECRET_KEY')

ROOT_URL = os.getenv('ROOT_URL')

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = os.getenv('TIME_ZONE')

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

ROOT_URLCONF = DJANGO_ROOT_SETTING+'urls'

if os.getenv('ADMINS') and os.getenv('ADMINS') != '':
    ADMINS = [admin.split(',') for admin in os.getenv('ADMINS').split(';')]
else :
    ADMINS = ()

MANAGERS = ADMINS

"""
    Django internal Settings
"""
INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'rest_framework.authtoken',
    'team.apps.TeamConfig',
    'accession.apps.AccessionConfig',
    'lot.apps.LotConfig',
    'phenotyping.apps.PhenotypingConfig',
    'classification.apps.ClassificationConfig',
    'genotyping.apps.GenotypingConfig',
    'commonfct.apps.CommonfctConfig',
    'dataview.apps.DataviewConfig',
    'genealogy_accession.apps.GenealogyAccessionConfig',
    'genealogy_seedlot.apps.GenealogySeedlotConfig',
    'genealogy_managers.apps.GenealogyManagersConfig',
    'images.apps.ImagesConfig',
    'wsBrAPI.v1_0',
    'wsBrAPI.v2_1',
    # Uncomment the next line to enable the admin:
    # 'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
#    'django_extensions',
    'dal',
    'dal_select2',
    'dal_queryset_sequence',
    'django.contrib.admin',
    'debug_toolbar',
    
)

MIDDLEWARE = (      
    #'django.middleware.cache.UpdateCacheMiddleware',    # This must be first on the list
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    #'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'images.middleware.EmptyTempUploadFile',
    'django.middleware.cache.FetchFromCacheMiddleware', # This must be last
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [PROJECT_PATH+"/templates/",],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.template.context_processors.request",
                "django.contrib.messages.context_processors.messages",
                DJANGO_ROOT_SETTING+'commonfct.processor.code_version',
                DJANGO_ROOT_SETTING+'commonfct.processor.user_doc',
                DJANGO_ROOT_SETTING+'commonfct.processor.userdocpattern',
                DJANGO_ROOT_SETTING+"accession.processor.accession_menu",
                DJANGO_ROOT_SETTING+"accession.processor.locus_menu",
                DJANGO_ROOT_SETTING+"accession.processor.seedlot_menu",
                DJANGO_ROOT_SETTING+'commonfct.processor.rooturl',
                DJANGO_ROOT_SETTING+'commonfct.processor.global_search_form',
                DJANGO_ROOT_SETTING+'commonfct.processor.is_public',
                'team.custom_context_processor.notification_icon_renderer'
            ],
            'debug':True
        },
    },
]

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

"""
    WebServer Settings
"""

INTERNAL_IPS = ["127.0.0.1"]

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
if os.getenv('ALLOWED_HOSTS') and os.getenv('ALLOWED_HOSTS') != '' :
    ALLOWED_HOSTS = os.getenv('ALLOWED_HOSTS').split(',')
else :
    ALLOWED_HOSTS = []

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'wsgi.application'

LOGIN_REDIRECT_URL = '/'

LOGIN_URL = '/'

"""
    Session settings
""" 

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

SESSION_EXPIRE_AT_BROWSER_CLOSE = True


"""
    Database settings
"""

#Database settings
DATABASES = {
    'default': {

        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'HOST':os.getenv('DATABASE_HOST'),
        'NAME': os.getenv('DATABASE_NAME'),
        'USER': os.getenv('DATABASE_USER'),
        'PASSWORD':os.getenv('DATABASE_PASSWORD'),
        'PORT':int(os.getenv('DATABASE_PORT')),
        'ATOMIC_REQUESTS': True,
    },
}

AUTH_USER_MODEL = 'team.User'

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

#we use sqlite if test are run
if 'test' in sys.argv:
    DATABASES['default'] = {'ENGINE': 'django.db.backends.sqlite3'}


"""
    Files Upload settings
"""
FILE_UPLOAD_HANDLERS = ("django.core.files.uploadhandler.MemoryFileUploadHandler",                      
                        "django.core.files.uploadhandler.TemporaryFileUploadHandler")

TEMP_FOLDER = os.getenv('TEMP_FOLDER')

FILE_UPLOAD_TEMP_DIR = os.getenv('FILE_UPLOAD_TEMP_DIR')

DATA_UPLOAD_MAX_NUMBER_FILES = int(os.getenv('DATA_UPLOAD_MAX_NUMBER_FILES'))


"""
    Form Upload Settings
"""

DATA_UPLOAD_MAX_NUMBER_FIELDS = None

#Temp dir for django file form
FILE_FORM_UPLOAD_DIR = FILE_UPLOAD_TEMP_DIR

"""
    Static & Media Settings
"""

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = os.getenv('MEDIA_ROOT')

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = os.path.join(PROJECT_PATH, "static")

# Additional locations of static files
STATICFILES_DIRS = (
    os.path.join(PROJECT_PATH, "media"),
    os.path.join(PROJECT_PATH, "_docs"),
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = ROOT_URL+"static/"

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/'

#IMAGE_PATH is relative to MEDIA_ROOT
IMAGE_PATH = "images/"

"""
    Date Settings
"""
# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = False

DATE_FORMAT = 'd/m/Y'

DATE_INPUT_FORMATS = ['%d/%m/%Y','%d/%m/%y']

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

"""
    REST API & BrAPI Settings
"""

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'wsBrAPI.paginator.BrAPIPagination',
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'wsBrAPI.v2_1.auth.BrAPIAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated', ),

}

AUTH_TOKEN_LIFE = 60 #days

#BrAPI settings
COMMONCROPNAME = os.getenv('COMMONCROPNAME')
GENUS = os.getenv('GENUS')
SPECIES = os.getenv('SPECIES')
SPECIESAUTHORITY = os.getenv('SPECIESAUTHORITY')
SUBTAXA = os.getenv('SUBTAXA')
SUBTATAAUTHORITY = os.getenv('SUBTATAAUTHORITY')
