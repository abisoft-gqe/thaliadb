$(document).ready(     
	      function () {
				$('#id_trait').on('change',function() {
					objects = get_selected();
					ajaxRequest(objects);
			   });
				$('#tick').on('click',function() {
					if(this.checked){
						objects = get_all();
						if (objects.length != 0){
							ajaxRequest(objects);
						}
					}
					else{
						widget.jsTreePanel.reset();
					}
			   });
	      }
	);

function get_selected(){
	var list = [];
	var i = 0;
	var options = document.getElementById('id_trait').options; 
	for (i=0;i<options.length;i++){
		if(options[i].selected){
			var temp = options[i].value;
			list.push(temp);
		}
	}
	return list.join();
}

function get_all(){
	var list = [];
	var i = 0;
	var options = document.getElementById('id_trait').options; 
	for (i=0;i<options.length;i++){
		var temp = options[i].value;
		list.push(temp);
	}
	return list.join(); 
}

function ajaxRequest(objects){
	$.ajax({
		url:ROOT_URL.concat("get_ont_terms/"),
		type:"POST",
		data: {'objects':objects},
		dataType: "json",
        success: function(json) {
        	a = '['+ json.response.toString() +']';
        	while(a.includes("'")){
        		a = a.replace("'","\"");
        	}
        	widget.jsTreePanel.reset();
        	widget.jsTreePanel.setSelectedNodeIds(JSON.parse(a));
        },
        error: function(jqXHR, textStatus, errorThrown) {
        	widget.jsTreePanel.reset();
        	//alert("Error "+textStatus+"/ "+errorThrown);
        }
	});
}