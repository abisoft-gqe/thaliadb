function updateElementIndex(el, prefix, ndx) {
	var id_regex = new RegExp('(' + prefix + '-\\d+)');
	var replacement = prefix + '-' + ndx;
	if ($(el).attr("for")) $(el).attr("for", $(el).attr("for").replace(id_regex, replacement));
	if (el.id) el.id = el.id.replace(id_regex, replacement);
	if (el.name) el.name = el.name.replace(id_regex, replacement);
}

function addForm(btn, prefix) {
	//console.log(prefix);
    var formCount = parseInt($('#id_' + prefix + '-TOTAL_FORMS').val());
    var row = $('.dynamic-form:first').clone(true).get(0);
    
    $(row).removeAttr('id').insertAfter($('.dynamic-form:last')).children('.hidden_formset').removeClass('hidden_formset');
    $(row).children().not(':last').children().each(function() {
	    updateElementIndex(this, prefix, formCount);
	    $(this).val('');
    });
    $(row).find('.delete-row').click(function() {
	    deleteForm(this, prefix);
    });
    $(row).find('.nb-locus-data').attr('id', prefix + "-" + formCount + "-row-nb-data" ).text("");
    $(row).find('.formset-loading').attr('id', prefix + "-" + formCount + "-loading" );
    $(row).attr('id', prefix + "-" + formCount + "-row" );
    $('#id_' + prefix + '-TOTAL_FORMS').val(formCount + 1);
    return false;
}

function deleteForm(btn, prefix) {
    $(btn).parents('.dynamic-form').remove();
    var forms = $('.dynamic-form');
    $('#id_' + prefix + '-TOTAL_FORMS').val(forms.length);
    for (var i=0, formCount=forms.length; i<formCount; i++) {
	    $(forms.get(i)).children().not(':last').children().each(function() {
	        updateElementIndex(this, prefix, i);
	    });
	    $(forms.get(i)).attr('id', prefix + "-" + i + "-row" );
	    $(forms.get(i)).find('.nb-locus-data').attr('id', prefix + "-" + i + "-row-nb-data" );
	    $(forms.get(i)).find('.formset-loading').attr('id', prefix + "-" + i + "-loading" );
    }
    return false;
}