function apply_accession_properties(acc_position) {
	// To apply the class to all the Accession column
	$('.pandas-result tbody>tr>:nth-child(2)').addClass("pandas-result-acc-col");
	$('.pandas-result tbody>tr>:nth-child(2)').css('left',acc_position + "px");
	
	// To have the Accession column header cell displayed over the other headers when scrolling to the right
	$('.pandas-result thead>:nth-child(1)>:nth-child(2)').addClass("pandas-result-acc-headers");
	$('.pandas-result thead>:nth-child(1)>:nth-child(2)').css('left',acc_position + "px");
	
	// To make the cells of the "Accession" column of each of the three lines displayed on top of the three 
	// lines when scrolling to the right
	if (display == "individual"){
		$('#basictable').addClass("pandas-result-ind-acc");
	}
	
	// Specific for the frequency table to have the Accession column cell of second header line displayed 
	// over other cells when scrolling to the right
	$('.pandas-result thead>:nth-child(2)>:nth-child(2)').addClass("pandas-result-acc-headers");
	$('.pandas-result thead>:nth-child(2)>:nth-child(2)').css('left',acc_position + "px");
}

function remove_accession_properties() {
	// Remove the properties when the checkbox is not checked
	$('.pandas-result tbody>tr>:nth-child(2)').removeClass("pandas-result-acc-col");
	$('.pandas-result thead>:nth-child(1)>:nth-child(2)').removeClass("pandas-result-acc-headers");
	
	if (display == "individual"){
		$('#basictable').removeClass("pandas-result-ind-acc");
	}
	
	$('.pandas-result thead>:nth-child(2)>:nth-child(2)').removeClass("pandas-result-acc-headers");
	
	$('.pandas-result thead>:nth-child(1)>:nth-child(2)').css('left', '');
	$('.pandas-result thead>:nth-child(2)>:nth-child(2)').css('left', '');
}

function apply_seedlot_properties(sdl_position) {
	// To apply the class to all the Seedlot column
	$('.pandas-result tbody>tr>:nth-child(3)').addClass("pandas-result-acc-col");
	$('.pandas-result tbody>tr>:nth-child(3)').css('left',sdl_position + "px");
	
	// To have the Seedlot column header cell displayed over the other headers when scrolling to the right
	$('.pandas-result thead>:nth-child(1)>:nth-child(3)').addClass("pandas-result-acc-headers");
	$('.pandas-result thead>:nth-child(1)>:nth-child(3)').css('left',sdl_position + "px");
	
	// To make the cells of the "Seedlot" column of each of the three lines displayed on top of the three 
	// lines when scrolling to the right
	if (display == "individual"){
		$('#basictable').addClass("pandas-result-ind-sdl");
	}
	
	// Specific for the frequency table to have the Seedlot column cell of second header line displayed 
	// over other cells when scrolling to the right
	$('.pandas-result thead>:nth-child(2)>:nth-child(3)').addClass("pandas-result-acc-headers");
	$('.pandas-result thead>:nth-child(2)>:nth-child(3)').css('left',sdl_position + "px");
}

function remove_seedlot_properties() {
	// Remove the properties when the checkbox is not checked
	$('.pandas-result tbody>tr>:nth-child(3)').removeClass("pandas-result-acc-col");
	$('.pandas-result thead>:nth-child(1)>:nth-child(3)').removeClass("pandas-result-acc-headers");
	
	if (display == "individual"){
		$('#basictable').removeClass("pandas-result-ind-sdl");
	}
	
	$('.pandas-result thead>:nth-child(2)>:nth-child(3)').removeClass("pandas-result-acc-headers");
	
	$('.pandas-result thead>:nth-child(1)>:nth-child(3)').css('left', '');
	$('.pandas-result thead>:nth-child(2)>:nth-child(3)').css('left', '');
}

function apply_sample_properties(spl_position) {
	// To apply the class to all the Sample column
	$('.pandas-result tbody>tr>:nth-child(4)').addClass("pandas-result-acc-col");
	$('.pandas-result tbody>tr>:nth-child(4)').css('left',spl_position + "px");
	
	// To have the Sample column header cell displayed over the other headers when scrolling to the right
	$('.pandas-result thead>:nth-child(1)>:nth-child(4)').addClass("pandas-result-acc-headers");
	$('.pandas-result thead>:nth-child(1)>:nth-child(4)').css('left',spl_position + "px");
	
	// To make the cells of the "Sample" column of each of the three lines displayed on top of the three 
	// lines when scrolling to the right
	if (display == "individual"){
		$('#basictable').addClass("pandas-result-ind-spl");
	}
	
	// Specific for the frequency table to have the Sample column cell of second header line displayed 
	// over other cells when scrolling to the right
	$('.pandas-result thead>:nth-child(2)>:nth-child(4)').addClass("pandas-result-acc-headers");
	$('.pandas-result thead>:nth-child(2)>:nth-child(4)').css('left',spl_position + "px");
}

function remove_sample_properties() {
	//Remove the properties when the checkbox is not checked
	$('.pandas-result tbody>tr>:nth-child(4)').removeClass("pandas-result-acc-col");
	$('.pandas-result thead>:nth-child(1)>:nth-child(4)').removeClass("pandas-result-acc-headers");
	
	if (display == "individual"){
		$('#basictable').removeClass("pandas-result-ind-spl");
	}
	$('.pandas-result thead>:nth-child(2)>:nth-child(4)').removeClass("pandas-result-acc-headers");
	
	$('.pandas-result thead>:nth-child(1)>:nth-child(4)').css('left', '');
	$('.pandas-result thead>:nth-child(2)>:nth-child(4)').css('left', '');
}

function apply_locus_properties(loc_position, col_nb) {
	// To apply the class to all the Locus column
	$('.pandas-result tbody>tr>:nth-child('+col_nb+')').addClass("pandas-result-acc-col");
	$('.pandas-result tbody>tr>:nth-child('+col_nb+')').css('left',loc_position + "px");
	
	// To have the Sample column header cell displayed over the other headers when scrolling to the right
	$('.pandas-result thead>:nth-child(1)>:nth-child('+col_nb+')').addClass("pandas-result-acc-headers");
	$('.pandas-result thead>:nth-child(1)>:nth-child('+col_nb+')').css('left',loc_position + "px");
	
	// Specific for the frequency table to have the Locus column cell of second header line displayed 
	// over other cells when scrolling to the right
	$('.pandas-result thead>:nth-child(2)>:nth-child('+col_nb+')').addClass("pandas-result-acc-headers");
	$('.pandas-result thead>:nth-child(2)>:nth-child('+col_nb+')').css('left',loc_position + "px");
	
}

function remove_locus_properties(col_nb) {
	//Remove the properties when the checkbox is not checked
	$('.pandas-result tbody>tr>:nth-child('+col_nb+')').removeClass("pandas-result-acc-col");
	$('.pandas-result thead>:nth-child(1)>:nth-child('+col_nb+')').removeClass("pandas-result-acc-headers");
	$('.pandas-result thead>:nth-child(2)>:nth-child('+col_nb+')').removeClass("pandas-result-acc-headers");
	
	$('.pandas-result thead>:nth-child(1)>:nth-child('+col_nb+')').css('left', '');
	$('.pandas-result thead>:nth-child(2)>:nth-child('+col_nb+')').css('left', '');
}

// To handle the dynamic freezing of columns in the tables
$(document).ready(function() {
	var first_th = $( '#basictable th' );
	var second_th = $( '#basictable th:nth-child(2)' );
	var third_th = $( '#basictable th:nth-child(3)' );
	var fourth_th = $( '#basictable th:nth-child(4)' );
	
	const th_width_num = parseInt(first_th.css('width'), 10);
	const th_width = th_width_num + 4;
	
	const sec_th_width_num = parseInt(second_th.css('width'), 10);
	const sec_th_width = th_width + sec_th_width_num + 4;
	
	const third_th_width_num = parseInt(third_th.css('width'), 10);
	const third_th_width = sec_th_width + third_th_width_num + 4;
	
	const fourth_th_width_num = parseInt(fourth_th.css('width'), 10);
	const fourth_th_width = third_th_width + fourth_th_width_num + 4;
	
	//Only the lenght of the first th + third th (without the second one)
	const third_th_width_raw = th_width + third_th_width_num + 4;
	
	var accession_col = $('#accession-col');
	var seedlot_col = $('#seedlot-col');
	var sample_col = $('#sample-col');
	var locus_col = $('#locus-col');
	
	var col_nb = '7'; // Default value when all the columns are present

	if (sample_col.length > 0){
		if (sample_col[0].disabled==true){
			col_nb = '6';
		}
	}
	if (seedlot_col.length > 0){
		if (seedlot_col[0].disabled==true){
			col_nb = '5';
		}
	}
	if (accession_col.length > 0){
		if (accession_col[0].disabled==true){
			col_nb = '4';
		}
	}
	
	
	$('#accession-col').on('change',function(){
		
		if (accession_col[0].checked==true){
			apply_accession_properties(th_width);
			
			if (seedlot_col[0].checked==true && sample_col[0].checked==true && locus_col[0].checked==true){
				remove_seedlot_properties();
				apply_seedlot_properties(sec_th_width);
				
				remove_sample_properties();
				apply_sample_properties(third_th_width);
				
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(fourth_th_width, col_nb);
				}
			}
			else if (seedlot_col[0].checked==true && sample_col[0].checked==true && locus_col[0].checked==false){
				remove_seedlot_properties();
				apply_seedlot_properties(sec_th_width);
				
				remove_sample_properties();
				apply_sample_properties(third_th_width);
			}
			else if (seedlot_col[0].checked==true && sample_col[0].checked==false && locus_col[0].checked==true){
				remove_seedlot_properties();
				apply_seedlot_properties(sec_th_width);
				
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(third_th_width, col_nb);
				}
			}
			else if (seedlot_col[0].checked==true && sample_col[0].checked==false && locus_col[0].checked==false){
				remove_seedlot_properties();
				apply_seedlot_properties(sec_th_width);
			}
			else if (seedlot_col[0].checked==false && sample_col[0].checked==true && locus_col[0].checked==true){
				remove_sample_properties();
				apply_sample_properties(sec_th_width);
				
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(third_th_width, col_nb);
				}
			}
			else if (seedlot_col[0].checked==false && sample_col[0].checked==true && locus_col[0].checked==false){
				remove_sample_properties();
				apply_sample_properties(sec_th_width);
			}
			else if (seedlot_col[0].checked==false && sample_col[0].checked==false && locus_col[0].checked==true){
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(sec_th_width, col_nb);
				}
			}
			
		}
		else{
			remove_accession_properties();
			
			if (seedlot_col[0].checked==true && sample_col[0].checked==true && locus_col[0].checked==true){
				remove_seedlot_properties();
				apply_seedlot_properties(th_width);
				
				remove_sample_properties();
				apply_sample_properties(third_th_width_raw);
				
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(third_th_width, col_nb);
				}
			}
			else if (seedlot_col[0].checked==true && sample_col[0].checked==true && locus_col[0].checked==false){
				remove_seedlot_properties();
				apply_seedlot_properties(th_width);
				
				remove_sample_properties();
				apply_sample_properties(third_th_width_raw);
			}
			
			else if (seedlot_col[0].checked==true && sample_col[0].checked==false && locus_col[0].checked==true){
				remove_seedlot_properties();
				apply_seedlot_properties(th_width);
				
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(sec_th_width, col_nb);
				}
			}
			else if (seedlot_col[0].checked==true && sample_col[0].checked==false && locus_col[0].checked==false){
				remove_seedlot_properties();
				apply_seedlot_properties(th_width);
			}
			else if (seedlot_col[0].checked==false && sample_col[0].checked==true && locus_col[0].checked==true){
				remove_sample_properties();
				apply_sample_properties(th_width);
				
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(sec_th_width, col_nb);
				}
			}
			else if (seedlot_col[0].checked==false && sample_col[0].checked==true && locus_col[0].checked==false){
				remove_sample_properties();
				apply_sample_properties(th_width);
			}
			else if (seedlot_col[0].checked==false && sample_col[0].checked==false && locus_col[0].checked==true){
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(th_width, col_nb);
				}
			}
			
		}
	});
	
	$('#seedlot-col').on('change',function(){
		
		if (seedlot_col[0].checked==true){
			
			if (accession_col[0].checked==true && sample_col[0].checked==true && locus_col[0].checked==true){
				apply_seedlot_properties(sec_th_width);
				
				remove_sample_properties();
				apply_sample_properties(third_th_width);
				
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(fourth_th_width, col_nb);
				}
			}
			
			else if (accession_col[0].checked==true && sample_col[0].checked==true && locus_col[0].checked==false){
				apply_seedlot_properties(sec_th_width);
				
				remove_sample_properties();
				apply_sample_properties(third_th_width);
			}
			
			else if (accession_col[0].checked==true && sample_col[0].checked==false && locus_col[0].checked==true){
				apply_seedlot_properties(sec_th_width);
				
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(third_th_width, col_nb);
				}
			}
			
			else if (accession_col[0].checked==true && sample_col[0].checked==false && locus_col[0].checked==false){
				apply_seedlot_properties(sec_th_width);
			}
			
			else if (accession_col[0].checked==false && sample_col[0].checked==true && locus_col[0].checked==true){
				apply_seedlot_properties(th_width);
				
				remove_sample_properties();
				apply_sample_properties(third_th_width_raw);
				
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(third_th_width, col_nb);
				}
			}
			
			else if (accession_col[0].checked==false && sample_col[0].checked==true && locus_col[0].checked==false){
				apply_seedlot_properties(th_width);
				
				remove_sample_properties();
				apply_sample_properties(third_th_width_raw);
			}
			
			else if (accession_col[0].checked==false && sample_col[0].checked==false && locus_col[0].checked==true){
				apply_seedlot_properties(th_width);
				
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(sec_th_width, col_nb);
				}
			}
			
			else if (accession_col[0].checked==false && sample_col[0].checked==false && locus_col[0].checked==false){
				apply_seedlot_properties(th_width);
			}
			
		}
		else{
			remove_seedlot_properties();
			
			if (accession_col[0].checked==true && sample_col[0].checked==true && locus_col[0].checked==true){
				remove_sample_properties();
				apply_sample_properties(sec_th_width);
				
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(third_th_width, col_nb);
				}
			}
			
			else if (accession_col[0].checked==true && sample_col[0].checked==true && locus_col[0].checked==false){
				remove_sample_properties();
				apply_sample_properties(sec_th_width);
			}
			
			else if (accession_col[0].checked==false && sample_col[0].checked==true && locus_col[0].checked==true){
				remove_sample_properties();
				apply_sample_properties(th_width);
				
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(sec_th_width, col_nb);
				}
			}
			
			else if (accession_col[0].checked==false && sample_col[0].checked==true && locus_col[0].checked==false){
				remove_sample_properties();
				apply_sample_properties(th_width);
			}
			
			else if (accession_col[0].checked==true && sample_col[0].checked==false && locus_col[0].checked==true){
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(sec_th_width, col_nb);
				}
			}
			
			else if (accession_col[0].checked==false && sample_col[0].checked==false && locus_col[0].checked==true){
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(th_width, col_nb);
				}
			}
		}
	});
	
	$('#sample-col').on('change',function(){
		
		if (sample_col[0].checked==true){
			if (accession_col[0].checked==true && seedlot_col[0].checked==true && locus_col[0].checked==true){
				apply_sample_properties(third_th_width);
				
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(fourth_th_width, col_nb);
				}
			}
			
			else if (accession_col[0].checked==true && seedlot_col[0].checked==true && locus_col[0].checked==false){
				apply_sample_properties(third_th_width);
			}
			
			else if (accession_col[0].checked==true && seedlot_col[0].checked==false && locus_col[0].checked==true){
				apply_sample_properties(sec_th_width);
				
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(third_th_width, col_nb);
				}
			}
			
			else if (accession_col[0].checked==true && seedlot_col[0].checked==false && locus_col[0].checked==false){
				apply_sample_properties(sec_th_width);
			}
			
			else if (accession_col[0].checked==false && seedlot_col[0].checked==true && locus_col[0].checked==true){
				apply_sample_properties(third_th_width_raw);
				
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(third_th_width, col_nb);
				}
			}
			
			else if (accession_col[0].checked==false && seedlot_col[0].checked==true && locus_col[0].checked==false){
				apply_sample_properties(third_th_width_raw);
			}
			
			else if (accession_col[0].checked==false && seedlot_col[0].checked==false && locus_col[0].checked==true){
				apply_sample_properties(th_width);
				
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(sec_th_width, col_nb);
				}
			}
			
			else if (accession_col[0].checked==false && seedlot_col[0].checked==false && locus_col[0].checked==false){
				apply_sample_properties(th_width);
			}
		}
		else{
			remove_sample_properties();
			
			if (accession_col[0].checked==true && seedlot_col[0].checked==true && locus_col[0].checked==true){
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(third_th_width, col_nb);
				}
			}
			
			else if (accession_col[0].checked==false && seedlot_col[0].checked==true && locus_col[0].checked==true){
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(sec_th_width, col_nb);
				}
			}
			
			else if (accession_col[0].checked==true && seedlot_col[0].checked==false && locus_col[0].checked==true){
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(sec_th_width, col_nb);
				}
			}
			
			else if (accession_col[0].checked==false && seedlot_col[0].checked==false && locus_col[0].checked==true){
				if (display == "frequencies"){
					remove_locus_properties(col_nb);
					apply_locus_properties(th_width, col_nb);
				}
			}
		}
	});
	
	$('#locus-col').on('change',function(){
		
		if (locus_col[0].checked==true){
			if (accession_col[0].checked==true && seedlot_col[0].checked==true && sample_col[0].checked==true){
				apply_locus_properties(fourth_th_width, col_nb);
			}
			else if (accession_col[0].checked==true && seedlot_col[0].checked==true && sample_col[0].checked==false){
				apply_locus_properties(third_th_width, col_nb);
			}
			else if (accession_col[0].checked==true && seedlot_col[0].checked==false && sample_col[0].checked==false){
				apply_locus_properties(sec_th_width, col_nb);
			}
			else if (accession_col[0].checked==true && seedlot_col[0].checked==false && sample_col[0].checked==true){
				apply_locus_properties(third_th_width, col_nb);
			}
			else if (accession_col[0].checked==false && seedlot_col[0].checked==false && sample_col[0].checked==true){
				apply_locus_properties(sec_th_width, col_nb);
			}
			else if (accession_col[0].checked==false && seedlot_col[0].checked==true && sample_col[0].checked==false){
				apply_locus_properties(sec_th_width, col_nb);
			}
			else if (accession_col[0].checked==false && seedlot_col[0].checked==true && sample_col[0].checked==true){
				apply_locus_properties(third_th_width, col_nb);
			}
			else if (accession_col[0].checked==false && seedlot_col[0].checked==false && sample_col[0].checked==false){
				apply_locus_properties(th_width, col_nb);
			}
		}
		else{
			remove_locus_properties(col_nb);
		}
		
	});
	
});