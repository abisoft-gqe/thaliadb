$(document).ready(		
		function () {			
			$('.check_all').on('change',function(){
			    var checkbox_delete = document.getElementsByClassName('checkbox_delete');
			    var case_all = $(this);			    
			    if (case_all[0].checked==true){
			    	for (var checkbox in checkbox_delete){
			    		checkbox_delete.item(checkbox).checked=true;
			    	}
			    }
			    else{
			    	for (var checkbox in checkbox_delete){
			    		checkbox_delete.item(checkbox).checked=false;
			    	}
			    }
			}
			);   
				
			$('.delete_all').on('click',function(){
				var elements=document.getElementsByClassName('checkbox_delete');
				var len=elements.length;
				if(confirm("Do you really want to delete these elements ?")){
					for (var i=(len-1); i>=0; i--){
						if (elements[i].checked==true){
							
							var element=$(elements[i]);
							var object = element.next('.delete').next('[name=objectid]');
							classe = object.next('[name=classid]');
							
							
								$.ajax({
									url:ROOT_URL.concat("genericdelete/"),
									type:"POST",
									data: {'objectid':object.val(),
											'classname':classe.val()},
									dataType: "json",
					                success: function(json) {
					                	alert(json.response);
					                },
					                error: function(jqXHR, textStatus, errorThrown) {
					                	alert("Deletion failed !");
					                }
								});
								
								element.closest('tr').remove();
								//cas particulier du password pour User
								element.closest('form').find("#id_login").closest('tr').after('<tr><th><label for="id_password">Password:</label></th><td><input id="id_password" maxlength="128" name="password" type="text"></td></tr>');
							
						}
					}
				}
			});
}
		
)