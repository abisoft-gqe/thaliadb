/* To mark as selected the pagination button representing the number of results shown on top of tables*/

$(document).ready(function() {
	var pag_b = $("[name='nb_per_page']");
	var pag_b_value = pag_b.val();
	var numbers = ['10', '50', '100', '200', 10, 50, 100, 200];
	//console.log(pag_b_value);
	
	if (numbers.includes(pag_b_value) == false) {
		pag_b_value = 'all';
	}
	
	$('input[name=nb_per_page][value='+pag_b_value+']').attr('checked', true);
})
