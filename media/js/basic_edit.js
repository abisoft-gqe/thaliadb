$(document).ready(		
	function() {
			
		$('.edit').on('click',function() {
			// On navigue à travers les objets
			object = $(this).next('.checkbox_delete').next('.delete').next('[name=objectid]');
			classe = object.next('[name=classid]');
			$.ajax({
				url:ROOT_URL.concat("genericedit/"), //l'URL du script qui reçoit la requête
				type:"POST",
				data: {csrfmiddlewaretoken: '{{ csrf_token }}',
					   'objectid':object.val(),
					   'classname':classe.val()},
				dataType: "json",
				jsonp: 'jsonp',
                success: function(json) {
                	// Key = les différents attributs de l'objet (Locus ou Accession)
                	for (var key in json) {
                		if (key == "password") {
                			//cas particulier du password : on update de façon différente
                			//Nous recherchons le 1ien ancêtre de "#id_+key..." ex:"id_chromosome" qui correspond à un element "tr" pour le supprimer
                			$("*[id='id_"+key+"']").closest('tr').remove() // ex: id_chromosome pour Locus
                		} else if ($("*[id='id_"+key+"']").is(':checkbox')) {
                			//cas des checkbox
                			$("*[id='id_"+key+"']").prop('checked', json[key]);
                		}
                		else if (key == "institutions"){
                			for (var i=0;i<=document.getElementById('id_institutions').options.length-1;i++){
                				document.getElementById('id_institutions').options[i].selected = false;
                			}
            				for (var j=0;j<=json[key].length-1;j++){
                				for (var i=0;i<=document.getElementById('id_institutions').options.length-1;i++){
                					if (document.getElementById('id_institutions').options[i].value == json[key][j]){
                						document.getElementById('id_institutions').options[i].selected = "selected"; // on selectionne les projets correspondant au dictionnaire
                					}
                				}
            				}
                		}
                		else if (key == "projects"){
                			// on deselectionne les projets qui auraient pu être sélectionnés précédemment
                			for (var i=0;i<=document.getElementById('id_projects').options.length-1;i++){
                				document.getElementById('id_projects').options[i].selected = false;
                			}
            				for (var j=0;j<=json[key].length-1;j++){
                				for (var i=0;i<=document.getElementById('id_projects').options.length-1;i++){
                					if (document.getElementById('id_projects').options[i].value == json[key][j]){
                						document.getElementById('id_projects').options[i].selected = "selected"; // on selectionne les projets correspondant au dictionnaire
                					}
                				}
            				}
                		}
                		else {
                			if (json[key] != null) {
                				if (key == 'attributes_values'){
                					for (var h in json[key]){
                						$("*[id='id_"+h+"']").val(json[key][h]);
                					}
                				}
                				else{
                					$("*[id='id_"+key+"']").val(json[key]);
                				}
                			}              			
                		}
                	}
                	// Set the value from the input button to "Update"
                	$('#submit-btn').val('Update');
                	//alert('object.val(): '+object.val())
                	// On ajoute un "input" de type "hidden" au formulaire
//                	alert(object.val());
                	$('#genericform').append('<input type="hidden" name="hiddenid" value="'+object.val()+'" />');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus + "    Error:" + errorThrown);
                   }
			}); 
		});

	
		$('.hideshow').on('click',function() {
			if ($(this).next('div').is(":visible")) {
				$(this).next('div').hide('slow');
			} else {
				$(this).next('div').show('slow');
			}
		})	;
	}
);