#-*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.urls import path

from images.views import images_renderer, img_management, imgLink_management, image_manager, UploadedImagesView
from images.views import UploadedImagesView, LinkImagesView, UploadSummaryView

urls_as_dict = {
    'imagerender': {'path':'render/<int:pk>/', 'view':images_renderer},
    'img_management': {'path':'images/', 'view':img_management, 'docpath':'admin/images.html#single-image-management'},
    'image_manager': {'path':'images/<int:pk>/', 'view':image_manager, 'docpath':'admin/images.html#single-image-management'},
    'upload_images': {'path':'upload_images', 'view':UploadedImagesView.as_view(), 'docpath':'admin/images.html#upload-a-set-of-images'},
    'link_images': {'path':'link_images/', 'view':LinkImagesView.as_view(), 'docpath':'admin/images.html#upload-a-set-of-images'},
    'upload_summary': {'path':'upload_summary/', 'view':UploadSummaryView.as_view(), 'docpath':'admin/images.html#upload-a-set-of-images'},
    
    }

urlpatterns = [path(url['path'], url['view'], name=namespace) for namespace, url in urls_as_dict.items()]

# urlpatterns = [
#     path('render/<int:pk>/', images_renderer, name='imagerender'),
#     path('images/', img_management, name='img_management'),
#     path('images/<int:pk>/', image_manager, name='image_manager'),
#
#     path('upload_images/', UploadedImagesView.as_view(), name='upload_images'),
#     path('link_images/', LinkImagesView.as_view(), name='link_images'),
#     path('upload_summary/', UploadSummaryView.as_view(), name='upload_summary'),
# ]
#



