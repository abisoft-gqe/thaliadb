#-*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.urls import path
from phenotyping.views import phenotypinghome, environment_management, treatment_management, trait_management,\
                              phenotypingvalue_management, ontology_management, store_ontology_file, ontology_edit,\
                              trait_edit, method_management, viewphenovalues, viewontology, method_management_edit
from commonfct.genericviews import return_csv

urls_as_dict = {
    'phenotypinghome': {'path':'home/', 'view':phenotypinghome, 'docpath':'admin/phenotyping.html'},
    'environment_management': {'path':'environment/', 'view':environment_management, 'docpath':'admin/phenotyping.html#environments'},
    'treatment_management': {'path':'treatment/', 'view':treatment_management, 'docpath':'admin/phenotyping.html#specific-treatments'},
    'trait_management': {'path':'trait/', 'view':trait_management, 'docpath':'admin/phenotyping.html#traits'},
    'ontology_management': {'path':'ontology/', 'view':ontology_management, 'docpath': 'admin/phenotyping.html#ontologies'},
    'trait_edit': {'path':'trait/<int:pk>/edit/', 'view':trait_edit, 'docpath':'admin/phenotyping.html#traits'},
    'ontology_edit': {'path':'ontology/<int:pk>/edit/', 'view':ontology_edit, 'docpath': 'admin/phenotyping.html#ontologies'},
    'store_ontology_file': {'path':'ontology/<str:update>/', 'view':store_ontology_file, 'docpath': 'admin/phenotyping.html#ontologies'},
    'phenotypingvalue_management': {'path':'phenotypingvalue/', 'view':phenotypingvalue_management, 'docpath':'admin/phenotyping.html#insert-phenotyping-data'},
    'method_management': {'path':'method/', 'view':method_management, },
    'method_management_edit': {'path':'method/<int:object_id>/', 'view':method_management_edit, },
    'viewphenovalues': {'path':'vphenovalues/', 'view':viewphenovalues, },
    'viewontology': {'path':'vontology/', 'view':viewontology, },
    'return_csv': {'path':'return_csv/<str:filename>/', 'view':return_csv, },
    
    }

urlpatterns = [path(url['path'], url['view'], name=namespace) for namespace, url in urls_as_dict.items()]

# urlpatterns = [
#     path('home/', phenotypinghome, name='phenotypinghome'),
#     path('environment/', environment_management, name='environment_management'),
#     path('treatment/', treatment_management, name='treatment_management'),
#     path('trait/', trait_management, name='trait_management'),
#     path('ontology/', ontology_management, name='ontology_management'),
#     path('trait/<int:pk>/edit/', trait_edit, name='trait_edit'),
#     path('ontology/<int:pk>/edit/', ontology_edit, name='ontology_edit'),
#     path('ontology/<str:update>/', store_ontology_file, name='store_ontology_file'),
#     path('phenotypingvalue/', phenotypingvalue_management, name='phenotypingvalue_management'),
#     path('method/', method_management, name='method_management'),
#     path('method/<int:object_id>/', method_management_edit, name='method_management_edit'),
#     path('vphenovalues/', viewphenovalues,name='viewphenovalues'),
#     path('vontology/', viewontology, name='viewontology'),
#     path('return_csv/<str:filename>/', return_csv, name='return_csv'),
# ]

