from rest_framework import serializers

from phenotyping.models import Environment, Trait, Ontology


class StudiesBrAPISerializer(serializers.ModelSerializer): 
    
    active = serializers.BooleanField(source = 'getActive')
    additionalInfo = serializers.JSONField(source = 'getStudiesAdditionalInfo')
    commonCropName = serializers.CharField(source = 'getCommonCropName')
    contacts = serializers.ListField(source = 'getBlankValue')
    culturalPractices = serializers.CharField(source = 'getBlankValue')
    dataLinks = serializers.ListField(source = 'getBlankList')
    documentationURL = serializers.CharField(source = 'getDocumentationURL')
    endDate = serializers.CharField(source = 'getBlankDateValue')
    environmentParameters = serializers.ListField(source = 'getEnvironmentParameters')
    experimentalDesign = serializers.JSONField(source = 'getExperimentalDesign')
    externalReferences = serializers.ListField(source = 'getBlankValue')
    growthFacility = serializers.JSONField(source = 'getGrowthFacility')
    lastUpdate = serializers.JSONField(source = 'getLastUpdate')
    license = serializers.CharField(source = 'getBlankValue')
    locationDbId = serializers.CharField(source = 'getBlankValue')
    locationName = serializers.CharField(source = 'location')
    observationLevels = serializers.ListField(source = 'getBlankList')
    observationUnitsDescription = serializers.CharField(source = 'getBlankValue')
    observationVariableDbIds = serializers.ListField(source = 'getBlankList')
    seasons = serializers.ListField(source = 'getBlankList')
    startDate = serializers.CharField(source = 'getBlankDateValue')
    studyCode = serializers.CharField(source = 'getBlankValue')
    studyDbId = serializers.CharField(source = 'id')
    studyDescription = serializers.CharField(source = 'description')
    studyName = serializers.CharField(source = 'name')
    studyPUI = serializers.CharField(source = 'getBlankValue')
    studyType = serializers.CharField(source = 'getBlankValue')
    trialDbId = serializers.CharField(source = 'getBlankValue')
    trialName = serializers.CharField(source = 'getBlankValue')

    class Meta:
        model = Environment
        fields = ['active',
                 'additionalInfo',
                 'commonCropName',
                 'contacts',
                 'culturalPractices',
                 'dataLinks',
                 'documentationURL',
                 'endDate',
                 'environmentParameters',
                 'experimentalDesign',
                 'externalReferences',
                 'growthFacility',
                 'lastUpdate',
                 'license',
                 'locationDbId',
                 'locationName',
                 'observationLevels',
                 'observationUnitsDescription',
                 'observationVariableDbIds',
                 'seasons',
                 'startDate',
                 'studyCode',
                 'studyDbId',
                 'studyDescription',
                 'studyName',
                 'studyPUI',
                 'studyType',
                 'trialDbId',
                 'trialName'
            ]   
        depth = 1
        
class TraitsBrAPISerializer(serializers.ModelSerializer): 
    
    traitDbId = serializers.CharField(source = 'id')
    traitName = serializers.CharField(source = 'name')
    additionalInfo = serializers.JSONField(source = 'getTraitAdditionalInfo')
    alternativeAbbreviations = serializers.ListField(source = 'getBlankList')
    attribute = serializers.CharField(source = 'getBlankValue')
    attributePUI = serializers.CharField(source = 'getBlankValue')
    entity = serializers.CharField(source = 'getBlankValue')
    entityPUI = serializers.CharField(source = 'getBlankValue')
    externalReferences = serializers.ListField(source = 'getBlankValue')
    mainAbbreviation = serializers.CharField(source = 'abbreviation')
    ontologyReference = serializers.JSONField(source = 'getOntologyReference')
    status = serializers.CharField(source = 'getBlankValue')
    synonyms = serializers.ListField(source = 'getBlankList')
    traitClass = serializers.CharField(source = 'getBlankValue')
    traitDescription = serializers.CharField(source = 'comments')
    traitPUI = serializers.CharField(source = 'getBlankValue')
    
    class Meta:
        model = Trait
        fields = [
            'traitDbId',
            'traitName',
            'additionalInfo',
            'alternativeAbbreviations',
            'attribute',
            'attributePUI',
            'entity',
            'entityPUI',
            'externalReferences',
            'mainAbbreviation',
            'ontologyReference',
            'status',
            'synonyms',
            'traitClass',
            'traitDescription',
            'traitPUI',
            ]
        
class OntologiesBrAPISerializer(serializers.ModelSerializer): 
    
    ontologyDbId = serializers.CharField(source = 'ontology_id')
    ontologyName = serializers.CharField(source = 'name')
    additionalInfo = serializers.JSONField(source = 'getOntologyAdditionalInfo')
    authors = serializers.CharField(source = 'getBlankValue')
    copyright = serializers.CharField(source = 'getBlankValue')
    description = serializers.CharField(source = 'getBlankValue')
    documentationURL = serializers.CharField(source = 'getDocumentationURL')
    licence = serializers.CharField(source = 'getBlankValue')
    version = serializers.CharField(source = 'getBlankValue')
    
    
    class Meta:
        model = Ontology
        fields = [
            'ontologyDbId',
            'ontologyName',
            'additionalInfo',
            'authors',
            'copyright',
            'description',
            'documentationURL',
            'licence',
            'version',
            ]
        depth = 1