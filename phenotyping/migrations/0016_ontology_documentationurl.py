# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-01-29 11:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('phenotyping', '0015_remove_ontology_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='ontology',
            name='documentationURL',
            field=models.TextField(blank=True, max_length=500, null=True),
        ),
    ]
