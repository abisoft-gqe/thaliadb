# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-01-31 09:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('phenotyping', '0022_auto_20190131_1014'),
    ]

    operations = [
        migrations.CreateModel(
            name='OntologyFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('file', models.FileField(upload_to='')),
            ],
        ),
    ]
