# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-01-31 12:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('phenotyping', '0023_ontologyfile'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ontologyfile',
            name='name',
        ),
        migrations.AddField(
            model_name='ontologyfile',
            name='ontology_name',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='phenotyping.Ontology'),
        ),
        migrations.AlterField(
            model_name='ontology',
            name='co_id',
            field=models.CharField(max_length=100, unique=True),
        ),
    ]
