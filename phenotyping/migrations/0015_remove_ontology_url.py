# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-01-29 11:29
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('phenotyping', '0014_ontology_url'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ontology',
            name='URL',
        ),
    ]
