# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-01-29 11:25
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('phenotyping', '0006_auto_20190129_1023'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ontology',
            name='authors',
        ),
        migrations.RemoveField(
            model_name='ontology',
            name='copyright',
        ),
        migrations.RemoveField(
            model_name='ontology',
            name='documentationURL',
        ),
        migrations.RemoveField(
            model_name='ontology',
            name='version',
        ),
    ]
