# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-01-29 11:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('phenotyping', '0013_auto_20190129_1227'),
    ]

    operations = [
        migrations.AddField(
            model_name='ontology',
            name='URL',
            field=models.TextField(blank=True, max_length=500, null=True),
        ),
    ]
