# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Laetitia Courgey

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import json
import ast
import os
import io
import csv
import cProfile
import csv
import shlex
import pandas as pd
import time

from datetime import date

from django.db import models, transaction
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.conf import settings
from django.core.files import File
from django.http import HttpResponse
from django.db.models import Q
from django.db import IntegrityError
from django.template.context_processors import request
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.http import HttpResponse,HttpResponseServerError, Http404

from accession.models import Accession
from commonfct.genericviews import generic_management
from csv_io.views import CSVFactoryForTempFiles
from phenotyping.models import Environment, SpecificTreatment, Trait,PhenotypicValue, Ontology
from phenotyping.forms import EnvironmentForm, SpecificTreatmentForm, TraitForm,TraitForm2,OntologyForm2, OntologyUpdateForm, UploadPhenoTypValueFileForm, PhenotypingValueForm, ViewPhenotypingValuesForm, OntologyForm
from commonfct.utils import get_fields, add_pagination
from team.models import Method, Project
from team.forms import MethodForm
from lot.models import Seedlot
from images.models import Image
from team.views import write_csv_search_result
from dataview.forms import PhenoViewerAccessionForm, PhenoViewerTraitForm, PhenoViewerEnvironmentForm, DataviewPhenotypingValuesForm
from django.apps import apps
from accession.forms import UploadFileForm, UploadFileFormWithoutDelimiter
from collections import OrderedDict


@login_required
@user_passes_test(lambda u: u.is_phenotyping_admin_user(), login_url='/login/')
def phenotypinghome(request):
    """
        Renders the basic phenotyping template 

    """
    return render(request,'phenotyping/phenotyping_base.html',{"admin":True,})


######################################### MANAGEMENT METHODS ############################################### 

@login_required
@user_passes_test(lambda u: u.is_phenotyping_admin_user(), login_url='/login/')
def environment_management(request):
    """
        if it's a POST request, it's either for EnvironmentForm (then we call generic_management to create the data) 
        or UploadFileForm (we call the _create_phenodata_from_file method to insert or upload the data)
        if it's a GET method, we call refine_search 
        
        :var form form : for to create a new element of this model 
        :var form formf : form for the file that we can upload to insert or update data
        :var string template : template used
        :var list list_attributes : all the fields of one element of this model
        :var list list_exceptions : 
        :var model model : 
        :var list environments : all the elements to display on this page
        :var string filetype :
        :var int typeid :
        :var list names : all the fields of one element of this model
        :var list labels : all the fields of one element of this model but starting with a capital letter
        :var string title : title to display on the page
        :var dict tag_fields : dict used to give all the information to a method 
        
    """
    form = EnvironmentForm()
    formf = UploadFileForm()
    template = 'phenotyping/phenotyping_generic.html'
    list_attributes=['name', 'date', 'location', 'description', 'specific treatment', 'projects']
    list_exceptions = ['id','phenotypicvalue']
    title = "Environment Management"
    model = Environment
    environments, nb_per_page = _get_data(request, model)
    filetype = "environment"
    typeid = 0
    if request.method == "POST":
        formf = UploadFileForm(request.POST, request.FILES) 
        if formf.is_valid():
            names, labels = get_fields(form)
            tag_fields = {"list_attributes":list_attributes,
                          "admin":True,
                          'fields_name':names,
                          'fields_label':labels,
                          'title': title,
                          'creation_mode':True,
                          'form':form,
                          "formfile":formf,
                          }
            print("-"*20+"Names : "+str(names)+"-"*20+"Labels : "+str(labels))
            return _create_phenodata_from_file(request, template, tag_fields, model)
        else:
            return generic_management(Environment, EnvironmentForm, request, 'phenotyping/phenotyping_generic.html',{"admin":True,},"Environment Management")
        
    tag_fields = {"form" : form,
                  "formfile" : formf,
                  "data" : environments,
                  "template" : template,
                  "list_attributes" : list_attributes,
                  "list_exceptions" : list_exceptions,
                  "title" : title,
                  "model" : model,
                  "filetype" : filetype,
                  "typeid" : typeid,
                  'creation_mode':True,
        }
    return refine_search(request, tag_fields)
    
          
@login_required
@user_passes_test(lambda u: u.is_phenotyping_admin_user(), login_url='/login/')
def treatment_management(request):
    """
        if it's a POST request, it's either for SpecificTreatmentForm (then we call generic_management to create the data) or UploadFileForm (we call the _create_phenodata_from_file method to insert or upload the data)
        if it's a GET method, we call refine_search 
        
        :var form form : for to create a new element of this model 
        :var form formf : form for the file that we can upload to insert or update data
        :var string template : template used
        :var list list_attributes : all the fields of one element of this model
        :var list list_exceptions : 
        :var model model : 
        :var list specific_treatments : all the elements to display on this page
        :var string filetype :
        :var int typeid :
        :var list names : all the fields of one element of this model
        :var list labels : all the fields of one element of this model but starting with a capital letter
        :var string title : title to display on the page
        :var dict tag_fields : dict used to give all the information to a method 
    
    """
    form = SpecificTreatmentForm()
    formf = UploadFileForm()
    template = 'phenotyping/phenotyping_generic.html'
    list_attributes=['name', 'decription']
    list_exceptions = ['environment','id']
    title = "Treatment Management"
    model = SpecificTreatment
    specific_treatments, nb_per_page = _get_data(request, model)
    filetype = 'treatment'
    typeid = 0
    if request.method == "POST":
        formf = UploadFileForm(request.POST, request.FILES) 
        if formf.is_valid():
            names, labels = get_fields(form)
            tag_fields = {"list_attributes":list_attributes,
                          "admin":True,
                          'fields_name':names,
                          'fields_label':labels,
                          'title': title,
                          'creation_mode':True,
                          'form':form,
                          "formfile":formf,
                          }
            return _create_phenodata_from_file(request, template, tag_fields, model)
        else:
            return generic_management(SpecificTreatment, SpecificTreatmentForm, request, 'phenotyping/phenotyping_generic.html',{"admin":True,},"Treatment Management")

    tag_fields = {"form" : form,
                  "formfile" : formf,
                  "data" : specific_treatments,
                  "template" : template,
                  "list_attributes" : list_attributes,
                  "list_exceptions" : list_exceptions,
                  "title" : title,
                  "model" : model,
                  "filetype" : filetype,
                  "typeid" : typeid,
                  'creation_mode':True,
        }
    return refine_search(request, tag_fields)        
        
          
@login_required
@user_passes_test(lambda u: u.is_phenotyping_admin_user(), login_url='/login/')
def trait_management(request):
    """
        if it's a POST request, it's either for TraitForm (then we call generic_management to create the data) or UploadFileForm (we call the _create_phenodata_from_file method to insert or upload the data)
        if it's a GET method, we call refine_search 
        
        :var form form : for to create a new element of this model 
        :var form formf : form for the file that we can upload to insert or update data
        :var string template : template used
        :var list list_attributes : all the fields of one element of this model
        :var list list_exceptions : 
        :var model model : 
        :var list traits : all the elements to display on this page
        :var string filetype :
        :var int typeid :
        :var list names : all the fields of one element of this model
        :var list labels : all the fields of one element of this model but starting with a capital letter
        :var string title : title to display on the page
        :var dict tag_fields : dict used to give all the information to a method 

    
    """
    form = TraitForm2()
    formf = UploadFileFormWithoutDelimiter()
    template = 'phenotyping/trait/phenotyping_generic_trait.html'
    list_attributes=['name','abbreviation','comments', 'projects','ontology terms']
    list_exceptions = ['id','phenotypicvalue',]
    title = "Trait Management"
    model = Trait
    traits, nb_per_page = _get_data(request, model)
    filetype = 'trait'
    typeid = 0
    names, labels = get_fields(form)
    if request.method == "POST":
        formf = UploadFileFormWithoutDelimiter(request.POST, request.FILES) 
        if formf.is_valid():
            tag_fields = {"list_attributes":list_attributes,
                          "admin":True,
                          'fields_name':names,
                          'fields_label':labels,
                          'title': title,
                          'creation_mode':True,
                          'form':form,
                          "formfile":formf,
                          'model' : Trait,
                          }
            return _create_phenodata_from_file(request, template, tag_fields, model)
        else:
            return generic_management(Trait, TraitForm, request, template,{"admin":True, "filetype" : filetype},"Trait Management")
        
    tag_fields = {"form" : form,
                  "formfile" : formf,
                  "data" : traits,
                  "template" : template,
                  "list_attributes" : list_attributes,
                  "list_exceptions" : list_exceptions,
                  "title" : title,
                  "model" : model,
                  "filetype" : filetype,
                  "typeid" : typeid,
                  'creation_mode':True,
        }
    return refine_search(request, tag_fields)


def create_method_from_file(request, file_dict, method_type):
    """
      Function used to create methods from a file uploaded by the user.
      :param dict file_dict: file on dict format returned by the CSVFactory
    """
    with transaction.atomic():
        for line in file_dict:
            try:
                with transaction.atomic():
                    method = Method.objects.create(name=line["Name"],
                                                   type=method_type,
                                                   description=line["Description"])
                    method.save()
                    
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+").")
                    continue
        
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return 
            
    messages.add_message(request, messages.SUCCESS, "The file has been successfully inserted.")


def update_method_from_file(request, file_dict, method_type):
    """
      Function used to update methods from a file uploaded by the user.
      :param dict file_dict: file on dict format returned by the CSVFactory
    """
    with transaction.atomic():       
        for line in file_dict:
            try:
                with transaction.atomic():
                    method = Method.objects.get(name=line["Name"], type=method_type)
                    method.description=line["Description"]
                    method.save()
                    
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+").")
                    continue
        
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return 

    messages.add_message(request, messages.SUCCESS, "The file has been successfully uploaded and the methods have been updated.")                   


@login_required
@user_passes_test(lambda u: u.is_phenotyping_admin_user(), login_url='/login/')
def method_management(request):
    """
      View to manage phenotyping methods.
    """
    form_class = MethodForm
    file_form_class = UploadFileFormWithoutDelimiter
    template = 'phenotyping/phenotyping_method.html'
    title = "Method Management"
    method_type = 1 #Phenotyping
    list_attributes=['name', 'description']
    list_exceptions = ['id','phenotypicvalue','classification','type']
    
    if request.method == "GET":
        form = form_class()
        form_file = file_form_class()
        methods = Method.objects.filter(type=method_type)
        number_all = len(methods)
        methods = add_pagination(request, methods)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        
        tag_fields = {'all':methods,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': title, 
                      'filetype':'method',
                      'typeid':method_type,
                      'excel_empty_file':True,
                      'admin':True,
                      'template':template}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':methods,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'formfile':form_file,
                               'list_attributes':list_attributes,
                               'list_exceptions':list_exceptions})
 
            return render(request,template,tag_fields)

        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            methods = Method.objects.refine_search(or_and, dico_get, method_type)

            number_all = len(methods)
            methods = add_pagination(request, methods)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':methods,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'formfile':form_file,
                               'list_attributes':list_attributes,
                               'list_exceptions':list_exceptions})
 
            return render(request,template,tag_fields)

        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
             
            methods = Method.objects.refine_search(or_and, dico_get, method_type)

            number_all = len(methods)
            download_not_empty = False
            if methods:
                download_not_empty = write_csv_search_result(methods, list_exceptions)#, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            methods = add_pagination(request, methods)
        
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':methods,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           'formfile':form_file,
                           'list_attributes':list_attributes,
                           'list_exceptions':list_exceptions})  
        return render(request, template, tag_fields)
        
        
    elif request.method == "POST":
        form = form_class(request.POST)
        form_file = file_form_class(request.POST, request.FILES)
        
        methods = Method.objects.filter(type=method_type)
        number_all = len(methods)
        methods = add_pagination(request, methods)
        names, labels = get_fields(form)
        
        tag_fields = {'all':methods,
                      'number_all':number_all,
                      'fields_name':names,
                      'fields_label':labels,
                      'list_attributes':list_attributes,
                      'title': title,
                      'creation_mode':True,
                      'form':form,
                      'formfile':form_file,
                      'filetype':'method',
                      'typeid':method_type,
                      'excel_empty_file':True,
                      'admin':True,
                      'template':template}
        
        if form_file.is_valid():
            print(request.POST)
            
            file = request.FILES['file']
            myfactory = CSVFactoryForTempFiles(file)
            myfactory.find_dialect()
            myfactory.read_file()
            file_dict = myfactory.get_file()
            
            headers_list = ['Name','Description']
            
            missing = myfactory.check_headers_conformity(headers_list)
            
            if missing != []:
                messages.add_message(request, messages.ERROR, "One or several mandatory headers are missing in your file : "+format(', '.join(missing))+\
                                     ". The required headers for this file are: "+format(', '.join(headers_list))+".")
                return redirect('cl_method')
            
            if request.POST.get('submit'):
                print("create")
                create_method_from_file(request, file_dict, method_type)
                
            elif request.POST.get('update'):
                print("update")
                update_method_from_file(request, file_dict, method_type)
            
            methods = Method.objects.filter(type=method_type)
            number_all = len(methods)
            methods = add_pagination(request, methods)
            
            tag_fields.update({'all':methods,
                               'number_all':number_all})
            
            return render(request,template,tag_fields)
            
        else:
            return generic_management(Method, MethodForm, request, template,dict(tag_fields, **{"method_type":method_type}),"Method Management")
    
    else:
        print("An error occurred.")
        return redirect('method_management')


def update_method_from_form(request, form_class):
    """
      Function used to update an method object from the form.
      :param form form_class: method form class
    """
    if "hiddenid" in request.POST.keys():
        m = Method.objects.get(id=request.POST['hiddenid'])
    else:
        messages.add_message(request, messages.ERROR, "An error as occurred, please try again.")
        return
    
    try:
        old_file = m.method_file.path if m.method_file else None
        
        form = form_class(request.POST, request.FILES, instance=m)
        
        if form.is_valid():
            
            file_to_delete = False
            if old_file and (type(form.cleaned_data['method_file']) == InMemoryUploadedFile or form.cleaned_data['method_file'] == False):
                file_to_delete = True
            
            form.save()
            
            #When the form is saved, the file is deleted
            if file_to_delete:
                os.remove(old_file)

        else:
            messages.add_message(request, messages.ERROR, ' '.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form.errors.items() ]))

    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
        return
    
    messages.add_message(request, messages.SUCCESS, "The method named {0} has been updated.".format(m.name))
    return 



@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def method_management_edit(request, object_id):
    """
      View to manage the edit of phenotyping methods.
    """
    form_class = MethodForm
    file_form_class = UploadFileFormWithoutDelimiter
    template = 'phenotyping/phenotyping_method.html'
    title = "Method Management"
    method_type = 1 #Phenotyping
    list_attributes=['name', 'description']
    list_exceptions = ['id','phenotypicvalue','classification','type']
    
    if request.method == "GET":
        try:
            m = Method.objects.get(id=object_id)
        except:
            messages.add_message(request, messages.ERROR, "This method doesn't exist in the database.")
            return redirect('method_management')
        
        form = form_class(initial={'name':m.name,
                                   'description':m.description,
                                   'method_file':m.method_file
                                   })
        form_file = file_form_class()
        
        methods = Method.objects.filter(type=method_type)
        number_all = len(methods)
        methods = add_pagination(request, methods)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        
        tag_fields = {'all':methods,
                      'hiddenid':m.id,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': title, 
                      'filetype':'method',
                      'typeid':method_type,
                      'excel_empty_file':True,
                      'admin':True,
                      'template':template}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':methods,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'formfile':form_file,
                               'list_attributes':list_attributes,
                               'list_exceptions':list_exceptions})
 
            return render(request,template,tag_fields)

        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            methods = Method.objects.refine_search(or_and, dico_get, method_type)

            number_all = len(methods)
            methods = add_pagination(request, methods)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':methods,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'formfile':form_file,
                               'list_attributes':list_attributes,
                               'list_exceptions':list_exceptions})
 
            return render(request,template,tag_fields)

        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
             
            methods = Method.objects.refine_search(or_and, dico_get, method_type)

            number_all = len(methods)
            download_not_empty = False
            if methods:
                download_not_empty = write_csv_search_result(methods, list_exceptions)#, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            methods = add_pagination(request, methods)
        
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':methods,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':False,
                           'form':form,
                           'formfile':form_file,
                           'list_attributes':list_attributes,
                           'list_exceptions':list_exceptions})  
        return render(request, template, tag_fields)

    elif request.method == "POST":
        update_method_from_form(request, form_class)
        
        return redirect('method_management')


@login_required
@user_passes_test(lambda u: u.is_phenotyping_admin_user(), login_url='/login/')
def phenotypingvalue_management(request):
    phenotypingvalues = PhenotypicValue.objects.all()
    formattype = PhenotypingValueForm()
    register_formfile = UploadPhenoTypValueFileForm()
     
    names, labels = get_fields(formattype)
 
    template = 'phenotyping/pheno_values/phenotyping_values.html'
#     
    tag_fields = {'fields_name':names,
                  'fields_label':labels,
                  "admin":True,
                  }
    
    if request.method == "GET":
        count = PhenotypicValue.objects.count()
        if count <1:
            count = 'no phenotypic data in database'

        warning = 'The field separator into your CSV file must be a "Semi-colons (;)"'
        tag_fields.update({'all':phenotypingvalues,
                           'form':register_formfile,
                           'creation_mode':True,
                           'allentries':count,
                           'warning':warning})
        return render(request, template, tag_fields)
        
    elif request.method == 'POST':   
        formf = UploadPhenoTypValueFileForm(request.POST, request.FILES)        
        if formf.is_valid():                             
            tag_fields.update({'form':formf,
                               'fields_label':labels,
                               'fields_name':names,
                               'creation_mode':True}) 
            return getphenodata(request, tag_fields, template)            
    else:
        return generic_management(PhenotypicValue, formf, request, template,{}, "PhenotypingValue Management")
    
    return render(request, template, {"admin":True,'form':formf,'creation_mode':True, 'all':phenotypingvalues, 'fields_label':labels,'fields_name':names})

 
@login_required
@user_passes_test(lambda u: u.is_phenotyping_admin_user(), login_url='/login/')
def ontology_management(request):
    """
        if it's a POST request, it's either for OntologyForm (then we call generic_management to create the data) or OntologyUpdateForm (  )
        if it's a GET method, we call refine_search 
        
        :var form form : for to create a new element of this model 
        :var form formf : form for the file that we can upload to insert or update data
        :var string template : template used
        :var list list_attributes : all the fields of one element of this model
        :var list list_exceptions : 
        :var model model : 
        :var list ontologies : all the elements to display on this page
        :var string filetype :
        :var int typeid :
        :var list names : all the fields of one element of this model
        :var list labels : all the fields of one element of this model but starting with a capital letter
        :var string title : title to display on the page
        :var dict tag_fields : dict used to give all the information to a method 
        
    """ 
    form = OntologyForm()
    formf = OntologyUpdateForm()
    template = 'phenotyping/ontology/phenotyping_generic_ontology.html'
    list_attributes=['name', 'ontology id', 'description', 'url']
    list_exceptions = ['id','phenotypicvalue',]
    title = "Ontology Management"
    model = Ontology
    ontologies, nb_per_page = _get_data(request, model)
    filetype = 'ontology'
    typeid = 0
    names, labels = get_fields(formf)
    
    if request.method == "POST":
        formf = OntologyUpdateForm(request.POST, request.FILES) 
        
        if formf.is_valid():
            dictvalues_final = {}
            ont = Ontology.objects.get(pk=request.POST.get('ontology'))
            dictvalues_final['ontology_id'] = ont.ontology_id
            store_ontology_file(request.FILES, dictvalues_final)
            tag_fields = {"form" : form,
                  "formfile" : formf,
                  "data" : ontologies,
                  "template" : template,
                  "list_attributes" : list_attributes,
                  "list_exceptions" : list_exceptions,
                  "title" : title,
                  "model" : model,
                  "filetype" : filetype,
                  "typeid" : typeid,
                  'creation_mode':True,
                  }
            return refine_search(request, tag_fields)
        else :
            print('not valid')
            return generic_management(Ontology, OntologyForm, request, template,{"admin":True,"list_ont":Ontology.objects.all(),"filetype" : filetype, "typeid" : typeid},"Ontology Management")
          
    tag_fields = {"form" : form,
                  "formfile" : formf,
                  "data" : ontologies,
                  "template" : template,
                  "list_attributes" : list_attributes,
                  "list_exceptions" : list_exceptions,
                  "title" : title,
                  "model" : model,
                  "filetype" : filetype,
                  "typeid" : typeid,
                  'creation_mode':True,
        }
    return refine_search(request, tag_fields)

#====================================  EDIT METHODS  ====================================

def trait_edit(request, pk):
    template = "phenotyping/trait/phenotyping_generic_trait.html"
    form_class = TraitForm2
    formf = UploadFileForm()
    type = "Trait"
    list_exceptions = ['id','phenotypicvalue',]
    traits, nb_per_page = _get_data(request, Trait)
    obj = Trait.objects.get(pk=pk)
    obj_projects = []
    for p in obj.projects.values() : #we get the pk from obj.projects to select them in the form 
        obj_projects.append(p['id'])
    names = ['name', 'abbreviation', 'comments', 'ontology_terms', 'projects']
    labels = ['Name', 'Abbreviation', 'Comments', 'Ontology Terms', 'Projects']
    if request.method == "GET":
        form = form_class(initial = {'name':obj.name,
                                     'abbreviation':obj.abbreviation,
                                     'comments':obj.comments,
                                     'projects':obj_projects})  
        
        dict = {"admin":True, "type":type, 
                "object":obj, "ontology_terms":obj.ontology_terms, 
                "form": form, "formfile":formf, 
                "all": Trait.objects.all(), "data": traits, 
                "fields_label":labels, "fields_name":names,
                "number_all" : len(Trait.objects.all()),
                "template" : template, "model" : Trait, 
                'creation_mode':False,"list_attributes" : names, 
                "filetype" : 'trait', "typeid" : 0,
                "list_exceptions" : list_exceptions,}
        
        return refine_search(request, dict)
    else : 
        form = TraitForm2(request.POST or None, instance=obj)
        if form.is_valid():
            obj.name = request.POST['name']
            obj.abbreviation = request.POST['abbreviation']
            obj.comments = request.POST['comments']
            print('post :'+str(request.POST))
            ont = str(request.POST['ontology_terms'])
            if ont.__contains__('[') : 
                ont = ont[1:len(ont)-1].replace('\'','').replace('"','')
            else :
                ont = ont.replace('\'','').replace('"','')
            obj.ontology_terms = '{'+str(ont)+'}'
            projects = Project.objects.filter(pk__in=request.POST.getlist('projects'))
            obj.projects.set(projects)
        try:
            obj.save()
        except : 
            dict = {'admin':True, "type":type, 
                    "form":form, #"error_msg":error_msg, 
                    "ontology_terms":obj.ontology_terms, "object":obj,
                    "formfile":formf, "all": Trait.objects.all(), 
                    "fields_label":labels, "fields_name":names,
                    "template" : template, "model" : Trait, 
                    'creation_mode':False,"list_attributes" : names, 
                    "filetype" : 'trait', "typeid" : 0, 
                    "update":True,  "list_exceptions" : list_exceptions,
                    "hiddenid":pk,"number_all" : len(Trait.objects.all()),}
            
            return refine_search(request, dict)
        
        
    dict = {"admin":True, "type":type, 
            "object":obj, "ontology_terms":obj.ontology_terms, 
            "form": form, "formfile":formf, 
            "all": Trait.objects.all(), "data": traits, 
            "fields_label":labels, "fields_name":names,
            "number_all" : len(Trait.objects.all()),
            "template" : template, "model" : Trait, 
            'creation_mode':False,"list_attributes" : names, 
            "filetype" : 'trait', "typeid" : 0, 
            "update":True,  "list_exceptions" : list_exceptions,}
    
    return refine_search(request, dict)


def ontology_edit(request, pk):
    print('ontology_edit')
    form_class = OntologyForm2
    template = "phenotyping/ontology/phenotyping_edit_ontology.html"
    type = "Ontology"
    ontologies, nb_per_page = _get_data(request, Ontology)
    list_exceptions = ['id','phenotypicvalue',]
    obj = Ontology.objects.get(pk=pk)
    formf = OntologyUpdateForm()
    names = ['ontology_id', 'name', 'description', 'url', 'file']
    labels = ['Ontology id','Name', 'Description', 'Url', 'File']
    if request.method == "GET":
        form = form_class(initial={'name':obj.name,
                                   'ontology_id':obj.ontology_id,
                                   'url':obj.url,
                                   'description':obj.description,
                                   })
        dict = {"admin":True, "type":type, "object":obj, 
                "form": form, "formfile":formf, 
                "all": Ontology.objects.all(), "fields_label":labels, 
                "fields_name":names,"hiddenid":pk, "data": ontologies, 
                "number_all" : len(Ontology.objects.all()),
                "template" : template, "model" : Ontology, 
                'creation_mode':False,"list_attributes" : names, 
                "filetype" : 'ontology', "typeid" : 0,
                "list_exceptions" : list_exceptions,}
        return refine_search(request, dict)
        #return render(request, template, dict)
    else : 
        form = OntologyForm2(request.POST or None, instance=obj)
        if form.is_valid():
            obj.name = request.POST['name']
            obj.description = request.POST['description']
            obj.ontology_id = request.POST['ontology_id']
            obj.url = request.POST['url']
        try:
            obj.save()
        except Exception as e:
            error_msg = e
            dict = {'admin':True, "type":type, "form":form, "error_msg":error_msg,
            "formfile":formf, "all": Trait.objects.all(), "fields_label":labels, "fields_name":names,"hiddenid":pk}
            #return render(request, template, dict)
            return refine_search(request, dict)
    dict = {"update":True,"admin":True, "type":type, "object":obj, 
                "form": form, "formfile":formf, 
                "all": Ontology.objects.all(), "fields_label":labels, 
                "fields_name":names,"hiddenid":pk, "data": ontologies, 
                "number_all" : len(Ontology.objects.all()),
                "template" : template, "model" :Ontology, 
                'creation_mode':False,"list_attributes" : names, 
                "filetype" : 'ontology', "typeid" : 0,
                "list_exceptions" : list_exceptions,}
    #return render(request, template, dict)
    return refine_search(request, dict)

#=================================  AJAX REQUEST  =======================================
#for the widget ajax request
def get_ont_terms(request):
    """
        :var list objects : names of the selected traits  
        :var list ont_terms : ontology terms linked to the selected traits
        :var object instance : for the for, correspond to one of the selected traits
        :var - temp : ontology terms linked to instance
        :var - l :
        :var - all :
        :var - response : 
    """
    if request.method == "POST":
        #print(str(request.POST.get('objects[]', None)))
        objects = []
        ont_terms = []
        i = 1
        try :
            objects = request.POST.get('objects').split(',')
            for obj in objects :
                instance = Trait.objects.filter(pk=int(obj))[0]
                temp = str(instance.ontology_terms)
                l = len(temp)
                print(l)
                if (l > 2):
                    temp = temp[1:l-1]
                    print("instance : "+str(instance)+", ont : "+str(temp))
                    if i == 1 : 
                        all = temp
                    else :
                        all = all +', '+ temp
                    i += 1
            ont_terms.append(all)
            
            response = {"response":ont_terms}
            print("response : "+str(response))
            return HttpResponse(json.dumps(response))
        except Exception as e:
            print(e)
            return HttpResponseServerError()
    else :
        raise Http404


#========================================================================

#COMMENT BECAUSE NO CALL

# def handle_uploads(request, keys):
#     saved = []   
#     upload_dir = date.today().strftime(settings.UPLOAD_PATH)
#     upload_full_path = os.path.join(settings.ROOT_URL, upload_dir)
#
#     if not os.path.exists(upload_full_path):
#         os.makedirs(upload_full_path, 0o755)
#
#     for key in keys:
#         if key in request.FILES:
#             upload = request.FILES[key]
#             while os.path.exists(os.path.join(upload_full_path, upload.name)):
#                 upload.name = '_' + upload.name
#             dest = open(os.path.join(upload_full_path, upload.name), 'wb')
#             for chunk in upload.chunks():
#                 dest.write(chunk)
#             dest.close() 
#             saved.append((key, os.path.join(upload_dir, upload.name)))
#     return saved


def getphenodata(request, tag_fields, template):
    main_f = request.FILES['main_file']
    envi = request.POST["environment"]
    meth = request.POST["method"]
    doc = None
    env = None
    method = None
    phenotypingvalues = PhenotypicValue.objects.all()
    
    try:        
        env = Environment.objects.get(id=envi)
    except Environment.DoesNotExist :
        errormsg='Environment not found in database'
        tag_fields.update({'errormsg':errormsg})
        return render(request, template, tag_fields)
    try:
        method = Method.objects.get(id=meth)
    except Method.DoesNotExist :
        errormsg='Method not found in database'
        tag_fields.update({'errormsg':errormsg})
        return render(request, template, tag_fields)

    file_dialect = csv.Sniffer().sniff(main_f.read().decode(errors='ignore'))
    main_f.seek(0)
    file_reader = csv.DictReader(io.StringIO(main_f.read().decode(errors='ignore')),dialect=file_dialect)         

    for row in file_reader:
        print(row)
        try:
            seedlot = Seedlot.objects.get(name = row[''])
        except:
            errormsg = "This seedlot doesn't exist in the database : "+row['']
            return render(request, 'phenotyping/phenotyping_values.html', {"phenotypingvalues":phenotypingvalues,"admin":True,'form':UploadPhenoTypValueFileForm, 'errormsg':errormsg})
             
        keys_row = [i for i in row.keys()]
        keys_row.remove('')
         
        for traits_file in keys_row:
            try:
                trait = Trait.objects.get(name=traits_file)
                value = row[traits_file]
                phenoval, created = PhenotypicValue.objects.get_or_create(sourcedata=doc,environment=env,method=method,seedlot=seedlot,trait=trait,defaults={'value':value})
            except:
                errormsg = "This trait doesn't exist in the database : "+traits_file
                return render(request, 'phenotyping/phenotyping_values.html', {"phenotypingvalues":phenotypingvalues,"admin":True,'form':UploadPhenoTypValueFileForm, 'errormsg':errormsg})
             
    phenotypingvalues = PhenotypicValue.objects.all()
    count = PhenotypicValue.objects.count()

    if count<1 :
        count = 'no phenotypic data in database'
    tag_fields.update({'all':phenotypingvalues, 'allentries':count})
      
    return render(request, template, tag_fields)


@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def viewphenovalues(request):
    error_msg=""
    headername = None
    if request.method=="POST":
        form2 = PhenoViewerAccessionForm(request.POST)
        formtrait = PhenoViewerTraitForm(request.POST)
        formenv = PhenoViewerEnvironmentForm(request.POST)
        if form2.is_valid() and formenv.is_valid() and formtrait.is_valid():
                if request.POST.getlist("name"):
                    id_autocomplete = request.POST.getlist("name")
                    listaccessions = Accession.objects.filter(id__in=id_autocomplete)   
                else:   
                    listaccessions = form2.cleaned_data['accession']
                listraits = formtrait.cleaned_data['trait']
                listenvs = formenv.cleaned_data['environment']   
                #============================
                mode = request.POST["choose_visualization_mode"]
                if mode == "1":
                    headername = 'Accession'
                else:
                    headername = 'Seedlot'     
                #===========================================================           
    
                if listaccessions!= None and listraits!= None and listenvs!= None :
                    df = get_phenodata(listaccessions, listraits, listenvs, headername)
                    if df.empty:
                        html=None
                    else:
                        html = df.to_html()    
                        html = html.split('\n')
    
                    pheno_html="phenotyping/phenotyping_base.html"
                    if request.POST["export_or_view_in_web_browser"]=="2":
                        return render(request, 'phenotyping/pheno_values/dtviewphenovalues.html', {"admin":True,"pheno_html":pheno_html,"html":html,'traits':Trait.objects.all()})
                    else:
                        return return_csv_pheno(request)
                        
                else:
                    if listaccessions==None:
                        error_msg="Please choose at least one accession (step 1)."
                    elif listraits==None:
                        error_msg="Please choose at least one trait (step 2)."
                    elif listenvs==None:
                        error_msg="Please choose at least one environment (step 3)."
                                        
                    return render(request, 'phenotyping/pheno_values/viewphenovalues.html', {"admin":True,'error_msg':error_msg,'accesform':PhenoViewerAccessionForm, 'traitform':PhenoViewerTraitForm, 'enviform':PhenoViewerEnvironmentForm, 'visumodeform':DataviewPhenotypingValuesForm, 'traits':Trait.objects.all()})       
                          
                #========================================================
        else:
            if not request.POST.getlist("name") and not request.POST.getlist('accession'):
                error_msg="Please choose at least one accession (step 1)."
            elif not request.POST.getlist("trait"):
                error_msg="Please choose at least one trait (step 2)."
            elif not request.POST.getlist('environment'):   
                error_msg="Please choose at least one environment (step 3)."
    accesform = PhenoViewerAccessionForm()
    traitform = PhenoViewerTraitForm()
    enviform = PhenoViewerEnvironmentForm()
    visumodeform = DataviewPhenotypingValuesForm()
    return render(request,'phenotyping/pheno_values/viewphenovalues.html',{"admin":True,'error_msg':error_msg,'accesform':accesform, 'traitform':traitform, 'enviform':enviform, 'visumodeform':visumodeform, 'traits':Trait.objects.all()})


def viewontology(request):
    try :
        object = Trait.objects.filter(name = list(request.GET.keys())[0])[0]

        return render(request,'phenotyping/arborescence.html',{"ontology_terms":object.ontology_terms})
    except : 
        return render(request,'phenotyping/arborescence.html',{})


def get_phenodata(listaccessions, listraits, listenvs, headername):
    traitslist = [exp for exp in listraits if exp != None]        
    envilist = [env for env in listenvs if env != None]
    acclist = [acc for acc in listaccessions if acc != None]
    df_pheno=pd.DataFrame()
    for trait in traitslist:
        for e in envilist:
            tr = str(trait.name) +'\n'+ str(e.name)
            for acces in acclist:
                seedlots=Seedlot.objects.filter(accession=acces)
                for seed in seedlots:
                    phenos=PhenotypicValue.objects.filter(seedlot=seed,environment=e,trait=trait)
                    for pheno in phenos:
                        #print(acces,pheno.value)
                        if headername == 'Accession':
                            df_pheno.loc[acces,tr]=str(pheno.value).replace(',', '.')
                        else: 
                            df_pheno.loc[seed,tr]=str(pheno.value).replace(',', '.')
    df_pheno.to_csv(settings.TEMP_FOLDER+"/view_pheno.csv", sep=';')
    return df_pheno


def return_csv_pheno(request):
    print('return_csv_pheno')
    tmpfile=open(settings.TEMP_FOLDER+'/view_pheno.csv','r')  
    response=HttpResponse(tmpfile.read(),content_type="application/vnd.ms-excel")
    response['Content-Disposition']='attachment; filename=view_pheno.csv'

    return response


def refine_search(request, tag_fields):
    """
        :var dict tag_fields : storing all the needed data
        
        :var list data : objects from the method _get_data if there is no search, 
        :var string model : model
        :var int number_all : total number of pages
        :var int nb_per_page : number of objects on each page
        :var list names : fields' names
        :var list labels : same as names but each one starting with a capital letter
        :var form formupdate : used for ontology because the form for an update is not the same as the insert one
        :var - dico_get : 
        :var string or_and : 
        
        :var - text_data : 
        :var - proj : 
        :var - query :
        :var - dat :
        :var - dico_type_data :
        :var - data_type : 
        :var - value :
        :var - var_search :
        :var - download_not_empty :
    """
    data = tag_fields['data']
    model = tag_fields['model']
    number_all = len(data)
    nb_per_page = 50
    names, labels = get_fields(tag_fields['form'])
    if model == Trait : 
        names = ['name', 'abbreviation', 'comments', 'ontology_terms', 'projects']
        labels = ['Name', 'Abbreviation', 'Comments', 'Ontology Terms', 'Projects']
    formupdate = tag_fields['form']
    if model == 'Ontology':
        formupdate = OntologyUpdateForm()

    tag_fields.update({'all':data,
              'fields_name':names,
              'fields_label':labels,
              "admin":True,})
    dico_get={}
    or_and = None
    
    if "no_search" in request.GET:
        tag_fields.update({"search_result":dico_get,
                       "or_and":or_and,
                       'all':data,
                       "number_all":number_all,
                       'nb_per_page':nb_per_page,
                       'formupdate' : formupdate})
        if tag_fields['creation_mode'] == False :
            tag_fields.update({'creation_mode':False})
        else :
            tag_fields.update({'creation_mode':True})
        if tag_fields['typeid'] == 1 and model == Trait :
            tag_fields.update({'creation_mode':False})
        return render(request,tag_fields['template'],tag_fields)
    
    
    elif "or_and" in request.GET:
        or_and=request.GET['or_and']
        for i in range(1,11):
            data="data"+str(i)
            text_data="text_data"+str(i)
            if data in request.GET and text_data in request.GET:
                dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera

        nb_per_page = 50
        if or_and == "or":
            proj=[]
            query=Q()
            for dat, dico_type_data in dico_get.items():
                if "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        print('Projects empty')
                        query &= Q(projects__isnull=True)
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query &= Q(projects__in=proj)
                else:
                    for data_type, value in dico_type_data.items():
                        var_search = str( data_type + "__icontains" ) 
                        var_search = var_search.replace(' ','_')
                        query |= Q(**{var_search : dico_type_data[data_type]})
            
        if or_and == "and":
            proj=[]
            query=Q()
            for dat, dico_type_data in dico_get.items():
                if "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        print('Projects empty')
                        query &= Q(projects__isnull=True)
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query &= Q(projects__in=proj)
                else:
                    for data_type, value in dico_type_data.items():
                        var_search = str( data_type + "__icontains" ) 
                        var_search = var_search.replace(' ','_')
                        print("----------",var_search)
                        query &= Q(**{var_search : dico_type_data[data_type]})
    
        data = model.objects.filter(query).distinct()
        print(data)
        number_all = len(data)
        download_not_empty = False
        if data:
            download_not_empty = write_csv_search_result(data, tag_fields['list_exceptions'])
        tag_fields.update({"download_not_empty":download_not_empty})
            
    #Cas où veut afficher la base de données
    #accessions = _get_accessions(accessiontype, attributes, request)
    tag_fields.update({"search_result":dico_get,
                       "or_and":or_and,
                       'all':data,
                       "number_all":number_all,
                       'nb_per_page':nb_per_page,
                       'excel_empty_file':True,
                       "list_ont" : Ontology.objects.all(),
                       "model_name":model,
                       'app_label' : "phenotyping",
                       })  
    if tag_fields['creation_mode'] == False :
        tag_fields.update({'creation_mode':False})
    else :
        tag_fields.update({'creation_mode':True})
        
    return render(request, tag_fields['template'], tag_fields) 


#====================================  FILE METHODS  ====================================

def _get_data(request, model):
    """
        returns the data depending on the number of element on each pages and the current page
    
        :var list data : data that should be displayed on the current page
        :var int nb_per_page : number of elements on each page
        :var - paginator : 
        :var int page : current page
    """
    
    if model.__name__ == "Image":
        data = model.objects.all().order_by('image_name')
    else:
        data = model.objects.all().order_by('name')

    # Adding Paginator  
    try:
        if request.method == "GET":
            nb_per_page=request.GET['nb_per_page']  
        elif request.method == "POST":
            nb_per_page=request.POST["nb_per_page"]   
        if nb_per_page=="all":
            nb_per_page = len(data)
    except:
        nb_per_page=50
    
    paginator = Paginator(data, nb_per_page)
    page = request.GET.get('page')
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        # If page number is not an interger then page one
        data = paginator.page(1)
    except EmptyPage:
        # invalid page number show the last
        data = paginator.page(paginator.num_pages)    
    return data, nb_per_page     


def _create_phenodata_from_file(request, template, tag_fields, modelname):
    """
        method called when the user wants to insert or update data using a file :
        calls _upload_file (this method will create or update the data or return an error), checks if there is an error
        if there is no error uses the method _get_data to get the data to display on this page  
        
        :var string template : 
        :var dict tag_fields :
        :var string modelname : 
        
        :var - file_uploaded : it's the value return by the methods reading the file, if it's a string it means that there was an error 
        :var int counter : used to count how many objects name were returned in file_uploaded
        :var string errormsg : used to create the error message that will be return
        :var string missing_data : used to store the element that are not in the database when the user wants to update them 
        :var boolean inserted : true if there were no errors 
        :var list data : data to display on the page
        :var int nb_per_page : number of element on each page
        :var list data_all : all the data for this model
        :var int number_all : number of total element
    """
    file_uploaded = _upload_file(request.FILES['file'], request, modelname)
    
    if type(file_uploaded) is str : #1er cas: l'insertion provoque des erreurs
        if file_uploaded.count(';') > 0 :
            counter = file_uploaded.count(';') + 1
            if counter == 1:
                errormsg = 'A {0} from your file is already in the database: '.format(str(modelname.__name__)) + file_uploaded
            else:
                errormsg=  str(counter) + ' {0}s are already in your database: '.format(str(modelname.__name__)) + file_uploaded
            tag_fields.update({'errormsg':errormsg})
            return render(request, template, tag_fields)
        else :
            tag_fields.update({'errormsg':file_uploaded})
            
            return render(request, template, tag_fields)
        
    elif type(file_uploaded) is list:
        #Cas où l'utilisateur tente un update mais que certaines données du CSV ne sont pas dans la BDD
        missing_data = file_uploaded[0]
        del file_uploaded[0]
        if file_uploaded !=[] and len(file_uploaded) ==1:
            missing_data= missing_data + ' ,' + file_uploaded[0]
        elif file_uploaded != [] and len(file_uploaded)>1:
            missing_data = missing_data + ' ,' +  '  ,'.join(file_uploaded)
        errormsg = 'Some {0}s of your file aren\'t in your database: '.format(str(modelname.__name__)) + missing_data + ' .No update was done.'
        tag_fields.update({'errormsg':errormsg})
        
        return render(request, template, tag_fields)
    
    else:
        #Cas d'une insertion où d'un update fonctionnel
        inserted = True
        data, nb_per_page = _get_data(request,modelname)
        data_all = modelname.objects.all().distinct()
        number_all = len(data_all)
        tag_fields.update({'inserted': inserted,'all':data, "nb_per_page":nb_per_page, "number_all":number_all})
        
        return render(request, template, tag_fields)


def _upload_file(_file, request, modelname):
    """
        Reads the file, create a dict with the data 
        and calls _submit_upload_file if it's an insert or _update_upload_file if it's an update 
        
        :var file _file : uploaded file
        :var string modelname : model
        
        :var dict dictvalues_final : data from the file
        :var - dialect : used to know the delimiter of the csv file
        :var - reader : used to read the file
        :var list fieldnames : fieldnames from the file
        :var dict line : used in the for to read a line from file
        :var dict dictvalues : used in the for to store data from a line before storing it in dictvalues_final
    """
    if _file != None:
        _file.seek(0)
        dictvalues_final = {}
        dialect = csv.Sniffer().sniff(_file.read().decode())
        _file.seek(0)
        reader = csv.DictReader(io.StringIO(_file.read().decode()),dialect=dialect)
        fieldnames = reader.fieldnames
        i = 0
        for line in reader :
            print(line)
            dictvalues = {}
            for c in range (0,len(line)) :
                dictvalues[fieldnames[c]] = line[fieldnames[c]]
            dictvalues_final[i] = dictvalues
            i+=1
            
        if request.POST.get("submit"): #INSERT
            return _submit_upload_file(dictvalues_final, modelname)
        
        elif request.POST.get("update"): #UPDATE
            return _update_upload_file(dictvalues_final, modelname)
        
        else:
            return 'ThaliaDB was unable to identify if you\'re trying to insert new data or to update former data.'
                        

############################################################################
#INSERT INTO DATABASE FROM A FILE

def _submit_upload_file(dictvalues_final, modelname):
    """
        Checks if the headers correspond, checks that some objects are not already in the database and then calls submit_upload_file_main to update the database
    
        :var dict database : all object for this model
        :var list keys_in_database : all the name of the objects for this model 
        :var list keys_inserted : names of the objects inserted in the files
        :var list data_doublons : names of objects that are in the database and in the file
    """
    try:
        database=modelname.objects.all().values_list('name') #on recupere les donnees de la base
        keys_in_database=[i[0] for i in database]#on recupere leurs names
        keys_inserted=[dictvalues_final[l]['Name'] for l in dictvalues_final]#Maintenant, on récupère les noms du CSV, contenues dans dictvalues_final
    
    except KeyError:
        return '1: The headers of your files do not correspond to those in the database. Please check them'
    
    else:
        data_doublons =[x for x in keys_in_database if x in keys_inserted]
        if set(keys_in_database) == set(keys_inserted):
            return 'All the objects in your CSV file are already in your database. Maybe you are trying to update former data ?'
        
        else:
            if data_doublons != []: 
                return data_doublons
            
            else: 
                return submit_upload_file_main(dictvalues_final,modelname)
                

def submit_upload_file_main(dictvalues_final,modelname):
    """
        Creates objects in the database from a file
    
        :var dict dictvalues_final : 
        :var string modelname : model
        
        :var dict dval : used to get a line from the file
        :var object a : object we have created 
        :var list ont : used only for traits, to store the ontology terms that we want to associate with a
    """
    try :
        for k in dictvalues_final:
            dval = dictvalues_final[k]
            try :
                if modelname == Method or modelname == SpecificTreatment:
                    a  = modelname.objects.create(name = str(dval['Name']),
                                           description = str(dval['Description']))
                    a.save()
                    
                elif modelname == Trait:
                    a  = modelname.objects.create(name = str(dval['Name']))
                    a.abbreviation = str(dval['Abbreviation'])
                    a.comments = str(dval['Comments'])
                    
                    ont = dval['Ontology terms'][1:len(dval['Ontology terms'])-1]
                    ont = ont.replace("'","")
                    ont = ont.replace("\"","")
                    ont = '{'+ont+'}'
                    a.ontology_terms = ont
                    
                    add_projects(a,dval)
                    a.save()
                    
                elif modelname == Environment:
                    a  = modelname.objects.create(name = str(dval['Name']),
                                                  date = str(dval['Date']),
                                                  location = str(dval['Location']),
                                                  specific_treatment = SpecificTreatment.objects.get(name = str(dval['Specific treatment'])),
                                                  description = str(dval['Description']))
                    add_projects(a,dval)
                    a.save()
                    
            except IntegrityError as ie :
                return str(ie)
            
    except KeyError:
        return '2: The headers of your files do not correspond to those in the database. Please check them and mind the uppercase and the lowercase.'
    
    except IntegrityError as ie:
        return str(ie)

 
#########################################################################
#UPDATE DATABASE FROM A FILE                            

def _update_upload_file(dictvalues_final, modelname):
    """
        Checks if the headers correspond, if the objects are already in the database then calls update_upload_file_main to update the database
        
        :var dict dictvalues_final : 
        :var string modelname : model
        
        :var dict database : all object for this model
        :var list keys_in_database : all the name of the objects for this model 
        :var list keys_inserted : names of the objects inserted in the files
        :var list common_keys : names of objects that are in the database and in the file
    """
    try:
        database=modelname.objects.all()
        database=database.values_list('name')
        keys_in_database=[i[0] for i in database]
        keys_inserted=[dictvalues_final[l]['Name'] for l in dictvalues_final]   
        common_keys =[x for x in keys_in_database if x in keys_inserted]
    
    except KeyError:
        return 'The headers of your files do not correspond to those in the database. Please check them'
    
    else:
        if common_keys == []:
            return 'Your file shares no data with the database. Please make sure you weren\'t trying to insert new data instead of updating some. '
        else:
            return update_upload_file_main(dictvalues_final, modelname)
            
       
def update_upload_file_main(dictvalues_final, modelname):
    """
        Modifies objects with the values from a file 
        
        :var dict dval : used to get a line from the file
        :var object a : used to get the object we want to modify (the object of the line dval)
        :var list ont : used only for traits, to store the ontology terms that we want to associate with a
        :var boolean created : if the object has been created or not
    """
    try:
        for k in dictvalues_final:
            #we get the line k+1 from the file
            dval = dictvalues_final[k]
            try :
                if modelname == Method or modelname == SpecificTreatment:
                    a  = modelname.objects.select_related().filter(name = str(dval['Name']))
                    a.delete()
                    a, created  = modelname.objects.update_or_create(name = str(dval['Name']), defaults={'description' : str(dval['Description'])})                          
                    a.save()
                    
                elif modelname == Trait:
                    a = modelname.objects.select_related().filter(name = str(dval['Name']))

                    ont = dval['Ontology terms'][1:len(dval['Ontology terms'])-1]
                    ont = ont.replace("'","").replace("\"","")
                    ont = '{'+ont+'}'

                    a.delete()
                    
                    if not ont : #if there is no ontology terms
                        a, created  = modelname.objects.update_or_create(name = str(dval['Name']), defaults={'abbreviation' : str(dval['Abbreviation']), 'comments' : str(dval['Comments'])})
                    else : 
                        a, created  = modelname.objects.update_or_create(name = str(dval['Name']), defaults={'abbreviation' : str(dval['Abbreviation']), 'comments' : str(dval['Comments']), 'ontology_terms' : ont})                              
                    add_projects(a,dval)
                    a.save()
                    
                elif modelname == Environment:
                    a  = modelname.objects.select_related().filter(name = str(dval['Name']))
                    
                    a.delete()
                    a, created  = modelname.objects.update_or_create(name = str(dval['Name']), defaults={'date' : str(dval['Date']),
                                                  'location' : str(dval['Location']),
                                                  'specific_treatment' : SpecificTreatment.objects.get(name = str(dval['Specific treatment'])),
                                                  'description' : str(dval['Description'])
                                                  })                          
                    add_projects(a,dval)
                    a.save()
                    
            except IntegrityError as ie :
                return str(ie)
            
    except Exception as e :
        return str(e)           
    
    
def add_projects(obj,dval):
    """
        adds the projects in the dict dval to the object obj
        
        :var dict dval : all the data associated with obj
        :var object obj : object we want ot modify
        
        :var list project_list : projects that we want to associate with obj
        :var list projects : same as project_list but project_list is a list of string and here we store objects from the model project
    """
    project_list = [i.strip() for i in str(dval['Projects']).split(',')]
    if project_list != "['']": 
        projects = Project.objects.filter(name__in = project_list)
        
        if projects.exists():
            obj.projects.add(*list(projects))
            

################################################################
#CHANGE FILE ASSOCIATED WITH A ONTOLOGY
    
def store_ontology_file(files, dictvalues_final) :
    ''' Method used to change the file associated with an ontology : 
        We need to copy the data from the uploaded file to a new file stored in the right directory 
        we delete the old file and its ontology and recreate an ontology with the new file
        
        :var object old_ont : ontology with the id that was given in the dict in argument
        :var string old_ont_id : id of old_ont
        :var string old_ont_name : name of old_ont
        :var string old_ont_description : description of old_ont
        :var string old_ont_url : url of old_ont
        :var file old_ont_file : ontology file associated with old_ont
        
        :var file new_file : file uploaded, it will replace the file associated with old_ont
        :var string new_file_name : name of the file associated with old_ont
        :var - new_file_dialect : used to detect what was used in the csv file as a delimiter
        :var - new_file_reader : used to read new_file
        :var - new_file_unsplited_fieldnames : used to store temporarily the fieldnames and modify them (suppressing [, ] and ')
        :var list new_file_fieldnames : fieldnames of the file
        
        :var file final_file : new file where we will copy the content of new_file
        :var - final_file_writer : used to write in final_file
        :var list lines : all the lines of new_file
        
        :var dict row : used in the for to store a line from new_file then write it into final_file
        :var string field : used in the for to know the name of the current column
        :var dict line : used in the for to store a line from new_file 
        
        :var object new_ont : ontology created with the final_file
        :var boolean created : if the ontology has been created or not 
    ''' 
    #getting the data from the ontology
    old_ont = Ontology.objects.select_related().filter(ontology_id=dictvalues_final['ontology_id'])[0]
    old_ont_id = old_ont.ontology_id
    old_ont_name = old_ont.name
    old_ont_description = old_ont.description
    old_ont_url =old_ont.url
    old_ont_file = old_ont.file
    
    #remove the file so we can replace it and delete the ontology
    os.remove(settings.MEDIA_ROOT+"/"+str(old_ont_file))
    old_ont.delete()
    
    #starting to read the uploaded file
    new_file = files.getlist('file')[0]
    new_file_name = str(old_ont_file)
    new_file_dialect = csv.Sniffer().sniff(new_file.read().decode(errors='ignore'))
    new_file.seek(0)
    new_file_reader = csv.DictReader(io.StringIO(new_file.read().decode(errors='ignore')),dialect=new_file_dialect)
    new_file_unsplited_fieldnames = new_file_reader.fieldnames
    new_file_unsplited_fieldnames = str(new_file_unsplited_fieldnames).replace("'", "")
    new_file_fieldnames = str(new_file_unsplited_fieldnames)[1:len(str(new_file_unsplited_fieldnames))-1].split(',')

    #creating a file and writing the data from the uploaded file
    final_file = open(settings.MEDIA_ROOT+'/'+str(old_ont_file), 'w+') 
    final_file_writer = csv.writer(final_file)
    for i in range (0,len(new_file_fieldnames)):
        if i != 0 :
            new_file_fieldnames[i] = str(new_file_fieldnames[i])[1:len(str(new_file_fieldnames[i]))]
    final_file_writer.writerow(new_file_fieldnames)
    lines = list(new_file_reader) 
    for line in lines : 
        #the fieldnames are not necessarily in the right order 
        #so we are writing the values in the dict row in the correct order and we use row to write the values in final_file
        row = OrderedDict()
        for i in range (0,len(new_file_fieldnames)):
            field = new_file_fieldnames[i]
            row[field] = str(line[field])
        final_file_writer.writerow(row.values())
            
    #creating a new ontology with the final_file and the data that we stored from the old_ont
    new_ont, created  = Ontology.objects.update_or_create(ontology_id=old_ont_id, defaults={'name':old_ont_name, 'description' : old_ont_description, 'url':old_ont_url, 'file' : new_file_name})
    