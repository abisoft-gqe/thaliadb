# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from jsonfield import JSONField
import json
import datetime

from django.apps import apps
from django.db import models
from django.dispatch import receiver
from django.contrib.postgres.fields import ArrayField
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.conf import settings
from django.core.files.storage import FileSystemStorage

from phenotyping.managers import TraitManager, EnvironmentManager
from team.serializers import ContactsSerializer
from team.models import Person

#****************** 3/06/15
import datetime

class SpecificTreatment(models.Model):
    name = models.CharField(max_length = 100, unique = True)
    description = models.TextField(max_length=500, blank=True, null=True)
    def __str__(self):
        return self.name

class Environment(models.Model) :
    name = models.CharField(max_length = 100, unique = True)
    #********3/06/15 **********************************
    date = models.IntegerField()
    location = models.CharField(max_length = 100)
    description = models.TextField(max_length=500, blank=True, null=True)
    specific_treatment = models.ForeignKey('SpecificTreatment', blank=True, null=True, on_delete=models.CASCADE)
    objects = EnvironmentManager() 
    
    projects = models.ManyToManyField('team.Project')
    def __str__(self):
        return self.name
    
    class Meta:
        ordering = ['name',]
     
    #for BrAPI    
    def getBlankDateValue(self):
        return '1900-01-01T12:00:00-0600'
    
    def getBlankList(self):
        return []
    
    def getBlankObject(self):
        return {}
    
    def getBlankValue(self):
        return ''
    
    def getActive(self):
        return 'true'
        
    def getCommonCropName(self):
        return settings.COMMONCROPNAME    
    
    def getDocumentationURL(self):
        return 'https://brapicore21.docs.apiary.io/#/reference/studies'
    
    def getEnvironmentParameters(self):
        return [{'description' : self.specific_treatment.description, 'parameterName' : self.specific_treatment.name, 'parameterPUI' : '', 'unit' : '', 'unitPUI' : '', 'value' : '', 'valuePUI' : ''}]
    
    def getExternalReferences(self):
        return [{'referenceId' : '','referenceSource': ''}]
    
    def getExperimentalDesign(self):
        externalDesign = {}
        externalDesign['PUI'] = ''
        externalDesign['description'] = ''
        return externalDesign
    
    def getGrowthFacility(self):
        growthFacility = {}
        growthFacility['PUI'] = ''
        growthFacility['description'] = ''
        return growthFacility
    
    def getLastUpdate(self):
        lastUpdate = {}
        lastUpdate['timestamp'] = '1900-01-01T12:00:00-0600'
        lastUpdate['version'] = ''
        return lastUpdate
    
    def getStudiesAdditionalInfo(self):
        studiesAdditionalInfo = {}
        if self.date :
            studiesAdditionalInfo['year'] = str(self.date)
        return studiesAdditionalInfo
        
    
class Ontology(models.Model) :
    ontology_id = models.CharField(max_length = 100, unique = True)
    name = models.CharField(max_length = 100)
    description = models.TextField(max_length=500, blank=True, null=True)
    last_file_update = models.DateTimeField(auto_now_add=True)
    url = models.URLField(max_length = 200 , null=True, blank=True)
    fs = FileSystemStorage(location=settings.MEDIA_ROOT)
    file = models.FileField(upload_to= "ontology", storage=fs, unique = True)
    def __str__(self):
        return self.name+", "+self.ontology_id
    
    #for BrAPI
    def getBlankValue(self):
        return '' 
    
    def getDocumentationURL(self):
        return str(self.url)
    
    def getOntologyAdditionalInfo(self):
        ontologyAdditionalInfo = {}
        if self.last_file_update :
            ontologyAdditionalInfo['last_file_update (date)'] = str(self.last_file_update)
        if self.fs :
            ontologyAdditionalInfo['file system storage'] = str(self.fs)
        if self.file :
            ontologyAdditionalInfo['file'] = str(self.file)
        return ontologyAdditionalInfo
    
class Trait(models.Model):
    name = models.CharField(max_length = 100, unique = True) 
    abbreviation = models.CharField(max_length = 20, blank=True, null=True)
    comments = models.TextField(max_length=500, blank=True, null=True)
    ontology_terms = ArrayField(models.CharField(max_length = 100),null=True, blank=True)
    objects = TraitManager() 
    
    projects = models.ManyToManyField('team.Project')
    def __str__(self):
        return self.name
    class Meta:
        ordering = ['name',]
        
    #for BrAPI   
    def getBlankList(self):
        return []
    
    def getBlankValue(self):
        return '' 
    
    def getExternalReferences(self):
        return [{'referenceID' : '', 'referenceId' : '', 'referenceSource' : ''}]
    
    def getTraitAdditionalInfo(self):
        traitAdditionalInfo = {}
        return traitAdditionalInfo
    
    def getOntologyReference(self):
        return {'ontologyDbId' : '', 'ontologyName' : str(self.ontology_terms), 'documentationLinks' : [], 'URL' : '', 'type' : '', 'version' : ''}

class PhenotypicValue(models.Model):
    sourcedata = models.ForeignKey('team.Documents', null=True, on_delete=models.CASCADE)
    environment = models.ForeignKey('Environment', on_delete=models.CASCADE)
    method = models.ForeignKey('team.Method', on_delete=models.CASCADE)
    seedlot = models.ForeignKey('lot.Seedlot', on_delete=models.CASCADE)
    trait = models.ForeignKey('Trait', on_delete=models.CASCADE)
    value = models.CharField(max_length = 100)
    measure_unit = models.CharField(max_length = 50, blank=True, null=True)

    class Meta:
        unique_together = ("sourcedata","environment","seedlot","trait")
        indexes = [models.Index(fields=["environment","trait","seedlot"])]
        
class FileWithUniversalNewLine(object):

    def __init__(self, file_obj):
        self.file = file_obj

    def lines(self):
        buff = ""  # In case of reading incomplete line, buff will temporarly keep the incomplete line
        while True:
            line = self.file.read(2048)
            if not line:
                if buff:
                    yield buff
                raise StopIteration

            # Convert all new lines into linux new line
            line = buff + line.replace("\r\n", "\n").replace("\r", "\n")
            lines = line.split("\n")
            buff = lines.pop()
            for sline in lines:
                yield sline

    def close(self):
        self.file.close()

    def __exit__(self, *args, **kwargs):
        return self.file.__exit__(*args, **kwargs)

    def __enter__(self, *args, **kwargs):
        return self

    def __iter__(self):
        return self.lines()

