#-*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.urls import path
from classification.views import classificationhome, classification, classe, classification_data,\
                                 classification_view, method, method_edit
                                 
urls_as_dict = {
        'classificationhome' : {'path':'home/', 'view':classificationhome, 'docpath': 'admin/structure.html'},
        'classification' : {'path':'classification/', 'view':classification, 'docpath': 'admin/structure.html#manage-structure-datasets'},
        'classe': {'path':'classe/', 'view':classe, 'docpath': 'admin/structure.html#manage-population-groups'},
        'cl_method': {'path':'method/', 'view':method,'docpath': 'admin/structure.html#structure-methods-management'},
        'cl_method_edit': {'path':'method/<int:object_id>/', 'view':method_edit,'docpath': 'admin/structure.html#structure-methods-management'},
        'classification_data': {'path':'data/', 'view':classification_data, 'docpath':'admin/structure.html#insert-structure-data'},
        'classification_view': {'path':'view/', 'view':classification_view,},
    }

urlpatterns = [path(url['path'], url['view'], name=namespace) for namespace, url in urls_as_dict.items()]

# urlpatterns = [
#     path('home/', classificationhome, name='classificationhome'),                       
#     path('classification/', classification, name='classification'),                       
#     path('classe/', classe, name='classe'),                       
#     path('method/', method, name='cl_method'),
#     path('method/<int:object_id>/', method_edit, name='cl_method_edit'),
#     path('data/', classification_data, name='classification_data'),                       
#     path('view/', classification_view, name='classification_view'),
# ]