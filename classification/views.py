"""LICENCE
    ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    LICENCE
"""

import ast
import csv
import json
import os

from _csv import Dialect
from unidecode import unidecode

from django.db.models import Q
from django.forms.models import model_to_dict
from django.db.models.query import QuerySet

from django.shortcuts import render, redirect
from django.db import models, IntegrityError, transaction
from django.http import HttpResponse,HttpResponseServerError, Http404
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.conf import settings

from accession.models import AccessionType, AccessionTypeAttribute, AccessionAttributePosition, Accession
from accession.forms import AccessionTypeForm, AccessionTypeAttForm, AccessionDataForm
from team.models import Project
from commonfct.genericviews import generic_management, get_fields
from commonfct.utils import recode, add_pagination
from commonfct.constants import ACCEPTED_TRUE_VALUES

from team.models import User, Method
from lot.models import Seedlot
from classification.models import Classification, Classe, ClasseValue
from classification.forms import ClassificationForm, ClasseForm, ClasseValueForm, UploadClasseValueFileForm, UploadFileForm
from csv_io.views import CSVFactoryForTempFiles
from team.forms import MethodForm
from phenotyping.forms import EnvironmentForm
from team.views import write_csv_search_result
from phenotyping.views import _get_data, _create_phenodata_from_file, create_method_from_file, update_method_from_file,\
                              update_method_from_form
from phenotyping.views import refine_search as refine_search2

from team.models import Method, Person
from accession.forms import UploadFileForm as UploadFileForm2, UploadFileFormWithoutDelimiter

from classification.forms import *
from dataview.forms import ClasseViewerForm, ClassificationViewerForm, SeedlotViewerForm, DataviewClassificationVisuForm
from commonfct.genericviews import return_csv

#============================ VIEWS =======================#


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def classificationhome(request):
    """
        Classification home.
    """
    return render(request,'classification/classification_base.html',{"admin":True})


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def classification(request):
    """
        View to manage classifications
        
        :var string template: template for classification
        :var form form: form to create classification
        :var list list_attributes: list of classifications's attributes
    """
    template = 'classification/classification_generic.html'
    form = ClassificationForm()
    list_attributes=['name', 'date', 'reference', 'method', 'person', 'projects']
    model = Classification
    classifications, nb_per_page = _get_data(request, model)
    list_exceptions = ['id', 'classe']
    title = "Structure Management"
    filetype = 'classification'
    typeid = 0
    
    if request.method == "GET":
        number_all = len(classifications)
        nb_per_page = 10
        names, labels = get_fields(form)
        tag_fields = {'all':classifications,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': title,
                      'admin':True}
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            upload_form = UploadFileForm()
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':classifications,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'list_attributes':list_attributes,
                               'upload_form':upload_form})
    
            return render(request,template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
                    
            classifications = Classification.objects.refine_search(or_and, dico_get)
            number_all = len(classifications)
            download_not_empty = False
            if classifications:
                download_not_empty = write_csv_search_result(classifications, list_exceptions)
            tag_fields.update({"download_not_empty":download_not_empty})
            
        upload_form = UploadFileForm()
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':classifications,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           'list_attributes':list_attributes,
                           'filetype':filetype,
                           'excel_empty_file':True,
                           'typeid':typeid,
                           'upload_form':upload_form}) 
    
        return render(request, template, tag_fields)  
    
    #CSV add (if add_csv exists)
    try:
        all = Classification.objects.all()
        names, labels = get_fields(form)
        return insert_csv_data(request, template, labels, model, form, all, names)
            
    except:
        print("EXCEPT")

    #Add a classification
    return generic_management(Classification, ClassificationForm, request, template,{'list_attributes':list_attributes,'admin':True,},"Structure Management")


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def classe(request):
    """
        View to manage classes
        
        :var string template: template for groups
        :var form form: form to create groups
        :var list list_attributes: list of group's attributes
    """
    template = 'classification/classification_generic.html'
    list_attributes=['name', 'description', 'classification']
    form = ClasseForm()
    model = Classe
    classes, nb_per_page = _get_data(request, model)
    list_exceptions = ['id', 'classevalue']
    title = "Group Management"
    filetype = 'classe'
    typeid = 0
    
    #Research & first page
    if request.method == "GET":
        number_all = len(classes)
        nb_per_page = 10
        names, labels = get_fields(form)
        tag_fields = {'all':classes,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': title,
                      'admin':True}
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            upload_form = UploadFileForm()
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':classes,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'list_attributes':list_attributes,
                               'upload_form':upload_form})
    
            return render(request,template,tag_fields)
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
                    
            classes = Classe.objects.refine_search(or_and, dico_get)
            print(dico_get)
            print(classes)
            number_all = len(classes)
            download_not_empty = False
            if classes:
                download_not_empty = write_csv_search_result(classes, list_exceptions)
            tag_fields.update({"download_not_empty":download_not_empty})
            
        upload_form = UploadFileForm()
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':classes,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           'list_attributes':list_attributes,
                           'filetype':filetype,
                           'excel_empty_file':True,
                           'typeid':typeid,
                           'upload_form':upload_form}) 
    
        return render(request, template, tag_fields)
    
    #CSV add (if add_csv exists)
    try:
        all = Classe.objects.all()
        names, labels = get_fields(form)
        return insert_csv_data(request, template, labels, model, form, all, names)
            
    except:
        print("EXCEPT")
    
    #Manual add
    return generic_management(Classe, ClasseForm, request, template,{"list_attributes":list_attributes,"admin":True,},"Group Management")          


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def method(request):
    """
      View to manage classifications methods.
    """
    form_class = MethodForm
    file_form_class = UploadFileFormWithoutDelimiter
    template = 'classification/classification_method.html'
    title = "Method Management"
    method_type = 2 #Classification
    list_attributes=['name', 'description']
    list_exceptions = ['id','phenotypicvalue','classification','type']
    
    if request.method == "GET":
        form = form_class()
        form_file = file_form_class()
        methods = Method.objects.filter(type=method_type)
        number_all = len(methods)
        methods = add_pagination(request, methods)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        
        tag_fields = {'all':methods,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': title, 
                      'filetype':'method',
                      'typeid':method_type,
                      'excel_empty_file':True,
                      'admin':True,
                      'template':template}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':methods,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'formfile':form_file,
                               'list_attributes':list_attributes,
                               'list_exceptions':list_exceptions})
 
            return render(request,template,tag_fields)

        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            methods = Method.objects.refine_search(or_and, dico_get, method_type)

            number_all = len(methods)
            methods = add_pagination(request, methods)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':methods,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':True,
                               'form':form,
                               'formfile':form_file,
                               'list_attributes':list_attributes,
                               'list_exceptions':list_exceptions})
 
            return render(request,template,tag_fields)

        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
             
            methods = Method.objects.refine_search(or_and, dico_get, method_type)

            number_all = len(methods)
            download_not_empty = False
            if methods:
                download_not_empty = write_csv_search_result(methods, list_exceptions)#, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            methods = add_pagination(request, methods)
        
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':methods,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           'formfile':form_file,
                           'list_attributes':list_attributes,
                           'list_exceptions':list_exceptions})  
        return render(request, template, tag_fields)
        
        
    elif request.method == "POST":
        form = form_class(request.POST)
        form_file = file_form_class(request.POST, request.FILES)
        
        methods = Method.objects.filter(type=method_type)
        number_all = len(methods)
        methods = add_pagination(request, methods)
        names, labels = get_fields(form)
        
        tag_fields = {'all':methods,
                      'number_all':number_all,
                      'fields_name':names,
                      'fields_label':labels,
                      'list_attributes':list_attributes,
                      'title': title,
                      'creation_mode':True,
                      'form':form,
                      'formfile':form_file,
                      'filetype':'method',
                      'typeid':method_type,
                      'excel_empty_file':True,
                      'admin':True,
                      'template':template}
        
        if form_file.is_valid():
            file = request.FILES['file']
            myfactory = CSVFactoryForTempFiles(file)
            myfactory.find_dialect()
            myfactory.read_file()
            file_dict = myfactory.get_file()
            
            headers_list = ['Name','Description']
            
            missing = myfactory.check_headers_conformity(headers_list)
            
            if missing != []:
                messages.add_message(request, messages.ERROR, "One or several mandatory headers are missing in your file : "+format(', '.join(missing))+\
                                     ". The required headers for this file are: "+format(', '.join(headers_list))+".")
                return redirect('cl_method')
            
            if request.POST.get('submit'):
                create_method_from_file(request, file_dict, method_type)
                
            elif request.POST.get('update'):
                update_method_from_file(request, file_dict, method_type)
            
            methods = Method.objects.filter(type=method_type)
            number_all = len(methods)
            methods = add_pagination(request, methods)
            
            tag_fields.update({'all':methods,
                               'number_all':number_all})
            
            return render(request,template,tag_fields)
            
        else:
            return generic_management(Method, MethodForm, request, template,dict(tag_fields, **{"method_type":method_type}),"Method Management")
        
    
    else:
        print("An error occurred.")
        return redirect('cl_method')


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def method_edit(request, object_id):
    """
      View to manage the edit of classification methods.
    """
    form_class = MethodForm
    file_form_class = UploadFileFormWithoutDelimiter
    template = 'classification/classification_method.html'
    title = "Method Management"
    method_type = 2 #Classification
    list_attributes=['name', 'description']
    list_exceptions = ['id','phenotypicvalue','classification','type']

    if request.method == "GET":
        try:
            m = Method.objects.get(id=object_id)
        except:
            messages.add_message(request, messages.ERROR, "This method doesn't exist in the database.")
            return redirect('cl_method')
        
        form = form_class(initial={'name':m.name,
                                   'description':m.description,
                                   'method_file':m.method_file
                                   })
        form_file = file_form_class()
        
        methods = Method.objects.filter(type=method_type)
        number_all = len(methods)
        methods = add_pagination(request, methods)
        nb_per_page = 50 if 'nb_per_page' not in request.GET.keys() else request.GET['nb_per_page']
        names, labels = get_fields(form)
        
        tag_fields = {'all':methods,
                      'hiddenid':m.id,
                      'fields_name':names,
                      'fields_label':labels,
                      'title': title, 
                      'filetype':'method',
                      'typeid':method_type,
                      'excel_empty_file':True,
                      'admin':True,
                      'template':template}
        
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({'search_result':dico_get,
                               'or_and':or_and,
                               'all':methods,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'formfile':form_file,
                               'list_attributes':list_attributes,
                               'list_exceptions':list_exceptions})
 
            return render(request,template,tag_fields)

        elif "in_search" in request.GET:
            dico_get = ast.literal_eval(request.GET["in_search"])
            or_and = request.GET["condition"]
            
            methods = Method.objects.refine_search(or_and, dico_get, method_type)

            number_all = len(methods)
            methods = add_pagination(request, methods)
            download_not_empty = True
            
            tag_fields.update({'search_result':dico_get,
                               'download_not_empty':download_not_empty,
                               'or_and':or_and,
                               'all':methods,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'creation_mode':False,
                               'form':form,
                               'formfile':form_file,
                               'list_attributes':list_attributes,
                               'list_exceptions':list_exceptions})
 
            return render(request,template,tag_fields)

        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
             
            methods = Method.objects.refine_search(or_and, dico_get, method_type)

            number_all = len(methods)
            download_not_empty = False
            if methods:
                download_not_empty = write_csv_search_result(methods, list_exceptions)#, "Result search")
            tag_fields.update({'download_not_empty':download_not_empty})
            
            methods = add_pagination(request, methods)
        
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'all':methods,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':False,
                           'form':form,
                           'formfile':form_file,
                           'list_attributes':list_attributes,
                           'list_exceptions':list_exceptions})  
        return render(request, template, tag_fields)

    elif request.method == "POST":
        update_method_from_form(request, form_class)
        
        return redirect('cl_method')


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def classification_data(request):
    """
        View to add structure's data
        
        :var list classe_values: all the classification values of the database (ClasseValue)
        :var form upload_form: form to add ClasseValue
        :var string template: template for classification data
        :var int count: number of ClasseValue in the database
    """
    classe_values = ClasseValue.objects.all()
    upload_form = UploadClasseValueFileForm()
    template = 'classification/classification_data.html'

    if request.method == "GET":
        count = ClasseValue.objects.count()
        if count <1:
            count = 'no classification data in database'

    if request.method == "POST":
        upload_form = UploadClasseValueFileForm(request.POST, request.FILES)
        if upload_form.is_valid():
            return get_classif_data(request, template, upload_form)
        
    return render(request, template, {"admin":True,'upload_form':upload_form, 'classe_values': classe_values, 'count': count})


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def classification_view(request):
    """
      View to display structure's data in admin menu.
      
      :var string template: template for dataview
      :var int n_page: page / step of the search in the dataview
      :var list select_classification: classification selected in the first step
      :var list classe_values: all the values in the classification
      :var Seedlot sl: seedlot in the classification
      :var int export_or_web: boolean to have the type of visualization
      :var int show_accession: boolean to show accession in the table
    """
    template = "classification/classification_view.html"
    filename = 'view_classif'
    
    if request.method == 'GET':
        formclassification = ClassificationViewerForm(projects=Project.objects.all())
        classifications = Classification.objects.all().order_by('name')
        formclassification.fields['classification'].queryset=classifications

        return render(request,template,{'admin':True, 'n_page':1, 'formclassification':formclassification})

    elif request.method == "POST":
        
        #Get the actual page number
        n_page = int(request.POST['n_page'])
        
        #Choose Seedlots & groups
        if n_page == 2:
            select_classification = request.POST.get('classification')
            
            visumodeform = DataviewClassificationVisuForm()             
            formclasse = ClasseViewerForm()
            formseedlot = SeedlotViewerForm()
            
            classe_values = ClasseValue.objects.filter(classe__in=Classe.objects.filter(classification=select_classification).distinct())
            names = classe_values.values_list('seedlot__name', flat=True)

            formseedlot.fields["seedlot"].queryset = Seedlot.objects.filter(name__in=list(set(names))).order_by('name')
            formclasse.fields["group"].queryset = Classe.objects.filter(classification=select_classification).distinct().order_by('name')

            return render(request,template,{'admin':True, 
                                            'n_page':n_page, 
                                            'formclasse':formclasse, 
                                            'formseedlot':formseedlot, 
                                            'visumodeform': visumodeform, 
                                            'classification':select_classification})


        #Results
        if n_page == 3:
            
            classification_id = request.POST.get('classification') #ID de la classification
            classe = request.POST.getlist('classe')
            seedlot = request.POST.getlist('seedlot')
            export_or_web = request.POST.get('export_or_web')
            show_accession = request.POST.get('show_accession')
            
            classification = Classification.objects.get(id=classification_id)
            
            formclasse = ClasseViewerForm(request.POST)
            formseedlot = SeedlotViewerForm(request.POST)
            
            if formclasse.is_valid() and formseedlot.is_valid():
                select_classe = formclasse.cleaned_data['group']
                select_seedlot = formseedlot.cleaned_data['seedlot']
                
            else:
                visumodeform = DataviewClassificationVisuForm(request.POST)
                return render(request,template,{'admin':True, 
                                                'n_page':2, 
                                                'formclasse':formclasse, 
                                                'formseedlot':formseedlot,
                                                'visumodeform': visumodeform, 
                                                'classification':classification_id})
            
            fields_label = ["Seedlot"]
            
            if show_accession == "1":
                fields_label.append("Accession")
            for cl in select_classe:
                fields_label.append(cl.name)
            
            #Récupération des données
            values = []
            
            for seedlot in select_seedlot:
                line = [seedlot]
                if show_accession == "1":
                    line.append(seedlot.accession.name)
                for cl in select_classe:
                    cv = ClasseValue.objects.get(seedlot=seedlot, classe=cl)
                    line.append(cv.value)
                
                values.append(line)
            
            write_structure_csv_from_viewer(filename, fields_label, values)
            
            if export_or_web == "1":
                return return_csv(request, filename)
            
            else:
                tag_fields = {'admin':True,
                              'n_page':n_page, 
                              'show_accession': show_accession,
                              'values':values,
                              'filename':filename,
                              'fields_label':fields_label,
                              'export_or_web': export_or_web,
                              'list_seedlot':select_seedlot,
                              'show_accession':show_accession,
                              'classification':classification}
            
                return render(request,template,tag_fields)


def write_structure_csv_from_viewer(filename, fields_label, values):
    """
      View to write structure's data in a csv file.
      
      :param string filename: name of the file
      :param list fields_label: Columns' labels
      :param list values: structure values to print in the file
    """
    dialect = Dialect(delimiter = ';')
    
    with open(settings.TEMP_FOLDER+filename+".csv", 'w', encoding='utf-8') as csv_file:
        writer = csv.writer(csv_file, dialect)
        writer.writerow(fields_label)
        
        for row in values:
            writer.writerow(row)


#=========================== CLASSIFICATION DATA FUNCTIONS ==================#

def get_classif_data(request, template, upload_form):
    """
    Read a CSV file containing structure's data, and create data in the database 
    (only if seedlots & classes in the file already exist).
    
    :param string template: template for add classification's data
    :param form upload_form: form to associate data with a classification
    
    :var file file: CSV file containing structure's data
    :var dict dict_line: lines of the file
    :var dict dict_data: values for each seedlot/groups
    :var int errors: number of error when data are added
    :var int add_count: number of data added
    """
    
    #Get the file and the data
    file = request.FILES['main_file']

    #Get the selected classification
    if upload_form.is_valid():
        select_classification = upload_form.cleaned_data['classification']
            
    #Check the csv file
    filename = file.name
    filtetype = filename.split ('.')
    extension = filtetype[1]
    
    if not extension == "csv":
        error_message = 'Please choose a CSV file.'
        return render(request, template, {"admin":True,'upload_form':upload_form, 'error_message': error_message})
    
    #Get the line in a dictionary (0: line1, 1: line2...)
    dictline = {}
    i = 0
    for line in file:
        dictline[i] = line.strip()
        dictline[i] = recode(dictline[i])   #Conversion
        i+=1
         
    #Dictionary contains {lot1 : {gp1 : val, gp2 : val}, lot2 : {gp1 : val, gp2 : val}...}
    dict_data = {}    
    groups = dictline[0]    #First line of the file = groups
    
    if ';' in groups:
        delim = ';'
    elif ',' in groups:
        delim = ','
    else:
        error_message = 'Please use a correct delimiter (";" or ",").'
        return render(request, template, {"admin":True,'upload_form':upload_form, 'error_message': error_message})
    
    #List of groups (classes)
    groups = groups.split(delim)
           
    #Manage the other lines (seedlot with values)
    for line in dictline.keys():
        if line != 0:
            line = dictline[line]
            line = line.split(delim)
                                
            dict_data[line[0]] = {}
            seedlot = dict_data[line[0]]
            
            #Add groups as keys with their values
            i=1
            for group in groups[1:len(groups)]:
                seedlot[group] = line[i]
                i+=1       
        
    #Check is seedlots & groups are in the DB.
    errors = check_classes_seedlots(dict_data, groups, select_classification)
    
    #If errors, show the errors's type
    if(len(errors) > 0):
        return render(request, template, {"admin":True,'upload_form':upload_form, 'error_message_seedlot': errors[1], 'error_message_groups': errors[0]})
    
    #Now, datas are in the dictionnary
    #Seedlots & Classes OK
    #We add classe_values in the database
    
    """
    GESTION DES DOUBLONS / SUPPRESSIONS / ECRASEMENT, d'un CSV
    """
    
    add_count = add_classification_values(dict_data, select_classification, request, template, upload_form)
        
    return render(request, template, {"admin":True,'upload_form':upload_form,'add_count':add_count,'select_classification':select_classification})


def add_classification_values(dict_data, select_classification, request, template, upload_form):
    """
    Add classification values in the database and return the number of values added.
    
    :param dict dict_data: contains classification data from the CSV file
    :param list select_classification: selected classification
    :param string template: template
    :param form upload_form: form with structure and CSV file with data
    
    :var int add_count: nmber of values added in the database
    :var float somme: sum of the values for 1 seedlot
    :var string error_message: error message if the total in a line isn't 1
    
    :returns:  int -- number of values added
    """

    add_count = 0
    for sl in dict_data.keys():
        for group in dict_data[sl].keys():
            value = dict_data[sl][group]
            seedlot = Seedlot.objects.get(name=sl)
            classe = Classe.objects.get(name=group, classification=Classification.objects.get(name=select_classification))
            
            try:
                cv = ClasseValue.objects.get(seedlot=seedlot, classe=classe)
                cv.value=value
                cv.save()
                add_count+=1
                
            except ClasseValue.DoesNotExist :
                cv = ClasseValue(value=value, seedlot=seedlot, classe=classe)
                cv.save()
                add_count+=1
        
        """
        Retravailler la vérification d'une ligne pour 1 seedlot.
        Pb : add_count prend le render alors que le render devrait s'afficher directement.
        
        #Check one seedlot = 1
        #Chercher une methode differente
        if somme > 1.01 or somme < 0.99:
            error_message = "Sum of : " + sl + " =/= 1."
            return render(request, template, {"admin":True,'upload_form':upload_form, 'error_message': error_message})
        """
        
    return add_count


def check_classes_seedlots(dict_data, groups, select_classification):
    """
    Check if Seedlots & Groups are in the database
    
    :param dict dict_data: keys = seedlots in the file
    :param list groups: groups in the file
    :param list select_classification: selected classification
    
    :var list wrong_groups: missing groups in the database
    :var list wrong_seedlots: missing seedlots in the database
    :var list error_message_groups: all the errors for groups
    :var list error_message_seedlot: all the errors for seedlots
    
    :returns: list -- error messages for groups/seedlots which doesn't exist in the database
    """

    #Check datas exist in the DB
    #Check the groups in the database
    wrong_groups = []
    for group in groups[1:len(groups)]:                
        try:
            Classe.objects.get(name=group, classification=select_classification)
        except Classe.DoesNotExist :
            wrong_groups.append(group)
            
    error_message_groups = ''
    if(len(wrong_groups) > 0):
        error_message_groups = 'Classe(s) " '
        for w_group in wrong_groups:
            error_message_groups += (w_group + ' ')
        error_message_groups += (' " not in the classification " ' + select_classification.name + ' ".')

    #Check the seedlots exist
    wrong_seedlots = []
    for seedlot in dict_data.keys():
        try:
            Seedlot.objects.get(name=seedlot)
        except Seedlot.DoesNotExist :
            wrong_seedlots.append(seedlot)
    
    error_message_seedlot = ''
    if(len(wrong_seedlots) > 0):
        error_message_seedlot = 'Seedlot(s) " '
        for w_seedlot in wrong_seedlots:
            error_message_seedlot += (w_seedlot + ' ')
        error_message_seedlot += (' " not in the database.')

    #If error, return the error message with missing objects
    if len(error_message_groups)+len(error_message_seedlot) > 0:
        return [error_message_groups, error_message_seedlot]
    
    #0 Errors
    return []
    
#=========================== OTHERS FUNCTIONS =================================#   


#Classe & Classification CSV add
def insert_csv_data(request, template, labels, model, form, all, names):
    """
    Read a CSV file (like the header),
    and create data in the database 
    (for classe & classification)
    
    :param labels: names of the form's field
    :param model: model
    :param form: form
    :param all: all objects of the model
    :param names: names of the form's field
    """
    # TODO(melanie) use the CSV Factory to modify the parsing of csv file
    
    #Get the file and the data
    file = request.FILES['main_file']
    
    #Check the csv file
    filename = file.name
    filtetype = filename.split ('.')
    extension = filtetype[1]
    
    if not extension == "csv":
        csv_error = "Not a CSV file."
        return render(request, template,{'form':form, "admin":True, 'all':all, 'fields_label': labels, 'fields_name': names, 'csv_error':csv_error})
        
    dictline = {}
    
    i = 0
    for line in file:
        dictline[i] = line.strip()
        dictline[i] = recode(dictline[i])   #Conversion
        i+=1
    
    header = dictline[0]
    #Get the header of the CSV
    if '"' in header :
        header = header.replace('"','')
    
    if ',' in header:
        delim = ','
    elif ';' in header:
        delim = ';'
    else:
        csv_error =  "No \",\" or \";\" as separator."
        return render(request, template,{'form':form, "admin":True, 'all':all, 'fields_label': labels, 'fields_name': names, 'csv_error':csv_error})
    
    header = header.split(delim)
    
    if header != labels:
        print(labels)
        print(header)
        csv_error =  "Not the good header."
        return render(request, template,{'form':form, "admin":True, 'all':all, 'fields_label': labels, 'fields_name': names, 'csv_error':csv_error})

    add_count = 0
    for i in dictline.keys():
        if i != 0:
            if '"' in  dictline[i]:
                dictline[i] =  dictline[i].replace('"','')
                
            line = dictline[i].split(delim)
            
            #If we add Classe
            if model == Classe:
                #Check if the classe already exists
                try:
                    """Peut créer une fonction pour reporter tous les éléments deja existents, et demander si on ajoute quand meme les nouveaux"""                        
                    Classe.objects.get(name = line[0], classification = Classification.objects.get(name=line[2]))
                    csv_error =  "Some classe(s) already exists."
                    return render(request, template,{'form':form, "admin":True, 'all':all, 'fields_label': labels, 'fields_name': names, 'csv_error':csv_error})
                #We add the class
                except:
                    try:
                        new_classe = Classe(name = line[0], description = line[1], classification = Classification.objects.get(name=line[2]))
                        new_classe.save()
                        add_count += 1
                    except:
                        #Check the associate classification exists
                        csv_error =  "Assossiate Classification doesn't exist."
                        return render(request, template,{'form':form, "admin":True, 'all':all, 'fields_label': labels, 'fields_name': names, 'csv_error':csv_error})
                      
            #If we add Classification
            elif model == Classification:
                #Check if the classification already exists
                try:
                    """Peut créer une fonction pour reporter tous les éléments deja existents, et demander si on ajoute quand meme les nouveaux"""
                    Classification.objects.get(name = line[0])
                    csv_error =  "Some classification(s) already exists."
                    return render(request, template,{'form':form, "admin":True, 'all':all, 'fields_label': labels, 'fields_name': names, 'csv_error':csv_error})
                #We add the classification
                except:
                    Classification.objects.create_classification(line)
                    add_count += 1
                

    #If add is OK
    return render(request, template,{'form':form, "admin":True, 'all':all, 'fields_label': labels, 'fields_name': names, 'add_count': add_count})
