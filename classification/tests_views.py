"""LICENCE
    ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

LICENCE"""

from django.test import TestCase
from django.db import models
from django.apps import apps
from django.db.utils import IntegrityError

import datetime

from classification.forms import ClassificationForm, ClasseForm, ClasseValueForm


class ViewTest(TestCase):
    """ 
    Tests on classification module views
    """
    def setUp(self):

        User = apps.get_model('team','user')
        User.objects.create_superuser("admin", "admin", "Admin", "Admin")
        self.client.post('/', {'username': 'admin', 'password':'admin'})
        
    def test_url_bidon(self):
        response = self.client.get('/classification/ca/nexiste/pas/')
        self.assertEqual(response.status_code, 404)
        
    def test_get_home(self):
        response = self.client.get('/classification/home/')
        self.assertEqual(response.status_code, 200)
        
    def test_get_classification(self):
        response = self.client.get('/classification/classification/')
        self.assertEqual(response.status_code, 200)
    
    def test_get_classe(self):
        response = self.client.get('/classification/classe/')
        self.assertEqual(response.status_code, 200)
    
    def test_get_method(self):
        response = self.client.get('/classification/method/')
        self.assertEqual(response.status_code, 200)
        
    def test_get_data(self):
        response = self.client.get('/classification/data/')
        self.assertEqual(response.status_code, 200)
        
    def test_get_view(self):
        response = self.client.get('/classification/view/')
        self.assertEqual(response.status_code, 200)
    
    