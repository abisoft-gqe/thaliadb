# Generated by Django 4.2 on 2024-02-19 15:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('classification', '0005_merge_20220406_1400'),
    ]

    operations = [
        migrations.RenameIndex(
            model_name='classevalue',
            new_name='classificat_seedlot_272048_idx',
            old_fields=('seedlot', 'classe'),
        ),
    ]
