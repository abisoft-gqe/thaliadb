# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-01-07 15:11
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('classification', '0002_classification_description'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='classe',
            unique_together=set([('name', 'classification')]),
        ),
    ]
