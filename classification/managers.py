from django.db import models
from django.db.models import Q
from django.apps import apps

from team.models import User, Project
from operator import ior, iand

class ClassificationManager(models.Manager):
    """
    Manager for classification module.
    """
    
    def by_username(self,username):
        """
        Returns only the classifications from projects linked to this user 

        :param string username : username

        :var int username_id : user's id
        :var list projects : projects linked to the user
        """
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
        return self.filter(projects__in=projects).distinct()
    
    def create_classification(self, line):
        """
        Create a classification with a line containing all the informations.

        :param list line: classification's informations

        :var model Classification: classification's model
        :var model Method: method's model
        :var model Person: person's model
        :var model Project: project's model
        :var Classification new_classif: new classification
        """
        Classification = apps.get_model('classification', 'classification')
        Method = apps.get_model('team', 'method')
        Person = apps.get_model('team', 'person')
        Project = apps.get_model('team', 'project')
        
        name=line[0]
        date=line[1]
        reference=line[2]
        method = Method.objects.get(name=line[3])
        first_name = line[4].split(" ")[0]
        last_name = line[4].split(" ")[1]
        person = Person.objects.get(first_name=first_name, last_name=last_name)
        description = line[5]
        #If more than one project
        if "|" in line[6]:
            projects = Project.objects.filter(name__in=line[6].split("|"))
        else:
            projects = Project.objects.filter(name=line[6])
    
        #Classification creation               
        new_classif = Classification(name = name, date=date, reference = reference, method=method, person=person, description=description)
        new_classif.save()
        new_classif.projects = projects
        new_classif.save()
        
        
    def refine_search(self, or_and, dico_get):
        """
        Search in classification according to some parameters

        :param string or_and : link between parameters (or, and)
        :param dict dico_get : data to search for
        """
        
        Classification = apps.get_model('classification', 'classification')
        Person = apps.get_model('team', 'person')
        Method = apps.get_model('team', 'method')
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
        
        proj=[]
        query=Q()
        for dat, dico_type_data in dico_get.items():
            if "projects" in dico_type_data:
                if dico_type_data['projects']=='':
                    #print('Projects empty')
                    query = myoperator(query, Q(projects__isnull=True))
                else:
                    proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                    query = myoperator(query, Q(projects__in=proj))
            else:
                for data_type, value in dico_type_data.items():
                    var_search = str( data_type + "__icontains" )
                    search_value = dico_type_data[data_type]
                    data_type = data_type[0].lower()+data_type[1:]
                    
                    data_filter = ""
                    if data_type == "person":
                        var_search = str(data_type+"__in")
                        data_filter = Person.objects.filter(Q(first_name__icontains=value)|Q(last_name__icontains=value))
                    elif data_type == "method":
                        var_search = str(data_type+"__in")
                        data_filter = Method.objects.filter(name__icontains=value)

                    if data_filter:
                        data_id_list = []
                        for i in data_filter:
                            data_id_list.append(i.id)
                            search_value = data_id_list
                    query |= Q(**{var_search : search_value})
                    query = myoperator(query, Q(**{var_search : search_value}))
        
        return Classification.objects.filter(query).distinct()

class ClasseManager(models.Manager):
    
    def refine_search(self, or_and, dico_get):
        """
        Search in classification according to some parameters

        :param string or_and : link between parameters (or, and)
        :param dict dico_get : data to search for
        """
        
        Classe = apps.get_model('classification', 'classe')
        Classification = apps.get_model('classification', 'classification')
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
        
        proj=[]
        query=Q()
        for dat, dico_type_data in dico_get.items():
            if "projects" in dico_type_data:
                if dico_type_data['projects']=='':
                    #print('Projects empty')
                    query = myoperator(query, Q(projects__isnull=True))
                else:
                    proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                    query = myoperator(query, Q(projects__in=proj))
            else:
                for data_type, value in dico_type_data.items():
                    var_search = str( data_type + "__icontains" )
                    search_value = dico_type_data[data_type]
                    data_type = data_type[0].lower()+data_type[1:]
                    
                    data_filter = ""
                    if data_type == "classification":
                        var_search = str(data_type+"__in")
                        data_filter = Classification.objects.filter(name__icontains=value)

                    if data_filter:
                        data_id_list = []
                        for i in data_filter:
                            data_id_list.append(i.id)
                            search_value = data_id_list
                    query |= Q(**{var_search : search_value})
                    query = myoperator(query, Q(**{var_search : search_value}))
        
        return Classe.objects.filter(query).distinct()
    


