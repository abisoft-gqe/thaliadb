# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Elise Peluso

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.db import models

from django.utils.encoding import force_bytes
from lot.models import Seedlot
from accession.models import Accession
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.


REPRODUCTION_TYPES = ((1,'Multiplication method'),
                      (2,'Accession Hybrid Crossing method'),
                      (3,'Seedlot Hybrid Crossing method'),
                      (4,'Other Accession Pedigree'),
                      (5,'Selfing')
                      )

class Reproduction_method(models.Model):
    name = models.CharField(max_length=100,unique=True)
    category = models.IntegerField(choices=REPRODUCTION_TYPES, default=1)
    description = models.CharField(max_length=500,blank=True,null=True)
    def __unicode__(self):
        return u"%s"%self.name
    def __str__(self):
        return self.name
     
    #for BrAPI
    def getBlankValue(self):
        return ''
    
    def getBreedingMethodDescription(self):
        return {self.category : self.description}


class Reproduction(models.Model):
    reproduction_method = models.ForeignKey('genealogy_managers.Reproduction_method', blank=True, null=True, on_delete=models.CASCADE)
    entity_type = models.IntegerField(validators = [MinValueValidator(0),MaxValueValidator(1)])
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    description = models.CharField(max_length=200, blank=True, null=True)
    
    def __str__(self):
        s = "["
        if self.entity_type == 0 : ## 0 ==> Accession
            if self.accessionrelation_set.all() :
                s += "{0}".format(self.accessionrelation_set.all().first())
                for r in self.accessionrelation_set.all()[1:] :
                    s += ", {0}".format(r)
            s += ']'
            return "{0}-{1} {2}".format(self.start_date, self.end_date, s)
        else : ## 1 ==> Seedlot
            if self.seedlotrelation_set.all() :
                s += "{0}".format(self.seedlotrelation_set.all().first())
                for r in self.seedlotrelation_set.all()[1:] :
                    s += ", {0}".format(r)
            s += ']'
            return "{0}-{1} {2}".format(self.start_date, self.end_date, s)
    
    def delete(self,*args, **kwargs):
        if self.entity_type == 0 :
            self.accessionrelation_set.all().delete()
        else :
            self.seedlotrelation_set.all().delete()
        super(Reproduction, self).delete(*args, **kwargs)
    
    #for BrAPI    
    def getBlankValue(self):
        return ''
    
    def getNullValue(self):
        return {}
    
    def getCrossesAdditionalInfo(self):
        crossesAdditionalInfo = {}
        if self.reproduction_method.name :
            crossesAdditionalInfo['reproductionMethod'] = self.reproduction_method.name
        if self.entity_type :
            crossesAdditionalInfo['entityType'] = str(self.entity_type)    
        if self.start_date :
            crossesAdditionalInfo['startDate'] = str(self.start_date)
        if self.end_date :
            crossesAdditionalInfo['endDate'] = str(self.end_date)
        if self.description :
            crossesAdditionalInfo['description'] = self.description    
        return crossesAdditionalInfo
    
    def getCrossType(self):
        if self.reproduction_method.category == 1 :
            crossType = None
        elif self.reproduction_method.category == 2 :
            crossType = 'BIPARENTAL'  
        elif self.reproduction_method.category == 3 :
            crossType = 'BIPARENTAL'
        elif self.reproduction_method.category == 4 :
            crossType = None 
        elif self.reproduction_method.category == 5 :
            crossType = 'SELF' 
        else : 
            crossType = None
        return crossType   
        
    def getExternalReferences(self):
        return [{'referenceId' : '','referenceSource': ''}]
    
    def getParent1(self):
        parent_relation = self.accessionrelation_set.get(parent_gender = 'M')
        return {'germplasmDbId' : str(parent_relation.parent.id), 'germplasmName' : parent_relation.parent.name, 'observationUnitDbId' : '', 'observationUnitName' : '', 'parentType' : 'MALE'}
    
    def getParent2(self):
        parent_relation = self.accessionrelation_set.get(parent_gender = 'F')
        return {'germplasmDbId' : str(parent_relation.parent.id), 'germplasmName' : parent_relation.parent.name, 'observationUnitDbId' : '', 'observationUnitName' : '', 'parentType' : 'FEMALE'}
    
    