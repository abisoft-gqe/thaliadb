#-*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.urls import path
from genealogy_accession.views import Accessionhybridcross_management, Otheraccessionpedigree_management, selfing_management
from genealogy_managers.views import genealogyhome, Method_management, genealogy_manager
from genealogy_seedlot.views import Seedlothybridcross_management, Multiplication_management

urls_as_dict = {
    'accessionhybridcross': {'path':'accessionhybridcross/', 'view':Accessionhybridcross_management, 'docpath': 'admin/genealogy.html#accession-hybrid-cross-other-accession-pedigree' },
    'otheraccessionpedigree': {'path':'otheraccessionpedigree/', 'view':Otheraccessionpedigree_management, 'docpath': 'admin/genealogy.html#accession-hybrid-cross-other-accession-pedigree' },
    'seedlothybridcross': {'path':'seedlothybridcross/', 'view':Seedlothybridcross_management, 'docpath':'admin/genealogy.html#seedlot-hybrid-cross'},
    'multiplication': {'path':'multiplication/', 'view':Multiplication_management, 'docpath':'admin/genealogy.html#multiplication' },
    'selfing': {'path':'selfing/', 'view':selfing_management, 'docpath':'admin/genealogy.html#selfing' },
    'genealogyhome': {'path':'home/', 'view':genealogyhome, 'docpath':'admin/genealogy.html'},
    'genealogy-method': {'path':'method/', 'view':Method_management, 'docpath':'admin/genealogy.html#breeding-methods-management' },
    'genealogy_manager': {'path':'manager/<int:pk>/', 'view':genealogy_manager, 'docpath':'admin/genealogy.html'}
    }

urlpatterns = [path(url['path'], url['view'], name=namespace) for namespace, url in urls_as_dict.items()]

# urlpatterns = [
#     path('accessionhybridcross/', Accessionhybridcross_management, name='accessionhybridcross'),
#     path('otheraccessionpedigree/', Otheraccessionpedigree_management, name='otheraccessionpedigree'),
#     path('seedlothybridcross/', Seedlothybridcross_management, name='seedlothybridcross'),
#     path('multiplication/', Multiplication_management, name='multiplication'),
#     path('selfing/', selfing_management, name='selfing'),
#     path('home/', genealogyhome, name='genealogyhome'),
#     path('method/', Method_management, name='genealogy-method'),
#     path('manager/<int:pk>/', genealogy_manager, name='genealogy_manager'),
# ]