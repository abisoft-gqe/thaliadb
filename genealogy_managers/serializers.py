from rest_framework import serializers

from genealogy_managers.models import Reproduction, Reproduction_method

class CrossesBrAPISerializer(serializers.ModelSerializer):
    """
        CrossesBrAPISerializer gives the informations of cross entities
    """
    additionalInfo = serializers.JSONField(source = 'getCrossesAdditionalInfo') 
    crossAttributes = serializers.ListField(source = 'getBlankValue')
    crossDbId = serializers.CharField(source = 'id')  
    crossName = serializers.CharField(source = 'getBlankValue')  
    crossType = serializers.CharField(source = 'getCrossType') 
    crossingProjectDbId = serializers.CharField(source = 'getBlankValue')  
    crossingProjectName = serializers.CharField(source = 'getBlankValue')  
    externalReferences = serializers.ListField(source = 'getExternalReferences')  
    parent1 = serializers.JSONField(source = 'getParent1')  
    parent2 = serializers.JSONField(source = 'getParent2')  
    plannedCrossDbId = serializers.CharField(source = 'getBlankValue')  
    plannedCrossName = serializers.CharField(source = 'getBlankValue')
    pollinationEvents = serializers.ListField(source = 'getBlankValue')  
    
    class Meta:
        model = Reproduction
        fields = [
            'additionalInfo',
            'crossAttributes',
            'crossDbId',
            'crossName',
            'crossType',
            'crossingProjectDbId',
            'crossingProjectName',
            'externalReferences',
            'parent1',
            'parent2',
            'plannedCrossDbId',
            'plannedCrossName',
            'pollinationEvents',
            ]   
        depth = 1
        
        
class BreedingMethodsBrAPISerializer(serializers.ModelSerializer):
    """
        BreedingMethodsBrAPISerializer gives the informations of germplasm breeding methods available in a system
    """
    abbreviation = serializers.CharField(source = 'getBlankValue')
    breedingMethodDbId = serializers.CharField(source = 'id') 
    breedingMethodName = serializers.CharField(source = 'name') 
    description = serializers.CharField(source = 'getBreedingMethodDescription') 
    
    class Meta:
        model = Reproduction_method
        fields = [
                  'abbreviation',
                  'breedingMethodDbId',
                  'breedingMethodName',
                  'description',
                  ] 
        depth = 1 