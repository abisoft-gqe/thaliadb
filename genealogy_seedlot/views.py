"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import absolute_import

import ast ##convertir des strings en dict
import csv
import json

from datetime import datetime
from itertools import product

from django import forms
from django.http import HttpResponse
from django.http.response import Http404
from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.utils.datastructures import MultiValueDictKeyError
from django.db.transaction import TransactionManagementError
from django.db.models import Q as Query_dj
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db import models, IntegrityError, transaction
from django.conf import settings
from django.contrib import messages

from csv_io.views import CSVFactoryForTempFiles
from commonfct.constants import filerenderer_headers
from genealogy_managers.models import Reproduction, Reproduction_method
from genealogy_seedlot.models import SeedlotRelation
from genealogy_accession.models import AccessionRelation
from genealogy_managers.forms import MethodForm
from genealogy_accession.forms import AccessionHybridCrossForm
from genealogy_seedlot.forms import SeedlotHybridCrossForm, MultiplicationForm
from commonfct.utils import get_fields
from lot.models import Seedlot
from accession.models import Accession
from accession.forms import UploadFileForm, UploadFileFormWithoutDelimiter
from commonfct.utils import recode
from phenotyping.views import write_csv_search_result

@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def Seedlothybridcross_management(request):
    seedlothybridform = SeedlotHybridCrossForm()
    template = "genealogy/hybridcross.html"
    names, labels = get_fields(seedlothybridform) 
    title = "Seedlot Hybrid Cross Management"
    formf = UploadFileFormWithoutDelimiter()
    filetype = "seedlothybridcross"
    typeid = 0
    errormsg = False
    type_method = 3
        
    ##type method = 3 => sl crossing method
    model_relation = "SeedlotRelationCross"
    modelname = SeedlotRelation
    data_or_null = 0
    list_attributes=['hybrid seedlot', 'parent male', 'parent female', "method", "site", "start date", "end date", "comments"]
    list_exceptions = []
    dico = fill_dico(type_method, model_relation, modelname, data_or_null)
    classname = "SeedlotRelation"
    tag_fields = {'title':title,
              'fields_name':names,
              'fields_label':labels,
              "admin":True,
              "form":seedlothybridform,
              "formfile":formf,
              'excel_empty_file':True,
              "creation_mode":True,
              'nb_per_page':50,
              'all':dico.items,
              "filetype":filetype,
              "typeid":typeid,
              "classname":"SeedlotRelation",
              'refine_search':True,
              'list_attributes':list_attributes,
              }
    if len(dico)==0:
        tag_fields.update({'all':False})
    if request.method == "POST":
        seedlothybridform = SeedlotHybridCrossForm(request.POST)
        formf = UploadFileFormWithoutDelimiter(request.POST, request.FILES) 
        if seedlothybridform.is_valid():
            try:
                with transaction.atomic():
                    repro_id, child, parent_male, parent_female, repro_method, site, start_date, end_date, comments = seedlothybridform.create_hybrid_cross()
        
                    dico[repro_id] = [child, parent_male, parent_female, repro_method, site, start_date, end_date, comments]
                    tag_fields.update({"all":dico.items,
                                       "hybridform":seedlothybridform,
                                       })
                    return render(request, template, tag_fields)
                
            except ValidationError as e:
                errormsg=e.message
                
        elif formf.is_valid():
            return _create_genealogydata_from_file(request, template, tag_fields, model_relation, modelname, type_method)
        
        else:
            errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in seedlothybridform.errors.items() ])
#     return render(request, template, tag_fields)
    return refine_search(request, seedlothybridform, formf, dico, template, list_attributes, list_exceptions, title, modelname, filetype, typeid, errormsg, model_relation, type_method, classname)


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def Multiplication_management(request):
    multiplicationform = MultiplicationForm()
    template = "genealogy/hybridcross.html"

    names, labels = get_fields(multiplicationform) 
    title = "Seedlot Multiplication Management"
    
    formf = UploadFileFormWithoutDelimiter()
    filetype = "seedlotmultiplication"
    typeid = 0
    errormsg = False
    type_method = 1 ##type method = 1 => multiplication method
    model_relation = "SeedlotRelationMultiplication"
    modelname = SeedlotRelation
    dico = {}
    dico_interm = {}
    data_or_null = 0
    list_attributes=['seedlot', 'parent(s)', "method", "site", "start date", "end date", "comments"]
    list_exceptions = []
    dico = fill_dico(type_method, model_relation, modelname, data_or_null)
    classname = "SeedlotRelation"
    tag_fields = {'title':title,
              'fields_name':names,
              'fields_label':labels,
              "admin":True,
              "form":multiplicationform,
              "formfile":formf,
              'excel_empty_file':True,
              "creation_mode":True,
              'nb_per_page':50,
              'all':dico.items,
              "filetype":filetype,
              "typeid":typeid,
              "classname":"SeedlotRelation"
              }
    if len(dico)==0:
        tag_fields.update({'all':False})
    if request.method == "POST":
        multiplicationform = MultiplicationForm(request.POST)
        formf = UploadFileFormWithoutDelimiter(request.POST, request.FILES) 
        if multiplicationform.is_valid():
            try:
                with transaction.atomic():
                    repro_id, child, parent, repro_method, site, start_date, end_date, comments = multiplicationform.create_multiplication()
                    
                    dico[repro_id] = [child, ' | '.join(parent), repro_method, site, start_date, end_date, comments]
                    
                    tag_fields.update({"all":dico.items,
                                       "hybridform":multiplicationform,
                                       })
                    return render(request, template, tag_fields)
                
            except ValidationError as e:
                errormsg=e.message
        
        elif formf.is_valid():
            return _create_genealogydata_from_file(request, template, tag_fields, model_relation, modelname, type_method)
                
        else:
            errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in multiplicationform.errors.items() ])
    return refine_search(request, multiplicationform, formf, dico, template, list_attributes, list_exceptions, title, modelname, filetype, typeid, errormsg, model_relation, type_method, classname)


def _create_genealogydata_from_file(request, template, tag_fields, model_relation, modelname, type_method):
    
    _upload_file(request.FILES['file'], request, model_relation, modelname, type_method)
    search_or_not = 0
    dico = fill_dico(type_method, model_relation, modelname, search_or_not)
    number_all = len(dico.items())
    tag_fields.update({'all':dico.items, "number_all":number_all})
    
    return render(request, template, tag_fields)
    

def _upload_file(file, request, model_relation, modelname, type_method):
    
    myfactory = CSVFactoryForTempFiles(file)
    myfactory.find_dialect()
    myfactory.read_file()
    file_dict = myfactory.get_file()
    
    headers_dict = {1:'Seedlot Multiplication',
                    2:'Accession Cross',
                    3:'Seedlot Cross',
                    4:'Other Accession Pedigree',
                    5:'Selfing'}
    
    headers_list = filerenderer_headers[headers_dict[type_method]]
    
    missing = myfactory.check_headers_conformity(headers_list)
    
    if missing != []:
        messages.add_message(request, messages.ERROR, "One or several mandatory headers are missing in your file : "+format(', '.join(missing))+\
                             ". The required headers for this file are: "+format(', '.join(headers_list))+".")
        return
    
    if request.POST.get("submit"): #Cas d'une insertion de nouvelles données
        _submit_upload_file(request, file_dict, model_relation, modelname, type_method)
    
    elif request.POST.get("update"): #L'utilisateur a cliqué sur le bouton update
        _update_upload_file(request, file_dict, model_relation, modelname, type_method)
    else:
        messages.add_message(request, messages.ERROR,'ThaliaDB was unable to identify if you\'re trying to insert new data or to update former data.')


def convert_string_to_date(string, formats):
    for format in formats:
        print(format)
        try:
            date = datetime.strptime(string,format)
            print(date)
        except:
            pass
        else:
            break
    return date
 
 
def _submit_upload_file(request, file_dict, model_relation, modelname, type_method):
    '''Dans ce cas, on va comparer les données du CSV et celles de la BDD via leur clé primaire
    S'il y a au moins un doublon, aucune donnée n'est insérée : soit l'utilisateur voulait faire un update, soit il ne s'est pas apercu des doublons
    Tout d'abord, on extrait les noms des accessions (clé primaire) de la BDD'''
    
    with transaction.atomic():
        for line in file_dict:
            try:
                with transaction.atomic():
                    method = Reproduction_method.objects.get(name=str(line['Method']), category=type_method)
                                                                 
                    if model_relation == "SeedlotRelationCross":
                        repro = Reproduction.objects.create(reproduction_method=method,
                                                            start_date = line['Start Date'],
                                                            end_date = line['End Date'],
                                                            description = str(line['Comments']),
                                                            entity_type=1)
                        repro.save()
                        
                        pm  = modelname.objects.create(child = Seedlot.objects.get(name = str(line['Name'])),
                                                       parent = Seedlot.objects.get(name = str(line['Parent Male'])),
                                                       parent_gender = 'M',
                                                       reproduction = repro,
                                                       site = str(line['Site']))
                        pm.save()
                        
                        pf = modelname.objects.create(child = Seedlot.objects.get(name = str(line['Name'])),
                                                      parent = Seedlot.objects.get(name = str(line['Parent Female'])),
                                                      parent_gender = 'F',
                                                      reproduction = repro,
                                                      site = str(line['Site']))
                        pf.save()
                        
                    elif model_relation == "SeedlotRelationMultiplication":
                        repro = Reproduction.objects.create(reproduction_method=method,
                                                            start_date = line['Start Date'],
                                                            end_date = line['End Date'],
                                                            description = str(line['Comments']),
                                                            entity_type=1)
                        repro.save()
                        
                        for parent in str(line['Parent(s)']).split('|'):
                            p = modelname.objects.create(child = Seedlot.objects.get(name = str(line['Name'])),
                                                          parent = Seedlot.objects.get(name = parent.strip()),
                                                          parent_gender = 'X',
                                                          reproduction = repro,
                                                          site = str(line['Site']))
                            p.save()
                            
                    elif model_relation == "AccessionRelationCross" and str(type_method) == '2':
                        if line['First Production'] == "":
                            line['First Production'] = None
                            
                        repro = Reproduction.objects.create(reproduction_method=method,
                                                            description = str(line['Comments']),
                                                            entity_type=0)
                        repro.save()

                        pm  = modelname.objects.create(child = Accession.objects.get(name = str(line['Name'])),
                                                       parent = Accession.objects.get(name = str(line['Parent Male'])),
                                                       parent_gender = 'M',
                                                       reproduction = repro,
                                                       first_production = line['First Production'])
                        pm.save()
                        
                        pf = modelname.objects.create(child = Accession.objects.get(name = str(line['Name'])),
                                                      parent = Accession.objects.get(name = str(line['Parent Female'])),
                                                      parent_gender = 'F',
                                                      reproduction = repro,
                                                      first_production = line['First Production'])
                        pf.save()
                        
                    elif model_relation == "AccessionRelationCross" and str(type_method) == '4':
                        if line['First Production'] == "":
                            line['First Production'] = None
                            
                        repro = Reproduction.objects.create(reproduction_method=method,
                                                            description = str(line['Comments']),
                                                            entity_type=0)
                        repro.save()

                        for parent_male in str(line['Parent Male']).split('|'):
                            pm  = modelname.objects.create(child = Accession.objects.get(name = str(line['Name'])),
                                                           parent = Accession.objects.get(name = parent_male.strip()),
                                                           parent_gender = 'M',
                                                           reproduction = repro,
                                                           first_production = line['First Production'])
                            pm.save()
                        
                        for parent_female in str(line['Parent Female']).split('|'):
                            pf = modelname.objects.create(child = Accession.objects.get(name = str(line['Name'])),
                                                          parent = Accession.objects.get(name = parent_female.strip()),
                                                          parent_gender = 'F',
                                                          reproduction = repro,
                                                          first_production = line['First Production'])
                            pf.save()
                        
                    elif model_relation == "AccessionSelfing":
                        repro = Reproduction.objects.create(reproduction_method=method,
                                                            description = str(line['Comments']),
                                                            entity_type=0)
                        repro.save()
                        
                        for parent in str(line['Parent(s)']).split('|'):
                            p = modelname.objects.create(child = Accession.objects.get(name = str(line['Name'])),
                                                         parent = Accession.objects.get(name = parent.strip()),
                                                         parent_gender = 'X',
                                                         reproduction = repro,
                                                         first_production = line['First Production'])
                            p.save()
                            
                    else:
                        raise Exception('An error has occurred.')
            
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+").")
                continue
            
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return
            
    messages.add_message(request, messages.SUCCESS, "The file has been successfully inserted.")


def _update_upload_file(request, file_dict, model_relation, modelname, type_method):
    '''Dans ce cas, on va comparer les données du CSV et celles de la BDD via leur clé primaire
    S'il y a au moins une nouvelle donnee, rien n'est mis à jour
    '''
    with transaction.atomic():
        for line in file_dict:
            try:
                with transaction.atomic():
                    if model_relation == "AccessionRelationCross" or model_relation == "AccessionSelfing":
                        if line['First Production'] == "":
                            line['First Production'] = None
                            
                        first_production = convert_string_to_date(line['First Production'], settings.DATE_INPUT_FORMATS)
                        print(first_production)
                        print(type(first_production))
                                    
                        parents = modelname.objects.filter(child = Accession.objects.get(name = str(line['Name'])))
                        
                        if parents:
                            for p in parents:
                                p.first_production = str(line['First Production'])
                                p.save()
                                
                            repro = Reproduction.objects.get(id = parents[0].reproduction_id)
                            
                            repro.reproduction_method = Reproduction_method.objects.get(name=str(line['Method']), category=type_method)
                            repro.description = str(line['Comments'])
                            
                            repro.save()
                        
                        else:
                            raise Exception("There is no genealogy relation for this accession.")
                        
                    elif model_relation == "SeedlotRelationCross" or model_relation == "SeedlotRelationMultiplication":
                        parents = modelname.objects.filter(child = Seedlot.objects.get(name = str(line['Name'])))
                        
                        if parents:      
                            for p in parents:
                                p.site = str(line['Site'])
                                p.save()
                                
                                
                            repro = Reproduction.objects.get(id = parents[0].reproduction_id)
                            
                            repro.reproduction_method = Reproduction_method.objects.get(name=str(line['Method']), category=type_method)
                                
                            repro.start_date = line['Start Date']
                            repro.end_date = line['End Date']
                            repro.description = str(line['Comments'])
                        
                        else:
                            raise Exception("There is no genealogy relation for this seedlot.")
                    
                    else:
                        return 'An error has occurred.'
                    
                    
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+").")
                    continue
                
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully uploaded and the relations have been updated.")


def fill_dico(type_method, model_relation, modelname, data_or_null):
    dico = {}
    ## pour n'afficher que les hybrides cross et pas les multiplications, il faut filtrer selon le type de methode utilisee
    if data_or_null == 0:
        repro_method_cross = Reproduction_method.objects.filter(category=type_method) ## type3 = sl cross method, type 2 = acc crossing method, type1=multiplication method
        repro_cross = Reproduction.objects.filter(reproduction_method__in=repro_method_cross)
        all_relation = modelname.objects.filter(reproduction__in=repro_cross).order_by('child')
         
    else:
        name_child = []
        for i in data_or_null:
            name_child.append(i.child)
        all_relation = modelname.objects.filter(child__in = name_child).order_by('child')
         
    model_ids = all_relation.values_list('child', flat=True).distinct()
     
    for id in model_ids:
        if modelname == SeedlotRelation:
            child = Seedlot.objects.get(id=id)
        else:
            child = Accession.objects.get(id=id)
             
        relations = modelname.objects.select_related('parent',
                                                     'reproduction', 
                                                     'reproduction__reproduction_method').filter(child=child)
     
        list_p_male = []
        list_p_female = []
        list_p_x = []
         
        for rel in relations:
            if rel.parent_gender == 'M':
                parent = rel
                list_p_male.append(rel.parent.name)
                 
            elif rel.parent_gender == 'F':
                list_p_female.append(rel.parent.name)
                 
            elif rel.parent_gender == 'X':
                parent = rel
                list_p_x.append(rel.parent.name)
                 
        if list_p_male:
            dico[parent.reproduction.id] = [child.name]
            dico[parent.reproduction.id].append(' | '.join(list_p_male))
             
        if list_p_female:
            dico[parent.reproduction.id].append(' | '.join(list_p_female))
             
        if list_p_x:
            dico[parent.reproduction.id] = [child.name]
            dico[parent.reproduction.id].append(' | '.join(list_p_x))
             
        dico[parent.reproduction.id].append(parent.reproduction.reproduction_method.name)
         
        if modelname == SeedlotRelation:
            dico[parent.reproduction.id].append(parent.site)
            dico[parent.reproduction.id].append(parent.reproduction.start_date)
            dico[parent.reproduction.id].append(parent.reproduction.end_date)
         
        else:
            dico[parent.reproduction.id].append(parent.first_production)
             
        dico[parent.reproduction.id].append(parent.reproduction.description)
     
    return dico


def refine_search(request, form, formf, data, template, list_attributes, list_exceptions, title, model, filetype, typeid, errormsg, model_relation, type_method,classname):
    number_all = len(data)
    if type(data)==dict:
        data=data.items
    nb_per_page = 10
    names, labels = get_fields(form)
    tag_fields = {'all':data,
                  'fields_name':names,
                  'fields_label':labels,
                  'title': title, "admin":True,
                  "errormsg":errormsg,
                  "refine_search":True,
                  "classname":classname,
                  'admin':True}
    dico_get={}
    or_and = None
    if "no_search" in request.GET:
        tag_fields.update({"search_result":dico_get,
                           "or_and":or_and,
                           'all':data,
                           'excel_empty_file':True,
                           "formfile":formf,
                           "number_all":number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           "filetype":filetype,
                           "typeid":typeid,
                           'form':form,
                           "list_attributes":list_attributes})
        return render(request,template,tag_fields)
     
    elif "or_and" in request.GET:
        or_and=request.GET['or_and']
        for i in range(1,11):
            data="data"+str(i)
            text_data="text_data"+str(i)
            if data in request.GET and text_data in request.GET:
                dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera

        length_dico_get = len(dico_get)
        print("\ndico_get: ",dico_get)
        nb_per_page = 10
        if or_and == "or":
            proj=[]
            query=Query_dj()
            list_name_in=[]
            list_attr=[]
            for dat, dico_type_data in dico_get.items():
                if "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        print('Projects empty')
                        query |= Query_dj(projects__isnull=True)
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query |= Query_dj(projects__in=proj)
                elif "accession" in dico_type_data:
                    child_list = Accession.objects.filter(name__icontains=dico_type_data['accession'])
                    query |= Query_dj(child__in=child_list)
                elif "hybrid seedlot" in dico_type_data:
                    child_list = Seedlot.objects.filter(name__icontains=dico_type_data['hybrid seedlot'])
                    query |= Query_dj(child__in=child_list)
                elif "seedlot" in dico_type_data:
                    child_list = Seedlot.objects.filter(name__icontains=dico_type_data['seedlot'])
                    query |= Query_dj(child__in=child_list)
                elif "parent male" in dico_type_data :
                    if model == AccessionRelation:
                        parent_list = Accession.objects.filter(name__icontains=dico_type_data['parent male'])
                        query |= Query_dj(parent__in=parent_list)
                    elif model == SeedlotRelation:
                        parent_list = Seedlot.objects.filter(name__icontains=dico_type_data['parent male'])
                        query |= Query_dj(parent__in=parent_list)
                elif "parent female" in dico_type_data:
                    if model == AccessionRelation:
                        parent_list = Accession.objects.filter(name__icontains=dico_type_data['parent female'])
                        query |= Query_dj(parent__in=parent_list)
                    elif model == SeedlotRelation:
                        parent_list = Seedlot.objects.filter(name__icontains=dico_type_data['parent female'])
                        query |= Query_dj(parent__in=parent_list)
                elif "parent(s)" in dico_type_data:
                    if model == AccessionRelation:
                        parent_list = Accession.objects.filter(name__icontains=dico_type_data['parent(s)'])
                        query |= Query_dj(parent__in=parent_list)
                    elif model == SeedlotRelation:
                        parent_list = Seedlot.objects.filter(name__icontains=dico_type_data['parent(s)'])
                        query |= Query_dj(parent__in=parent_list)
                elif "method" in dico_type_data:
                    repro_method_cross = Reproduction_method.objects.filter(name__icontains=dico_type_data['method']) 
                    repro_cross = Reproduction.objects.filter(reproduction_method__in=repro_method_cross)
                    query |= Query_dj(reproduction__in=repro_cross)
                elif 'start date' in dico_type_data:
                    date_repro = Reproduction.objects.filter(start_date__icontains=dico_type_data['start date'])
                    query |= Query_dj(reproduction__in=date_repro)
                elif 'end date' in dico_type_data:
                    date_repro = Reproduction.objects.filter(start_date__icontains=dico_type_data['end date'])
                    query |= Query_dj(reproduction__in=date_repro)
                elif 'comments' in dico_type_data:
                    comments = Reproduction.objects.filter(description__icontains=dico_type_data['comments'])
                    query |= Query_dj(reproduction__in=comments)
                else:
                    for data_type, value in dico_type_data.items():
                        var_search = str( data_type + "__icontains" ) 
                        var_search = var_search.replace(' ','_')
                        query |= Query_dj(**{var_search : dico_type_data[data_type]})
             
        if or_and == "and":
            proj=[]
            query=Query_dj()
            list_name_in=[]
            list_attr=[]
            for dat, dico_type_data in dico_get.items():
                if "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        print('Projects empty')
                        query &= Query_dj(projects__isnull=True)
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query &= Query_dj(projects__in=proj)
                elif "accession" in dico_type_data:
                    child_list = Accession.objects.filter(name__icontains=dico_type_data['accession'])
                    query |= Query_dj(child__in=child_list)
                elif "hybrid seedlot" in dico_type_data:
                    child_list = Seedlot.objects.filter(name__icontains=dico_type_data['hybrid seedlot'])
                    query |= Query_dj(child__in=child_list)
                elif "seedlot" in dico_type_data:
                    child_list = Seedlot.objects.filter(name__icontains=dico_type_data['seedlot'])
                    query |= Query_dj(child__in=child_list)
                elif "parent male" in dico_type_data :
                    if model == AccessionRelation:
                        parent_list = Accession.objects.filter(name__icontains=dico_type_data['parent male'])
                        query &= Query_dj(parent__in=parent_list)
                    elif model == SeedlotRelation:
                        parent_list = Seedlot.objects.filter(name__icontains=dico_type_data['parent male'])
                        query &= Query_dj(parent__in=parent_list)
                elif "parent female" in dico_type_data:
                    if model == AccessionRelation:
                        parent_list = Accession.objects.filter(name__icontains=dico_type_data['parent female'])
                        query &= Query_dj(parent__in=parent_list)
                    elif model == SeedlotRelation:
                        parent_list = Seedlot.objects.filter(name__icontains=dico_type_data['parent female'])
                        query &= Query_dj(parent__in=parent_list)
                elif "parent(s)" in dico_type_data:
                    if model == AccessionRelation:
                        parent_list = Accession.objects.filter(name__icontains=dico_type_data['parent(s)'])
                        query &= Query_dj(parent__in=parent_list)
                    elif model == SeedlotRelation:
                        parent_list = Seedlot.objects.filter(name__icontains=dico_type_data['parent(s)'])
                        query &= Query_dj(parent__in=parent_list)
                elif "method" in dico_type_data:
                    repro_method_cross = Reproduction_method.objects.filter(name__icontains=dico_type_data['method'])
                    repro_cross = Reproduction.objects.filter(reproduction_method__in=repro_method_cross)
                    query &= Query_dj(reproduction__in=repro_cross)
                elif 'start date' in dico_type_data:
                    date_repro = Reproduction.objects.filter(start_date__icontains=dico_type_data['start date'])
                    query &= Query_dj(reproduction__in=date_repro)
                elif 'end date' in dico_type_data:
                    date_repro = Reproduction.objects.filter(start_date__icontains=dico_type_data['end date'])
                    query &= Query_dj(reproduction__in=date_repro)
                elif 'comments' in dico_type_data:
                    comments = Reproduction.objects.filter(description__icontains=dico_type_data['comments'])
                    query &= Query_dj(reproduction__in=comments)
                else:
                    for data_type, value in dico_type_data.items():
                        var_search = str( data_type + "__icontains" ) 
                        var_search = var_search.replace(' ','_')
                        query &= Query_dj(**{var_search : dico_type_data[data_type]})
        repro_method_cross = Reproduction_method.objects.filter(category=type_method) ## type3 = sl cross method, type 2 = acc crossing method, type1=multiplication method
        repro_cross = Reproduction.objects.filter(reproduction_method__in=repro_method_cross)
        query &= Query_dj(reproduction__in=repro_cross)
        data = model.objects.filter(query).distinct()
        number_all = len(data)
        dico = fill_dico(type_method, model_relation, model, data)
        download_not_empty = False
        if data:
            download_not_empty = write_csv_search_result(data, list_exceptions)
        tag_fields.update({"download_not_empty":False, 'all':dico.items})
    #Cas où veut afficher la base de données
    #accessions = _get_accessions(accessiontype, attributes, request)
    tag_fields.update({"search_result":dico_get,
                       "or_and":or_and,
                       "number_all":number_all,
                       'nb_per_page':nb_per_page,
                       'creation_mode':True,
                       'form':form,
                       "list_attributes":list_attributes,
                       "filetype":filetype,
                       'excel_empty_file':True,
                       "typeid":typeid,
                       "formfile":formf})  
    return render(request, template, tag_fields) 
