# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import unicode_literals
import json
import ast

from django.apps import apps
from django.db import models
from django.dispatch import receiver
from django.core.exceptions import ValidationError
from django.utils.encoding import force_bytes

from commonfct.managers import EntityTypeManager
from genotyping.managers import SampleManager, ReferentialManager, ExperimentManager, GenotypingIDManager, LocusManager, LocusAttributePostionManager
from team.serializers import ContactsSerializer
from team.models import Person
from commonfct.constants import ATTRIBUTE_TYPES

class Sample(models.Model):
    name = models.CharField(max_length=100, unique=True, db_index=True)
    seedlot = models.ForeignKey('lot.Seedlot', db_index=True, on_delete=models.CASCADE)
    description = models.TextField(blank=True, null=True)
    is_bulk = models.BooleanField(default=False)
    is_obsolete = models.BooleanField(default=False)
    codelabo = models.CharField(max_length=100,blank=True, null=True)
    tubename = models.CharField(max_length=100,blank=True, null=True)
    objects = SampleManager() 
    
    #projects = models.ManyToManyField(Project)
    projects = models.ManyToManyField('team.Project', blank=True)
    def __str__(self):
        return self.name
    
    class Meta:
        ordering = ['name',]
    
    #forBrAPI
    def getBlankDateValue(self):
        return '1900-01-01T12:00:00-0600'
    
    def getBlankValue(self):
        return ''
    
    def getExternalReferences(self):
        return [{'referenceID' : '','referenceId' : '','referenceSource': ''}] 
     
    def getGermplasmDbId(self):  
        return self.seedlot.accession.id
    
    def getSampleAdditionalInfo(self):
        sampleAdditionalInfo = {}
        if self.is_bulk :
            sampleAdditionalInfo['is_bulk'] = str(self.is_bulk)
        if self.is_obsolete :
            sampleAdditionalInfo['is_obsolete'] = str(self.is_obsolete)
        if self.tubename :
            sampleAdditionalInfo['tubename'] = str(self.tubename)
        return sampleAdditionalInfo
    
    
@receiver(models.signals.m2m_changed, sender=Sample.projects.through)
def report_projects_sample(sender, instance, action, pk_set, **kwargs):
    """
      Actions to be performed according to the link or unlink of projects.
      If projects are added to the sample they are added to the seedlot.
      If projects are removed, they are also removed for the genotyping id.
    """
    Project = apps.get_model('team','project')
    
    if action == 'post_add':
        projects = Project.objects.filter(id__in=pk_set)
        seedlot = instance.seedlot
        for project in projects:
            if project not in seedlot.projects.all():
                seedlot.projects.add(project)
                
    elif action == 'post_remove':
        projects = Project.objects.filter(id__in=pk_set)
        for project in projects:
            genoids = instance.genotypingid_set.all()
            if genoids:
                for genoid in genoids:
                    if project in genoid.projects.all():
                        genoid.projects.remove(project)
    
    
class Referential(models.Model):
    name  = models.CharField(max_length=100, unique=True)
    person = models.ForeignKey('team.Person', on_delete=models.CASCADE)
    date = models.DateField()
    comments = models.TextField(blank=True, null=True)
    linkedfiles = models.ManyToManyField('team.Documents', blank=True, null=True)
    objects = ReferentialManager()
    
    projects = models.ManyToManyField('team.Project', blank = True)
    
    def __str__(self):
        return self.name
    
    class Meta:
        ordering = ['name',]

class Experiment(models.Model):
    name  = models.CharField(max_length=100, unique=True)
    institution = models.ForeignKey('team.Institution', on_delete=models.CASCADE)
    person = models.ForeignKey('team.Person', on_delete=models.CASCADE)
    date = models.DateField()
    comments = models.TextField(blank=True, null=True)
    objects = ExperimentManager()

    projects = models.ManyToManyField('team.Project',blank=True)
    def __str__(self):
        return self.name
    class Meta:
        ordering = ['name']

class GenotypingID(models.Model):
    name = models.CharField(max_length=100, unique=True)
    sample = models.ForeignKey('Sample', null=True, on_delete=models.CASCADE)
    experiment = models.ForeignKey('Experiment', null=True, on_delete=models.CASCADE)
    sentrixbarcode_a = models.CharField(max_length=100, null=True,blank=True)
    sentrixposition_a = models.CharField(max_length=100, null=True,blank=True)
    funding = models.CharField(max_length=100, null=True,blank=True)
    objects = GenotypingIDManager()
    
    projects = models.ManyToManyField('team.Project',blank=True)

    def __str__(self):
        return self.name
     
    class Meta:
        ordering = ['name',]
        
        indexes = [
            models.Index(fields=['sample','experiment'])
        ]
        
@receiver(models.signals.m2m_changed, sender=GenotypingID.projects.through)
def report_projects_genotypingid(sender, instance, action, pk_set, **kwargs):
    """
      Actions to be performed according to the link or unlink of projects.
      If projects are added to the genotypingid they are added to the sample.
    """
    Project = apps.get_model('team','project')
   
    if action == 'post_add':
        projects = Project.objects.filter(id__in=pk_set)
        sample = instance.sample
        for project in projects:
            if project not in sample.projects.all():
                sample.projects.add(project)
                
    
class LocusType(models.Model):
    name = models.CharField(max_length=100, unique=True, db_index=True)
    description = models.TextField(max_length=500, blank=True, null=True)
    def __str__(self):
        return self.name
    
    def attribute_set(self):
        return self.locustypeattribute_set
    
    def remove_attribute(self, attribute_id):
        #remove an attribute from list and update positions
        attribute = LocusTypeAttribute.objects.get(id=attribute_id)
        self.locustypeattribute_set.remove(attribute)
        self.update_attributes_positions()
    
    def update_attributes_positions(self):
        i = 1
        for position in LocusAttributePosition.objects.filter(type=self) :
            position.position = i
            position.save()
            i += 1
    
    def recompute_positions(self, new_positions):
        for attribute in self.locustypeattribute_set.all() :
            self.locustypeattribute_set.remove(attribute)
        
        for stringpos in new_positions:
            pos = ast.literal_eval(stringpos)
            attribute = LocusTypeAttribute.objects.get(id=pos[0])
            LocusAttributePosition.objects.create(attribute=attribute,
                                                      position=pos[1],
                                                      type=self)

class LocusTypeAttribute(models.Model):
    class AttributeType(models.IntegerChoices):
        SHORT_TEXT = 1,"Short Text"
        LONG_TEXT = 2,"Long Text"
        NUMBER = 3,"Number"
        URL = 4,"URL"
    
    name = models.CharField(max_length=100, unique=True, blank=True)
    type = models.IntegerField(choices=AttributeType.choices)
    locus_type = models.ManyToManyField('LocusType', through='LocusAttributePosition')
    
    def natural_key(self):
        return (self.name, self.type)
    
    class Meta:
        unique_together = ("name","type")

    def __str__(self):
        return self.name
    
    def clean(self):
        loc_fields = Locus._meta.get_fields()
        loc_fields_names = [f.name for f in loc_fields]
        pos_fields = LocusPosition._meta.get_fields()
        pos_fields_names = [f.name for f in pos_fields]
        clean_fields_names = loc_fields_names + pos_fields_names
        
        if self.name.lower().replace(' ','_') in clean_fields_names:
            raise ValidationError('A field with this name already exists for locus.')
        
        

class LocusAttributePosition(models.Model):
    
    attribute = models.ForeignKey('LocusTypeAttribute', on_delete=models.CASCADE)
    type = models.ForeignKey('LocusType', on_delete=models.CASCADE)
    position = models.IntegerField()
    
    objects = LocusAttributePostionManager()
    
    class Meta:
        unique_together = (("type",'attribute'),('type',"position"))
        ordering = ('type','position')  


#### Locus new models

class Locus(models.Model):
    """
      The Locus model gives informations about the locus.
      
      :var CharField name: name of the locus
      :var ForeignKey type: type of the locus, defined in the LocusType model
      :var TextField comments: comments on the locus
      :var JSONField attributes_values: values of the locus type's attributes
    """
    name = models.CharField(max_length=100, unique=True, db_index=True)
    type = models.ForeignKey('genotyping.LocusType', db_index=True, on_delete=models.CASCADE)
    comments = models.TextField(max_length=500, blank=True, null=True)
    attributes_values = models.JSONField(blank=True, null=True)
    
    geno_values = models.ManyToManyField('genotyping.GenotypingValue', through='genotyping.LocusToValue')
    
    objects = LocusManager()
    
    def __str__(self):
        return self.name
    
    class Meta:
        ordering = ['name']
        
    def update_attributes(self,attributes, postdata):
        jsonvalues = {}
        for att in attributes:
            if att.name.lower().replace(' ','_') in postdata.keys():
                jsonvalues[att.id] = postdata[att.name.lower().replace(' ','_')]
            else:
                jsonvalues[att.id] = ''
        self.attributes_values = jsonvalues


class LocusPosition(models.Model):
    """
      The LocusPosition model gives informations on the position of the locus according to the genome version
      
      :var ForeignKey locus: locus to which belongs position informations
      :var IntegerField genome_version: genome version of the locus' informations
      :var CharField chromosome: chromosome on which the locus is
      :var IntegerField position: position of the locus on the chromosome
    """
    locus = models.ForeignKey('genotyping.Locus', on_delete=models.CASCADE)
    genome_version = models.ForeignKey('genotyping.GenomeVersion', on_delete=models.CASCADE, blank=True, null=True)
    chromosome = models.CharField(max_length=100, blank=True, null=True)
    position = models.IntegerField(blank=True, null=True)
    
    def __str__(self):
        return "Information of "+str(self.locus.name)+". "
    
    class Meta:
        unique_together=("locus", "genome_version")
        
        indexes = [
            models.Index(fields=['chromosome', 'position']),
        ]
        
    #for BrAPI
    def getBlankValue(self):
        return ''
    
    def getAdditionalInfo(self):
        locusPositionAdditionalInfo = {}
        if self.chromosome :
            locusPositionAdditionalInfo['chromosome'] = self.chromosome
        return locusPositionAdditionalInfo
    
    def getId(self):
        return self.locus.id
    
    def getMapId(self):
        return self.genome_version.id 
    
    def getMapName(self):
        return self.genome_version.name 
    
    def getName(self):
        return self.locus.name


@receiver(models.signals.post_save, sender=LocusPosition)
def delete_null_positions(sender, instance, created, raw, **kwargs):
    """
      To automatically delete null positions if real 
      positions are submitted later by the user.
    """
    if created:
        if instance.genome_version != None:
            none_position = LocusPosition.objects.filter(locus=instance.locus, genome_version=None)
            if none_position:
                none_position.delete()
        
        

class GenomeVersion(models.Model):
    """
      The GenomeVersion model gives informations about genome versions.
      
      :var CharField name: name of the genome version
      :var TextField name: description of the genome version
    """
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(max_length=500, blank=True, null=True)
    
    def __str__(self):
        return self.name
    
    #for BrAPI
    def get0Value(self):
        return 0
    
    def getBlankDateValue(self):
        return '1900-01-01T12:00:00-0600'
    
    def getBlankValue(self):
        print (type(self.id))
        return ''
    
    def getAdditionalInfo(self):
        genomeVersionAdditionalInfo = {}
        return genomeVersionAdditionalInfo
    
    def getCommonCropName(self):
        return settings.COMMONCROPNAME  
    
    def getDocumentationURL(self):
        return 'https://brapicore21.docs.apiary.io/#/reference/genome-maps/get-maps'
    
  

class LocusToValue(models.Model):
    """
      The LocusToValue model is an intermediate model between Locus and GenotypingValue models
      to store the ManyToMany relationship.
    """
    id = models.BigAutoField(primary_key=True)
    locus = models.ForeignKey('genotyping.Locus', on_delete=models.CASCADE)
    genotyping_value = models.ForeignKey('genotyping.GenotypingValue', on_delete=models.CASCADE)
    
    class Meta:
        #unique_together=("locus", "genotyping_value")
        indexes = [
                models.Index(fields=("locus", "genotyping_value")),
            ]

class GenotypingValue(models.Model):
    """
      The GenotypingValue model allows to store genotyping data.
      :var ForeignKey genotyping_id: genotyping_id on which the value has been measured.
      :var ForeignKey locus: locus for which the value has been measured.
      :var ForeignKey referential: referential
      :var CharField allele: genotyping value.
      :var FloatField frequency: allelic frequency.
    """
    id = models.BigAutoField(primary_key=True)
    genotyping_id = models.ForeignKey('genotyping.GenotypingID', on_delete=models.CASCADE)
    referential = models.ForeignKey('genotyping.Referential', on_delete=models.CASCADE)
    allele = models.ForeignKey('genotyping.CodeAllele', on_delete=models.CASCADE)
    frequency = models.FloatField(default='1', null=True)
    
    class Meta:
        #unique_together=("referential", "genotyping_id", "allele", "frequency")
        
        indexes = [
            models.Index(fields=['referential']),
            models.Index(fields=("referential", "genotyping_id", "allele", "frequency"))
        ]
        

class CodeAllele(models.Model):
    """
      The CodeAllele model allows to store allele code to link to GenotypingValue model.
    """
    code_allele = models.CharField(max_length=255, unique=True)


# class VcfValue(models.Model):
#     """
#       The VcfValue model allows to store VCF genotyping data.
#     """

"""Mongodb model classes"""
# class Position(EmbeddedDocument):
#     genomeversion = fields.IntField(blank=False)
#     chromosome = fields.StringField(blank=True)
#     position = fields.LongField(blank=True)
# 
# class AttributeValue(EmbeddedDocument):
#     attribute_id = fields.IntField()
#     value = fields.StringField()
# 
# class RefXpGenotype(EmbeddedDocument):
#     referential_id = fields.IntField()
#     experiment_id = fields.IntField()
#     genotype = fields.StringField()
#     frequency = fields.FloatField()
#     
# class SampleGenotype(EmbeddedDocument):
#     sample_id = fields.IntField()
#     genotyping = fields.ListField(fields.EmbeddedDocumentField('RefXpGenotype'))
# 
# class Locus(Document):
#     name = fields.StringField()
#     type = fields.IntField(blank=True)
#     positions = fields.ListField(fields.EmbeddedDocumentField('Position'), blank=True)
#     comment = fields.StringField(blank=True)
#     attributes = fields.ListField(fields.EmbeddedDocumentField('AttributeValue'), blank=True)
#     genotyping = fields.ListField(fields.EmbeddedDocumentField('SampleGenotype'), blank=True)
# 
#     #referential = fields.StringField(blank=True)
#     meta = {
#         'indexes': [
#             'name',
#             '$genotyping',
#             'attributes.attribute_id',
#             ('positions.chromosome','-positions.genomeversion'),
#             'type'
#         ],
#         'strict': False,
#         'collection':'locus',
#         'unique':'name', #unicite du nom a definir ici et non pas dans les () sinon erreur
#             }
#     
#     #pour afficher leur nom sur l'interface et non "<Locus object>"
#     def __str__(self):
#         return self.name
#     

class VcfFiles(models.Model):
    name = models.CharField(max_length=100, null=True)

    def __str(self):
        return self.name

    class Meta:
        ordering = ['name']
    