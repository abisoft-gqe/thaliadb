# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from cProfile import label

from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.utils.safestring import mark_safe

from dal import autocomplete

from genotyping.models import Experiment, Referential, LocusType, LocusTypeAttribute,\
                              Locus, Sample, GenotypingID, LocusPosition, GenomeVersion
from accession.models import Accession
from phenotyping.models import Trait, Environment
from commonfct.forms_utils import HorizontalRadioSelect

class LocusTypeAttributeSearch(forms.Form):
    attributes = forms.ModelChoiceField(label="Attribute",
                                       queryset=LocusTypeAttribute.objects.all(),
                                       required=False,
                                       widget=autocomplete.ModelSelect2(url='locus-attributes-autocomplete', attrs={'style': 'width:300px'}))
    

class ExperimentForm(forms.ModelForm):
    class Meta:
        model = Experiment
        fields = '__all__'
        widgets = {'date': forms.TextInput(attrs = {'class':'datePicker'}),
                  }
        
class SampleForm(forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = Sample
        blank = True
        
class ReferentialForm(forms.ModelForm):
    class Meta:
        model = Referential
        fields = '__all__'
        widgets = {'date': forms.TextInput(attrs = {'class':'datePicker'}),
                  }

class LocusTypeForm(forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = LocusType
        

class LocusTypeAttForm(forms.ModelForm):
    class Meta:
        model = LocusTypeAttribute
        exclude = ['locus_type']
        widgets = {'name': forms.TextInput(attrs = {'id':'attributes_autocomplete'}),}

class PositionForm(forms.ModelForm):
    class Meta:
        model = LocusPosition
        fields = ['genome_version','chromosome','position']
        widgets = {'chromosome': forms.TextInput(),}

class LocusDataForm(forms.ModelForm):
    class Meta:
        model = Locus
        exclude = ['type','attributes_values','geno_values']
        

    def __init__(self, *args, **kwargs):
        if 'formstype' in kwargs.keys():
            attrs = kwargs['formstype']
            print(kwargs.keys())
            if 'data' in kwargs.keys():
                if 'instance' in kwargs.keys():
                    super(LocusDataForm, self).__init__(data=kwargs['data'], instance=kwargs['instance'])
                else :
                    super(LocusDataForm, self).__init__(data=kwargs['data'])
            else :
                super(LocusDataForm, self).__init__()
            for at in attrs:
                if at.type == 2:
                    self.fields[at.name.lower().replace(' ','_')] = forms.CharField(widget=forms.Textarea(attrs={'rows':'1'}),
                                                                                              required = False,
                                                                                              label = str(at.name))
                else:
                    self.fields[at.name.lower().replace(' ','_')] = forms.CharField(required = False,
                                                                                              label = str(at.name))

        else :
            if 'data' in kwargs.keys():
                super(LocusDataForm, self).__init__(data=kwargs['data'])
            else :
                super(LocusDataForm, self).__init__()
    #=======================
    
    
                
class LocusDataFileForm(forms.ModelForm):
    class Meta:
        model = Locus
        exclude = ['attributes_values', 'type']
#         widgets = {'name': forms.TextInput(attrs = {'id':'attributes_autocomplete'}),}
    def __init__(self, *args, **kwargs):
        if 'formstype' in kwargs.keys():
            attrs = kwargs['formstype']
            if 'data' in kwargs.keys():
                if 'instance' in kwargs.keys():
                    super(LocusDataFileForm, self).__init__(data=kwargs['data'], instance=kwargs['instance'])
                else :
                    super(LocusDataFileForm, self).__init__(data=kwargs['data'])
            else :
                super(LocusDataFileForm, self).__init__()
            for at in attrs:
                self.fields[at.name.lower().replace(' ','_')] = forms.CharField(required = False)
        else :
            if 'data' in kwargs.keys():
                super(LocusDataFileForm, self).__init__(data=kwargs['data'])
            else :
                super(LocusDataFileForm, self).__init__()
    #=============================================================================================
    
class GenomeVersionForm(forms.ModelForm):
    """
      Form based on the GenomeVersion model managing data of this model
    """
    class Meta:
        model = GenomeVersion
        fields = '__all__'
        

#==================================================================
#==================================================================            

#from django.core.validators import validate

    
CHOICES1 = (('1', 'Allele mode',), ('2', 'Standard mode',))

class UploadGenoTypValueFileForm(forms.Form):
    
    experiment = forms.ModelChoiceField(queryset=Experiment.objects.all(),empty_label="Choose Experiment")
    referential = forms.ModelChoiceField(queryset=Referential.objects.all(),empty_label="Choose Referential")    
    select_mode = forms.ChoiceField(widget=HorizontalRadioSelect(), choices=CHOICES1)
    verication_file = forms.FileField(label='Verification file', required=False)
    main_file = forms.FileField(label='Main file')


class UploadFileForm(forms.Form):
    file = forms.FileField(label='Enter input file ')
    delimiter = forms.CharField(label='Delimiter of the file ')
    
class UploadFileFormWithoutDelimiter(forms.Form):
    file = forms.FileField(label='Enter input file ')
    
   
MATRIX_CHOICES = [('loci_col','Columns of the file = LOCI'),
                  ('genoid_col','Columns of the file = GENOTYPINGID')]
class UploadFileForm_Matrix(forms.Form):
    file = forms.FileField(label='Enter input file ')
    delimiter = forms.CharField(label='Delimiter of the file ')
    matrix_orientation = forms.ChoiceField(widget=HorizontalRadioSelect(), choices = MATRIX_CHOICES, required = True, initial = 'loci_col')
    
CHOIX = [('1', 'Au niveau individu'),
         ('2', 'Au niveau frequence')]

class ChoiceIndOrFreq(forms.Form):
    choice = forms.ChoiceField(widget=HorizontalRadioSelect(), choices=CHOIX)
    
class ChoiceRefExpForm(forms.Form):
    ref = forms.ModelChoiceField(queryset=Referential.objects.all(),
                                 label='Choose a referential')
    #exp = forms.ModelChoiceField(queryset=Experiment.objects.all(),
      #                           label='Choose an experiment')

    
CHOICES = [('1', 'View considering Accession',), 
           ('2', 'View considering Seed lot',), 
           ('3', 'View considering Sample',)]

class ViewGenotypingValuesForm(forms.Form):
    
    accessions = forms.ModelMultipleChoiceField(queryset=Accession.objects.all(),
                                                widget=FilteredSelectMultiple("Accession",False,attrs={'rows':'10'}))
    locus = forms.FileField(label="Enter a loci file")
    experiments = forms.ModelMultipleChoiceField(queryset=Experiment.objects.all())
    referentials = forms.ModelMultipleChoiceField(queryset=Referential.objects.all())
    missing_data = forms.CharField(max_length=30, initial='missing')
    check = forms.BooleanField(label="View considering correspondence", required=False)
    choose_visualization_mode = forms.ChoiceField(widget=HorizontalRadioSelect(), choices=CHOICES)
    #forms.ChoiceField(widget=forms.RadioSelect(renderer=MyCustomRenderer), choices=CHOICES1)
    

class DropGenoValuesForm (forms.Form):
    #experiments = forms.CharField( widget=forms.Textarea)
    experiments = forms.ModelMultipleChoiceField(queryset=Experiment.objects.all(),
                                                widget=FilteredSelectMultiple("Experiment",False,attrs={'rows':'100'}))
    samples = forms.ModelMultipleChoiceField(queryset=Sample.objects.all(),
                                                widget=FilteredSelectMultiple("Sample",False,attrs={'rows':'100'}))
#     locis = forms.ModelMultipleChoiceField(queryset=Locus.objects.all(),
#                                                 widget=FilteredSelectMultiple("Locus",False,attrs={'rows':'100'}))
    locis = forms.FileField(label="Enter a loci file to delete")
    
class GenotypingIDForm(forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = GenotypingID
        blank = True

class LocusPositionUploadForm(forms.Form):
    """
      Form used to upload a file with position info for locus.
    """
    genome_version = forms.ModelChoiceField(required=True,
                                            queryset=GenomeVersion.objects.all(),
                                            empty_label="Choose a genome version")
    file = forms.FileField(label='Select the file to upload ')


FILE_FORMATS = (('1', 'IUPAC code matrix'),
                ('2', '2 letters code matrix'),
                ('3', '"/" matrix'),
                ('4', 'Flat file'),
                ('5', 'VCF format'))

MATRIX_ORIENTATION = (('1', 'Loci in lines'),
                      ('2', 'Loci in columns'))


class ChoiceFieldWithDisableOption(forms.Select):
    """
      Custom widget used to restrict the file format list.
    """
    def create_option(self, *args, **kwargs):
        options_dict = super().create_option(*args, **kwargs)
        
        if options_dict['value'] in ['1','5']:
            options_dict['attrs']['disabled'] = ''
        
        return options_dict

class InsertGenotypicValuesForm(forms.Form):
    """
      Form used to upload a file containing genotypic values.
    """
    referential = forms.ModelChoiceField(required=True,
                                         queryset=Referential.objects.all(),
                                         label='Choose a referential')
    
    file_format = forms.ChoiceField(required=True,
                                    widget = ChoiceFieldWithDisableOption(),
                                    choices = FILE_FORMATS,
                                    label='Select file format')
    
    matrix_orientation = forms.ChoiceField(required=False,
                                           choices=MATRIX_ORIENTATION,
                                           label='Select matrix orientation')
    
    missing_value = forms.CharField(required=False,
                                    max_length=30,
                                    label="Missing value code")
    
    frequency_test = forms.BooleanField(required=False,
                                        label="Check if frequencies sum up to 1 (0.1% test)")
    
    file = forms.FileField(label='Select the file to upload')
    
