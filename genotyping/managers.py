"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Laetitia Courgey

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import ast

from operator import ior, iand

from django.apps import apps
from django.db import models
from django.db.models import Q

from team.models import User, Project

class LocusAttributePostionManager(models.Manager):
    
    def create_last_element(self, attribute_id, locus_type):
        LocusTypeAttribute = apps.get_model('genotyping','locustypeattribute')
        
        attribute = LocusTypeAttribute.objects.get(id=attribute_id)
        allattributes = locus_type.locusattributeposition_set.order_by('-position')
        if allattributes :
            last_position = allattributes[0].position+1
        else :
            last_position = 1
        return self.create(attribute=attribute,
                    type=locus_type,
                    position=last_position)

class SampleManager(models.Manager):
    """
      Manager for sample model.
    """
    def by_username(self,username):
        """
        Returns only the samples from projects linked to this user 

        :param string username : username

        :var int username_id : user's id
        :var list projects : projects linked to the user
        """
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
        return self.filter(projects__in=projects).distinct()
    
    def refine_search(self, or_and, dico_get):
        """
        Search in all samples according to some parameters

        :param string or_and : link between parameters (or, and)
        :param dict dico_get : data to search for
        """
        # Managers are called in genotyping.models
        # To avoid crossed imports we use apps to get objects from the model 
        Sample = apps.get_model('genotyping','sample')
        Seedlot = apps.get_model('lot','seedlot')
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
            
        proj=[]
        query=Q()
        
        for dat, dico_type_data in dico_get.items():
            if "projects" in dico_type_data:
                if dico_type_data['projects']=='':
                    query = myoperator(query, Q(projects__isnull=True))
                else:
                    proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                    query = myoperator(query, Q(projects__in=proj))
            else:
                for data_type, value in dico_type_data.items():
                    search_value = dico_type_data[data_type]
                    data_type = data_type[0].lower()+data_type[1:]
                    var_search = str(data_type + "__icontains" ) 
                    if data_type == "seedlot":
                        var_search = str(data_type+"__in")
                        data_filter = Seedlot.objects.filter(name__icontains=value)
                        data_id_list = []
                        for i in data_filter:
                            data_id_list.append(i.id)
                        search_value = data_id_list
                    query = myoperator(query, Q(**{var_search : search_value}))
                    
        return Sample.objects.select_related('seedlot').prefetch_related('projects').filter(query).distinct()


class ExperimentManager(models.Manager):
    """
      Manager for experiment model.
    """
    def by_username(self,username):
        """
        Returns only the experiments from projects linked to this user 

        :param string username : username

        :var int username_id : user's id
        :var list projects : projects linked to the user
        """
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
        return self.filter(projects__in=projects).distinct()
    


class ReferentialManager(models.Manager):
    """
      Manager for referential model.
    """
    def by_username(self,username):
        """
        Returns only the referentials from projects linked to this user 

        :param string username : username

        :var int username_id : user's id
        :var list projects : projects linked to the user
        """
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
        return self.filter(projects__in=projects).distinct()


class GenotypingIDManager(models.Manager):
    """
      Manager for genotypingid model.
    """
    def refine_search(self, or_and, dico_get):
        """
          Search in all GenotypingID according to some parameters

          :param string or_and : link between parameters (or, and)
          :param dict dico_get : data to search for
        """
        # Managers are called in genotyping.models
        # To avoid crossed imports we use apps to get objects from the model
        GenotypingID = apps.get_model('genotyping','genotypingid')
        Sample = apps.get_model('genotyping','sample')
        Experiment = apps.get_model('genotyping','experiment')
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
        
        proj=[]
        query=Q()
        
        for dat, dico_type_data in dico_get.items():
            if "projects" in dico_type_data:
                if dico_type_data['projects']=='':
                    query = myoperator(query, Q(projects__isnull=True))
                else:
                    proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                    query = myoperator(query, Q(projects__in=proj))
            else:
                for data_type, value in dico_type_data.items():
                    search_value = dico_type_data[data_type]
                    data_type = data_type[0].lower()+data_type[1:]
                    var_search = str(data_type + "__icontains" ) 
                    if data_type == "sample" or data_type == "experiment":
                        var_search = str(data_type+"__in")
                        if data_type == "sample":
                            data_filter = Sample.objects.filter(name__icontains=value)
                        else:
                            data_filter = Experiment.objects.filter(name__icontains=value)
                        data_id_list = []
                        for i in data_filter:
                            data_id_list.append(i.id)
                        search_value = data_id_list
                    query = myoperator(query, Q(**{var_search : search_value}))
                    
        return GenotypingID.objects.select_related('sample','experiment').prefetch_related('projects').filter(query).distinct()
    
    def viewer_refine_search(self, or_and, dico_get):
        """
          Search in all GenotypingID according to some parameters
          specific for dataview.

          :param string or_and : link between parameters (or, and)
          :param dict dico_get : data to search for
        """
        # Managers are called in genotyping.models
        # To avoid crossed imports we use apps to get objects from the model
        GenotypingID = apps.get_model('genotyping','genotypingid')
        Sample = apps.get_model('genotyping','sample')
        Seedlot = apps.get_model('lot','seedlot')
        Accession = apps.get_model('accession','accession')
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
            
        proj=[]
        query=Q()
        list_attr=[]
        for dat, dico_type_data in dico_get.items():
            for data_type, value in dico_type_data.items():
                var_search = str( data_type + "__icontains" )
                
                if "genotyping id" in dico_type_data:
                    query = myoperator(query, Q(name__icontains=dico_type_data['genotyping id']))
                
                elif "sample" in dico_type_data :
                    spls = Sample.objects.filter(name__icontains=dico_type_data['sample'])
                    query = myoperator(query, Q(sample__in=spls))
                    
                elif "seedlot" in dico_type_data :
                    sls = Seedlot.objects.filter(name__icontains=dico_type_data['seedlot'])
                    query = myoperator(query, Q(sample__seedlot__in=sls))
                
                elif "accession" in dico_type_data:
                    accs = Accession.objects.filter(name__icontains=dico_type_data['accession'])
                    query = myoperator(query, Q(sample__seedlot__accession__in=accs))
                
                elif "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        query = myoperator(query, Q(projects__isnull=True))
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query = myoperator(query, Q(projects__in=proj))
                    
                else:
                    query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))
        
        return GenotypingID.objects.select_related('sample','sample__seedlot','sample__seedlot__accession').prefetch_related('projects').filter(query).distinct()


class LocusManager(models.Manager):
    """
      Manager for Locus model.
    """
    def refine_search(self, or_and, dico_get, locus_type):
        """
          Search in all locus according to some parameters

          :param string or_and : link between parameters (or, and)
          :param dict dico_get : data to search for
        """
        # Managers are called in genotyping.models
        # To avoid crossed imports we use apps to get objects from the model
        Locus = apps.get_model('genotyping','locus')
        LocusTypeAttribute = apps.get_model('genotyping', 'locustypeattribute')
        LocusPosition = apps.get_model('genotyping', 'locusposition')
        Project = apps.get_model('team', 'project')
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
        
        proj=[]
        query=Q()
        list_attr=[]

        for dat, dico_type_data in dico_get.items():
            for data_type, value in dico_type_data.items():
                
                if "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        query = myoperator(query, Q(projects__isnull=True))
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query = myoperator(query, Q(locus__projects__in=proj))
                
                elif "genome version" in dico_type_data:
                    position_id=LocusPosition.objects.filter(genome_version__icontains=dico_type_data['genome version'])
                    query = myoperator(query, Q(id__in=position_id))
                
                elif "chromosome" in dico_type_data:
                    position_id=LocusPosition.objects.filter(chromosome__icontains=dico_type_data['chromosome'])
                    query = myoperator(query, Q(id__in=position_id))
                    
                elif "position" in dico_type_data:
                    position_id=LocusPosition.objects.filter(position__icontains=dico_type_data['position'])
                    query = myoperator(query, Q(id__in=position_id))
                   
                else:
                    for data_type, value in dico_type_data.items():
                        list_id_in=[]
                        for i in LocusTypeAttribute.objects.filter(locus_type=locus_type):
                            if str(data_type) == str(i):
                                list_attr.append(str(i))
                        if data_type in list_attr:
                            id_attribute = str(LocusTypeAttribute.objects.get(name=data_type, locus_type=locus_type).id)
                            for i in Locus.objects.filter(type=locus_type):
                                try:
                                    if id_attribute in i.attributes_values.keys():
                                        if value in ast.literal_eval(i.attributes_values)[id_attribute]:
                                            list_id_in.append(i.id)
                                except:
                                    try:
                                        if id_attribute in i.attributes_values.keys():
                                            if value in i.attributes_values[id_attribute]:
                                                list_id_in.append(i.id)
                                    except:
                                        i.attributes_values = ast.literal_eval(i.attributes_values)
                                        if id_attribute in i.attributes_values.keys():
                                            if value in i.attributes_values[id_attribute]:
                                                list_id_in.append(i.id)
        
                            query = myoperator(query, Q(locus__in = list_id_in))
                
                        else:
                            var_search = str( "locus__" + data_type + "__icontains" )
                            query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))

#         return Locus.objects.filter(query).distinct()
    
        return LocusPosition.objects.select_related('locus', 'genome_version').filter(query).order_by('locus__name')





