from rest_framework import serializers
from genotyping.models import GenomeVersion, LocusPosition, Sample

class MapsBrAPISerializer(serializers.ModelSerializer):
    
    commonCropName = serializers.CharField(source = 'getCommonCropName')
    mapDbId = serializers.CharField(source = 'id')
    type = serializers.CharField(source = 'getBlankValue')
    additionalInfo = serializers.JSONField(source = 'getAdditionalInfo')
    comments = serializers.CharField(source = 'description')
    documentationURL = serializers.CharField(source = 'getDocumentationURL')
    linkageGroupCount = serializers.IntegerField(source = 'get0Value')
    mapName = serializers.CharField(source = 'name')
    mapPUI = serializers.CharField(source = 'getBlankValue')
    markerCount = serializers.IntegerField(source = 'get0Value')
    publishedDate = serializers.CharField(source = 'getBlankDateValue')
    scientificName = serializers.CharField(source = 'getBlankValue')
    unit = serializers.CharField(source = 'getBlankValue')
    
    class Meta:
        model = GenomeVersion
        fields = ['commonCropName',
                  'mapDbId',
                  'type',
                  'additionalInfo',
                  'comments',
                  'documentationURL',
                  'linkageGroupCount',
                  'mapName',
                  'mapPUI',
                  'markerCount',
                  'publishedDate',
                  'scientificName',
                  'unit',
            ]
        depth = 1
        
class MarkerPositionsBrAPISerializer(serializers.ModelSerializer):
    
    additionalInfo = serializers.JSONField(source = 'getAdditionalInfo')
    linkageGroupCount = serializers.CharField(source = 'getBlankValue')
    mapDbId = serializers.CharField(source = 'getMapId')
    mapName = serializers.CharField(source = 'getMapName')
    position = serializers.IntegerField(source = 'position')
    variantDbId = serializers.CharField(source = 'getId')
    variantName = serializers.CharField(source = 'getName')
    
    class Meta:
        model = LocusPosition
        fields = ['additionalInfo',
                  'linkageGroupCount',
                  'mapDbId',
                  'mapName',
                  'position',
                  'variantDbId',
                  'variantName',
            ]
        depth = 1
        
class SamplesBrAPISerializer(serializers.ModelSerializer):
    
    sampleName = serializers.CharField(source = 'name')
    additionalInfo = serializers.JSONField(source = 'getSampleAdditionalInfo')
    column = serializers.IntegerField(source = 'getBlankValue')
    externalReferences = serializers.ListField(source = 'getExternalReferences')
    germplasmDbId = serializers.CharField(source = 'getGermplasmDbId')
    observationUnitDbId = serializers.CharField(source = 'getBlankValue')
    plateDbId = serializers.CharField(source = 'getBlankValue')
    plateName = serializers.CharField(source = 'getBlankValue')
    programDbId = serializers.CharField(source = 'getBlankValue')
    row = serializers.CharField(source = 'getBlankValue')
    sampleBarcode = serializers.CharField(source = 'codelabo')
    sampleDescription = serializers.CharField(source = 'description')
    sampleGroupDbId = serializers.CharField(source = 'id')
    samplePUI = serializers.CharField(source = 'getBlankValue')
    sampleTimestamp = serializers.CharField(source = 'getBlankDateValue')
    sampleType = serializers.CharField(source = 'getBlankValue')
    studyDbId = serializers.CharField(source = 'getBlankValue')
    takenBy = serializers.CharField(source = 'getBlankValue')
    tissueType = serializers.CharField(source = 'getBlankValue')
    trialDbId = serializers.CharField(source = 'getBlankValue')
    well = serializers.CharField(source = 'getBlankValue')
    
    
    class Meta:
        model = Sample
        fields = ['sampleName',
                  'additionalInfo',
                  'column',
                  'externalReferences',
                  'germplasmDbId',
                  'observationUnitDbId',
                  'plateDbId',
                  'plateName',
                  'programDbId',
                  'row',
                  'sampleBarcode',
                  'sampleDescription',
                  'sampleGroupDbId',
                  'samplePUI',
                  'sampleTimestamp',
                  'sampleType',
                  'studyDbId',
                  'takenBy',
                  'tissueType',
                  'trialDbId',
                  'well',
            ]
        depth = 1    