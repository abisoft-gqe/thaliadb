# Generated by Django 3.2 on 2023-12-19 08:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('genotyping', '0044_alter_genotypingvalue_frequency'),
    ]

    operations = [
        migrations.AlterField(
            model_name='locusposition',
            name='chromosome',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddIndex(
            model_name='locusposition',
            index=models.Index(fields=['chromosome', 'position'], name='genotyping__chromos_3b0b6f_idx'),
        ),
    ]
