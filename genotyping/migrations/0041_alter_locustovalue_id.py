# Generated by Django 3.2 on 2023-07-11 12:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('genotyping', '0040_merge_20230221_1102'),
    ]

    operations = [
        migrations.AlterField(
            model_name='locustovalue',
            name='id',
            field=models.BigAutoField(primary_key=True, serialize=False),
        ),
    ]
