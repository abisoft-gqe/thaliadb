# Generated by Django 2.2 on 2022-02-09 10:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('genotyping', '0027_auto_20211029_1246'),
    ]

    operations = [
        migrations.CreateModel(
            name='GenomeVersion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, unique=True)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='locusposition',
            unique_together={('locus', 'genome_version')},
        ),
    ]
