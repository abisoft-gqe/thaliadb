# Generated by Django 2.2 on 2021-10-29 10:46

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('genotyping', '0026_locus_locusposition'),
    ]

    operations = [
        migrations.AlterField(
            model_name='locus',
            name='attributes_values',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='locus',
            name='matrix_genotyping',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='locus',
            name='vcf_genotyping',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True),
        ),
    ]
