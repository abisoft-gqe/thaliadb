# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-26 08:54
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('genotyping', '0004_auto_20160726_0915'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='genotypingid',
            name='experiment',
        ),
        migrations.AddField(
            model_name='genotypingid',
            name='experiment',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='genotyping.Experiment'),
        ),
        migrations.RemoveField(
            model_name='genotypingid',
            name='referential',
        ),
        migrations.AddField(
            model_name='genotypingid',
            name='referential',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='genotyping.Referential'),
        ),
    ]
