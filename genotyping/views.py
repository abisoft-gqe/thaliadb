# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import allel
import io
import datetime
import time
import json
import csv
from _csv import Dialect
import os
import io
import sys
import re
import datetime
import subprocess
import uuid
import pandas as pd
import numpy as np
import traceback

from asgiref.sync import async_to_sync, sync_to_async
from dal import autocomplete
from collections import Counter
from threading import Thread

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, user_passes_test
from django.db import transaction
from django.contrib import messages
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q as Query_dj
from django.conf import settings
from django.db import IntegrityError
from django.db.models import Count
from django.utils import timezone

from accession.models import Accession
from commonfct.constants import filerenderer_headers, ACCEPTED_TRUE_VALUES, ACCEPTED_FALSE_VALUES, standard_allele
from commonfct.filerenderer import filerenderer
from commonfct.search_csvfile import RefineSearchFactory
from commonfct.genericviews import generic_management
from commonfct.utils import get_fields, execute_function_in_thread, bytestomegabytes
from csv_io.views import CSVFactoryForTempFiles, CSVFactoryForMatrix
from lot.models import Seedlot
from genotyping.models import Experiment, Referential, LocusType, LocusTypeAttribute, LocusAttributePosition, Locus, Sample,\
                              GenotypingID, LocusPosition, GenomeVersion, GenotypingValue, LocusToValue, CodeAllele
from genotyping.forms import UploadFileForm_Matrix, PositionForm, ExperimentForm, ReferentialForm, LocusTypeForm, LocusTypeAttForm,\
                             LocusDataForm,SampleForm, LocusDataFileForm, UploadGenoTypValueFileForm, ViewGenotypingValuesForm,\
                             DropGenoValuesForm, UploadFileForm, ChoiceRefExpForm, ChoiceIndOrFreq, GenotypingIDForm, GenomeVersionForm,\
                             LocusPositionUploadForm, InsertGenotypicValuesForm, LocusTypeAttributeSearch, UploadFileFormWithoutDelimiter

from dataview.forms import PhenoViewerAccessionForm, PhenoViewerTraitForm, PhenoViewerEnvironmentForm,GenoViewerAccessionForm,\
                           GenoViewerLocusForm, GenoViewerExperimentForm,DataviewGenotypingValuesForm, GenoViewerReferentialForm,\
                           GenoViewerChrForm, UploadLocusFileForm
from dataview.views import display_genoviewer_form, check_genoviewer_form_data, process_genoviewer_form_data
from team.models import Project, Institution, Person, ThreadReport



@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def genotypinghome(request):
    return render(request,'genotyping/genotyping_base.html',{"admin":True,})

@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def experiment_management(request):
    number_all = len(Experiment.objects.all())
    
    list_attributes = ['Name','Institution','Person','Date','Comments','Projects']

    adds= {"list_attributes":list_attributes, "number_all":number_all,"admin":True,
           "noheader":True,'excel_empty_file':True,'filetype':'experiment','typeid':0, "refine_search":True}
    
    if 'or_and' in request.GET and "no_search" not in request.GET:
        dico_get={}
        or_and = None
        or_and=request.GET['or_and']
        length_dico_get = len(dico_get)
        for i in range(1,11):
            data="data"+str(i)
            text_data="text_data"+str(i)
            if data in request.GET and text_data in request.GET:
                dico_get[data]={request.GET[data]:request.GET[text_data]}
        
        if or_and == "or":       
            query=Query_dj()
            list_name_in=[]
            list_attr=[]
            for dat, dico_type_data in dico_get.items():
                if "Projects" in dico_type_data:
                    proj=(Project.objects.filter(name__icontains=dico_type_data['Projects']))
                    query |= Query_dj(projects__in=proj)
                else:
                    for data_type, value in dico_type_data.items():
                        search_value = dico_type_data[data_type]
                        data_type = data_type[0].lower()+data_type[1:]
                        var_search = str(data_type + "__icontains" ) 
                        if data_type == "person" or data_type == "institution":
                            var_search = str(data_type+"__in")
                            if data_type == "person":
                                data_filter = Person.objects.filter(Query_dj(first_name__icontains=value)|Query_dj(last_name__icontains=value))
                            else:
                                data_filter = Institution.objects.filter(name__icontains=value)
                            data_id_list = []
                            for i in data_filter:
                                data_id_list.append(i.id)
                            search_value = data_id_list
                        query |= Query_dj(**{var_search : search_value})
        elif or_and == "and":       
            query=Query_dj()
            list_name_in=[]
            list_attr=[]
            for dat, dico_type_data in dico_get.items():
                if "Projects" in dico_type_data:
                    proj=(Project.objects.filter(name__icontains=dico_type_data['Projects']))
                    query &= Query_dj(projects__in=proj)
                else:
                    for data_type, value in dico_type_data.items():
                        search_value = dico_type_data[data_type]
                        data_type = data_type[0].lower()+data_type[1:]
                        var_search = str(data_type + "__icontains" ) 
                        if data_type == "person" or data_type == "institution":
                            var_search = str(data_type+"__in")
                            if data_type == "person":
                                data_filter = Person.objects.filter(Query_dj(first_name__icontains=value)|Query_dj(last_name__icontains=value))
                            else:
                                data_filter = Institution.objects.filter(name__icontains=value)
                            data_id_list = []
                            for i in data_filter:
                                data_id_list.append(i.id)
                            search_value = data_id_list
                        query &= Query_dj(**{var_search : search_value})
        experiments = Experiment.objects.filter(query).distinct()
        number_all = len(experiments)
        nb_per_page = 10
        adds.update({"query":query,"number_all":number_all,"all":experiments,'search_result':dico_get,})
    return generic_management(Experiment, ExperimentForm, request, 'genotyping/genotyping_generic.html',
                              adds, "Experiment Management")


@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def sample_management(request):
    register_formFile = UploadFileFormWithoutDelimiter()
    form = SampleForm()
    names, labels = get_fields(form) 
    template = 'genotyping/genotyping_generic.html'
    title = ' Sample Management'
    list_attributes = ['name','seedlot','description','is_bulk','is_obsolete','codelabo','tubename','projects']
    tag_fields = {'title':title,
                  'fields_name':names,
                  'fields_label':labels,
                  "admin":True,
                  "refine_search":True,
                  'list_attributes':list_attributes}
    
    if request.method == "GET":
        samples, number_all, nb_per_page = _get_samples(request)
        dico_get={}
        or_and = None
        
        if "no_search" in request.GET:
            return render(request, template, {'formfile':register_formFile,
                                              'filetype':'sample',
                                              'form':form,
                                              'creation_mode':True,
                                              "number_all":number_all,
                                              'all':samples})
        
        if 'or_and' in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
                    
            nb_per_page = 10
            
            samples = Sample.objects.refine_search(or_and, dico_get)
            number_all = len(samples)
            
            tag_fields.update({'search_result':dico_get})
            
        tag_fields.update({'all':samples,
                           "number_all":number_all,
                           "nb_per_page":nb_per_page,
                           'formfile':register_formFile,
                           'form':form,
                           'creation_mode':True,
                           'excel_empty_file':True,
                           'filetype':'sample',
                           'typeid': 0})
        return render(request, template, tag_fields)

    # modif de la bdd
    elif request.method == "POST":      
        formf = UploadFileFormWithoutDelimiter(request.POST, request.FILES)
        
        if formf.is_valid():
            upload_sample_file(request, request.FILES['file'])
            samples, number_all, nb_per_page = _get_samples(request)
            
            tag_fields.update({'form':SampleForm(),
                               'formfile':register_formFile,
                               'creation_mode':True,
                               'filetype':'sample',
                               "number_all":number_all,
                               "nb_per_page":nb_per_page,
                               'all':samples})
            
            return render(request, template, tag_fields)
        else:
            samples, number_all, nb_per_page = _get_samples(request)
            messages.add_message(request, messages.ERROR, '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form.errors.items() ])) 
            return generic_management(Sample, SampleForm, request, 'genotyping/genotyping_generic.html',{"refine_search":True,"admin":True,"number_all":number_all,
                           "nb_per_page":nb_per_page,"list_attributes":list_attributes,'formfile':register_formFile}, "Sample Management")       
    else:
        samples, number_all, nb_per_page = _get_samples(request)   
        return generic_management(Sample, SampleForm, request, 'genotyping/genotyping_generic.html',{"refine_search":True,"admin":True,'formfile':register_formFile,
                            "list_attributes":list_attributes, "number_all":number_all,"nb_per_page":nb_per_page,}, "Sample Management")


def upload_sample_file(request, file):
    """
      File reading in order to insert or update data.
    """
    myfactory = CSVFactoryForTempFiles(file)
    myfactory.find_dialect()
    myfactory.read_file()
    file_dict = myfactory.get_file()
    
    headers_list = filerenderer_headers['Sample']
    
    missing = myfactory.check_headers_conformity(headers_list)
    
    if missing != []:
        messages.add_message(request, messages.ERROR, "One or several mandatory headers are missing in your file : "+format(', '.join(missing))+\
                             ". The required headers for this file are: "+format(', '.join(headers_list))+".")
        return
    
    if request.POST.get("submit"): #Cas d'une insertion de nouvelles données
        submit_upload_sample_file(request, file_dict)
        
    elif request.POST.get("update"):
        update_upload_sample_file(request, file_dict)
        
    else:
        messages.add_message(request, messages.ERROR,'ThaliaDB was unable to identify if you\'re trying to insert new data or to update former data.')


def submit_upload_sample_file(request, file_dict):
    """
      Insertion of sample data from the file.
    """
    with transaction.atomic():
        for line in file_dict:
            try:
                with transaction.atomic():
                    
                    seedlot = Seedlot.objects.get(name=str(line['Seedlot']))
                    
                    if line["Is obsolete"] in ACCEPTED_TRUE_VALUES:
                        is_obsolete = True 
                    elif line["Is obsolete"] in ACCEPTED_FALSE_VALUES:
                        is_obsolete = False
                    
                    s = Sample.objects.create(name=str(line['Name']),
                                              seedlot=seedlot,
                                              description=str(line['Description']),
                                              is_obsolete=is_obsolete,
                                              codelabo=str(line['Codelabo']),
                                              tubename=str(line['Tubename']))
                    s.save()
                    
                    project_list = [i.strip() for i in str(line['Projects']).split('|')]
                    projects = Project.objects.filter(name__in = project_list)
                    if projects.exists():
                        s.projects.add(*list(projects))
            
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+").")
                continue
            
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return
            
    messages.add_message(request, messages.SUCCESS, "The file has been successfully inserted.")


def update_upload_sample_file(request, file_dict):
    """
      Update of sample data from the file.
    """
    with transaction.atomic():
        for line in file_dict:
            try:
                with transaction.atomic():
                    sample_to_update = Sample.objects.get(name=str(line['Name']))
                    
                    seedlot = Seedlot.objects.get(name=str(line['Seedlot']))
                    
                    sample_to_update.seedlot = seedlot
                    sample_to_update.description = str(line['Description'])
                    
                    if line["Is obsolete"] in ACCEPTED_TRUE_VALUES:
                        sample_to_update.is_obsolete = True 
                    elif line["Is obsolete"] in ACCEPTED_FALSE_VALUES:
                        sample_to_update.is_obsolete = False
                    
                    sample_to_update.codelabo = str(line['Codelabo'])
                    sample_to_update.tubename = str(line['Tubename'])
                    
                    if line['Projects']:
                        project_list = [p.strip() for p in str(line['Projects']).split('|')]
                        projects = Project.objects.filter(name__in = project_list)
                        if len(project_list) != len(projects):
                            messages.add_message(request, messages.ERROR, "There is an error with a project in this list: "+ ', '.join(project_list)+" (As a reminder, the separator must be | ).")
                            raise Exception
                        sample_to_update.projects.clear()
                        sample_to_update.projects.add(*list(projects))
                        
                    sample_to_update.save()
                        
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+").")
                    continue
                
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully uploaded and the samples have been updated.")
                        

#==========================GENOTYPING ID=============================
def _get_genotypingid(request):
    genotypingids = GenotypingID.objects.select_related('sample','experiment').prefetch_related('projects').all()
    try:
        if request.method == "GET":
            nb_per_page=request.GET['nb_per_page']  
        elif request.method == "POST":
            nb_per_page=request.POST["nb_per_page"]   
        if nb_per_page=="all":
            nb_per_page = len(genotypingids)
    except:
        nb_per_page=50
        
    paginator = Paginator(genotypingids, nb_per_page)
    page = request.GET.get('page')
    try:
        genotypingids = paginator.page(page)
    except PageNotAnInteger:
        genotypingids = paginator.page(1)
    except EmptyPage:
        genotypingids = paginator.page(paginator.num_pages)
    number_all = (paginator.num_pages-1)*int(nb_per_page) + len(paginator.page(paginator.num_pages))
    return genotypingids, number_all, nb_per_page


@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def genotypingid_management(request):
    register_formFile = UploadFileFormWithoutDelimiter()
    form = GenotypingIDForm()
    names, labels = get_fields(form)
    
    template = 'genotyping/genotyping_generic.html'
    title = 'GenotypingID Management'

    list_attributes = ['name','sample','experiment','sentrixbarcode_a','sentrixposition_a','funding','projects']
    tag_fields = {'title':title,
                  'fields_name':names,
                  'fields_label': labels,
                  "admin":True,
                  "refine_search":True,
                  "list_attributes":list_attributes,
                  }
    if request.method == 'GET':
        genotypingids, number_all, nb_per_page = _get_genotypingid(request)
        dico_get={}
        or_and = None
        
        if "no_search" in request.GET:
            return render(request, template, {'formfile':register_formFile,
                                              'filetype':'genotypingid',
                                              'form':form,
                                              'creation_mode':True,
                                              "number_all":number_all,
                                              'all':genotypingids})
        
        if 'or_and' in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
            
            nb_per_page = 10
            
            genotypingids = GenotypingID.objects.refine_search(or_and, dico_get)
            number_all = len(genotypingids)
            
            tag_fields.update({'search_result':dico_get})
            
        tag_fields.update({'all':genotypingids,
                           'formfile':register_formFile,
                           'form':form,
                           'creation_mode':True,
                           "number_all":number_all,
                           "nb_per_page":nb_per_page,
                           'excel_empty_file':True,
                           'filetype':'genotypingid',
                           'typeid':0,})
        return render(request, template, tag_fields)

    elif request.method == 'POST':
        formf = UploadFileFormWithoutDelimiter(request.POST, request.FILES)
        
        if formf.is_valid():
            upload_genoid_file(request, request.FILES['file'])
            genotypingids, number_all, nb_per_page = _get_genotypingid(request)
            
            tag_fields.update({'form':GenotypingIDForm(),
                               'formfile':register_formFile,
                               'creation_mode':True,
                               'filetype':'genotypingid',
                               "number_all":number_all,
                               "nb_per_page":nb_per_page,
                               'all':genotypingids})
            
            return render(request, template, tag_fields)

        else:
            messages.add_message(request, messages.ERROR, '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form.errors.items() ]))
            return generic_management(GenotypingID, GenotypingIDForm, request, 'genotyping/genotyping_generic.html',{"refine_search":True,"admin":True,
                                    "list_attributes":list_attributes,'formfile':register_formFile}, "GenotypingID Management")       

    else:
        genotypingids, number_all, nb_per_page = _get_genotypingid(request)
        return generic_management(GenotypingID, GenotypingIDForm, request, 'genotyping/genotyping_generic.html',{"refine_search":True,"admin":True,
                                    "list_attributes":list_attributes,'formfile':register_formFile}, "GenotypingID Management")       


def upload_genoid_file(request, file):
    """
      File reading in order to insert or update data.
    """
    myfactory = CSVFactoryForTempFiles(file)
    myfactory.find_dialect()
    myfactory.read_file()
    file_dict = myfactory.get_file()
    
    headers_list = filerenderer_headers['GenotypingID']
    
    missing = myfactory.check_headers_conformity(headers_list)
    
    if missing != []:
        messages.add_message(request, messages.ERROR, "One or several mandatory headers are missing in your file : "+format(', '.join(missing))+\
                             ". The required headers for this file are: "+format(', '.join(headers_list))+".")
        return
    
    if request.POST.get("submit"): #Cas d'une insertion de nouvelles données
        submit_upload_genoid_file(request, file_dict)
        
    elif request.POST.get("update"):
        update_upload_genoid_file(request, file_dict)
        
    else:
        messages.add_message(request, messages.ERROR,'ThaliaDB was unable to identify if you\'re trying to insert new data or to update former data.')


def submit_upload_genoid_file(request, file_dict):
    """
      Insertion of genotyping_id data from the file.
    """
    with transaction.atomic():
        for line in file_dict:
            try:
                with transaction.atomic():
                    
                    sample = Sample.objects.get(name=str(line['Sample']))
                    experiment = Experiment.objects.get(name=str(line['Experiment']))
                    
                    g = GenotypingID.objects.create(name=str(line['Name']),
                                                    sample=sample,
                                                    experiment=experiment,
                                                    sentrixbarcode_a=str(line['Sentrixbarcode a']),
                                                    sentrixposition_a=str(line['Sentrixposition a']),
                                                    funding=str(line['Funding']))
                    g.save()
                    
                    project_list = [i.strip() for i in str(line['Projects']).split('|')]
                    projects = Project.objects.filter(name__in = project_list)
                    if projects.exists():
                        g.projects.add(*list(projects))
                        
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+").")
                continue
            
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return
            
    messages.add_message(request, messages.SUCCESS, "The file has been successfully inserted.")


def update_upload_genoid_file(request, file_dict):
    """
      Update of genotyping_id data from the file.
    """
    with transaction.atomic():
        for line in file_dict:
            try:
                with transaction.atomic():
                    genoid_to_update = GenotypingID.objects.get(name=str(line['Name']))
                    
                    sample = Sample.objects.get(name=str(line['Sample']))
                    experiment = Experiment.objects.get(name=str(line['Experiment']))
                    
                    genoid_to_update.sample = sample
                    genoid_to_update.experiment = experiment
                    
                    genoid_to_update.sentrixbarcode_a = str(line['Sentrixbarcode a'])
                    genoid_to_update.sentrixposition_a = str(line['Sentrixposition a'])
                    genoid_to_update.funding = str(line['Funding'])
                    
                    if line['Projects']:
                        project_list = [p.strip() for p in str(line['Projects']).split('|')]
                        projects = Project.objects.filter(name__in = project_list)
                        if len(project_list) != len(projects):
                            messages.add_message(request, messages.ERROR, "There is an error with a project in this list: "+ ', '.join(project_list)+" (As a reminder, the separator must be | ).")
                            raise Exception
                        genoid_to_update.projects.clear()
                        genoid_to_update.projects.add(*list(projects))
                        
                    genoid_to_update.save()
                    
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+").")
                    continue
                
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully uploaded and the genotyping_ids have been updated.")
                    

@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def referential_management(request):
    number_all = len(Referential.objects.all())
    list_attributes = ['Name','Person','Date','Comments','Projects']
    adds= {"refine_search":True, "list_attributes":list_attributes, "number_all":number_all,"admin":True,"noheader":True,'excel_empty_file':True,'filetype':'referential','typeid':0}
    if 'or_and' in request.GET and 'no_search' not in request.GET:
        dico_get={}
        or_and = None
        or_and=request.GET['or_and']
        length_dico_get = len(dico_get)
        for i in range(1,11):
            data="data"+str(i)
            text_data="text_data"+str(i)
            if data in request.GET and text_data in request.GET:
                dico_get[data]={request.GET[data]:request.GET[text_data]}
        
        if or_and == "or":       
            query=Query_dj()
            list_name_in=[]
            list_attr=[]
            for dat, dico_type_data in dico_get.items():
                if "Projects" in dico_type_data:
                    proj=(Project.objects.filter(name__icontains=dico_type_data['Projects']))
                    query |= Query_dj(projects__in=proj)
                else:
                    for data_type, value in dico_type_data.items():
                        search_value = dico_type_data[data_type]
                        data_type = data_type[0].lower()+data_type[1:]
                        var_search = str(data_type + "__icontains" ) 
                        if data_type == "person":
                            var_search = str(data_type+"__in")
                            person_filter = Person.objects.filter(Query_dj(first_name__icontains=value)|Query_dj(last_name__icontains=value))
                            person_id_list = []
                            for i in person_filter:
                                person_id_list.append(i.id)
                            search_value = person_id_list
                        query |= Query_dj(**{var_search : search_value})
        elif or_and == "and":       
            query=Query_dj()
            list_name_in=[]
            list_attr=[]
            for dat, dico_type_data in dico_get.items():
                if "Projects" in dico_type_data:
                    proj=(Project.objects.filter(name__icontains=dico_type_data['Projects']))
                    query &= Query_dj(projects__in=proj)
                else:
                    for data_type, value in dico_type_data.items():
                        search_value = dico_type_data[data_type]
                        data_type = data_type[0].lower()+data_type[1:]
                        var_search = str(data_type + "__icontains" ) 
                        if data_type == "person":
                            var_search = str(data_type+"__in")
                            person_filter = Person.objects.filter(Query_dj(first_name__icontains=value)|Query_dj(last_name__icontains=value))
                            person_id_list = []
                            for i in person_filter:
                                person_id_list.append(i.id)
                            search_value = person_id_list
                        query &= Query_dj(**{var_search : search_value})
        referentials = Referential.objects.filter(query).distinct()
        number_all = len(referentials)
        nb_per_page = 10
        adds.update({"query":query,"number_all":number_all,"all":referentials,'search_result':dico_get,})
    return generic_management(Referential, ReferentialForm, request, 'genotyping/genotyping_generic.html', adds, "Referential Management")


@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def insert_genotypic_values(request):
    """
      View displaying a form to insert genotyping data files.
      :var str title: title displayed on the template
      :var str template: html template for the rendering of the view
      :var form form_class: the file submission form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var file file: file uploaded by the user in the form
      :var dict file_dict: dict version of the file, parsed by the CSVFactory
    """
    title = "Insert Genotypic Values"
    template = 'genotyping/insert_genotypic_values.html'
    form_class = InsertGenotypicValuesForm
    
    if request.method == 'GET':
        form = form_class()
        
        tag_fields = {'title':title,
                      'filetype':"Flat_matrix",
                      'form':form,
                      'admin':True}
        
        return render(request, template, tag_fields) 
    
    elif request.method == 'POST':
        print("test")
        form = form_class(request.POST, request.FILES)
    
        tag_fields = {'title':title,
                      'filetype':"Flat_matrix",
                      'form':form,
                      'admin':True}
        
        if form.is_valid():
            file = request.FILES['file']
            referential_name = form.cleaned_data['referential']
#             print(referential_name)
            
            if form.cleaned_data['file_format'] == "1" or form.cleaned_data['file_format'] == "2" or form.cleaned_data['file_format'] == "3":
                orientation = request.POST.get('matrix_orientation')
#                 print(orientation)
                
                myfactory = CSVFactoryForMatrix(file)
                myfactory.find_dialect()
                myfactory.read_file()
                file_dict = myfactory.get_file()
                
                label = myfactory.get_first_label()
                
                if not label:
                    messages.add_message(request, messages.ERROR, "The fist column label is missing in your file.")
                    return redirect('insert_genotypic_values')
                
                if form.cleaned_data['file_format'] == "1":
                    print("IUPAC")
                    insert_iupac_matrix_genotyping_values(request, file_dict, label, referential_name, orientation)
                
                elif form.cleaned_data['file_format'] == "2":
                    print("2 letters")
                    missing_value = request.POST.get('missing_value')
                    
                    #Mandatory missing value field
                    if not missing_value:
                        messages.add_message(request, messages.ERROR, "The missing value was not specified.")
                        return redirect('insert_genotypic_values')
                    
                    if missing_value in ['A','T','G','C']:
                        messages.add_message(request, messages.ERROR, "The missing value cannot be equal to A, T, G or C.")
                        return redirect('insert_genotypic_values')
                    
                    thread_name = uuid.uuid4().hex
                    tr = ThreadReport.objects.create(user=request.user,
                                                     message="Your file: "+ file.name,
                                                     thread=thread_name,
                                                     date=timezone.now(),
                                                     subject=1)
                    
                    thread_name = execute_function_in_thread(insert_2_letters_matrix_genotyping_values, [request, file_dict, label, referential_name, orientation, missing_value, tr], {}, thread_name)
                    #insert_2_letters_matrix_genotyping_values(request, file_dict, label, referential_name, orientation, missing_value)
                    
                    messages.add_message(request, messages.SUCCESS, "Your file has been successfully received.")
                
                
                elif form.cleaned_data['file_format'] == "3":
                    print("slash matrix")
                    missing_value = request.POST.get('missing_value')
                    
                    #Mandatory missing value field
                    if not missing_value:
                        messages.add_message(request, messages.ERROR, "The missing value was not specified.")
                        return redirect('insert_genotypic_values')
                    
                    if missing_value in ['A','T','G','C']:
                        messages.add_message(request, messages.ERROR, "The missing value cannot be equal to A, T, G or C.")
                        return redirect('insert_genotypic_values')
                    
                    thread_name = uuid.uuid4().hex
                    tr = ThreadReport.objects.create(user=request.user,
                                                     message="Your file: "+ file.name,
                                                     thread=thread_name,
                                                     date=timezone.now(),
                                                     subject=1)
                    
                    thread_name = execute_function_in_thread(insert_slash_matrix_genotyping_values, [request, file_dict, label, referential_name, orientation, missing_value, tr], {}, thread_name)
                    #insert_slash_matrix_genotyping_values(request, file_dict, label, referential_name, orientation, missing_value)
                
                    messages.add_message(request, messages.SUCCESS, "Your file has been successfully received.")
                
                return render(request, template, tag_fields)
                
            elif form.cleaned_data['file_format'] == "4":
                print("Flat matrix")
                print(file.charset)
                myfactory = CSVFactoryForTempFiles(file)
                myfactory.find_dialect()
                myfactory.read_file()
                file_dict = myfactory.get_file()
                
                headers_list = filerenderer_headers['Flat_matrix']
                
                missing = myfactory.check_headers_conformity(headers_list)
                
                if missing != []:
                    messages.add_message(request, messages.ERROR, "One or several mandatory headers are missing in your file : "+format(', '.join(missing))+\
                                         ". The required headers for this file are: "+format(', '.join(headers_list))+".")
                    return redirect('insert_genotypic_values')
                
                missing_value = request.POST.get('missing_value')
                frequency_test = request.POST.get('frequency_test')
                
                #Mandatory missing value field
                if not missing_value:
                    messages.add_message(request, messages.ERROR, "The missing value was not specified.")
                    return redirect('insert_genotypic_values')
                
                if missing_value in ['A','T','G','C']:
                    messages.add_message(request, messages.ERROR, "The missing value cannot be equal to A, T, G or C.")
                    return redirect('insert_genotypic_values')
                
                thread_name = uuid.uuid4().hex
                tr = ThreadReport.objects.create(user=request.user,
                                                 message="Your file: "+ file.name,
                                                 thread=thread_name,
                                                 date=timezone.now(),
                                                 subject=1)
                
                thread_name = execute_function_in_thread(insert_flat_file_genotyping_values, [request, file_dict, referential_name, missing_value, frequency_test, tr], {}, thread_name)
                
                messages.add_message(request, messages.SUCCESS, "Your file has been successfully received.")
                
                return render(request, template, tag_fields)
                
                
            elif form.cleaned_data['file_format'] == "5":
                print("VCF")
                insert_vcf_genotyping_values(request, file_dict, referential_name)
                
                return render(request, template, tag_fields)
                
            else:
                messages.add_message(request, messages.ERROR, "An error occurred with your file.")
                return render(request, template, tag_fields)
    
        else:
            messages.add_message(request, messages.ERROR, "An error occurred with your file.")
            return render(request, template, tag_fields)
    
    
def insert_iupac_matrix_genotyping_values(request, file_dict, label, referential_name, orientation):
    print("insert iupac")
    # A faire
    
def insert_2_letters_matrix_genotyping_values(request, file_dict, label, referential_name, orientation, missing_value, report):
    print("insert 2 letters")
    
    # To let the db save the report before updating it (else duplicate key error on .save())
    time.sleep(1)
    
    #Lists used for the report
    missing_geno = []
    missing_loc = []
    
    locus_objs = {}
    genoid_objs = {}
    
    #Dict used to store codeallele created or retrieved
    code_objs = {}
    
    #list of column (this the same at each row)
    cols_list = None
    #Dict used to store genovalues created or retrieved
    genovalues_objs = {}
    
    #To store locustovalue objects until bulk_create
    locus_to_genoval = []
    
    tr_messages = {'success':[],
                   'warning':[],
                   'info':[],
                   'error':[]}
    
    i = -1
    
    try:
        referential = Referential.objects.get(name=referential_name)
    except Exception as e:
        tr_messages['error'].append("The referential is not valid, please try again.")
        add_messages_to_thread_report(tr_messages, report)
        return
    
    with transaction.atomic():
        for line in file_dict:
            i += 1
            try:
                if i%1000 == 0:
                    print(str(i)+" ("+str(datetime.datetime.now())+")", flush=True)
                if len(locus_to_genoval) >= 500000:
                    start = datetime.datetime.now()
                    try:
                        LocusToValue.objects.bulk_create(locus_to_genoval, batch_size=100000)
                    except Exception as e:
                        if str(e) != "":
                            tr_messages['error'].append(str(e)+" (when inserting data).")
                            continue
                     
                    locus_to_genoval = []
                    
                line_name = line[label]
                
                with transaction.atomic():
                    if orientation == '1':
                        # Loci in lines
                        try:
                            locus = Locus.objects.get(name=line_name)
                        except Locus.DoesNotExist:
                            locus = ''
                            if line_name not in missing_loc:
                                missing_loc.append(line_name)
                        
                    elif orientation == '2':
                        # Loci in columns
                        try:
                            genoid = GenotypingID.objects.get(name=line_name)
                        except GenotypingID.DoesNotExist:
                            genoid = ''
                            if line_name not in missing_geno:
                                missing_geno.append(line_name)
                    if not cols_list :
                        del line[label]
                        cols_list = line.keys()
                    for col_name in cols_list:
                        try:
                            #with transaction.atomic():
                            if orientation == '1':
                                # Loci in lines
                                try :
                                    genoid = genoid_objs[col_name]
                                except KeyError :
                                    try:
                                        genoid = GenotypingID.objects.get(name=col_name)
                                        genoid_objs[col_name] = genoid
                                    except GenotypingID.DoesNotExist:
                                        genoid = ''
                                        if col_name not in missing_geno:
                                            missing_geno.append(col_name)
                            
                            elif orientation == '2':
                                # Loci in columns
                                
                                try :
                                    locus = locus_objs[col_name]
                                except KeyError :
                                    try:
                                        locus = Locus.objects.get(name=col_name)
                                        locus_objs[col_name] = locus
                                    except Locus.DoesNotExist:
                                        locus = ''
                                        if col_name not in missing_loc:
                                            missing_loc.append(col_name)                                        
                            if line[col_name].strip() == missing_value:
                                value = 'NA'
                                frequency = 1
                                
                                if genoid and locus:
                                    
                                    if value in code_objs.keys():
                                        allele = code_objs[value]
                                    else:
                                        allele, created = CodeAllele.objects.get_or_create(code_allele=value)
                                        code_objs[value] = allele
                                    
                                    try:
                                        geno_value = genovalues_objs[(genoid,referential,allele,frequency)]
                                    except KeyError :
                                        geno_value, created = GenotypingValue.objects.get_or_create(genotyping_id=genoid,
                                                                                                    referential=referential,
                                                                                                    allele=allele,
                                                                                                    frequency=frequency)
                                        genovalues_objs[(genoid,referential,allele,frequency)] = geno_value
                                    
                                    #loc_val = LocusToValue.objects.create(locus=locus, genotyping_value=geno_value)
                                    locus_to_genoval.append(LocusToValue(locus=locus, genotyping_value=geno_value))
                                
                            else:
                                value = line[col_name].strip()
                                
                                if len(value) != 2 :
                                    raise Exception('The value is not composed of 2 letters: '+str(value)+'.')
                                    
                                if value[0] in standard_allele and value[1] in standard_allele:
                                    
                                    if genoid and locus:
                                        if value[0] == value[1]:
                                            frequency = 1
                                            
                                            if value[0] in code_objs.keys():
                                                allele = code_objs[value[0]]
                                            else:
                                                allele, created = CodeAllele.objects.get_or_create(code_allele=value[0])
                                                code_objs[value[0]] = allele
                                            
                                            try:
                                                geno_value = genovalues_objs[(genoid,referential,allele,frequency)]
                                            except KeyError :
                                                geno_value, created = GenotypingValue.objects.get_or_create(genotyping_id=genoid,
                                                                                                            referential=referential,
                                                                                                            allele=allele,
                                                                                                            frequency=frequency)
                                                genovalues_objs[(genoid,referential,allele,frequency)] = geno_value

                                            locus_to_genoval.append(LocusToValue(locus=locus, genotyping_value=geno_value))
                                            
                                        else:
                                            frequency = 0.5
                                            for val in value:
                                                if val in code_objs.keys():
                                                    allele = code_objs[val]
                                                else:
                                                    allele, created = CodeAllele.objects.get_or_create(code_allele=val)
                                                    code_objs[val] = allele
                                                
                                                try:
                                                    geno_value = genovalues_objs[(genoid,referential,allele,frequency)]
                                                except KeyError :
                                                    geno_value, created = GenotypingValue.objects.get_or_create(genotyping_id=genoid,
                                                                                                                referential=referential,
                                                                                                                allele=allele,
                                                                                                                frequency=frequency)
                                                    genovalues_objs[(genoid,referential,allele,frequency)] = geno_value
                                                            
                                                #loc_val = LocusToValue.objects.create(locus=locus, genotyping_value=geno_value)
                                                locus_to_genoval.append(LocusToValue(locus=locus, genotyping_value=geno_value))
                                        
                                    else:
                                        raise Exception('The genotyping value '+str(value)+' is not a genotype.')
                        except Exception as e:
                            if str(e) != "":
                                tr_messages['error'].append(str(e)+" (at line: "+line_name+", column: "+col_name+").")
                                continue
            
            except Exception as e:
                if str(e) != "":
                    tr_messages['error'].append(str(e)+" (at line: "+line[label]+").")
                    continue
                
        #If some locustovalue objects where not inserted
        if locus_to_genoval:
            #print("final bulk create", flush=True)
            try:
                LocusToValue.objects.bulk_create(locus_to_genoval)
            except Exception as e:
                if str(e) != "":
                    tr_messages['error'].append(str(e)+" (when inserting data).")
             
            locus_to_genoval = []
                
        if missing_geno != []:
            tr_messages['error'].append("The following GenotypingID does not exist in the database: "+', '.join(missing_geno)+".")
            
        if missing_loc != []:
            tr_messages['error'].append("The following Locus does not exist in the database: "+', '.join(missing_loc)+".")
 
        if tr_messages['error'] != []:
            transaction.set_rollback(True)
        else:
            tr_messages['success'].append("The file has been successfully inserted.")
    
    add_messages_to_thread_report(tr_messages, report)
    

def insert_slash_matrix_genotyping_values(request, file_dict, label, referential_name, orientation, missing_value, report):
    print("insert slash")
    
    # To let the db save the report before updating it (else duplicate key error on .save())
    time.sleep(1)
    
    #Lists used for the report
    missing_geno = []
    missing_loc = []
    
    locus_objs = {}
    genoid_objs = {}
    
    #Dict used to store codeallele created or retrieved
    code_objs = {}
    
    #Dict used to store genovalues created or retrieved
    genovalues_objs = {}
    
    #To store locustovalue objects until bulk_create
    locus_to_genoval = []
    
    tr_messages = {'success':[],
                   'warning':[],
                   'info':[],
                   'error':[]}
    
    i = -1
    
    try:
        referential = Referential.objects.get(name=referential_name)
    except Exception as e:
        tr_messages['error'].append("The referential is not valid, please try again.")
        add_messages_to_thread_report(tr_messages, report)
        return
    
    
    with transaction.atomic():
        for line in file_dict:
            i += 1
            try:
                if i%1000 == 0:
                    print(str(i)+" ("+str(datetime.datetime.now())+")", flush=True)
                    
                if len(locus_to_genoval) >= 50000:
                    #print("bulk create", flush=True)
                    try:
                        LocusToValue.objects.bulk_create(locus_to_genoval)
                    except Exception as e:
                        if str(e) != "":
                            tr_messages['error'].append(str(e)+" (when inserting data).")
                            continue
                
                    locus_to_genoval = []
                
                
                line_name = line[label]
            
                with transaction.atomic():
                    if orientation == '1':
                        # Loci in lines
                        try:
                            locus = Locus.objects.get(name=line_name)
                        except Locus.DoesNotExist:
                            locus = ''
                            if line_name not in missing_loc:
                                missing_loc.append(line_name)
                        
                    elif orientation == '2':
                        # Loci in columns
                        try:
                            genoid = GenotypingID.objects.get(name=line_name)
                        except GenotypingID.DoesNotExist:
                            genoid = ''
                            if line_name not in missing_geno:
                                missing_geno.append(line_name)
                        
                    del line[label]
            
                    for col_name in line.keys():
                        try:
                            with transaction.atomic():
                                if orientation == '1':
                                    # Loci in lines
                                    if col_name in genoid_objs.keys():
                                        genoid = genoid_objs[col_name]
                                    
                                    else:
                                        try:
                                            genoid = GenotypingID.objects.get(name=col_name)
                                            genoid_objs[col_name] = genoid
                                            
                                        except GenotypingID.DoesNotExist:
                                            genoid = ''
                                            if col_name not in missing_geno:
                                                missing_geno.append(col_name)
                                
                                elif orientation == '2':
                                    # Loci in columns
                                    if col_name in locus_objs.keys():
                                        locus = locus_objs[col_name]
                                    else:
                                        try:
                                            locus = Locus.objects.get(name=col_name)
                                            locus_objs[col_name] = locus
                                            
                                        except Locus.DoesNotExist:
                                            locus = ''
                                            if col_name not in missing_loc:
                                                missing_loc.append(col_name)
                                                
                                if line[col_name].strip() == missing_value:
                                    value = 'NA'
                                    frequency = 1
                                    
                                    if genoid and locus:
                                        
                                        code_objs, genovalues_objs, locus_to_genoval = save_genovalue_and_locustovalue(value, frequency, genoid, referential, locus, code_objs, genovalues_objs, locus_to_genoval)
                                        
                                        # if value in code_objs.keys():
                                        #     allele = code_objs[value]
                                        # else:
                                        #     allele, created = CodeAllele.objects.get_or_create(code_allele=value)
                                        #     code_objs[value] = allele
                                        #
                                        # if (genoid,referential,allele,frequency) in genovalues_objs.keys():
                                        #     geno_value = genovalues_objs[(genoid,referential,allele,frequency)]
                                        #
                                        # else:
                                        #     geno_value, created = GenotypingValue.objects.get_or_create(genotyping_id=genoid,
                                        #                                                                 referential=referential,
                                        #                                                                 allele=allele,
                                        #                                                                 frequency=frequency)
                                        #
                                        #     genovalues_objs[(genoid,referential,allele,frequency)] = geno_value
                                        #
                                        # locus_to_genoval.append(LocusToValue(locus=locus, genotyping_value=geno_value))
                                
                                else:
                                    value = line[col_name].strip()
                                    
                                    if '/' not in value:
                                        raise Exception('There is no "/" to separate the values : '+str(value)+'.')
                                    
                                    list_values = value.split('/')
                                    print(list_values)
                                    if len(list_values) != 2 or list_values[0] == '' or list_values[1] == '':
                                        raise Exception('There is more or less than two genotyping values in this field: '+str(value)+'.')
                                    
                                    if genoid and locus:
                                        # Deux memes valeurs
                                        if list_values[0] == list_values[1]:
                                            # Les deux valeurs sont missing value
                                            if list_values[0] == missing_value:
                                                value = 'NA'
                                                frequency = 1
                                                
                                                if genoid and locus:
                                                    
                                                    code_objs, genovalues_objs, locus_to_genoval = save_genovalue_and_locustovalue(value, frequency, genoid, referential, locus, code_objs, genovalues_objs, locus_to_genoval)
                                                    
                                                    # if value in code_objs.keys():
                                                    #     allele = code_objs[value]
                                                    # else:
                                                    #     allele, created = CodeAllele.objects.get_or_create(code_allele=value)
                                                    #     code_objs[value] = allele
                                                    #
                                                    # if (genoid,referential,allele,frequency) in genovalues_objs.keys():
                                                    #     geno_value = genovalues_objs[(genoid,referential,allele,frequency)]
                                                    #
                                                    # else:
                                                    #     geno_value, created = GenotypingValue.objects.get_or_create(genotyping_id=genoid,
                                                    #                                                                 referential=referential,
                                                    #                                                                 allele=allele,
                                                    #                                                                 frequency=frequency)
                                                    #
                                                    #     genovalues_objs[(genoid,referential,allele,frequency)] = geno_value
                                                    #
                                                    # locus_to_genoval.append(LocusToValue(locus=locus, genotyping_value=geno_value))
                                            
                                            # Les deux valeurs sont identiques mais non egales a missing value
                                            else:
                                                frequency = 1
                                                
                                                if genoid and locus:
                                                    
                                                    code_objs, genovalues_objs, locus_to_genoval = save_genovalue_and_locustovalue(list_values[0], frequency, genoid, referential, locus, code_objs, genovalues_objs, locus_to_genoval)
                                                    
                                                    # if list_values[0] in code_objs.keys():
                                                    #     allele = code_objs[list_values[0]]
                                                    # else:
                                                    #     allele, created = CodeAllele.objects.get_or_create(code_allele=list_values[0])
                                                    #     code_objs[list_values[0]] = allele
                                                    #
                                                    # if (genoid,referential,allele,frequency) in genovalues_objs.keys():
                                                    #     geno_value = genovalues_objs[(genoid,referential,allele,frequency)]
                                                    #
                                                    # else:
                                                    #     geno_value, created = GenotypingValue.objects.get_or_create(genotyping_id=genoid,
                                                    #                                                                 referential=referential,
                                                    #                                                                 allele=allele,
                                                    #                                                                 frequency=frequency)
                                                    #
                                                    #     genovalues_objs[(genoid,referential,allele,frequency)] = geno_value
                                                    #
                                                    # locus_to_genoval.append(LocusToValue(locus=locus, genotyping_value=geno_value))
                                                
                                        # Les deux valeurs sont differentes
                                        else:
                                            # Une des deux vaut missing value
                                            if list_values[0] == missing_value or list_values[1] == missing_value:
                                                if list_values[0] == missing_value:
                                                    list_values[0] = 'NA'
                                                else:
                                                    list_values[1] = 'NA'
                                                    
                                                frequency = 0.5
                                                    
                                                for val in list_values:
                                                    
                                                    code_objs, genovalues_objs, locus_to_genoval = save_genovalue_and_locustovalue(val, frequency, genoid, referential, locus, code_objs, genovalues_objs, locus_to_genoval)
                                                    
                                                    # if val in code_objs.keys():
                                                    #     allele = code_objs[val]
                                                    # else:
                                                    #     allele, created = CodeAllele.objects.get_or_create(code_allele=val)
                                                    #     code_objs[val] = allele
                                                    #
                                                    # if (genoid,referential,allele,frequency) in genovalues_objs.keys():
                                                    #     geno_value = genovalues_objs[(genoid,referential,allele,frequency)]
                                                    # else:
                                                    #     geno_value, created = GenotypingValue.objects.get_or_create(genotyping_id=genoid,
                                                    #                                                                 referential=referential,
                                                    #                                                                 allele=allele,
                                                    #                                                                 frequency=frequency)
                                                    #
                                                    #     genovalues_objs[(genoid,referential,allele,frequency)] = geno_value
                                                    #
                                                    # locus_to_genoval.append(LocusToValue(locus=locus, genotyping_value=geno_value))
                                                
                                            
                                            # Aucune n'est missing value 
                                            else:
                                                frequency = 0.5
                                                for val in list_values:
                                                    
                                                    code_objs, genovalues_objs, locus_to_genoval = save_genovalue_and_locustovalue(val, frequency, genoid, referential, locus, code_objs, genovalues_objs, locus_to_genoval)
                                                    
                                                    # if val in code_objs.keys():
                                                    #     allele = code_objs[val]
                                                    # else:
                                                    #     allele, created = CodeAllele.objects.get_or_create(code_allele=val)
                                                    #     code_objs[val] = allele
                                                    #
                                                    # if (genoid,referential,allele,frequency) in genovalues_objs.keys():
                                                    #     geno_value = genovalues_objs[(genoid,referential,allele,frequency)]
                                                    # else:
                                                    #     geno_value, created = GenotypingValue.objects.get_or_create(genotyping_id=genoid,
                                                    #                                                                 referential=referential,
                                                    #                                                                 allele=allele,
                                                    #                                                                 frequency=frequency)
                                                    #
                                                    #     genovalues_objs[(genoid,referential,allele,frequency)] = geno_value
                                                    #
                                                    # locus_to_genoval.append(LocusToValue(locus=locus, genotyping_value=geno_value))
                                    
                        except Exception as e:
                            if str(e) != "":
                                tr_messages['error'].append(str(e)+" (at line: "+line_name+", column: "+col_name+").")
                                continue
            
            except Exception as e:
                if str(e) != "":
                    tr_messages['error'].append(str(e)+" (at line: "+line[label]+").")
                    continue
                
        #If some locustovalue objects where not inserted
        if locus_to_genoval:
            #print("final bulk create", flush=True)
            try:
                LocusToValue.objects.bulk_create(locus_to_genoval)
            except Exception as e:
                if str(e) != "":
                    tr_messages['error'].append(str(e)+" (when inserting data).")
             
            locus_to_genoval = []
            
        if missing_geno != []:
            tr_messages['error'].append("The following GenotypingID does not exist in the database: "+', '.join(missing_geno)+".")
            
        if missing_loc != []:
            tr_messages['error'].append("The following Locus does not exist in the database: "+', '.join(missing_loc)+".")
 
        if tr_messages['error'] != []:
            transaction.set_rollback(True)
        else:
            tr_messages['success'].append("The file has been successfully inserted.")
    
    add_messages_to_thread_report(tr_messages, report)


def save_genovalue_and_locustovalue(value, frequency, genoid, referential, locus, code_objs, genovalues_objs, locus_to_genoval):
    """
      To retrieve and save genotyping value 
      and locus to value in dicts and lists 
      in order to insert them.
    """
    if value in code_objs.keys():
        allele = code_objs[value]
    else:
        allele, created = CodeAllele.objects.get_or_create(code_allele=value)
        code_objs[value] = allele
    
    if (genoid,referential,allele,frequency) in genovalues_objs.keys():
        geno_value = genovalues_objs[(genoid,referential,allele,frequency)]
        
    else:
        geno_value, created = GenotypingValue.objects.get_or_create(genotyping_id=genoid,
                                                                    referential=referential,
                                                                    allele=allele,
                                                                    frequency=frequency)
        
        genovalues_objs[(genoid,referential,allele,frequency)] = geno_value
    
    locus_to_genoval.append(LocusToValue(locus=locus, genotyping_value=geno_value))
    
    return code_objs, genovalues_objs, locus_to_genoval



def insert_flat_file_genotyping_values(request, file_dict, referential_name, missing_value, frequency_test, report):
    print("insert flat")
    
    # To let the db save the report before updating it (else duplicate key error on .save())
    time.sleep(1)
    
    #Dict used for the addition test of frequencies at 0.1%
    frequencies = {}
    
    #Lists used for the report
    missing_geno = []
    missing_loc = []
    
    locus_objs = {}
    genoid_objs = {}
    genovalues = {}
    
    bulk_l2v = []
    bulk_l2v_gv = {}
    bulk_gv = {}
    
    #Dict used to store codeallele created or retrieved
    code_objs = {}
    
    tr_messages = {'success':[],
                   'warning':[],
                   'info':[],
                   'error':[]}
    
    i = -1
    
    try:
        referential = Referential.objects.get(name=referential_name)
    except Exception as e:
        tr_messages['error'].append("The referential is not valid, please try again.")
        add_messages_to_thread_report(tr_messages, report)
        return
    
    #On récupère les genoID existants pour ce référentiel et on les mets en mémoire (attention à la gestion de celle ci)
    #et on commence à peupler genoid_on=bjs
    #si pb de mémoire un jour veiller à ne travailler qu'avec des IDs et pas des objets
    genoval_qs = GenotypingValue.objects.filter(referential=referential)
    for gv in genoval_qs :
        genovalues[(gv.referential_id, gv.genotyping_id_id, gv.allele_id, frequency)] = gv
    
    with transaction.atomic():
        for line in file_dict:
            i += 1
            try:
                with transaction.atomic():
                    try :
                        genoid = genoid_objs[line['Genotyping_ID']]
                    except KeyError :
                        try:
                            genoid = GenotypingID.objects.get(name=line['Genotyping_ID'])
                        except GenotypingID.DoesNotExist:
                            genoid = ''
                            if line['Genotyping_ID'] not in missing_geno:
                                missing_geno.append(line['Genotyping_ID'])
                    try:
                        locus = locus_objs[line['Locus']]
                    except KeyError:
                        try:
                            locus = Locus.objects.get(name=line['Locus'])
                            locus_objs[line['Locus']] = locus
                        except Locus.DoesNotExist:
                            locus = ''
                            if line['Locus'] not in missing_loc:
                                missing_loc.append(line['Locus'])

                    if line['Allele_value'].strip() == missing_value:
                        value = 'NA'
                    else:
                        value = line['Allele_value'].strip()

                    try :
                        allele = code_objs[value]
                    except KeyError:
                        allele, created = CodeAllele.objects.get_or_create(code_allele=value)
                        code_objs[value] = allele
                    
                    if line['Allelic_frequency'].strip() != '':
                        if line['Allelic_frequency'] == missing_value:
                            allelic_freq = None
                            
                        else:
                            line['Allelic_frequency'] = line['Allelic_frequency'].strip().replace(',','.')
                            
                            # Test if frequency is a number
                            try:
                                float(line['Allelic_frequency'])
                            except Exception:
                                raise Exception('Allelic frequency is not a number')
                                
                            if float(line['Allelic_frequency']) >= 0.0 and float(line['Allelic_frequency']) <= 1.0:
                                allelic_freq = round(float(line['Allelic_frequency']), 3) #Pour arrondir a 3 décimales
                            else:
                                raise Exception('Allelic frequency must be between 0 and 1')
                    else:
                        raise Exception('Allelic frequency is missing')                     
                    
                    if genoid and locus:
                        #We do not count the frequencies at None
                        if frequency_test and allelic_freq != None:
                            try :
                                frequencies[locus.name][genoid.name].append(allelic_freq)
                            except KeyError as e:
                                if e == locus.name :
                                    frequencies[locus.name] = {}
                                    frequencies[locus.name][genoid.name] = [allelic_freq]
                                elif e == genoid.name :
                                    frequencies[locus.name][genoid.name] = [allelic_freq]
                        recordtime = datetime.datetime.now() 
                        ##il faut créer les genoid par bulk_create
                        #mettre à jour la table quand les bulk sont créés (avec les ids)
                        try :
                            geno_value = genovalues[(referential.id,genoid.id,allele.id,allelic_freq)]
                        except KeyError:

                            geno_value = None
                            bulk_gv[(referential.id,genoid.id,allele.id,allelic_freq)]= GenotypingValue(referential=referential,genotyping_id=genoid,allele=allele,frequency=allelic_freq)
                            try :
                                bulk_l2v_gv[(referential.id,genoid.id,allele.id,allelic_freq)].append(locus)
                            except KeyError:
                                bulk_l2v_gv[(referential.id,genoid.id,allele.id,allelic_freq)]=[]
                                bulk_l2v_gv[(referential.id,genoid.id,allele.id,allelic_freq)].append(locus)
                            #print("\tNew - Geno value : {0}".format(datetime.datetime.now()-recordtime))                        
                        
                        if geno_value is not None :
                            bulk_l2v.append(LocusToValue(locus=locus, genotyping_value=geno_value))
                        
                        if i%50000 == 0 and i != 0:
                            LocusToValue.objects.bulk_create(bulk_l2v, batch_size=10000)
                            objs = GenotypingValue.objects.bulk_create(bulk_gv.values(), batch_size=10000)
                            new_l2v = []
                            for gv in objs :
                                for loc in bulk_l2v_gv[(gv.referential_id, gv.genotyping_id_id, gv.allele_id, gv.frequency)] :
                                    new_l2v.append(LocusToValue(locus=loc, genotyping_value=gv))
                                genovalues[(gv.referential_id, gv.genotyping_id_id, gv.allele_id, gv.frequency)] = gv
                            if new_l2v : 
                                LocusToValue.objects.bulk_create(new_l2v)
                            bulk_l2v = []
                            bulk_gv = {}
                            bulk_l2v_gv = {}
                            
                            print("Batch insert "+str(i)+" ("+str(datetime.datetime.now())+")", flush=True)
                        #loc_val = LocusToValue.objects.create(locus=locus, genotyping_value=geno_value)
                #print("One record [{1}]: {0}".format(datetime.datetime.now()-recordtime, i))
            except Exception as e:
                traceback.print_exception(e)
                if str(e) != "":
                    #print(e)
                    tr_messages['error'].append(str(e)+" (at line: "+line['Genotyping_ID']+", "+line['Locus']+").")
                    continue
 
        if missing_geno != []:
            tr_messages['error'].append("The following GenotypingID does not exist in the database: "+', '.join(missing_geno)+".")
            
        if missing_loc != []:
            tr_messages['error'].append("The following Locus does not exist in the database: "+', '.join(missing_loc)+".")
 
        if frequency_test:
            frequencies_addition_test(frequencies, tr_messages)
 
        if tr_messages['error'] != []:
            transaction.set_rollback(True)
        else:
            tr_messages['success'].append("The file has been successfully inserted.")
    
    add_messages_to_thread_report(tr_messages, report)


def frequencies_addition_test(frequencies, tr_messages):
    """
      Test to check that the sum of frequencies is 
      1 for each locus and genoid.
    """
    no_error = True
    
    for locus, genoids_dict in frequencies.items():
        for genoid, freq_list in genoids_dict.items():
            result = 0
            for freq in freq_list:
                result += freq 
            
            if not 0.999 <= result <= 1.001:
                tr_messages['error'].append("The sum of the frequencies for the locus {0} and the genotypingID {1} does not sum up to 1: {2}.".format(locus, genoid, round(result,3)))
                no_error = False
    
    if no_error:        
        tr_messages['info'].append("The sums of all frequencies add up to 1.")
                


def add_messages_to_thread_report(tr_messages, report):
    """
      This function is used to add messages to a thread report 
      and to mark if it is successful or not.
      3 Mb of messages allowed: equals less than 33 000 messages max.
    """
    dtime = datetime.datetime.now()
    report.message = report.message + '\n' + 'Message updated on the ' + dtime.strftime("%d %B %Y, %H:%M") + '.'
    
    if tr_messages['info']:
        report.message = report.message + '\n\n Information: '
        for msg in tr_messages['info']:
            if bytestomegabytes(sys.getsizeof(report.message + '\n' + str(msg))) <= 1:
                report.message = report.message + '\n' + str(msg)
            else:
                report.message = report.message + '\n' + "... (there are more info messages that can't be stored in the database)"
                break
    
    if tr_messages['warning']:
        report.message = report.message + '\n\n Warnings: '
        for msg in tr_messages['warning']:
            if bytestomegabytes(sys.getsizeof(report.message + '\n' + str(msg))) <= 2:
                report.message = report.message + '\n' + str(msg)
            else:
                report.message = report.message + '\n' + "... (there are more warning messages that can't be stored in the database)"
                break
    
    if tr_messages['error']:
        report.message = report.message + '\n\n Errors: '
        for msg in tr_messages['error']:
            if bytestomegabytes(sys.getsizeof(report.message + '\n' + str(msg))) <= 3:
                report.message = report.message + '\n' + str(msg)
            else:
                report.message = report.message + '\n' + "... (there are more error messages that can't be stored in the database)"
                break
            
        report.success = False 
    
    if tr_messages['success']:
        report.message = report.message + '\n '
        for msg in tr_messages['success']:
            report.message = report.message + '\n' + str(msg)
        report.success = True
    
    report.new = True
    report.save()


def insert_vcf_genotyping_values(request, file_dict, referential_name):
    print("insert vcf")
    # A faire


def genotyping_matrix_template_filerenderer(request):
    """
      View to render a matrix file template.
    """
    response = HttpResponse(content_type='text/csv') 
    writer = csv.writer(response,delimiter=';', quoting=csv.QUOTE_ALL)
    
    headers = ['Name', 'Genotyping_ID_1', 'Genotyping_ID_2', 'Genotyping_ID_3', 'Genotyping_ID_4', '...']
    
    writer.writerow(headers)
    
    writer.writerow(['Locus_1', '', '', '', ''])
    writer.writerow(['Locus_2', '', '', '', ''])
    writer.writerow(['Locus_3', '', '', '', ''])
    writer.writerow(['Locus_4', '', '', '', ''])
    writer.writerow(['...', '', '', '', ''])
    
    filename = 'Matrix_file_template.csv'
    
    response['Content-Disposition'] = 'attachment; filename=%s' %filename  
    writer = csv.writer(response)
    
    return response

# Encore utilise pour le vcf
def export_to_json(ref_name, file, deli, ref_id, matrix_orientation):
    #TODO MongoClient a remplacer
#     client = MongoClient(settings.MONGODB_DATABASES['default']['host'], settings.MONGODB_DATABASES['default']['port'])
#     db = client[settings.MONGODB_DATABASES['default']['name']]
    i = 0
    msg_error = []
    print("Start JSON")
    if matrix_orientation == "loci_col" and "GBS" not in ref_name:
        if deli == '\\t':
            deli = '\t'
        try:
            fichier = np.genfromtxt(file, delimiter=deli, dtype=np.str)
            print('ok loci not GBS')
        except ValueError:
            msg_error = "Some errors were detected. The format of your file is not right. Please check you try to upload the right file."
            return msg_error
        
        output = settings.TEMP_FOLDER + 'inter.json'
        res = open(output,'w')
        
        nb_lo = np.count_nonzero(fichier[0,1:])
#         print(( "nb lo: "+str(nb_lo)
        row_loc0 = fichier[0,1:nb_lo+1]
        row_loc = [s.replace('"','') for s in row_loc0]

#         print(( "row lo: "+str(row_lo)
        genoid_doesnt_exist = []
    
        ref = Referential.objects.get(name=ref_name)
    
        nb_genoid = np.count_nonzero(fichier[1:,0])
        col_genoid0 = fichier[1:,0]
        col_genoid = [s.replace('"', '') for s in col_genoid0]
        sorted_listgenoid = GenotypingID.objects.filter(name__in=col_genoid, referential=ref)
        dict_genoid = dict([(genoid.name, genoid) for genoid in sorted_listgenoid])
        #dict_exp = dict([(genoid.name, genoid.experiment) for genoid in sorted_listgenoid])
        
        print('ici')
        print(sorted_listgenoid)
        listgenoid = []
        for genoid in col_genoid:
            try:
                listgenoid.append(dict_genoid[genoid])
            except KeyError:
                print('GenoID ' + genoid + ' does not exist')
                genoid_doesnt_exist.append(genoid)
                continue
        if genoid_doesnt_exist != []:
            msg_error = "Some errors were detected. The following genotypingID you are trying to updload are not in the database: {0}. No data will be inserted.".format(genoid_doesnt_exist)
            #return msg_error
        listsample_exp = {} #création d'un dictionnaire vide
        
        print('RECUP FINIE')
        
#         print(listgenoid)
        for genoid in listgenoid:
            listsample_exp[genoid.sample_id]=genoid.experiment_id #remplissage du dictionnaire: pour chaque sample_id on lui associe l'expérience correspondante
#             print('genoid: ',genoid)
#             print('list sample exp: ',listsample_exp)
        
        for i in range(len(row_loc)):
            j = 0
            if i%1000 == 0:
                print(i)
#             print('locus: ',row_loc[i])
            #recuperation de l'id du marqueur grace au nom du marqueur
            variable = '{"name": "' + str(row_loc[i]) + '", "genotyping": [' #on remplace name par l'id
            res.write(variable)
            k=0
            for genoid in listgenoid :
             #on  récupère la clé: le sample_id et la valeur associée: l'expérience dans le dictionnaire précédemment créé
#                 print(j)
                val_geno0 = str(fichier[j+1,i+1])
                val_geno1 = [s.replace('"', '') for s in val_geno0]
                val_geno = ''.join(val_geno1)
#                     print('valgeno: ',val_geno)
#                     print(k, '  len: ',len(listsample_exp)-1)
                if k < len(listgenoid)-1:
                    variable_2 = '{"sample_id": ' + str(genoid.sample_id) + ','
                    variable_3 = '"genotyping":[{"genotype": "' + val_geno + '","experiment_id":' + str(genoid.experiment_id) + ', "referential_id": ' + str(ref_id) + '}]},'
                    res.write(variable_2 + variable_3)
                elif k == len(listgenoid)-1:    
                    variable_2 = '{"sample_id": ' + str(genoid.sample_id) + ',' 
                    variable_3 = '"genotyping":[{"genotype": "' + val_geno + '","experiment_id":' + str(genoid.experiment_id) + ', "referential_id": ' + str(ref_id) + '}]}'
                    res.write(variable_2 + variable_3)
                k+=1
                j += 1
#                 print("res:   ",variable,variable_2,variable_3)
            res.write(']}\n')
        # verif que tout a ete parse
        
#         if i != len(row_loc)-1 and j != len(listsample_exp) or i != len(row_loc)-1 or j != len(listsample_exp):
#             print('Erreur')
        res.close()
        db_name = settings.MONGODB_DATABASES['default']['name']
        db_host = settings.MONGODB_DATABASES['default']['host']
        db_port = settings.MONGODB_DATABASES['default']['port']
        appel_mongo = 'mongoimport -c locus -d ' + db_name  + ' --host ' + db_host + ' --port ' + str(db_port) + ' < ' + output + ' --batchSize 1' #ajouter -vvvv pour verbose, pour avoir plus de renseignements mais plus lent
#         print("appel_mongo: ",appel_mongo)
        print("END JSON")
        os.system(appel_mongo)
        os.remove(output)
        os.remove(file)
        return msg_error
        
    elif matrix_orientation == "genoid_col" and "GBS" not in ref_name:
        output = settings.TEMP_FOLDER +'inter600.json'
        res = open(output,'w')
        genoid_doesnt_exist = []
        if deli == '\\t':
            deli = '\t'
        try:
            fichier = np.genfromtxt(file, delimiter=deli, dtype=np.str)
            print("ok")
        except ValueError as ve:
            print(ve)
            msg_error = "Some errors were detected. The format of your file is not right. Please check you try to upload the right file."
            return msg_error
        # lecture du fichier
        #nb_genoid = np.count_nonzero(fichier[0,1:])-4 #récupération du nombre de genoID (sans les 4 valeurs non genoID: probesetID, chr, chrpos et affyID)
        nb_genoid = np.count_nonzero(fichier[0,1:])
        row_genoid0 = fichier[0,1:nb_genoid+1]
        row_genoid = [s.replace('"', '') for s in row_genoid0]
    
        nb_lo = np.count_nonzero(fichier[1:,0])
        col_loc0 = fichier[1:,0]
        col_loc = [s.replace('"', '') for s in col_loc0]
        
        ref = Referential.objects.get(name=ref_name)
        
        sorted_listgenoid = GenotypingID.objects.filter(name__in=row_genoid, referential=ref)
        sorted_listgenoid = dict([(genoid.name, genoid) for genoid in sorted_listgenoid])
        listgenoid = []
        print(sorted_listgenoid)
        for genoid in row_genoid:
            try:
                listgenoid.append(sorted_listgenoid[genoid])
            except KeyError:
                genoid_doesnt_exist.append(genoid)
                continue
        if genoid_doesnt_exist != []:
            msg_error = "Some errors were detected. The following genotypingID you are trying to updload are not in the database: {0}. No data will be inserted.".format(genoid_doesnt_exist)
            return msg_error
        # recup tous les samples dont le nom est present dans le fichier
        listsample_exp = {} #création d'un dictionnaire vide
        print("FIRST LOOP")
        for genoid in listgenoid:
            listsample_exp[genoid.sample_id]=genoid.experiment_id #remplissage du dictionnaire: pour chaque sample_id on lui associe l'expérience correspondante

        # pour chaque locus
        print("SECOND LOOP")
        for i in range(len(col_loc)):
            if i%1000 == 0:
                print(i)
            #chr = fichier[i+1,nb_genoid+2]
            #pos = fichier[i+1, nb_genoid+3]
            # pour obtenir les valeurs de genotypage par colonne
            f = fichier[i+1,1:nb_genoid+1]
                    
            #variable = '{"name": "' + str(col_loc[i]) + '", "positions": [{"genomeversion": '\
            #                + str(3) + ', "chromosome": "' + str(chr) + '", "position": '\
            #                + str(pos) + '}], "genotyping": ['
            variable = '{"name": "' + str(col_loc[i]) + '", "genotyping": ['
            res.write(variable)

            j = 0
            # pour chaque sample d'un locus
            print("LAST LOOP")
            for sample, experiment in listsample_exp.items():
                val_geno0 = f[j]   
                val_geno1 = [s.replace('"', '') for s in val_geno0]
                val_geno = ''.join(val_geno1) 
                
                variable_2 = '{"sample_id": ' + str(sample) + ','
                if j < len(listsample_exp)-1:
                    variable_3 = ' "genotyping":[{"genotype": "' + val_geno + '","experiment_id":' + str(experiment)+\
                                 ', "referential_id":' + str(ref_id) + '}]},'
                    res.write(variable_2+variable_3)
                elif j == len(listsample_exp)-1: #si on arrive à la derniere ligne, il n'y a plus besoin de virgule à la fin
                    variable_3 = ' "genotyping":[{"genotype": "' + val_geno + '","experiment_id":' + str(experiment)+\
                                 ', "referential_id":' + str(ref_id) + '}]}'
                    res.write(variable_2+variable_3)
                else:
                    print("Else")   
                j += 1
                 
            res.write(']}\n')
            
        res.close()
        
        # verif que tout a ete parse
#         print(i 
#         print(j
#         print(len(col_loc)
#         print(len(listsample)
        if i != len(col_loc)-1 and j != len(listsample_exp) or i != len(col_loc)-1 or j != len(listsample_exp):
            print('Erreur')
        
        print("DATABASE")
        db_name = settings.MONGODB_DATABASES['default']['name']
        db_host = settings.MONGODB_DATABASES['default']['host']
        db_port = settings.MONGODB_DATABASES['default']['port']
        appel_mongo = 'mongoimport -c locus -d ' + db_name + ' --host ' + db_host + ' --port ' + str(db_port) + ' < ' + output + ' --batchSize 1 '
        os.system(appel_mongo)

        print("END JSON")
        
        # On utilise subprocess pour voir les messages d'erreur
        #cmd = ["mongoimport","--collection", "locus", "--db", "thaliadb-trunk", "--file", settings.TEMP_FOLDER+"/inter600.json"]
        #r = subprocess.run(cmd, stderr=subprocess.PIPE)
        #print(r.stderr)
        
#         print_mongoimport = subprocess.getoutput(appel_mongo)
#         if 'Failed:' in print_mongoimport:
#             msg_error = "There is an error in the mongo import module, please contact a competent person."
#             appel_mongo = 'mongoimport -c locus -d ' + db_name + ' < ' + output + ' --batchSize 1'
#         
        #os.remove(output)
        os.remove(file)
        return msg_error

    elif "GBS" in ref_name:
        print(ref_name)
        #VERIFICATION pour tous les fichiers que les genoids et les loci existent bien dans la base
        output = settings.TEMP_FOLDER + 'interGBS.json'
        res = open(output,'w')
        v=0
        for f in file:
            print("f: ",f)
            v=+1
            if deli == '\\t':
                deli = '\t'
            
            try:
                fichier = np.genfromtxt(f, delimiter=deli, dtype=np.str)
#<<<<<<< .working
            except ValueError:
                msg_error = "Some errors were detected. The format of your file is not right. Please check you try to upload the right file."
                return msg_error

            genoid_doesnt_exist = []
            locusid_doesnt_exist = []
            print(fichier[0,0:])
            nb_lo = np.count_nonzero(fichier[0,0:])
            #row_loc0 = fichier[0,0:nb_lo]
            row_loc0 = fichier[0,1:nb_lo]
            row_loc = [s.replace('"','') for s in row_loc0]
#            print('row loc: ',row_loc)
            nb_genoid = np.count_nonzero(fichier[1:,0])
            print("Nombre genoid : ", nb_genoid)
            col_genoid0 = fichier[1:,0]
            col_genoid = [s.replace('"', '') for s in col_genoid0]
            sorted_listgenoid = GenotypingID.objects.filter(name__in=col_genoid)
            sorted_listgenoid = dict([(genoid.name, genoid) for genoid in sorted_listgenoid])
            listgenoid = []
#                 print("col_genoid: ",col_genoid)
            print('GBS OK')
            for genoid in col_genoid:
                try:
                    listgenoid.append(sorted_listgenoid[genoid])
                except KeyError:
                    print('GenoID ' + genoid + ' does not exist.')
                    genoid_doesnt_exist.append(genoid)
                    continue
            if v ==1:
                for i in range(len(row_loc)-1):
                    try:
                        len_locus = len(Locus.objects.filter(name = row_loc[i]))
                    except:
                        locusid_doesnt_exist.append(row_loc[i])
                        continue
            if genoid_doesnt_exist != []:
                msg_error = "Some errors were detected. The following genotypingID you are trying to updload are not in the database: {0}. No data will be inserted.".format(genoid_doesnt_exist)
                return msg_error
            if locusid_doesnt_exist != []:
                msg_error = "Some errors were detected. The following loci you are trying to updload are not in the database: {0}. No data will be inserted.".format(locusid_doesnt_exist)
                return msg_error

            listsample = []
            listsample_exp = {} #création d'un dictionnaire vide
            print('RECUP FINIE')
            
            for genoid in listgenoid:
                listsample_exp[genoid.sample_id]=genoid.experiment_id #remplissage du dictionnaire: pour chaque sample_id on lui associe l'expérience correspondante
    #             print('genoid: ',genoid)
    #             print('list sample exp: ',listsample_exp)
            
            #for i in range(len(row_loc)):
            for i in range(len(row_loc)):
                j = 0
                if i%1000 == 0:
                    print(i)
    #             print('locus: ',row_loc[i])
                variable = '{"name": "' + str(row_loc[i]) + '", "genotyping": ['
                res.write(variable)
                #print("variable = ", variable)
                k=0
                for genoid in listgenoid :
                 #on  récupère la clé: le sample_id et la valeur associée: l'expérience dans le dictionnaire précédemment créé
    #                 print(j)
                    val_geno0 = str(fichier[j+1,i+1])
                    val_geno1 = [s.replace('"', '') for s in val_geno0]
                    val_geno = ''.join(val_geno1)
                    if k < len(listgenoid)-1:
                        variable_2 = '{"sample_id": ' + str(genoid.sample_id) + ','
                        variable_3 = '"genotyping":[{"genotype": "' + val_geno + '","experiment_id":' + str(genoid.experiment_id) + ', "referential_id": ' + str(ref_id) + '}]},'
                        res.write(variable_2 + variable_3)
                    #elif k == len(listgenoid)-1: 
                    elif k == len(listgenoid)-1:
                        variable_2 = '{"sample_id": ' + str(genoid.sample_id) + ',' 
                        variable_3 = '"genotyping":[{"genotype": "' + val_geno + '","experiment_id":' + str(genoid.experiment_id) + ', "referential_id": ' + str(ref_id) + '}]}'
                        res.write(variable_2 + variable_3)
                    k+=1
                    j += 1
                    #print("variable 2 et 3 = ", variable_2, variable_3)
    #                 print("res:   ",variable,variable_2,variable_3)
                #print("]}")
                res.write(']}\n')
            #os.remove(f)
        res.close()
        db_name = settings.MONGODB_DATABASES['default']['name']
        db_host = settings.MONGODB_DATABASES['default']['host']
        db_port = settings.MONGODB_DATABASES['default']['port']
        appel_mongo = 'mongoimport -c locus -d ' + db_name + ' --host ' + db_host + ' --port ' + str(db_port) + ' < ' + output + ' --batchSize 1' #ajouter -vvvv pour verbose, pour avoir plus de renseignements mais plus lent et ça plombe les log...
#         print("appel_mongo: ",appel_mongo)
        print("fin JSON")
        os.system(appel_mongo)
        return msg_error


#=============================================
@login_required
@user_passes_test(lambda u: u.is_seedlot_admin_user(), login_url='/team/')
def locusattribute_edit(request, pk=None):
    attribute = LocusTypeAttribute.objects.get(id=pk)
    form = LocusTypeAttForm(instance=attribute)
    template = 'genotyping/locus_attributes.html'
    fields_name, fields_label = get_fields(form)
    template_data = {'fields_label':fields_label,
                    'fields_name':fields_name,
                    'all': LocusTypeAttribute.objects.all(),
                    'form' : form,
                    'urlname':'edit_locusattribute',
                    'creation_mode':False}
    if request.method == "POST" :
        form = LocusTypeAttForm(request.POST, instance=attribute)
        if form.is_valid() :
            form.save()
            template_data.update({'form':LocusTypeAttForm(),'creation_mode':True })
            return render(request, template, template_data)
        else :
            template_data.update({'form':form})
            return render(request, template, template_data)
    return render(request, template, template_data)


@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def locusattributes(request):
    form = LocusTypeAttForm()
    template = 'genotyping/locus_attributes.html'
    fields_name, fields_label = get_fields(form)
    template_data = {'fields_label': fields_label,
                    'fields_name': fields_name,
                    'all': LocusTypeAttribute.objects.all().order_by('name'),
                    'form' : form,
                    'urlname': 'edit_locusattribute',
                    'creation_mode':True}
    
    if request.method == "POST" :
        form = LocusTypeAttForm(request.POST)
        if form.is_valid() :
            form.save()
            template_data.update({'form':LocusTypeAttForm()})
            return render(request, template, template_data)
        else :
            template_data.update({'form':form})
            return render(request, template, template_data)
    return render(request, template, template_data)


@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def locustype(request):
    form = LocusTypeForm()
    template = 'genotyping/genotyping_type.html'
    fields_name, fields_label = get_fields(form)
    template_data = {'fields_label':fields_label,
                    'fields_name':fields_name,
                    'all': LocusType.objects.all(),
                    'form' : form,
                    'urlname':'edit_locustype',
                    'creation_mode':True}
    
    if request.method == "POST" :
        form = LocusTypeForm(request.POST)
        if form.is_valid() :
            form.save()
            template_data.update({'form':LocusTypeForm()})
            return render(request, template, template_data)
        else :
            template_data.update({'form':form})
            return render(request, template, template_data)
    return render(request, template, template_data)


@login_required
@user_passes_test(lambda u: u.is_seedlot_admin_user(), login_url='/team/')
def locustype_edit(request, pk=None):
    try :
        locus_type = LocusType.objects.get(id=pk)
        form = LocusTypeForm(instance=locus_type)
        formatt = LocusTypeAttributeSearch()
        template = 'genotyping/genotyping_type.html'
        fields_name, fields_label = get_fields(form)
        template_data = {'fields_label':fields_label,
                        'fields_name':fields_name,
                        'all': LocusType.objects.all(),
                        'form' : form,
                        'urlname':'edit_locustype',
                        'attributeform':formatt,
                        'creation_mode':False,
                        'attributes':locus_type.locusattributeposition_set.all().order_by('position')}
        if request.method == "POST" :
            if request.POST.get('submitedform') == "locustype" :
                form = LocusTypeForm(request.POST, instance=locus_type)
                if form.is_valid() :
                    form.save()
                    template_data.update({'form':LocusTypeForm(),'creation_mode':True })
                    return render(request, template, template_data)
                else :
                    template_data.update({'form':form})
                    return render(request, template, template_data)
            elif request.POST.get('submitedform') == "removingattribute" :
                attribute_id = request.POST.get('attributeid')
                template_data.update({'activetab':1})
                try :
                    locus_type.remove_attribute(attribute_id)
                    messages.add_message(request,messages.SUCCESS, "Attribute have been removed from the list")
                except LocusTypeAttribute.DoesNotExist:
                    messages.add_message(request,messages.ERROR, "Attribute doesn't exists")
                return render(request, template, template_data)
            elif request.POST.get('submitedform') == "addattribute" :
                attribute_id = request.POST.get('attributes')
                template_data.update({'activetab':1})
                try :
                    with transaction.atomic():
                        LocusAttributePosition.objects.create_last_element(attribute_id, locus_type)
                except LocusTypeAttribute.DoesNotExist:
                    messages.add_message(request, messages.ERROR, "Attribute doesn't exists")
                except IntegrityError as e :
                    messages.add_message(request, messages.ERROR, "Attribute can't be added")
                else :
                    messages.add_message(request,messages.SUCCESS, "Attribute have been added to the list")
                return render(request, template, template_data)
            elif request.POST.get('submitedform') == "reorderingattributes" :
                ordered_list = request.POST.getlist('attributes')
                print(ordered_list)
                locus_type.recompute_positions(ordered_list)
                template_data.update({'activetab':1})
                return render(request, template, template_data)
                    
        return render(request, template, template_data)
    except LocusType.DoesNotExist :
        messages.add_message(request,messages.ERROR, "The LocusType you are trying to update doesn't exist")
        return redirect('locustype')


class LocusAttributesAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return LocusTypeAttribute.objects.none()
        qs = LocusTypeAttribute.objects.all()
        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs
    
    def get_result_label(self, result):
        attribute_type = dict(LocusTypeAttribute.AttributeType.choices)[result.type]
        label = "{0} ({1})".format(result.name, attribute_type)
        return label


@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def locusdata(request, type_id):
    # On récupère l'accessionType correspondant à l'id = type_id 
    locustype = LocusType.objects.get(id=int(type_id))
    # On récupère tous les attributs du type
    attributes = locustype.locustypeattribute_set.all()
#     print('attrs: ',attributes)
    # On récupère tous les accession du type dans "formattype"
    formattype = LocusDataForm(formstype=attributes)
    formposition = PositionForm()
    
    formfile = UploadFileFormWithoutDelimiter()
    
    names, labels = get_fields(formattype)
    
    names = names + ['genome_version','chromosome','position']
    labels = labels + ['Genome version','Chromosome','Position']
    
    title = locustype.name+' Locus Management'
    
    template = 'genotyping/locus_data.html'
    
    list_attributes=['name','comments']
    list_attributes_id = []
    
    for i in LocusTypeAttribute.objects.filter(locus_type=locustype):
        list_attributes_id.append(i.id)
    for i in list_attributes_id:
        if str(LocusTypeAttribute.objects.get(id=i)) not in list_attributes:
            list_attributes.append(str(LocusTypeAttribute.objects.get(id=i)))
    for i in ['genome version','chromosome','position']:
        list_attributes.append(i)

    tag_fields = {'title':title,
                  'fields_name':names,
                  'fields_label':labels,
                  "admin":True,
                  "refine_search":True,
                  "list_attributes":list_attributes,
                  }

    if request.method == "GET":
        
        print(datetime.datetime.now())
        locus_positions, nb_per_page, locus_positions_qs = _get_locus(locustype, attributes, request)
        print(datetime.datetime.now())
        
        number_all = locus_positions_qs.count()
        dico_get={}
        or_and = None
        
        if "no_search" in request.GET:
            return render(request, template, {"refine_search":True,"admin":True,"locus_special":True,"list_attributes":list_attributes,'formfile':formfile,'filetype':'locus','typeid': type_id,'form':formattype,'creation_mode':True,"number_all":number_all, 'all':locus_positions, 'fields_label':labels,'fields_name':names})
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera

            locus_positions = Locus.objects.refine_search(or_and, dico_get, locustype)

            _set_locus_attributes(locus_positions, attributes)
            print("juste après Refine search")
            
            number_all = locus_positions.count()
            download_not_empty = False
            
            if locus_positions:
                list_col = [str(f.name).replace("_", " ") for f in locus_positions[0].locus._meta.get_fields()]
                
                attr_names = locustype.locustypeattribute_set.all()
                for at in attr_names:
                    list_col.append(str(at.name))
                    
                for j in ['genome version','chromosome','position']:
                    list_col.append(j)
                for element_to_remove in ['id', 'type', 'attributes values', 'geno values', 'locustovalue', 'locusposition']:
                    list_col.remove(element_to_remove)
                
                search_result = RefineSearchFactory()
                #Usage of a thread: the file is computed while the page is displayed
                thread_name = execute_function_in_thread(search_result.write_locus, [locus_positions, list_col, LocusTypeAttribute], {})

                download_not_empty = True
                tag_fields.update({'download_not_empty':download_not_empty,
                                   'thread_name':thread_name})
                
        elif not "or_and" in request.GET:
            #Usage of a thread: the file is computed while the page is displayed
            thread_name = execute_function_in_thread(write_locus_csv, [locus_positions_qs, attributes, tag_fields, locustype], {})
            tag_fields.update({'thread_name':thread_name})
        
        
        #Cas où veut afficher la base de données
        tag_fields.update({'search_result':dico_get,
                           'or_and':or_and,
                           'refine_search':True,
                           'all':locus_positions,
                           'number_all':number_all,
                           'nb_per_page':nb_per_page,
                           'formposition':formposition,
                           'formfile':formfile,
                           'form':formattype,
                           'creation_mode':True,
                           'filename':(locustype.name).lower().replace(' ','_'),
                           'filetype':'locus',
                           'typeid': type_id,
                           'list_attributes':list_attributes})
        
        return render(request, template, tag_fields)
                    
    elif request.method == "POST":
        tag_fields.update({'search_result':{},
                           'or_and':None,
                           'refine_search':True,
                           'filename':(locustype.name).lower().replace(' ','_'),
                           'filetype':'locus',
                           'typeid': type_id,
                           'list_attributes':list_attributes})
        
        if "hiddenid" in request.POST.keys():
            locus_positions_qs = _update_locus_from_form(request, attributes, tag_fields, locustype)
            
        else:
            formattype = LocusDataForm(data=request.POST, formstype=attributes)
            formposition = PositionForm(data=request.POST)
            formfile = UploadFileFormWithoutDelimiter(request.POST, request.FILES) 
            
            if formfile.is_valid(): 
                locus_positions_qs = _manage_locus_from_file(request, attributes, tag_fields, locustype, formfile)
            
            elif formattype.is_valid() and formposition.is_valid():
                locus_positions_qs = _create_locus_from_form(request, attributes, tag_fields, locustype, formattype, formposition)
            
            else:
                locus_positions, nb_per_page, locus_positions_qs = _get_locus(locustype, attributes, request)
                number_all = locus_positions_qs.count()
                
                tag_fields.update({'all':locus_positions,
                                   'number_all':number_all,
                                   'nb_per_page':nb_per_page,
                                   'creation_mode':True})
                
                messages.add_message(request, messages.ERROR, "An error occurred.")
                    
        
        #Usage of a thread: the file is computed while the page is displayed
        thread_name = execute_function_in_thread(write_locus_csv, [locus_positions_qs, attributes, tag_fields, locustype], {})
        
        tag_fields.update({'thread_name':thread_name,
                           'formposition':formposition,
                           'formfile':formfile,
                           'form':formattype})
        
        return render(request, template, tag_fields)        

                    
def _update_locus_from_form(request, attributes, tag_fields, locustype):
    """
      To update a locus with the data from the form.
    """
    try:
        position = LocusPosition.objects.get(id=request.POST['hiddenid'])
        locus = position.locus
        formattype = LocusDataForm(instance=locus, data=request.POST, formstype=attributes)
        formposition = PositionForm(data=request.POST)
        formfile = UploadFileFormWithoutDelimiter()
    
        if formattype.is_valid() and formposition.is_valid():
            locus = formattype.save()
            
            position.genome_version = formposition.cleaned_data['genome_version']
            position.chromosome = formposition.cleaned_data['chromosome']
            position.position = formposition.cleaned_data['position']
            
            position.save()
            
            locus.update_attributes(attributes, request.POST)
            locus.save()
            
            locus_positions, nb_per_page, locus_positions_qs = _get_locus(locustype, attributes, request)
            number_all = locus_positions_qs.count()
            
            tag_fields.update({'all':locus_positions,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'formposition':formposition,
                               'object':locus,
                               'form':formattype,
                               'formfile':formfile,
                               'creation_mode':False,
                               'hiddenid':position.id})
            
            messages.add_message(request, messages.SUCCESS, "{0} has been updated.".format(locus.name))
    
        else:
            locus_positions, nb_per_page, locus_positions_qs = _get_locus(locustype, attributes, request)
            number_all = locus_positions_qs.count()
            
            tag_fields.update({'all':locus_positions,
                               'number_all':number_all,
                               'nb_per_page':nb_per_page,
                               'formposition':formposition,
                               'object':locus,
                               'form':formattype,
                               'formfile':formfile,
                               'creation_mode':False,
                               'hiddenid':position.id})
            
            messages.add_message(request, messages.ERROR, " An error occurred, {0} has not been updated.".format(locus.name))
    
        return locus_positions_qs
    
    except:
        messages.add_message(request, messages.ERROR, "The object you try to update doesn't exist in the database")
        
        return redirect('locusdata', type_id=locustype.id)


def _create_locus_from_form(request, attributes, tag_fields, locustype, formattype, formposition):
    """
      To create a locus with the data from the form.
    """
    new_locus = formattype.save(commit=False)
    new_locus.type = locustype
    new_locus.update_attributes(attributes, request.POST)
    new_locus.save()
    
    new_position = LocusPosition.objects.create(locus=new_locus,
                                                genome_version=formposition.cleaned_data['genome_version'],
                                                chromosome=formposition.cleaned_data['chromosome'],
                                                position=formposition.cleaned_data['position'])
    new_position.save()
    
    locus_positions, nb_per_page, locus_positions_qs = _get_locus(locustype, attributes, request)
    number_all = locus_positions_qs.count()
    
    tag_fields.update({'all':locus_positions,
                       'number_all':number_all,
                       'nb_per_page':nb_per_page,
                       'creation_mode':True})
    
    messages.add_message(request, messages.SUCCESS, "The locus {0} has been created.".format(new_locus.name))
    
    return locus_positions_qs
    

def _manage_locus_from_file(request, attributes, tag_fields, locustype, formfile):
    """
      Reading of the file in order to insert or update data.
    """
    myfactory = CSVFactoryForTempFiles(request.FILES['file'])
    myfactory.find_dialect()
    myfactory.read_file()
    file_dict = myfactory.get_file()
    
    headers_list = filerenderer_headers['Locus']
    
    missing = myfactory.check_headers_conformity(headers_list)
    
    if missing != []:
        messages.add_message(request, messages.ERROR, "One or several mandatory headers are missing in your file : "+format(', '.join(missing))+\
                             ". The required headers for this file are: "+format(', '.join(headers_list))+".")
        return
    
    if request.POST.get("submit"): #Cas d'une insertion de nouvelles données
        _submit_upload_file(request, file_dict, attributes, locustype)
    
    elif request.POST.get("update"): #L'utilisateur a cliqué sur le bouton update
        _update_upload_file(request, file_dict, attributes)
    
    else:
        messages.add_message(request, messages.ERROR,'ThaliaDB was unable to identify if you\'re trying to insert new data or to update former data.')
    
    locus_positions, nb_per_page, locus_positions_qs = _get_locus(locustype, attributes, request)
    number_all = locus_positions_qs.count()
    
    tag_fields.update({'all':locus_positions,
                       'number_all':number_all,
                       'nb_per_page':nb_per_page,
                       'creation_mode':True})
    
    return locus_positions_qs
    

def _submit_upload_file(request, file_dict, attributes, locustype):
    """
      Insertion of locus data from the file.
    """
    with transaction.atomic():
        jsonvalues={}
        for line in file_dict:
            try:
                with transaction.atomic(): 
                    if str(line['Genome version']):
                        genome_version = GenomeVersion.objects.get(name=str(line['Genome version']))
                    else:
                        genome_version = None
                    
                    for att in attributes:
                        value = line[str(att.name).strip()]
                        if value != None or value !='':
                            jsonvalues[att.id] = value   
                        else:
                            jsonvalues[att.id] = ''
                            
                    l = Locus.objects.create(name = str(line['Name']),
                                             type = locustype,
                                             comments = str(line['Comments']),
                                             attributes_values = jsonvalues)
                    l.save()
                    
                    p = LocusPosition.objects.create(locus = l,
                                                     genome_version = genome_version,
                                                     chromosome = str(line['Chromosome']),
                                                     position = int(line['Position']) if line['Position'] else None)
                    p.save()
                            
            
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+").")
                continue
            
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return
            
    messages.add_message(request, messages.SUCCESS, "The file has been successfully inserted.")


def _update_upload_file(request, file_dict, attributes):
    """
      Update of locus data from the file.
    """
    with transaction.atomic():
        for line in file_dict:
            jsonvalues ={}
            try:
                with transaction.atomic():
                    locus_to_update = Locus.objects.get(name=str(line['Name']))
                    if str(line['Genome version']):
                        genome_version = GenomeVersion.objects.get(name=str(line['Genome version']))
                        position_to_update = LocusPosition.objects.get(locus=locus_to_update, genome_version=genome_version)
                    else:
                        # Case of a non positioned marker, only one LocusPosition object and no genome version
                        position_to_update = LocusPosition.objects.get(locus=locus_to_update, genome_version=None)
                        
                    
                    locus_to_update.comments = str(line['Comments'])
                    
                    for att in attributes:
                        value = line[str(att.name).strip()]
                        if value != None or value !='':
                            jsonvalues[att.id] = value   
                        else:
                            jsonvalues[att.id] = ''
                    if type(locus_to_update.attributes_values) is dict:
                        attributes_in_database = locus_to_update.attributes_values
                    else:    
                        attributes_in_database = json.loads(locus_to_update.attributes_values)
                    shared_items = set(jsonvalues.items()) & set(attributes_in_database.items()) #Cette liste contient les valeurs en commun pour chaque clé
                    if len(shared_items) < len(attributes_in_database):
                        locus_to_update.attributes_values = jsonvalues
                        
                    locus_to_update.save()
                    
                    position_to_update.chromosome = str(line['Chromosome'])
                    position_to_update.position = int(line['Position']) if line['Position'] else None
                    
                    position_to_update.save()
                        
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+").")
                    continue
                
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully uploaded and the locus have been updated.")


def write_locus_csv(locus_positions_qs, attributes, tag_fields, locustype):
    """
      CSV writer specific for locus management, to render the file via a thread.
    """
    filename = (locustype.name).lower().replace(' ','_')
    dialect = Dialect(delimiter = ';')
    
    _set_locus_attributes(locus_positions_qs, attributes)
    
    with open(settings.TEMP_FOLDER+filename+".csv", 'w', encoding='utf-8') as csv_file:
        writer = csv.writer(csv_file, dialect)
        writer.writerow(tag_fields['fields_label'])
        
        for locus_p in locus_positions_qs:
            row = []
            for attribute in tag_fields['fields_name']:
                try:
                    if attribute == 'genome_version' or attribute == 'chromosome' or attribute == 'position':
                        attr_value = locus_p.__getattribute__(attribute.lower().replace(' ','_'))
                    else:
                        attr_value = locus_p.locus.__getattribute__(attribute.lower().replace(' ','_'))
                except:
                    attr_value = ""
                
                row.append(attr_value)
                
            writer.writerow(row)


def _get_samples(request):
    samples = Sample.objects.select_related('seedlot').prefetch_related('projects').all()
        # Adding Paginator        
    try:
        if request.method == "GET":
            nb_per_page=request.GET['nb_per_page']  
        elif request.method == "POST":
            nb_per_page=request.POST["nb_per_page"]   
        if nb_per_page=="all":
            nb_per_page = len(samples)
    except:
        nb_per_page=50
    paginator = Paginator(samples, nb_per_page)
    page = request.GET.get('page')
    try:
        samples = paginator.page(page)
    except PageNotAnInteger:
        # If page number is not an interger then page one
        samples = paginator.page(1)
    except EmptyPage:
        # invalid page number show the last
        samples = paginator.page(paginator.num_pages)   
    number_all = (paginator.num_pages-1)*int(nb_per_page) + len(paginator.page(paginator.num_pages))
    return samples, number_all, nb_per_page


#=======================================================  
def _get_locus(locustype, attributes, request):
    locus_positions_qs = LocusPosition.objects.select_related('locus', 'genome_version').filter(locus__type=locustype).order_by('locus__name')
                
    # Adding Paginator  
    try:
        if request.method == "GET":
            nb_per_page=request.GET['nb_per_page']  
        elif request.method == "POST":
            nb_per_page=request.POST["nb_per_page"]   
        if nb_per_page=="all":
            nb_per_page = len(locus_positions_qs)
    except:
        nb_per_page=50
    
    
    paginator = Paginator(locus_positions_qs, nb_per_page)
    page = request.GET.get('page')
#     print("paginator: ",paginator.object_list)
    try:
        locus_positions = paginator.page(page)
    except PageNotAnInteger:
        # If page number is not an interger then page one
        locus_positions = paginator.page(1)
    except EmptyPage:
        # invalid page number show the last
        locus_positions = paginator.page(paginator.num_pages)
    
    _set_locus_attributes(locus_positions, attributes)
    
             
    return locus_positions, nb_per_page, locus_positions_qs
    

def _set_locus_attributes(locus_positions, attributes):
    i = 0
    for loc_pos in locus_positions:
        if i%10000 == 0:
            print(i, flush=True)
        for att in attributes:
            if loc_pos.locus.attributes_values :
                if type(loc_pos.locus.attributes_values) is dict:
                    attributes_values=loc_pos.locus.attributes_values
                else:
                    attributes_values =json.loads(loc_pos.locus.attributes_values)
                #si l'id de l'attribut "att" est présent dans les clés de attributes_values
                if str(att.id) in attributes_values.keys() :
                    try:
                        setattr(loc_pos.locus,att.name.lower().replace(' ','_'),attributes_values[str(att.id)])
                    except ValueError as e:
                        print(e)
                        setattr(loc_pos.locus,att.name.lower().replace(' ','_'),'')
                else :
                    setattr(loc_pos.locus,att.name.lower().replace(' ','_'),'')
            else :
                setattr(loc_pos.locus,att.name.lower().replace(' ','_'),'')
                 
        i += 1


@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def insert_locus_position(request):
    """
      View with form to upload a file that contains position information for locus. 
      The user must select a genome version to link to the positions of the markers.
      
      :var str title: title displayed on the template
      :var str template: html template for the rendering of the view
      :var form form_class: the file submission form
      :var dict tag_fields: dictionary containing the parameters passed to the template
      :var file file: file uploaded by the user in the form
      :var dict file_dict: dict version of the file, parsed by the CSVFactory
    """
    title = "Insert or Update Locus Position Information"
    template = 'genotyping/insert_locus_position.html'
    form_class = LocusPositionUploadForm
    
    if request.method == 'GET':
        form = form_class()
        
        tag_fields = {'title':title,
                      'form':form,
                      'filetype':"Locus_position",
                      'admin':True}
        
        return render(request, template, tag_fields) 
        
        
    elif request.method == 'POST':
        form = form_class(request.POST, request.FILES)
        
        tag_fields = {'title':title,
                      'form':form,
                      'filetype':"Locus_position",
                      'admin':True}
        
        if form.is_valid():
            genome_version_id = request.POST.get('genome_version')
            genome_version = GenomeVersion.objects.get(id=genome_version_id)
            file = request.FILES['file']
            
            myfactory = CSVFactoryForTempFiles(file)
            myfactory.find_dialect()
            myfactory.read_file()
            file_dict = myfactory.get_file()
            
            headers_list = filerenderer_headers['Locus_position']
            
            missing = myfactory.check_headers_conformity(headers_list)
            
            if missing != []:
                messages.add_message(request, messages.ERROR, "One or several mandatory headers are missing in your file : "+format(', '.join(missing))+\
                                     ". The required headers for this file are: "+format(', '.join(headers_list))+".")
                return redirect('insertlocusposition')

            if request.POST.get('submit'):
                submit_locus_position_info(request, file_dict, genome_version)

            elif request.POST.get('update'):
                update_locus_position_info(request, file_dict, genome_version)

        else:
            messages.add_message(request, messages.ERROR, "An error occurred with your file.")
        
        return render(request, template, tag_fields)


def submit_locus_position_info(request, file_dict, genome_version):
    """
      Function used to create locus positions from a file uploaded by the user.
      :param dict file_dict: file on dict format returned by the CSVFactory
      :param GenomeVersion file_dict: file on dict format returned by the CSVFactory
    """
    with transaction.atomic():       
        for line in file_dict:
            try:
                with transaction.atomic():
                    
                    locus = Locus.objects.get(name=line["Name"])
                    
                    position = LocusPosition.objects.create(locus=locus,
                                                            genome_version=genome_version,
                                                            chromosome=line["Chromosome"],
                                                            position=line["Position"])
                    
                    position.save()
                    
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+").")
                    continue
                
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully inserted.")
    
def update_locus_position_info(request, file_dict, genome_version):
    """
      Function used to update locus positions from a file uploaded by the user.
      :param dict file_dict: file on dict format returned by the CSVFactory
      :param GenomeVersion file_dict: file on dict format returned by the CSVFactory
    """
    with transaction.atomic():       
        for line in file_dict:
            try:
                with transaction.atomic():
                    
                    locus = Locus.objects.get(name=line["Name"])
                    
                    position = LocusPosition.objects.get(locus=locus,
                                                         genome_version=genome_version)
                    
                    position.chromosome = line["Chromosome"]
                    position.position = line["Position"]
                    
                    position.save()
                    
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+").")
                    continue
                
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return
            
    messages.add_message(request, messages.SUCCESS, "The file has been successfully uploaded and the locus positions have been updated.")


@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def genome_version_management(request):
    """
      View managing the creation of Genome versions.
    """
    form_class = GenomeVersionForm
    template = 'genotyping/genotyping_generic.html'

    return generic_management(GenomeVersion, form_class, request, template, {'admin':True}, "Genome Version Management")


@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def admin_genotyping_viewer(request):
    """
      View allowing the user to visualize genotyping data in admin mode.
    """
    title = "Admin Genotyping viewer"
    template = 'dataview/genotyping_viewer_new.html'
    is_admin = True
    parent_temp = "genotyping/genotyping_base.html"

    if request.method == 'GET':
        n_page = 1
        projects = list(Project.objects.all().values_list('id', flat=True))
        request.session['projects'] = projects
        tag_fields = display_genoviewer_form(request, projects, is_admin)
        
        tag_fields.update({'title': title,
                           'n_page':n_page,
                           'admin':True,
                           'parent_temp':parent_temp})
        
        return render(request,template,tag_fields)
    
    elif request.method == 'POST':
        #Get the actual page number
        n_page = int(request.POST['n_page'])
        
        if int(n_page) == 2:
            start_time = time.time()
            
            tag_fields, locus, genome_version = check_genoviewer_form_data(request, title, is_admin)
            
            if tag_fields == {}:
                return process_genoviewer_form_data(request, template, title, locus, genome_version, start_time, is_admin, parent_temp)
            
            else:
                tag_fields.update({'title': title,
                                   'admin':True,
                                   'parent_temp':parent_temp})
                
                return render(request,template,tag_fields)


def export_vcf(file_name, file, refname, deli, ref_id, matrix_orientation):
    print('MongoClient a remplacer')
    #TODO MongoClient a remplacer
#     #binary_file = open(file, "rb")
#     client = MongoClient(settings.MONGODB_DATABASES['default']['host'], settings.MONGODB_DATABASES['default']['port'])
#     db = client[settings.MONGODB_DATABASES['default']['name']]
#     if check_if_vcf_exists(db, file_name):
#         return 0
#     fs = gridfs.GridFSBucket(db)
#     fs.upload_from_stream(
#         file_name,
#         #binary_file,
#         file,
#         metadata={"contentType": "text/plain", "referential :" : refname})
#     client.close()
#     file.seek(0)
#     Thread(target=vcf_to_matrix, args=(file, refname, deli, ref_id, matrix_orientation)).start()
#     #vcf_to_matrix(file, refname, deli, ref_id, matrix_orientation)

#TODO gridfs depend de PyMongo et doit donc etre remplace
def check_if_vcf_exists(db ,name):
    print('Supprimer/remplacer gridfs')
#     fs = gridfs.GridFS(db)
#     return fs.exists(filename=name)


def vcf_to_matrix(fileobject, ref_name, deli, ref_id, matrix_orientation):
    byteobject = io.BytesIO(bytes(fileobject.read()))
    callset = allel.read_vcf(byteobject, alt_number = 1)
    print("Start VCF")
    file = open(settings.TEMP_FOLDER + "matrix.txt", "w+")
    file.write("\"Echantillons\"\t")
    loci = callset['variants/ID']  # colonnes matrices
    samples = callset['samples']  # lignes matrice
    ref = callset['variants/REF']
    alt = callset['variants/ALT']
    alleles = callset['calldata/GT']
    for locus in loci:
        file.write("\"" + locus + "\"" + "\t")
    file.write("\n")
    count = 0
    for sample in samples:
        all_l = sample + "\t"
        for x in range(0, len(alleles)):
            if alleles[x][count][0] == 0:
                all_l += "\"" + ref[x]
            elif alleles[x][count][0] == 1:
                all_l += "\"" + alt[x]
            else:
                all_l += "\"NA\""
            if alleles[x][count][1] == 0:
                all_l += ref[x] + "\""
            elif alleles[x][count][1] == 1:
                all_l += alt[x] + "\""
            all_l += "\t"
        all_l += "\n"
        count += 1
        file.write(all_l)
    path = settings.TEMP_FOLDER + "matrix.txt"
    print("END VCF")
    export_to_json(ref_name, path, deli, ref_id, matrix_orientation)
