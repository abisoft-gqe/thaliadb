#-*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.urls import path
from genotyping.views import genotypinghome, experiment_management, sample_management, referential_management, locustype,\
                             locusdata, genotypingid_management, genome_version_management, insert_locus_position,\
                             insert_genotypic_values, genotyping_matrix_template_filerenderer, locusattributes, locusattribute_edit, locustype_edit,\
                             LocusAttributesAutocomplete, admin_genotyping_viewer
from commonfct.genericviews import return_csv


urls_as_dict = {
    'genotypinghome': {'path':'home/', 'view':genotypinghome, 'docpath':'admin/genotyping.html'},
    'experiment_management': {'path':'experiment/', 'view':experiment_management, 'docpath':'admin/genotyping.html#experiment'},
    'sample_management': {'path':'sample/', 'view':sample_management, 'docpath':'admin/genotyping.html#sample'},
    'referential_management': {'path':'referential/', 'view':referential_management, 'docpath':'admin/genotyping.html#referential'},
    'insert_genotypic_values': {'path':'insertgenotypicvalues/', 'view':insert_genotypic_values, 'docpath':'admin/genotyping.html#insert-genotyping-data'},
    'locustype': {'path':'types/', 'view':locustype, 'docpath':'admin/genotyping.html#defining-locus-types-and-attributes' },
    'edit_locustype': {'path':'types/<int:pk>/', 'view':locustype_edit, 'docpath':'admin/genotyping.html#defining-locus-types-and-attributes'},
    'locusattributes': {'path':'attributes/', 'view':locusattributes, 'docpath':'admin/genotyping.html#defining-locus-types-and-attributes'},
    'edit_locusattribute': {'path':'attributes/<int:pk>/', 'view':locusattribute_edit, 'docpath':'admin/genotyping.html#defining-locus-types-and-attributes'},
    'locus-attributes-autocomplete': {'path':'attributes-autocomplete/', 'view':LocusAttributesAutocomplete.as_view(), },
    'locusdata': {'path':'locusdata/<int:type_id>/', 'view':locusdata, 'docpath':'admin/genotyping.html#locus-management'},
    'insertlocusposition': {'path':'insertlocusposition/', 'view':insert_locus_position, },
    'admin_genotyping_viewer': {'path':'admingenotypingviewer/', 'view':admin_genotyping_viewer, },
    'genotypingid_management': {'path':'genotypingid/', 'view':genotypingid_management, 'docpath':'genotyping.html#genotypingid' },
    'genomeversion_management': {'path':'genomeversion/', 'view':genome_version_management, },
    'return_csv': {'path':'return_csv/<int:filename>/', 'view':return_csv, },
    'matrixfilerenderer': {'path':'matrixfilerenderer/', 'view':genotyping_matrix_template_filerenderer, },
    }

urlpatterns = [path(url['path'], url['view'], name=namespace) for namespace, url in urls_as_dict.items()]


