#-*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.conf.urls import include
from django.contrib.auth import views as auth_views
from commonfct.genericviews import genericdelete, genericedit, getattributes, addattributes, typeedit,\
                                   get_empty_file, check_thread_activity
from commonfct.filerenderer import filerenderer, get_file, headersrenderer
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from phenotyping.views import get_ont_terms


from team.views import authentication, home, user_profile, mailbox, report_page
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
print("reading urls")
urlpatterns = [
    path('home/', home, name="home"),
    path('userprofile/', user_profile, name="user_profile"),
    path('mailbox/', mailbox, name="mailbox"),
    path('mailbox/<int:report_id>/', report_page, name="report_page"),

    path('', authentication, name='login' ),
    path('logout/', auth_views.LogoutView.as_view(next_page= settings.ROOT_URL), name='logout'),
    #path('contact/',include('team.urls')),
    #path('help/',include('team.urls')),
    path('team/', include('team.urls')),

    path('genealogy/', include('genealogy_managers.urls')),
    path('lot/', include('lot.urls')),
    path('phenotyping/', include('phenotyping.urls')),
    path('accession/', include('accession.urls')),
    path('images/', include('images.urls')),
    #Ajout Guy_Ross,
    path('genotyping/', include('genotyping.urls')),
    path('dataview/', include('dataview.urls')),
    path('classification/', include('classification.urls')),
    path('api/', include('wsBrAPI.urls')),
    #path('api_token_auth/', CustomAuthToken.as_view(), include('wsBrAPI.urls')),
    
    #Appels JQuery
    path('get_ont_terms/', get_ont_terms, name='get_ont_terms' ),
    path('genericdelete/', genericdelete, name='genericdelete' ),
    path('genericedit/', genericedit, name='genericedit' ),
    path('getattributes/', getattributes, name='getattributes' ),
    path('addattributes/', addattributes, name='addattributes' ),
    path('entitytypeedit/', typeedit, name='typeedit' ),
    path('checkthread/',check_thread_activity,name='check_thread'),
    
    path('getemptyfile/<str:classtype>/<int:object_id>/', get_empty_file, name='getemptyfile'),
    path('filerenderer/<str:filetype>/<int:type_id>/',filerenderer, name='filerenderer'),
    path('headersrenderer/<str:filetype>/',headersrenderer, name='headersrenderer'),
    path('getfile/<int:file_id>',get_file,name='getfile')
    
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),

    ] + urlpatterns
