from rest_framework import serializers

from team.models import Person, Project, Method


class ContactsSerializer(serializers.ModelSerializer):
    """
        ContactsSerializer gives the informations of the contact requested by the BrAPI
    """
    contactDbId = serializers.CharField(source = 'id')
    email = serializers.CharField(source = 'email_address')
    instituteName = serializers.EmailField(source = 'institutions')
    name = serializers.CharField(source = 'get_full_name')
    orcid = serializers.CharField(source = 'getBlankValue') 
    type = serializers.CharField(source = 'getBlankValue') 
    
    class Meta:
        model = Person
        fields = [
            'contactDbId',
            'email',
            'instituteName',
            'name',
            'orcid',
            'type',
            ]

class MethodsBrAPISerializer(serializers.ModelSerializer):
    """
        MethodsBrAPISerializer gives the informations of the method requested by the BrAPI
    """
    methodDbId = serializers.CharField(source = 'id')
    methodName = serializers.CharField(source = 'name')
    additionalInfo = serializers.JSONField(source = 'getMethodAdditionalInfo')
    bibliographicalReference = serializers.CharField(source = 'getBlankValue')
    description = serializers.CharField(source = 'getBlankValue')
    externalReferences = serializers.ListField(source = 'getExternalReferences')
    formula = serializers.CharField(source = 'getBlankValue')
    methodClass = serializers.CharField(source = 'getBlankValue')
    methodPUI = serializers.CharField(source = 'getBlankValue')
    ontologyReference = serializers.JSONField(source = 'getOntologyReference')
    
    class Meta :
        model = Method
        fields = [
            'methodDbId',
            'methodName',
            'additionalInfo',
            'bibliographicalReference',
            'description',
            'externalReferences',
            'formula',
            'methodClass',
            'methodPUI',
            'ontologyReference',
            ]

        
class PeopleBrAPISerializer(serializers.ModelSerializer):
    """
        PeopleBrAPISerializer gives the informations of the person requested by the BrAPI
    """
    additionalInfo = serializers.JSONField(source = 'getPeopleAdditionalInfo')
    description = serializers.CharField(source = 'note_on_person') 
    emailAddress = serializers.CharField(source = 'email_address') 
    externalReferences = serializers.ListField(source = 'getBlankList') 
    firstName = serializers.CharField(source = 'first_name') 
    lastName = serializers.CharField(source = 'last_name') 
    mailingAddress = serializers.CharField(source = 'getMailingAddress') 
    middleName = serializers.CharField(source = 'getBlankValue') 
    personDbId = serializers.CharField(source = 'id') 
    phoneNumber = serializers.CharField(source = 'work_phone') 
    userID = serializers.CharField(source = 'user.id') 
    
    class Meta :
        model = Person
        fields = [
            'additionalInfo',
            'description',
            'emailAddress',
            'externalReferences',
            'firstName',
            'lastName',
            'mailingAddress',
            'middleName',
            'personDbId',
            'phoneNumber',
            'userID',
            ]
        
class ProgramsBrAPISerializer(serializers.ModelSerializer):
    """
        ProgramsBrAPISerializer gives the informations of the project requested by the BrAPI
    """
    abbreviation = serializers.CharField(source = 'getBlankValue')
    additionalInfo = serializers.JSONField(source = 'getProgramAdditionalInfo') 
    commonCropName = serializers.CharField(source = 'getCommonCropName') 
    documentationURL = serializers.CharField(source = 'getDocumentationURL') 
    externalReferences = serializers.ListField(source = 'getBlankList') 
    fundingInformation = serializers.CharField(source = 'getBlankValue') 
    leadPersonDbId = serializers.CharField(source = 'getBlankValue') 
    leadPersonName = serializers.CharField(source = 'authors') 
    objective = serializers.CharField(source = 'getBlankValue') 
    programDbId = serializers.CharField(source = 'id') 
    programName = serializers.CharField(source = 'name') 
    programType = serializers.CharField(source = 'getProgramTypeValue') 
    
    class Meta :
        model = Project
        fields = [
            'abbreviation',
            'additionalInfo',
            'commonCropName',
            'documentationURL',
            'externalReferences',
            'fundingInformation',
            'leadPersonDbId',
            'leadPersonName',
            'objective',
            'programDbId',
            'programName',
            'programType',
            ]     