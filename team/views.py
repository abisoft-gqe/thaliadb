# -*- coding: utf-8 -*-
"""LICENCE
    ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Elise Peluso

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

import csv
import smtplib
import datetime
import pytz

from _csv import Dialect
from rest_framework.authtoken.models import Token
from unidecode import unidecode 

from django.forms.models import model_to_dict
from django.db.models.query import QuerySet
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import AuthenticationForm
from django.http import HttpResponse
from django.http.response import HttpResponseRedirect
from django.utils.encoding import smart_str
from django.shortcuts import render, redirect
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import user_passes_test, login_required
from django.db import transaction
from django.apps import apps
from django.core.mail import send_mail
from django.db.models import Q
from django.conf import settings
from django.urls.base import reverse

from team.forms import PhenoenvForm, PhenotraitForm, UploadFileForm, ContactForm, SeedLotForm, SampleForm, ReferentialForm, GenotypingIDForm, UserForm, ExperimentForm, PersonForm, InstitutionForm, ProjectForm, UserCreateForm, UserUpdateForm, MethodForm, DocumentForm, DataFileForm, ClassificationForm
from team.models import Person, Institution, Project, User, Method, Documents, DataFile, ThreadReport
from commonfct.genericviews import generic_management
from commonfct.utils import get_fields, get_list_from_list_file
from dataview.forms import DtviewProjectForm, GenoViewerAccessionForm
from accession.models import Accession,AccessionType
from phenotyping.models import Environment, Trait,SpecificTreatment
from classification.models import Classification, Classe
from lot.models import Seedlot
from genotyping.models import Sample, GenotypingID, Experiment, Referential
from images.models import Image, ImageLink
from images.forms import ImageForm, ImageLinkForm


def authentication(request):
    """
        View allowing the authentication of a user.
        "home" is True so that the home tab is colored in orange and the user knows which tab he is on.
        
        :var string username: username gotten from the request.POST
        :var string password: password gotten from the request.POST
        :var model user: using the function authenticate from django, this variable gets an User object
        :var model personname: the name of the person authenticated
        :var int person_id: the id of the person authenticated
        :var queryset projects: the projects which the user have access to
    """
    if request.method == "POST" :
        #on recupere le username et le mot de passe depuis la requete POST
        username = request.POST['username']
        password = request.POST['password']
        #on utilise la fonction authenticate de django pour récupérer un objet User
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active: #on test si l'utilisateur a un statut actif
                login(request, user) #si oui on le log
                personname = User.objects.get(login=username).person
                person_id = User.objects.get(login=username).person_id
                projects = list(Project.objects.filter(users=person_id))
                if 'next' in request.POST :
                    next_url = request.POST['next']
                else :
                    next_url = None
                if next_url is not None :
                    return redirect(next_url)
                else :
                    return render(request,'team/home.html',{"projects":projects,"personname":personname,"home":True})
            else:
                #Sinon on renvoie le formulaire d'authentification avec un message d'erreur
                return render(request,'team/login.html',{'form':AuthenticationForm(data=request.POST),'message':'Authentication failed !'})
        else:
            #Sinon on renvoie le formulaire d'authentification avec un message d'erreur
            return render(request,'team/login.html',{'form':AuthenticationForm(data=request.POST),'message':'Authentication failed !'})
    elif request.method == "GET" :
        #En GET on affiche juste le formulaire d'authentification
        if 'next' in request.GET :
            return render(request,'team/login.html',{'form':AuthenticationForm(), 'next':request.GET.get('next')})
        else :
            return render(request,'team/login.html',{'form':AuthenticationForm()})


@login_required
def home(request):
    """
       View rendering the home page.
       "home" is True so that the home tab is colored in orange and the user knows which tab he is on.
    """
    return render(request,'team/home.html',{'home':True})


@login_required
def user_profile(request):
    """
      View listing the projects which the user has access to.
      "user_profile" is True so that the user profile tab is orange and the user knows which tab he is on.
      
      :var string login: get the login of the user
      :var model personname: model of the person authenticated
      :var int person_id: id of the person authenticated
      :var queryset projects: if the person is a superuser, he has access to all the projects and all of them are displayed. Else, only the projects he as access to are displayed and contained in this variable
    """

    template = 'team/user_profile.html'
    login = request.user
    personname = User.objects.get(login=login).person

    user_id = User.objects.get(login=login).id
    projects = list(Project.objects.filter(users=user_id))
    
    if request.method == "GET" :
        if login is not None and login.is_active :
            try :
                token = Token.objects.get(user=login)
            except Token.DoesNotExist:
                return render(request, template, {"projects":projects,"personname":personname,"user_profile":True})
            else :
                expiration_date = (token.created + datetime.timedelta(days=settings.AUTH_TOKEN_LIFE)).replace(tzinfo = pytz.UTC)
                print(expiration_date.tzinfo)
                print(datetime.datetime.now().tzinfo)
                has_expired = datetime.datetime.now().replace(tzinfo = pytz.UTC) > expiration_date
                
            print('token : '+str(token))
            return render(request, template, {'token': token, 'expired':has_expired, "projects":projects,"personname":personname,"user_profile":True})
        else :
            return render(request, template, {})
    
    elif request.method == "POST" :
        try :
            token = Token.objects.get(user=login)
        except Token.DoesNotExist:
            token = Token.objects.create(user=login)
        else :
            token.delete()
            token = Token.objects.create(user=login)
        return render(request, template, {'token':token, "projects":projects,"personname":personname,"user_profile":True})
    
    else:
        return render(request,template,{})


@login_required
def mailbox(request):
    """
      View allowing the user to view their reports.
    """
    template = 'team/mailbox.html'
    
    login = request.user.login
    user_id = User.objects.get(login=login).id
    
    thread_reports = ThreadReport.objects.filter(user=user_id).order_by('-date')
    
    names = ['subject','message','date']
    labels = ['Subject','Message','Date']
    
    new_reports = []
    for report in thread_reports:
        if report.new:
            new_reports.append(report.id)
    
    tag_fields = {'thread_reports':thread_reports,
                  'fields_name':names,
                  'fields_label':labels,
                  'new_reports': new_reports,
                  'mailbox':True}
    
    return render(request,template,tag_fields)


def report_page(request, report_id):
    """
      View showing the details of a report.
    """
    template = 'team/report_page.html'
    
    report = ThreadReport.objects.get(id=report_id)
    
    if report.new == True:
        report.new = False
        report.save()
    
    tag_fields = {'report':report,
                  'mailbox':True}
    
    return render(request,template,tag_fields)


@login_required
@user_passes_test(lambda u: u.is_team_admin_user(), login_url='/login/')
def teamhome(request):
    """
      View displaying the home page.
      "admin" is True so that the admin tab is orange and the user knows which tab he is on.
    """
    return render(request,'team/team_base.html',{"admin":True})


@login_required
@user_passes_test(lambda u: u.is_team_admin_user(), login_url='/login/')
def person_management(request):
    """
      View managing the person model. It allows the edition, deletion or insertion of persons based on the Person Model.
      The filters are applied to this view. The user can make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var form form: the person form
      :var queryset persons: contains all the persons of the database
      :var int number_all: contains the number of persons of the database and is used, mostly, for the pagination
      :var int nb_per_page: initialized to 10, it is used for the pagination, it represents the number of data to display per page
      :var list labels: variable containing the field names of the form
      :var list list_attributes: list containing the names of the fields which can be searched with the refine search
      :var dict dico_get: dictionary containing the data obtained with the GET request method
    """
    form = PersonForm()
    persons = Person.objects.all()
    number_all = len(persons)
    nb_per_page = 10
    names, labels = get_fields(form)
    template = 'team/team_generic.html'
    list_attributes=['first name', 'last name', 'initial']
    
    tag_fields = {'all':persons,
                  'fields_name':names,
                  'fields_label':labels,
                  'title': "Person management", "admin":True,"refine_search":True,}
    if request.method == "GET":
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({"search_result":dico_get,
                           "or_and":or_and,
                           'all':persons,
                           "number_all":number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           "list_attributes":list_attributes})

            return render(request,template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
#                 if or_and in request.GET:
#                     dico_get[]
            length_dico_get = len(dico_get)
            nb_per_page = 10
            if or_and == "or":
                proj=[]
                query=Q()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    for data_type, value in dico_type_data.items():
                        var_search = str( data_type + "__icontains" ) 
                        var_search = var_search.replace(' ','_')
                        query |= Q(**{var_search : dico_type_data[data_type]})
            
            if or_and == "and":
                proj=[]
                query=Q()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    for data_type, value in dico_type_data.items():
                        var_search = str( data_type + "__icontains" ) 
                        var_search = var_search.replace(' ','_')
                        query &= Q(**{var_search : dico_type_data[data_type]})

            persons = Person.objects.filter(query).distinct()
            number_all = len(persons)
            list_exceptions = ['user','datafile','classification','referential','experiment']
            download_not_empty = False
            if persons:
                download_not_empty = write_csv_search_result(persons, list_exceptions)
            tag_fields.update({"download_not_empty":download_not_empty})
        #Cas où veut afficher la base de données
        #accessions = _get_accessions(accessiontype, attributes, request)
        tag_fields.update({"search_result":dico_get,
                           "or_and":or_and,
                           'all':persons,
                           "number_all":number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           "list_attributes":list_attributes})  
        return render(request, template, tag_fields)
    return generic_management(Person, PersonForm, request, 'team/team_generic.html',{"refine_search":True,"list_attributes":list_attributes,"admin":True},"Person Management")


@login_required
@user_passes_test(lambda u: u.is_team_admin_user(), login_url='/login/')
def institution_management(request):
    """
      View managing the institution model. It allows the edition, deletion or insertion of institutions based on the Institution Model.
      The filters are applied to this view. The user can make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var form form: the institution form based on the Institution model
      :var queryset institutions: list of all the institutions from the database
      :var int number_all: number of all the institutions from the database
      :var int nb_per_page: number of data to display per page, initialized to 10
      :var list labels: list of the names of each field from the form
      :var list list_attributes: list of the fields the user can search with the refine search module  
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """

    form = InstitutionForm()
    institutions = Institution.objects.all()
    number_all = len(institutions)
    nb_per_page = 10
    names, labels = get_fields(form)
    template = 'team/team_generic.html'
    list_attributes=[]
    for i in ['name', 'acronym', 'address', 'postal code', 'city', 'country', 'web site', 'email address']:
        list_attributes.append(i)
    
    tag_fields = {'all':institutions,
                  'fields_name':names,
                  "refine_search":True,
                  'fields_label':labels,
                  'title': "Institution management", "admin":True,}
    if request.method == "GET":
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({"search_result":dico_get,
                           "or_and":or_and,
                           'all':institutions,
                           "number_all":number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           "list_attributes":list_attributes})

            return render(request,template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
#                 if or_and in request.GET:
#                     dico_get[] 
            length_dico_get = len(dico_get)
            nb_per_page = 10
            if or_and == "or":
                proj=[]
                query=Q()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    for data_type, value in dico_type_data.items():
                        var_search = str( data_type + "__icontains" ) 
                        var_search = var_search.replace(' ','_')
                        query |= Q(**{var_search : dico_type_data[data_type]})
            
            if or_and == "and":
                proj=[]
                query=Q()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    for data_type, value in dico_type_data.items():
                        var_search = str( data_type + "__icontains" ) 
                        var_search = var_search.replace(' ','_')
                        query &= Q(**{var_search : dico_type_data[data_type]})

            institutions = Institution.objects.filter(query).distinct()
            number_all = len(institutions)
            list_exceptions = ['person', 'project', 'experiment']
            download_not_empty = False
            if institutions:
                download_not_empty = write_csv_search_result(institutions, list_exceptions)
            tag_fields.update({"download_not_empty":download_not_empty})
        #Cas où veut afficher la base de données
        #accessions = _get_accessions(accessiontype, attributes, request)
        tag_fields.update({"search_result":dico_get,
                           "or_and":or_and,
                           'all':institutions,
                           "number_all":number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           "list_attributes":list_attributes})  
        return render(request, template, tag_fields)
    return generic_management(Institution, InstitutionForm, request, 'team/team_generic.html',{"admin":True},"Institution Management")


def write_csv_search_result(model_data, list_exceptions):
    """
      Function allowing the writing of the csv file from the refine search.
      
      :param model model_data: model corresponding to the data required 
      :param list list_exceptions: list containing the model fields which can't be displayed in the csv file
      
      :var list list_col: headers of the csv file
      :var dict dico: model_data turned into a dictionary
      :var list row: list containing the elements of each row to be written in the csv file
    """
    list_col = [f.name for f in model_data[0]._meta.get_fields()]
    for element_to_remove in list_exceptions:
        list_col.remove(element_to_remove)
    
    with open(settings.TEMP_FOLDER+"/result_search.csv", 'w', encoding='utf-8') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(list_col)
        for i in model_data:
            row=[]
            dico=model_to_dict(i)
#             print("dico: ",dico)
            for key, value in dico.items():
#                 print(model_data.model.__name__)
                if key=="type" and model_data.model.__name__=='Accession':
                    dico[key]=AccessionType.objects.get(id=dico[key]).name
                if key=="specific_treatment" and model_data.model.__name__=="Environment":
                    dico[key]=SpecificTreatment.objects.get(id=dico[key]).name
                if not dico[key]:
                    dico[key]=""
                elif isinstance(dico[key], QuerySet):
                    list_qs = []
                    for queryset_i in dico[key]:
                        try:
                            list_qs.append(queryset_i.name)
                        except:
                            list_qs.append(queryset_i.login)
                    dico[key]=', '.join(list_qs)
            for col in list_col:
                if isinstance(model_data,QuerySet):
                    get_model_name = model_data.model.__name__
                else:
                    get_model_name = model_data.__class__.__name__
                if col == "person" and get_model_name == "User":
                    dico[col]=Person.objects.get(id=dico[col])
                row.append(dico[col])
            writer.writerow(row) 
    download_not_empty = True
    return download_not_empty


@login_required
@user_passes_test(lambda u: u.is_team_admin_user(), login_url='/login/')
def project_management(request):
    """
      View managing the projects where the user can edit,delete and insert data into the database based on the Project model.
      The filters are applied to this view. The user can make a refined request on the table.
      The request method used for the modification of the database is a POST request method while the refine search uses a GET request method.
      
      :var form form: the projects form based on the Project model
      :var queryset projects: list of all the projects from the database
      :var int number_all: number of all the projects from the database
      :var int nb_per_page: number of data to display per page, initialized to 10
      :var list labels: list of the names of each field from the form
      :var list list_attributes: list of the fields the user can search with the refine search module  
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
    """
    form = ProjectForm()
    projects = Project.objects.all()
    number_all = len(projects)
    nb_per_page = 10
    names, labels = get_fields(form)
    template = 'team/team_generic.html'
    list_attributes=[]
    for i in ['name', 'authors', 'start date', 'end date', 'description', 'users', 'institutions']:
        list_attributes.append(i)
    
    tag_fields = {'all':projects,
                  'fields_name':names,
                  'fields_label':labels,
                  'title': "Project management", 
                  "refine_search":True,
                  "admin":True,}
    if request.method == "GET":
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({"search_result":dico_get,
                           "or_and":or_and,
                           'all':projects,
                           "number_all":number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           "list_attributes":list_attributes})

            return render(request,template,tag_fields)
        
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
#                 if or_and in request.GET:
#                     dico_get[]
            length_dico_get = len(dico_get)
            nb_per_page = 10
            if or_and == "or":
                proj=[]
                query=Q()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    for data_type, value in dico_type_data.items():
                        if data_type == "users":
                            list_proj = list(set(Project.objects.filter(users=User.objects.filter(login__in=dico_type_data[data_type]))))
                            var_search = "name__in"
                            data_search = list_proj
                        elif data_type == "institutions":
                            list_proj = list(set(Project.objects.filter(institutions__in=Institution.objects.filter(name__icontains=dico_type_data[data_type]))))
                            var_search = "name__in"
                            data_search = list_proj
                        else:
                            var_search = str( data_type + "__icontains" ) 
                            var_search = var_search.replace(' ','_')
                            data_search = dico_type_data[data_type]
                        query |= Q(**{var_search : data_search})
            
            if or_and == "and":
                proj=[]
                query=Q()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    for data_type, value in dico_type_data.items():
                        if data_type == "users":
                            list_proj = list(set(Project.objects.filter(users__in=User.objects.filter(login__icontains=dico_type_data[data_type]))))
                            var_search = "name__in"
                            data_search = list_proj
                        elif data_type == "institutions":
                            list_proj = list(set(Project.objects.filter(institutions__in=Institution.objects.filter(name__icontains=dico_type_data[data_type]))))
                            var_search = "name__in"
                            data_search = list_proj
                        else:
                            var_search = str( data_type + "__icontains" ) 
                            var_search = var_search.replace(' ','_')
                            data_search = dico_type_data[data_type]
                        query &= Q(**{var_search : data_search})

            projects = Project.objects.filter(query).distinct()
            
            number_all = len(projects)
            list_exceptions = ["id","accession","seedlot","environment","trait","classification","sample","referential","experiment","genotypingid"]
            download_not_empty = False
            if projects:
                download_not_empty = write_csv_search_result(projects, list_exceptions)
            tag_fields.update({"download_not_empty":download_not_empty})
        #Cas où veut afficher la base de données
        #accessions = _get_accessions(accessiontype, attributes, request)
        tag_fields.update({"search_result":dico_get,
                           "or_and":or_and,
                           'all':projects,
                           "number_all":number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           "list_attributes":list_attributes})  
        return render(request, template, tag_fields)
    return generic_management(Project, ProjectForm, request, 'team/team_generic.html',{"admin":True},"Project Management")


@login_required
@user_passes_test(lambda u: u.is_team_admin_user(), login_url='/login/')
def method_management(request):
    """
      View managing the projects where the user can edit, delete and insert data into the database based on the Method model.
    """
    return generic_management(Method, MethodForm, request, 'team/team_generic.html',{"admin":True},"Method Management")


@login_required
@user_passes_test(lambda u: u.is_team_admin_user(), login_url='/login/')
def doc_management(request):
    """
      View managing the documents where the user can edit, delete and insert data into the database based on the Document model.
    """
    return generic_management(Documents, DocumentForm, request, 'team/team_generic.html',{"admin":True,'special_edit':'file_manager'},"Document File Management")


@login_required
@user_passes_test(lambda u: u.is_team_admin_user(), login_url='/login/')
def file_manager(request, pk):
    fileform = DataFileForm()
    template = 'team/team_generic.html'
    fields_name, fields_label = get_fields(fileform)
    doc = Documents.objects.get(id=int(pk))
    allfiles = DataFile.objects.filter(document=doc)
    tag_fields = {'all':allfiles,
                  'fields_name':fields_name,
                  'fields_label':fields_label,
                  'title':"Add files to document ({0})".format(doc.name),
                  'editmode':'view',
                  'urlname':'editfile'}
    if request.method == "GET":
        tag_fields.update({"admin":True,'form':fileform,'creation_mode':True})
        return render(request,template,tag_fields)
    elif request.method == "POST":
        fileform = DataFileForm(request.POST, request.FILES)
        if fileform.is_valid():
            newfile = fileform.save()
            try :
                newfile.document = doc
                newfile.save()
            except :
                transaction.rollback()
                errormsg = "Impossible to create this file"
                tag_fields.update({"admin":True,'form':DataFileForm(),'creation_mode':True,'errormsg':errormsg})
                return render(request,template,tag_fields)
            else :
                tag_fields.update({"admin":True,'form':DataFileForm(),'creation_mode':True})
                return render(request,template,tag_fields)
        else :
            tag_fields.update({'form':fileform,'creation_mode':False,"admin":True})
            return render(request,template,tag_fields)


def editfile(request, pk):
    template = 'team/team_generic.html'
    fileobj = DataFile.objects.get(id=pk)
    if request.method == "GET":
        fileobj = DataFile.objects.get(id=pk)
        allfiles = DataFile.objects.filter(document=fileobj.document)
        fileform = DataFileForm(instance=fileobj)
        fields_name, fields_label = get_fields(fileform)
        tag_fields = {'all':allfiles,
                  'fields_name':fields_name,
                  'fields_label':fields_label,
                  'title':"Edit files to document ({0})".format(fileobj.document.name),
                  "admin":True,
                  'form':fileform,
                  'creation_mode':False,
                  'urlname':'editfile'}
        return render(request,template,tag_fields)
    else :
        fileform = DataFileForm(request.POST, request.FILES, instance=fileobj)
        if fileform.is_valid() :
            fileform.save()
            return HttpResponseRedirect(reverse('file_manager', args=[fileobj.document.id]))
        else :
            fileobj = DataFile.objects.get(id=pk)
            allfiles = DataFile.objects.filter(document=fileobj.document)
            fields_name, fields_label = get_fields(fileform)
            tag_fields = {'all':allfiles,
                  'fields_name':fields_name,
                  'fields_label':fields_label,
                  'title':"Edit files to document ({0})".format(fileobj.document.name),
                  "admin":True,
                  'form':fileform,
                  'creation_mode':False}
            return render(request,template,tag_fields)


@login_required
def file_renderer(request,filename):
    fileresource = DataFile.objects.get(datafile=filename)
    response = HttpResponse(mimetype='application/octet-stream')
    response['Content-Disposition'] = 'attachment; filename=%s' % smart_str(filename.split('/')[-1])
    stream = open(fileresource.datafile.path,'r')
    response.write(stream.read())
    # It's usually a good idea to set the 'Content-Length' header too.
    # You can also set any other required headers: Cache-Control, etc.
    return response


@login_required
@user_passes_test(lambda u: u.is_team_admin_user(), login_url='/login/')
def user_management(request):
    """
      View managing the users where the user can edit, delete and insert data into the database based on the User model.
      Thanks to the User model and this view, superusers are created or some rights are given.
      The password can't be edited. To create a new password, you have to use the python shell of the application. (user.set_password())
    
      :var form form: the user form based on the User model
      :var queryset projects: list of all the users from the database
      :var int number_all: number of all the users from the database
      :var int nb_per_page: number of data to display per page, initialized to 10
      :var list labels: list of the names of each field from the form
      :var list list_attributes: list of the fields the user can search with the refine search module  
      :var bool download_not_empty: boolean allowing or not the downloading of the refine search file
      :var dict tag_fields: dictionary containing the parameters passed to the template
    """
    form = UserCreateForm()
    users = User.objects.all()
    number_all = len(users)
    nb_per_page = 10
    names, labels = get_fields(form)
    template = 'team/team_generic.html'
    #dans le formulaire UserForm on supprime tout de même le champ password_bis
    #names.remove(f['password_bis'].name)
    #labels.remove(f['password_bis'].label)
    
    names.remove(form['password'].name)
    labels.remove(form['password'].label)
    list_attributes=[]
    for i in ['person', 'login']:
        list_attributes.append(i)
    tag_fields = {'all':users,
                  'fields_name':names,
                  'fields_label':labels,
                  'title': "User management", "admin":True,
                  "refine_search":True,}
    if request.method == "GET":
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            tag_fields.update({"search_result":dico_get,
                           "or_and":or_and,
                           'all':users,
                           "number_all":number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           "list_attributes":list_attributes})

            return render(request,template,tag_fields)
    
        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
#                 if or_and in request.GET:
#                     dico_get[]
            length_dico_get = len(dico_get)
            nb_per_page = 10
            if or_and == "or":
                proj=[]
                query=Q()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    for data_type, value in dico_type_data.items():
                        var_search = str( data_type + "__icontains" ) 
                        if data_type == 'person':
                            query_person=Q()
                            query_person |= Q(last_name__icontains=dico_type_data[data_type])
                            query_person |= Q(first_name__icontains=dico_type_data[data_type])
                            query |= Q(person__in=Person.objects.filter(query_person))
                        else:
                            query |= Q(**{var_search : dico_type_data[data_type]})
            
            if or_and == "and":
                proj=[]
                query=Q()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    for data_type, value in dico_type_data.items():
                        var_search = str( data_type + "__icontains" ) 
                        if data_type == 'person':
                            query_person=Q()
                            query_person |= Q(last_name__icontains=dico_type_data[data_type])
                            query_person |= Q(first_name__icontains=dico_type_data[data_type])
                            query &= Q(person__in=Person.objects.filter(query_person))
                        else:
                            query &= Q(**{var_search : dico_type_data[data_type]})

            users = User.objects.filter(query).distinct()
            number_all = len(users)
            list_exceptions = ['project','logentry','start_date','password','end_date','id','last_login']
            download_not_empty = False
            if users:
                download_not_empty = write_csv_search_result(users, list_exceptions)
            tag_fields.update({"download_not_empty":download_not_empty})
        #Cas où veut afficher la base de données
        #accessions = _get_accessions(accessiontype, attributes, request)
        tag_fields.update({"search_result":dico_get,
                           "or_and":or_and,
                           'all':users,
                           "number_all":number_all,
                           'nb_per_page':nb_per_page,
                           'creation_mode':True,
                           'form':form,
                           "list_attributes":list_attributes})  

        return render(request,template,tag_fields)
    
    elif request.method == "POST":
        if "hiddenid" in request.POST.keys() :
            try :
                #on récupère l'objet déjà existant puis on utilise les fonctionnalités du formulaire pour l'updater
                instance = User.objects.get(id=request.POST['hiddenid'])
                form = UserUpdateForm(instance=instance, data=request.POST)
                if form.is_valid():
                    form.save()
                    tag_fields.update({'form':form, 'creation_mode':False, 'object':instance, 'hiddenid':instance.id})
                    return render(request,template,tag_fields)
                else :
                    #sinon on génère un message d'erreur qui sera affiché
                    errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form.errors.items() ])
                    tag_fields.update({'form':form, 'creation_mode':False, 'hiddenid':instance.id, 'errormsg':errormsg})
                    return render(request,template,tag_fields)
            except ObjectDoesNotExist :
                tag_fields.update({'form':form, 'creation_mode':True, 'errormsg':"The object you try to update doesn't exist in the database"})
                return render(request,template,tag_fields)
        else :
            form = UserCreateForm(data=request.POST)
            if form.is_valid():
                username = request.POST['login']
                password = request.POST['password']
                person = Person.objects.get(id=int(request.POST['person']))
                
                extrafields = {}
                extralist = ['is_team_admin','is_accession_admin','is_seedlot_admin','is_classification_admin','is_phenotyping_admin','is_genotyping_admin']
                
                #traitement des cases à cocher: si la case n'est pas cochée, aucune référence dans le request.POST (le field n'existe même pas)
                for field in extralist :
                    if field in request.POST.keys() and request.POST[field] == 'on':
                        extrafields[field] = True
                    else :
                        extrafields[field] = False
                
                User.objects.create_user_with_person(username, password, person, **extrafields)
                tag_fields.update({'form':UserCreateForm(), 'creation_mode':True})
                return render(request, template, tag_fields)
            else :
                form = UserCreateForm(data=request.POST)
                tag_fields.update({'form':form, 'creation_mode':True})
                return render(request, template, tag_fields)


@login_required
@user_passes_test(lambda u: u.is_team_admin_user(), login_url='/login/')            
def link_data(request):
    """
      This view allows the link between data and projects. The users can link users, accessions, seedlots, samples, genotypingIDs, experiments, environments, traits
      and classification data to the project(s) they want.
      A table with data already linked to the existing projects can be displayed. 
      Data can be linked by uploading a file containing the data or they can be added one by one thanks to the form.
    """
    template = 'team/link_data.html'
    formproject=DtviewProjectForm()
    referentialform = ReferentialForm()
    accesform=GenoViewerAccessionForm()
    seedform=SeedLotForm()
    sampleform=SampleForm()
    expform=ExperimentForm()
    genoIDform=GenotypingIDForm()
    userform=UserForm()
    pheno_envform=PhenoenvForm()
    pheno_traitform=PhenotraitForm()
    formfile=UploadFileForm()
    classificationform=ClassificationForm()
    projects_id=[]
    dico={}
    projects_tab={}
    dico_accession_projects={}
    dico_sl_projects={}
    dico_sample_projects={}
    dico_experiment_projects={}
    dico_genoid_projects={}
    dico_env_projects={}
    dico_trait_projects={}
    dico_ref_projects={}
    dico_classification_projects={}
    
    for i in Project.objects.all():
        projects_tab[i.name]=i.id
    
    for name, id in projects_tab.items():
        accession_filter=Accession.objects.filter(projects=id)
        seedlot_filter=Seedlot.objects.filter(projects=id)
        sample_filter=Sample.objects.filter(projects=id)
        experiment_filter=Experiment.objects.filter(projects=id)
        genoid_filter=GenotypingID.objects.filter(projects=id)
        env_filter=Environment.objects.filter(projects=id)
        trait_filter=Trait.objects.filter(projects=id)
        ref_filter=Referential.objects.filter(projects=id)
        classification_filter=Classification.objects.filter(projects=id)
        dico_accession_projects[name]={id:", ".join([g.name for g in accession_filter])}
        dico_sl_projects[name]={id:", ".join([g.name for g in seedlot_filter])}
        dico_sample_projects[name]={id:", ".join([g.name for g in sample_filter])}
        dico_experiment_projects[name]={id:", ".join([g.name for g in experiment_filter])}
        dico_genoid_projects[name]={id:", ".join([g.name for g in genoid_filter])}
        dico_env_projects[name]={id:", ".join([g.name for g in env_filter])}
        dico_trait_projects[name]={id:', '.join([g.name for g in trait_filter])}
        dico_ref_projects[name]={id:', '.join([g.name for g in ref_filter])}
        dico_classification_projects[name]={id:', '.join([g.name for g in classification_filter])}
    
    projects_all=Project.objects.all()
    dico_user_projects={}
    
    for nom_project in projects_all:
        dico_user_projects[nom_project.name]={nom_project.id:", ".join([g.login for g in nom_project.users.filter()])}
    
    dico_names_csv={settings.TEMP_FOLDER+'/dict_acc.csv':dico_accession_projects,settings.TEMP_FOLDER+'/dict_sl.csv':dico_sl_projects,settings.TEMP_FOLDER+'/dict_sample.csv':dico_sample_projects,
                    settings.TEMP_FOLDER+'/dict_exp.csv':dico_experiment_projects,settings.TEMP_FOLDER+'/dict_genoid.csv':dico_genoid_projects,settings.TEMP_FOLDER+'/dict_env.csv':dico_env_projects,
                    settings.TEMP_FOLDER+'/dict_trait.csv':dico_trait_projects,settings.TEMP_FOLDER+'/dict_user.csv':dico_user_projects,settings.TEMP_FOLDER+'/dict_ref.csv':dico_ref_projects,
                    settings.TEMP_FOLDER+'/dict_classification.csv':dico_classification_projects}
    
    for name_file, dico in dico_names_csv.items():
        write_csv_from_dict(name_file,**dico)
    
    projects=[i.id for i in Project.objects.all()]    
    objects_acc=set(Accession.objects.filter(projects__in=projects))
    objects_sl=set(Seedlot.objects.filter(projects__in=projects))
    objects_sample=set(Sample.objects.filter(projects__in=projects))
    objects_exp=set(Experiment.objects.filter(projects__in=projects))
    objects_geno=set(GenotypingID.objects.filter(projects__in=projects))
    objects_env=set(Environment.objects.filter(projects__in=projects))
    objects_trait=set(Trait.objects.filter(projects__in=projects))
    objects_ref=set(Referential.objects.filter(projects__in=projects))
    objects_classification=set(Classification.objects.filter(projects__in=projects))
    dico_user={}
    
    for i in User.objects.all():
        dico_user[i]=', '.join([g.name for g in Project.objects.filter(users=i)])
    
    if request.method=="GET":
        formproject=DtviewProjectForm()
        formproject_sl=formProjects("id_project_sl")
        formproject_sple=formProjects("id_project_sple")
        formproject_geno=formProjects("id_project_geno")
        formproject_exp=formProjects("id_project_exp")
        formproject_user = formProjects("id_project_user")
        formproject_env = formProjects('id_project_env')
        formproject_trait = formProjects('id_project_trait')
        formproject_ref = formProjects('id_project_ref')
        formproject_classification = formProjects('id_project_classification')
        dico={"formfile":formfile,
              "userform":userform,
              "genoIDform":genoIDform,
              "expform":expform,
              "sampleform":sampleform,
              "seedform":seedform,
              "pheno_envform":pheno_envform,
              "pheno_traitform":pheno_traitform,
              'formproject':formproject,
              'pheno_envform':pheno_envform,
              "pheno_traitform":pheno_traitform,
              'referentialform':referentialform,
              'classificationform':classificationform,
              "formproject_sl":formproject_sl,
              "formproject_sple":formproject_sple,
              "formproject_geno":formproject_geno,
              "formproject_exp":formproject_exp,
              "formproject_user":formproject_user,
              "formproject_env":formproject_env,
              "formproject_trait":formproject_trait,
              'formproject_ref':formproject_ref,
              'formproject_classification':formproject_classification,
              "dico_accession_projects":dico_accession_projects,
              "dico_sl_projects":dico_sl_projects,
              "dico_user_projects":dico_user_projects,
              "dico_sample_projects":dico_sample_projects,
              "dico_experiment_projects":dico_experiment_projects,
              "dico_genoid_projects":dico_genoid_projects,
              "dico_env_projects":dico_env_projects,
              "dico_trait_projects":dico_trait_projects,
              "dico_ref_projects":dico_ref_projects,
              "dico_classification_projects":dico_classification_projects,
              'accesform':GenoViewerAccessionForm(),
              'admin':True
            }   
        return render(request, template, dico)
    
    elif request.method=="POST":
        formf = UploadFileForm(request.POST, request.FILES)
        formproject=DtviewProjectForm(request.POST)
        referentialform = ReferentialForm(request.POST)
        accesform=GenoViewerAccessionForm(request.POST)
        seedform=SeedLotForm(request.POST)
        sampleform=SampleForm(request.POST)
        expform=ExperimentForm(request.POST)
        genoIDform=GenotypingIDForm(request.POST)
        userform=UserForm(request.POST)
        pheno_envform=PhenoenvForm(request.POST)
        pheno_traitform=PhenotraitForm(request.POST)
        classificationform=ClassificationForm(request.POST)
        submit=request.POST["submit"]
        
        if formf.is_valid():  
            file_uploaded = request.FILES.get('file')
        else:
            file_uploaded = ''

        if formproject.is_valid() and pheno_envform.is_valid() and pheno_traitform.is_valid() and accesform.is_valid() and seedform.is_valid() and sampleform.is_valid() and expform.is_valid() and genoIDform.is_valid() and userform.is_valid() and referentialform.is_valid() and classificationform.is_valid():    
            for i in request.POST.keys():
                if 'unlink' in i:
                    unlink_data=i
            
            unlink=request.POST[unlink_data]
            project=formproject.cleaned_data['project']
            formproject=DtviewProjectForm()
            formproject_sl=formProjects("id_project_sl")
            formproject_sple=formProjects("id_project_sple")
            formproject_geno=formProjects("id_project_geno")
            formproject_exp=formProjects("id_project_exp")
            formproject_user = formProjects("id_project_user")
            formproject_env = formProjects('id_project_env')
            formproject_trait = formProjects('id_project_trait')
            formproject_ref = formProjects('id_project_ref')
            formproject_classification = formProjects('id_project_classification')
            
            if not project:
                accesform = GenoViewerAccessionForm()
                seedform = SeedLotForm()
                sampleform = SampleForm()
                expform = ExperimentForm()
                genoIDform = GenotypingIDForm()
                userform = UserForm()
                pheno_envform = PhenoenvForm()
                pheno_traitform = PhenotraitForm()
                referentialform = ReferentialForm()
                classificationform = ClassificationForm()
                messages.add_message(request, messages.ERROR, "Please choose a project.")
                
                dico_template = {'admin':True,
                                 "formproject_classification":formproject_classification,
                                 "formproject_ref":formproject_ref,
                                 "formproject_env":formproject_env,
                                 "formproject_trait":formproject_trait,
                                 "formproject_sl":formproject_sl,
                                 "formproject_sple":formproject_sple,
                                 "formproject_geno":formproject_geno,
                                 "formproject_exp":formproject_exp,
                                 "formproject_user":formproject_user,
                                 "formfile":formfile,
                                 "classificationform":classificationform,
                                 "pheno_envform":pheno_envform,
                                 "pheno_traitform":pheno_traitform,
                                 "userform":userform,
                                 "genoIDform":genoIDform,
                                 "expform":expform,
                                 "sampleform":sampleform,
                                 "seedform":seedform,
                                 'formproject':formproject,
                                 'accesform':accesform}
                
                return render(request, template, dico_template)
            
            if submit=="Link user to projects":
                dataform=userform
                name_object="name_user"
                object_used="user"
                model_object=User
                name_id="user_id"
            if submit=="Link genotyping ID to projects":
                dataform=genoIDform
                name_object="name_genoID"
                object_used="genoID"
                model_object=GenotypingID
                name_id="genoid_id"
            if submit=="Link accessions to projects":    
                dataform=accesform
                name_object="name"
                object_used="accession"
                model_object=Accession
                name_id="accession_id"
            elif submit=="Link seedlots to projects":   
                dataform=seedform
                name_object="name_seedlot"
                object_used="seedlot"
                model_object=Seedlot
                name_id="lot_id"
            elif submit=="Link samples to projects":   
                dataform=sampleform
                name_object="name_sample"
                object_used="sample"
                model_object=Sample
                name_id="sample_id"
            elif submit=="Link experiments to projects":   
                dataform=expform
                name_object="name_xp"
                object_used="experiment"
                model_object=Experiment
                name_id="exp_id"
            elif submit=="Link environments to projects":   
                dataform=pheno_envform
                name_object="name_env"
                object_used="environment"
                model_object=Environment
                name_id="env_id"
            elif submit=="Link traits to projects":   
                dataform=pheno_traitform
                name_object="name_trait"
                object_used="trait"
                model_object=Trait
                name_id="trait_id"
            elif submit=="Link referentials to projects":   
                dataform=referentialform
                name_object="name_ref"
                object_used="referential"
                model_object=Referential
                name_id="ref_id"
            elif submit=="Link classifications to projects":   
                dataform=classificationform
                name_object="name_classification"
                object_used="classification"
                model_object=Classification
                name_id="classification_id"

            if file_uploaded:
                listobjects = get_list_from_list_file(file_uploaded.read())
            else:
                listobjects = dataform.cleaned_data[object_used]
            
            if not listobjects:
                referentialform = ReferentialForm()
                accesform=GenoViewerAccessionForm()
                seedform=SeedLotForm()
                sampleform=SampleForm()
                expform=ExperimentForm()
                genoIDform=GenotypingIDForm()
                userform=UserForm()
                pheno_envform=PhenoenvForm()
                pheno_traitform=PhenotraitForm()
                classificationform=ClassificationForm()
                
                messages.add_message(request, messages.ERROR, "Please choose an {0} to be linked.".format(object_used))
                dico_template={"formproject_sl":formproject_sl, 
                               "formproject_sple":formproject_sple, 
                               "formproject_geno":formproject_geno, 
                               "formproject_exp":formproject_exp, 
                               "formproject_user":formproject_user,
                               "formproject_env":formproject_env,
                               "formproject_trait":formproject_trait,
                               "formproject_ref":formproject_ref,
                               "formproject_classification":formproject_classification,
                               "formfile":formfile,
                               "userform":userform,
                               "genoIDform":genoIDform,
                               "expform":expform,
                               "sampleform":sampleform,
                               "seedform":seedform,
                               'formproject':formproject,
                               "pheno_envform":pheno_envform,
                               "pheno_traitform":pheno_traitform,
                               'accesform':GenoViewerAccessionForm(),
                               'referentialform':referentialform,
                               'classificationform':classificationform,
                               "dico_accession_projects":dico_accession_projects,
                               "dico_sl_projects":dico_sl_projects,
                               "dico_user_projects":dico_user_projects,
                               "dico_sample_projects":dico_sample_projects,
                               "dico_experiment_projects":dico_experiment_projects,
                               "dico_genoid_projects":dico_genoid_projects,
                               "dico_env_projects":dico_env_projects,
                               "dico_trait_projects":dico_trait_projects,
                               'dico_ref_projects':dico_ref_projects,
                               'dico_classification_projects':dico_classification_projects,
                               'admin':True}
                
                return render(request, template, dico_template)
            
            print(listobjects)
            
            error = False
            with transaction.atomic():
                for obj in listobjects:
                    try:
                        with transaction.atomic():
                            if obj.__class__ is not model_object and obj != '':
                                if model_object == User:
                                    obj=User.objects.get(login=obj)
                                else:
                                    obj=model_object.objects.get(name=obj)
                            
                            if obj.__class__ is model_object :
                                print("object: ",obj)
                                projects_objects = Project.objects.filter(id__in=project)
                                
                                if unlink=="unlink" :
                                    try :
                                        obj.projects.remove(*projects_objects)
                                    except AttributeError as ae :
                                        obj.project_set.remove(*projects_objects)
                                           
                                else:
                                    try :
                                        obj.projects.add(*projects_objects)
                                    except AttributeError as ae :
                                        obj.project_set.add(*projects_objects)

                    except Exception as e:
                        if str(e) != "":
                            messages.add_message(request, messages.ERROR, str(e)+" (for object: "+str(obj)+").")
                        continue
                  
                #To cancel all the transactions if there is an error message
                for msg in messages.get_messages(request):
                    if msg.level == 40:
                        error = True
                        transaction.set_rollback(True)
            
            if error:
                dico_template={"formproject_sl":formproject_sl, 
                               "formproject_sple":formproject_sple, 
                               "formproject_geno":formproject_geno, 
                               "formproject_exp":formproject_exp, 
                               "formproject_user":formproject_user,
                               "formproject_env":formproject_env,
                               "formproject_trait":formproject_trait,
                               "formproject_ref":formproject_ref,
                               "formproject_classification":formproject_classification,
                               "formfile":formfile,
                               "userform":userform,
                               "genoIDform":genoIDform,
                               "expform":expform,
                               "sampleform":sampleform,
                               "seedform":seedform,
                               "pheno_envform":pheno_envform,
                               "pheno_traitform":pheno_traitform,
                               'formproject':formproject,
                               "pheno_envform":pheno_envform,
                               "pheno_traitform":pheno_traitform,
                               'referentialform':referentialform,
                               'classificationform':classificationform,
                               'accesform':GenoViewerAccessionForm(),
                               "dico_accession_projects":dico_accession_projects,
                               "dico_sl_projects":dico_sl_projects,
                               "dico_user_projects":dico_user_projects,
                               "dico_sample_projects":dico_sample_projects,
                               "dico_experiment_projects":dico_experiment_projects,
                               "dico_genoid_projects":dico_genoid_projects,
                               "dico_env_projects":dico_env_projects,
                               "dico_trait_projects":dico_trait_projects,
                               "dico_ref_projects":dico_ref_projects,
                               "dico_classification_projects":dico_classification_projects,
                               'admin':True}
                
                return render(request, template, dico_template)
                        

            formproject=DtviewProjectForm()
            accesform=GenoViewerAccessionForm()
            seedform=SeedLotForm()
            sampleform=SampleForm()
            expform=ExperimentForm()
            genoIDform=GenotypingIDForm()
            userform=UserForm()
            pheno_envform=PhenoenvForm()
            pheno_traitform=PhenotraitForm()
            classificationform=ClassificationForm()
            
            for i in Project.objects.all():
                projects_tab[i.name]=i.id
            
            for name, id in projects_tab.items():
                accession_filter=Accession.objects.filter(projects=id)
                seedlot_filter=Seedlot.objects.filter(projects=id)
                sample_filter=Sample.objects.filter(projects=id)
                experiment_filter=Experiment.objects.filter(projects=id)
                genoid_filter=GenotypingID.objects.filter(projects=id)
                env_filter=Environment.objects.filter(projects=id)
                trait_filter=Trait.objects.filter(projects=id)
                ref_filter=Referential.objects.filter(projects=id)
                classification_filter=Classification.objects.filter(projects=id)
                dico_accession_projects[name]={id:", ".join([g.name for g in accession_filter])}
                dico_sl_projects[name]={id:", ".join([g.name for g in seedlot_filter])}
                dico_sample_projects[name]={id:", ".join([g.name for g in sample_filter])}
                dico_experiment_projects[name]={id:", ".join([g.name for g in experiment_filter])}
                dico_genoid_projects[name]={id:", ".join([g.name for g in genoid_filter])}
                dico_env_projects[name]={id:", ".join([g.name for g in env_filter])}
                dico_trait_projects[name]={id:', '.join([g.name for g in trait_filter])}
                dico_ref_projects[name]={id:', '.join([g.name for g in ref_filter])}
                dico_classification_projects[name]={id:', '.join([g.name for g in classification_filter])}
            
            projects_all=Project.objects.all()
            dico_user_projects={}
            for nom_project in projects_all:
                dico_user_projects[nom_project.name]={nom_project.id:", ".join([g.login for g in nom_project.users.filter()])}
            
            dico_names_csv={settings.TEMP_FOLDER+'/dict_acc.csv':dico_accession_projects,settings.TEMP_FOLDER+'/dict_sl.csv':dico_sl_projects,settings.TEMP_FOLDER+'/dict_sample.csv':dico_sample_projects,
                            settings.TEMP_FOLDER+'/dict_exp.csv':dico_experiment_projects,settings.TEMP_FOLDER+'/dict_genoid.csv':dico_genoid_projects,settings.TEMP_FOLDER+'/dict_env.csv':dico_env_projects,
                            settings.TEMP_FOLDER+'/dict_trait.csv':dico_trait_projects,settings.TEMP_FOLDER+'/dict_user.csv':dico_user_projects,settings.TEMP_FOLDER+'/dict_ref.csv':dico_ref_projects,
                            settings.TEMP_FOLDER+'/dict_classification.csv':dico_classification_projects}
            
            for name_file, dico in dico_names_csv.items():
                write_csv_from_dict(name_file,**dico)
           
            if unlink=="unlink":
                project=', '.join([Project.objects.get(id=projects.id).name for projects in set(project)])
                if object_used=="user":
                    print(listobjects)
                    try:
                        objects=", ".join([str(object_name.login) for object_name in listobjects])
                    except:
                        objects=", ".join([str(object_name) for object_name in listobjects]) 
                else:
                    try:
                        objects=', '.join([str(object_name.name) for object_name in listobjects])
                    except:
                        objects=", ".join([str(object_name) for object_name in listobjects])
                messages.add_message(request, messages.SUCCESS, "The {0}(s) {1} is(are) not linked to the project(s) {2} anymore.".format(object_used,objects,project))
            
            else:
                project=', '.join([Project.objects.get(id=projects.id).name for projects in set(project)])
                if object_used=="user":
                    print(listobjects)
                    try:
                        objects=", ".join([str(object_name.login) for object_name in listobjects])
                    except:
                        objects=", ".join([str(object_name) for object_name in listobjects])
                    
                else:
                    try:
                        print("listobjects: <<<<<<< : ",listobjects)
                        objects=", ".join([str(object_name.name) for object_name in listobjects])
                    except:
                        objects=", ".join([str(object_name) for object_name in listobjects])
                messages.add_message(request, messages.SUCCESS, "The {0}(s) {1} is(are) now linked to the project(s) {2}.".format(object_used,objects,project))

            projects=[i.id for i in Project.objects.all()]

            objects_acc=set(Accession.objects.filter(projects__in=projects))
            objects_sl=set(Seedlot.objects.filter(projects__in=projects))
            objects_sample=set(Sample.objects.filter(projects__in=projects))
            objects_exp=set(Experiment.objects.filter(projects__in=projects))
            objects_geno=set(GenotypingID.objects.filter(projects__in=projects))
            objects_env=set(Environment.objects.filter(projects__in=projects))
            objects_trait=set(Trait.objects.filter(projects__in=projects))
            objects_ref=set(Referential.objects.filter(projects__in=projects))
            objects_ref=set(Classification.objects.filter(projects__in=projects))

            for i in Project.objects.all():
                projects_tab[i.name]=i.id
            for name, id in projects_tab.items():
                accession_filter=Accession.objects.filter(projects=id)
                dico_accession_projects[name]={id:', '.join([g.name for g in accession_filter])}
                seedlot_filter=Seedlot.objects.filter(projects=id)
                dico_sl_projects[name]={id:', '.join([g.name for g in seedlot_filter])}
                sample_filter=Sample.objects.filter(projects=id)
                dico_sample_projects[name]={id:', '.join([g.name for g in sample_filter])}
                experiment_filter=Experiment.objects.filter(projects=id)
                dico_experiment_projects[name]={id:', '.join([g.name for g in experiment_filter])}
                genoid_filter=GenotypingID.objects.filter(projects=id)
                dico_genoid_projects[name]={id:', '.join([g.name for g in genoid_filter])}
                env_filter=Environment.objects.filter(projects=id)
                dico_env_projects[name]={id:', '.join([g.name for g in env_filter])}
                trait_filter=Trait.objects.filter(projects=id)
                dico_trait_projects[name]={id:', '.join([g.name for g in trait_filter])}
                ref_filter=Referential.objects.filter(projects=id)
                dico_ref_projects[name]={id:', '.join([g.name for g in ref_filter])}
                classification_filter=Classification.objects.filter(projects=id)
                dico_classification_projects[name]={id:', '.join([g.name for g in classification_filter])}
                
            projects_all=Project.objects.all()
            dico_user_projects={}
            
            for nom_project in projects_all:
                dico_user_projects[nom_project.name]={nom_project.id:", ".join([g.login for g in nom_project.users.filter()])}
            dico_template={"formproject_sl":formproject_sl, 
                           "formproject_sple":formproject_sple, 
                           "formproject_geno":formproject_geno, 
                           "formproject_exp":formproject_exp, 
                           "formproject_user":formproject_user,
                           "formproject_env":formproject_env,
                           "formproject_trait":formproject_trait,
                           "formproject_ref":formproject_ref,
                           "formproject_classification":formproject_classification,
                           "formfile":formfile,
                           "userform":userform,
                           "genoIDform":genoIDform,
                           "expform":expform,
                           "sampleform":sampleform,
                           "seedform":seedform,
                           "pheno_envform":pheno_envform,
                           "pheno_traitform":pheno_traitform,
                           'formproject':formproject,
                           'accesform':GenoViewerAccessionForm(),
                           "referentialform":referentialform,
                           "classificationform":classificationform,
                           "dico_accession_projects":dico_accession_projects,
                           "dico_sl_projects":dico_sl_projects,
                           "dico_user_projects":dico_user_projects,
                           "dico_sample_projects":dico_sample_projects,
                           "dico_experiment_projects":dico_experiment_projects,
                           "dico_genoid_projects":dico_genoid_projects,
                           "dico_env_projects":dico_env_projects,
                           "dico_trait_projects":dico_trait_projects,
                           "dico_ref_projects":dico_ref_projects,
                           "dico_classification_projects":dico_classification_projects,
                           'admin':True
                           }
            return render(request, template, dico_template)
        
        formproject=DtviewProjectForm()      
        formproject_sl=formProjects("id_project_sl")
        formproject_sple=formProjects("id_project_sple")
        formproject_geno=formProjects("id_project_geno")
        formproject_exp=formProjects("id_project_exp")
        formproject_user = formProjects("id_project_user")
        formproject_env = formProjects('id_project_env')
        formproject_trait = formProjects('id_project_trait')
        formproject_ref = formProjects('id_project_ref')
        formproject_classification = formProjects('id_project_classification')
        accesform=GenoViewerAccessionForm()
        dico={"formfile":formfile,
              "userform":userform,
              "pheno_envform":pheno_envform,
              "pheno_traitform":pheno_traitform,
              "genoIDform":genoIDform,
              "expform":expform,
              "sampleform":sampleform,
              "seedform":seedform,
              "pheno_envform":pheno_envform,
              "pheno_traitform":pheno_traitform,
              'referentialform':referentialform,
              'classificationform':classificationform,
              'formproject':formproject,
              "formproject_sl":formproject_sl,
              "formproject_sple":formproject_sple,
              "formproject_geno":formproject_geno,
              "formproject_exp":formproject_exp,
              "formproject_user":formproject_user,
              "formproject_env":formproject_env,
              "formproject_trait":formproject_trait,
              "formproject_ref":formproject_ref,
              "formproject_classification":formproject_classification,
              "dico_accession_projects":dico_accession_projects,
              "dico_sl_projects":dico_sl_projects,
              "dico_user_projects":dico_user_projects,
              "dico_sample_projects":dico_sample_projects,
              "dico_experiment_projects":dico_experiment_projects,
              "dico_genoid_projects":dico_genoid_projects,
              "dico_env_projects":dico_env_projects,
              "dico_trait_projects":dico_trait_projects,
              "dico_ref_projects":dico_ref_projects,
              "dico_classification_projects":dico_classification_projects,
              'accesform':GenoViewerAccessionForm(),
              'admin':True
        }    
    
        return render(request, template, dico)
    
    else:
        messages.add_message(request, messages.ERROR, "An error as occurred, please try again.")
        return redirect('link_data')
    
    

def formProjects(id_project):
    formproject=DtviewProjectForm()
    formproject.fields['project'].widget=FilteredSelectMultiple("Project", False, attrs={"id":id_project,'rows':'100','size':'15'})
    formproject.fields['project'].queryset=Project.objects.all()
    return formproject


def write_csv_from_dict(name_file,**dico):
    """
      This function is used to write a csv file from a dictionary
      
      :param str name_file: name of the file
      :param dict dico: dictionary containing the data to be written into the csv file
    """
    dialect = Dialect(delimiter = ';')
    with open(name_file, 'w', encoding='utf-8') as csv_file:
        writer = csv.writer(csv_file, dialect)
        for key, value in dico.items():
            for i, j in value.items():
                writer.writerow([key, j])
    

def uploadFile(files):
    """
      When a file is uploaded, this function is used to read the lines and append them into a list used by other views.
      
      :param file files: file uploaded containing the data used and appended into a list
    """
    if files != None:
        tabline = []
        i=0
        for line in files:
            try:
                line = line.decode()
            except (UnicodeDecodeError, AttributeError):
                pass

            tabline.append(line.strip())
            i+=1
        return tabline


def user_failed(request):
    """
      This view manages the failure of an authentication and returns the corresponding template.
    """
    template='team/failure.html'
    return render(request, template)


def contact(request):
    """
      This view allows the users to contact a competent person with who they can ask questions, ask for rights on some projects...
      
      :var form formContact: form based on the Contact model where the data are submitted
      :var str subject, message, sender, cc_myself, type_email: variables containing every information gotten thanks to the form
    """
    template="team/contact.html"
    formContact=ContactForm()
    
    if request.method=="POST":
        formContact=ContactForm(request.POST)
        if formContact.is_valid():
            subject = formContact.cleaned_data['subject']
            message = formContact.cleaned_data['message']
            sender = formContact.cleaned_data['sender']
            cc_myself = formContact.cleaned_data['cc_myself']
            type_email = formContact.cleaned_data['type_of_mail']
            if sender:
                message=str(message)+"\n\n\nsender: "+str(sender) #pour pouvoir répondre à l'adresse mail de l'utilisateur
            # si feedback, on n'envoie l'email qu'à certaines personnes
            if type_email=="1":
                recipients = ['thalia.devsi@gmail.com']
            else:
                recipients = ['alice.beaugrand@inra.fr','thalia.devsi@gmail.com']
            
            if cc_myself:
                recipients.append(sender)
        
            send_mail(subject, message, sender, recipients)
            return render(request, template, {"contact":True})
    return render(request, template,{"contact":True,"ContactForm":formContact, "contact":True})


def help(request):
    """
      This view returns the help template.
    """
    template="team/help.html"
    return render(request, template, {"help":True})
