# Generated by Django 2.2 on 2021-07-23 15:00

import django.core.files.storage
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('team', '0013_auto_20210723_1643'),
    ]

    operations = [
        migrations.AlterField(
            model_name='datafile',
            name='datafile',
            field=models.FileField(storage=django.core.files.storage.FileSystemStorage(location='/mnt/d/repositories/shinemas/'), upload_to='./documents/'),
        ),
    ]
