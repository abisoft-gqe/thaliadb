from team.models import User, ThreadReport

def notification_icon_renderer(request):
    """
      Context processor used to display an icon on the user 
      profile title if the user has a new message.
    """
    if request.user.is_authenticated:
        login = request.user.login
        user = User.objects.get(login=login)
        
        thread_reports = ThreadReport.objects.filter(user=user)
#         print(thread_reports)
        notification = False
        for report in thread_reports:
            if report.new:
                notification = True
        
#         print(notification)
        
    else:
        notification = False
        
    return {'notification': notification,}
            