#-*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.urls import path
from team.views import teamhome, person_management, institution_management, project_management, user_management, doc_management, file_manager,\
                       link_data, user_failed, contact, help, editfile

urls_as_dict = {
    'institution_management': {
        'path':'institution/',
        'view':institution_management,
        'docpath':'admin/team.html#institutions-management'
        },
    
    'project_management': {
        'path':'project/',
        'view':project_management,
        'docpath':'admin/project.html#create-update-projects'
        },
    
    'person_management': {
        'path':'person/',
        'view':person_management,
        'docpath': 'admin/team.html#people-management'
        },
    
    'teamhome': {
        'path':'home/',
        'view':teamhome,
        },
    
    'user_management': {
        'path':'user/',
        'view':user_management,
        'docpath': 'admin/team.html#users-management'
        },
    
    'doc_management': {
        'path':'docs/',
        'view':doc_management,
        'docpath': 'admin/project.html#manage-documents-in-projects'
        },
    
    'file_manager': {
        'path':'doc_files/<int:pk>/',
        'view':file_manager,
        'docpath': 'admin/project.html#manage-documents-in-projects'
        },
    
    'editfile': {
        'path':'editfile/<int:pk>/',
        'view':editfile,
        'docpath': 'admin/project.html#manage-documents-in-projects'
        },
    
    'link_data': {
        'path':'link_data/',
        'view':link_data,
        'docpath': 'admin/project.html#share-data-in-projects'
        },
    
    'user_failed': {
        'path':'',
        'view':user_failed,
        },
    
    'contact': {
        'path':'contact/',
        'view':contact,
        },
    
    'help': {
        'path':'help/',
        'view':help,
        },
    
    }

urlpatterns = [path(url['path'], url['view'], name=namespace) for namespace, url in urls_as_dict.items()]


# urlpatterns.extend([
#     #path('home/', teamhome, name='teamhome'),
#     #path('person/', person_management, name='person_management'),
#     #path('institution/', institution_management, name='institution_management'),
#     #path('project/', project_management, name='project_management'),
#     path('user/', user_management, name='user_management'),
#     path('docs/', doc_management, name='doc_management'),
#     path('doc_files/<int:pk>/', file_manager, name='file_manager'),
#     path('editfile/<int:pk>/', editfile, name='editfile'),
#     path('link_data/', link_data, name='link_data'),
#     path('', user_failed, name="user_failed"),
#     path('contact/', contact, name="contact"),
#     path('help/', help, name="help")
# ])
