# -*- coding: utf-8 -*-
""" LICENCE
    ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Elise Peluso

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""
import os

from django.db import models
from django_countries.fields import CountryField
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser
from django.dispatch import receiver
from django.utils.encoding import force_bytes

from commonfct.constants import METHOD_TYPE, DOCUMENT_TYPE, THREAD_ORIGIN
from team.managers import UserManager, MethodManager


class Institution(models.Model):
    """
        The Institution model gives information about the different attributes of an institution.
        
        
        :var CharField name: the name of the institution
        :var CharField acronym: the short name of the institution
        :var TextField address: the location of the institution
        :var CharField postal_code: the postal code of the institution
        :var CharField country: the country from where is the institution
        :var URLField web_site: the address where can be found the web site of the institution
        :var EmailField email_address: the email address of the institution
    
    """
    name = models.CharField(max_length=200, unique=True)
    acronym = models.CharField(max_length=50, blank=True, null=True)
    address = models.TextField(max_length=500, blank=True, null=True)
    postal_code = models.CharField(max_length=20, blank=True, null=True)
    city = models.CharField(max_length=200, blank=True, null=True)
    country = CountryField(blank=True, null=True)
    web_site = models.URLField(max_length=200, blank=True, null=True)
    email_address = models.EmailField(max_length=200, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name','acronym']

class Person(models.Model):
    """
        The Person model gives information about the person. Be careful, it is different from User.
        
        
        :var CharField first_name: the first name of the person
        :var CharField last_name: the last name of the person
        :var TextField initial: the initial (first letters of the first name and of the last name) of the person
        :var CharField title_of_person: the title of the person
        :var CharField other_language: the other language which might be spoken by the person
        :var CharField work_phone: the phone number from work where the person can be found
        :var CharField work_extension: the work extension of the person (useless field)
        :var CharField fax_number: the fax number of the person
        :var EmailField email_address: the email address of the person
        :var TextField note_on_person: description about the person (useless field)
        :var ManyToManyField institutions: institution where the person is from
    
    """
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    initial = models.CharField(max_length=25,blank=True, null=True)
    title_of_person = models.CharField(max_length=25, blank=True, null=True)
    other_language = models.CharField(max_length=25, blank=True, null=True)
    work_phone = models.CharField(max_length=25, blank=True, null=True)
    work_extension = models.CharField(max_length=25, blank=True, null=True)
    fax_number = models.CharField(max_length=25, blank=True, null=True)
    email_address = models.EmailField(max_length=200, blank=True, null=True)
    note_on_person = models.TextField(max_length=200, blank=True, null=True)
    institutions = models.ManyToManyField('Institution')
    
    def __str__(self):
        return '%s %s' %(self.first_name, self.last_name)
    
    class Meta:
        unique_together=("first_name","last_name")
        ordering = ["first_name","last_name"]
        
    def get_full_name(self):
        return "{0} {1}".format(self.first_name,self.last_name)
    
    
    #for BrAPI 
    def getBlankValue(self):
        return ''
    
    def getBlankList(self):
        return []
    
    def getMailingAddress(self):
        if not self.institutions.all() :
            return ''
        else :
            institution = self.institutions.all()[0]
            return '{0} {1} {2} {3}'.format(institution.address, institution.postal_code, institution.city, institution.country)  
        
    def getPeopleAdditionalInfo(self):
        peopleAdditionalInfo = {}
        if self.initial :
            peopleAdditionalInfo['initial'] = self.initial
        if self.title_of_person :
            peopleAdditionalInfo['title_of_person'] = self.title_of_person    
        if self.other_language :
            peopleAdditionalInfo['other_language'] = self.other_language 
        if self.work_extension :
            peopleAdditionalInfo['work_extension'] = self.work_extension      
        return peopleAdditionalInfo
      


class User(AbstractBaseUser):
    """
        The User model gives information about the user. A user is a person with access to the database. This model also manages the rights granted to a user.
        
        
        :var CharField person: the real name of the user
        :var CharField login: the login of the user
        :var DateField start_date: the date when the account of the user is activated
        :var DateField end_date: the date when the account of the user is closed
        :var BooleanField is_team_admin: check if the user is admin for the team app
        :var BooleanField is_accession_admin: check if the user is admin for the accession app 
        :var BooleanField is_seedlot_admin: check if the user is admin for the seedlot app 
        :var BooleanField is_genotyping_admin: check if the user is admin for the genotyping app 
        :var BooleanField is_phenotyping_admin: check if the user is admin for the phenotyping app
        :var BooleanField is_classification_admin: check if the user is admin for the classification app
    
    """
    person = models.OneToOneField('Person', on_delete=models.CASCADE)
    login = models.CharField(max_length=50, unique=True)
    start_date = models.DateField(auto_now_add=True)
    end_date = models.DateField(blank=True, null=True)
    is_team_admin = models.BooleanField(default=False)
    is_accession_admin = models.BooleanField(default=False)
    is_seedlot_admin = models.BooleanField(default=False)
    is_genotyping_admin = models.BooleanField(default=False)
    is_phenotyping_admin = models.BooleanField(default=False)
    is_classification_admin = models.BooleanField(default=False)
    
    objects = UserManager()
    
    USERNAME_FIELD = 'login'

    def get_full_name(self):
        return "{0} {1}".format(self.person.first_name,self.person.last_name)
    
    def get_short_name(self):
        return self.person.initialOrOtherName

    def is_superuser(self):
        return any([self.is_team_admin,self.is_accession_admin,self.is_seedlot_admin,self.is_genotyping_admin,self.is_phenotyping_admin,self.is_classification_admin])
    
    def is_team_admin_user(self):
        return self.is_team_admin
    
    def is_accession_admin_user(self):
        return self.is_accession_admin
    
    def is_seedlot_admin_user(self):
        return self.is_seedlot_admin
    
    def is_genotyping_admin_user(self):
        return self.is_genotyping_admin
    
    def is_phenotyping_admin_user(self):
        return self.is_phenotyping_admin
    
    def is_classification_admin_user(self):
        return self.is_classification_admin

    class Meta:
        ordering = ['login']
        
fs = FileSystemStorage(location=settings.MEDIA_ROOT)
class Documents(models.Model):
    """
        The Documents model gives information about the documents.
        
        
        :var CharField name: the name of the document
        :var IntegerField type: the type of the document  
    """
    name = models.CharField(max_length=200, unique=True)
    type = models.IntegerField(choices=DOCUMENT_TYPE)
    def __str__(self):
        return self.name

class DataFile(models.Model):
    """
        The DataFile model gives information about the datafile which is linked to a document previously created.
        
        
        :var FileField datafile: the datafile to load
        :var ForeignKey document: the document to link to the datafile
        :var ForeignKey source: the source of the datafile
        :var DateField date: the date of submission of the datafile
        :var TextField comments: the description of the datafile
        :var CharField version: the version of the datafile  
    """
    datafile = models.FileField(upload_to='./documents/', storage=fs)
    document = models.ForeignKey('Documents', blank=True, null=True, on_delete=models.CASCADE)
    source = models.ForeignKey('Person', on_delete=models.CASCADE,)
    date = models.DateField(auto_now_add=True)
    comments = models.TextField(max_length=500, blank=True, null=True)
#     version = models.CharField(max_length=5)
#     class Meta:
#         unique_together = ("version","document")
    def filename(self):
        return os.path.basename(self.datafile.name)

class Project(models.Model):
    """
        The Project model gives information about the project. A project is linked to some other models such as institutions, users and documents. Users linked to a project 
        have access to any data belonging to this project.
        
        
        :var CharField name: the name of the project
        :var CharField authors: the authors who made the project
        :var DateField start_date: the beginning date of the project
        :var DateField end_date: the ending date of the project
        :var TextField description: the description of the project
        :var ManyToManyField users: the users who have rights on the project 
        :var ManyToManyField institutions: the institution where the project has been created
        :var ManyToManyField linked_files: the files linked to the project
    """
    name = models.CharField(max_length=200, unique=True)
    authors = models.CharField(max_length=200)
    start_date = models.DateField()
    end_date = models.DateField(blank=True, null=True)
    description = models.TextField(max_length=500, blank=True, null=True)
    public = models.BooleanField(default=False)
    
    users = models.ManyToManyField(settings.AUTH_USER_MODEL) ##, through="FavoriteProject"
    institutions = models.ManyToManyField('Institution')
    linked_files = models.ManyToManyField('Documents',blank=True, null=True)

    def __str__(self):
        return self.name
    
    class Meta:
        ordering = ["name",]
        
    
    #for BrAPI    
    def getBlankList(self):
        return []
    def getBlankValue(self):
        return ''
    
    def getCommonCropName(self):
        return settings.COMMONCROPNAME
    
    def getDocumentationURL(self):
        return 'https://brapicore21.docs.apiary.io/#/reference/programs'
    
    def getProgramAdditionalInfo(self):
        programsAdditionalInfo = {}
        if self.start_date :
            programsAdditionalInfo['startDate'] = self.start_date
        if self.end_date :
            programsAdditionalInfo['endDate'] = self.end_date    
        if self.description :
            programsAdditionalInfo['description'] = self.description  
        return programsAdditionalInfo
        
    
    def getProgramTypeValue(self):
        return "PROJECT"
    
    
# class FavoriteProject(models.Model):
#     project = models.ForeignKey('Project')
#     user = models.ForeignKey('User')
#     favorite = models.BooleanField(default=True)

fs = FileSystemStorage(location=settings.MEDIA_ROOT)

class Method(models.Model):
    """
        The Method model gives information about the method.
        
        :var CharField name: the name of the method
        :var IntegerField type: the type of the method
        :var TextField description: the description of the method
    """
    name = models.CharField(max_length=200, unique=True)
    type = models.IntegerField(choices=METHOD_TYPE, null=True)
    description = models.TextField(max_length=500, blank=True, null=True)
    method_file = models.FileField(storage=fs, upload_to='methods/', blank=True)
    
    objects = MethodManager()
    
    def __str__(self):
        return self.name
        
    class Meta:
        ordering = ["name"]
     
    #for BrAPI 
    """
      Some functions for BrAPI serializers in team.serializers.py
    """     
    def getBlankValue(self):
        return ''
    
    def getExternalReferences(self):
        return [{'referenceId' : '', 'referenceSource' : ''}]
    
    def getMethodAdditionalInfo(self):
        methodAdditionalInfo = {}
        if self.type :
            methodAdditionalInfo['type'] = str(self.type)
        if self.method_file :
            methodAdditionalInfo['method_file'] = self.method_file
        return methodAdditionalInfo
    
    def getOntologyReference(self):
        return {'ontologyDbId' : '', 'ontologyName' : '', 'documentationLinks' : [], 'URL' : '', 'type' : '', 'version' : ''}
        

@receiver(models.signals.post_delete, sender=Method)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
      Deletes files of filesystem
      when corresponding Method object is deleted.
    """
    if instance.method_file and os.path.isfile(instance.method_file.path):
        os.remove(instance.method_file.path)


class ThreadReport(models.Model):
    """
      The ThreadReport models contains informations on the submissions made by users.
      
      :var ForeignKey user: The user concerned by the submission
      :var CharField thread: name of the thread
      :var TextField message: success or error message of the submission
      :var DateTimeField date: date and time of the notification
      :var IntegerField origin: view that launched the thread
      :var BooleanField success: if it is an error or success report
      :var BooleanField new: if the message has been read or not
      
    """
    user = models.ForeignKey('team.User', on_delete=models.CASCADE)
    thread = models.CharField(max_length=200)
    message = models.TextField(blank=True, null=True)
    date = models.DateTimeField(blank=True, null=True)
    subject = models.IntegerField(choices=THREAD_ORIGIN)
    success = models.BooleanField(null=True)
    new = models.BooleanField(default=True)
    
    class Meta:
        unique_together=("user", "thread")
    
    def __str__(self):
        return self.message if self.message else ""
