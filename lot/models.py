# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Elise Peluso

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import datetime
import ast

from django.apps import apps
from django.db import models
from django.dispatch import receiver
from django.core.exceptions import ValidationError

from commonfct.constants import ATTRIBUTE_TYPES
from django.utils.encoding import force_bytes
from lot.managers import SeedlotManager, SeedlotAttributePostionManager

# Create your models here.

class SeedlotType(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(max_length=500, blank=True, null=True)
    
    def attribute_set(self):
        return self.seedlottypeattribute_set
    
    def remove_attribute(self, attribute_id):
        #remove an attribute from list and update positions
        attribute = SeedlotTypeAttribute.objects.get(id=attribute_id)
        self.seedlottypeattribute_set.remove(attribute)
        self.update_attributes_positions()
    
    def update_attributes_positions(self):
        i = 1
        for position in SeedlotAttributePosition.objects.filter(type=self) :
            position.position = i
            position.save()
            i += 1
    
    def recompute_positions(self, new_positions):
        for attribute in self.seedlottypeattribute_set.all() :
            self.seedlottypeattribute_set.remove(attribute)
        
        for stringpos in new_positions:
            pos = ast.literal_eval(stringpos)
            attribute = SeedlotTypeAttribute.objects.get(id=pos[0])
            SeedlotAttributePosition.objects.create(attribute=attribute,
                                                      position=pos[1],
                                                      type=self)
    
    def __str__(self):
        return self.name
    
class Seedlot(models.Model):
    name = models.CharField(max_length=100, unique=True, db_index=True)
    accession = models.ForeignKey('accession.Accession', db_index=True, on_delete=models.CASCADE)
    type = models.ForeignKey('lot.SeedlotType', db_index=True, on_delete=models.CASCADE)
    is_obsolete = models.BooleanField(default= False)
    description = models.TextField(max_length=500, blank=True, null=True)
    attributes_values = models.JSONField(blank=True, null=True)
    objects = SeedlotManager() 
    
    #projects = models.ManyToManyField(Project)#blank=True, null=True
    projects = models.ManyToManyField('team.Project', blank=True)

    #images = models.ManyToManyField(Documents)

    def __str__(self):
        return self.name
    
    def relation_set(self):
        return self.seedlotrelation_set   
    
    class Meta:
        ordering = ['name',]
   
    
    #for BrAPI  
    def get0Value(self):
        return 0
    
    def getBlankDateValue(self):
        return '1900-01-01T12:00:00-0600'
    
    def getBlankList(self):
        return []
    
    def getBlankValue(self):
        return ''
    
    def getSeedlotAdditionalInfo(self):
        seedlotAdditionalInfo = {}
        if self.accession :
            seedlotAdditionalInfo['germplasm'] = str(self.accession)
        if self.type :
            seedlotAdditionalInfo['seedLotType'] = str(self.type)    
        if self.is_obsolete :
            seedlotAdditionalInfo['seedLotIsObsolete'] = str(self.is_obsolete)
        if self.attributes_values :
            seedlotAdditionalInfo['seedLotAttributesValues'] = str(self.attributes_values)  
        return seedlotAdditionalInfo
    
            
@receiver(models.signals.m2m_changed, sender=Seedlot.projects.through)
def report_projects_seedlot(sender, instance, action, pk_set, **kwargs):
    """
      Actions to be performed according to the link or unlink of projects.
      If projects are added to the seed lot they are added to the accession.
      If projects are removed, they are also removed for the sample.
    """
    Project = apps.get_model('team','project')
    
    if action == 'post_add':
        projects = Project.objects.filter(id__in=pk_set)
        accession = instance.accession
        for project in projects:
            if project not in accession.projects.all():
                accession.projects.add(project)
        
    elif action == 'post_remove':
        projects = Project.objects.filter(id__in=pk_set)
        for project in projects:
            samples = instance.sample_set.all()
            if samples:
                for sample in samples:
                    if project in sample.projects.all():
                        sample.projects.remove(project)
                        
    

class SeedlotTypeAttribute(models.Model):
    class AttributeType(models.IntegerChoices):
        SHORT_TEXT = 1,"Short Text"
        LONG_TEXT = 2,"Long Text"
        NUMBER = 3,"Number"
        URL = 4,"URL"
    
    name = models.CharField(max_length=100, blank=True)
    type = models.IntegerField(choices=AttributeType.choices)
    seedlot_type = models.ManyToManyField('lot.SeedlotType', through="SeedlotAttributePosition")
    
    class Meta:
        unique_together = ("name","type")

    def natural_key(self):
        return (self.name, self.type)

    def __str__(self):
        return self.name
    
    def clean(self):
        sl_fields = Seedlot._meta.get_fields()
        sl_fields_names = [f.name for f in sl_fields]
        
        if self.name.lower().replace(' ','_') in sl_fields_names:
            raise ValidationError('A field with this name already exists for seedlots.')


class SeedlotAttributePosition(models.Model):
    attribute = models.ForeignKey('lot.SeedlotTypeAttribute', on_delete=models.CASCADE)
    type = models.ForeignKey('lot.SeedlotType', on_delete=models.CASCADE)
    position = models.IntegerField()
    
    objects = SeedlotAttributePostionManager()
    
    class Meta:
        unique_together = (("type",'attribute'),('type',"position"))
        ordering = ('type','position')  
    
    
#TODO : Reste à implémenter les genealogies