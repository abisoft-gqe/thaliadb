#-*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.urls import path
from lot.views import SeedlotAttributesAutocomplete, lothome, lottype, seedlotdata, \
seedlotattributes, seedlotattribute_edit, seedlottype_edit

urls_as_dict = {
    'lothome': {'path':'home/', 'view':lothome, 'docpath':'admin/lot.html'},
    'lottype': {'path':'types/', 'view':lottype, 'docpath':'admin/lot.html#defining-seed-lot-types-and-attributes'},
    'edit_seedlottype': {'path':'types/<int:pk>/', 'view':seedlottype_edit, 'docpath':'admin/lot.html#defining-seed-lot-types-and-attributes'},
    'seedlotattributes': {'path':'attributes/', 'view':seedlotattributes, 'docpath':'admin/lot.html#defining-seed-lot-types-and-attributes'},
    'edit_seedlotattribute': {'path':'attributes/<int:pk>/', 'view':seedlotattribute_edit, 'docpath':'admin/lot.html#defining-seed-lot-types-and-attributes'},
    'seedlotdata': {'path':'seedlotdata/<int:type_id>/', 'view':seedlotdata, 'docpath':'admin/lot.html#seed-lot-management'},
    'seedlot-attributes-autocomplete': {'path':'attributes-autocomplete/', 'view':SeedlotAttributesAutocomplete.as_view(), },
    }

urlpatterns = [path(url['path'], url['view'], name=namespace) for namespace, url in urls_as_dict.items()]

# urlpatterns = [
#     path('home/', lothome, name='lothome'),
#     path('types/', lottype, name='lottype'),
#     path('types/<int:pk>/', seedlottype_edit, name='edit_seedlottype'),
#     path('attributes/', seedlotattributes, name='seedlotattributes'),
#     path('attributes/<int:pk>/', seedlotattribute_edit, name='edit_seedlotattribute'),
#     # Ajout 09/01/2015
#     path('seedlotdata/<int:type_id>/', seedlotdata, name='seedlotdata'),
#     path('attributes-autocomplete/', SeedlotAttributesAutocomplete.as_view(), name='seedlot-attributes-autocomplete'),
#
# ]