# Generated by Django 3.2 on 2023-07-11 14:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lot', '0008_auto_20230310_1002'),
    ]

    operations = [
        migrations.AlterField(
            model_name='seedlottypeattribute',
            name='type',
            field=models.IntegerField(choices=[(1, 'Short Text'), (2, 'Long Text'), (3, 'Number'), (4, 'URL')]),
        ),
    ]
