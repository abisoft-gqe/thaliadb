from rest_framework import serializers

from lot.models import Seedlot

class SeedlotBrAPISerializer(serializers.ModelSerializer):
    """
        CrossesBrAPISerializer gives the informations of cross entities
    """
    additionalInfo = serializers.JSONField(source = 'getSeedlotAdditionalInfo')
    amount = serializers.IntegerField(source = 'get0Value') 
    contentMixture = serializers.ListField(source = 'getBlankValue') #a revoir plus tard
    createdDate = serializers.CharField(source = 'getBlankDateValue') #a corriger
    externalReferences = serializers.ListField(source = 'getBlankList') 
    lastUpdated = serializers.CharField(source = 'getBlankDateValue') #a corriger
    locationDbId = serializers.CharField(source = 'getBlankValue') 
    locationName = serializers.CharField(source = 'getBlankValue') 
    programDbId = serializers.CharField(source = 'projects') 
    programName = serializers.CharField(source = 'projects') 
    seedLotDbId = serializers.CharField(source = 'id') 
    seedLotDescription = serializers.CharField(source = 'description') 
    seedLotName = serializers.CharField(source = 'name') 
    sourceCollection = serializers.CharField(source = 'getBlankValue') 
    storageLocation = serializers.CharField(source = 'getBlankValue') 
    units = serializers.CharField(source = 'getBlankValue') 
    
    class Meta:
        model = Seedlot
        fields = ['additionalInfo',
                  'amount',
                  'contentMixture',
                  'createdDate',
                  'externalReferences',
                  'lastUpdated',
                  'locationDbId',
                  'locationName',
                  'programDbId',
                  'programName',
                  'seedLotDbId',
                  'seedLotDescription',
                  'seedLotName',
                  'sourceCollection',
                  'storageLocation',
                  'units',
            ]   
        depth = 1