# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django import forms

from lot.models import SeedlotType, SeedlotTypeAttribute, Seedlot
from accession.models import Accession
from django.utils.safestring import mark_safe
from commonfct.forms_utils import HorizontalRadioSelect
from django.contrib.admin.widgets import FilteredSelectMultiple
from dal import autocomplete

class SeedlotTypeForm(forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = SeedlotType

class SeedlotTypeAttForm(forms.ModelForm):
    class Meta:
        model = SeedlotTypeAttribute
        exclude = ['seedlot_type']
        widgets = {'name': forms.TextInput(attrs = {'id':'attributes_autocomplete', 'required':False}),}

class SeedlotTypeAttributeSearch(forms.Form):
    attributes = forms.ModelChoiceField(label="Attribute",
                                       queryset=SeedlotTypeAttribute.objects.all(),
                                       required=False,
                                       widget=autocomplete.ModelSelect2(url='seedlot-attributes-autocomplete', attrs={'style': 'width:300px'}))
    

class SeedlotDataForm(forms.ModelForm):
    class Meta:
        model = Seedlot
        exclude = ['attributes_values', 'type']
#         widgets = {'name': forms.TextInput(attrs = {'id':'attributes_autocomplete'}),}
    def __init__(self, *args, **kwargs):
        if 'formstype' in kwargs.keys():
            attrs = kwargs['formstype']
            if 'data' in kwargs.keys():
                if 'instance' in kwargs.keys():
                    super(SeedlotDataForm, self).__init__(data=kwargs['data'], instance=kwargs['instance'])
                else :
                    super(SeedlotDataForm, self).__init__(data=kwargs['data'])
            else :
                super(SeedlotDataForm, self).__init__()
            for at in attrs:
                if at.type == 2:
                    self.fields[at.name.lower().replace(' ','_')] = forms.CharField(widget=forms.Textarea(attrs={'rows':'1'}),
                                                                                              required = False)
                else:
                    self.fields[at.name.lower().replace(' ','_')] = forms.CharField(required = False)
        
        else :
            if 'data' in kwargs.keys():
                super(SeedlotDataForm, self).__init__(data=kwargs['data'])
            else :
                super(SeedlotDataForm, self).__init__()

    
SepChoices=[(',',' , '),
            (';',' ; ')]          
                
class UploadFileForm(forms.Form):
    """
      Form allowing the uploading of a file, querying a delimiter to be readable
      
      :var FileField file: required field, file to be uploaded
      :var ChoiceField delimiter: delimiter of the file, required so that the developer knows how to read the file
      :var list SepChoices: list of tuples that defines the delimiters that can be used by the user
    """
    file = forms.FileField()
    delimiter = forms.ChoiceField(widget=HorizontalRadioSelect(), choices=SepChoices, required=True, initial=",")
    
class UploadFileFormWithoutDelimiter(forms.Form):
    """
      Form allowing the uploading of a file, querying a delimiter to be readable
      
      :var FileField file: required field, file to be uploaded
      :var ChoiceField delimiter: delimiter of the file, required so that the developer knows how to read the file
      :var list SepChoices: list of tuples that defines the delimiters that can be used by the user
    """
    file = forms.FileField(required=True)

