# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import json
import ast
import io
import operator
import csv

from _csv import Dialect
from unidecode import unidecode
from dal import autocomplete

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, user_passes_test
from django.db.models import Q
from django.conf import settings
from django.forms.models import model_to_dict
from django.db.models.query import QuerySet, Prefetch
from django.http import HttpResponse,HttpResponseServerError, Http404
from django.db import IntegrityError, transaction
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages

from accession.models import Accession
from team.models import Project 
from lot.models import SeedlotType, SeedlotTypeAttribute, SeedlotAttributePosition, Seedlot
from lot.forms import SeedlotTypeAttForm, SeedlotTypeForm, SeedlotDataForm,\
                      UploadFileFormWithoutDelimiter, SeedlotTypeAttributeSearch
from accession.views import get_accession_or_seedlot_relations
from commonfct.genericviews import generic_management
from commonfct.utils import get_fields, recode
from commonfct.constants import ACCEPTED_TRUE_VALUES, filerenderer_headers
from genealogy_seedlot.models import SeedlotRelation
from csv_io.views import CSVFactoryForTempFiles


class SeedlotAttributesAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return SeedlotTypeAttribute.objects.none()
        qs = SeedlotTypeAttribute.objects.all()
        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs
    
    def get_result_label(self, result):
        attribute_type = dict(SeedlotTypeAttribute.AttributeType.choices)[result.type]
        label = "{0} ({1})".format(result.name, attribute_type)
        return label


@login_required
@user_passes_test(lambda u: u.is_seedlot_admin_user(), login_url='/login/')
def lothome(request):
    return render(request,'lot/seedlot_base.html',{"admin":True})


@login_required
@user_passes_test(lambda u: u.is_seedlot_admin_user(), login_url='/team/')
def seedlotattribute_edit(request, pk=None):
    attribute = SeedlotTypeAttribute.objects.get(id=pk)
    form = SeedlotTypeAttForm(instance=attribute)
    template = 'lot/seedlot_attributes.html'
    fields_name, fields_label = get_fields(form)
    template_data = {'fields_label':fields_label,
                    'fields_name':fields_name,
                    'all': SeedlotTypeAttribute.objects.all(),
                    'form' : form,
                    'urlname':'edit_seedlotattribute',
                    'creation_mode':False}
    if request.method == "POST" :
        form = SeedlotTypeAttForm(request.POST, instance=attribute)
        if form.is_valid() :
            form.save()
            template_data.update({'form':SeedlotTypeAttForm(),'creation_mode':True })
            return render(request, template, template_data)
        else :
            template_data.update({'form':form})
            return render(request, template, template_data)
    return render(request, template, template_data)


@login_required
@user_passes_test(lambda u: u.is_seedlot_admin_user(), login_url='/team/')
def seedlotattributes(request):
    form = SeedlotTypeAttForm()
    template = 'lot/seedlot_attributes.html'
    fields_name, fields_label = get_fields(form)
    template_data = {'fields_label':fields_label,
                    'fields_name':fields_name,
                    'all': SeedlotTypeAttribute.objects.all().order_by('name'),
                    'form' : form,
                    'urlname':'edit_seedlotattribute',
                    'creation_mode':True}
    
    if request.method == "POST" :
        form = SeedlotTypeAttForm(request.POST)
        if form.is_valid() :
            form.save()
            template_data.update({'form':SeedlotTypeAttForm()})
            return render(request, template, template_data)
        else :
            template_data.update({'form':form})
            return render(request, template, template_data)
    return render(request, template, template_data)


@login_required
@user_passes_test(lambda u: u.is_seedlot_admin_user(), login_url='/team/')
def lottype(request):
    form = SeedlotTypeForm()
    template = 'lot/seedlot_type.html'
    fields_name, fields_label = get_fields(form)
    template_data = {'fields_label':fields_label,
                    'fields_name':fields_name,
                    'all': SeedlotType.objects.all(),
                    'form' : form,
                    'urlname':'edit_seedlottype',
                    'creation_mode':True}
    
    if request.method == "POST" :
        form = SeedlotTypeForm(request.POST)
        if form.is_valid() :
            form.save()
            template_data.update({'form':SeedlotTypeForm()})
            return render(request, template, template_data)
        else :
            template_data.update({'form':form})
            return render(request, template, template_data)
    return render(request, template, template_data)


@login_required
@user_passes_test(lambda u: u.is_seedlot_admin_user(), login_url='/team/')
def seedlottype_edit(request, pk=None):
    try :
        seedlot_type = SeedlotType.objects.get(id=pk)
        form = SeedlotTypeForm(instance=seedlot_type)
        formatt = SeedlotTypeAttributeSearch()
        template = 'lot/seedlot_type.html'
        fields_name, fields_label = get_fields(form)
        template_data = {'fields_label':fields_label,
                        'fields_name':fields_name,
                        'all': SeedlotType.objects.all(),
                        'form' : form,
                        'urlname':'edit_seedlottype',
                        'attributeform':formatt,
                        'creation_mode':False,
                        'attributes':seedlot_type.seedlotattributeposition_set.all().order_by('position')}
        if request.method == "POST" :
            if request.POST.get('submitedform') == "seedlottype" :
                form = SeedlotTypeForm(request.POST, instance=seedlot_type)
                if form.is_valid() :
                    form.save()
                    template_data.update({'form':SeedlotTypeForm(),'creation_mode':True })
                    return render(request, template, template_data)
                else :
                    template_data.update({'form':form})
                    return render(request, template, template_data)
            elif request.POST.get('submitedform') == "removingattribute" :
                attribute_id = request.POST.get('attributeid')
                template_data.update({'activetab':1})
                try :
                    seedlot_type.remove_attribute(attribute_id)
                    messages.add_message(request,messages.SUCCESS, "Attribute have been removed from the list")
                except SeedlotTypeAttribute.DoesNotExist:
                    messages.add_message(request,messages.ERROR, "Attribute doesn't exists")
                return render(request, template, template_data)
            elif request.POST.get('submitedform') == "addattribute" :
                attribute_id = request.POST.get('attributes')
                template_data.update({'activetab':1})
                try :
                    with transaction.atomic():
                        SeedlotAttributePosition.objects.create_last_element(attribute_id, seedlot_type)
                except SeedlotTypeAttribute.DoesNotExist:
                    messages.add_message(request,messages.ERROR, "Attribute doesn't exists")
                except IntegrityError as e :
                    messages.add_message(request, messages.ERROR, "Attribute can't be added")
                else :
                    messages.add_message(request,messages.SUCCESS, "Attribute have been added to the list")
                return render(request, template, template_data)
            elif request.POST.get('submitedform') == "reorderingattributes" :
                ordered_list = request.POST.getlist('attributes')
                print(ordered_list)
                seedlot_type.recompute_positions(ordered_list)
                template_data.update({'activetab':1})
                return render(request, template, template_data)
                    
        return render(request, template, template_data)
    except SeedlotType.DoesNotExist :
        messages.add_message(request,messages.ERROR, "The SeedlotType you are trying to update doesn't exist")
        return redirect('seedlottype')



def upload_file(file, form, attributess, seedtype, request):
    """
      Reading of the file in order to insert or update data.
    """
    myfactory = CSVFactoryForTempFiles(file)
    myfactory.find_dialect()
    myfactory.read_file()
    file_dict = myfactory.get_file()
    
    headers_list = filerenderer_headers['Seedlot']
    
    missing = myfactory.check_headers_conformity(headers_list)
    
    if missing != []:
        messages.add_message(request, messages.ERROR, "One or several mandatory headers are missing in your file : "+format(', '.join(missing))+\
                             ". The required headers for this file are: "+format(', '.join(headers_list))+".")
        return
    
    if request.POST.get("submit"): #Cas d'une insertion de nouvelles données
        submit_upload_file(request, file_dict, attributess, seedtype)
        
    elif request.POST.get("update"):
        update_upload_file(request, file_dict, attributess)
        
    else:
        messages.add_message(request, messages.ERROR,'ThaliaDB was unable to identify if you\'re trying to insert new data or to update former data.')


def submit_upload_file(request, file_dict, attributess, seedtype):
    """
      Insertion of seedlot data from the file.
    """
    with transaction.atomic():
        jsonvalues={}
        for line in file_dict:
            try:
                with transaction.atomic(): 
                    for att in attributess:
                        value = line[str(att.name).strip()]
                        if value != None or value !='':
                            jsonvalues[att.id] = value   
                        else:
                            jsonvalues[att.id] = ''
                    
                    accession = Accession.objects.get(name=str(line['Accession']))
                    
                    s = Seedlot.objects.create(name=str(line['Name']),
                                               accession=accession,
                                               type=seedtype,
                                               is_obsolete=(str(line['Is Obsolete']) in ACCEPTED_TRUE_VALUES ),
                                               description=str(line['Description']),
                                               attributes_values=json.dumps(jsonvalues))
                    s.save()
                    
                    project_list = [i.strip() for i in str(line['Projects']).split('|')]
                    projects = Project.objects.filter(name__in = project_list)
                    if projects.exists():
                        s.projects.add(*list(projects))
            
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+").")
                continue
        
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return
            
    messages.add_message(request, messages.SUCCESS, "The file has been successfully inserted.")


def update_upload_file(request, file_dict, attributess):
    """
      Update of seedlot data from the file.
    """
    with transaction.atomic():
        for line in file_dict:
            jsonvalues ={}
            try:
                with transaction.atomic():
                    seed_to_update = Seedlot.objects.get(name=str(line['Name']))
                    
                    accession = Accession.objects.get(name=str(line['Accession']))
                    
                    seed_to_update.accession = accession
                    seed_to_update.description = str(line['Description'])
                    
                    if line['Is Obsolete'] == 'False' or line['Is Obsolete'] == '':
                        seed_to_update.is_obsolete = False
                    else:
                        seed_to_update.is_obsolete = True
                        
                    if line['Projects']:
                        project_list = [p.strip() for p in str(line['Projects']).split('|')]
                        projects = Project.objects.filter(name__in = project_list)
                        if len(project_list) != len(projects):
                            messages.add_message(request, messages.ERROR, "There is an error with a project in this list: "+ ', '.join(project_list)+" (As a reminder, the separator must be | ).")
                            raise Exception
                        seed_to_update.projects.clear()
                        seed_to_update.projects.add(*list(projects))
                        
                    for att in attributess:
                        value = line[str(att.name).strip()]
                        if value != None or value !='':
                            jsonvalues[att.id] = value   
                        else:
                            jsonvalues[att.id] = ''
                    if type(seed_to_update.attributes_values) is dict:
                        attributes_in_database = seed_to_update.attributes_values
                    else:    
                        attributes_in_database = json.loads(seed_to_update.attributes_values)
                    shared_items = set(jsonvalues.items()) & set(attributes_in_database.items()) #Cette liste contient les valeurs en commun pour chaque clé
                    if len(shared_items) < len(attributes_in_database):
                        seed_to_update.attributes_values = jsonvalues
                    
                    seed_to_update.save()
                    
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+").")
                    continue
                
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully uploaded and the seedlots have been updated.")

 
def _get_seedlots(seedlottype, attributes, request):

    seedlots_init = Seedlot.objects.prefetch_related(
                    'projects',
                    Prefetch('relations_as_child', queryset=SeedlotRelation.objects.select_related('parent').all(), to_attr='rel_as_child'),
            ).select_related('accession').filter(type=seedlottype).order_by('name')


    seedlots = get_accession_or_seedlot_relations(seedlots_init, SeedlotRelation)

    # Ici, on ne peut pas utiliser "seedlots.iterator()
    # car sur notre ModelForm les attributs dynamiques ne font pas partie
    # de la liste d'attributs du Model
    for seed in seedlots: 
        for att in attributes:
            if seed[0].attributes_values :
                if type(seed[0].attributes_values) is dict:
                    attributes_values = seed[0].attributes_values
                else:
                    attributes_values= json.loads(seed[0].attributes_values)
                #si l'id de l'attribut "att" est présent dans les clés de attributes_values
                if str(att.id) in attributes_values.keys() :
                    setattr(seed[0],att.name.lower().replace(' ','_'),attributes_values[str(att.id)])
                else :
                    setattr(seed[0],att.name.lower().replace(' ','_'),None)
            else :
                setattr(seed[0],att.name.lower().replace(' ','_'),'')
    #Adding Paginator        
    try:
        nb_per_page=request.GET['nb_per_page']     
        if nb_per_page=="all":
            nb_per_page = len(seedlots)
    except:
        nb_per_page=50
    paginator = Paginator(seedlots, nb_per_page)
    page = request.GET.get('page')
    try:
        seedlots = paginator.page(page)
    except PageNotAnInteger:
        # Si la page n'est pas un entier, on affiche la page un
        seedlots = paginator.page(1)
    except EmptyPage:
        # Si la page déborder, on affiche la dernière page
        seedlots = paginator.page(paginator.num_pages)
    return seedlots, nb_per_page 


@login_required
@user_passes_test(lambda u: u.is_seedlot_admin_user(), login_url='/login/')
def seedlotdata(request, type_id):
    # On récupère l'accessionType correspondant à l'id = type_id 
    seedlottype = SeedlotType.objects.get(id=int(type_id))
    # On récupère tous les attributs du type
    attributes = seedlottype.seedlottypeattribute_set.all()
    # On récupère tous les attributs du seedlot du type dans "formattype"
    formattype = SeedlotDataForm(formstype=attributes)
    
    register_formFile = UploadFileFormWithoutDelimiter()
    
    names, labels = get_fields(formattype)
    names_genealogy = ['parent_male','parent_female','parent_x']
    labels_genealogy = ['Parent Male','Parent Female','Parent(s)']
    
    title = seedlottype.name+' Seedlot Management'
    tag_fields = {'title':title,
                  'fields_name':names,
                  'fields_label':labels,
                  "names_genealogy":names_genealogy,
                  "labels_genealogy":labels_genealogy,
                  "admin":True,
                  "refine_search":True,
                  }
        
    template = 'lot/seedlot_data.html'
    seedlots_all = Seedlot.objects.filter(type=seedlottype).distinct()
    number_all = len(seedlots_all)
    list_attributes=['name', 'accession', 'description', 'projects']
    list_attributes_id=[]
    for i in SeedlotTypeAttribute.objects.filter(seedlot_type=seedlottype):
        list_attributes_id.append(i.id)
    for i in list_attributes_id:
        if str(SeedlotTypeAttribute.objects.get(id=i)) not in list_attributes:
            list_attributes.append(str(SeedlotTypeAttribute.objects.get(id=i)))
    for i in ['parent male','parent female','parent(s)']:
        list_attributes.append(i)
            
    if request.method == "GET": 
        seedlots, nb_per_page = _get_seedlots(seedlottype, attributes, request)
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            return render(request, 'lot/seedlot_data.html', {"refine_search":True,"admin":True,"list_attributes":list_attributes,'formfile':register_formFile,'filetype':'seedlot','typeid': type_id,'form':formattype,'creation_mode':True,"number_all":number_all, 'all':seedlots, 'fields_label':labels,'fields_name':names})

        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
            length_dico_get = len(dico_get)
            nb_per_page = 10

            seedlots_init = Seedlot.objects.refine_search(or_and, seedlottype, dico_get)
            number_all = len(seedlots)

            dico = {}
            download_not_empty = False

            seedlots = get_accession_or_seedlot_relations(seedlots_init, SeedlotRelation)
            
            if seedlots:      
                list_col = [str(f.name).replace("_", " ") for f in seedlots[0][0]._meta.get_fields()]
                
                attr_names = seedlottype.seedlottypeattribute_set.all()
                for at in attr_names:
                    list_col.append(str(at.name))
                for j in ['parent male','parent female','parent(s)']:
                    list_col.append(j)
                for element_to_remove in ['id','relations as parent','relations as child','type',"phenotypicvalue","classevalue","sample","attributes values"]:
                    list_col.remove(element_to_remove)
                
                dialect = Dialect(delimiter = ';')
                with open(settings.TEMP_FOLDER+"/result_search.csv", 'w', encoding='utf-8') as csv_file:
                    writer = csv.writer(csv_file, dialect)
                    writer.writerow(list_col)
                    for i in seedlots_init:
                        row=[]
                        dico=model_to_dict(i)
                        try:
                            dico_attr_values = ast.literal_eval(getattr(i,"attributes_values"))
                        except:
                            dico_attr_values = getattr(i,"attributes_values")
                        for id_attr, val_attr in dico_attr_values.items():
                            dico[SeedlotTypeAttribute.objects.get(id=id_attr).name]=val_attr
                            
                        dico['parent male'] = [a.parent.name for a in SeedlotRelation.objects.filter(parent_gender = "M", child=i)]
                        dico['parent female'] = [a.parent.name for a in SeedlotRelation.objects.filter(parent_gender = "F", child=i)]
                        dico['parent(s)'] = [a.parent.name for a in SeedlotRelation.objects.filter(parent_gender = "X", child=i)]
                        
                        for key, value in dico.items():
                            if key == 'accession':
                                dico[key] = Accession.objects.get(id=dico[key]).name
                            if not dico[key]:
                                dico[key]=""
                            elif isinstance(dico[key], QuerySet):
                                list_qs = []
                                for queryset_i in dico[key]:
                                    list_qs.append(queryset_i.name)
                                dico[key]=', '.join(list_qs)
                            elif isinstance(dico[key], list):
                                dico[key]=', '.join(str(obj) for obj in dico[key])
                        for col in list_col:
                            if col not in dico:
                                dico[col]=""
                            row.append(dico[col])
                        writer.writerow(row)    
                        
                download_not_empty = True
            tag_fields.update({"download_not_empty":download_not_empty})

        tag_fields.update({'all':seedlots,
                           "search_result":dico_get,
                           "list_attributes":list_attributes,
                           'or_and':or_and,
                           'nb_per_page':nb_per_page,
                           "number_all":number_all,
                           'formfile':register_formFile,
                           'form':formattype,
                           'creation_mode':True,
                           'excel_empty_file':True,
                           'filetype':'seedlot',
                           'typeid': type_id})
        return render(request, template, tag_fields)#{'form':formattype,'creation_mode':True, 'all':seedlots, 'fields_label':labels,'fields_name':names})
    
    elif request.method == "POST":
        if "hiddenid" in request.POST.keys() :
            try :
                #on récupère l'objet déjà existant puis on utilise les fonctionnalités du formulaire pour l'updater
                instance = Seedlot.objects.get(id=request.POST['hiddenid'])
                form = SeedlotDataForm(instance=instance, data=request.POST, formstype=attributes)

                if form.is_valid():
                    instance = form.save()
                    jsonvalues = {}
                    for att in attributes:
                        jsonvalues[att.id] = request.POST[att.name.lower().replace(' ','_')]
                    instance.attributes_values = jsonvalues
                    instance.save()
                    seedlots,nb_per_page = _get_seedlots(seedlottype, attributes, request)                                 
                    tag_fields.update({'all':seedlots,
                                       "number_all":number_all,
                                       'nb_per_page':nb_per_page,"list_attributes":list_attributes,
                                       'form':form,
                                       'creation_mode':False,
                                       'object':instance,
                                       'hiddenid':instance.id})
                    return render(request,template,tag_fields)
                else :
                    #sinon on génère un message d'erreur qui sera affiché
                    errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form.errors.items() ])
                    tag_fields.update({'form':form,
                                       'creation_mode':False,
                                       'hiddenid':instance.id,"list_attributes":list_attributes,
                                       'errormsg':errormsg})
                    return render(request,template,tag_fields)
            except ObjectDoesNotExist :
                tag_fields.update({'form':form,
                                    'creation_mode':True,
                                    'errormsg':"The object you try to update doesn't exist in the database"})
                return render(request,template,tag_fields)
                        
        else:
            formattype = SeedlotDataForm(data=request.POST,formstype=attributes)
            formf = UploadFileFormWithoutDelimiter(request.POST, request.FILES)
            
            if formf.is_valid():
                upload_file(request.FILES['file'], formf, attributes, seedlottype, request) 
                seedlots, nb_per_page = _get_seedlots(seedlottype, attributes, request)  
                
                tag_fields = {'labels_genealogy':labels_genealogy,
                              'refine_search':True,
                              'list_attributes':list_attributes,
                              'admin':True,
                              'formfile':register_formFile,
                              'filetype':'seedlot',
                              'typeid': type_id,
                              'excel_empty_file':True,
                              'form':SeedlotDataForm(formstype=attributes),
                              'creation_mode':True,
                              'all':seedlots,
                              'number_all':number_all,
                              'nb_per_page':nb_per_page,
                              'fields_label':labels,
                              'fields_name':names}
                
                return render(request, template, tag_fields)                       

                    
            if formattype.is_valid():
                newseedlot = formattype.save(commit=False)
                newseedlot.type = seedlottype
                jsonvalues = {}
                for att in attributes:
                    jsonvalues[att.id] = request.POST[att.name.lower().replace(' ','_')]
                        
                newseedlot.attributes_values = jsonvalues
                newseedlot.save()
                request_dict = dict(request.POST)
                if 'projects' in request_dict:
                    projectss = request_dict['projects']
                    if len(projectss) == 1:
                        project_id = projectss[0]
                        project = Project.objects.get(id=project_id)
                        newseedlot.projects.add(project)
                    else:
                        for i in range(len(projectss)):
                            project_id = projectss[i]
                            project = Project.objects.get(id=project_id)
                            newseedlot.projects.add(project)

                seedlots, nb_per_page = _get_seedlots(seedlottype, attributes, request)
                return render(request, template, {"labels_genealogy":labels_genealogy,"refine_search":True,"object":newseedlot ,"list_attributes":list_attributes,"number_all":number_all, "admin":True,'filetype':'seedlot','typeid': type_id,'formfile':register_formFile,'excel_empty_file':True,'form':formattype,'creation_mode':True, 'all':seedlots, 'nb_per_page':nb_per_page, 'fields_label':labels,'fields_name':names})
            else :
                #Ajout 23/7/15
                errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in formattype.errors.items() ])
                seedlots, nb_per_page = _get_seedlots(seedlottype, attributes, request)
                #return generic_management(Seedlot, SeedlotDataForm, request, 'lot/lot_generic.html',{},"Seed lot Management")
                return render(request,template, {"labels_genealogy":labels_genealogy,"refine_search":True,"list_attributes":list_attributes,"number_all":number_all, "admin":True,'formfile':register_formFile, 'form':formattype,'creation_mode':True, 'all':seedlots, 'nb_per_page':nb_per_page, 'fields_label':labels,'fields_name':names,'errormsg':errormsg})        
    else :
        return render(request, template, {"labels_genealogy":labels_genealogy,"refine_search":True,"list_attributes":list_attributes,"number_all":number_all, "admin":True,'formfile':register_formFile, 'form':SeedlotDataForm,'creation_mode':True, 'all':seedlots, 'nb_per_page':nb_per_page, 'fields_label':labels,'fields_name':names},"Seedlot Management")
 