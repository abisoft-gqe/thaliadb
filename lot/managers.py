"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Laetitia Courgey

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import ast

from django.apps import apps
from django.db import models
from django.db.models import Q
from django.db.models.query import Prefetch

from team.models import User, Project
from operator import ior, iand

class SeedlotAttributePostionManager(models.Manager):
    
    def create_last_element(self, attribute_id, seedlot_type):
        SeedlotTypeAttribute = apps.get_model('lot','seedlottypeattribute')
        
        attribute = SeedlotTypeAttribute.objects.get(id=attribute_id)
        allattributes = seedlot_type.seedlotattributeposition_set.order_by('-position')
        if allattributes :
            last_position = allattributes[0].position+1
        else :
            last_position = 1
        return self.create(attribute=attribute,
                    type=seedlot_type,
                    position=last_position)

class SeedlotManager(models.Manager):
    """
    Manager for seedlot model.
    """
    def by_username(self,username):
        """
        Returns only the seedlots from projects linked to this user 

        :param string username : username

        :var int username_id : user's id
        :var list projects : projects linked to the user
        """
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
        return self.filter(projects__in=projects).distinct()
    
    def refine_search(self, or_and, seedlottype, dico_get):
        """
        Search in all accession according to some parameters

        :param string or_and : link between parameters (or, and)
        :param SeedlotType seedlottype : seedlot type to filter seedlot
        :param dict dico_get : data to search for
        """
        # Managers are called in accession.model
        # To avoid crossed imports we use apps to get objects from the model 
        Seedlot = apps.get_model('lot', 'seedlot')
        Accession = apps.get_model('accession', 'accession')
        SeedlotTypeAttribute = apps.get_model('lot','seedlottypeattribute')
        SeedlotRelation = apps.get_model('genealogy_seedlot','seedlotrelation')

        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
            
        proj=[]
        acc=[]
        query=Q()
        list_attr=[]
        for dat, dico_type_data in dico_get.items():
            if "projects" in dico_type_data:
                if dico_type_data['projects']=='':
#                     print('Projects empty')
                    query = myoperator(query, Q(projects__isnull=True))
                else:
                    proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                    query = myoperator(query, Q(projects__in=proj))
            elif 'accession' in dico_type_data:
                if dico_type_data['accession']=='':
                    query = myoperator(query, Q(accession__isnull=True))
                else:
                    acc =(Accession.objects.filter(name__icontains=dico_type_data['accession']))
                    query = myoperator(query, Q(accession__in=acc))
            elif 'parent male' in dico_type_data:
                parent_relation = []
                for sl in Seedlot.objects.filter(name__icontains=dico_type_data['parent male']):
                    sl_relation = sl.relations_as_parent.filter(parent_gender="M")
                    for i in sl_relation:
                        parent_relation.append(i.child)
                query = myoperator(query, Q(name__in=parent_relation))
            elif 'parent female' in dico_type_data:
                parent_relation = []
                for sl in Seedlot.objects.filter(name__icontains=dico_type_data['parent female']):
                    sl_relation = sl.relations_as_parent.filter(parent_gender="F")
                    for i in sl_relation:
                        parent_relation.append(i.child)
                query = myoperator(query, Q(name__in=parent_relation))
            elif 'parent(s)' in dico_type_data:
                parent_relation = []
                for sl in Seedlot.objects.filter(name__icontains=dico_type_data['parent(s)']):
                    sl_relation = sl.relations_as_parent.filter(parent_gender="X")
                    for i in sl_relation:
                        parent_relation.append(i.child)
                query = myoperator(query, Q(name__in=parent_relation))
            else:
                for data_type, value in dico_type_data.items():
                    list_name_in=[]
                    for i in SeedlotTypeAttribute.objects.filter(seedlot_type=seedlottype):
                        if str(data_type) == str(i):
#                                     print(str(i))
                            list_attr.append(str(i))
                    if data_type in list_attr:
                        id_attribute = str(SeedlotTypeAttribute.objects.get(name=data_type, seedlot_type=seedlottype).id)
                        for i in Seedlot.objects.filter(type=seedlottype):
                            try:
                                if id_attribute in i.attributes_values.keys():
                                    if value in ast.literal_eval(i.attributes_values)[id_attribute]:
#                                         print(str(i))
                                        list_name_in.append(str(i))
                            except:
                                try:
                                    if id_attribute in i.attributes_values.keys():
                                        if value in i.attributes_values[id_attribute]:
        #                                             print(str(i))
                                            list_name_in.append(str(i))
                                except:
                                    i.attributes_values = ast.literal_eval(i.attributes_values)
                                    if id_attribute in i.attributes_values.keys():
                                        if value in i.attributes_values[id_attribute]:
                                            list_name_in.append(str(i))

                        query = myoperator(query, Q(name__in = list_name_in))
                    else:
                        var_search = str( data_type + "__icontains" ) 
                        query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))

        query = Q(query, type=seedlottype)  
        return Seedlot.objects.prefetch_related(
                    'projects',
                    Prefetch('relations_as_child', queryset=SeedlotRelation.objects.select_related('parent').all(), to_attr='rel_as_child'),
            ).select_related('accession').filter(query).distinct()

