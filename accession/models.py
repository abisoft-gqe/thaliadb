# -*- coding: utf-8 -*-
"""LICENCE
    ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Elise Peluso

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

import json
import ast

from django.apps import apps
from django.db import models
from django.db.models import Q
from django.db.models.functions import Upper
from django.dispatch import receiver
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.conf import settings
from django_countries.fields import CountryField
from rest_framework.renderers import JSONRenderer

from genealogy_accession.serializers import AccessionRelationParentSerializer, AccessionRelationChildSerializer

from accession.managers import AccessionManager, AccessionAttributePostionManager
from commonfct.managers import EntityTypeManager


"""
    The MCPD_CODE details the different levels of biological status
"""
MCPD_CODE = ((100,"Wild"),
             (110,"Natural"),
             (120,"Semi-natural/Wild"),
             (130,"Semi-natural/Sown"),
             (200,"Weedy"),
             (300,"Traditional cultivar/landrace"),
             (400,"Breeding/research material"),
             (410,"Breeders line"),
             (411,"Synthetic population"),
             (412,"Hybrid"),
             (413,"Founder stock/base population"),
             (414,"Inbred line(parent of hybrid cultivar"),
             (415,"Segregating population"),
             (416,"Clonal selection"),
             (420,"Genetic stock"),
             (421,"Mutant(e.g. induced/insertion mutants, tilling populations)"),
             (422,"Cytogenetic stocks (e.g. chromosome addition/substitution, aneuploids, amphiploids)"),
             (423,"Other genetic stocks (e.g. mapping populations)"),
             (500,"Advanced or improved cultivar (conventional breeding methods)"),
             (600,"GMO (by genetic engineering)"),
             (999,"Other (Elaborate in REMARKS field)")
            )

# Create your models here.
class AccessionType(models.Model):
    """
      The AccessionType model gives information about the type of accessions.
      
      :var CharField name: name of the accession type
      :var TextField description: description of the accession type
    """
    
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(max_length=500, blank=True, null=True)
    code_mcpd = models.IntegerField(choices=MCPD_CODE, blank=True, null=True)

    def __str__(self):
        return self.name
    
    #shortcut that works with LotType and LocusType
    def attribute_set(self):
        return self.accessiontypeattribute_set
    
    def remove_attribute(self, attribute_id):
        #remove an attribute from list and update positions
        attribute = AccessionTypeAttribute.objects.get(id=attribute_id)
        self.accessiontypeattribute_set.remove(attribute)
        self.update_attributes_positions()
    
    def update_attributes_positions(self):
        i = 1
        for position in AccessionAttributePosition.objects.filter(type=self) :
            position.position = i
            position.save()
            i += 1
    
    def recompute_positions(self, new_positions):
        for attribute in self.accessiontypeattribute_set.all() :
            self.accessiontypeattribute_set.remove(attribute)
        
        for stringpos in new_positions:
            pos = ast.literal_eval(stringpos)
            attribute = AccessionTypeAttribute.objects.get(id=pos[0])
            AccessionAttributePosition.objects.create(attribute=attribute,
                                                      position=pos[1],
                                                      type=self)
            
    
class Accession(models.Model):
    """
      The Accession model gives information about the accessions.
      
      :var ForeignKey type: type of the accession, defined in the AccessionType model
      :var CharField name: name of the accession
      :var CharField doi: doi of the accession
      :var BooleanField private_doi: check if the doi of the accession is a private doi or not
      :var TextField pedigree: pedigree of the accession
      :var BooleanField is_obsolete: check if the accession is obsolete or not
      :var TextField description: description of the accession
      :var CharField variety: variety of the accession
      :var JSONField attributes_values: dictionary containing the attributes values of the accession
      :var FloatField latitude: latitude of the accession
      :var FloatField longitude: longitude of the accession
      :var CountryField country_of_origin: country of the accession
      :var CharField donors: donors of the accession
      :var CharField institute_name: name of the institute of the accession
      :var CharField institute_code: code of the institute of the accession
      :var ManyToManyField projects: project(s) linked to the accession
    """
    
    type = models.ForeignKey('AccessionType', db_index=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, unique=True, db_index=True)
    doi = models.CharField(max_length=100, null=True, unique=True)
    private_doi= models.BooleanField(default=True)
    pedigree = models.TextField(max_length=500, blank=True, null=True)
    is_obsolete = models.BooleanField(default= False)
    description = models.TextField(max_length=500, blank=True, null=True)
    variety = models.CharField(max_length=100, blank=True, null=True)
    attributes_values = models.JSONField(blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    country_of_origin = CountryField(blank=True, null=True)
    donors = models.CharField(max_length=100, blank=True, null=True)
    institute_name = models.CharField(max_length=100, blank=True, null=True)
    institute_code = models.CharField(max_length=100, blank=True, null=True)
    objects = AccessionManager() 
    
    projects = models.ManyToManyField('team.Project', blank = True)
    
    ACCESSION_FILE_HEADERS = ['Name','DOI','Private DOI','Pedigree','Is Obsolete','Description','Variety','Latitude','Longitude','Country of Origin','Donors','Institute Name','Institute Code','Projects']
    
    class Meta:
        ordering = ['name',]
        
    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.doi == "" :
            self.doi=None
        super(Accession, self).save(*args, **kwargs)
        
    def relation_set(self):
        return self.accessionrelation_set
    
    def update_attributes(self,attributes, postdata):
        jsonvalues = {}
        for att in attributes:
            if att.name.lower().replace(' ','_') in postdata.keys():
                jsonvalues[att.id] = postdata[att.name.lower().replace(' ','_')]
            else:
                jsonvalues[att.id] = ''
        self.attributes_values = jsonvalues
        
    def update_projects(self,projectss):
        Project = apps.get_model('team','project')
        #for i in range(len(projectss)):
            #project_id = projectss[i]
        self.projects.clear()
        try :
            self.projects.add(*Project.objects.filter(id__in=projectss))
        except ValueError :
            print("One of the following argument is not a project id : {0}.".format(projectss))
#         for project_id in projectss:
#             try:
#                 project = Project.objects.get(id=project_id)
#                 self.projects.add(project)
#             except ObjectDoesNotExist:
#                 print("The project with id {0} does not exist.".format(project_id))

    def projects_list(self):
        return ', '.join([p.name for p in self.projects.all()])
    
    
    #for BrAPI 
    """
      Some functions for BrAPI serializers in accession.serializers.py
    """ 
    def get0Value(self):
        return 0 
    
    def getBlankValue(self):
        return ''
    
    def getAcquisitionDate(self):
        AccessionRelation = apps.get_model('genealogy_accession', 'accessionrelation')
        accessionRelation = AccessionRelation.objects.filter(child = self).distinct()
        if accessionRelation:
            return accessionRelation[0].first_production
        else:
            return ''
    
    def getBiologicalStatusOfAccessionCode(self):
        return self.type.code_mcpd
    
    def getBiologicalStatusOfAccessionDescription(self):
        return self.type.description
    
    def getBreedingMethodDbId(self):
        Reproduction = apps.get_model('genealogy_managers', 'reproduction')
        reproduction = Reproduction.objects.filter(accessionrelation__child = self).distinct()
        if reproduction:
            return reproduction[0].reproduction_method.id
        else:
            return ''
        
    def getBreedingMethodName(self):
        Reproduction = apps.get_model('genealogy_managers', 'reproduction')
        reproduction = Reproduction.objects.filter(accessionrelation__child = self).distinct()
        if reproduction:
            return reproduction[0].reproduction_method.name
        else:
            return '' 
    
    def getChild(self):
        AccessionRelation = apps.get_model('genealogy_accession', 'accessionrelation')
        child = AccessionRelation.objects.filter(parent=self)
        serializer = AccessionRelationChildSerializer(child, many=True)
        if child:
            return serializer.data
        else:
            return ''  
        
    def getCommonCropName(self):
        return settings.COMMONCROPNAME  
    
    def getCountryOfOriginCode(self):
        code = self.country_of_origin.alpha3
        return code
    
    def getGenus(self):
        return settings.GENUS 
    
    def getGermplasmAdditionalInfo(self):
        additionalInfo = {}
        
        if not bool(self.attributes_values) :
            for k,value in self.attributes_values.items():
                attribute = AccessionTypeAttribute.objects.get(id = k)
                additionalInfo[attribute.attribute_name] = value
        return additionalInfo
    
    def getGermplasmOrigin(self):  
        if self.longitude is not None and self.latitude is not None: 
            coordinates = [self.longitude, self.latitude]
        else :
            coordinates = []
        return [{'coordinateUncertainty' : '', 
                 'coordinates':
                 {'geometry':
                  {'coordinates':coordinates}, 'type' : 'Feature'}}] 
    
    def getParents(self):
        AccessionRelation = apps.get_model('genealogy_accession', 'accessionrelation')
        parents = AccessionRelation.objects.filter(child=self)
        serializer = AccessionRelationParentSerializer(parents, many=True)
        if parents:
            return serializer.data
        else:
            return '' 
        
    def getPedigreeAdditionalInfo(self): 
        pedigreeAdditionalInfo = {}
        if self.doi :
            pedigreeAdditionalInfo['germplasmDoi'] = self.doi
        if self.private_doi :
            pedigreeAdditionalInfo['germplasmPrivateDoi'] = str(self.private_doi)    
        if self.is_obsolete :
            pedigreeAdditionalInfo['germplasmIsObsolete'] = self.is_obsolete  
        if self.description :
            pedigreeAdditionalInfo['germplasmDescription'] = self.description
        if self.variety :
            pedigreeAdditionalInfo['germplasmVariety'] = self.variety    
        if self.attributes_values :
            pedigreeAdditionalInfo['germplasmAttributesValues'] = str(self.attributes_values) 
        if self.latitude :
            pedigreeAdditionalInfo['germplasmPositionLatitude'] = str(self.latitude)
        if self.longitude :
            pedigreeAdditionalInfo['germplasmPositionLongitude'] = str(self.longitude)  
        return pedigreeAdditionalInfo
        
    def getReferences(self):
        return [{'referenceId' : self.doi,'referenceSource': 'DOI'}]
    
    def getSiblings(self):
        from accession.serializers import AccessionSiblingBrAPISerializer
        
        AccessionRelation = apps.get_model('genealogy_accession', 'accessionrelation')
        parents = AccessionRelation.objects.filter(child=self)
        qrel = Q()
        for p in parents :
            qrel = qrel|Q(relations_as_parent__parent=p.parent)
        siblings = Accession.objects.filter(qrel).exclude(id=self.id).distinct()
        serializer = AccessionSiblingBrAPISerializer(siblings, many=True)
        return serializer.data
       
    def getSpecies(self):
        return settings.SPECIES
    
    def getSpeciesAuthority(self):
        return settings.SPECIESAUTHORITY
    
    def getSubtaxa(self):
        return settings.SUBTAXA
    
    def getSubtaxaAuthority(self):
        return settings.SUBTAXAAUTHORITY


#move to proxy_models.py
class AccessionFromPedigree(Accession):
    class Meta:
        proxy = True
        verbose_name = 'Accession from pedigree'
        

class AccessionFromVariety(Accession):
    class Meta:
        proxy = True
        verbose_name = 'Accession from variety'


@receiver(models.signals.m2m_changed, sender=Accession.projects.through)
def report_projects_accession(sender, instance, action, pk_set, **kwargs):
    """
      Actions to be performed according to the link or unlink of projects.
      If projects are removed, they are also removed for the seedlot.
    """
    Project = apps.get_model('team','project')
    
    if action == 'post_remove':
        projects = Project.objects.filter(id__in=pk_set)
        
        for project in projects:
            seedlots = instance.seedlot_set.all()
            if seedlots:
                for seedlot in seedlots:
                    if project in seedlot.projects.all():
                        seedlot.projects.remove(project)


class AccessionTypeAttribute(models.Model):
    """
      The AccessionTypeAttribute model gives information about the attributes linked to the accession type
      
      :var CharField name: name of the attribute
      :var IntegerField type: type of the attribute
      :var ManyToManyField accession_type: accession type linked to the attribute, defined in the AccessionType model
    """
    class AttributeType(models.IntegerChoices):
        SHORT_TEXT = 1,"Short Text"
        LONG_TEXT = 2,"Long Text"
        NUMBER = 3,"Number"
        URL = 4,"URL"
        
    name = models.CharField(max_length=100)
    type = models.IntegerField(choices=AttributeType.choices)
    accession_type = models.ManyToManyField('AccessionType', through='AccessionAttributePosition')
    description = models.TextField(blank=True, null=True)
    
    
    objects = EntityTypeManager()
    
    def natural_key(self):
        return (self.name, self.type)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ("name","type")
        ordering = (Upper('name'),'type')
    
    def clean(self):
        acc_fields = Accession._meta.get_fields()
        acc_fields_names = [f.name for f in acc_fields]
        
        if self.name.lower().replace(' ','_') in acc_fields_names:
            raise ValidationError('A field with this name already exists for accessions.')
     
    def save(self,*args, **kwargs):
        self.clean()
        super(AccessionTypeAttribute,self).save(*args, **kwargs)
    
    def getAttributeValueAdditionalInfo(self):
        return [{'AccessionType' : self.accession_type }]
               
    def getBlankValue(self):
        return ''

class AccessionAttributePosition(models.Model):
    """
      The AccessionAttributePosition model gives information about the position of the attribute. 
      It allows the movement of the attribute thanks to a drag and drop system and the memorization of the position of this attribute.
      
      :var ForeignKey attribute: name of the attribute, defined with the AccessionTypeAttribute model
      :var ForeignKey type: type of the accession which is linked to the attribute, defined with the AccessionType model
      :var IntegerField position: integer rendering the position number
    """
    
    attribute = models.ForeignKey('AccessionTypeAttribute', on_delete=models.CASCADE)
    type = models.ForeignKey('AccessionType', on_delete=models.CASCADE)
    position = models.IntegerField()  
    
    objects = AccessionAttributePostionManager()
    
    class Meta:
        unique_together = (("type",'attribute'),('type',"position"))
        ordering = ('type','position')      
        