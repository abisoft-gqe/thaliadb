#-*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.urls import path
from accession.views import accessionhome, accession_type, accessiondata, AccessionAttributesAutocomplete, accessiontype_edit, accessionattributes, \
                            accessionattribute_edit
urls_as_dict = {
    'accessionhome' : {'path':'home/', 'view':accessionhome,},
    'accessiontype' : {'path':'types/', 'view':accession_type, 'docpath':'admin/accession.html#defining-accession-types-and-attributes'},
    'edit_accessiontype' : {'path':'types/<int:pk>/', 'view':accessiontype_edit, 'docpath':'admin/accession.html#defining-accession-types-and-attributes'},
    'accessionattributes' : {'path':'attributes/', 'view':accessionattributes, 'docpath':'admin/accession.html#defining-accession-types-and-attributes'},
    'edit_accessionattribute' : {'path':'attributes/<int:pk>/', 'view':accessionattribute_edit, 'docpath':'admin/accession.html#defining-accession-types-and-attributes'},
    'accessiondata' : {'path':'accessiondata/<int:type_id>/', 'view':accessiondata, 'docpath':'admin/accession.html#accession-management'},
    'accession-attributes-autocomplete' : {'path':'attributes-autocomplete/', 'view':AccessionAttributesAutocomplete.as_view(),},
    }

urlpatterns = [path(url['path'], url['view'], name=namespace) for namespace, url in urls_as_dict.items()]

# urlpatterns = [
#     path('home/', accessionhome, name='accessionhome'),                       
#     path('types/', accession_type, name='accessiontype'),
#     path('types/<int:pk>/', accessiontype_edit, name='edit_accessiontype'),
#     path('attributes/', accessionattributes, name='accessionattributes'),
#     path('attributes/<int:pk>/', accessionattribute_edit, name='edit_accessionattribute'),
#     path('accessiondata/<int:type_id>/', accessiondata, name='accessiondata'),
#     path('attributes-autocomplete/', AccessionAttributesAutocomplete.as_view(), name='accession-attributes-autocomplete'),
# ]