# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import json
import csv
import ast

from dal import autocomplete

from django_countries import countries as dc_countries
from django.db.models import Q
from django.forms.models import model_to_dict
from django.db.models.query import QuerySet, Prefetch
from django.shortcuts import render, redirect
from django.db import IntegrityError, transaction
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages

from accession.models import AccessionType, AccessionTypeAttribute, AccessionAttributePosition, Accession
from accession.forms import AccessionTypeForm, AccessionTypeAttForm, AccessionDataForm
from accession.forms import UploadFileForm, UploadFileFormWithoutDelimiter, AccessionTypeAttributeSearch
from genealogy_accession.models import AccessionRelation
from team.models import Project
from commonfct.genericviews import generic_management, get_fields
from commonfct.utils import recode
from commonfct.constants import ACCEPTED_TRUE_VALUES, filerenderer_headers
from commonfct.search_csvfile import RefineSearchFactory
from csv_io.views import CSVFactoryForTempFiles

class AccessionAttributesAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return AccessionTypeAttribute.objects.none()
        qs = AccessionTypeAttribute.objects.all()
        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs
    
    def get_result_label(self, result):
        attribute_type = dict(AccessionTypeAttribute.AttributeType.choices)[result.type]
        label = "{0} ({1})".format(result.name, attribute_type)
        return label


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def accessionattributes(request):
    form = AccessionTypeAttForm()
    template = 'accession/accession_attributes.html'
    fields_name, fields_label = get_fields(form)
    template_data = {'fields_label':fields_label,
                    'fields_name':fields_name,
                    'all': AccessionTypeAttribute.objects.all().order_by('name'),
                    'form' : form,
                    'urlname':'edit_accessionattribute',
                    'creation_mode':True}
    
    if request.method == "POST" :
        form = AccessionTypeAttForm(request.POST)
        if form.is_valid() :
            form.save()
            template_data.update({'form':AccessionTypeAttForm()})
            return render(request, template, template_data)
        else :
            template_data.update({'form':form})
            return render(request, template, template_data)
    return render(request, template, template_data)


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def accessionattribute_edit(request, pk=None):
    attribute = AccessionTypeAttribute.objects.get(id=pk)
    form = AccessionTypeAttForm(instance=attribute)
    template = 'accession/accession_attributes.html'
    fields_name, fields_label = get_fields(form)
    template_data = {'fields_label':fields_label,
                    'fields_name':fields_name,
                    'all': AccessionTypeAttribute.objects.all(),
                    'form' : form,
                    'urlname':'edit_accessionattribute',
                    'creation_mode':False}
    if request.method == "POST" :
        form = AccessionTypeAttForm(request.POST, instance=attribute)
        if form.is_valid() :
            form.save()
            template_data.update({'form':AccessionTypeAttForm(),'creation_mode':True })
            return render(request, template, template_data)
        else :
            template_data.update({'form':form})
            return render(request, template, template_data)
    return render(request, template, template_data)


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def accession_type(request):
    form = AccessionTypeForm()
    template = 'accession/accession_type.html'
    fields_name, fields_label = get_fields(form)
    template_data = {'fields_label':fields_label,
                    'fields_name':fields_name,
                    'all': AccessionType.objects.all(),
                    'form' : form,
                    'urlname':'edit_accessiontype',
                    'creation_mode':True}
    
    if request.method == "POST" :
        form = AccessionTypeForm(request.POST)
        if form.is_valid() :
            form.save()
            template_data.update({'form':AccessionTypeForm()})
            return render(request, template, template_data)
        else :
            template_data.update({'form':form})
            return render(request, template, template_data)
    return render(request, template, template_data)


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def accessiontype_edit(request, pk=None):
    try :
        accession_type = AccessionType.objects.get(id=pk)
        form = AccessionTypeForm(instance=accession_type)
        formatt = AccessionTypeAttributeSearch()
        template = 'accession/accession_type.html'
        fields_name, fields_label = get_fields(form)
        template_data = {'fields_label':fields_label,
                        'fields_name':fields_name,
                        'all': AccessionType.objects.all(),
                        'form' : form,
                        'urlname':'edit_accessiontype',
                        'attributeform':formatt,
                        'creation_mode':False,
                        'attributes':accession_type.accessionattributeposition_set.all().order_by('position')}
        if request.method == "POST" :
            if request.POST.get('submitedform') == "accessiontype" :
                form = AccessionTypeForm(request.POST, instance=accession_type)
                if form.is_valid() :
                    form.save()
                    template_data.update({'form':AccessionTypeForm(),'creation_mode':True })
                    return render(request, template, template_data)
                else :
                    template_data.update({'form':form})
                    return render(request, template, template_data)
            elif request.POST.get('submitedform') == "removingattribute" :
                attribute_id = request.POST.get('attributeid')
                template_data.update({'activetab':1})
                try :
                    accession_type.remove_attribute(attribute_id)
                    messages.add_message(request,messages.SUCCESS, "Attribute have been removed from the list")
                except AccessionTypeAttribute.DoesNotExist:
                    messages.add_message(request,messages.ERROR, "Attribute doesn't exists")
                return render(request, template, template_data)
            elif request.POST.get('submitedform') == "addattribute" :
                attribute_id = request.POST.get('attributes')
                template_data.update({'activetab':1})
                try :
                    with transaction.atomic() :
                        AccessionAttributePosition.objects.create_last_element(attribute_id, accession_type)
                except AccessionTypeAttribute.DoesNotExist:
                    messages.add_message(request,messages.ERROR, "Attribute doesn't exists")
                except IntegrityError as e :
                    messages.add_message(request, messages.ERROR, "Attribute can't be added")
                else :
                    messages.add_message(request,messages.SUCCESS, "Attribute have been added to the list")
                return render(request, template, template_data)
            elif request.POST.get('submitedform') == "reorderingattributes" :
                ordered_list = request.POST.getlist('attributes')
                print(ordered_list)
                accession_type.recompute_positions(ordered_list)
                template_data.update({'activetab':1})
                return render(request, template, template_data)
                    
        return render(request, template, template_data)
    except AccessionType.DoesNotExist :
        messages.add_message(request,messages.ERROR, "The AccessionType you are trying to update doesn't exist")
        return redirect('accessiontype')

@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def accessionhome(request):
    return render(request,'accession/accession_base.html',{"admin":True})

    
@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
#@cache_page(60 * 3)
def accessiondata(request, type_id):
    #Getting AccessionType corresponding to type_id
    accessiontype = AccessionType.objects.get(id=int(type_id))
    #Getting all attributes of this AccessionType
    attributes = accessiontype.accessiontypeattribute_set.all()
    # Building the form
    formattype = AccessionDataForm(formstype=attributes)
    
    # Building the FileForm
    register_formFile = UploadFileFormWithoutDelimiter()
    
    names, labels = get_fields(formattype)  
    names_genealogy = ['parent_male','parent_female','parent_x']
    labels_genealogy = ['Parent Male','Parent Female','Parent(s)']

            
    request.session['accession'] = 'accession'
    title = accessiontype.name+' Accession Management'
    
    template = 'accession/accession_data.html'   
    accessions, nb_per_page = _get_accessions(accessiontype, attributes, request)
    accessions_all = Accession.objects.filter(type=accessiontype).distinct()
    number_all = len(accessions_all)
    list_attributes=['name', 'doi', 'pedigree', 'description', 'variety', 'latitude', 'longitude', 'country_of_origin', 'donors', 'institute_name', 'institute_code', 'projects']
    list_attributes_id = []

    for i in AccessionTypeAttribute.objects.filter(accession_type=accessiontype):
        list_attributes_id.append(i.id)
    for i in list_attributes_id:
        if str(AccessionTypeAttribute.objects.get(id=i)) not in list_attributes:
            list_attributes.append(str(AccessionTypeAttribute.objects.get(id=i)))
    for i in ['parent male','parent female','parent(s)']:
        list_attributes.append(i)
        
    tag_fields = {'title':title,
                  'fields_name':names,
                  'fields_label':labels,
                  "names_genealogy":names_genealogy,
                  "labels_genealogy":labels_genealogy,
                  "admin":True,
                  "refine_search":True,
                  "list_attributes":list_attributes,
                  }
    
    if request.method == "GET":
        dico_get={}
        or_and = None
        ## refine search
        if "no_search" in request.GET:
            return render(request, 'accession/accession_data.html', {"refine_search":True,"admin":True,"list_attributes":list_attributes,'formfile':register_formFile,'filetype':'accession','typeid': type_id,'form':formattype,'creation_mode':True,"number_all":number_all, 'all':accessions, 'fields_label':labels,'fields_name':names})

        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,11):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera

            length_dico_get = len(dico_get)
            nb_per_page = 10

            accessions_init = Accession.objects.refine_search(or_and, accessiontype, dico_get)
            #print("juste après Refine search")
            number_all = len(accessions_init)
            dico = {}
            download_not_empty = False
            
            accessions = get_accession_or_seedlot_relations(accessions_init, AccessionRelation)

            if accessions:      
                #list_col = filerenderer_headers['Accession']
                list_col = Accession.ACCESSION_FILE_HEADERS.copy()
                
                attr_names = accessiontype.accessiontypeattribute_set.all()
                for at in attr_names:
                    list_col.append(str(at.name))

                for j in ['Parent Male','Parent Female','Parent(s)']:
                    list_col.append(j)
                
                #print("juste avant Refine factory")
                search_result = RefineSearchFactory()
                search_result.write_accession(accessions_init, list_col, AccessionTypeAttribute, AccessionRelation)
                #print("juste après Refine factory")  
                download_not_empty = True
                tag_fields.update({"download_not_empty":download_not_empty})

        #Cas où veut afficher la base de données
        tag_fields.update({"search_result":dico_get,
                           "or_and":or_and,
                           "refine_search":True,
                           'all':accessions,
                           "number_all":number_all,
                           'nb_per_page':nb_per_page,
                           'formfile':register_formFile,
                           'form':formattype,
                           'creation_mode':True,
                           'excel_empty_file':True,
                           'filetype':'accession',
                           'typeid': type_id,
                           "list_attributes":list_attributes})
        #print("juste avant render")
        return render(request, template, tag_fields)

    elif request.method == "POST":
        #Cas où veut modifier la base de données  
        if "hiddenid" in request.POST.keys() :
            return _update_accession_from_form(request, attributes, template, tag_fields, accessiontype)

        else : #Ajout de nouvelles données        
            #Formulaire
            formattype = AccessionDataForm(data=request.POST,formstype=attributes)

            # EXCEL FILE             
            formf = UploadFileFormWithoutDelimiter(request.POST, request.FILES) 
            
            if formf.is_valid():       
                tag_fields.update({'formfile':register_formFile,
                                   'filetype':'accession',
                                   "list_attributes":list_attributes,
                                   "number_all":number_all,
                                   'typeid': type_id,
                                   'form':AccessionDataForm(formstype=attributes),
                                   'creation_mode':True,
                                   'all':accessions,
                                   'nb_per_page':nb_per_page})
                return _create_accession_from_file(request, attributes, accessiontype, template, tag_fields)
            
            elif formattype.is_valid():
                tag_fields.update({'formfile':register_formFile,
                                  'filetype':'accession',"number_all":number_all,
                                  'typeid': type_id,"list_attributes":list_attributes,
                                  'excel_empty_file':True,
                                  'form':AccessionDataForm(formstype=attributes),
                                  'creation_mode':True, 
                                  'all':accessions,
                                  'nb_per_page':nb_per_page})
                return _create_accession_from_form(formattype, accessiontype, attributes, request, template, tag_fields)
                
            else :
                errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in formattype.errors.items() ])
                return render(request, template, {'formfile':register_formFile,"list_attributes":list_attributes,'filetype':'accession','typeid': type_id,'nb_per_page':nb_per_page,'form':AccessionDataForm(formstype=attributes),'formfile':register_formFile,'creation_mode':True,"number_all":number_all, 'all':accessions, 'fields_label':labels,'fields_name':names,'errormsg':errormsg})
                #return render(request, template, {'form':formattype,'creation_mode':False, 'all':accessions, 'fields_label':labels,'fields_name':names})    
            #return render(request, 'accession/accession_data.html', {'form':formattype,'creation_mode':True, 'all':accessions, 'fields_label':labels,'fields_name':names})
    else :
        return render(request, 'accession/accession_data.html', {"list_attributes":list_attributes,'formfile':register_formFile,'filetype':'accession','typeid': type_id,'form':formattype,'creation_mode':True,"number_all":number_all, 'all':accessions, 'fields_label':labels,'fields_name':names})


def _create_accession_from_file(request, attributes, accessiontype, template, tag_fields):
    """
      View to render the template after the upload of the file.
    """
    _upload_file(request.FILES['file'], attributes, accessiontype, request)
    accessions, nb_per_page = _get_accessions(accessiontype, attributes, request)
    accessions_all = Accession.objects.filter(type=accessiontype).distinct()
    number_all = len(accessions_all)
    tag_fields.update({'all':accessions,"number_all":number_all})
    return render(request, template, tag_fields)


def _create_accession_from_form(formattype, accessiontype, attributes, request, template, tag_fields):
    #Insertion depuis un forumulaire
    newaccession = formattype.save(commit=False)
    newaccession.type = accessiontype
    newaccession.update_attributes(attributes, request.POST)
    newaccession.save()
    
    if 'projects' in request.POST:
        newaccession.update_projects(request.POST.getlist('projects'))
        
    accessions, nb_per_page = _get_accessions(accessiontype, attributes, request)
    accessions_all = Accession.objects.filter(type=accessiontype).distinct()
    number_all = len(accessions_all)
    tag_fields.update({'all':accessions,"number_all":number_all,})
    return render(request, template, tag_fields)
 
def _update_accession_from_form(request, attributes, template, tag_fields, accessiontype):
    #Edition des données
    try : 
        #on récupère l'objet déjà existant puis on utilise les fonctionnalités du formulaire pour l'updater
        instance = Accession.objects.get(id=request.POST['hiddenid'])
        form = AccessionDataForm(instance=instance, data=request.POST, formstype=attributes)
        
        if form.is_valid():
            instance = form.save()
            instance.update_attributes(attributes, request.POST)
            instance.save()
            if 'projects' in request.POST:
                instance.update_projects(request.POST.getlist('projects'))

            accessions, nb_per_page = _get_accessions(accessiontype, attributes, request)     
            accessions_all = Accession.objects.filter(type=accessiontype).distinct()
            number_all = len(accessions_all)                       
            tag_fields.update({'all':accessions,
                               "number_all":number_all,
                               'nb_per_page':nb_per_page,
                               'form':form,
                               'creation_mode':False,
                               'object':instance,
                               'hiddenid':instance.id})
            return render(request,template,tag_fields)
        else :
            #sinon on génère un message d'erreur qui sera affiché
            accessions, nb_per_page = _get_accessions(accessiontype, attributes, request)     
            accessions_all = Accession.objects.filter(type=accessiontype).distinct()
            number_all = len(accessions_all)       
            errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form.errors.items() ])
            tag_fields.update({'all':accessions,
                               "number_all":number_all,
                               'nb_per_page':nb_per_page,
                               'form':form,
                               'creation_mode':False,
                               'object':instance,
                               'hiddenid':instance.id,
                               'errormsg':errormsg})
            return render(request,template,tag_fields)
    except ObjectDoesNotExist :
        tag_fields.update({'form':form,
                            'creation_mode':True,
                            'errormsg':"The object you try to update doesn't exist in the database"})
        return render(request,template,tag_fields)

def _get_accessions(accessiontype, attributes, request, ):
    accessions_init = Accession.objects.prefetch_related(
                      'projects',
                      Prefetch('relations_as_child', queryset=AccessionRelation.objects.select_related('parent').all(), to_attr='rel_as_child'),
                ).filter(type=accessiontype).order_by('name')

    accessions = get_accession_or_seedlot_relations(accessions_init, AccessionRelation)

    # Ici, on ne peut pas utiliser accessions.iterator()
    # car sur notre ModelForm les attributs dynamiques ne font pas partie
    # de la liste d'attributs du Model
    for acc in accessions: 
        for att in attributes:
            #On vérifie si au moins un des attributs a été rempli
            if acc[0].attributes_values :
                #Lorsque les données sont insérées depuis un .csv plutôt qu'à la main, acc.attributes_values est traité comme un String contenant le JSON
                #Il faut donc vérifier la nature de acc.attributes_values
                if type(acc[0].attributes_values) is dict:
                    attributes_values=acc[0].attributes_values
                else:
                    attributes_values =json.loads(acc[0].attributes_values)
                #si l'id de l'attribut "att" est présent dans les clés de attributes_values
                if str(att.id) in attributes_values.keys() :
                    try:
                        setattr(acc[0],att.name.lower().replace(' ','_'),attributes_values[str(att.id)])
                    except ValueError as e:
                        print(e)
                        setattr(acc[0],att.name.lower().replace(' ','_'),'')
                else :
                    setattr(acc[0],att.name.lower().replace(' ','_'),'')
            else :
                setattr(acc[0],att.name.lower().replace(' ','_'),'')
    
    # Adding Paginator  
    try:
        if request.method == "GET":
            nb_per_page=request.GET['nb_per_page']  
        elif request.method == "POST":
            nb_per_page=request.POST["nb_per_page"]   
        if nb_per_page=="all":
            nb_per_page = len(accessions)
    except:
        nb_per_page=50
    
    
    paginator = Paginator(accessions, nb_per_page)
    page = request.GET.get('page')
#     print("paginator: ",paginator.object_list)
    try:
        accessions = paginator.page(page)
    except PageNotAnInteger:
        # If page number is not an interger then page one
        accessions = paginator.page(1)
    except EmptyPage:
        # invalid page number show the last
        accessions = paginator.page(paginator.num_pages)    
    return accessions, nb_per_page


def get_accession_or_seedlot_relations(acc_or_sl_init, model_relation):
    """
      This function returns a list of accessions or seedlots formatted 
      for the Admin table with the lists of parents for each object.
    """
    acc_or_sl=[]
    
    for i in acc_or_sl_init:
        relations = i.rel_as_child
        list_relation = ['','','']
        
        if relations:
            list_rel_m = []
            list_rel_f = []
            list_rel_x = []
            
            for rel in relations:
                if rel.parent_gender == 'M':
                    list_rel_m.append(str(rel.parent))
                elif rel.parent_gender == 'F':
                    list_rel_f.append(str(rel.parent))
                elif rel.parent_gender == 'X':
                    list_rel_x.append(str(rel.parent))
             
            list_relation = [', '.join(list_rel_m), ', '.join(list_rel_f), ', '.join(list_rel_x)]
        
        acc_or_sl.append([i,list_relation])
        
    return acc_or_sl


def _upload_file(file, attributess, accestype, request):
    """
      Reading of the file in order to insert or update data.
    """
    myfactory = CSVFactoryForTempFiles(file)
    myfactory.find_dialect()
    myfactory.read_file()
    file_dict = myfactory.get_file()
    
    #headers_list = filerenderer_headers['Accession']
    headers_list = Accession.ACCESSION_FILE_HEADERS.copy()
    
    missing = myfactory.check_headers_conformity(headers_list)
    
    if missing != []:
        messages.add_message(request, messages.ERROR, "One or several mandatory headers are missing in your file : "+format(', '.join(missing))+\
                             ". The required headers for this file are: "+format(', '.join(headers_list))+".")
        return
    
    if request.POST.get("submit"): #Cas d'une insertion de nouvelles données
        _submit_upload_file(request, file_dict, attributess, accestype)
    
    elif request.POST.get("update"): #L'utilisateur a cliqué sur le bouton update
        _update_upload_file(request, file_dict, attributess)
    
    else:
        messages.add_message(request, messages.ERROR,'ThaliaDB was unable to identify if you\'re trying to insert new data or to update former data.')


def _submit_upload_file(request, file_dict, attributess, accestype):
    """
      Insertion of accession data from the file.
    """
    with transaction.atomic():
        jsonvalues={}
        for line in file_dict:
            try:
                with transaction.atomic(): 
                    for att in attributess:
                        value = line[str(att.name).strip()]
                        if value != None or value !='':
                            jsonvalues[att.id] = value   
                        else:
                            jsonvalues[att.id] = ''
                            
                    if line["Country of Origin"] != "" and line["Country of Origin"] not in dict(dc_countries).keys():
                        raise Exception("The country code is unknown, please check https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes to find the ISO 3166 country's code.")
                            
                    if line["Latitude"] != "" and line["Longitude"] != "":
                        a  = Accession.objects.create(name = str(line['Name']),
                                                      type=accestype,
                                                      doi = str(line['DOI']),
                                                      pedigree = str(line['Pedigree']),
                                                      description = str(line['Description']),
                                                      variety = str(line['Variety']),
                                                      latitude = line['Latitude'],
                                                      longitude = line['Longitude'],
                                                      country_of_origin = line["Country of Origin"],
                                                      donors = str(line["Donors"]),
                                                      institute_name = str(line["Institute Name"]),
                                                      institute_code = str(line["Institute Code"]),
                                                      is_obsolete = (str(line['Is Obsolete']) in ACCEPTED_TRUE_VALUES ),
                                                      private_doi = (str(line['Private DOI']) in ACCEPTED_TRUE_VALUES ),
                                                      attributes_values = jsonvalues
                                                      )
                        a.save()
                    
                    else:
                        a  = Accession.objects.create(name = str(line['Name']),
                                                      type=accestype,
                                                      doi = str(line['DOI']),
                                                      pedigree = str(line['Pedigree']),
                                                      description = str(line['Description']),
                                                      variety = str(line['Variety']),
                                                      country_of_origin = line["Country of Origin"],
                                                      donors = str(line["Donors"]),
                                                      institute_name = str(line["Institute Name"]),
                                                      institute_code = str(line["Institute Code"]),
                                                      is_obsolete = (str(line['Is Obsolete']) in ACCEPTED_TRUE_VALUES ),
                                                      private_doi = (str(line['Private DOI']) in ACCEPTED_TRUE_VALUES ),
                                                      attributes_values = jsonvalues
                                                      )
                        a.save()
                    
                    project_list = [i.strip() for i in str(line['Projects']).split('|')]
                    projects = Project.objects.filter(name__in = project_list)
                    if projects.exists():
                        a.projects.add(*list(projects))
                            
                            
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+").")
                continue
            
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return
            
    messages.add_message(request, messages.SUCCESS, "The file has been successfully inserted.")


def _update_upload_file(request, file_dict, attributess):
    """
      Update of accession data from the file.
    """
    with transaction.atomic():
        for line in file_dict:
            jsonvalues ={}
            try:
                with transaction.atomic():
                    accession_to_update = Accession.objects.get(name=line['Name'])
                    
                    accession_to_update.doi = str(line['DOI'])
                    accession_to_update.pedigree = str(line['Pedigree'])
                    accession_to_update.description = str(line['Description'])
                    accession_to_update.variety = str(line['Variety'])
                    accession_to_update.donors = str(line["Donors"])
                    accession_to_update.institute_name = str(line["Institute Name"])
                    accession_to_update.institute_code = str(line["Institute Code"])
                    accession_to_update.latitude = line['Latitude'] if line['Latitude'] != '' else None
                    accession_to_update.longitude = line['Longitude'] if line['Longitude'] != '' else None
                    
                    if line["Country of Origin"] != "" and line["Country of Origin"] not in dict(dc_countries).keys():
                        raise Exception("The country code is unknown, please check https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes to find the ISO 3166 country's code.")
                    
                    accession_to_update.country_of_origin = line["Country of Origin"]
                    
                    if line['Private DOI'] == 'False' or line['Private DOI'] == '':
                        accession_to_update.private_doi = False
                    else:
                        accession_to_update.private_doi = True
                    
                    if line['Is Obsolete'] == 'False' or line['Is Obsolete'] == '':
                        accession_to_update.is_obsolete = False
                    else:
                        accession_to_update.is_obsolete = True
                    
                    if line['Projects']:
                        project_list = [p.strip() for p in str(line['Projects']).split('|')]
                        projects = Project.objects.filter(name__in = project_list)
                        if len(project_list) != len(projects):
                            messages.add_message(request, messages.ERROR, "There is an error with a project in this list: "+ ', '.join(project_list)+". (As a reminder, the separator must be | ).")
                            raise Exception
                        accession_to_update.projects.clear()
                        accession_to_update.projects.add(*list(projects))
                        
                    for att in attributess:
                        value = line[str(att.name).strip()]
                        if value != None or value !='':
                            jsonvalues[att.id] = value   
                        else:
                            jsonvalues[att.id] = ''
                    if type(accession_to_update.attributes_values) is dict:
                        attributes_in_database = accession_to_update.attributes_values
                    else:    
                        attributes_in_database = json.loads(accession_to_update.attributes_values)
                    shared_items = set(jsonvalues.items()) & set(attributes_in_database.items()) #Cette liste contient les valeurs en commun pour chaque clé
                    if len(shared_items) < len(attributes_in_database):
                        accession_to_update.attributes_values = jsonvalues
                    
                    accession_to_update.save()
                        
                    
            except Exception as e:
                if str(e) != "":
                    messages.add_message(request, messages.ERROR, str(e)+" (at line: "+str(line["Name"])+").")
                    continue
                
        #To cancel all the transactions if there is an error message
        for msg in messages.get_messages(request):
            if msg.level == 40:
                transaction.set_rollback(True)
                return

    messages.add_message(request, messages.SUCCESS, "The file has been successfully uploaded and the accessions have been updated.")
