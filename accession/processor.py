"""LICENCE
    ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from accession.models import AccessionType
from lot.models import SeedlotType
from genotyping.models import LocusType

def accession_menu(request):
    """
      This function returns the table containing the AccessionType objects ordered by name
    """
    return {'menu':AccessionType.objects.all().order_by('name')}

def seedlot_menu(request):
    """
      This function returns the table containing the SeedlotType objects ordered by name
    """
    return {'seedlot_menu':SeedlotType.objects.all().order_by('name')}

def locus_menu(request):
    """
      This function returns the table containing the LocusType objects ordered by name
    """
    #print "tous les locus:", LocusType.objects.all()
    return {'locus_menu':LocusType.objects.all().order_by('name')}
