from rest_framework import serializers

from accession.models import Accession, AccessionTypeAttribute

class AccessionSiblingBrAPISerializer(serializers.ModelSerializer):   
    """
        AccessionSiblingBrAPISerializer gives the id and the name of the accession
    """
    germplasmDbId = serializers.CharField(source = 'id')
    germplasmName = serializers.CharField(source = 'name')

    class Meta:      
        model = Accession
        fields = [
            'germplasmDbId',
            'germplasmName',
            ]


class GermplasmBrAPISerializer(serializers.ModelSerializer):
    """
        GermplasmBrAPISerializer gives the informations of the accession requested by the BrAPI
    """
    accessionNumber = serializers.CharField(source = 'name')
    acquisitionDate = serializers.DateField(source = 'getAcquisitionDate')
    additionalInfo = serializers.JSONField(source = 'getGermplasmAdditionalInfo')
    biologicalStatusOfAccessionCode = serializers.CharField(source = 'getBiologicalStatusOfAccessionCode')
    biologicalStatusOfAccessionDescription = serializers.CharField(source = 'getBiologicalStatusOfAccessionDescription')
    breedingMethodDbId = serializers.CharField(source = 'getBreedingMethodDbId')
    breedingMethodName = serializers.CharField(source = 'getBreedingMethodName')
    collection = serializers.CharField(source = 'projects_list')
    commonCropName = serializers.CharField(source = 'getCommonCropName')
    countryOfOriginCode = serializers.CharField(source = 'getCountryOfOriginCode')
    defaultDisplayName = serializers.CharField(source = 'name')
    documentationURL = serializers.CharField(source = 'getBlankValue')
    donors = serializers.ListField(source = 'getBlankValue')
    externalReferences = serializers.ListField(source = 'getReferences')
    genus = serializers.CharField(source = 'getGenus')
    germplasmDbId = serializers.CharField(source = 'id')
    germplasmName = serializers.CharField(source = 'name')
    germplasmOrigin = serializers.ListField(source = 'getGermplasmOrigin')
    germplasmPUI = serializers.CharField(source = 'getBlankValue')
    germplasmPreprocessing = serializers.CharField(source = 'getBlankValue')
    instituteCode = serializers.CharField(source = 'getBlankValue')
    instituteName = serializers.CharField(source = 'getBlankValue')
    seedSource = serializers.CharField(source = 'getBlankValue')
    seedSourceDescription = serializers.CharField(source = 'getBlankValue')
    species = serializers.CharField(source = 'getSpecies')
    speciesAuthority = serializers.CharField(source = 'getSpeciesAuthority')
    storageTypes = serializers.ListField(source = 'getBlankValue')
    subtaxa = serializers.CharField(source = 'getSubtaxa')
    subtaxaAuthority = serializers.CharField(source = 'getSubtaxaAuthority')
    synonyms = serializers.ListField(source = 'getBlankValue')
    taxonIds = serializers.ListField(source = 'getBlankValue')
    
    class Meta:        
        model = Accession
        fields = ['accessionNumber',
                  'acquisitionDate', 
                  'additionalInfo',
                  'biologicalStatusOfAccessionCode',
                  'biologicalStatusOfAccessionDescription',
                  'breedingMethodDbId',
                  'breedingMethodName',
                  'collection',
                  'commonCropName',
                  'countryOfOriginCode',
                  'defaultDisplayName', 
                  'documentationURL',
                  'donors',
                  'externalReferences',
                  'genus', 
                  'germplasmDbId',
                  'germplasmName',
                  'germplasmOrigin',
                  'germplasmPUI',
                  'germplasmPreprocessing',
                  'instituteCode',
                  'instituteName',
                  'pedigree',
                  'seedSource',
                  'seedSourceDescription',
                  'species',
                  'speciesAuthority',
                  'storageTypes',
                  'subtaxa',
                  'subtaxaAuthority',
                  'synonyms',
                  'taxonIds',
                  ]
        depth = 1
         
        
class PedigreeBrAPISerializer(serializers.ModelSerializer):
    """
        PedigreeBrAPISerializer gives the informations of the pedigree requested by the BrAPI
    """
    additionalInfo = serializers.JSONField(source = 'getPedigreeAdditionalInfo')
    breedingMethodDbId = serializers.CharField(source = 'getBreedingMethodDbId')
    breedingMethodName = serializers.CharField(source = 'getBreedingMethodName')
    crossingProjectDbId = serializers.CharField(source = 'getBlankValue') #a corriger (nom du projet lié au pedigree ?)
    crossingYear = serializers.IntegerField(source = 'get0Value')
    defaultDisplayName = serializers.CharField(source = 'name')
    externalReferences = serializers.ListField(source = 'getReferences')
    familyCode = serializers.CharField(source = 'type.name')
    germplasmDbId = serializers.CharField(source = 'id')
    germplasmName = serializers.CharField(source = 'name')
    germplasmPUI = serializers.CharField(source = 'getBlankValue')
    parents = serializers.ListField(source = 'getParents')
    pedigreeString = serializers.CharField(source = 'pedigree')
    progeny = serializers.ListField(source = 'getChild')
    siblings = serializers.ListField(source = 'getSiblings')

    class Meta:
        model = Accession
        fields = [ 
                  'additionalInfo',
                  'breedingMethodDbId',
                  'breedingMethodName',
                  'crossingProjectDbId',
                  'crossingYear',
                  'defaultDisplayName', 
                  'externalReferences',
                  'familyCode',
                  'germplasmDbId',
                  'germplasmName',
                  'germplasmPUI',
                  'parents',
                  'pedigreeString',
                  'progeny',
                  'siblings',
                  ]
        depth = 1
