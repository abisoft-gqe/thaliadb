"""LICENCE
    ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
import datetime

from django.test import TestCase
from django.apps import apps
from django.urls import reverse
#from django.core.urlresolvers import reverse
#from django.test import Client

class ViewsTest(TestCase):
    def setUp(self):
        User = apps.get_model('team','user')
        User.objects.create_superuser("admin", "admin", "Admin", "Admin")
        
        AccessionTypeAttribute = apps.get_model('accession','accessiontypeattribute')
        AccessionType = apps.get_model('accession','accessiontype')
        AccessionAttributePosition = apps.get_model('accession','accessionattributeposition')
        Accession = apps.get_model('accession','accession')
        Project = apps.get_model('team','project')
         
        attr = AccessionTypeAttribute.objects.create(name = "attribut1",
                                                     type = 1,
                                                     )
        attr2 = AccessionTypeAttribute.objects.create(name = "attribut1.1",
                                                     type = 1,
                                                     )
        attr3 = AccessionTypeAttribute.objects.create(name = "attribut1.2",
                                                     type = 2,
                                                     )
        attr4 = AccessionTypeAttribute.objects.create(name = "attribut1.3",
                                                     type = 3,
                                                     )
        attr5 = AccessionTypeAttribute.objects.create(name = "attribut 1.4",
                                                     type = 3,
                                                     )
        attr6 = AccessionTypeAttribute.objects.create(name = "Attribut1.5",
                                                     type = 3,
                                                     )
        acctype = AccessionType.objects.create(name = "Type1",
                                               description = "type 1 description")
        
        AccessionAttributePosition.objects.create(attribute=attr,type=acctype,position=1)
        AccessionAttributePosition.objects.create(attribute=attr2,type=acctype,position=2)
        AccessionAttributePosition.objects.create(attribute=attr3,type=acctype,position=3)
        AccessionAttributePosition.objects.create(attribute=attr4,type=acctype,position=4)
        AccessionAttributePosition.objects.create(attribute=attr5,type=acctype,position=5)
        AccessionAttributePosition.objects.create(attribute=attr6,type=acctype,position=6)
         
        Accession.objects.create(type=acctype, name='accession1')
        Accession.objects.create(type=acctype, name='accession2')
        Accession.objects.create(type=acctype, name='accession3')
        
        self.accession_type = acctype
        self.client.post('/', {'username': 'admin', 'password':'admin'})

    def test_url_bidon(self):
        response = self.client.get('/accession/ca/nexiste/pas/')
        self.assertEqual(response.status_code, 404)
        
    def test_accession_home(self):
        response = self.client.get('/accession/home/')
        self.assertEqual(response.status_code, 200)
    
    def test_accession_type(self):
        response = self.client.get('/accession/types/')
        self.assertEqual(response.status_code, 200)
    
    def test_post_accession_type(self):
        data = {'name':'Inbred Line',
                'attribute0':'Variety;1',
                'attribute1':'Boolean;3'}
        response = self.client.post('/accession/types/',data)
        
        self.assertEqual(response.status_code, 200)
    
    def test_get_accessiondata(self):
        response = self.client.get('/accession/accessiondata/{0}/'.format(self.accession_type.id))
        self.assertEqual(response.status_code, 200)

# class ModelsTest(TestCase):
#     def setUp(self):
#         AccessionTypeAttribute = apps.get_model('accession','accessiontypeattribute')
#         AccessionType = apps.get_model('accession','accessiontype')
#         AccessionAttributePosition = apps.get_model('accession','accessionattributeposition')
#         Accession = apps.get_model('accession','accession')
#         Project = apps.get_model('team','project')
#         
#         attr = AccessionTypeAttribute.objects.create(name = "attribut1",
#                                                      type = 1,
#                                                      )
#         attr2 = AccessionTypeAttribute.objects.create(name = "attribut1.1",
#                                                      type = 1,
#                                                      )
#         attr3 = AccessionTypeAttribute.objects.create(name = "attribut1.2",
#                                                      type = 2,
#                                                      )
#         attr4 = AccessionTypeAttribute.objects.create(name = "attribut1.3",
#                                                      type = 3,
#                                                      )
#         attr5 = AccessionTypeAttribute.objects.create(name = "attribut 1.4",
#                                                      type = 3,
#                                                      )
#         attr6 = AccessionTypeAttribute.objects.create(name = "Attribut1.5",
#                                                      type = 3,
#                                                      )
#         acctype = AccessionType.objects.create(name = "Type1",
#                                                description = "type 1 description")
#         acctype2 = AccessionType.objects.create(name = "Type2",
#                                                description = "type 2 description")
#         acctype3 = AccessionType.objects.create(name = "Type3",
#                                                description = "type 3 description")
#         AccessionAttributePosition.objects.create(attribute=attr,type=acctype2,position=1)
#         AccessionAttributePosition.objects.create(attribute=attr2,type=acctype2,position=2)
#         AccessionAttributePosition.objects.create(attribute=attr,type=acctype3,position=1)
#         AccessionAttributePosition.objects.create(attribute=attr3,type=acctype3,position=2)
#         AccessionAttributePosition.objects.create(attribute=attr4,type=acctype3,position=3)
#         AccessionAttributePosition.objects.create(attribute=attr5,type=acctype,position=1)
#         AccessionAttributePosition.objects.create(attribute=attr6,type=acctype3,position=4)
#         
#         accession1 = Accession.objects.create(type=acctype2, name='accession1')
#         accession2 = Accession.objects.create(type=acctype, name='accession2')
#         accession3 = Accession.objects.create(type=acctype3, name='accession3')
#         
#         project1 = Project.objects.create(name='project1', authors='author1',start_date=datetime.date.today())
#         project2 = Project.objects.create(name='project2', authors='author1',start_date=datetime.date.today())
#         project3 = Project.objects.create(name='project3', authors='author1',start_date=datetime.date.today())
#         
#         accession1.projects.add(project1, project2)
#         
#     def testAttribute_set(self):
#         AccessionType = apps.get_model('accession','accessiontype')
#         acctype2 = AccessionType.objects.get(name='Type2')
#         
#         self.assertEqual(acctype2.attribute_set(),acctype2.accessiontypeattribute_set)
#         
#     def testSave_doi(self):
#         Accession = apps.get_model('accession','accession')
#         AccessionType = apps.get_model('accession','accessiontype')
#         acc1=Accession()
#         acc1.name="acc1"
#         acc1.type=AccessionType.objects.get(name='Type2')
#         acc1.doi=""
#         acc1.save()
#         self.assertEqual(acc1.doi,None)
#         
#         acc1.doi="1"
#         acc1.save()
#         self.assertNotEqual(acc1.doi,None)
#         
#         acc1.doi=None
#         acc1.save()
#         self.assertEqual(acc1.doi,None)
#         
#     def testUpdateAttributes(self):
#         Accession = apps.get_model('accession','accession')
#         accession1 = Accession.objects.get(name='accession1')
#         attributes = accession1.type.accessiontypeattribute_set.all()
#         postdata = {"attribut1" : 'toto',
#                      "attribut1.1" : 'titi'}
#         accession1.update_attributes(attributes,postdata)
#         values_to_test = {1 : 'toto',
#                           2 : 'titi'}
#         self.assertDictEqual(accession1.attributes_values, values_to_test)
#         
#         accession2 = Accession.objects.get(name='accession2')
#         attributes2 = accession2.type.accessiontypeattribute_set.all()
#         postdata2 = {"attribut_1.4" : 'tutu'}
#         accession2.update_attributes(attributes2, postdata2)
#         values_to_test2 = {5 : 'tutu'}
#         self.assertDictEqual(accession2.attributes_values, values_to_test2)
#         
#         accession3 = Accession.objects.get(name='accession3')
#         attributes3 = accession3.type.accessiontypeattribute_set.all()
#         postdata3 = {"attribut1" : 'toto',
#                      "attribut1.2" : 'titi',
#                      "attribut1.3" : 'tutu',
#                      "attribut1.5" : 'tata'}
#         accession3.update_attributes(attributes3, postdata3)
#         values_to_test3 = {1 : 'toto',
#                            3 : 'titi',
#                            4 : 'tutu',
#                            6 : 'tata'}
#         self.assertDictEqual(accession3.attributes_values, values_to_test3)
#         
#     def testUpdateProjects(self):
#         Accession = apps.get_model('accession','accession')
#         Project = apps.get_model('team','project')
#         
#         projects_id = [2,3]
#         project_qs = Project.objects.filter(id__in=projects_id)
#         
#         accession1 = Accession.objects.get(name='accession1')
#         accession1.update_projects(projects_id)
#         
#         self.assertQuerysetEqual(accession1.projects.all(), project_qs, lambda x:x, False)
#         
        
# class ViewsTest(TestCase):
#     @classmethod
#     def setUpTestData(cls):
#         User = apps.get_model('team','user')
#         Person = apps.get_model('team','person')
#         AccessionType = apps.get_model('accession','accessiontype')
#         Accession = apps.get_model('accession','accession')
#         
#         cls.yannick = Person.objects.create(first_name = "Yannick",
#                               last_name = "DeOliveira")
#         cls.harry = Person.objects.create(first_name = "Harry",
#                               last_name = "Belcram")
#         cls.alice = Person.objects.create(first_name = "Alice",
#                               last_name = "Beaugrand")
#         cls.alain = Person.objects.create(first_name = "Alain",
#                                 last_name = "Charcosset")
#         cls.yannick_user = cls.user = User.objects.create(person = cls.yannick,
#                             login = 'ydeoliveira',
#                             is_team_admin = True,
#                             is_accession_admin = True,
#                             is_seedlot_admin = True,
#                             is_genotyping_admin = True,
#                             is_phenotyping_admin = True,
#                             is_classification_admin = True,)
#         cls.yannick_user.set_password('yannick')
#         cls.yannick_user.save()
#         cls.harry_user = User.objects.create(person = cls.harry,
#                             login = 'hbelcram',
#                             password = "harry",
#                             is_team_admin = False,
#                             is_accession_admin = False,
#                             is_seedlot_admin = False,
#                             is_genotyping_admin = False,
#                             is_phenotyping_admin = False,
#                             is_classification_admin = False,)
#         cls.harry_user.set_password('harry')
#         cls.harry_user.save()
#         cls.alice_user = User.objects.create(person = cls.alice,
#                             login = 'abeaugrand',
#                             password = "alice",
#                             is_team_admin = False,
#                             is_accession_admin = False,
#                             is_seedlot_admin = True,
#                             is_genotyping_admin = False,
#                             is_phenotyping_admin = False,
#                             is_classification_admin = False,)
#         cls.alice_user.set_password('alice')
#         cls.alice_user.save()
#         cls.alain_user = User.objects.create(person = cls.alain,
#                             login = 'acharcosset',
#                             password = "alain",
#                             is_team_admin = True,
#                             is_accession_admin = False,
#                             is_seedlot_admin = False,
#                             is_genotyping_admin = False,
#                             is_phenotyping_admin = False,
#                             is_classification_admin = False,)
#         cls.alain_user.set_password('alain')
#         cls.alain_user.save()
#         
#         cls.acctype1 = AccessionType.objects.create(name='acctype1')
#         cls.acctype2 = AccessionType.objects.create(name='acctype2')
#         cls.acctype3 = AccessionType.objects.create(name="acctype3")
#         
#         cls.acc1 = Accession.objects.create(name='acc_inra1',
#                                             type = cls.acctype1,
#                                             doi = '1',)
#         
#         
#         
#         
#     def testIsAccessionAdmin(self):   
#         response = self.client.get(reverse('accessionhome'))
#         self.assertRedirects(response,'/?next={0}'.format(reverse('accessionhome')) )
#             
#         self.client.login(login=self.yannick_user.login,password="yannick")
#         response = self.client.get(reverse('accessionhome'))
#         self.assertEqual(response.status_code, 200)
#         self.client.logout()
#         
#         self.client.login(login=self.harry_user.login,password="harry")
#         response = self.client.get(reverse('accessionhome'))
#         self.assertRedirects(response,'/team/?next={0}'.format(reverse('accessionhome')) )
#         self.client.logout()
#          
#         self.client.login(login=self.alice_user.login,password="alice")
#         response = self.client.get(reverse('accessionhome'))
#         self.assertRedirects(response,'/team/?next={0}'.format(reverse('accessionhome')) )
#         self.client.logout() 
#          
#         self.client.login(login=self.alain_user.login,password="alain")
#         response = self.client.get(reverse('accessionhome'))
#         self.assertRedirects(response,'/team/?next={0}'.format(reverse('accessionhome')) )
#         self.client.logout()
#         
#     def testGETAccessionType(self):
#         menu2test = {'acctype1','acctype2','acctype3'}
#         fields_label2test = ['Name', 'Description']
#         fields_name2test = ['name', 'description']
#         classname2test = 'Accession'
#         admin2test = True
#         creation_mode2test = True
#         title2test = 'Accession Type Management'
#         
#         response = self.client.get(reverse('accessiontype'))
#         self.assertRedirects(response,'/?next={0}'.format(reverse('accessiontype')) )
#         
#         self.client.login(login=self.harry_user.login,password="harry")
#         response = self.client.get(reverse('accessiontype'))
#         self.assertRedirects(response,'/team/?next={0}'.format(reverse('accessiontype')) )
#         self.client.logout()
#         
#         self.client.login(login=self.yannick_user.login,password="yannick")
#         response = self.client.get(reverse('accessiontype'))
#         self.assertEqual(response.status_code, 200)     
#         
#         menuset = set([acctype.name for acctype in response.context[-1]['menu']])        
#         self.assertEqual(menu2test, menuset)
#         self.assertEqual(fields_label2test, response.context['fields_label'])
#         self.assertEqual(fields_name2test, response.context['fields_name'])
#         self.assertEqual(classname2test, response.context['classname'])
#         self.assertEqual(title2test, response.context['title'])
#         self.assertEqual(admin2test, response.context['admin'])
#         self.assertEqual(creation_mode2test, response.context['creation_mode'])
#         self.client.logout()
#         
#     def testPOSTAccessionType(self):
#         self.client.login(login=self.yannick_user.login,password="yannick")
#         response = self.client.post(reverse('accessiontype'), {'name':'acctype4','attribute0':'accattr0;1','attribute1':'accattr1;2'})
# 
#         self.assertEqual('acctype4',response.context['object'].name)
#         attrposition = response.context['object'].accessionattributeposition_set.filter(position=1)[0]
#         attrposition2 = response.context['object'].accessionattributeposition_set.filter(position=2)[0]
#         #self.assertEqual(1, attrposition.position)
#         self.assertEqual('accattr0', attrposition.attribute.name)
#         self.assertEqual(1,attrposition.attribute.type)
#         self.assertEqual('accattr1', attrposition2.attribute.name)
#         self.assertEqual(2,attrposition2.attribute.type)
#         response = self.client.post(reverse('accessiontype'), {'name':'acctype4','attribute0':'accattr3;1'})
#         self.assertEqual('name : * Accession type with this Name already exists.',response.context['errormsg'])
#         
#         response = self.client.post(reverse('accessiontype'), {'name':'acctype5','attribute0':'projects;1'})
#         self.assertEqual("The field 'projects' already exists, please change the attribute name.",response.context['errormsg'])
#         
#     def testGETAccessionData(self):
#         response = self.client.get(reverse('accessiondata',args=[1]))
#         self.assertRedirects(response,'/?next={0}'.format(reverse('accessiondata',args=[1])) )
#         
#         self.client.login(login=self.harry_user.login,password="harry")
#         response = self.client.get(reverse('accessiondata',args=[1]))
#         self.assertRedirects(response,'/team/?next={0}'.format(reverse('accessiondata',args=[1])) )
#         self.client.logout()
#         
#         self.client.login(login=self.yannick_user.login,password="yannick")
#         response = self.client.get(reverse('accessiondata',args=[1]))
#         self.assertEqual(response.status_code, 200)     
#         self.client.logout()
#         
#     def testPOSTAccessionData(self):
#         self.client.login(login=self.yannick_user.login,password="yannick")
#         
#         #à faire dans un test de formulaires, pas dans la vue?
#         ##test form is valid
#         response1 = self.client.post(reverse('accessiondata',args=[1]), {'name':'acc_inra1'})
#         self.assertEqual(response1.status_code, 200)
#         self.assertEqual(True, 'errormsg' in response1.context)
#         
#         response2 = self.client.post(reverse('accessiondata',args=[1]), {'name':'acc_inra2','doi':'1'})
#         self.assertEqual(response2.status_code, 200)
#         self.assertEqual(True, 'errormsg' in response2.context)
#         
#         response3 = self.client.post(reverse('accessiondata',args=[1]), {'name':''})
#         self.assertEqual(response3.status_code, 200)
#         self.assertEqual(True, 'errormsg' in response3.context)
#         
#         response_ok = self.client.post(reverse('accessiondata',args=[1]), {'name':'acc_inra5','is_obsolete':False})
#         self.assertEqual(response_ok.status_code, 200)
#         self.assertEqual(True,'excel_empty_file' in response_ok.context)
#         
        ##test form file is valid
        #à faire dans un test de formulaires, pas dans la vue
#         with open('accession/tests/testPOSTAccessionData_file.txt') as test_file:
#             response = self.client.post(reverse('accessiondata',args=[1]), {'delimiter':',','file':test_file})
#         self.assertEqual(response.status_code, 200)
#         print(response.context)
#         self.assertEqual(True,'excel_empty_file' not in response.context and 'errormsg' not in response.context)
#         
#         response = self.client.post(reverse('accessiondata',args=[1]), {'delimiter':',','file':None})
#         self.assertEqual(response.status_code, 200)
#         self.assertEqual(True,'errormsg' in response.context)
        
#     def testCreateAccessionFromForm(self):response2
#         self.client.login(login=self.yannick_user.login,password="yannick")
#         response = self.client.post(reverse('accessiondata'), {'name':'acc_inra2',''})


        