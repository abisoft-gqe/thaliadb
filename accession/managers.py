"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Laetitia Courgey

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import ast

from django.apps import apps
from django.db import models
from django.db.models import Q
from django.db.models.query import Prefetch

from team.models import User, Project
from operator import ior, iand


class AccessionAttributePostionManager(models.Manager):
    
    def create_last_element(self, attribute_id, accession_type):
        AccessionTypeAttribute = apps.get_model('accession','accessiontypeattribute')
        
        attribute = AccessionTypeAttribute.objects.get(id=attribute_id)
        allattributes = accession_type.accessionattributeposition_set.order_by('-position')
        if allattributes :
            last_position = allattributes[0].position+1
        else :
            last_position = 1
        return self.create(attribute=attribute,
                    type=accession_type,
                    position=last_position)

class AccessionManager(models.Manager):
    """
    Manager for accession model.
    """
    def by_username(self,username):
        """
        Returns only the accessions from projects linked to this user 

        :param string username : username

        :var int username_id : user's id
        :var list projects : projects linked to the user
        """
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
        return self.filter(projects__in=projects).distinct()
    
    def refine_search(self, or_and, accessiontype, dico_get):
        """
        Search in all accession according to some parameters

        :param string or_and : link between parameters (or, and)
        :param AccessionType accessiontype : accession type to filter accessions
        :param dict dico_get : data to search for
        """
        # Managers are called in accession.model
        # To avoid crossed imports we use apps to get objects from the model 
        Accession = apps.get_model('accession', 'accession')
        AccessionTypeAttribute = apps.get_model('accession', 'accessiontypeattribute')
        AccessionRelation = apps.get_model('genealogy_accession','accessionrelation')

        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
            
        proj=[]
        query=Q()
        list_attr=[]
        for dat, dico_type_data in dico_get.items():
            if "projects" in dico_type_data:
                if dico_type_data['projects']=='':
#                     print('Projects empty')
                    query = myoperator(query, Q(projects__isnull=True))
                else:
                    proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                    query = myoperator(query, Q(projects__in=proj))
            elif 'parent male' in dico_type_data:
                parent_relation = []
                for acc in Accession.objects.filter(name__icontains=dico_type_data['parent male']):
                    acc_relation = acc.relations_as_parent.filter(parent_gender="M")
                    for i in acc_relation:
                        parent_relation.append(i.child)
                query = myoperator(query, Q(name__in=parent_relation))
            elif 'parent female' in dico_type_data:
                parent_relation = []
                for acc in Accession.objects.filter(name__icontains=dico_type_data['parent female']):
                    acc_relation = acc.relations_as_parent.filter(parent_gender="F")
                    for i in acc_relation:
                        parent_relation.append(i.child)
                query = myoperator(query, Q(name__in=parent_relation))
            elif 'parent(s)' in dico_type_data:
                parent_relation = []
                for acc in Accession.objects.filter(name__icontains=dico_type_data['parent(s)']):
                    acc_relation = acc.relations_as_parent.filter(parent_gender="X")
                    for i in acc_relation:
                        parent_relation.append(i.child)
                query = myoperator(query, Q(name__in=parent_relation))
                
            else:
                for data_type, value in dico_type_data.items():
                    list_name_in=[]
                    for i in AccessionTypeAttribute.objects.filter(accession_type=accessiontype):
                        if str(data_type) == str(i):
#                                     print(str(i))
                            list_attr.append(str(i))
                    if data_type in list_attr:
                        id_attribute = str(AccessionTypeAttribute.objects.get(name=data_type, accession_type=accessiontype).id)
                        for i in Accession.objects.filter(type=accessiontype):
                            try:
                                if id_attribute in i.attributes_values.keys():
                                    if value in ast.literal_eval(i.attributes_values)[id_attribute]:
                                        #print(str(i))
                                        list_name_in.append(str(i))
                            except:
                                try:
                                    if id_attribute in i.attributes_values.keys():
                                        if value in i.attributes_values[id_attribute]:
        #                                             print(str(i))
                                            list_name_in.append(str(i))
                                except:
                                    i.attributes_values = ast.literal_eval(i.attributes_values)
                                    if id_attribute in i.attributes_values.keys():
                                        if value in i.attributes_values[id_attribute]:
                                            list_name_in.append(str(i))
                                    
                        query = myoperator(query, Q(name__in = list_name_in))
                    else:
                        var_search = str( data_type + "__icontains" ) 
                        query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))
        
        query = Q(query, type=accessiontype)  
        return Accession.objects.prefetch_related(
                      'projects',
                      Prefetch('relations_as_child', queryset=AccessionRelation.objects.select_related('parent').all(), to_attr='rel_as_child'),
                ).filter(query).distinct()


    def viewer_refine_search(self, or_and, accessiontype, dico_get):
        """
          Search in all accession according to some parameters (special for accession viewer)

          :param string or_and : link between parameters (or, and)
          :param dict dico_get : data to search for
        """
        
        Accession = apps.get_model('accession', 'accession')
        AccessionType = apps.get_model('accession','accessiontype')
        AccessionTypeAttribute = apps.get_model('accession', 'accessiontypeattribute')
        Project = apps.get_model('team', 'project')
        Seedlot = apps.get_model('lot', 'seedlot')
        Sample = apps.get_model('genotyping', 'sample')
        
        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
            
        proj=[]
        query=Q()
        list_attr=[]
        for dat, dico_type_data in dico_get.items():
            for data_type, value in dico_type_data.items():
                var_search = str( data_type + "__icontains" )
                
                if "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        query = myoperator(query, Q(projects__isnull=True))
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query = myoperator(query, Q(projects__in=proj))
                
                elif "type" in dico_type_data:
                    type=(AccessionType.objects.filter(name__icontains=dico_type_data['type']))
                    acc_names=(Accession.objects.filter(type__in=type))
                    query = myoperator(query, Q(id__in=acc_names))
                    
                elif "sample" in dico_type_data or "seedlot" in dico_type_data:
                    if "sample" in dico_type_data:
                        sample_ids = Sample.objects.filter(name__icontains = dico_type_data['sample'])
                        seedlots = Seedlot.objects.filter(sample__in=sample_ids)
                    else:
                        seedlots = Seedlot.objects.filter(name__icontains = dico_type_data['seedlot'])
                    
                    acc_names = Accession.objects.filter(seedlot__in=seedlots)
                    query = myoperator(query, Q(id__in=acc_names))
                
                elif "accession" in dico_type_data:
                    acc_names = Accession.objects.filter(name__icontains=dico_type_data['accession'])
                    query = myoperator(query, Q(id__in=acc_names))
                
                else:
                    if accessiontype:
                        for data_type, value in dico_type_data.items():
                            list_name_in=[]
                            for i in AccessionTypeAttribute.objects.filter(accession_type=accessiontype):
                                if str(data_type) == str(i):
                                    list_attr.append(str(i))
                            if data_type in list_attr:
                                id_attribute = str(AccessionTypeAttribute.objects.get(name=data_type, accession_type=accessiontype).id)
                                for i in Accession.objects.filter(type=accessiontype):
                                    try:
                                        if id_attribute in i.attributes_values.keys():
                                            if value in ast.literal_eval(i.attributes_values)[id_attribute]:
                                                #print(str(i))
                                                list_name_in.append(str(i))
                                    except:
                                        try:
                                            if id_attribute in i.attributes_values.keys():
                                                if value in i.attributes_values[id_attribute]:
                                                    list_name_in.append(str(i))
                                        except:
                                            i.attributes_values = ast.literal_eval(i.attributes_values)
                                            if id_attribute in i.attributes_values.keys():
                                                if value in i.attributes_values[id_attribute]:
                                                    list_name_in.append(str(i))
                                            
                                query = myoperator(query, Q(name__in = list_name_in))
                            else:
                                var_search = str( data_type + "__icontains" ) 
                                query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))
                    
                        query = Q(query, type=accessiontype)
                    
                    else:
                        query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))
        
        return Accession.objects.filter(query).distinct()













