# Generated by Django 2.2 on 2021-05-03 15:15

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accession', '0010_merge_20180725_1519'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='accession',
            options={'ordering': ['name']},
        ),
    ]
